/**
 * Created by coretechies on 31/5/19.
 */

$(".selection-category").on("click", function(e) {
  e.preventDefault();
  var getid = $(this).attr("id");
  $(".selection-category").removeClass("active");
  $(this).addClass("active");
  console.log(getid);
  $(".menus").hide();
  if (getid === "sc-1") {
    $("#menu-bases").show();
  } else if (getid === "sc-2") {
    $("#menu-ingredients").show();
  } else if (getid === "sc-3") {
    $("#menu-extras").show();
  } else if (getid === "sc-4") {
    $("#menu-dressings").show();
  }
});

$(".btn-change-title").on("click", function() {
  $(this).hide();
  $(".btn-change-title-done").show();
  $(".bag-title-input").show();
  $(".bag-title").hide();
});
$(".btn-change-title-done").on("click", function() {
  $(this).hide();
  var gettitle = $(".bag-title-input").val();
  $(".bag-title").html(gettitle);
  $(".btn-change-title-done").hide();
  $(".btn-change-title").show();
  $(".bag-title-input").hide();
  $(".bag-title").show();
});

$(".btn-change-title-salad").on("click", function() {
  $(this).hide();
  $(".btn-change-title-done-salad").show();
  $(".salad-title-input").show();
  $(".salad-title").hide();
});
$(".btn-change-title-done-salad").on("click", function() {
  $(this).hide();
  var gettitle = $(".salad-title-input").val();
  console.log(gettitle);
  $(".salad-title").html(gettitle);
  $(".btn-change-title-done-salad").hide();
  $(".btn-change-title-salad").show();
  $(".salad-title-input").hide();
  $(".salad-title").show();
});

// jQuery(function($) {
//     function fixDiv() {
//         var $cache = $('.bag');
//         if ($(window).scrollTop() > 120)
//             $cache.css({
//                 'position': 'fixed',
//                 'right': '105px',
//                 'top': '20px',
//                 'z-index': '999'
//             });
//         else
//             $cache.css({
//                 'position': 'relative',
//                 'top': 'auto',
//                 'right':'auto',
//                 'float':'right'
//             });
//     }
//     $(window).scroll(fixDiv);
//     fixDiv();
// });

// $(document).ready(function() {
//   var limit = 7;
//   var start = 0;
//   var action = "inactive";
//
//   function lazzy_loader(limit) {
//     var output = "";
//     for (var count = 0; count < limit; count++) {
//       output +=
//         '<div class="col-md-6"> <div class="media"> <div class="media-img">';
//       output += " &nbsp;";
//       output +=
//         '</div><div class="media-body"><h4 class="media-heading">&nbsp;</h4>';
//       output += '  <h5 class="media-category">&nbsp;</h5>    ';
//       output +=
//         '<div class="mu-ingredients" style="margin-top: 16px">&nbsp;</div>';
//       output +=
//         '<span class="mu-menu-energy">&nbsp;</span>&nbsp;<span class="mu-menu-price">&nbsp;</span>  <span class="media-price">&nbsp;</span>';
//       output += "&nbsp;</div></div></div>";
//     }
//     $("#load_data_message").html(output);
//   }
//
//   lazzy_loader(limit);
//
//   function load_data(limit, start) {
//     var cat_id = $(".menu-tags .active").attr('id');
//     var table = "sm_ingredients";
//     var join_with_table = "sm_categories";
//     var join_condition = "sm_ingredients.cat_id=sm_categories.cat_id";
//     var select_fields = [
//       "sm_ingredients.ing_name",
//       "sm_ingredients.ing_name_arabic",
//       "sm_ingredients.ing_price",
//       "sm_ingredients.ing_image",
//       "sm_ingredients.ing_id",
//       "sm_ingredients.cat_id",
//       "sm_ingredients.ing_calories"
//     ];
//     var where = { cat_id: cat_id, ing_status: 1 };
//     var order_by = "ing_id DESC";
//
//     $.ajax({
//       url: base_url + "custom-salad-load-data",
//       method: "POST",
//       data: {
//         limit: limit,
//         start: start,
//         table: table,
//         join_with_table: join_with_table,
//         join_condition: join_condition,
//         select_fields: select_fields,
//         where: where,
//         order_by: order_by
//       },
//       cache: false,
//       success: function(data) {
//         if (data == "") {
//           $("#load_data_message").html(
//             '<h3 class="text-center">No More Ingredients</h3>'
//           );
//           action = "active";
//         } else {
//           $("#load_data").append(data);
//           $("#load_data_message").html("");
//           action = "inactive";
//         }
//       }
//     });
//   }
//
//   if (action == "inactive") {
//     action = "active";
//     load_data(limit, start);
//   }
//
//   $(window).scroll(function() {
//     if (
//       $(window).scrollTop() + $(window).height() > $("#load_data").height() &&
//       action == "inactive"
//     ) {
//       lazzy_loader(limit);
//       action = "active";
//       start = start + limit;
//       setTimeout(function() {
//         load_data(limit, start);
//       }, 1000);
//     }
//   });
// });
//
// $(".menu-tags a").on("click", function(e) {
//   $(".menu-tags a").removeClass("active");
//   $(this).addClass("active");
//   $("#load_data").html("");
//
//   var limit = 7;
//   var start = 0;
//   var action = "inactive";
//
//   function lazzy_loader(limit) {
//     var output = "";
//     for (var count = 0; count < limit; count++) {
//       output +=
//         '<div class="col-md-6"> <div class="media"> <div class="media-img">';
//       output += " &nbsp;";
//       output +=
//         '</div><div class="media-body"><h4 class="media-heading">&nbsp;</h4>';
//       output += '  <h5 class="media-category">&nbsp;</h5>    ';
//       output +=
//         '<div class="mu-ingredients" style="margin-top: 16px">&nbsp;</div>';
//       output +=
//         '<span class="mu-menu-energy">&nbsp;</span>&nbsp;<span class="mu-menu-price">&nbsp;</span>  <span class="media-price">&nbsp;</span>';
//       output += "&nbsp;</div></div></div>";
//     }
//     $("#load_data_message").html(output);
//   }
//
//   lazzy_loader(limit);
//
//   function load_data(limit, start) {
//     var cat_id = $(".menu-tags .active").attr('id');
//     var table = "sm_ingredients";
//     var join_with_table = "sm_categories";
//     var join_condition = "sm_ingredients.cat_id=sm_categories.cat_id";
//     var select_fields = [
//       "sm_ingredients.ing_name",
//       "sm_ingredients.ing_name_arabic",
//       "sm_ingredients.ing_price",
//       "sm_ingredients.ing_image",
//       "sm_ingredients.ing_id",
//       "sm_ingredients.cat_id",
//       "sm_ingredients.ing_calories"
//     ];
//     var where = { cat_id: cat_id, ing_status: 1 };
//     var order_by = "ing_id DESC";
//
//     $.ajax({
//       url: base_url + "custom-salad-load-data",
//       method: "POST",
//       data: {
//         limit: limit,
//         start: start,
//         table: table,
//         join_with_table: join_with_table,
//         join_condition: join_condition,
//         select_fields: select_fields,
//         where: where,
//         order_by: order_by
//       },
//       cache: false,
//       success: function(data) {
//         if (data == "") {
//           $("#load_data_message").html(
//             '<h3 class="text-center">No More Ingredients</h3>'
//           );
//           action = "active";
//         } else {
//           $("#load_data").append(data);
//           $("#load_data_message").html("");
//           action = "inactive";
//         }
//       }
//     });
//   }
//
//   if (action == "inactive") {
//     action = "active";
//     load_data(limit, start);
//   }
//
//   $(window).scroll(function() {
//     if (
//       $(window).scrollTop() + $(window).height() > $("#load_data").height() &&
//       action == "inactive"
//     ) {
//       lazzy_loader(limit);
//       action = "active";
//       start = start + limit;
//       setTimeout(function() {
//         load_data(limit, start);
//       }, 1000);
//     }
//   });
// });

$(document).ready(function() {
  var limit = 7;
  var start = 0;
  var action = "inactive";

  function lazzy_loader(limit) {
    var output = "";
    for (var count = 0; count < limit; count++) {
      output +=
        '<div class="col-md-6"> <div class="media"> <div class="media-img">';
      output += " &nbsp;";
      output +=
        '</div><div class="media-body"><h4 class="media-heading">&nbsp;</h4>';
      output += '  <h5 class="media-category">&nbsp;</h5>    ';
      output +=
        '<div class="mu-ingredients" style="margin-top: 16px">&nbsp;</div>';
      output +=
        '<span class="mu-menu-energy">&nbsp;</span>&nbsp;<span class="mu-menu-price">&nbsp;</span>  <span class="media-price">&nbsp;</span>';
      output += "&nbsp;</div></div></div>";
    }
    $("#load_data_message").html(output);
  }

  lazzy_loader(limit);

  function load_data(limit, start) {
    var page = "cart";
    var cat_id = $(".menu-tags .active").attr("id");
    var table = "sm_salads";
    var select_fields = [
      "salad_name",
      "salad_name_arabic",
      "salad_desc",
      "salad_desc_arabic",
      "salad_img",
      "cat_id",
      "salad_id",
      "salad_price",
      "salad_calories"
    ];
    var where = { cat_id: cat_id, salad_status: 1 };
    var order_by = "salad_id DESC";

    $.ajax({
      url: base_url + "menu-load-data",
      method: "POST",
      data: {
        cat_id: cat_id,
        limit: limit,
        start: start,
        table: table,
        select_fields: select_fields,
        where: where,
        order_by: order_by,
        page: page
      },
      cache: false,
      success: function(data) {
        if (data == "") {
          $("#load_data_message").html("<h3>No More Salads</h3>");
          action = "active";
        } else {
          $("#load_data").append(data);
          $("#load_data_message").html("");
          action = "inactive";
        }
      }
    });
  }

  if (action == "inactive") {
    action = "active";
    load_data(limit, start);
  }

  $(window).scroll(function() {
    if (
      $(window).scrollTop() + $(window).height() > $("#load_data").height() &&
      action == "inactive"
    ) {
      lazzy_loader(limit);
      action = "active";
      start = start + limit;
      setTimeout(function() {
        load_data(limit, start);
      }, 1000);
    }
  });
});

$(".menu-tags a").on("click", function(e) {
  e.preventDefault();
  $(".menu-tags a").removeClass("active");
  $(this).addClass("active");
  $("#load_data").html("");

  var limit = 7;
  var start = 0;
  var action = "inactive";

  function lazzy_loader(limit) {
    var output = "";
    for (var count = 0; count < limit; count++) {
      output +=
        '<div class="col-md-6"> <div class="media"> <div class="media-img">';
      output += " &nbsp;";
      output +=
        '</div><div class="media-body"><h4 class="media-heading">&nbsp;</h4>';
      output += '  <h5 class="media-category">&nbsp;</h5>    ';
      output +=
        '<div class="mu-ingredients" style="margin-top: 16px">&nbsp;</div>';
      output +=
        '<span class="mu-menu-energy">&nbsp;</span>&nbsp;<span class="mu-menu-price">&nbsp;</span>  <span class="media-price">&nbsp;</span>';
      output += "&nbsp;</div></div></div>";
    }
    $("#load_data_message").html(output);
  }

  lazzy_loader(limit);

  function load_data(limit, start) {
    var cat_id = $(".menu-tags .active").attr("id");
    var table = "sm_salads";
    var select_fields = [
      "salad_name",
      "salad_name_arabic",
      "salad_desc",
      "salad_desc_arabic",
      "salad_img",
      "cat_id",
      "salad_id",
      "salad_price",
      "salad_calories"
    ];
    var where = { cat_id: cat_id, salad_status: 1 };
    var order_by = "salad_id DESC";

    $.ajax({
      url: base_url + "menu-load-data",
      method: "POST",
      data: {
        cat_id: cat_id,
        limit: limit,
        start: start,
        table: table,
        select_fields: select_fields,
        where: where,
        order_by: order_by
      },
      cache: false,
      success: function(data) {
        if (data == "") {
          $("#load_data_message").html("<h3>No More Salads</h3>");
          action = "active";
        } else {
          $("#load_data").append(data);
          $("#load_data_message").html("");
          action = "inactive";
        }
      }
    });
  }

  if (action == "inactive") {
    action = "active";
    load_data(limit, start);
  }

  $(window).scroll(function() {
    if (
      $(window).scrollTop() + $(window).height() > $("#load_data").height() &&
      action == "inactive"
    ) {
      lazzy_loader(limit);
      action = "active";
      start = start + limit;
      setTimeout(function() {
        load_data(limit, start);
      }, 1000);
    }
  });
});

//Delete Cart Values

function DeleteCartValues(id) {
  var options = {
    theme: "sk-fading-circle",
    backgroundColor: "rgb(0, 0, 0, 0.3)",
    textColor: "white",
    where_id: "body"
  };
  HoldOn.open(options);
  var r = confirm("Do you want to remove this item!");
  if (r == true) {
    //delete inot cart
    $.ajax({
      type: "POST",
      dataType: "json",
      data: { cart_id: id },
      url: base_url + "ajax/remove-cart-values",
      success: function(response) {
        if (response["status"] === true) {
          HoldOn.close();
          location.reload();
        } else {
          HoldOn.close();
          //alert("Something went wrong");
          location.reload();
        }
      }
    });
  }
  HoldOn.close();
}

$(".add").click(function() {
  if (
    $(this)
      .prev()
      .val() < 9
  ) {
    $(this)
      .prev()
      .val(
        +$(this)
          .prev()
          .val() + 1
      );
    update_quantity(
      $(this)
        .prev()
        .attr("cart_id"),
      $(this)
        .prev()
        .val()
    );
  }
});
$(".sub").click(function() {
  if (
    $(this)
      .next()
      .val() > 1
  ) {
    if (
      $(this)
        .next()
        .val() > 1
    ) {
      $(this)
        .next()
        .val(
          +$(this)
            .next()
            .val() - 1
        );
      update_quantity(
        $(this)
          .next()
          .attr("cart_id"),
        $(this)
          .next()
          .val()
      );
    }
  }
});

function update_quantity(cart_id, quantity) {
  var options = {
    theme: "sk-fading-circle",
    backgroundColor: "rgb(0, 0, 0, 0.3)",
    textColor: "white",
    where_id: "body"
  };
  HoldOn.open(options);
  // var r = confirm("Do you want update quantity!");

  //delete inot cart
  $.ajax({
    type: "POST",
    dataType: "json",
    data: { cart_id: cart_id, quantity: quantity },
    url: base_url + "ajax/update-quantity",
    success: function(response) {
      if (response["status"] === true) {
        HoldOn.close();
        location.reload();
      } else {
        HoldOn.close();
        //alert("Something went wrong");
        location.reload();
      }
    }
  });

  HoldOn.close();
}

//Delivery
$(document).ready(function() {
  var options = {
    theme: "sk-fading-circle",
    backgroundColor: "rgb(0, 0, 0, 0.3)",
    textColor: "white",
    where_id: "body"
  };
  HoldOn.open(options);

  var sub_total = 0;
  $(".salad_price").each(function(i) {
    var price = parseFloat($(this).html());
    sub_total = sub_total + price;
  });
  $("#sub_total").html(sub_total);
  var tax_percentage = parseFloat($("#tax_percentage").html());
  var tax_charges = (tax_percentage * sub_total) / 100;
  $("#tax_charges").html(tax_charges.toFixed(2));
  var delivery_charges = parseFloat($("#delivery_charges").html());
  var total_price = sub_total + tax_charges + delivery_charges;

  $("#total_price").html(total_price.toFixed(2));

  HoldOn.close();

  $("#pickup_detail_form").validate({
    rules: {
      delivery: {
        required: true
      },
      date_time: {
        required: true
      },
      name: {
        required: true,
        letterswithspace: true,
        minlength: 2,
        maxlength: 40
      },
      mobile: {
        required: true,
        number: true,
        minlength: 9,
        maxlength: 9
      }
    },
    submitHandler: function(form) {
      var options = {
        theme: "sk-fading-circle",
        backgroundColor: "rgb(0, 0, 0, 0.3)",
        textColor: "white",
        where_id: "body"
      };
      HoldOn.open(options);
      var delivery_type = $("input[name='delivery']:checked").val();
      var checkout = [];
      if (delivery_type === "delivery") {
        checkout.push({
          delivery_type: delivery_type
        });
      } else if (delivery_type === "pickup") {
        var date_time = $("#datetimepicker").val();
        var pickUpDate = moment(date_time).format("YYYY-MM-DD");
        var pickUpTime = moment(date_time).format("H:m");

        checkout.push({
          delivery_type: delivery_type,
          pickup_date: pickUpDate,
          pickup_time: pickUpTime,
          name: $("#name").val(),
          mobile: $("#mobile").val()
        });
      }
      $.ajax({
        type: "POST",
        url: base_url + "checkout-order",
        data: { checkout: checkout },
        dataType: "json",
        success: function(response) {
          if (response["status"] === true) {
            window.location = base_url + "confirm-order/" + delivery_type;
          } else {
          }
          HoldOn.close();
        }
      });
    }
  });
  jQuery.validator.addMethod(
    "password_check",
    function(value, element) {
      // allow any non-whitespace characters as the host part
      return (
        this.optional(element) ||
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/.test(
          value
        )
      );
    },
    "Password must have one lowerCase, one upperCase,one special character, one digit and min 8 char."
  );
  jQuery.validator.addMethod(
    "lettersonly",
    function(value, element) {
      return this.optional(element) || /^[a-z]+$/i.test(value);
    },
    "Please enter only alphabets"
  );
  jQuery.validator.addMethod(
    "email_regex",
    function(value, element) {
      return (
        this.optional(element) ||
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
          value
        )
      );
    },
    "Please enter a valid email address"
  );
  jQuery.validator.addMethod(
    "letterswithspace",
    function(value, element) {
      return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
    },
    "Please enter only alphabets"
  );
});
function DeliveryType(delivery_type) {
  UpdateCartPrice(delivery_type);
  if (delivery_type === "delivery") {
    $("#pickup_details").hide();
  } else if (delivery_type === "pickup") {
    $("#pickup_details").show();
  }
}
