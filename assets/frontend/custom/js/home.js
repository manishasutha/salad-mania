/**
 * Created by coretechies on 28/5/19.
 */

$(document).ready(function() {
  var options = {
    theme: "sk-fading-circle",
    backgroundColor: "rgb(0, 0, 0, 0.3)",
    textColor: "white",
    where_id: "body"
  };
  HoldOn.open(options);

  var cat_id = $(".menu-tags a").attr("id");

  var table = "sm_categories";
  var join_with_table = "sm_salads";
  var join_condition = "sm_salads.cat_id=sm_categories.cat_id";
  var select_fields = [
    "sm_salads.salad_desc",
    "sm_salads.salad_desc_arabic",
    "sm_salads.salad_name",
    "sm_salads.salad_name_arabic",
    "sm_salads.salad_img",
    "sm_salads.cat_id",
    "sm_salads.salad_id",
    "sm_salads.salad_price",
    "sm_salads.salad_calories"
  ];
  var where = { cat_id: cat_id, salad_status: 1 };
  var order_by = "salad_id DESC";
  var limit = 4;
  $.ajax({
    type: "POST",
    dataType: "json",
    data: {
      table: table,
      join_with_table: join_with_table,
      join_condition: join_condition,
      select_fields: select_fields,
      where: where,
      order_by: order_by,
      limit: limit,
      cat_id: cat_id
    },
    url: base_url + "ajax/get-salad-detail-for-home-page/join",
    success: function(response) {
      if (response["status"] === true) {
        $("#salads")
          .html("")
          .append(response["data"]);
      }

      if (response["status"] === false) {
        $("#load_data_message").html("<h3>No More Salads</h3>");
      }
      HoldOn.close();

      $("#register_form").trigger("reset");
      HoldOn.close();
    }
  });
});

$(".menu-tags a").on("click", function(e) {
  var options = {
    theme: "sk-fading-circle",
    backgroundColor: "rgb(0, 0, 0, 0.3)",
    textColor: "white",
    where_id: "body"
  };
  HoldOn.open(options);

  e.preventDefault();
  $(".menu-tags a").removeClass("active");
  $(this).addClass("active");

  var cat_id = $(this).attr("id");

  var table = "sm_categories";
  var join_with_table = "sm_salads";
  var join_condition = "sm_salads.cat_id=sm_categories.cat_id";
  var select_fields = [
    "sm_salads.salad_desc",
    "sm_salads.salad_desc_arabic",
    "sm_salads.salad_name",
    "sm_salads.salad_name_arabic",
    "sm_salads.salad_img",
    "sm_salads.cat_id",
    "sm_salads.salad_id",
    "sm_salads.salad_price",
    "sm_salads.salad_calories"
  ];
  var where = { cat_id: cat_id, salad_status: 1 };
  var order_by = "salad_id DESC";
  var limit = 4;
  $.ajax({
    type: "POST",
    dataType: "json",
    data: {
      table: table,
      join_with_table: join_with_table,
      join_condition: join_condition,
      select_fields: select_fields,
      where: where,
      order_by: order_by,
      limit: limit,
      cat_id: cat_id
    },
    url: base_url + "ajax/get-salad-detail-for-home-page/join",
    success: function(response) {
      if (response["status"] === true) {
        $("#salads")
          .html("")
          .append(response["data"]);
      }
      if (response["status"] === false) {
        $("#salads").html("");
        $("#load_data_message").html("<h3>No More Salads</h3>");
      }

      $("#register_form").trigger("reset");
      HoldOn.close();
    }
  });
});
