/**
 * Created by coretechies on 24/7/19.
 */
//Delivery
$(document).ready(function() {
  var options = {
    theme: "sk-fading-circle",
    backgroundColor: "rgb(0, 0, 0, 0.3)",
    textColor: "white",
    where_id: "body"
  };
  HoldOn.open(options);

  var sub_total = 0;
  $(".salad_price").each(function(i) {
    var price = parseFloat($(this).html());
    sub_total = sub_total + price;
  });
  $("#sub_total").html(sub_total.toFixed(2));
  var tax_percentage = parseFloat($("#tax_percentage").html());
  var tax_charges = (tax_percentage * sub_total) / 100;
  $("#tax_charges").html(tax_charges.toFixed(2));
  
  var delivery_charges = 0;
  var delivery_type = $("#delivery_type").attr("delivery_type");
  if (delivery_type === "delivery") {
    delivery_charges = parseFloat($("#delivery_charges").html());
  }

  var total_price = sub_total + tax_charges + delivery_charges;
  $("#total_price").html(total_price.toFixed(2));

  //Store pricing data in DB with user, so we can call on order instertion
  sub_total = sub_total.toFixed(2);
  tax_charges = tax_charges.toFixed(2);
  delivery_charges = delivery_charges.toFixed(2);
  total_price = total_price.toFixed(2);
  
  $.post(base_url + "insert-order-pricing", { sub_total:sub_total, tax_charges: tax_charges, delivery_charges:delivery_charges, total_price:total_price }, function(data) {
    if(data == 'true'){
      //alert("done");
    }
  });


  /*$.ajax({
    type: "POST",
    url: base_url + "insert-order-pricing",
    data: {
      sub_total: sub_total,
      tax_charges: tax_charges,
      delivery_charges: delivery_charges,
      total_price: total_price
    },
    dataType: "json",
    success: function(response) {
      if (response["status"] === true) {
        alert("true");
      } else {
        alert("false");
      }
    }
  });*/


  HoldOn.close();
});

$("#confirm_order_form").validate({
  rules: {
    payment_type: {
      required: true
    }
  },
  submitHandler: function(form) {
    if (login_status) {
      var order_type = $("#payment_type").val();
      var sub_total = $("#sub_total").html();
      var tax_charges = $("#tax_charges").html();
      var delivery_type = $("#delivery_type").attr("delivery_type");
      var delivery_charges = 0;
      if (delivery_type === "delivery") {
        delivery_charges = parseFloat($("#delivery_charges").html());
      }
      var total_price = $("#total_price").html();
      if (delivery_type === "delivery") {
        var check_address = $("#address_div").attr("check_address");

        if (check_address === "false") {
          $("#address_error_message")
            .html("")
            .append(
              '<h6 class="text-center" ><span style="color: red;" id="error_msg">Please select address first!!</span></h6>'
            );
          $("html, body").animate({ scrollTop: 0 }, "slow");
          return false;
        }
      }
      var options = {
        theme: "sk-fading-circle",
        backgroundColor: "rgb(0, 0, 0, 0.3)",
        textColor: "white",
        where_id: "body"
      };
      HoldOn.open(options);

      $.ajax({
        type: "POST",
        url: base_url + "create-order",
        data: {
          order_type: order_type,
          sub_total: sub_total,
          tax_charges: tax_charges,
          delivery_charges: delivery_charges,
          total_price: total_price
        },
        dataType: "json",
        success: function(response) {
          if (response["status"] === true) {
            if (response["order_type"] === "online") {
              window.location.href = base_url + "payment";
            } else { //COD
              window.location.href = base_url + "order-success";
            }
          } else {
            $("#error-div").show();
            $("#error-div")
              .html("")
              .append(response["msg"]);
          }
          HoldOn.close();
        }
      });
    } else {
      $("#loginModal").modal("show");
    }
  }
});

$("select[name='payment_type']").change(function() {
  var payment_type = $(this).val();

  $.ajax({
    type: "POST",
    url: base_url + "payment-type",
    data: {
      payment_type: payment_type
    },
    dataType: "json",
    success: function(response) {
      if (response["status"] == true) {
      }
    }
  });
  var delivery_type = $("#delivery_type").attr("delivery_type");
  if (delivery_type === "delivery") {
    var check_address = $("#address_div").attr("check_address");

    if (check_address === "false") {
      $("#address_error_message")
        .html("")
        .append(
          '<h6 class="text-center" ><span style="color: red;" id="error_msg">Please select address first!!</span></h6>'
        );
      $("html, body").animate({ scrollTop: 0 }, "slow");
      return false;
    }
  }
});

$("#address_update").validate({
  rules: {
    location: {
      required: true
    },
    city_latitude_first: {
      required: true
    },
    city_longitude_first: {
      required: true
    }
  },
  submitHandler: function(form) {
    var options = {
      theme: "sk-fading-circle",
      backgroundColor: "rgb(0, 0, 0, 0.3)",
      textColor: "white",
      where_id: "body"
    };
    HoldOn.open(options);

    var address1 = $("#location").val();
    var latitude = $("#city_latitude_first").val();
    var logitude = $("#city_longitude_first").val();
    var city = $("#city").val();

    $.ajax({
      type: "POST",
      url: base_url + "address-update",
      data: {
        address1: address1,
        latitude: latitude,
        logitude: logitude,
        city: city
      },
      dataType: "json",
      success: function(response) {
        if (response["status"] == true) {
          $("#address_update").trigger("reset");
          $(".address-form").hide();
          $(".address-detail").show();
          var deliveryType = $("#deliveryType").val();
          var payment_type = $("#payment_type").val();
          window.location =
            base_url + "confirm-order/" + deliveryType + "/#payment_type";
          $("select[name='payment_type']").val(payment_type);
        } else {
          //alert("something went wrong");
        }
      }
    });
    HoldOn.close();
  }
});

function confirm_address() {
  if (login_status) {
    $.ajax({
      type: "POST",
      url: base_url + "get-address",
      dataType: "json",
      success: function(response) {
        if (response["status"] == true) {
          $("#all_addresses")
            .html("")
            .append(response["data"]);

          if (response["address"].length == 0) {
            $(".address-form").show();
            $(".address-detail").hide();
          } else {
            $(".address-detail").show();
            $(".address-form").hide();
          }

          $("#loginModal2").show();
          $(".hide-on-change-add").hide();
        } else {
          //alert("something went wrong");
        }
      }
    });
  } else {
    $("#loginModal").modal("show");
  }
}

$(".hide-loginmodal").click(function() {
  $("#loginModal2").hide();
    $(".hide-on-change-add").show();
});

$(".show-address-form").click(function() {
  $(".address-form").show();
  $(".address-detail").hide();
});

$(".hide-address-form").click(function() {
  $(".address-form").hide();
  $(".address-detail").show();
});

$(".hide-deliver-form").click(function() {
  $(".address-detail").hide();
  $(".hide-on-change-add").show();
});

$("#confirm_address_from").validate({
  rules: {
    address_id: {
      required: true
    }
  },
  messages: {
    address_id: "Please select address"
  },
  submitHandler: function(form) {
    var address_id = $("input[name='address_id']:checked").val();
    $.ajax({
      type: "POST",
      url: base_url + "confirm-address",
      data: { address_id: address_id },
      dataType: "json",
      success: function(response) {
        if (response["status"] == false) {
          //alert("Something went wrong");
        } else {
          $(".address-form").hide();
          $(".address-detail").show();
          var deliveryType = $("#deliveryType").val();
          var payment_type = $("#payment_type").val();
          window.location =
            base_url + "confirm-order/" + deliveryType + "/#payment_type";
          $("select[name='payment_type']").val(payment_type);
        }
      }
    });
  }
});
