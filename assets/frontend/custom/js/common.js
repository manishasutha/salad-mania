/**
 * Created by coretechies on 22/5/19.
 */
//register validation
function addsave() {
  document.getElementById("salad-textbox").removeAttribute("readonly");
  document.getElementById("savebtn").style.display = "block";

}

function addname() {
  document.getElementById("savebtn").style.display = "none";
  document.getElementById("salad-textbox").setAttribute("readonly");
}

function isNumber(evt) {
  evt = evt ? evt : window.event;
  var charCode = evt.which ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  }
  return true;
}



$(document).ready(function() {
  if (verified === true) {
    if (!login_status) {
      $("#error_msg").html("");
      $("#success_msg").html("Your email is successfully verified!");
      $("#success_msg").show();
      $("#loginModal").modal("show");
      $("#success_msg")
        .delay(5000)
        .fadeOut("slow");
      $("#error_msg")
        .delay(5000)
        .fadeOut("slow");
    } else {
      window.location.href = base_url;
    }
  }

  $("#register_form").validate({
    rules: {
      name: {
        required: true,
        letterswithspace: true,
        minlength: 2,
        maxlength: 30
      },
      build_no: {
        required: true,
        minlength: 2,
        maxlength: 50
      },
      street_no: {
        required: true,
        minlength: 2,
        maxlength: 100
      },
      city: {
        required: true
      },
      reg_email: {
        required: true,
        email_regex: true,
        remote: {
          url: base_url + "check-email",
          type: "post"
        }
      },
      mobile: {
        required: true,
        number: true,
        minlength: 9,
        maxlength: 9,
        remote: {
          url: base_url + "check-mobile",
          type: "post"
        }
      },
      pswd: {
        required: true,
        password_check: true,
        minlength: 8,
        maxlength: 15
      },
      conf_pswd: {
        required: true,
        equalTo: "#pswd"
      }
    },
    messages: {
      reg_email: {
        remote: "Email is already registered."
      },
      mobile: {
        remote: "Phone Number is already registered.",
        minlength: "Number Must Be Of 9 Digits",
        maxlength: "Number Must Be Of 9 Digits"
      },
      conf_pswd: {
        equalTo: "Password and confirm password do not match"
      }
    },
    submitHandler: function(form) {
      var options = {
        theme: "sk-fading-circle",
        backgroundColor: "rgb(0, 0, 0, 0.3)",
        textColor: "white",
        where_id: "body"
      };
      HoldOn.open(options);
      var name = $("#name").val();
      var build_no = $("#build_no").val();
      var street_no = $("#street_no").val();
      var city = $("#city").val();
      var email = $("#reg_email").val();
      var mobile = $("#mobile").val();
      var pswd = $("#pswd").val();

      $.ajax({
        type: "POST",
        url: base_url + "register",
        data: {
          name: name,
          build_no: build_no,
          street_no: street_no,
          city: city,
          email: email,
          mobile: mobile,
          pswd: pswd
        },
        dataType: "json",
        success: function(response) {
          if (response["status"] === true) {
            $("#error_msg").html("");
            $("#success_msg").show();
            $("#success_msg").html(response["msg"]);
          } else {
            $("#success_msg").html("");
            $("#error_msg").show();
            $("#error_msg").html(response["msg"]);
          }
          HoldOn.close();
          $("#success_msg")
            .delay(5000)
            .fadeOut("slow");
          $("#error_msg")
            .delay(5000)
            .fadeOut("slow");
          $("#register_form").trigger("reset");
        }
      });
    }
  });
  jQuery.validator.addMethod(
    "password_check",
    function(value, element) {
      // allow any non-whitespace characters as the host part
      return (
        this.optional(element) ||
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/.test(
          value
        )
      );
    },
    "Password must have one lowerCase, one upperCase,one special character, one digit and min 8 char."
  );
  jQuery.validator.addMethod(
    "lettersonly",
    function(value, element) {
      return this.optional(element) || /^[a-z]+$/i.test(value);
    },
    "Please enter only alphabets"
  );
  jQuery.validator.addMethod(
    "email_regex",
    function(value, element) {
      return (
        this.optional(element) ||
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
          value
        )
      );
    },
    "Please enter a valid email"
  );
  jQuery.validator.addMethod(
    "letterswithspace",
    function(value, element) {
      return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
    },
    "Please enter only alphabets"
  );
});

//login form validation and submition
$(document).ready(function() {
  $("#login_form").validate({
    rules: {
      log_email: {
        required: true,
        email_regex: true
      },
      log_pswd: {
        required: true
      }
    },
    submitHandler: function(form) {
      var options = {
        theme: "sk-fading-circle",
        backgroundColor: "rgb(0, 0, 0, 0.3)",
        textColor: "white",
        where_id: "body"
      };
      HoldOn.open(options);
      var email = $("#log_email").val();
      var pswd = $("#log_pswd").val();

      $.ajax({
        type: "POST",
        url: base_url + "login",
        data: { email: email, pswd: pswd },
        dataType: "json",
        success: function(response) {
          if (response["status"] === true) {
            $("#error_msg").html("");
            $("#success_msg").show();
            $("#success_msg").html(response["msg"]);
            location.reload();
          } else {
            $("#success_msg").html("");
            $("#error_msg").show();
            $("#error_msg").html(response["msg"]);
          }
          HoldOn.close();
          $("#success_msg")
            .delay(5000)
            .fadeOut("slow");
          $("#error_msg")
            .delay(5000)
            .fadeOut("slow");
          $("#register_form").trigger("reset");
        }
      });
    }
  });
  jQuery.validator.addMethod(
    "password_check",
    function(value, element) {
      // allow any non-whitespace characters as the host part
      return (
        this.optional(element) ||
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/.test(
          value
        )
      );
    },
    "Password must have one lowerCase, one upperCase,one special character, one digit and min 8 char."
  );
  jQuery.validator.addMethod(
    "lettersonly",
    function(value, element) {
      return this.optional(element) || /^[a-z]+$/i.test(value);
    },
    "Please enter only alphabets"
  );
  jQuery.validator.addMethod(
    "email_regex",
    function(value, element) {
      return (
        this.optional(element) ||
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
          value
        )
      );
    },
    "Please enter a valid email address"
  );
  jQuery.validator.addMethod(
    "letterswithspace",
    function(value, element) {
      return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
    },
    "Please enter only alphabets"
  );
});

//update form validation
$(document).ready(function() {
  $("#user_form").validate({
    rules: {
      name: {
        required: true,
        letterswithspace: true,
        minlength: 2,
        maxlength: 30
      },
      build_no: {
        required: true,
        minlength: 2,
        maxlength: 100
      },
      street_no: {
        required: true,
        minlength: 2,
        maxlength: 100
      },
      city: {
        required: true,
        letterswithspace: true,
        minlength: 2,
        maxlength: 30
      },
      phone: {
        required: true,
        number: true,
        minlength: 9,
        maxlength: 9,
      },
    },

    submitHandler: function(form) {
      var options = {
        theme: "sk-fading-circle",
        backgroundColor: "rgb(0, 0, 0, 0.3)",
        textColor: "white",
        where_id: "body"
      };
      HoldOn.open(options);
      var name = $("#name").val();
      var add_id = $("#add_id").val();
      var build_no = $("#build_no").val();
      var street_no = $("#street_no").val();
      var phone = $("#phone").val();
      var city = $("#city").val();

      $.ajax({
        type: "POST",
        url: base_url + "profile-update",
        data: {
          name: name,
          build_no: build_no,
          street_no: street_no,
          add_id: add_id,
          phone: phone
        },
        dataType: "json",
        success: function(response) {
          if (response["status"] === true) {
            // window.location.reload();
            $("#fullname").html(name);
            $(".add").html(build_no + ", " + street_no + ", " + city);
            $(".add_id").html(add_id);
            $(".phone").html(phone);
            $("#error_msg").html("");
            $("#success_msg").show();
            $("#success_msg").html(response["msg"]);
          } else {
            $("#success_msg").html("");
            $("#error_msg").show();
            $("#error_msg").html(response["msg"]);
          }

          HoldOn.close();

          $("#success_msg")
            .delay(5000)
            .fadeOut("slow");
          $("#error_msg")
            .delay(5000)
            .fadeOut("slow");
          $("#register_form").trigger("reset");
        }
      });
    }
  });

  jQuery.validator.addMethod(
    "lettersonly",
    function(value, element) {
      return this.optional(element) || /^[a-z]+$/i.test(value);
    },
    "Please enter only alphabets"
  );
  jQuery.validator.addMethod(
    "email_regex",
    function(value, element) {
      return (
        this.optional(element) ||
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
          value
        )
      );
    },
    "Please enter a valid email address"
  );
  jQuery.validator.addMethod(
    "letterswithspace",
    function(value, element) {
      return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
    },
    "Please enter only alphabets"
  );
});

//forgot form validation and submition
$(document).ready(function() {
  $("#forgot_form").validate({
    rules: {
      forgot_email: {
        required: true,
        email_regex: true
      }
    },
    submitHandler: function(form) {
      var options = {
        theme: "sk-fading-circle",
        backgroundColor: "rgb(0, 0, 0, 0.3)",
        textColor: "white",
        where_id: "body"
      };
      HoldOn.open(options);
      var email = $("#forgot_email").val();

      $.ajax({
        type: "POST",
        url: base_url + "forgot",
        data: { email: email },
        dataType: "json",
        success: function(response) {
          if (response["status"] === true) {
            $("#error_msg").html("");
            $("#success_msg").show();
            $("#success_msg").html(response["msg"]);
          } else {
            $("#success_msg").html("");
            $("#error_msg").show();
            $("#error_msg").html(response["msg"]);
          }
          HoldOn.close();
          $("#success_msg")
            .delay(5000)
            .fadeOut("slow");
          $("#error_msg")
            .delay(5000)
            .fadeOut("slow");
          validator = $("#forgot_email").validate();
          validator.resetForm();
          $("#register_form").trigger("reset");
        }
      });
    }
  });
  jQuery.validator.addMethod(
    "password_check",
    function(value, element) {
      // allow any non-whitespace characters as the host part
      return (
        this.optional(element) ||
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/.test(
          value
        )
      );
    },
    "Password must have one lowerCase, one upperCase,one special character, one digit and min 8 char."
  );
  jQuery.validator.addMethod(
    "lettersonly",
    function(value, element) {
      return this.optional(element) || /^[a-z]+$/i.test(value);
    },
    "Please enter only alphabets"
  );
  jQuery.validator.addMethod(
    "email_regex",
    function(value, element) {
      return (
        this.optional(element) ||
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
          value
        )
      );
    },
    "Please enter a valid email address"
  );
  jQuery.validator.addMethod(
    "letterswithspace",
    function(value, element) {
      return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
    },
    "Please enter only alphabets"
  );
});

//Reset Password form validation and submit
$(document).ready(function() {
  $("#reset_pswd_form").validate({
    rules: {
      pswd: {
        required: true,
        password_check: true,
        minlength: 8,
        maxlength: 15
      },
      conf_pswd: {
        required: true,
        equalTo: "#pswd"
      }
    },
    submitHandler: function(form) {
      var options = {
        theme: "sk-fading-circle",
        backgroundColor: "rgb(0, 0, 0, 0.3)",
        textColor: "white",
        where_id: "body"
      };
      HoldOn.open(options);
      var pswd = $("#pswd").val();
      var encrypted_id = $("#encrypted_id").val();

      $.ajax({
        type: "POST",
        url: base_url + "reset-pswd",
        data: { pswd: pswd, encrypted_id: encrypted_id },
        dataType: "json",
        success: function(response) {
          if (response["status"] === true) {
            $("#error_msg").html("");
            $("#success_msg").show();
            $("#success_msg").html(response["msg"]);
          } else {
            $("#success_msg").html("");
            $("#error_msg").show();
            $("#error_msg").html(response["msg"]);
          }
          HoldOn.close();
          $("#register_form").trigger("reset");
          $("#success_msg")
            .delay(5000)
            .fadeOut("slow");
          $("#error_msg")
            .delay(5000)
            .fadeOut("slow");
          location.reload();
        }
      });
    }
  });
  jQuery.validator.addMethod(
    "password_check",
    function(value, element) {
      // allow any non-whitespace characters as the host part
      return (
        this.optional(element) ||
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/.test(
          value
        )
      );
    },
    "Password must have one lowerCase, one upperCase,one special character, one digit and min 8 char."
  );
  jQuery.validator.addMethod(
    "lettersonly",
    function(value, element) {
      return this.optional(element) || /^[a-z]+$/i.test(value);
    },
    "Please enter only alphabets"
  );
  jQuery.validator.addMethod(
    "email_regex",
    function(value, element) {
      return (
        this.optional(element) ||
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
          value
        )
      );
    },
    "Please enter a valid email address"
  );
  jQuery.validator.addMethod(
    "letterswithspace",
    function(value, element) {
      return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
    },
    "Please enter only alphabets"
  );
});

//change Password form validation and submit
$(document).ready(function() {
  $("#ch_pswd_form").validate({
    rules: {
      pswd: {
        required: true,
        password_check: true,
        minlength: 8,
        maxlength: 15
      },
      conf_pswd: {
        required: true,
        equalTo: "#pswd"
      }
    },
    submitHandler: function(form) {
      var options = {
        theme: "sk-fading-circle",
        backgroundColor: "rgb(0, 0, 0, 0.3)",
        textColor: "white",
        where_id: "body"
      };
      HoldOn.open(options);
      var pswd = $("#pswd").val();

      $.ajax({
        type: "POST",
        url: base_url + "change-password",
        data: { pswd: pswd },
        dataType: "json",
        success: function(response) {
          if (response["status"] === true) {
            $("#error_msg").html("");
            $("#success_msg").show();
            $("#success_msg").html(response["msg"]);
          } else {
            $("#success_msg").html("");

            $("#error_msg").show();

            $("#error_msg").html(response["msg"]);
          }
          HoldOn.close();
          $("#success_msg")
            .delay(5000)
            .fadeOut("slow");
          $("#error_msg")
            .delay(5000)
            .fadeOut("slow");
          $("#register_form").trigger("reset");
          window.location.href = base_url;
        }
      });
    }
  });
  jQuery.validator.addMethod(
    "password_check",
    function(value, element) {
      // allow any non-whitespace characters as the host part
      return (
        this.optional(element) ||
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/.test(
          value
        )
      );
    },
    "Password must have one lowerCase, one upperCase,one special character, one digit and min 8 char."
  );
  jQuery.validator.addMethod(
    "lettersonly",
    function(value, element) {
      return this.optional(element) || /^[a-z]+$/i.test(value);
    },
    "Please enter only alphabets"
  );
  jQuery.validator.addMethod(
    "email_regex",
    function(value, element) {
      return (
        this.optional(element) ||
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
          value
        )
      );
    },
    "Please enter a valid email address"
  );
  jQuery.validator.addMethod(
    "letterswithspace",
    function(value, element) {
      return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
    },
    "Please enter only alphabets"
  );
});

function add_to_cart(type, cat_id, salad_id, name) {
  var options = {
    theme: "sk-fading-circle",
    backgroundColor: "rgb(0, 0, 0, 0.3)",
    textColor: "white",
    where_id: "body"
  };
  HoldOn.open(options);
  $.ajax({
    type: "POST",
    dataType: "json",
    data: { type: type, cat_id: cat_id, salad_id: salad_id, name: name },
    url: base_url + "get-model-data",
    success: function(response) {
      if (response["status"] === true) {
        $("#model_data")
          .html("")
          .append(response["data"]);
      }
      if (response["status"] === false) {
        //alert("Something went wrong");
      }
      $("#myModal").modal("show");
      HoldOn.close();
    }
  });

  $("#ql").val(1);

  // if (login_status === true) {
  var total_price = 0;
  if (type === "salad") {
    $("#salad_id").val(id);
    $(".checked").prop("checked", false);
    var ign_ids = $("#salad_" + id).val();
    ign_ids = ign_ids.split(",");
    $("#name_button").hide();

    for (i = 0; i < ign_ids.length; i++) {
      $("#ing_" + ign_ids[i]).prop("checked", true);
      var check_id = "#ing_" + ign_ids[i];
      $(check_id + ":checked").each(function() {
        data = $(this)
          .val()
          .split("-");
        var total_price = (total_price + Number(data[1])).toFixed(2);
      });

      $("#total_price").html(total_price);
    }
  } else if (type === "ing") {
    $("#name_button").show();
    $(".checked").prop("checked", false);
    $("#ing_" + Number(id)).prop("checked", true);

    var check_id = "#ing_" + Number(id);
    $(check_id + ":checked").each(function() {
      data = $(this)
        .val()
        .split("-");
      total_price = (total_price + Number(data[1])).toFixed(2);
    });

    $("#total_price").html(total_price);
  }

  $("#salad-textbox").val(name);

  $("#myModal").modal("show");
  // } else {
  //   $("#loginModal").modal("show");
  // }

  HoldOn.close();
}

function UpdatePrice(value) {
  HoldOn.open({
    theme: "sk-fading-circle",
    where_id: "body"
  });

  var tota_price = 0;
  $('input[type="checkbox"]:checked').each(function() {
    //     removed the space ^
    var data = $(this)
      .val()
      .split("-");
    tota_price = tota_price + Number(data[1]);
  });
  var quatity = $("#ql").val();

  var final_price = (quatity * tota_price).toFixed(2);
  $("#total_price").html(final_price);
  HoldOn.close();
}

$("#address_update").validate({
  rules: {
    edit_build_no: {
      required: true,
      minlength: 2,
      maxlength: 300
    },
    edit_street_no: {
      required: true,
      minlength: 2,
      maxlength: 300
    },
    city: {
      required: true
    }
  },
  submitHandler: function(form) {
    var options = {
      theme: "sk-fading-circle",
      backgroundColor: "rgb(0, 0, 0, 0.3)",
      textColor: "white",
      where_id: "body"
    };
    HoldOn.open(options);

    var build_no = $("#edit_build_no").val();
    var street_no = $("#edit_street_no").val();
    var city = $("#city").val();
    var latitude = $("#city_latitude_first").val();
    var longitude = $("#city_longitude_first").val();

    $.ajax({
      type: "POST",
      url: base_url + "address-update",
      data: {
        build_no: build_no,
        street_no: street_no,
        city: city,
        latitude: latitude,
        longitude: longitude
      },
      dataType: "json",
      success: function(response) {
        $("#address_update").trigger("reset");
        location.reload();
      }
    });
    HoldOn.close();
  }
});

function UpdateCartPrice(delivery_type) {
  var options = {
    theme: "sk-fading-circle",
    backgroundColor: "rgb(0, 0, 0, 0.3)",
    textColor: "white",
    where_id: "body"
  };
  HoldOn.open(options);

  if (delivery_type === "delivery") {
    $("#pickup_details").hide();

    var sub_total = $("#sub_total").html();
    var tax_rate = $("#tax_rate").html();
    var delivery_charge = $("#delivery_charge").html();
    var tax_price = 0;
    $("#delivery_div").show();
    tax_price = (parseFloat(sub_total) * parseFloat(tax_rate)) /(100);
    $("#tax_price").html(parseFloat(tax_price).toFixed(2));
    var final_price =
      parseFloat(sub_total) +
      parseFloat(delivery_charge) +
      parseFloat(tax_price);

    $("#total").html(final_price.toFixed(2));
  } else if (delivery_type === "pickup") {
    $("#pickup_details").hide();

    var sub_total = $("#sub_total").html();
    var tax_rate = $("#tax_rate").html();
    $("#delivery_div").hide();
    var delivery_charge = 0;

    var tax_price = 0;
    tax_price = (parseFloat(sub_total) * parseFloat(tax_rate)) /(100);
    tax_price = parseFloat(tax_price);
    $("#tax_price").html(tax_price.toFixed(2));
    var final_price =
      parseFloat(sub_total) +
      parseFloat(delivery_charge) +
      parseFloat(tax_price);

    $("#total").html(final_price.toFixed(2));
  }

  HoldOn.close();
}

$(document).ready(function() {
  $("#guest_register_form").validate({
    rules: {
      guest_name: {
        required: true,
        letterswithspace: true,
        minlength: 2,
        maxlength: 30
      },
      guest_email: {
        required: true,
        email_regex: true
      },
      guest_mobile: {
        required: true,
        number: true,
        minlength: 9,
        maxlength: 9,
      }
    },
    submitHandler: function(form) {
      var options = {
        theme: "sk-fading-circle",
        backgroundColor: "rgb(0, 0, 0, 0.3)",
        textColor: "white",
        where_id: "body"
      };
      var name = $("#guest_name").val();
      var email = $("#guest_email").val();
      var mobile = $("#guest_mobile").val();

      $.ajax({
        type: "POST",
        url: base_url + "guest-sign-up",
        data: { name: name, email: email, mobile: mobile },
        dataType: "json",
        success: function(response) {
          if (response["status"] === true) {
            $("#error_msg").html("");
            $("#success_msg").html(response["msg"]);
            $("#success_msg").show();
            location.reload();
          } else {
            $("#success_msg").html("");
            $("#error_msg").show();
            $("#error_msg").html(response["msg"]);
          }
          HoldOn.close();
          $("#register_form").trigger("reset");

          $("#success_msg")
            .delay(5000)
            .fadeOut("slow");
          $("#error_msg")
            .delay(5000)
            .fadeOut("slow");
        }
      });
    }
  });
  jQuery.validator.addMethod(
    "password_check",
    function(value, element) {
      // allow any non-whitespace characters as the host part
      return (
        this.optional(element) ||
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/.test(
          value
        )
      );
    },
    "Password must have one lowerCase, one upperCase,one special character, one digit and min 8 char."
  );
  jQuery.validator.addMethod(
    "lettersonly",
    function(value, element) {
      return this.optional(element) || /^[a-z]+$/i.test(value);
    },
    "Please enter only alphabets"
  );
  jQuery.validator.addMethod(
    "email_regex",
    function(value, element) {
      return (
        this.optional(element) ||
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
          value
        )
      );
    },
    "Please enter a valid email address"
  );
  jQuery.validator.addMethod(
    "letterswithspace",
    function(value, element) {
      return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
    },
    "Please enter only alphabets"
  );
});

//Load Order Details in My Order Page
$(document).ready(function() {
  $("#orderid").change(function() {
    var id = $(this).val();
    if (id != "") {
      $("#orderDetailBox").html("");
      $("#pleasewait").show();

      $.get(base_url + "myorderdetail/" + id, { id: id }, function(
        res,
        status
      ) {
        var data = jQuery.parseJSON(res);

        if (status === "success") {
          if (!data.error) {
            $("#orderDetailBox").html(data.htmlContent);
            $("#pleasewait").hide();

            /*$("#order").css("display", "block");
                       $("#order_detail").css("display", "block");
                       $("#order_head").css("display", "block");
                       $("#orderStatus").css("display", "block");
                       var img = base_url + "uploads/salads/" + data.salad_img;
                       $("#salad_img").attr("src", img);
                       $("#name_salad").html(data.salad_name);
                       $("#salad_price").html( data.salad_price+"JD " );
                       $("#salad_calories").html(
                       data.salad_calories + " Cal | "  + data.salad_price+ " JD "
                       );
                       $("#quantity").html("Qty: " + "<b>" + data.quantity + "</br>");
                       $("#cat_name").html(data.cat_name);
                       $("#ing_calories").html(data.ing_calories + " Cal");
                       $("#ing_price").html( data.ing_price +"JD " );
                       $("#ingName").html(data.ingName);
                       $("#delivery_charge").html( data.delivery_charge + "JD " );
                       $("#tax_amt").html(data.tax_amt+"JD "  );
                       $("#price_total").html( data.total_price+"JD " );
                       $("#discount_amt").html( data.discount_amt+"JD " );
                       $("#grand_total").html( data.grand_total+"JD " );
                       $("#statuschange").html(data.status);
                       $("#method_pay").html(data.pymentmeth);
                       $("#status").html(
                       "<a class='btn btn-finish btn-primary'><i class='fa fa-shopping-bag'> </i> " +
                       data.status +
                       "</a>"
                       );*/
          } else {
            $("#custom_error").html(data.msg);
            $("#pleasewait").hide();
          }
        }
      });
    }
  });
});

// Delete Cart Values from session

function DeleteCartValuesFromSession(key) {
  HoldOn.open({
    theme: "sk-fading-circle",
    where_id: "body"
  });
  // var r = confirm("Do you want to remove this item!");

  //delete inot cart
  $.ajax({
    type: "POST",
    dataType: "json",
    data: { key: key },
    url: base_url + "SaladController/DeleteCartFromSession",
    success: function(response) {
      if (response["status"] === true) {
        HoldOn.close();
        location.reload();
      }
    }
  });

  HoldOn.close();
}

//clear all text box
$("#login_signup").click(function() {
  $("input").val("");
  $("#mobile_code").val("+962");
  $("#city").val("Amman");
  var validator = $("#login_form").validate();
  validator.resetForm();
  validator = $("#register_form").validate();
  validator.resetForm();
  validator = $("#guest_register_form").validate();
  validator.resetForm();
  validator = $("#forgot_email").validate();
  validator.resetForm();
});

//contact us form

$(document).ready(function() {
  $("#contact_us_form").validate({
    rules: {
      contact_name: {
        required: true,
        letterswithspace: true,
        minlength: 3,
        maxlength: 30
      },
      contact_email: {
        required: true,
        email_regex: true
      },
      contact_subject: {
        required: true,
        letterswithspace: true,
        minlength: 2,
        maxlength: 200
      },
      contact_message: {
        required: true,
        letterswithspace: true,
        minlength: 2,
        maxlength: 500
      }
    },
    submitHandler: function(form) {
      var options = {
        theme: "sk-fading-circle",
        backgroundColor: "rgb(0, 0, 0, 0.3)",
        textColor: "white",
        where_id: "body"
      };
      HoldOn.open(options);

      var name = $("#contact_name").val();
      var email = $("#contact_email").val();
      var subject = $("#contact_subject").val();
      var message = $("#contact_message").val();

      $.ajax({
        type: "POST",
        url: base_url + "contact-us-submit",
        data: {
          name: name,
          email: email,
          subject: subject,
          message: message,
          type: "contact"
        },
        dataType: "json",
        success: function(response) {
          if (response["status"] === true) {
            // $("span#success").text(response["msg"]);
            $("span#success").show();
          } else if (response["status"] === false){
            //$("span#error").text(response["msg"]);
            $("span#error").show();
          }
          HoldOn.close();
          location.reload();
        }
      });
    }
  });
  jQuery.validator.addMethod(
    "password_check",
    function(value, element) {
      // allow any non-whitespace characters as the host part
      return (
        this.optional(element) ||
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/.test(
          value
        )
      );
    },
    "Password must have one lowerCase, one upperCase,one special character, one digit and min 8 char."
  );
  jQuery.validator.addMethod(
    "lettersonly",
    function(value, element) {
      return this.optional(element) || /^[a-z]+$/i.test(value);
    },
    "Please enter only alphabets"
  );
  jQuery.validator.addMethod(
    "email_regex",
    function(value, element) {
      return (
        this.optional(element) ||
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
          value
        )
      );
    },
    "Please enter a valid email address"
  );
  jQuery.validator.addMethod(
    "letterswithspace",
    function(value, element) {
      return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
    },
    "Please enter only alphabets"
  );
});

$(document).ready(function() {
  $("#dietician_form").validate({
    rules: {
      dietician_name: {
        required: true,
        letterswithspace: true,
        minlength: 3,
        maxlength: 30
      },
      dietician_email: {
        required: true,
        email_regex: true
      },
      dietician_subject: {
        required: true,
        letterswithspace: true,
        minlength: 2,
        maxlength: 200
      },
      dietician_message: {
        required: true,
        letterswithspace: true,
        minlength: 2,
        maxlength: 500
      }
    },
    submitHandler: function(form) {
      var options = {
        theme: "sk-fading-circle",
        backgroundColor: "rgb(0, 0, 0, 0.3)",
        textColor: "white",
        where_id: "body"
      };
      HoldOn.open(options);

      var name = $("#dietician_name").val();
      var email = $("#dietician_email").val();
      var subject = $("#dietician_subject").val();
      var message = $("#dietician_message").val();

      $.ajax({
        type: "POST",
        url: base_url + "contact-us-submit",
        data: {
          name: name,
          email: email,
          subject: subject,
          message: message,
          type: "dietician"
        },
        dataType: "json",
        success: function(response) {
          if (response["status"] === true) {
            // $("span#success").text(response["msg"]);
            $("span#success").show();
          } else if (response["status"] === false) {
            // $("span#error").text(response["msg"]);
            $("span#error").show();
          }
          HoldOn.close();
          location.reload();
        }
      });
    }
  });
  jQuery.validator.addMethod(
    "password_check",
    function(value, element) {
      // allow any non-whitespace characters as the host part
      return (
        this.optional(element) ||
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/.test(
          value
        )
      );
    },
    "Password must have one lowerCase, one upperCase,one special character, one digit and min 8 char."
  );
  jQuery.validator.addMethod(
    "lettersonly",
    function(value, element) {
      return this.optional(element) || /^[a-z]+$/i.test(value);
    },
    "Please enter only alphabets"
  );
  jQuery.validator.addMethod(
    "email_regex",
    function(value, element) {
      return (
        this.optional(element) ||
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
          value
        )
      );
    },
    "Please enter a valid email address"
  );
  jQuery.validator.addMethod(
    "letterswithspace",
    function(value, element) {
      return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
    },
    "Please enter only alphabets"
  );
});

function AddressModel() {
  if (login_status) {
    $("#addressModal").modal("show");
  } else {
    $("#loginModal").modal("show");
  }
}

//============>calculation price and ingrediant validation<=====================

//increment validation
function Manuplation_model(ing_id, subcat_id) {
  var max_limit = parseInt($("#max_limit_" + subcat_id).text());

  var cat_id = "#cat_" + subcat_id;

  var count = 0;

  var quantity = 0;

  $(cat_id + " input:checked").each(function() {
    var check_box_value = $(this).attr("value");
    quantity = parseInt($("#qty-" + check_box_value).val());
    count = count + quantity;
  });

  if (count > max_limit) {
    $("#ing_id-" + ing_id + "-" + subcat_id).prop("checked", false);
  }
  var count = 0;
  $(cat_id + " input:checked").each(function() {
    var check_box_value = $(this).attr("value");
    quantity = parseInt($("#qty-" + check_box_value).val());
    count = count + quantity;
  });

  $("#count_" + subcat_id).html(count);
  var fixed_price = parseFloat($("#total_price").attr("fixed_price"));
  var fixed_calories = parseInt($("#total_calories").attr("fixed_calories"));
  calculate_price(fixed_price, fixed_calories);
}
//ingrediant increment
$("#myModal").on("click", ".ing_add", function() {
  var qty_id = $(this)
    .prev()
    .attr("id");

  var hideen_qty_id = qty_id.split("_");

  if (
    $(this)
      .prev()
      .val() < 9
  ) {
    $(this)
      .prev()
      .val(
        +$(this)
          .prev()
          .val() + 1
      );
    $("#qty-" + hideen_qty_id[1]).val(
      $(this)
        .prev()
        .val()
    );
  }
  var ing_id_cat_id = hideen_qty_id[1].split("-");
  Manuplation_model(ing_id_cat_id[0], ing_id_cat_id[1]);
});
//increment decrement
$("#myModal").on("click", ".ing_sub", function() {
  var qty_id = $(this)
    .next()
    .attr("id");

  var hideen_qty_id = qty_id.split("_");
  if (
    $(this)
      .next()
      .val() > 1
  ) {
    if (
      $(this)
        .next()
        .val() > 1
    )
      $(this)
        .next()
        .val(
          +$(this)
            .next()
            .val() - 1
        );
    $("#qty-" + hideen_qty_id[1]).val(
      $(this)
        .next()
        .val()
    );
  }

  var ing_id_cat_id = hideen_qty_id[1].split("-");

  Manuplation_model(ing_id_cat_id[0], ing_id_cat_id[1]);
});
//calculation price
function calculate_price(fixed_price, fixed_calories) {
  var quantity = 0;
  var total_price = 0;
  var total_calories = 0;
  $("#model_data input:checkbox:checked").each(function() {
    var check_box_value = $(this).attr("value");
    quantity = parseInt($("#qty-" + check_box_value).val());
    var price = $("#price-" + check_box_value).html();
    var calories = $("#calories-" + check_box_value).html();

    total_price = total_price + price * quantity;
    total_calories = total_calories + calories * quantity;
  });

  total_price = total_price + fixed_price;
  total_calories = total_calories + fixed_calories;

  var total_quantity = parseInt($("#ql").val());
  var totalPrice=(total_price * total_quantity).toFixed(2);
  $("#total_price").html(totalPrice);
  $("#total_calories").html(total_calories * total_quantity);
}
//salad increment
$("#myModal").on("click", ".add", function() {

  if (
    $(this)
      .prev()
      .val() < 9
  ) {
    $(this).prev().val(+$(this) .prev().val() + 1);

    var fixed_price = parseFloat($("#total_price").attr("fixed_price"));
    var fixed_calories = parseInt($("#total_calories").attr("fixed_calories"));
    calculate_price(fixed_price, fixed_calories);
  }
});
//salad decrement
$("#myModal").on("click", ".sub", function() {
  if (
    $(this)
      .next()
      .val() > 1
  ) {
    if (
      $(this)
        .next()
        .val() > 1
    )
      $(this)
        .next()
        .val(
          +$(this)
            .next()
            .val() - 1
        );
    var fixed_price = parseFloat($("#total_price").attr("fixed_price"));
    var fixed_calories = parseInt($("#total_calories").attr("fixed_calories"));
    calculate_price(fixed_price, fixed_calories);
  }
});

function AddToBag() {
  // var options = {
  //     theme:"sk-fading-circle",
  //     backgroundColor:"rgb(0, 0, 0, 0.3)",
  //     textColor:"white",
  //     where_id:'body'
  // };
  // HoldOn.open(options);

  var ingrediants = [];

  $("#model_data input:checkbox:checked").each(function() {
    var check_box_value = $(this).attr("value");
    var ing_id_cat_id = check_box_value.split("-");
    var ing_id = ing_id_cat_id[0];
    var cat_id = ing_id_cat_id[1];
    var quantity = parseInt($("#qty-" + check_box_value).val());

    ingrediants.push({
      ing_id: ing_id,
      cat_id: cat_id,
      quantity: quantity
    });
  });

  var qty = $("#ql").val();
  var salad_id = $("#salad_data").attr("salad_id");
  var salad_type = $("#salad_data").attr("salad_type");
  var salad_name = null;
  var price = null;
  if (salad_type == "create_own") {
    salad_name = $("#salad-textbox").val();
    price = $("span#total_price").text();
    if(price == 0) {
      $("span#salad_price_error").text('Please select atleast one ingredient');
      return false;
    }
    if(salad_name ==''){
      $("span#salad_name_error").text('This field is required');
      return false;
    }
  }
  var cat_id = $("#salad_data").attr("cat_id");
  var special_request = $("#special_request").val();

  $.ajax({
    type: "POST",
    url: base_url + "insert-data-into-cart",
    data: {
      salad_type: salad_type,
      salad_name: salad_name,
      cat_id: cat_id,
      salad_id: salad_id,
      qty: qty,
      ingrediants: ingrediants,
      special_request: special_request
    },
    dataType: "json",
    success: function(response) {
      if (response["status"] === true) {
      } else {
        // alert("Something went wrong.");
      }
      HoldOn.close();
      location.reload();
    }
  });

  // HoldOn.close();

  $("#address_update").validate({
    rules: {
      edit_build_no: {
        required: true,
        minlength: 2,
        maxlength: 300
      },
      edit_street_no: {
        required: true,
        minlength: 2,
        maxlength: 300
      },
      city: {
        required: true
      }
    },
    submitHandler: function(form) {
      var options = {
        theme: "sk-fading-circle",
        backgroundColor: "rgb(0, 0, 0, 0.3)",
        textColor: "white",
        where_id: "body"
      };
      HoldOn.open(options);

      var build_no = $("#edit_build_no").val();
      var street_no = $("#edit_street_no").val();
      var city = $("#city").val();
      var latitude = $("#city_latitude_first").val();
      var longitude = $("#city_longitude_first").val();

      $.ajax({
        type: "POST",
        url: base_url + "address-update",
        data: {
          build_no: build_no,
          street_no: street_no,
          city: city,
          latitude: latitude,
          longitude: longitude
        },
        dataType: "json",
        success: function(response) {
          $("#address_update").trigger("reset");
          location.reload();
        }
      });
      HoldOn.close();
    }
  });
}
