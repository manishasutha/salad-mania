/**
 * Created by coretechies on 28/5/19.
 */
$(document).ready(function() {
  var limit = 7;
  var start = 0;
  var action = "inactive";

  function lazzy_loader(limit) {
    var output = "";
    for (var count = 0; count < limit; count++) {
      output +=
        '<div class="col-md-6"> <div class="media"> <div class="media-img">';
      output += " &nbsp;";
      output +=
        '</div><div class="media-body"><h4 class="media-heading">&nbsp;</h4>';
      output += '  <h5 class="media-category">&nbsp;</h5>    ';
      output +=
        '<div class="mu-ingredients" style="margin-top: 16px">&nbsp;</div>';
      output +=
        '<span class="mu-menu-energy">&nbsp;</span>&nbsp;<span class="mu-menu-price">&nbsp;</span>  <span class="media-price">&nbsp;</span>';
      output += "&nbsp;</div></div></div>";
    }
    $("#load_data_message").html(output);
  }

  lazzy_loader(limit);

  function load_data(limit, start) {
    var page = "menu";
    var cat_id = $(".menu-tags .active").attr("id");
    var table = "sm_salads";
    var select_fields = [
      "salad_name",
      "salad_name_arabic",
      "salad_desc",
      "salad_desc_arabic",
      "salad_img",
      "cat_id",
      "salad_id",
      "salad_price",
      "salad_calories"
    ];
    var where = { cat_id: cat_id, salad_status: 1 };
    var order_by = "salad_id DESC";

    $.ajax({
      url: base_url + "menu-load-data",
      method: "POST",
      data: {
        cat_id: cat_id,
        limit: limit,
        start: start,
        table: table,
        select_fields: select_fields,
        where: where,
        order_by: order_by,
        page: page
      },
      cache: false,
      success: function(data) {
        if (data == "") {
          $("#load_data_message").html("<h3>No More Salads</h3>");
          action = "active";
        } else {
          $("#load_data").append(data);
          $("#load_data_message").html("");
          action = "inactive";
        }
      }
    });
  }

  if (action == "inactive") {
    action = "active";
    load_data(limit, start);
  }

  $(window).scroll(function() {
    if (
      $(window).scrollTop() + $(window).height() > $("#load_data").height() &&
      action == "inactive"
    ) {
      lazzy_loader(limit);
      action = "active";
      start = start + limit;
      setTimeout(function() {
        load_data(limit, start);
      }, 1000);
    }
  });
});

$(".menu-tags a").on("click", function(e) {
  e.preventDefault();
  $(".menu-tags a").removeClass("active");
  $(this).addClass("active");
  $("#load_data").html("");

  var limit = 7;
  var start = 0;
  var action = "inactive";

  function lazzy_loader(limit) {
    var output = "";
    for (var count = 0; count < limit; count++) {
      output +=
        '<div class="col-md-6"> <div class="media"> <div class="media-img">';
      output += " &nbsp;";
      output +=
        '</div><div class="media-body"><h4 class="media-heading">&nbsp;</h4>';
      output += '  <h5 class="media-category">&nbsp;</h5>    ';
      output +=
        '<div class="mu-ingredients" style="margin-top: 16px">&nbsp;</div>';
      output +=
        '<span class="mu-menu-energy">&nbsp;</span>&nbsp;<span class="mu-menu-price">&nbsp;</span>  <span class="media-price">&nbsp;</span>';
      output += "&nbsp;</div></div></div>";
    }
    $("#load_data_message").html(output);
  }

  lazzy_loader(limit);

  function load_data(limit, start) {
    var cat_id = $(".menu-tags .active").attr("id");
    var table = "sm_salads";
    var select_fields = [
      "salad_name",
      "salad_name_arabic",
      "salad_desc",
      "salad_desc_arabic",
      "salad_img",
      "cat_id",
      "salad_id",
      "salad_price",
      "salad_calories"
    ];
    var where = { cat_id: cat_id, salad_status: 1 };
    var order_by = "salad_id DESC";

    $.ajax({
      url: base_url + "menu-load-data",
      method: "POST",
      data: {
        cat_id: cat_id,
        limit: limit,
        start: start,
        table: table,
        select_fields: select_fields,
        where: where,
        order_by: order_by
      },
      cache: false,
      success: function(data) {
        if (data == "") {
          $("#load_data_message").html("<h3>No More Salads</h3>");
          action = "active";
        } else {
          $("#load_data").append(data);
          $("#load_data_message").html("");
          action = "inactive";
        }
      }
    });
  }

  if (action == "inactive") {
    action = "active";
    load_data(limit, start);
  }

  $(window).scroll(function() {
    if (
      $(window).scrollTop() + $(window).height() > $("#load_data").height() &&
      action == "inactive"
    ) {
      lazzy_loader(limit);
      action = "active";
      start = start + limit;
      setTimeout(function() {
        load_data(limit, start);
      }, 1000);
    }
  });
});
