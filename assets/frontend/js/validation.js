$(document).ready(function () {
    $(".error").remove();
    let data = true;

    //First_Name validation
    $('.first_name').keyup(function () {
        var name = $('.first_name').val();
        if (name.length == 0) {
            $("#first_name").text("First Name must not be empty").show;
            data = false;
        } 
        else {
            let filter = /^[a-zA-Z\s]+$/;
            if (filter.test(name))
            {
                if (name.length < 2 || name.length > 15) {
                    $("#first_name_error").text("First Name must be between 2 to 15 characters").show();
                    data = false;
                }else{
                    $("#first_name_error").text("").show();
                    data = false;
                }  data = false;
            }
             else{
                if (!filter.test(name)) 
                {
                $("#first_name_error").text("First Name must be in valid format").show;
                data = false;
             } 
             else{
                    $("#first_name_error").text("").show();
                    data = false;
                }

                data = false;
        }
    }
    });
   
   
    $('.name').keyup(function () {
        var name = $('.name').val();
        if (name.length == 0) {
            $("#name_error").text("Name must not be empty").show;
            data = false;
        } 
        else {
            let filter = /^[a-zA-Z\s]+$/;
            if (filter.test(name))
            {
                if (name.length < 2 || name.length > 30) {
                    $("#name_error").text("Name must be between 2 to 30 characters").show();
                    data = false;
                }else{
                    $("#name_error").text("").show();
                    data = false;
                }  data = false;
            }
             else{
                if (!filter.test(name)) 
                {
                $("#name_error").text("Name must be in valid format").show;
                data = false;
             } 
             else{
                    $("#name_error").text("").show();
                    data = false;
                } data = false;
        }
    }
    });

  

    //last_Name validation
    $('.last_name').keyup(function () {
        var name = $('.last_name').val();
        if (name.length == 0) {
            $("#last_name_error").text("Last Name must not be empty").show;
            data = false;
        } else {
            let filter = /^[a-zA-Z\s]+$/;
            if (filter.test(name)) {
                if (name.length < 2 || name.length > 15) {
                    $("#last_name_error").text("Last Name must be between 2 to 15 characters").show();
                    data = false;
                  }else{
                    $("#last_name_error").text("").show();
                    data = false;
                } data = false;
            } else {
                 if (!filter.test(name)) 
                
            {
                $("#last_name_error").text("Last Name must be in valid format").show;
                data = false;
            } 
            else{
                   $("#last_name_error").text("").show();
                   data = false;
               } data = false;
            }
        }
    });
   
    //pin Code Validations
    $('.pin').keyup(function () {
        var pin = $('.pin').val();
        if (pin.length == 0) {
            $("#pin_error").text("Pin Code must not be empty").show;
            data = false;
        } 
        else {
            let filter = /^[1-9][0-9]{5}$/;
            if (!filter.test(pin) || pin.length != 6)
            {
                    $("#pin_error").text("Pin Code must be of 6 digits & accept only numeric values").show();  
                    data = false; }
             else
                {
                    $("#pin_error").text("").show();
                    data = false;
                }            data = false;
        }
    });

    //Email validation
    $('.email').keyup(function () {
        var email = $('.email').val();
        if (email.length == 0) {
            $("#email_error").text("Email must not be empty").show();
            data = false;
        } else {
            var pattern =/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (pattern.test(email)) {
                if (email.length < 5 || email.length > 50) {
                    $("#email_error").text("Email must be 5 to 50 characters").show();
                    data = false;
                }else{
                    $("#email_error").text(" ").show();
                    data = false;
                }
            }
            else {
                if (!pattern.test(email)) {
                $("#email_error").text("Email must be in valid format ").show();
                data = false;
            }else{
                $("#email_error").text(" ").show();
                data = false;
            } data = false;
        }
    }
    });
   
    $('.subject').keyup(function () {
        var name = $('.subject').val();
        if (name.length == 0) {
            $("#subject_error").text("subject must not be empty").show;
            data = false;
        } 
        else {
            let filter = /^[a-zA-Z\s]+$/;
            if (filter.test(name))
            {
                if (name.length < 2 || name.length > 30) {
                    $("#subject_error").text("subject must be between 2 to 15 characters").show();
                    data = false;
                }else{
                    $("#subject_error").text("").show();
                    data = false;
                }  data = false;
            }
             else{
                if (!filter.test(name)) 
                {
                $("#subject_error").text("subject must be in valid format").show;
                data = false;
             } 
             else{
                    $("#subject_error").text("").show();
                    data = false;
                } data = false;
        }
    }
    });
   
  //Phone No validation
  $('.phone').keyup(function () {
    var phoneno = $('.phone').val();
    if (phoneno.length == 0) {
        $("#phone_error").text("PhoneNo must not be empty").show();
        data = false;
    } else {
        var filter = /^[0-9]+$/;

        if (filter.test(phoneno)) {
            if (phoneno.length < 10 || phoneno.length > 10) {
                $("#phone_error").text("PhoneNo must be of 10 digits").show();
                data = false;
            }
            else{
                $("#phone_error").text("").show();
                data = false;
            }
        } else {
            if (!filter.test(phoneno)) {
            $("#phone_error").text("Phone No must be in valid format").show();
            data = false;
        }else{
            $("#phone_error").text(" ").show();
            data = false;
        } data = false;
    }
}
});


 //Password validation
 $('.password').keyup(function () {
    var password = $('.password').val();
    if (password.length == "") {
        $("#password_error").text("Password must not be empty").show();
        data = false;

    } else {
        var pattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;

        if (pattern.test(password)) {
            if (password.length < 8 || password.length > 15) {
                $("#password_error").text("Password must be between 8 to 15").show();
                data = false;
            }else{
                $("#password_error").text("").show();
                data = false;
            }
        } else {
            if (!pattern.test(password)){
            $("#password_error").text("Password must have one lowerCase, one upperCase,one special character and one digit").show();
            data = false;
        } else{
            $("#password_error").text(" ").show();
            data = false;
        }
    }
}
});


 //Password match validation
 $('.password_confirmation').keyup(function () {
    var password2 = $('.password_confirmation').val();
    if (password2.length == "") {
        $("#password_confirmation_error").text("Confirm Password must not be empty").show();
        data = false;
    } else {
        let password = $(".password").val();
        let confirmPassword = $(".password_confirmation").val();
        if (password != confirmPassword) {
            $("#password_confirmation_error").text("Password and Comfirm Password not matched").show();
            data = false;
         }else{
            $("#password_confirmation_error").text("").show();
            data = false;
        } data = false;
    }
});

//licence validation
$('.licence').keyup(function () {
    var licence = $('.licence').val();
    if (licence.length == "") {
        $("#licence_error").text("Password must not be empty").show();
        data = false;

    } 
    else 
    {
        var pattern = /^[0-9a-zA-Z]+$/;
        if (!pattern.test(licence)) {
         $("#licence_error").text("Licence must have only alpha-numeric values").show();
         data = false;
        } else{
            $("#licence_error").text(" ").show();
            data = false;
        }
    }
});


    //Age validation
    $('.age').keyup(function () {
        var age = $('.age').val();
        if (age.length == 0) {
            $("#age_error").text("Age must not be empty").show();
            data = false;
        } else {
            if (age < 18) {
                $("#age_error").text("Age must be greater than 18").show();
            } else {
                if (age > 200) {
                    $("#age_error").text("Age must not be greater than 200").show();
                }else {
                    $("#age_error").text("").show();
                    data = false;
                }
            }

        }
    });
    $('.age').focusout(function () {
        var age = $('.age').val();
    if (!age < 18) {
            $("#age_error").text("").show();
        }
    });


    //Image validation
    $('.file').keyup(function () {
        if ($('.file').val() == "") {
            $("#file_error").text("Image must not be empty").show();
            data = false;
        } else {
            var val = $(".file").val();
            if (!val.match(/(?:gif|jpg|png|bmp|jpeg|svg|pdf|doc|docx)$/)) {
                // inputted file path is not an image of one of the above types
                $("#file_error").text("Image must be gif, jpeg, jpg, png, bmp!").show();
                data = false;
            } else {
                var file_size = $('.file')[0].files[0].size;
                if (file_size > 6000) {
                    $("#file_error").text("File size must not be greater than 6kB").show();
                    data = false;
                }else
                {
                    $("#file_error").text("").show();
                    data = false;
                }
            }

        }
    });
    $('.file').focusout(function () {
         if (val.match(/(?:gif|jpg|png|bmp|jpeg|svg|pdf|doc|docx)$/)) {
            // inputted file path is not an image of one of the above types
            $("#file_error").text("").show();
            data = false;
        } 
    });


   

    //Date of birth validation
    $('.dob').keyup(function () {
        var dob = $('.dob').val();
        if (dob.length == "") {
            $("#dob_error").text("Dob must not be empty").show();
            } 
            else 
            {
            re = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
            if (regs = dob.match(re)) {
                // day value between 1 and 31
                if (regs[1] < 1 || regs[1] > 31) {
                    $("#dob_error").text("Day must be between 1 and 31").show();

                } else{
                    $("#dob_error").text(" ").show();
                 }
                // month value between 1 and 12
                if (regs[2] < 1 || regs[2] > 12) {
                    $("#dob_error").text("Month must be between 1 and 12 ").show();
                } else {
                    $("#dob_error").text("").show();
                }
                // year value between 1902 and 2019
                if (regs[3] < 1990 || regs[3] > (new Date()).getFullYear()) {
                    $("#dob_error").text("Date must be between 1990 & present year").show();
                }else{
                    $("#dob_error").text("").show();
                }
            } else {
                if (regs != dob.match(re))
            {
                $("#dob_error").text("Date format must be d/m/yy or d-m-yy").show();
                dob.focus();
            } else
            {
                $("#dob_error").text("").show();
                dob.focus();
        }
    }
}
    });
 
    //old password validation
    $('.past_password').keyup(function () {
    if ($('.past_password').val() !== "") {
            $("#past_password_error").text(" ").show();
            data = false;
        }
    });

    //Address validation
    $('.address').keyup(function () {
        var address = $('.address').val();
        if (address.length == "") {
            $("#address_error").text("Address must not be empty").show();
        } else {
                if (address.length < 15 || address.length > 100) {
                    $("#address_error").text("Address must be between 7 to 100 characters").show();
                               data = false;
            }else{
                $("#address_error").text("").show();
                           data = false;
        }
        }

    });
  

    //TextArea  validation
    $('.comment').keyup(function () {
        var comment = $('.comment').val();
        if (comment.length == " ") {
            $("#comment_error").text("Description must not be empty").show();
            data = false;
        } else {
            if (comment.length < 10 || comment.length > 300) {
                $("#comment_error").text("Description must be between 10 to 300").show();
                data = false;
            }else {
                $("#comment_error").text("").show();
                data = false;
            }
        }
    });

    //question title
    $('.title').keyup(function () {
        var title = $('.title').val();
        if (title.length == " ") {
            $("#title_error").text("Title must not be empty").show();
            data = false;
        } 
        else {
            let filter = /^[a-zA-Z\s]+$/;
            if (filter.test(title))
            {
                if (title.length < 10 || title.length > 120) {
                    $("#title_error").text("Title must be between 10 to 120").show();
                    data = false;
                }else{
                    $("#title_error").text("").show();
                    data = false;
                } 
            }
             else{
                if (!filter.test(name)) 
                {
                $("#title_error").text("Title must be in valid format").show;
             } 
             else{
                    $("#title_error").text("").show();
                    data = false;
                }
        }
    }
    });
return data;
  
});

//Change Password to text validation
function myFunction() {
    var x = document.getElementById('password');
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

//Check password strength validation
function passwordStrength() {
    var x = document.getElementById('password');
    var y = x.value;
    var specialCharacter = "!@#$%^&*()";
    var passwordScore = 0;
    for (var i = 0; i < y.length; i++) {
        if (specialCharacter.indexOf(y.charAt(i)) > -1) {
            passwordScore += 20;
        }
    }
    if (/[a-z]/.test(y)) {
        passwordScore += 20;
    }
    if (/[A-Z]/.test(y)) {
        passwordScore += 20;
    }
    if (/[\d]/.test(y)) {
        passwordScore += 20;
    }
    if (y.length >= 8) {
        passwordScore += 20;
    }
    var strength = "";
    var background = "";
    if (passwordScore >= 100) {
        strength = "Strong";
        $(".password").css("backgroundColor", " green");
    } else if (passwordScore >= 80) {
        strength = "Medium";
        $(".password").css("backgroundColor", "gray");
    } else if (passwordScore >= 60) {
        strength = "Week";
        $(".password").css("backgroundColor", " Marron");
    } else {
        strength = "Worst";
        $(".password").css("backgroundColor", "red");
    }
    if (passwordScore >= 100) {
        strength = "Strong";
        $(".label1").css("color", " green");
    } else if (passwordScore >= 80) {
        strength = "Medium";
        $(".label1").css("color", "gray");
    } else if (passwordScore >= 60) {
        strength = "Week";
        $(".label1").css("color", " Marron");
    } else {
        strength = "Worst";
        $(".label1").css("color", "red");
    }
    document.getElementById('label1').innerHTML = strength;
    $(".password").css("color", "white");

}
