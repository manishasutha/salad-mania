/** 


  Custom JS
  

  1. FIXED NAVBAR 
  2. TOP SLIDER
  3. ABOUT US (SLICK SLIDER) 
  4. DATEPICKER
  5. SHEF SLIDER (SLICK SLIDER)
  6. TESTIMONIAL SLIDER (SLICK SLIDER)
  7. COUNTER
  8. MIXIT FILTER (FOR GALLERY)
  9. FANCYBOX (FOR PORTFOLIO POPUP VIEW) 
  10. MENU SMOOTH SCROLLING
  11. HOVER DROPDOWN MENU
  12. SCROLL TOP BUTTON
  13. PRELOADER  

  
**/

jQuery(function($){


  /* ----------------------------------------------------------- */
  /*  1. FIXED NAVBAR 
  /* ----------------------------------------------------------- */
    
    
  jQuery(window).bind('scroll', function () {
    if (jQuery(window).scrollTop() > 200) {
//        jQuery('.mu-main-navbar').addClass('navbar-bg');
//        jQuery('.navbar-brand').addClass('navbar-brand-small');        
      } else {
//          jQuery('.mu-main-navbar').removeClass('navbar-bg');          
//          jQuery('.navbar-brand').removeClass('navbar-brand-small');          
      }
  });
  
  /* ----------------------------------------------------------- */
  /*  2. TOP SLIDER (SLICK SLIDER)
  /* ----------------------------------------------------------- */    

    jQuery('.mu-top-slider').slick({
      dots: true,
      infinite: true,
      arrows: false,
      speed: 1000,     
      autoplay: true,
      fade: true,
      cssEase: 'linear'
    });

  /* ----------------------------------------------------------- */
  /*  3. ABOUT US (SLICK SLIDER)
  /* ----------------------------------------------------------- */      

    jQuery('.mu-abtus-slider').slick({
      dots: false,
      infinite: true,
      arrows: false,
      speed: 500,
      autoplay: true,
      fade: true,      
      cssEase: 'linear'
    });

  /* ----------------------------------------------------------- */
  /*  4. DATEPICKER
  /* ----------------------------------------------------------- */      

    jQuery('#datepicker').datepicker();

  /* ----------------------------------------------------------- */
  /*  5. SHEF SLIDER (SLICK SLIDER)
  /* ----------------------------------------------------------- */    

    jQuery('.mu-chef-nav').slick({
      dots: true,
      arrows: false,
      infinite: true,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 2,
      autoplay: true,
      autoplaySpeed: 2500,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });

  /* ----------------------------------------------------------- */
  /*  6. TESTIMONIAL SLIDER (SLICK SLIDER)
  /* ----------------------------------------------------------- */    

    jQuery('.mu-testimonial-slider').slick({
      dots: true,      
      infinite: true,
      arrows: false,
      autoplay: true,
      speed: 500,      
      cssEase: 'linear'
    });       

  /* ----------------------------------------------------------- */
  /*  7. COUNTER
  /* ----------------------------------------------------------- */

    jQuery('.counter').counterUp({
        delay: 10,
        time: 1000
    });

  /* ----------------------------------------------------------- */
  /*  8. MIXIT FILTER (FOR GALLERY) 
  /* ----------------------------------------------------------- */  

    jQuery(function(){
      jQuery('#mixit-container').mixItUp();
    });

  /* ----------------------------------------------------------- */
  /*  9. FANCYBOX (FOR PORTFOLIO POPUP VIEW) 
  /* ----------------------------------------------------------- */ 
      
    jQuery(document).ready(function() {
      jQuery(".fancybox").fancybox();
    }); 
	
  /* ----------------------------------------------------------- */
  /*  10. MENU SMOOTH SCROLLING
  /* ----------------------------------------------------------- */ 

    //MENU SCROLLING WITH ACTIVE ITEM SELECTED

      // Cache selectors
      var lastId,
      topMenu = $(".mu-main-nav"),
      topMenuHeight = topMenu.outerHeight()+13,
      // All list items
      menuItems = topMenu.find('a[href^=\\#]'),
      // Anchors corresponding to menu items
      scrollItems = menuItems.map(function(){
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
      });

      // Bind click handler to menu items
      // so we can get a fancy scroll animation
      menuItems.click(function(e){
        var href = $(this).attr("href"),
            offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+32;
        jQuery('html, body').stop().animate({ 
            scrollTop: offsetTop
        }, 1500);           
         jQuery('.navbar-collapse').removeClass('in');  
        e.preventDefault();
      });

      // Bind to scroll
      jQuery(window).scroll(function(){
         // Get container scroll position
         var fromTop = $(this).scrollTop()+topMenuHeight;
         
         // Get id of current scroll item
         var cur = scrollItems.map(function(){
           if ($(this).offset().top < fromTop)
             return this;
         });
         // Get the id of the current element
         cur = cur[cur.length-1];
         var id = cur && cur.length ? cur[0].id : "";
         
         if (lastId !== id) {
             lastId = id;
             // Set/remove active class
             menuItems
               .parent().removeClass("active")
               .end().filter("[href=\\#"+id+"]").parent().addClass("active");
         }           
      })
  
  /* ----------------------------------------------------------- */
  /*  11. HOVER DROPDOWN MENU
  /* ----------------------------------------------------------- */ 
  
  // for hover dropdown menu
    jQuery('ul.nav li.dropdown').hover(function() {
      jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
    }, function() {
      jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
    });

    
  /* ----------------------------------------------------------- */
  /*  12. SCROLL TOP BUTTON
  /* ----------------------------------------------------------- */

  //Check to see if the window is top if not then display button

    jQuery(window).scroll(function(){
      if (jQuery(this).scrollTop() > 300) {
        jQuery('.scrollToTop').fadeIn();
      } else {
        jQuery('.scrollToTop').fadeOut();
      }
    });
     
    //Click event to scroll to top

    jQuery('.scrollToTop').click(function(){
      jQuery('html, body').animate({scrollTop : 0},800);
      return false;
    });
  
  /* ----------------------------------------------------------- */
  /*  13. PRELOADER
  /* ----------------------------------------------------------- */

   jQuery(window).load(function() { // makes sure the whole site is loaded      
      jQuery('#aa-preloader-area').delay(300).fadeOut('slow'); // will fade out      
    });
 
 
    $('.sm-toggle').on('click',function(e){
        e.preventDefault();
       if($(this).find('i').hasClass('fa-angle-down'))
       {
           $(this).find('i').removeClass('fa-angle-down');
           $(this).find('i').addClass('fa-angle-up');
           $('.selection-mania').animate({bottom:"-200px"});
       }
       else if($(this).find('i').hasClass('fa-angle-up'))
       {
           $(this).find('i').removeClass('fa-angle-up');
           $(this).find('i').addClass('fa-angle-down');
           $('.selection-mania').animate({bottom:"0px"});
       }
    });
    
    
        $('input[type="checkbox"]').change(function(){
            $('.sm-toggle').find('i').removeClass('fa-angle-up');
           $('.sm-toggle').find('i').addClass('fa-angle-down');
            $('.selection-mania').animate({bottom:"0px"});
           
            if($('input[type="checkbox"]:checked').attr('name')==="bases"){
                console.log("checked 1");
                var selected_base_wrapper = $(this).parent();
                var selected_base_img = $(this).parent().find('.media').find('.media-left').find('img').attr('src');
                var selected_base_title = $(this).parent().find('.media').find('.media-body').find('.media-heading').html();
                var selected_base_cal = $(this).parent().find('.media').find('.media-body').find('.mu-menu-energy').html();
                var value = $(this).val();
                    var base_template = " <li id='"+value+"' > "+
                         " <div class='media'>"+
                                    "<div class='media-left'>"+
                                     "     <img class='media-object' src='"+selected_base_img+"' alt='img'>"+
                                    "</div>"+
                                    "<div class='media-body' style='vertical-align: middle'>"+
                                     " <h4 class='media-heading'>"+selected_base_title+"</h4>"+
                                     " <span class='mu-menu-energy'>"+selected_base_cal+"</span>&nbsp;|&nbsp;"+
                                      "<span class='mu-menu-price'>Qty: <input type='number'></span>"+
                                    "</div>"+
                               " </div>"+
                     " </li> ";
                     $('#bases').append(base_template);
            }
            else if($('input[type="checkbox"]:checked').attr('name')==="ingredients"){
                console.log("checked 2");
                var selected_ingredients_wrapper = $(this).parent();
                var selected_ingredients_img = $(this).parent().find('.media').find('.media-left').find('img').attr('src');
                var selected_ingredients_title = $(this).parent().find('.media').find('.media-body').find('.media-heading').html();
                var selected_ingredients_cal = $(this).parent().find('.media').find('.media-body').find('.mu-menu-energy').html();
                var value = $(this).val();
                    var ingredients_template = " <li id='"+value+"'> "+
                         " <div class='media'>"+
                                    "<div class='media-left'>"+
                                     "     <img class='media-object' src='"+selected_ingredients_img+"' alt='img'>"+
                                    "</div>"+
                                    "<div class='media-body' style='vertical-align: middle'>"+
                                     " <h4 class='media-heading'>"+selected_ingredients_title+"</h4>"+
                                     " <span class='mu-menu-energy'>"+selected_ingredients_cal+"</span>&nbsp;|&nbsp;"+
                                      "<span class='mu-menu-price'>Qty: <input type='number'></span>"+
                                    "</div>"+
                               " </div>"+
                     " </li> ";
                     $('#ingredients').append(ingredients_template);
               
            }
            else if($('input[type="checkbox"]:checked').attr('name')==="extras"){
                console.log("checked 3");
                var selected_extras_wrapper = $(this).parent();
                var selected_extras_img = $(this).parent().find('.media').find('.media-left').find('img').attr('src');
                var selected_extras_title = $(this).parent().find('.media').find('.media-body').find('.media-heading').html();
                var selected_extras_cal = $(this).parent().find('.media').find('.media-body').find('.mu-menu-energy').html();
                var value = $(this).val();
                    var extras_template = " <li id='"+value+"'> "+
                         " <div class='media'>"+
                                    "<div class='media-left'>"+
                                     "     <img class='media-object' src='"+selected_extras_img+"' alt='img'>"+
                                    "</div>"+
                                    "<div class='media-body' style='vertical-align: middle'>"+
                                     " <h4 class='media-heading'>"+selected_extras_title+"</h4>"+
                                     " <span class='mu-menu-energy'>"+selected_extras_cal+"</span>&nbsp;|&nbsp;"+
                                      "<span class='mu-menu-price'>Qty: <input type='number'></span>"+
                                    "</div>"+
                               " </div>"+
                     " </li> ";
                     $('#extras').append(extras_template);
               
            }
            else if($('input[type="checkbox"]:checked').attr('name')==="dressings"){
                console.log("checked 4");
                var selected_dressings_wrapper = $(this).parent();
                var selected_dressings_img = $(this).parent().find('.media').find('.media-left').find('img').attr('src');
                var selected_dressings_title = $(this).parent().find('.media').find('.media-body').find('.media-heading').html();
                var selected_dressings_cal = $(this).parent().find('.media').find('.media-body').find('.mu-menu-energy').html();
                var value = $(this).val();
                    var dressings_template = " <li id='"+value+"'> "+
                         " <div class='media'>"+
                                    "<div class='media-left'>"+
                                     "     <img class='media-object' src='"+selected_dressings_img+"' alt='img'>"+
                                    "</div>"+
                                    "<div class='media-body' style='vertical-align: middle'>"+
                                     " <h4 class='media-heading'>"+selected_dressings_title+"</h4>"+
                                     " <span class='mu-menu-energy'>"+selected_dressings_cal+"</span>&nbsp;|&nbsp;"+
                                      "<span class='mu-menu-price'>Qty: <input type='number'></span>"+
                                    "</div>"+
                               " </div>"+
                     " </li> ";
                     $('#dressings').append(dressings_template);
               
            }
                
        });
        
        
  
});