
$( document ).ready(function() {
    console.log( "ready!" );
    /*  ###################### Auth  code statr here ######################  */   
    $('form[id="loginFrm"]').validate({
        rules: {
                email: {
                    required: true,
                    email: true,
                },
                password: 'required',
            },
            messages: {
                email: {
                    required: 'Please enter email address.',
                    email : 'Please enter valid email',
                },
                password: 'Please enter password.',
            },
        submitHandler: function(form) {
            //form.submit();
            var email = $('#email').val();
            var pass = $('#password').val();
            $.post(base_url()+"auth/loginAction",{email: email, password:pass}, function(res, status){
                var data = jQuery.parseJSON(res);
                if(status == 'success'){
                    if(!data.error){
                        var redirect =''
                        if (data.user_role == 1) {
                           redirect = 'admin/';
                        }else if (data.user_role== 2) {
                            redirect = 'customer/'
                        }else if(data.user_role == 3){
                            redirect = 'profile/user'
                        }
                        //alert(data.user_role);
                        window.location = base_url()+redirect;
                    }else{
                        console.log(data.msg);
                        $('#custom_error').html(data.msg);
                    }
                }
                //alert("Data: " + data + "\nStatus: " + status);
                console.log("Data: " + data + "\nStatus: " + status);
            });
        }
    });
    $('form[id="profileUpdateFrm"]').validate({
        rules: {
                name: 'required',
                surname: 'required',
                phone:{
                    required: true,
                    number: true,
                },             
                email: {
                    required: true,
                    email: true,
                },
            },
            messages: {
                name: 'Please enter name.',
                surname: 'Please enter surname.',
                email: {
                    required: 'Please enter email address.',
                    email : 'Please enter valid email',
                },
                phone:{
                    required: 'Please enter phone',
                    number: 'please enter valid phone number',
                },
            },
        submitHandler: function(form) {
            $.ajax({
                type:"POST",
                url:base_url()+"profile/updateProfile",
                data: new FormData(form),
                processData: false,
                contentType: false,
                success: function (res){
                    var data = jQuery.parseJSON(res);
                    if(!data.error){
                        /*$('#profileImage').attr('src',base_url()+data.imagepath);
                        $('#custom_error').html(data.msg);*/
                        window.location.reload(); 
                    }else{
                        console.log(data.msg);
                        $('#custom_error').html(data.msg);
                    }
                    //alert(data);
                }
            });
            
        }
    });

    $('form[id="passwordChangeFrm"]').validate({
        rules: {
                old_password: 'required',
                password:{
                    required: true,
                    minlength: 4,
                },             
                conf_password: {
                    required: true,
                    equalTo : '#password',
                },
            },
            messages: {
                old_password: 'old_password',
                password: {
                    required: 'Please enter password',
                    minlength : 'password  length should be more than 3 characters',
                },
                conf_password:{
                    required: 'Please enter conf_password',
                    equalTo: 'confirm password does not match.',
                },
            },
        submitHandler: function(form) {
            var old_password = $('#old_password').val();
            var password = $('#password').val();
            var conf_password = $('#conf_password').val();
            $('#custom_error').html('');
            $.post(base_url()+"profile/changePasswordAction",{old_password: old_password, password: password, conf_password:conf_password}, function(res, status){
                var data = jQuery.parseJSON(res);
                if(status == 'success'){
                    if(!data.error){
                        $('#custom_error').html(data.msg);
                        window.location.reload();
                    }else{
                        console.log(data.msg);
                        $('#custom_error').html(data.msg);
                    }
                }
                //alert("Data: " + data + "\nStatus: " + status);
                console.log("Data: " + data + "\nStatus: " + status);
            });
            
        }
    });
    /*  ###################### Pages  code statr here ######################  */
    $('form[id="PageFrm"]').validate({
        rules: {
                title: 'required',
                description: 'required',
                },
                messages: {
                    name: 'Please enter title.',
                    description: 'Please enter description.',
                },
        submitHandler: function(form) {
            var title = $('#title').val().trim();
            var description = CKEDITOR.instances.description.getData().trim();
            var type = $('#pageType').val();
            var page_id = $('#page_id').val();
            $.post(base_url()+"pages/add",{title: title, description: description, pageType:type, page_id:page_id}, function(res, status){
                var data = jQuery.parseJSON(res);
                if(status == 'success'){
                    if(!data.error){
                        //$('#custom_error').html(data.msg);
                        window.location.reload();
                        //alert(data.msg);
                    }else{
                        //alert(data.msg);
                        $('#custom_error').html(data.msg);
                    }
                }
            });
            
        }
    });

    /*  ###################### category code statr here ######################  */
    $("#addCategoryFrm").submit(function(e) {
        e.preventDefault();
        var name = $('#name').val().trim();
        var description = $('#description_cat').val().trim();
        $('#custom_error').html('');
        if(name ==''){
            alert("Please Enter Name");
            return true;
        }else if(description ==''){
            alert("Please Enter Description");
            return true;
        }else{
            $.post(base_url()+"category/addAction",{name: name, description_cat: description}, function(res, status){
                var data = jQuery.parseJSON(res);
                if(status == 'success'){
                    if(!data.error){
                        //$('#custom_error').html(data.msg);
                        $('#name').val('');
                        $('#description_cat').val('');
                        $('#custom_error').html(data.msg);
                        //alert(data.msg);
                    }else{
                        alert(data.msg);
                        //$('#custom_error').html(data.msg);
                    }
                }
            });
        }
    });
    $( "body" ).on( "click", "#editCatSubmitBtn", function(e) {
        e.preventDefault();
        var id = $('#cat_edit_id').val();
        var name =  $('#cat_edit_name').val().trim();
        var description = $('#cat_edit_description').val().trim();
        //$('#custom_error').html('');
        if(name ==''){
            alert("Please Enter Name");
            return true;
        }else if(description ==''){
            alert("Please Enter Description");
            return true;
        }else{
            $.post(base_url()+"category/edit",{name: name, description_cat: description, id: id}, function(res, status){
                var data = jQuery.parseJSON(res);
                if(status == 'success'){
                    if(!data.error){
                        $("#editCatModal").modal("toggle");
                        alert(data.msg);
                        window.location.reload(); 
                    }else{
                        alert(data.msg);
                    }
                }
            });
        }
    });
    $( "body" ).on( "click", "#editCategory", function(e) {
        e.preventDefault();
        var id = $(this).data('cat-id');
        $.get(base_url()+"category/getById",{cat_id: id}, function(res, status){
                var data = jQuery.parseJSON(res);
                if(status == 'success'){
                    if(!data.error){
                        $('#cat_edit_id').val(id);
                        $('#cat_edit_name').val(data.name);
                        $('#cat_edit_description').val(data.description);
                        $("#editCatModal").modal("toggle");
                    }else{
                        alert(data.msg);
                    }
                }
        });
    });
    /*  ###################### Keyword  code statr here ######################  */

    $( "body" ).on( "click", "#keywordAddBtn", function(e) {
        e.preventDefault();
        var keyword_array = new Array();
        $(".text-box-keywords").each(function () {
            if($(this).val().trim()){
                keyword_array.push($(this).val());
            }
        });
        console.log("length is:"+keyword_array.length);
        if($('#keyword_category').val().trim() == ''){
            alert("Plese Select Keyword category");
            return true;
        }
        if(keyword_array.length > 0){
            var category_id = $('#keyword_category').val().trim();
            $.post(base_url()+"keyword/addAction",{name: keyword_array, category_id:category_id}, function(res, status){
                var data = jQuery.parseJSON(res);
                if(status == 'success'){
                    if(!data.error){
                        alert(data.msg);
                        window.location.reload();
                    }else{
                        alert(data.msg);
                    }
                }
            });
        }else{
            alert("Please enter keyword name");
        }
        
    });

    $( "body" ).on( "click", "#editKeyword", function(e) {
        e.preventDefault();
        var id = $(this).data('keyword-id');
        $.get(base_url()+"keyword/getById",{keyword_id: id}, function(res, status){
                var data = jQuery.parseJSON(res);
                if(status == 'success'){
                    if(!data.error){
                        $('#keyword_edit_id').val(id);
                        $('#keyword_edit_name').val(data.name);
                        $('#keyword_category').html(data.category);
                        $("#editKeywordModal").modal("toggle");
                    }else{
                        alert(data.msg);
                    }
                }
        });
    });

    $( "body" ).on( "click", "#editKeywordSubmitBtn", function(e) {
        e.preventDefault();
        var id = $('#keyword_edit_id').val();
        var name =  $('#keyword_edit_name').val().trim();
        var category = $('#keyword_category').val().trim();
        //$('#custom_error').html('');
        if(name ==''){
            alert(" Please Enter Name");
            return true;
        }else if(category ==''){
            alert(" Please Select category.");
            return true;
        }else{
            $.post(base_url()+"keyword/edit",{name: name, category: category, id: id}, function(res, status){
                var data = jQuery.parseJSON(res);
                if(status == 'success'){
                    if(!data.error){
                        $("#editKeywordModal").modal("toggle");
                        alert(data.msg);
                        window.location.reload(); 
                    }else{
                        alert(data.msg);
                    }
                }
            });
        }
    });

/*  ###################### Filter code statr here ######################  */    
    $( "body" ).on( "click", "#FilterAddBtn", function(e) {
        e.preventDefault();
        e.preventDefault();
        var category_id = $('#filter_category').val();
        var title =  $('#title').val().trim();
        var filter = $('#filter').val().trim();
        //$('#custom_error').html('');
        if(category_id ==''){
            alert(" Please select category.");
            return true;
        }else if(title ==''){
            alert(" Please enter title.");
            return true;
        }else if(filter ==''){
            alert(" Please enter filter");
            return true;
        }else{
            $.post(base_url()+"filter/addAction",{category_id:category_id, title:title, filter:filter}, function(res, status){
                var data = jQuery.parseJSON(res);
                if(status == 'success'){
                    if(!data.error){
                        alert(data.msg);
                        window.location.reload();
                    }else{
                        alert(data.msg);
                    }
                }
           });
        }
    });

    $( "body" ).on( "click", "#editFilter", function(e) {
        e.preventDefault();
        var id = $(this).data('filter-id');
        $.get(base_url()+"filter/getById",{filter_id: id}, function(res, status){
                var data = jQuery.parseJSON(res);
                if(status == 'success'){
                    if(!data.error){
                        $('#filter_edit_id').val(id);
                        $('#filter_edit_title').val(data.title);
                        $('#filter_edit_filter').val(data.filter);
                        $('#filter_category').html(data.category);
                        $("#editFilterModal").modal("toggle");
                    }else{
                        alert(data.msg);
                    }
                }
        });
    });

     $( "body" ).on( "click", "#editFilterSubmitBtn", function(e) {
        e.preventDefault();
        var id = $('#filter_edit_id').val();
        var title = $('#filter_edit_title').val().trim();
        var filter = $('#filter_edit_filter').val().trim();
        var category_id = $('#filter_category').val().trim();
        //$('#custom_error').html('');
        if(category_id ==''){
            alert(" Please select category.");
            return true;
        }else if(title ==''){
            alert(" Please enter title.");
            return true;
        }else if(filter ==''){
            alert(" Please enter filter");
            return true;
        }else{
            $.post(base_url()+"filter/edit",{title: title, filter: filter, category_id: category_id, id: id}, function(res, status){
                var data = jQuery.parseJSON(res);
                if(status == 'success'){
                    if(!data.error){
                        $("#editFilterModal").modal("toggle");
                        alert(data.msg);
                        window.location.reload(); 
                    }else{
                        alert(data.msg);
                    }
                }
            });
        }
    });
/*  ###################### customer code statr here ######################  */    
     $('form[id="customerAddFrm"]').validate({
        rules: {
                user_first_name: 'required',
                user_last_name: 'required',
                user_profile_pic: 'required', 
                customer_name: 'required',
                customer_description: 'required',
                customer_address: 'required',
                //customer_geo_co: 'required',
                customer_lat: 'required',
                customer_category: 'required',
                customer_img: 'required',
                customer_lat: 'required',
                customer_long: 'required',
                customer_short_description: 'required',
                customer_imei_no: 'required',
                customer_sim_no: 'required',
                customer_phone:{
                    required: true,
                    number: true,
                },             
                user_email: {
                    required: true,
                    email: true,
                },
                user_pass: {
                    required: true,
                    minlength: 8,
                },
                user_conf_pass: {
                    required: true,
                    minlength: 8,
                    equalTo : '#user_pass',
                },
            },
            messages: {
                user_first_name: 'Please enter first name',
                user_last_name: 'Please enter last name',
                user_profile_pic: 'Please select profile image',
                user_email: 'Enter a valid email',
                user_pass: {
                    minlength: 'Password must be at least 8 characters long',
                    required: 'Please Enter password'
                },
                user_conf_pass: {
                    minlength: 'Confirm password must be at least 8 characters long',
                    required: 'Please enter confirm password',
                    equalTo : 'Password and confirm password does not match'
                },

                customer_name: 'Please enter customer name',
                customer_description: 'Please enter customer description',
                customer_short_description: 'Please enter customer short description',
                customer_address: 'Please enter customer address',
                //customer_geo_co: 'Please enter customer geo co-ordinate',
                customer_lat: 'Please enter customer latitude.',
                customer_long: 'Please enter customer  longitude',
                customer_category: 'Please select customer category',
                customer_img: 'Please select customer image',
                customer_name: 'Please enter customer phone number',
                customer_imei_no: 'Please enter customer IMEI number',
                customer_sim_no: 'Please enter customer sim number',
            },
        submitHandler: function(form) {
            form.submit();
        }
    });
     $('form[id="customerDetailsFrm"]').validate({
        rules: {
                user_first_name: 'required',
                user_last_name: 'required',
                // user_profile_pic: 'required', 
                customer_name: 'required',
                customer_description: 'required',
                customer_address: 'required',
                //customer_geo_co: 'required',
                customer_lat: 'required',
                customer_category: 'required',
                // customer_img: 'required',
                customer_lat: 'required',
                customer_long: 'required',
                customer_short_description: 'required',
                customer_imei_no: 'required',
                customer_sim_no: 'required',
                customer_phone:{
                    required: true,
                    number: true,
                },             
            },
            messages: {
                user_first_name: 'Please enter first name',
                user_last_name: 'Please enter last name',
                customer_name: 'Please enter customer name',
                customer_description: 'Please enter customer description',
                customer_short_description: 'Please enter customer short description',
                customer_address: 'Please enter customer address',
                //customer_geo_co: 'Please enter customer geo co-ordinate',
                customer_lat: 'Please enter customer latitude.',
                customer_long: 'Please enter customer  longitude',
                customer_category: 'Please select customer category',
                //customer_img: 'Please select customer image',
                customer_name: 'Please enter customer phone number',
                customer_imei_no: 'Please enter customer IMEI number',
                customer_sim_no: 'Please enter customer sim number',
            },
        submitHandler: function(form) {
            form.submit();
        }
    });
     $('form[id="customerEditFrm"]').validate({
        rules: {
                user_first_name: 'required',
                user_last_name: 'required',
                // user_profile_pic: 'required', 
                customer_name: 'required',
                customer_description: 'required',
                customer_address: 'required',
                //customer_geo_co: 'required',
                customer_lat: 'required',
                // customer_category: 'required',
                // customer_img: 'required',
                customer_lat: 'required',
                customer_long: 'required',
                customer_short_description: 'required',
                customer_imei_no: 'required',
                customer_sim_no: 'required',
                customer_phone:{
                    required: true,
                    number: true,
                },             
            },
            messages: {
                user_first_name: 'Please enter first name',
                user_last_name: 'Please enter last name',
                customer_name: 'Please enter customer name',
                customer_description: 'Please enter customer description',
                customer_short_description: 'Please enter customer short description',
                customer_address: 'Please enter customer address',
                //customer_geo_co: 'Please enter customer geo co-ordinate',
                customer_lat: 'Please enter customer latitude.',
                customer_long: 'Please enter customer  longitude',
                //customer_category: 'Please select customer category',
                //customer_img: 'Please select customer image',
                customer_name: 'Please enter customer phone number',
                customer_imei_no: 'Please enter customer IMEI number',
                customer_sim_no: 'Please enter customer sim number',
            },
        submitHandler: function(form) {
            $('#customer_category').prop('disabled',false);
            form.submit();
        }
    });
/*  ###################### Forget password code statr here ######################  */ 
        $('form[id="forgetPasswordFrm"]').validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                },
            },
            messages: {
                email: {
                    required: 'Please enter email address.',
                    email : 'Please enter valid email',
                },
            },
            submitHandler: function(form) {
                form.submit();
            }
 
/*  ###################### Reset password code statr here ######################  */         });
$('form[id="resetPasswordFrm"]').validate({
            rules: {
                password: {
                    required: true,
                    minlength: 8,
                },
                conf_password: {
                    required: true,
                    minlength: 8,
                    equalTo : '#password',
                },
            },
            messages: {
                password: {
                    minlength: 'Password must be at least 8 characters long',
                    required: 'Please Enter password'
                },
                conf_password: {
                    minlength: 'Confirm password must be at least 8 characters long',
                    required: 'Please enter confirm password',
                    equalTo : 'Password and confirm password does not match'
                },
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
/*  ##################
    /*$( "body" ).on( "blur change", "#customer_address", function(e){ 
        var address = document.getElementById('customer_address').value;
        getLatitudeLongitude(showResult, address)
    });*/

});
function ValidateEmail(mail) {
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)){
    return true;
  }
  return false;
}
function showResult(result) {
    document.getElementById('customer_lat').value = result.geometry.location.lat();
    document.getElementById('customer_long').value = result.geometry.location.lng();
}

function getLatitudeLongitude(callback, address) {
    // If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
    address = address || 'new delhi, noida, jaypur';
    // Initialize the Geocoder
    geocoder = new google.maps.Geocoder();
    if (geocoder) {
        geocoder.geocode({
            'address': address
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                callback(results[0]);
            }
        });
    }
}
function readURL(input,id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+id+' img')
                .attr('src', e.target.result)
                .width(150)
                .height(200);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

/* geo location auto complete  start here */
    var placeSearch, autocomplete;
    var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
    };
    /*var componentForm = {
    customer_lat: 'short_name',
    customer_long : 'long_name',
    };*/

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('customer_address')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        document.getElementById('customer_lat').value = place.geometry.location.lat();
        document.getElementById('customer_long').value = place.geometry.location.lng();
        // for (var component in componentForm) {
        //   document.getElementById(component).value = '';
        //   document.getElementById(component).disabled = false;
        // }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        // for (var i = 0; i < place.address_components.length; i++) {
        //   var addressType = place.address_components[i].types[0];
        //   if (componentForm[addressType]) {
        //     var val = place.address_components[i][componentForm[addressType]];
        //     document.getElementById(addressType).value = val;
        //   }
        // }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
      






