$(document).ready(function() {
  console.log("ready!");
  /*  ###################### Auth  code statr here ######################  */

  $('form[id="loginFrm"]').validate({
    rules: {
      email: {
        required: true,
        email: true
      },
      password: "required"
    },
    messages: {
      email: {
        required: "Please enter email address.",
        email: "Please enter valid email"
      },
      password: "Please enter password."
    },
    submitHandler: function(form) {
      //form.submit();
      var email = $("#email").val();
      var password = $("#password").val();
      $.post(
        url + "auth/loginAction",
        { email: email, password: password },
        function(res, status) {
          var data = jQuery.parseJSON(res);

          if (status == "success") {
            if (!data.error) {
              var redirect = "";
              if (data.user_role == 1) {
                redirect = "admin/";
              } else if (data.user_role == 2) {
                redirect = "customer/";
              } else if (data.user_role == 3) {
                redirect = "profile/user";
              }
              //alert(data.user_role);
              window.location = url + redirect;
            } else {
              console.log(data.msg);
              $("#custom_error").html(data.msg);
            }
          }
          //alert("Data: " + data + "\nStatus: " + status);
          console.log("Data: " + data + "Status: " + status);
        }
      );
    }
  });
  $('form[id="profileUpdateFrm"]').validate({
    rules: {
      name: "required",
      surname: "required",
      phone: {
        required: true,
        number: true
      },
      email: {
        required: true,
        email: true
      }
    },
    messages: {
      name: "Please enter name.",
      surname: "Please enter surname.",
      email: {
        required: "Please enter email address.",
        email: "Please enter valid email"
      },
      phone: {
        required: "Please enter phone",
        number: "please enter valid phone number"
      }
    },
    submitHandler: function(form) {
      $.ajax({
        type: "POST",
        url: url + "profile/updateProfile",
        data: new FormData(form),
        processData: false,
        contentType: false,
        success: function(res) {
          var data = jQuery.parseJSON(res);
          if (!data.error) {
            /*$('#profileImage').attr('src',base_url()+data.imagepath);
                        $('#custom_error').html(data.msg);*/
            window.location.reload();
          } else {
            console.log(data.msg);
            $("#custom_error").html(data.msg);
          }
          //alert(data);
        }
      });
    }
  });

  $('form[id="passwordChangeFrm"]').validate({
    rules: {
      old_password: "required",
      password: {
        required: true,
        minlength: 4
      },
      conf_password: {
        required: true,
        equalTo: "#password"
      }
    },
    messages: {
      old_password: "Old password must not be empty",
      password: {
        required: "Password must not be empty",
        minlength: "Password  length should be more than 3 characters"
      },
      conf_password: {
        required: "Confirm Password must not be empty",
        equalTo: "confirm password does not match."
      }
    },
    submitHandler: function(form) {
      var old_password = $("#old_password").val();
      var password = $("#password").val();
      var conf_password = $("#conf_password").val();
      $("#custom_error").html("");
      $.post(
        url + "profile/changePasswordAction",
        {
          old_password: old_password,
          password: password,
          conf_password: conf_password
        },
        function(res, status) {
          var data = jQuery.parseJSON(res);
          if (status == "success") {
            if (!data.error) {
              $("#custom_error").html(data.msg);
              window.location.reload();
            } else {
              console.log(data.msg);
              $("#custom_error").html(data.msg);
            }
          }
          //alert("Data: " + data + "\nStatus: " + status);
          console.log("Data: " + data + "\nStatus: " + status);
        }
      );
    }
  });
  /*  ###################### Pages  code statr here ######################  */
  $('form[id="PageFrm"]').validate({
    rules: {
      title: "required",
      title_italian: "required",
      description: "required",
      description_italian: "required"
    },
    messages: {
      title: "Please enter title.",
      title_italian: "Please enter title in italian",
      description: "Please enter description.",
      description_italian: "Please enter description_italian in italian"
    },
    submitHandler: function(form) {
      var title = $("#title")
        .val()
        .trim();
      var title_italian = $("#title_italian")
        .val()
        .trim();
      var description = CKEDITOR.instances.description.getData().trim();
      var description_italian = CKEDITOR.instances.description_italian
        .getData()
        .trim();
      var type = $("#pageType").val();
      var page_id = $("#page_id").val();
      $.post(
        url + "pages/add",
        {
          title: title,
          title_italian: title_italian,
          description: description,
          description_italian: description_italian,
          pageType: type,
          page_id: page_id
        },
        function(res, status) {
          var data = jQuery.parseJSON(res);
          if (status == "success") {
            if (!data.error) {
              //$('#custom_error').html(data.msg);
              window.location.reload();
              //alert(data.msg);
            } else {
              //alert(data.msg);
              $("#custom_error").html(data.msg);
            }
          }
        }
      );
    }
  });

  $("#add_banner").click(function() {
    $("#addbannerModel").show();
  });
  /*  ###################### ingredient code statr here ######################  */

  //call empty model on add click
  $("#addIngredient").click(function() {
    $("#editCatModal").show();
    $("h4.modal-title").text(" Add Ingredient");
    $("#cat_edit_id").val();
    $("#cat_edit_name").val("");
    $("#cat_edit_name_italian").val("");
    $("#unit_type").val("");
    $("#calories").val("");
    $("#Image").val("");
    $("#name_error").text("");
    $("#name_italian_error").html("");
    $("#unit_type_error").html("");
    $("#calories_error").html("");
    $("#Image_error").html("");
    $("#price").val("");
    $("#ing_weight").val("");
    $("#ing_weight_error").html("");
    $("#price_error").html("");
    $("#im").hide();
  });
  //edit model call
  $("body").on("click", "#editIngredient", function(e) {
    e.preventDefault();
    var id = $(this).data("cat-id");
    $.get(url + "ingredient/getById", { id: id }, function(res, status) {
      var data = jQuery.parseJSON(res);
      if (status === "success") {
        if (!data.error) {
          $("#cat_edit_id").val(id);
          $("#price").val(data.price);
          $("#ing_weight").val(data.ing_weight);
          $("#cat_edit_name").val(data.ing_name);
          $("#cat_edit_name_italian").val(data.ing_name_arabic);
          $("#unit_type").val(data.unit_type);
          $("#image").val(data.ing_image);
          // $("#catid").val(data.cat_id);
          $("#cat_edit_type")
            .val(data.cat_id)
            .attr("selected", "selected");
          var imgurl = url + "uploads/ingredients/" + data.ing_image;
          $("#im").attr("src", imgurl);
          $("#calories").val(data.ing_calories);
          // $("#Image").val(data.ing_image);

          $("#editCatModal").modal("toggle");
          $("#button").text("Update");
          var baseurl = url + "ingredient/edit/" + id;

          $("#form").attr("action", baseurl);
          $("h4.modal-title").text("Edit Ingredient");
        } else {
          $("#custom_error").html(data.msg);
        }
      }
    });
  });
  /*  ###################### category  code statr here ######################  */

  //call empty model on add click
  $("#addSubCategory").click(function() {
    $("#addSubCategoryModel").show();
  });

  //edit model call
  $(".edit").click(function(e) {
    e.preventDefault();
    var id = $(this).data("cat-id");
    $.get(url + "sub-category/getById", { id: id }, function(res, status) {
      var data = jQuery.parseJSON(res);

      if (status === "success") {
        if (!data.error) {
          $("#editSubCategoryModel").modal("toggle");
          $("#sub_cat_id").val(id);
          $("#subcat_name").val(data.subcat_name);
          $("#subcat_name_arabic").val(data.subcat_name_arabic);
          $limit = data.max_limit;
          $max = parseInt($limit);
          $("#number").val($max);
          $("#parentcat_id")
            .val(data.parent_cat_id)
            .attr("selected", "selected");
          $("#saladtype")
            .val(data.salad_type)
            .attr("selected", "selected");
        } else {
          $("#custom_error").html(data.msg);
        }
      }
    });
  });

  //add user
  $("body").on("click", "#adduser", function(e) {
    e.preventDefault();

    $("#cat_edit_id").val("");
    $("#phone").val("");
    $("#email").val("");
    $("#name").val("");
    $("#button").text("Submit");
    $("#editCatModal").modal("toggle");
    $("h4.modal-title").text("Add User");
  });

  //edit user model call

  $("body").on("click", "#edituser", function(e) {
    e.preventDefault();
    var id = $(this).data("cat-id");

    $.get(url + "users/getById", { id: id }, function(res, status) {
      var data = jQuery.parseJSON(res);

      if (status === "success") {
        if (!data.error) {
          $("#cat_edit_id").val(id);
          $("#name").val(data.full_name);
          $("#email").val(data.email);
          $("#phone").val(data.phone);
          var baseurl = url + "users/edit/" + id;
          $("#userform").attr("action", baseurl);
          $("#button").text("Update");
          $("#editCatModal").modal("toggle");
        } else {
          $("#custom_error").html(data.msg);
        }
      }
    });
  });

  $(".changeorder").click(function(e) {
    e.preventDefault();
    var id = $(this).data("cat-id");
    var orderid = $("input[name='id']").val($(this).data("cat-id"));

    $("#editCatModal").modal("toggle");
  });

  $(".MoreIcon").click(function() {
    var id = $(this).data("texbox_id");

    if (confirm("Do you want to delete this ingredient?")) {
      $.ajax({
        url: url + "salad/delete/" + id,
        type: "post",
        data: { id: id },
        success: function() {
          location.reload();
        },
        error: function() {
          alert("Something went wrong records not deleted");
        }
      });
    } else {
    }
  });

  $("select.boot-multiselect-demo").change(function() {
    var id = [];
    $(":checkbox:checked").each(function(i) {
      id[i] = $(this).val();
    });

    if (id.length > 0) {
      $.get(url + "getUnitType", { id: id }, function(res, status) {
        var data = jQuery.parseJSON(res);

        if (status === "success") {
          if (!data.error) {
            var rate = $("#salad_price").val(data.salad_price);
            var calorys = $("#calories").val(data.calories_data);
          } else {
            $("#custom_error").html(data.msg);
          }
        }
      });
    } else {
      var rate = $("#salad_price").val("");
      var calorys = $("#calories").val("");
    }
  });
  /*  ###################### Filter code statr here ######################  */

  $("body").on("click", "#FilterAddBtn", function(e) {
    e.preventDefault();
    e.preventDefault();
    var category_id = $("#filter_category").val();
    var title = $("#title")
      .val()
      .trim();
    var title_italian = $("#title_italian")
      .val()
      .trim();
    var filter = $("#filter")
      .val()
      .trim();
    var filter_italian = $("#filter_italian")
      .val()
      .trim();
    var error_status = 0;
    $("#filter_category_error").html("");
    $("#title_error").html("");
    $("#title_italian_error").html("");
    $("#filter_error").html("");
    $("#filter_italian_error").html("");
    if (category_id == "") {
      error_status = 1;
      $("#filter_category_error").html("please select category.");
      $("#filter_category_error").css("color", "red");
    }
    if (title == "") {
      error_status = 1;
      $("#title_error").html("Please enter title.");
      $("#title_error").css("color", "red");
    }
    if (title_italian == "") {
      error_status = 1;
      $("#title_error").html("Please enter title in italian.");
      $("#title_error").css("color", "red");
    }
    if (filter == "") {
      error_status = 1;
      $("#filter_error").html("Please enter filter");
      $("#filter_error").css("color", "red");
    }
    if (filter_italian == "") {
      error_status = 1;
      $("#filter_error").html("Please enter filter name in italian");
      $("#filter_error").css("color", "red");
    }
    if (!error_status) {
      $.post(
        url + "filter/addAction",
        {
          category_id: category_id,
          title: title,
          title_italian: title_italian,
          filter: filter,
          filter_italian: filter_italian
        },
        function(res, status) {
          var data = jQuery.parseJSON(res);
          if (status == "success") {
            if (!data.error) {
              //alert(data.msg);
              window.location.reload();
            } else {
              alert(data.msg);
            }
          }
        }
      );
    }
  });

  $("body").on("click", "#editFilter", function(e) {
    e.preventDefault();
    var id = $(this).data("filter-id");
    $.get(url + "filter/getById", { filter_id: id }, function(res, status) {
      var data = jQuery.parseJSON(res);
      if (status == "success") {
        if (!data.error) {
          $("#filter_edit_id").val(id);
          $("#filter_edit_title").val(data.title);
          $("#filter_edit_title_italian").val(data.title_italian);
          $("#filter_edit_filter").val(data.filter);
          $("#filter_edit_filter_italian").val(data.filter_italian);
          $("#filter_category").html(data.category);
          $("#editFilterModal").modal("toggle");
        } else {
          alert(data.msg);
        }
      }
    });
  });

  $("body").on("click", "#editFilterSubmitBtn", function(e) {
    e.preventDefault();
    var id = $("#filter_edit_id").val();
    var title = $("#filter_edit_title")
      .val()
      .trim();
    var title_italian = $("#filter_edit_title_italian")
      .val()
      .trim();
    var filter = $("#filter_edit_filter")
      .val()
      .trim();
    var filter_italian = $("#filter_edit_filter_italian")
      .val()
      .trim();
    var category_id = $("#filter_category")
      .val()
      .trim();
    $("#filter_category_error").html("");
    $("#title_error").html("");
    $("#filter_error").html("");
    //$('#custom_error').html('');
    if (category_id == "") {
      $("#filter_category_error").html("please select category.");
      $("#filter_category_error").css("color", "red");
      return true;
    } else if (title == "") {
      $("#title_error").html("Please enter title.");
      $("#title_error").css("color", "red");
      return true;
    } else if (title_italian == "") {
      $("#title_italian_error").html("Please enter title name in italian.");
      $("#title_italian_error").css("color", "red");
      return true;
    } else if (filter == "") {
      $("#filter_error").html("Please enter filter");
      $("#filter_error").css("color", "red");
      return true;
    } else if (filter_italian == "") {
      $("#filter_italian_error").html("Please enter filter name in italian");
      $("#filter_italian_error").css("color", "red");
      return true;
    } else {
      $.post(
        url + "filter/edit",
        {
          title: title,
          title_italian: title_italian,
          filter: filter,
          filter_italian: filter_italian,
          category_id: category_id,
          id: id
        },
        function(res, status) {
          var data = jQuery.parseJSON(res);
          if (status == "success") {
            if (!data.error) {
              $("#editFilterModal").modal("toggle");
              //alert(data.msg);
              window.location.reload();
            } else {
              alert(data.msg);
            }
          }
        }
      );
    }
  });
  /*  ###################### customer code statr here ######################  */

  $('form[id="customerAddFrm"]').validate({
    rules: {
      user_first_name: "required",
      user_last_name: "required",
      //user_profile_pic: 'required',
      customer_name: "required",
      customer_name_italian: "required",
      // customer_description: 'required',
      customer_address: "required",
      //customer_geo_co: 'required',
      customer_lat: "required",
      customer_category: "required",
      //customer_filter: 'required',
      "customer_filter[]": {
        required: true
      },
      customer_img: "required",
      customer_lat: "required",
      customer_long: "required",
      //customer_short_description: 'required',
      customer_imei_no: "required",
      customer_sim_no: "required",
      customer_phone: {
        required: true,
        number: true
      },
      user_email: {
        required: true,
        email: true
      },
      user_pass: {
        required: true,
        minlength: 8
      },
      user_conf_pass: {
        required: true,
        minlength: 8,
        equalTo: "#user_pass"
      }
    },
    messages: {
      user_first_name: "Please enter first name",
      user_last_name: "Please enter last name",
      //user_profile_pic: 'Please select profile image',
      user_email: "Enter a valid email",
      user_pass: {
        minlength: "Password must be at least 8 characters long",
        required: "Please Enter password"
      },
      user_conf_pass: {
        minlength: "Confirm password must be at least 8 characters long",
        required: "Please enter confirm password",
        equalTo: "Password and confirm password does not match"
      },
      customer_phone: {
        required: "Please enter customer phone number",
        number: true
      },

      customer_name: "Please enter customer name",
      customer_name_italian: "Please enter customer name in italian",
      customer_description: "Please enter customer description",
      customer_short_description: "Please enter customer short description",
      customer_address: "Please enter customer address",
      //customer_geo_co: 'Please enter customer geo co-ordinate',
      customer_lat: "Please enter customer latitude.",
      customer_long: "Please enter customer  longitude",
      customer_category: "Please select customer category",
      //customer_filter: 'Please select customer filter',
      "customer_filter[]": {
        required: "Please select customer filter"
      },
      customer_img: "Please select customer image",
      //customer_name: 'Please enter customer phone number',
      customer_imei_no: "Please enter customer IMEI number",
      customer_sim_no: "Please enter customer sim number"
    },
    submitHandler: function(form) {
      var img = $("#customer_img").val();
      $("#customer_address_error").html("");
      var country = $("#country")
        .val()
        .trim();
      var state = $("#administrative_area_level_1")
        .val()
        .trim();
      var city = $("#locality")
        .val()
        .trim();
      console.log("edit form is submited.");
      if (country == "" || state == "" || city == "") {
        console.log("edit form is submited.");
        $("#customer_address_error").html("Please enter proper address.");
        $("#customer_address_error").css("color", "red");
        return false;
      }
      if (img == "") {
        $("#customer_img_error").html("Please upload image.");
        $("#customer_img_error").css("color", "red");
        return false;
      } else {
        form.submit();
      }
    }
  });
  $("body").on("change", "#customer_address", function(e) {
    $("#customer_address_error").html("");
    var address = $("#customer_address")
      .val()
      .trim();
    if (address) {
      setTimeout(function() {
        var country = $("#country")
          .val()
          .trim();
        var state = $("#administrative_area_level_1")
          .val()
          .trim();
        var city = $("#locality")
          .val()
          .trim();
        console.log("address is validate.");
        if (country == "" || state == "" || city == "") {
          console.log("edit form is submited.");
          $("#customer_address_error").html("Please enter proper address.");
          $("#customer_address_error").css("color", "red");
          return false;
        } else {
          $("#customer_address_error").html("");
        }
      }, 700);
    }
  });
  $("body #customerAddFrm").on("change", "#user_email", function(e) {
    e.preventDefault();
    $("#userEmailError").html("");
    var email = $("#user_email").val();
    if (email.length > 0 && ValidateEmail(email)) {
      $.post(url + "customer/isCustomerExists", { email: email }, function(
        res,
        status
      ) {
        var data = jQuery.parseJSON(res);
        if (status == "success") {
          if (data.status == "success") {
            $("#userEmailError").html(data.msg);
            $("#userEmailError").css("color", "green");
            $("#customerAddBtn").attr("disabled", false);
          } else {
            $("#userEmailError").html(data.msg);
            $("#userEmailError").css("color", "red");
            $("#customerAddBtn").attr("disabled", true);
          }
        }
      });
    }
  });
  $("body #customerEditFrm").on("change", "#user_email", function(e) {
    e.preventDefault();
    $("#userEmailError").html("");
    var email = $("#user_email").val();
    var id = $("#user_id").val();
    if (email.length > 0 && ValidateEmail(email)) {
      $.post(
        url + "customer/isCustomerExists",
        { email: email, id: id },
        function(res, status) {
          var data = jQuery.parseJSON(res);
          if (status == "success") {
            if (data.status == "success") {
              $("#userEmailError").html(data.msg);
              $("#userEmailError").css("color", "green");
              $("#customerAddBtn").attr("disabled", false);
            } else {
              $("#userEmailError").html(data.msg);
              $("#userEmailError").css("color", "red");
              $("#customerAddBtn").attr("disabled", true);
            }
          }
        }
      );
    }
  });
  $('form[id="customerDetailsFrm"]').validate({
    rules: {
      customer_name: "required",
      // customer_description: 'required',
      customer_address: "required",
      customer_lat: "required",
      customer_category: "required",
      customer_lat: "required",
      customer_long: "required",
      // customer_short_description: 'required',
      customer_imei_no: "required",
      customer_sim_no: "required",
      customer_phone: {
        required: true,
        number: true
      }
    },
    messages: {
      customer_name: "Please enter customer name",
      // customer_description: 'Please enter customer description',
      //customer_short_description: 'Please enter customer short description',
      customer_address: "Please enter customer address",
      customer_lat: "Please enter customer latitude.",
      customer_long: "Please enter customer  longitude",
      customer_category: "Please select customer category",
      //customer_name: 'Please enter customer phone number',
      customer_imei_no: "Please enter customer IMEI number",
      customer_sim_no: "Please enter customer sim number"
    },
    submitHandler: function(form) {
      $("#customer_category").prop("disabled", false);
      $("#customer_address_error").html("");
      var country = $("#country")
        .val()
        .trim();
      var state = $("#administrative_area_level_1")
        .val()
        .trim();
      var city = $("#locality")
        .val()
        .trim();
      console.log("edit form is submited.");
      if (country == "" || state == "" || city == "") {
        //alert('form is submited in if part'+ "country:"+country+" state:"+state+" city:"+city);
        console.log("edit form is submited.");
        $("#customer_address_error").html("Please enter proper address.");
        $("#customer_address_error").css("color", "red");
        return false;
      } else {
        //alert('form is submited in else part'+ "country:"+country+" state:"+state+" city:"+city);
        form.submit();
      }
    }
  });
  $('form[id="customerEditFrm"]').validate({
    rules: {
      customer_name: "required",
      customer_address: "required",
      customer_lat: "required",
      customer_lat: "required",
      customer_long: "required",
      //customer_description: 'required',
      //customer_short_description: 'required',
      customer_imei_no: "required",
      customer_sim_no: "required",
      customer_phone: {
        required: true,
        number: true
      }
    },
    messages: {
      customer_name: "Please enter customer name",
      customer_description: "Please enter customer description",
      customer_short_description: "Please enter customer short description",
      customer_address: "Please enter customer address",
      customer_lat: "Please enter customer latitude.",
      customer_long: "Please enter customer  longitude",
      //customer_name: 'Please enter customer phone number',
      customer_imei_no: "Please enter customer IMEI number",
      customer_sim_no: "Please enter customer sim number"
    },
    submitHandler: function(form) {
      $("#customer_category").prop("disabled", false);
      $("#customer_address_error").html("");
      var country = $("#country")
        .val()
        .trim();
      var state = $("#administrative_area_level_1")
        .val()
        .trim();
      var city = $("#locality")
        .val()
        .trim();
      console.log("edit form is submited.");
      if (country == "" || state == "" || city == "") {
        //alert('form is submited in if part'+ "country:"+country+" state:"+state+" city:"+city);
        console.log("edit form is submited.");
        $("#customer_address_error").html("Please enter proper address.");
        $("#customer_address_error").css("color", "red");
        return false;
      } else {
        //alert('form is submited in else part'+ "country:"+country+" state:"+state+" city:"+city);
        form.submit();
      }
    }
  });
  /*  ###################### Forget password code statr here ######################  */

  $('form[id="forgetPasswordFrm"]').validate({
    rules: {
      email: {
        required: true,
        email: true
      }
    },
    messages: {
      email: {
        required: "Please enter email address.",
        email: "Please enter valid email"
      }
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
  /*  ###################### Reset password code statr here ######################  */

  $('form[id="resetPasswordFrm"]').validate({
    rules: {
      password: {
        required: true,
        minlength: 8
      },
      conf_password: {
        required: true,
        minlength: 8,
        equalTo: "#password"
      }
    },
    messages: {
      password: {
        minlength: "Password must be at least 8 characters long",
        required: "Please Enter password"
      },
      conf_password: {
        minlength: "Confirm password must be at least 8 characters long",
        required: "Please enter confirm password",
        equalTo: "Password and confirm password does not match"
      }
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
  /*  ###################### Menu added form validation code statr here ######################  */

  $('form[id="menuAddFrm"]').validate({
    rules: {
      //menu_name: 'required',
      "menu_name[]": {
        required: true
      },
      "menu_name_italian[]": {
        required: true
      },
      "menu_description[]": {
        required: true
      },
      "menu_description_italian[]": {
        required: true
      },
      "menu_filters[]": {
        required: true
      },
      "menu_images[]": {
        required: true
      },
      "menu_keywords[]": {
        required: true
      },

      //menu_description: 'required',
      menu_category: "required"
      //menu_keywords: 'required',
      //menu_filters: 'required',
      //menu_images: 'required',
    },
    messages: {
      //menu_name: 'Please enter menu name.',
      //menu_description: 'Please enter menu description.',
      menu_category: "Please select menu category.",
      //menu_keywords: 'Please select menu keywords.',
      //menu_filters: 'Please select menu filters',
      //menu_images: 'Please select menu images',
      "menu_name[]": {
        required: "Please enter menu name."
      },
      "menu_name_italian[]": {
        required: "Please enter menu name in italian."
      },
      "menu_description[]": {
        required: "Please enter menu description."
      },
      "menu_description_italian[]": {
        required: "Please enter menu description in italian."
      },
      "menu_filters[]": {
        required: "Please select menu filters"
      },
      "menu_images[]": {
        required: "Please select menu images"
      },
      "menu_keywords[]": {
        required: "Please select menu keywords"
      }
    },
    submitHandler: function(form) {
      var img = $("#menu_images").val();
      if (img == "") {
        $("#menu_images_error").html("Please upload image.");
        $("#menu_images_error").css("color", "red");
        return;
      } else {
        form.submit();
      }
    }
  });
  $('form[id="menuEditFrm]').validate({
    rules: {
      //menu_name: 'required',
      "menu_name[]": {
        required: true
      },
      "menu_description[]": {
        required: true
      },
      "menu_filters[]": {
        required: true
      },
      "menu_keywords[]": {
        required: true
      },

      //menu_description: 'required',
      menu_category: "required"
      //menu_keywords: 'required',
      //menu_filters: 'required',
      //menu_images: 'required',
    },
    messages: {
      //menu_name: 'Please enter menu name.',
      //menu_description: 'Please enter menu description.',
      menu_category: "Please select menu category.",
      //menu_keywords: 'Please select menu keywords.',
      //menu_filters: 'Please select menu filters',
      //menu_images: 'Please select menu images',
      "menu_name[]": {
        required: "Please enter menu name."
      },
      "menu_description[]": {
        required: "Please enter menu description."
      },
      "menu_filters[]": {
        required: "Please select menu filters"
      },
      "menu_keywords[]": {
        required: "Please select menu keywords"
      }
    },
    submitHandler: function(form) {
      form.submit();
      // var img = $('#menu_images').val();
      // if (img == '') {
      //     $('#menu_images_error').html('Please upload image.');
      //     $('#menu_images_error').css("color",'red');
      //     return;
      // }else{
      //     form.submit();
      // }
    }
  });
  //Add  category form validation start
  $('form[id="addMenuCategoryFrm"]').validate({
    rules: {
      name: "required",
      name_italian: "required",
      description: "required",
      category: "required"
    },
    messages: {
      name: "Please enter  name",
      name_italian: "Please enter name in italian.",
      description: "Please enter  description",
      category: "Please select category"
    },
    submitHandler: function(form) {
      //form.submit();
      var name = $("#name")
        .val()
        .trim();
      var name_italian = $("#name_italian")
        .val()
        .trim();
      var description = $("#description")
        .val()
        .trim();
      var category = $("#category")
        .val()
        .trim();
      $.post(
        url + "menu/addCategoryAction",
        {
          name: name,
          name_italian: name_italian,
          description: description,
          category: category
        },
        function(res, status) {
          var data = jQuery.parseJSON(res);
          if (status == "success") {
            if (!data.error) {
              //$('#custom_error').html(data.msg);
              $("#name").val("");
              $("#description").val("");
              $("#name_italian").val("");
              $("#custom_error").html(data.msg);
              //alert(data.msg);
            } else {
              //alert(data.msg);
              $("#custom_error").html(data.msg);
            }
          }
        }
      );
    }
  });
  //Edit category form validation start
  $('form[id="editMenuCategoryFrm"]').validate({
    rules: {
      name: "required",
      name_italian: "required",
      description: "required",
      category: "required"
    },
    messages: {
      name: "Please enter  name",
      name_italian: "Please enter name in italian.",
      description: "Please enter  description",
      category: "Please select category"
    },
    submitHandler: function(form) {
      //form.submit();
      var name = $("#name")
        .val()
        .trim();
      var name_italian = $("#name_italian")
        .val()
        .trim();
      var description = $("#description")
        .val()
        .trim();
      var menu_category_id = $("#menu_category_id").val();
      var category = $("#category")
        .val()
        .trim();
      $.post(
        url + "menu/editCategoryAction",
        {
          name: name,
          name_italian: name_italian,
          description: description,
          menu_category_id: menu_category_id,
          category: category
        },
        function(res, status) {
          var data = jQuery.parseJSON(res);
          if (status == "success") {
            if (!data.error) {
              $("#custom_error").html(data.msg);
              //alert(data.msg);
            } else {
              //alert(data.msg);
              $("#custom_error").html(data.msg);
            }
          }
        }
      );
    }
  });
  //notification code start here
  $("body").on("click", ".notification_read", function(e) {
    e.preventDefault();
    var notification_id = $(this).data("notification_id");
    //alert(notification_id);
    $.post(
      url + "notification/updateReadStatus",
      { id: notification_id },
      function(res, status) {
        var data = jQuery.parseJSON(res);
        if (status == "success") {
          if (!data.error) {
            //alert(data.msg);
            window.location.reload();
          } else {
            alert(data.msg);
          }
        }
      }
    );
  });
  $(".deleteCustomer").click(function(event) {
    event.preventDefault();
    var customer_id = $(this).data("customer_id");
    console.log("Delete customer is is:" + customer_id);
    bootbox.confirm({
      title: "Delete Customer?",
      message: "Are you sure want to delete ?",
      buttons: {
        cancel: {
          label: '<i class="fa fa-times"></i> Cancel'
        },
        confirm: {
          label: '<i class="fa fa-check"></i> Confirm'
        }
      },
      callback: function(result) {
        console.log("This was logged in the callback: " + result);
        if (result) {
          $.post(url + "customer/delete", { id: customer_id }, function(
            res,
            status
          ) {
            var data = jQuery.parseJSON(res);
            if (status == "success") {
              if (!data.error) {
                window.location.reload();
              } else {
                window.location.reload();
              }
            }
          });
        }
      }
    });
  });

  $("body").on("change", ".customer_status_select", function(event) {
    event.preventDefault();
    var customer_id = $(this).data("customer_id");
    var status = $("#customer_status_select_" + customer_id).val();
    console.log("status update customer is :" + customer_id);
    bootbox.confirm({
      title: "Update Customer status?",
      message: "Are you sure want to update customer status?",
      buttons: {
        cancel: {
          label: '<i class="fa fa-times"></i> Cancel'
        },
        confirm: {
          label: '<i class="fa fa-check"></i> Confirm'
        }
      },
      callback: function(result) {
        console.log("change status in the callback: " + result);
        if (result) {
          $.get(
            url + "customer/updateStatus",
            { id: customer_id, status: status },
            function(res, status) {
              var data = jQuery.parseJSON(res);
              if (status == "success") {
                if (!data.error) {
                  $("#custom_error").html(data.msg);
                } else {
                  $("#custom_error").html(data.msg);
                }
              }
            }
          );
        }
      }
    });
  });
  $("body").on("change", ".notification_delete_action", function(event) {
    event.preventDefault();
    var notification_id = $(this).data("notification_id");
    var status = $("#notification_delete_action_" + notification_id).val();
    console.log(
      "status update notification id is:" +
        notification_id +
        " status:" +
        status
    );
    if (status == 1) {
      var msg = "Are you sure want to delete notification?";
    } else {
      var msg = "Are you sure want to move notification into trash?";
    }
    if (status && status != "undefined") {
      bootbox.confirm({
        title: "Delete notification?",
        message: msg,
        buttons: {
          cancel: {
            label: '<i class="fa fa-times"></i> Cancel'
          },
          confirm: {
            label: '<i class="fa fa-check"></i> Confirm'
          }
        },
        callback: function(result) {
          console.log("change status in the callback: " + result);
          if (result) {
            $.post(
              url + "notification/delete",
              { id: notification_id, status: status },
              function(res, status) {
                var data = jQuery.parseJSON(res);
                if (status == "success") {
                  if (!data.error) {
                    window.location.reload();
                  } else {
                    window.location.reload();
                  }
                }
              }
            );
          }
        }
      });
    }
  });
  $(".disableItem").click(function(event) {
    return confirm("Are you sure want to disable this item?");
  });
  $(".delete").click(function(event) {
    return confirm("Are you sure want to delete this item?");
  });
  $(".enableItem").click(function(event) {
    return confirm("Are you sure want to enable this item?");
  });
  /*  ##################
    /*$( "body" ).on( "blur change", "#customer_address", function(e){ 
        var address = document.getElementById('customer_address').value;
        getLatitudeLongitude(showResult, address)
    });*/
});
function ValidateEmail(mail) {
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
    return true;
  }
  return false;
}
function showResult(result) {
  document.getElementById(
    "customer_lat"
  ).value = result.geometry.location.lat();
  document.getElementById(
    "customer_long"
  ).value = result.geometry.location.lng();
}

function getLatitudeLongitude(callback, address) {
  // If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
  address = address || "new delhi, noida, jaypur";
  // Initialize the Geocoder
  geocoder = new google.maps.Geocoder();
  if (geocoder) {
    geocoder.geocode(
      {
        address: address
      },
      function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          callback(results[0]);
        }
      }
    );
  }
}
function readURL(input, id) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $("#" + id + " img")
        .attr("src", e.target.result)
        .width(150)
        .height(200);
    };

    reader.readAsDataURL(input.files[0]);
  }
  /*$(input.files).each(function () {
        var reader = new FileReader();
        reader.readAsDataURL(this);
        reader.onload = function (e) {
            $("#"+id).append("<div class='col-md-2' id=1customer_img_display'><img src='"+e.target.result+"' height='90' style='border-radius: 4px' /></div>");
        }
    });*/
}

/* geo location auto complete  start here */
var placeSearch, autocomplete;
/*var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
    };*/
var componentForm = {
  locality: "long_name",
  administrative_area_level_1: "long_name",
  country: "long_name"
};

function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */ (document.getElementById(
      "customer_address"
    )),
    { types: ["geocode"] }
  );

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener("place_changed", fillInAddress);
}

function fillInAddress() {
  // Get the place details from the autocomplete object.
  $("#administrative_area_level_1").val("");
  $("#locality").val("");
  var place = autocomplete.getPlace();
  document.getElementById("customer_lat").value = place.geometry.location.lat();
  document.getElementById(
    "customer_long"
  ).value = place.geometry.location.lng();
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  /*if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }*/
}
