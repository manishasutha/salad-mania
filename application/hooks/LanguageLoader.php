<?php

class LanguageLoader

{

   function initialize() {

       $ci =& get_instance();

       $ci->load->helper('language');

       $siteLang = $ci->session->userdata('site_lang');
       if ($siteLang == 'italian') {
           $ci->lang->load('italian',$siteLang);
       } else {
           $ci->lang->load('information','english');

       }

   }

}
?>