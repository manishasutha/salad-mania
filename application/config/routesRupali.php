<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//=====================User Routes====================//
$route['/'] = 'HomeController/index';
$route['about-us'] = 'HomeController/about_us';
$route['contact-us'] = 'HomeController/contact_us';
$route['contact-us-submit'] = 'HomeController/contact_us_submit';

$route['terms'] = 'HomeController/terms';
$route['policy'] = 'HomeController/policy';

$route['verify-email/(:any)'] = 'HomeController/index/$1';
$route['our-story'] = 'HomeController/our_story';
$route['menu'] = 'SaladController/index';
$route['make-your-own'] = 'SaladController/make_your_own';
$route['change-password'] = 'HomeController/change_password';
$route['myorderdetail/(:any)'] = 'HomeController/myorderdetail/$1';

//change language
$route['change_language/(:any)'] = 'HomeController/change_language/$1';

$route['menu-load-data'] = 'CommonController/menu_load_data';

$route['custom-salad-load-data'] = 'CommonController/custom_load_data';
$route['add-to-cart-model'] = 'SaladController/add_to_cat_model';
$route['insert-data-into-cart'] = 'SaladController/insert_data_into_cart';

$route['confirm-order/(:any)'] = 'SaladController/confirm_order/$1';
$route['checkout-order'] = 'SaladController/checkout_order';

$route['address-update'] = 'SaladController/address_update';

$route['create-order'] = 'SaladController/create_order';
$route['payment'] = 'PaymentController/index';
$route['payment-success'] = 'PaymentController/success';

$route['myaccount'] = 'HomeController/myProfile';
$route['profile-update'] = 'HomeController/profile_update';

//==========>regist login forgot password by ajax routes<============
$route['register'] = 'RegisterLoginController/register';
$route['guest-sign-up'] = 'RegisterLoginController/guest_register';
$route['login'] = 'RegisterLoginController/login';
$route['forgot'] = 'RegisterLoginController/forgot';
$route['forgot/(:any)'] = 'RegisterLoginController/forgot/$1';
$route['reset-pswd'] = 'RegisterLoginController/reset_password';
$route['logout'] = 'RegisterLoginController/logout';



////////////////////COMMON ROUTES///////////////////////////
$route['ajax/get-single-row'] = 'CommonController/get_single_row';//deleted_at null, post
$route['ajax/get-detail'] = 'CommonController/get_detail_ajax';//deleted_at null, post
$route['ajax/get-detail/join'] = 'CommonController/get_detail_ajax_join';
$route['ajax/get-row/join-two-table'] = 'CommonController/get_row_with_join_two_tables';
$route['ajax/get-single-field'] = 'CommonController/get_single_field';
$route['soft-delete/(:any)/(:any)'] = 'CommonController/soft_delete/$1/$2';//(table/id)
$route['soft-delete-by-field/(:any)/(:any)/(:any)'] = 'CommonController/soft_delete_by_field/$1/$2/$3';//(table/field_name/field_value)
$route['permanent-delete/(:any)/(:any)'] = 'CommonController/permanent_delete/$1/$2';//(table/id)
$route['permanent_delete_by_field_name/(:any)/(:any)/(:any)'] = 'CommonController/permanent_delete_by_field_name/$1/$2/$3';//

$route['ajax/get-salad-detail-for-home-page/join'] = 'CommonController/get_salad_details_for_home_page';

$route['check-email']='CommonController/check_email';
$route['check-mobile']='CommonController/check_mobile';
$route['get-cat-id']='CommonController/get_cat_id';




//=====================Admin routes===================//
$route['getUnitType'] = 'Salad/getUnitType';
$route['add-salad'] = 'Salad/add_salad';
$route['view-salads'] = 'Salad/view_salads';
$route['view-salads/delete/(:any)'] = 'Salad/delete/$1';
$route['custom-view-salads'] = 'Salad/view_custom_salads';
$route['view-salads/details/(:any)'] = 'Salad/details/$1';
$route['salad/delete/(:any)'] = 'Salad/deleteing/$1';
$route['view-salads/enable/(:any)'] = 'Salad/enable/$1';
$route['view-salads/edit/(:any)'] = 'Salad/edit/$1';
$route['category'] = 'SmCategory/index';
// $route['category/getById'] = 'SmCategory/getById';
// $route['category/edit'] = 'SmCategory/edit';
// $route['category/delete/(:any)'] = 'SmCategory/delete/$1';
// $route['category/enable/(:any)'] = 'SmCategory/enable/$1';
// $route['add-check-category'] = 'SmCategory/check_add_category';
// $route['edit-check-category'] = 'SmCategory/check_edit_category';
// $route['category/add'] = 'SmCategory/add';
//sub-cat routes
$route['sub-category'] = 'SmCategory/list_subcat';
$route['sub-category/getById'] = 'SmCategory/getById';
$route['sub-category/edit'] = 'SmCategory/edit';
$route['sub-category/disable/(:any)'] = 'SmCategory/disable/$1';
$route['sub-category/enable/(:any)'] = 'SmCategory/enable/$1';
$route['sub-add-check-category'] = 'SmCategory/check_add_category';
$route['sub-edit-check-category'] = 'SmCategory/check_edit_category';
$route['sub-category/add'] = 'SmCategory/add';
$route['add-check-category'] = 'SmCategory/check_add_category';
$route['edit-check-category'] = 'SmCategory/check_edit_category';
//sub cat routes end
$route['ingredient'] = 'SmIngredients/index';
$route['ingredient/getById'] = 'SmIngredients/getById';
$route['ingredient/edit/(:any)'] = 'SmIngredients/edit/$1';
$route['ingredient/add'] = 'SmIngredients/add';
$route['ingredient/delete/(:any)'] = 'SmIngredients/delete/$1';
$route['ingredient/enable/(:any)'] = 'SmIngredients/enable/$1';

$route['add-check-ingredient'] = 'SmIngredients/add_check_ingredient';

$route['orders'] = 'SmOrders/index';
$route['orders/order_details']='SmOrders/order_details';
$route['orders/change_status']='SmOrders/change_status';
$route['orders/details/(:any)']='SmOrders/details/$1';
$route['add-coupons'] = 'SmCoupons/view_salads';
$route['view-coupons'] = 'SmCoupons/view_salads';
$route['users'] = 'SmUsers/index';
$route['users/details/(:any)'] = 'SmUsers/details/$1';
$route['users/delete/(:any)'] = 'SmUsers/delete/$1';
$route['users/enable/(:any)'] = 'SmUsers/enable/$1';
$route['users/getById'] = 'SmUsers/getById';
$route['users/edit/(:any)']='SmUsers/edit/$1';
$route['users/add']='SmUsers/add';
$route['add-coupons'] = 'Coupns/add_coupns';
$route['coupons'] = 'Coupns/list_coupns';
$route['coupons/delete/(:any)'] = 'Coupns/delete/$1';
$route['coupons/enable/(:any)'] = 'Coupns/enable/$1';
$route['coupons/edit/(:any)'] = 'Coupns/edit/$1';
$route['pages/about']='Stapages/about';
$route['pages/terms_and_condition']='Stapages/tc';
$route['pages/privacy']='Stapages/privacy';
$route['pages/settings']='Stapages/settings';
$route['notification']='Stapages/notification';
$route['notification/delete/(:any)'] = 'Stapages/deleteing/$1';
$route['admin'] = 'Stapages/dashboard';
$route["admin/login"] = "auth/index";


$route['default_controller'] = 'HomeController';
//$route['default_controller'] = 'profile';
$route['404_override'] =  'Condolence404/index';
$route['translate_uri_dashes'] = FALSE;