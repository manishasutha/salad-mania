<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SmOrders extends CI_Controller {
	public function __construct()
    {
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
			$this->load->helper('common_helper');
		$this->load->helper('string');
		$this->load->model('Main_model');
		$this->load->model('General_model');
		is_logged_in();
    }

	

	//list orders
	public function index()
	{	
		if(!is_admin()){    
            redirect(base_url().('customer'));
            exit;
        }
		$data['page_title'] = 'OrderList';
		 $sql="select ord.user_id,ord.in_store_pickup,ord.order_id,ord.grand_total,
		 ord.o_status,ord.payment_status,ord.payment_method,ord.customer_name,ord.customer_phone,
		 ord.order_delivered_at,ord.delivery_status,u.full_name,u.id from sm_orders as ord
				join sm_users as u on u.id = ord.user_id  order by order_id desc ";
			
				 $data['query'] = $this->Main_model->__callMasterquery($sql);
		
					 
		$this->load->view('sm_orders',$data);
	}



	//order details
	public function details($id)
	{	
		if(!is_admin()){    
            redirect(base_url().('customer'));
            exit;
				}
				if(isset($_GET['notid'])) {
            $data=array(
                'seen_status'=>1
						);
						$notiid=$_GET['notid'];
						$wheres=array('module_id'=>$id,'noti_id'=>$notiid);
						$data = $this->Main_model->__callUpdate('sm_notifications',$wheres,$data);
								redirect(base_url().('orders/details/'.$id));
        }
			
		$data['page_title'] = 'Order Details';
		
		$sql="select * from sm_orders where order_id=$id ";

		$saladsql="select ord.order_id, ord.user_id, ord.total_price, ord.discount_amt, ord.tax_amt, ord.delivery_charge, 
		ord.grand_total, ord.payment_status, ord.payment_method, ord.order_notes, ord.in_store_pickup, ord.customer_name, ord.customer_phone, ord.pickup_date, ord.pickup_time, ord.o_status, ord.address_id,ord.promo_id, ord.transaction_id, 
		ord.delivery_id, ord.delivery_status, ord.create_date, ord.created_at, ord.order_delivered_at, dor.odetail_id, dor.order_id, dor.salad_id
		,dor.salad_name, dor.cat_name, dor.cat_name_arabic, dor.salad_name_arabic, dor.salad_img, dor.quantity, 
		dor.dressing_mixed_in, dor.item_total_price, dor.item_total_calories, dor.salad_type
		from sm_orders as ord join sm_order_details as dor on ord.order_id = dor.order_id where ord.order_id=$id";

		$data['id']=$id;
		$data['query'] = $this->Main_model->__callMasterquery($sql);
		 $data['salad_query'] = $this->Main_model->__callMasterquery($saladsql);
	    if($data['query']->num_rows() > 0)
	    {
			$row = $data['query']->row();
			$userid=$row->user_id;
			$usersql = "select id,full_name from sm_users where id=$userid";
			$data['users'] = $this->Main_model->__callMasterquery($usersql);

			//Delivery Status from our DB
			if($row->delivery_status == 1)
				$data['delivery_status'] = 'Created';
			else if($row->delivery_status == 2)
				$data['delivery_status'] = 'Pending Pickup';
			else if($row->delivery_status == 3)
				$data['delivery_status'] = 'Picked';
			else if($row->delivery_status == 4)
				$data['delivery_status'] = 'Completed';
			else if($row->delivery_status == 5)
				$data['delivery_status'] = 'Canceled';
			else
				$data['delivery_status'] = 'Not Available';


			//Get the Delivery API Response of the order
            /*$data_array =  array(
                "username"=>"saladmania",
                "password"=>"salad@mania321",
                "integrator_number" => encryptId($id)
            );
            
            $make_call = $this->callAPI('POST', 'https://web.utracadmin.net/parse/functions/getOrderStatusByIntegratorNumber', json_encode($data_array));
            $response = json_decode($make_call, true);
            //echo "<pre>"; print_r($response);  exit;

            if($response){
                if(isset($response['result']['status']))
                    $data['delivery_status'] = $response['result']['status'];
                else
                    $data['delivery_status'] = 'Not Available';
            }*/
		}
		
		$this->load->view('sm_orders_details',$data);
	}
	

	//order change status
	public function change_status()
	{
		if(isset($_POST['submit'])){
			$id = $this->input->post('order_id');
			$status = $this->input->post('status');

			//GET ORDER DETAILS
			$this->db->select('');
			$this->db->from('sm_orders');
			$this->db->where('order_id',$id);
			$query=$this->db->get();
			$order_data=$query->row();
			//echo "<pre>"; print_r($order_data);exit;
			if($order_data){

				if($status == 2){
					//CAll the customer address, in case of delivery order
					if($order_data->in_store_pickup == 0)
					{
						$table_name = 'sm_addresses';
						$select_filds='';
						$where_condition=array(
							'add_id'=>$order_data->address_id
						);
						$address=$this->General_model->get_row($table_name,$select_filds,$where_condition);

						$reciver_location=array(
							'latitude'=> floatval($address->latitude),
							'longitude'=> floatval($address->longitude)
						);

						$payment_method="";
						if($order_data->payment_method =='COD'){
							$payment_method="Cash";
						}elseif ($order_data->payment_method =='online'){
							$payment_method="Online";
						}

						//Get app setting for the Restaurant info
						$sender_location = array();
						$sender_name = '';
						$sender_phone = '';
						
						$settings = getAppSettings();
        				if($settings){
        					//Restaurant Location
							$sender_location=array(
								'latitude'=> floatval($settings->latitude),
								'longitude'=> floatval($settings->longitude)
							);
							$sender_name = $settings->app_name;
							$sender_phone = $settings->contact_phone;
        				}

						//Crate data to send to Delivery API
						$data_array =  array(
							"username"=>"saladmania",
							/*"password"=>"salad@mania321", //staging
                            "price_id"=>"FBiq4KMNEY",*/
                            "password"=>"saladmania4321", //live
                            "price_id"=>"dX1Bj42hvX",
							"buyername"=>getSingleFieldDetail('id',$order_data->user_id,'full_name','sm_users'),
							"order_price"=> (int)$order_data->grand_total,
							"receiver_phone"=>getSingleFieldDetail('id',$order_data->user_id,'phone','sm_users'),
							"receiver_name"=>getSingleFieldDetail('id',$order_data->user_id,'full_name','sm_users'),
							"receiver_location"=>$reciver_location,
							"receiver_image"=>"https://s3.amazonaws.com/utrac-parser-server/823265829cc7c7939be1e6336fd84bdf_if_profle_1055000.png",
							"sender_name"=>$sender_name,
							"sender_phone"=>$sender_phone,
							"sender_location"=>$sender_location,
							"sender_image"=>base_url().'assets/frontend/img/logo_h.png',
							"integrator_number"=>encryptId($order_data->order_id),
							"receiver_city"=>"amman",
							"receiver_area"=>$address->addline1,
							"receiver_street"=>"",
							"receiver_building"=>"",
							"receiver_floor"=>"",
							"receiver_apartment"=>"126",
							"receiver_landmark"=>"khbp",
							"receiver_note"=>"test",
							"is_demo"=>false, //false on live orders
							"payment_method"=>$payment_method,
							"note"=>"testing",
							"change"=>50
						);

						//staging.utracadmin.net = staging server
						$make_call = $this->callAPI('POST', 'https://web.utracadmin.net/parse/functions/addIntegratorOrder', json_encode($data_array));
						$response = json_decode($make_call, true);
						//echo "<pre>"; print_r($response);exit;
						//$response['code'] == '10' = already created

						if($response['result']['orderId']){ 
							$update_status = true;
							$record_to_update = array(
								'o_status'=>$status,
								'delivery_id'=>$response['result']['orderId'],
								'delivery_status'=>1,
							);
							/*$wheres = array('order_id'=>$id);
							$data = $this->Main_model->__callUpdate('sm_orders',$wheres,$record_to_update);
							$this->session->set_flashdata('success_msg', 'Record is updated successfully');*/


							//Send email to user
							/*$this->db->select('');
							$this->db->from('sm_orders');
							$this->db->where('order_id',$id);
							$query=$this->db->get();
							$message['order'] = $query->row();

							$msg = $this->load->view('email/order_template', $message, TRUE);
							$userEmail = getSingleFieldDetail('id',$order_data->user_id,'email','sm_users');
							sendmail($userEmail,'','',$msg, 'Salad Mania - Order Status');*/
						}
						else{
							$this->session->set_flashdata('error_msg', 'Deliery API is not working. Please try again later.');
						}
					}
					else{
						$update_status = true;
						$record_to_update = array(
							'o_status'=>$status
						);
					}

					//Now update the order
					if(isset($update_status)){
						//Update the order status
						$wheres = array('order_id' => $id);
						$data = $this->Main_model->__callUpdate('sm_orders',$wheres,$record_to_update);
						$this->session->set_flashdata('success_msg', 'Record is updated successfully');

						//Send email & SMS to user
						$this->db->select('o.*, u.phone');
						$this->db->from('sm_orders as o');
						$this->db->join('sm_users as u', 'u.id = o.user_id', 'LEFT');
						$this->db->where('order_id',$id);
						$query=$this->db->get();
						$message['order'] = $order = $query->row();
						//echo "<pre>"; print_r($message['order']);exit;
						if(!empty($order))
						{
							$msg = $this->load->view('email/order_template', $message, TRUE);
							$userEmail = getSingleFieldDetail('id',$order_data->user_id,'email','sm_users');
							//$userEmail = 'wasim@coretechies.com';
							sendmail($userEmail,'','',$msg, 'Salad Mania - Order Status');

							//Send SMS Notification
			                $msg = "Your order has been successfully Accepted. You will get the delivery soon.";
			                SendSms($msg,$order->phone);
			            }
					}
				}
				elseif ($status == 6){
					$deliveryadd=array(
						'o_status'=>$status,
					);
					$wheres = array('order_id'=>$id);
					$data = $this->Main_model->__callUpdate('sm_orders',$wheres,$deliveryadd);
					$this->session->set_flashdata('success_msg', 'Record is updated successfully');

					//Send email to user
					$this->db->select('');
					$this->db->from('sm_orders');
					$this->db->where('order_id',$id);
					$query=$this->db->get();
					$message['order'] = $query->row();

					$msg = $this->load->view('email/order_template', $message, TRUE);
					$userEmail = getSingleFieldDetail('id',$order_data->user_id,'email','sm_users');
					//$userEmail = 'wasim@coretechies.com';
					sendmail($userEmail,'','',$msg, 'Salad Mania - Order Status');

					//Send email & SMS to user
					$this->db->select('o.*, u.phone');
					$this->db->from('sm_orders as o');
					$this->db->join('sm_users as u', 'u.id = o.user_id', 'LEFT');
					$this->db->where('order_id',$id);
					$query=$this->db->get();
					$message['order'] = $order = $query->row();
					if(!empty($order))
					{
						$msg = $this->load->view('email/order_template', $message, TRUE);
						$userEmail = getSingleFieldDetail('id',$order_data->user_id,'email','sm_users');
						//$userEmail = 'wasim@coretechies.com';
						sendmail($userEmail,'','',$msg, 'Salad Mania - Order Status');

						//Send SMS Notification
		                $msg = "Your order has been Cancelled. We apologise for the inconvenience.";
		                SendSms($msg,$order->phone);
		            }
				}
				else{
					$this->session->set_flashdata('error_msg', 'Some things went wrong. Please Try again');
				}
			}else{
				$this->session->set_flashdata('error_msg', 'Some things went wrong. Please Try again');
			}
			//print_r($data);exit;
			//redirect(base_url().('orders/'));
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	private function callAPI($method, $url, $data){
		$curl = curl_init();

		switch ($method){
			case "POST":
				curl_setopt($curl, CURLOPT_POST, 1);
				if ($data)
					curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
				break;
			case "PUT":
				curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
				if ($data)
					curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
				break;
			default:
				if ($data)
					$url = sprintf("%s?%s", $url, http_build_query($data));
		}

		// OPTIONS:
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'X-Parse-Application-Id: 9803886e4a724d87bc56b44bd7512d3e2093bd95',
			'Content-Type: application/json',
		));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

		// EXECUTE:
		$result = curl_exec($curl);
		if(!$result){die("Connection Failure");}
		curl_close($curl);
		return $result;
	}
	

}




	