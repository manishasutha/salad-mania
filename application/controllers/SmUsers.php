<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SmUsers extends CI_Controller {
	public function __construct()
    {
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
			$this->load->helper('common_helper');
		$this->load->helper('string');
		$this->load->model('Main_model');
			$this->load->model('admin_model');
		is_logged_in();
    }

	//list orders
	public function index()
	{	
		if(!is_admin()){    
            redirect(base_url().('customer'));
            exit;
        }
        $data['page_title'] = 'User List';
		$sql="select id,full_name,phone,email,user_status,created_at,updated_at from sm_users order by id desc";
	

       	$data['query'] = $this->Main_model->__callMasterquery($sql);
		$this->load->view('sm_users',$data);
	}

	


	

	//delete category
		public function delete($id){
			 $deliveryadd=array(
            'user_status'=>0
            );
            $wheres = array('id'=>$id);
            $data = $this->Main_model->__callUpdate('sm_users',$wheres,$deliveryadd); 
	   		if($data){
	   			$this->session->set_flashdata('success_msg', 'User Disable Successfully');
	   			redirect(base_url()."users/");
	   		} 
		
	}
	//enable category
		public function enable($id){
			 $deliveryadd=array(
            'user_status'=>1
            );
            $wheres = array('id'=>$id);
            $data = $this->Main_model->__callUpdate('sm_users',$wheres,$deliveryadd); 
	   		if($data){
	   			$this->session->set_flashdata('success_msg', 'User Enable Successfully');
	   			redirect(base_url()."users/");
	   		} 
		
	}

	//order details
	public function details($id)
	{	
		if(!is_admin()){    
            redirect(base_url().('customer'));
            exit;
        }
		$data['page_title'] = 'User Details';
		
		$sql="select id,full_name,phone,email,user_status,created_at from sm_users where id=$id";
		$addsql="select sm_addresses.addline1,sm_addresses.addline2,sm_addresses.add_type,sm_addresses.city_id,c.city_id,c.city from sm_addresses  join sm_cities as c on sm_addresses.city_id=c.city_id where sm_addresses.user_id=$id";
		$ordersql="select order_id,user_id,grand_total	,o_status,payment_status,payment_method	,order_notes,in_store_pickup,created_at,order_delivered_at from sm_orders where user_id=$id order by order_id desc";
		   $data['query'] = $this->Main_model->__callMasterquery($sql);
			$data['address'] = $this->Main_model->__callMasterquery($addsql);
			$data['orders'] = $this->Main_model->__callMasterquery($ordersql);
		
		$this->load->view('sm_user_details',$data);
	}
	
public function getById(){
		if($this->input->get('id')){
			$user_id = $this->input->get('id');
			$sql = "select id,full_name,phone,email,user_status,created_at,updated_at from sm_users where  id= $user_id AND user_status=1";
			$result = $this->Main_model->__callMasterquery($sql);
			if($result){
				foreach($result->result() as $user){
				$response['error'] = 0;
				$response['full_name'] = $user->full_name;
				$response['email'] = $user->email;
                $response['phone'] = $user->phone;
				// $response['ing_image'] = $ing->ing_image;
			
				}
			}else{
				$response['error'] = 1;
				$response['msg'] = 'Some Things Went Wrongs.';
			}
			
			print_r(json_encode($response));
			exit;
		}

	}

	//edit submit
	public function edit($id){
			 if(isset($_POST['submit'])){
		 
		 if(empty($this->input->post('name'))){
			$response['error'] = 1;
			$response['msg'] = 'Please enter user name';
			//$this->session->set_flashdata('error_msg', $msg);
				print_r(json_encode($response));
			exit;
		}else if(empty($this->input->post('email'))){
			$response['error'] = 1;
			$response['msg'] = 'Please enter email';
			//$this->session->set_flashdata('error_msg', $msg);
				print_r(json_encode($response));
			exit;
		}else if(empty($this->input->post('phone'))){
			$response['error'] = 1;
			$response['msg'] = 'Phone must not be empty';
			//$this->session->set_flashdata('error_msg', $msg);
				print_r(json_encode($response));
			exit;
		}else
			$user_name = $this->input->post('name');
			$email = $this->input->post('email');
            $phone = $this->input->post('phone');
			
			
			$data = array(
					'full_name'	=> $user_name,
					'email'	=> $email,
                    'phone' 	=> $phone,
					'updated_at' => date('Y-m-d h:i:s'),
					//'created_by'=> $this->session->userdata('user_id'),
					// 'last_update_by'=> $this->session->userdata('user_id')
			);
		 $where = array('id'=>$id);
			$result = $this->Main_model->__callUpdate('sm_users',$where,$data);
			if($result){
				$response['error'] = 0;
				$response['msg'] = 'User Information Updated Successfully';
				$this->session->set_flashdata('success_msg', ' User Information Updated Successfully');
				//$response['msg'] = $this->error_msg_html('Category added Successfully.',0);
			}else{
				$response['error'] = 1;
				$response['msg'] = 'some things went rong.';
				//$response['msg'] = $this->error_msg_html('Some things went wrong.Please Try again..',0);
			}
			 redirect(base_url().('users/'));
		

	}
}
  

	//add submit
	public function add(){
		 if(isset($_POST['submit'])){
				
		  if(empty($this->input->post('name'))){
			$response['error'] = 1;
			$response['msg'] = 'Please enter user name';
			//$this->session->set_flashdata('error_msg', $msg);
				print_r(json_encode($response));
			exit;
		}else if(empty($this->input->post('email'))){
			$response['error'] = 1;
			$response['msg'] = 'Please enter email';
			//$this->session->set_flashdata('error_msg', $msg);
				print_r(json_encode($response));
			exit;
		}else if(empty($this->input->post('phone'))){
			$response['error'] = 1;
			$response['msg'] = 'Phone must not be empty';
			//$this->session->set_flashdata('error_msg', $msg);
				print_r(json_encode($response));
			exit;
        }
       else
			$user_name = $this->input->post('name');
			$email = $this->input->post('email');
            $phone = $this->input->post('phone');
			$password = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567980'), 0,8);
				$pass= encryptPassword($password);
			
			// 	$wheres = array('ing_name'=>$ing_name);
			//    $catwheres = array('cat_id'=>$cat_name);
			// if($this->Main_model->check_category_for_edit('sm_ingredients',$wheres,$catwheres)){
            //     $response['error'] = 1;
            //     $response['msg'] = ' Ingredient is already exits.';
            //     print_r(json_encode($response));
            //    $this->session->set_flashdata('error_msg', ' Sorry ingredient alerady exist');
			// 	 redirect(base_url().('ingredient/'));
            //}

			$sql = "select id from sm_users where phone = '$phone' OR email = '$email'";
			$exist = $this->Main_model->__callMasterquery($sql);

			if($exist->num_rows() == 0)
			{
				$data = array(
						'full_name'=> $user_name,
						'email'	=> $email,
	                    'phone' 	=> $phone,
						'updated_at' => date('y-m-d h:i:s'),
						'created_at'=> date('y-m-d h:i:s'),
						'user_type'=> 'U',
						'user_status'=>1,
						'password'=>$pass
				);
				
				$result = $this->Main_model->__callInsert('sm_users',$data);
				if($result){
					$email = $this->input->post('email');
						$to = $email;
	        		//$to = 'backup.rkgupta@gmail.com';
	        		$from = 'rupali@coretechies.com';
	        		$subject = 'New Registration';
	        		$token = random_string('alnum',20);
	    			$data['link'] = base_url().'auth/resetPassword?email='.$email.'&token='.$token;
					$data['email']= $this->input->post('email');
					$password = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567980'), 0,8);
					$data['pass']= $password;
					 $html  = $this->load->view('email/new_register',$data,true);
					sendmail($to,'','',$html, $subject);
					$msg = 'Email has send successfully to user';
					$this->session->set_flashdata('success_msg', $msg);
					 redirect(base_url().('users/'));
					$response['error'] = 0;
					$response['msg'] = 'User Information Updated Successfully';
					$this->session->set_flashdata('success_msg', ' User Information Updated Successfully');
					//$response['msg'] = $this->error_msg_html('Category added Successfully.',0);
				}else{
					$response['error'] = 1;
					$response['msg'] = 'some things went rong.';
					$this->session->set_flashdata('error_msg', 'Some things went wrong.Please Try again');
					//$response['msg'] = $this->error_msg_html('Some things went wrong.Please Try again..',0);
				}
			}
			else{
				$this->session->set_flashdata('error_msg', 'Phone Number OR Email is already exist.');
			}
			redirect(base_url().('users/'));
	}
 
}

public function send_email($to, $from, $subject,$html){
		/*$config = Array(        
            'protocol' => SMTP,
            'smtp_host' => SMTP_HOST,
            'smtp_port' => SMTP_PORT,
            'smtp_user' => SMTP_USER,
            'smtp_pass' => SMTP_PASSWORD,
            'smtp_timeout' => '8',
            'mailtype'  => 'html', 
            'charset'   => 'utf-8',
            'starttls'  => true,
            'wordwrap' => TRUE
        );*/
        $email_setting = $this->admin_model->get_email_setting();
        if(isset($email_setting) && !empty($email_setting)){
        	$config = Array(        
	            'protocol' => $email_setting[0]['type'],
	            'smtp_host' => $email_setting[0]['smtp_host'],
	            'smtp_port' => $email_setting[0]['smtp_port'],
	            'smtp_user' => $email_setting[0]['smtp_user'],
	            'smtp_pass' => $email_setting[0]['smtp_password'],
	            'smtp_timeout' => '8',
	            'mailtype'  => 'html', 
	            'charset'   => 'utf-8',
	            'starttls'  => true,
         	); 
        }else{
        	$config = Array(        
	            'protocol' => SMTP,
	            'smtp_host' => SMTP_HOST,
	            'smtp_port' => SMTP_PORT,
	            'smtp_user' => SMTP_USER,
	            'smtp_pass' => SMTP_PASSWORD,
	            'smtp_timeout' => '8',
	            'mailtype'  => 'html', 
	            'charset'   => 'utf-8',
	            'starttls'  => true,
	            'wordwrap' => TRUE
        	);
        } 
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('commerciale@bee-o-veg.com', 'Salad-Mania');
        $this->email->to($to);  
        $this->email->subject($subject);
        $this->email->message($html);
        if($this->email->send()){
        	return true;
        }else{
        	return false;
        }        
	}
}



	