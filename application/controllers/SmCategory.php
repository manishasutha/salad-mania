<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SmCategory extends CI_Controller {

	public function __construct(){
            parent::__construct();
            $this->load->helper('form');
            $this->load->library('form_validation');
			$this->load->model('Main_model');
			$this->load->model('General_model');
			 $this->load->database();
        	$this->load->library('encryption');
            is_logged_in();
    }

	//list category
	public function index()
	{	
		if(!is_admin()){    
            redirect(base_url().('customer'));
            exit;
        }
		$table_name='sm_categories';
		$select_filds='';
		$data['categoreis']=$this->General_model->get_multiple_row($table_name, $select_filds);
		$data['page_title']='Category';
		$this->load->view('sm_category',$data);
	}

	//list sub-category
	public function list_subcat()
	{	
		if(!is_admin()){    
            redirect(base_url().('customer'));
            exit;
        }
		$this->db->select('sc.subcat_name,sc.id,sc.subcat_name_arabic,sc.parent_cat_id,c.cat_name,c.cat_id,sc.salad_type,sc.subcat_status');
        $this->db->from('sm_subcategories as sc');
		$this->db->join('sm_categories as c',' c.cat_id=sc.parent_cat_id');
		$this->db->order_by('sc.id','desc');
        $query=$this->db->get();
		$data['subCategories']=$query->result();
		$data['page_title']='SubCategory';
		$this->load->view('sm_sub_category',$data);
	}

	//disable category
	public function disable($id){
		if($id){
			 $delete_id=$id;
			 $deliveryadd=array(
                'subcat_status'=>0
            );
            $wheres = array('id'=>$delete_id);
            $data = $this->Main_model->__callUpdate('sm_subcategories',$wheres,$deliveryadd); 
	   		if($data){

	   			//update subcat last update time in sm_update_timing
				$time_data=array(
					'subcat_update_time'=>date('Y-m-d H:i:s'),
					'salad_update_time'=>date('Y-m-d H:i:s')
				);
				$time_where=array('id'=>1);
				$this->Main_model->__callUpdate('sm_update_timing',$time_where,$time_data);

	   			$this->session->set_flashdata('success_msg', 'SubCategory Disabled Successfully');
	   			redirect(base_url()."sub-category/");
	   		} 
		}

	}

	//enable category
	public function enable($id){
		if($id){
		
			 $delete_id=$id;
			 $deliveryadd=array(
                'subcat_status'=>1
            );
            $wheres = array('id'=>$delete_id);
            $data = $this->Main_model->__callUpdate('sm_subcategories',$wheres,$deliveryadd); 
	   		if($data){

	   			//update subcat last update time in sm_update_timing
				$time_data=array(
					'subcat_update_time'=>date('Y-m-d H:i:s'),
					'salad_update_time'=>date('Y-m-d H:i:s')
				);
				$time_where=array('id'=>1);
				$this->Main_model->__callUpdate('sm_update_timing',$time_where,$time_data);
				
	   			$this->session->set_flashdata('success_msg', 'SubCategory Enable Successfully');
	   			redirect(base_url()."sub-category/");
	   		} 
		}
	}


	//insert category
	public function add(){
		 if(isset($_POST['submit'])){
			if(empty($this->input->post('sub_cat_name'))){
				$response['error'] = 1;
				$response['msg'] = 'Please enter name';
				print_r(json_encode($response));
				exit;
			}else if(empty($this->input->post('sub_cat_name_arabic'))){
				$response['error'] = 1;
				$response['msg'] = 'please enter arabic name';
				print_r(json_encode($response));
				exit;
			}else if(empty($this->input->post('parent_cat_id'))){
				$response['error'] = 1;
				$response['msg'] = 'please select category';
				print_r(json_encode($response));
				exit;
			}else if(empty($this->input->post('salad_type'))){
				$response['error'] = 1;
				$response['msg'] = 'please select salad type';
				print_r(json_encode($response));
				exit;
			}else if(empty($this->input->post('max_limit'))){
				$response['error'] = 1;
				$response['msg'] = 'please enter Maximit';
				print_r(json_encode($response));
				exit;
			}
			else{
				$name 		 = $this->input->post('sub_cat_name');
				$name_arabic = $this->input->post('sub_cat_name_arabic');
				$cat = $this->input->post('parent_cat_id');
				$salad_type = $this->input->post('salad_type');
				$max_limit=$this->input->post('max_limit');
				
				$data = array(
						'subcat_name'			=> $name,
						'subcat_name_arabic'	=> $name_arabic,
						'max_limit'			=> $max_limit,
						'parent_cat_id' 	=> $cat,
						'salad_type'=>$salad_type,
						'created_at'	=> date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s'),
				);
				
				$result = $this->Main_model->__callInsert('sm_subcategories',$data);
				//update subcat last update time in sm_update_timing
				$time_data=array(
					'subcat_update_time'=>date('Y-m-d H:i:s'),
					'salad_update_time'=>date('Y-m-d H:i:s')
				);
				$time_where=array(
					'id'=>1
				);
				$result = $this->Main_model->__callUpdate('sm_update_timing',$time_where,$time_data);
				if($result){
					// $response['error'] = 0;
					// $response['msg'] = 'SubCategory inserted Successfully';
					$this->session->set_flashdata('success_msg', ' SubCategory inserted Successfully');
					//$response['msg'] = $this->error_msg_html('Category added Successfully.',0);
				}else{
					// $response['error'] = 1;
					// $response['msg'] = 'some things went rong.';
					$this->session->set_flashdata('error_msg', ' SubCategory inserted Successfully');
					//$response['msg'] = $this->error_msg_html('Some things went wrong.Please Try again..',0);
				}
				redirect(base_url().('sub-category/'));
			}
		}
	}


	//get edit model data from db
	public function getById(){
		if($this->input->get('id')){
			$cat_id = $this->input->get('id');
			$sql = "select subcat_name,subcat_name_arabic,parent_cat_id,salad_type,max_limit from sm_subcategories where id= $cat_id AND subcat_status=1";
			$result = $this->Main_model->__callMasterquery($sql);
			if($result){
				foreach($result->result() as $cat){
				$response['error'] = 0;
				$response['subcat_name'] = $cat->subcat_name;
				$response['subcat_name_arabic'] = $cat->subcat_name_arabic;
				$response['parent_cat_id'] = $cat->parent_cat_id;
				$response['salad_type'] = $cat->salad_type;
				$response['max_limit'] = $cat->max_limit;
				}
			}else{
				$response['error'] = 1;
				$response['msg'] = 'Some Things Went Wrongs.';
			}
			
			print_r(json_encode($response));
			exit;
		}

	}
	
	//Update category
	public function edit(){
		 if(isset($_POST['update'])){
			if(empty($this->input->post('sub_cat_name'))){
				$response['error'] = 1;
				$response['msg'] = 'Please enter name';
				print_r(json_encode($response));
				exit;
			}else if(empty($this->input->post('sub_cat_name_arabic'))){
				$response['error'] = 1;
				$response['msg'] = 'please enter arabic name';
				print_r(json_encode($response));
				exit;
			}else if(empty($this->input->post('parent_cat_id'))){
				$response['error'] = 1;
				$response['msg'] = 'please select category';
				print_r(json_encode($response));
				exit;
			}else if(empty($this->input->post('salad_type'))){
				$response['error'] = 1;
				$response['msg'] = 'please select salad type';
				print_r(json_encode($response));
				exit;
			}else if(empty($this->input->post('max_limit'))){
				$response['error'] = 1;
				$response['msg'] = 'please enter Max Limit';
				print_r(json_encode($response));
				exit;
			}
			else{
				$name 		 = $this->input->post('sub_cat_name');
				$max_limit 		 = $this->input->post('max_limit');
				$name_arabic = $this->input->post('sub_cat_name_arabic');
				$cat = $this->input->post('parent_cat_id');
				$salad_type = $this->input->post('salad_type');
				$sub_cat_id = $this->input->post('sub_cat_id');

				$data = array(
						'subcat_name'			=> $name,
						'subcat_name_arabic'	=> $name_arabic,
						'max_limit' 	=> $max_limit,
						'parent_cat_id' 	=> $cat,
						'salad_type'=>$salad_type,
						'updated_at' => date('Y-m-d H:i:s'),
				);
				
				$where = array('id'=>$sub_cat_id);
				$result = $this->Main_model->__callUpdate('sm_subcategories',$where,$data);
				//update subcat last update time in sm_update_timing
				$time_data=array(
					'subcat_update_time'=>date('Y-m-d H:i:s'),
					'salad_update_time'=>date('Y-m-d H:i:s')
				);
				$time_where=array(
					'id'=>1
				);
				$result = $this->Main_model->__callUpdate('sm_update_timing',$time_where,$time_data);

				if($result){
					$response['error'] = 0;
					$response['msg'] = 'SubCategory Updated Successfully';
					$this->session->set_flashdata('success_msg', ' SubCategory Updated Successfully');
					//$response['msg'] = $this->error_msg_html('Category added Successfully.',0);
				}else{
					$response['error'] = 1;
					$response['msg'] = 'some things went rong.';
					//$response['msg'] = $this->error_msg_html('Some things went wrong.Please Try again..',0);
				}
				redirect(base_url().('sub-category/'));
			}
		}
	}
	
		

	//check category is unique or not in add category form
	public function check_add_category(){
		if(!empty($_POST['sub_cat_id'])){
			$where=array(
				'subcat_name'=>$_POST['sub_cat_name'],
				'parent_cat_id'=>$_POST['parent_cat_id'],
				'salad_type'=>$_POST['salad_type']
			);
			$this->db->select('id');
			$this->db->from('sm_subcategories');
			$this->db->where($where);
			$this->db->where('id != ',$_POST['sub_cat_id'],FALSE);
			$query=$this->db->get();
			$if_exist=$query->row();
			if($if_exist){
				$data= false;
			}else{
				$data= true;
			}
		}else{
			$where=array(
				'subcat_name'=>$_POST['sub_cat_name'],
				'parent_cat_id'=>$_POST['parent_cat_id'],
				'salad_type'=>$_POST['salad_type']
			);
			$this->db->select('id');
			$this->db->from('sm_subcategories');
			$this->db->where($where);
			$query=$this->db->get();
			$if_exist=$query->row();
			if($if_exist){
				$data= false;
			}else{
				$data= true;
			}
		}

		echo json_encode($data);
	}

	//get category create own
	public function getcategoryown(){
		$where=array(
		'contains_make_own' => 1
		);
		$this->db->select('');
		$this->db->from('sm_categories');
		$this->db->where($where);
		$query=$this->db->get();
		$result_data=$query->result();
		echo json_encode(array("msg_status" => 1, "msg" => 'Successfully Get', 'data' => $result_data));
	}
	//get category custom
	public function getcategorypre(){
		$this->db->select('');
		$this->db->from('sm_categories');
		$query=$this->db->get();
		$result_data=$query->result();
		echo json_encode(array("msg_status" => 1, "msg" => 'Successfully Get', 'data' => $result_data));
	}
	
}
