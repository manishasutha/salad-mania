<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct()
    {
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('profile_model');
		$this->load->model('General_model');
		$this->load->model('api_model');
		$this->load->model('admin_model');
		$this->load->helper('string');
    }

	public function index()
	{	
		$user = $this->session->userdata('user_id');
		$user_role = $this->session->userdata('user_role');
		if(!empty($user)){
			if($user_role == 1){
				$redirect_path = 'admin/';
			}else if($user_role == 2){
				$redirect_path = 'customer/';
			}else if($user_role == 3){
				$redirect_path = 'user/';
			}
			redirect(base_url().$redirect_path);
		}else{
			$data['page_title'] = 'Login';
			$this->load->view('login',$data);
		}
	}
	public function forgetPassword(){
		$data['page_title']= 'Forget Password';
		$this->load->view('forget_password',$data);
	}
	public function appUserPage(){
		$data['page_title']= 'App user';
		$this->load->view('app_user_page',$data);
	}
	public function forgetPasswordAction(){
		$email = $this->input->post('email');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == FALSE){
        	//$error = validation_errors();
        	$data['page_title']= 'Forget password';
			$this->load->view('forget_password',$data);
        }else{
        	if(!$this->profile_model->checkEmail($email)){
				$msg = $this->lang->line('email_not_register');
				$this->session->set_flashdata('error_msg', $msg);
				redirect(base_url().'auth/forgetPassword'); 		
        	}else{
        		$to = $email;
				$where=array(
					'email_address'=>$email,
				);
				$if_exist=$this->General_model->get_row('sm_admin','',$where);
				if($if_exist){
					$message['password'] = dycryptPassword($if_exist->password);
					$message['email'] =  $email;
					$subj = 'Forget password';
					$msg  = $this->load->view('email/reset_password',$message,TRUE);
					$status = sendmail($to,'','',$msg, 'Salad Mania : '.$subj);
					if($status){
						$this->session->set_flashdata('success_msg', 'Email has been sent. Please check your email');
					}
					else {
						$this->session->set_flashdata('success_msg', 'Something went wrong');
					}
				}


				redirect(base_url().'auth/forgetPassword');	
        	}
        }
	}
	public function resetPassword(){
		$data['page_title']= 'Reset password';
		if(!empty($this->input->get('token')) && !empty($this->input->get('email'))){
			$data['token'] = $this->input->get('token');
			$data['email'] = $this->input->get('email');
			$data['type'] = $this->input->get('type');
			if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'app_user'){
				$user_token = $this->api_model->get_token($data['email']);
			}else{
				$user_token = $this->profile_model->get_token($data['email']);
			}
			if($user_token == $data['token']){
				$this->load->view('reset_password',$data);
			}else{
				$msg = 'Sorry, your token have expired';
				$this->session->set_flashdata('error_msg', $msg);
				if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'app_user'){
					redirect(base_url().'auth/appUserPage');
					exit; 
				}else{
					redirect(base_url().'auth/forgetPassword');
					exit; 
				}
			}
		}
	}
	public function resetPasswordAction(){
		$token = $this->input->post('reset_token');
		$email = $this->input->post('reset_email');
		$password = $this->input->post('password');
		$type = $this->input->post('type');
		if(isset($type) && !empty($type) && $type == 'app_user'){
			$user_token = $this->api_model->get_token($email);
		}else{
			$user_token = $this->profile_model->get_token($email);
		}
		if($token == $user_token){
			if(isset($type) && !empty($type) && $type == 'app_user'){
				$this->db->set('password', md5($password));
				$this->db->set('token', '');
				$this->db->set('last_updated_date', date('y-m-d h:i:sa'));
	    		$this->db->where('email_id', $email);
	    		$this->db->update('app_users');
			}else{
				$this->db->set('password', md5($password));
				$this->db->set('token', '');
				$this->db->set('last_updated_date', date('y-m-d h:i:sa'));
	    		$this->db->where('email_address', $email);
	    		$this->db->update('sm_admin');
    		}
    		$msg = $this->lang->line('pass_update_success');
			$this->session->set_flashdata('success_msg', $msg);
			if(isset($type) && !empty($type) && $type == 'app_user'){
				redirect(base_url().'auth/appUserPage');
				exit; 
			}else{
				redirect(base_url().'auth/'); 
				exit; 
			}
		}else{
			$msg = $this->lang->line('token_expired');
			$this->session->set_flashdata('error_msg', $msg);
			if(isset($type) && !empty($type) && $type == 'app_user'){
				redirect(base_url().'auth/appUserPage');
				exit; 
			}else{
				redirect(base_url().'auth/forgetPassword'); 
				exit; 
			}
		}
	}
	public function loginAction(){
		//print_r($_POST);exit;
		$session_set_value = $this->session->all_userdata();
		if (isset($session_set_value['remember_me']) && $session_set_value['remember_me'] == "1") {
			//echo "redirect";exit;
			redirect(base_url().('admin/'));
		} else
		{
			$email=$this->input->post('email');
			$password=$this->input->post('password');
			$remember = $this->input->post('remember');
			if ($remember) {
				$this->session->set_userdata('remember_me', TRUE);
			}
			$this->session->set_flashdata('last_email', $email);

			if(!$email){
				$response['error'] = 1;
				$msg= $this->lang->line('email_require');
				$response['msg'] = 'Please Enter email';
				$this->session->set_flashdata('error_msg', $this->lang->line('email_require'));
				redirect(base_url().('auth/'));
				exit;
			}else if(!$password){
				$response['error'] = 1;
				$response['msg'] = 'Please Enter password';
				$msg = $this->lang->line('pass_require');
				$this->session->set_flashdata('error_msg', $this->lang->line('pass_require'));
				redirect(base_url().('auth/'));
				exit;

			}else{

				$result = $this->profile_model->login($email,$password);

				//
				if($result){

					/*if($result[0]['status'] == 0){
                        $response['error'] = 1;
                        $response['msg'] = $this->error_msg_html(' Your account is not active. Please contact to admin.',1);
                    print_r(json_encode($response));
                    exit;
                    }*/
					$response['error'] = 0;
					$response['user_role'] = $result[0]['fk_role_id'];
					$response['user_id'] = $result[0]['serial_number'];
					$newdata = array(
						'user_id'           => $result[0]['serial_number'],
						'user_role'         => $result[0]['fk_role_id'],
						'user_email'        => $result[0]['email_address'],
						'user_first_name'   => $result[0]['first_name'],
						'user_last_name'    => $result[0]['last_name'],
						'user_profile_image'=> $result[0]['profile_image'],
						'last_login'        => $result[0]['last_login'],
						'logged_in' => TRUE
					);
					$this->session->set_userdata($newdata);

				}else{

					$response['error'] = 1;
					$response['msg'] = 'Please Enter correct email and password';
					$msg= $this->lang->line('correct_email_pass');
					$this->session->set_flashdata('error_msg', $this->lang->line('correct_email_pass'));
					redirect(base_url().('auth/'));
					exit;
				}
				redirect(base_url().('admin/'));

			}
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url().'auth/', 'refresh');
	}
	function error_msg_html($msg,$status){
	    if($status){
        	$html = '<div class="alert alert-danger fade in">
	        <a href="#" class="close" data-dismiss="alert">&times;</a>
	        <strong>Error!</strong>'." ". $msg.'
	    	</div>';
	    }else{
	    	$html = '<div class="alert alert-success fade in">
        		<a href="#" class="close" data-dismiss="alert">&times;</a>
        		<strong>Success!</strong>'." ". $msg.'
        		</div>';
	    }
	    return $html;
	}
	public function send_email($to, $from, $subject,$html){
		/*$config = Array(        
            'protocol' => SMTP,
            'smtp_host' => SMTP_HOST,
            'smtp_port' => SMTP_PORT,
            'smtp_user' => SMTP_USER,
            'smtp_pass' => SMTP_PASSWORD,
            'smtp_timeout' => '8',
            'mailtype'  => 'html', 
            'charset'   => 'utf-8',
            'starttls'  => true,
            'wordwrap' => TRUE
        );*/
        $email_setting = $this->admin_model->get_email_setting();
        if(isset($email_setting) && !empty($email_setting)){
        	$config = Array(        
	            'protocol' => $email_setting[0]['type'],
	            'smtp_host' => $email_setting[0]['smtp_host'],
	            'smtp_port' => $email_setting[0]['smtp_port'],
	            'smtp_user' => $email_setting[0]['smtp_user'],
	            'smtp_pass' => $email_setting[0]['smtp_password'],
	            'smtp_timeout' => '8',
	            'mailtype'  => 'html', 
	            'charset'   => 'utf-8',
	            'starttls'  => true,
         	); 
        }else{
        	$config = Array(        
	            'protocol' => SMTP,
	            'smtp_host' => SMTP_HOST,
	            'smtp_port' => SMTP_PORT,
	            'smtp_user' => SMTP_USER,
	            'smtp_pass' => SMTP_PASSWORD,
	            'smtp_timeout' => '8',
	            'mailtype'  => 'html', 
	            'charset'   => 'utf-8',
	            'starttls'  => true,
	            'wordwrap' => TRUE
        	);
        } 
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('commerciale@bee-o-veg.com', 'Bee-O-Veg');
        $this->email->to($to);  
        $this->email->subject($subject);
        $this->email->message($html);
        if($this->email->send()){
        	return true;
        }else{
        	return false;
        }        
	}

}