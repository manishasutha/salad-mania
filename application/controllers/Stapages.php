<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stapages extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
            $this->load->helper('common_helper');
            	$this->load->model('Main_model');
        is_logged_in();
        if(!is_admin()){    
            redirect(base_url().('customer'));
            exit;
        }
    }
    
     public function dashboard(){
      
        //Calling the Ingredients
        $data['total_users']=$this->Main_model->count_total('sm_users');
        $data['total_orders']=$this->Main_model->count_total('sm_orders');
        $data['total_salads']=$this->Main_model->count_total('sm_salads');
        $total_collection="select grand_total from sm_orders where  DATE(created_at) = DATE(NOW())";
       $data['collect'] = $this->Main_model->__callMasterquery($total_collection);

       $sql="select ord.user_id,ord.in_store_pickup,ord.order_id,ord.grand_total,
		 ord.o_status,ord.payment_status,ord.payment_method,ord.customer_name,ord.customer_phone,
		 ord.order_delivered_at,u.full_name,u.id from sm_orders as ord
				join sm_users as u on u.id = ord.user_id order by order_id desc limit 0,20 ";
			
		$data['query'] = $this->Main_model->__callMasterquery($sql);
        
    	$data['page_title'] = 'Dashboard';
		$data['page'] = 'static pages/dashboard';
		$this->load->view('content',$data);

    }
    //Add Salad Function
    public function about(){

        if(isset($_POST['submit'])){
         
            extract($_POST);
            
            //uploading Excel File
             $where = array('id'=>1);
            $data = array('about_page' => $description);        
            $last_id = $this->Main_model->__callUpdate('sm_app_settings',$where,$data);

            if($last_id){
                
                $this->session->set_flashdata('success_msg', 'About page  has been successfully updated.');
            }
            else
                $this->session->set_flashdata('error_msg', 'Sorry! Something went wrong.');
            
            redirect(base_url().'pages/about');
        }

        //Calling the Ingredients
        $sql="select about_page from sm_app_settings";
        $data['about']=$this->Main_model->__callMasterquery($sql);

    	$data['page_title'] = 'Add Salad';
		$data['page'] = 'static pages/about';
		$this->load->view('content',$data);

    }

   //Add Salad Function
    public function tc(){

        if(isset($_POST['submit'])){
            extract($_POST);
            
            //uploading Excel File
             $where = array('id'=>1);
            $data = array('tc_page' => $description,'tc_page_arabic' => $description_arabic);        
            $last_id = $this->Main_model->__callUpdate('sm_app_settings',$where,$data);

            //update settings last update time in sm_update_timing
            $time_data=array(
                'settings_update_time'=>date('Y-m-d H:i:s')
            );
            $time_where=array(
                'id'=>1
            );
            $last_id = $this->Main_model->__callUpdate('sm_update_timing',$time_where,$time_data);


            if($last_id){
                
                $this->session->set_flashdata('success_msg', 'Terms and Conditions has been successfully updated.');
            }
            else
                $this->session->set_flashdata('error_msg', 'Sorry! Something went wrong.');
            
            redirect(base_url().'pages/terms_and_condition');
        }
        //Calling the terms and condition data
        $sql="select tc_page,tc_page_arabic from sm_app_settings";
        $data['tc']=$this->Main_model->__callMasterquery($sql);

    	$data['page_title'] = 'Terms and Conditions';
		$data['page'] = 'static pages/termsandconditions';
		$this->load->view('content',$data);

    }

//Add Salad Function
    public function privacy(){

        if(isset($_POST['submit'])){
            extract($_POST);
            
            //uploading Excel File
             $where = array('id'=>1);
            $data = array('privacy_page' => $description,'privacy_page_arabic'=>$description_arabic);        
            $last_id = $this->Main_model->__callUpdate('sm_app_settings',$where,$data);
            
            //update settings last update time in sm_update_timing
            $time_data=array(
                'settings_update_time'=>date('Y-m-d H:i:s')
            );
            $time_where=array(
                'id'=>1
            );
            $last_id = $this->Main_model->__callUpdate('sm_update_timing',$time_where,$time_data);

            if($last_id){
                
                $this->session->set_flashdata('success_msg', 'Privacy and policy has been successfully updated.');
            }
            else
                $this->session->set_flashdata('error_msg', 'Sorry! Something went wrong.');
            
            redirect(base_url().'pages/privacy');
        }

        //Calling the privacy data
        $sql="select privacy_page,privacy_page_arabic from sm_app_settings";
        $data['privacy']=$this->Main_model->__callMasterquery($sql);

    	$data['page_title'] = 'Privacy and Policy';
		$data['page'] = 'static pages/privacy';
		$this->load->view('content',$data);

    }
    
    //Add Setings Function
    public function settings(){
        if(isset($_POST['submit'])){

            extract($_POST);
            
            //uploading image File
             /*$file_details = array('upload_path'=> "./uploads/logo/", 'allowed_types' => "jpg|jpeg|gif|png|svg", 'max_size'=>"50000",'max_width'=>"50000", 'max_height'=>"50000", 'filename'=>"app_logo");
            $uploadedFile = $this->uploadfile($file_details);
             
             if($uploadedFile){
            $data = array('app_name'=>$app_name,'address'=>$address,'monday_friday_hrs'=>$monday_friday_hrs,'saturday_hrs'=>$saturday_hrs,'sunday_hrs'=>$sunday_hrs,'app_logo'=>$uploadedFile,'contact_email'=>$contact_email,'contact_phone'=>$contact_phone,'tax_rate'=>$tax_rate,'delivery_charge'=>$delivery_charge,'fb_url'=>$fb_url,'twitter_url'=>$twitter_url,'gplus_url'=>$gplus_url,'insta_url'=>$insta_url,'linkedin_url'=>$linkedin_url,'about_us_link'=>$about_us_link,'our_story_link'=>$our_story_link);
              }
               else{*/

              //}

             $where = array('id'=>1);
             $data = array('app_name'=>$app_name,'address'=>$address,'monday_friday_hrs'=>$monday_friday_hrs,'saturday_hrs'=>$saturday_hrs,'sunday_hrs'=>$sunday_hrs,'diet_email'=>$diet_email, 'contact_email'=>$contact_email,'contact_phone'=>$contact_phone,'tax_rate'=>$tax_rate,'delivery_charge'=>$delivery_charge,'fb_url'=>$fb_url,'twitter_url'=>$twitter_url,'gplus_url'=>$gplus_url,'insta_url'=>$insta_url,'linkedin_url'=>$linkedin_url, 'our_story_link'=>$our_story_link);
              
              $last_id = $this->Main_model->__callUpdate('sm_app_settings',  $where,$data);

               //update settings last update time in sm_update_timing
            $time_data=array(
                'settings_update_time'=>date('Y-m-d H:i:s')
            );
            $time_where=array(
                'id'=>1
            );
            $last_id = $this->Main_model->__callUpdate('sm_update_timing',$time_where,$time_data);

            if($last_id){
                
                $this->session->set_flashdata('success_msg', 'Settings has been successfully updated.');
            }
            else
                $this->session->set_flashdata('error_msg', 'Sorry! Something went wrong.');
            
            redirect(base_url().'pages/settings');
        }

        //Calling the app settings data
        $sql="select * from sm_app_settings";
        $data['about']=$this->Main_model->__callMasterquery($sql);

    	$data['page_title'] = 'Settings';
		$data['page'] = 'static pages/settings';
		$this->load->view('content',$data);

    }

     //notification Function
    public function notification(){
        //Calling the Ingredients
        $sql="select  noti_id,noti_msg,module_id,noti_for,seen_status,created_at,updated_at from sm_notifications where noti_for='A'  order by noti_id desc";
        $data['notification']=$this->Main_model->__callMasterquery($sql);
    	$data['page_title'] = 'Notifications';
		$data['page'] = 'static pages/notification';
		$this->load->view('content',$data);
    }
    public function deleteing($id){
             $this->db->where('noti_id', $id);
          if($this->db->delete('sm_notifications')){
	   			$this->session->set_flashdata('success_msg', 'Notification deleted Successfully');
	   			redirect(base_url()."notification");
               } 
            //    if($id){
			
			
			//  $delete_id=$id;
			//  $deliveryadd=array(
            //     'seen_status'=>1
            // );
            // $wheres = array('noti_id'=>$delete_id);
            // $data = $this->Main_model->__callUpdate('sm_notifications',$wheres,$deliveryadd); 
	   		// if($data){
	   		// 	$this->session->set_flashdata('success_msg', 'Category deleted Successfully');
	   		// 	redirect(base_url()."notification");
	   		// } 
		//}
    }
     // Uploading file
    public function uploadfile($file_details = array())
    {  
        $file_name = $file_details['filename']; 
        $ext = pathinfo($_FILES[$file_name]['name'], PATHINFO_EXTENSION);
        $config['upload_path'] = $file_details['upload_path'];
        $config['allowed_types'] = $file_details['allowed_types'];
        $config['max_size']  = $file_details['max_size'];    
        $config['max_width']  = $file_details['max_width'];
        $config['max_height']  = $file_details['max_height'];
        $config['file_name']= md5((time()*rand())).".".$ext;

        //$file_name = 'group_photo';

        $this->load->library('Upload',$config);             
        $this->upload->initialize($config);

        $upload = $this->upload->do_upload($file_name);
        if($_FILES[$file_name]['size'] != 0)
        {
            if($upload == NULL)
            {
                $this->session->set_flashdata('error_msg', $this->upload->display_errors());
                redirect($_SERVER['HTTP_REFERER']);
                //$response_data = array("status" => 0,"msg" => $this->upload->display_errors(),"url" => '');
            }
            else{
                $file_data = $this->upload->data();    
                return $imgName = $file_data['file_name'];
            }
        }
    }
    public function load_graph(){
        if($_POST['time']=='Weekly' && $_POST['graph']=='user'){
            $sql1 ='select d.create_date, coalesce(count(id), 0) AS users 
            from (select DATE(curdate()) as create_date union all 
            select DATE(DATE_SUB( curdate(), INTERVAL 1 DAY)) union all 
            select DATE(DATE_SUB( curdate(), INTERVAL 2 DAY)) union all 
            select DATE(DATE_SUB( curdate(), INTERVAL 3 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 4 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 5 DAY)) union all
            select DATE(DATE_SUB( curdate(), INTERVAL 6 DAY))) d left outer join ';


            $sql = $sql1 . ' sm_users t on t.create_date = d.create_date and t.user_type =\'U\' and t.user_status = 1 and t.deleted_at IS NULL
            GROUP BY d.create_date 
            ORDER BY d.create_date asc';
            $query = $this->db->query($sql);
            $result = $query->result_array();

            $response=array(
                'status'=>true,
                'time'=>'week',
                'data'=>$result,
                'heading'=>'Registered Users',
                'side'=>'Users',
                'down'=>'Users'
            );
        }

        if($_POST['time']=='Weekly' && $_POST['graph']=='order'){
            $sql1 ='select d.create_date, coalesce(count(order_id), 0) AS users 
            from (select DATE(NOW()) as create_date union all 
            select DATE(DATE_SUB( NOW(), INTERVAL 1 DAY)) union all 
            select DATE(DATE_SUB( NOW(), INTERVAL 2 DAY)) union all 
            select DATE(DATE_SUB( NOW(), INTERVAL 3 DAY)) union all
            select DATE(DATE_SUB( NOW(), INTERVAL 4 DAY)) union all
            select DATE(DATE_SUB( NOW(), INTERVAL 5 DAY)) union all
            select DATE(DATE_SUB( NOW(), INTERVAL 6 DAY))) d left outer join ';


            $sql = $sql1 . ' sm_orders t on t.create_date = d.create_date where t.deleted_at IS NULL
            GROUP BY d.create_date 
            ORDER BY d.create_date asc';
            $query = $this->db->query($sql);
            $result = $query->result_array();

            $response=array(
                'status'=>true,
                'time'=>'week',
                'data'=>$result,
                'heading'=>'Total Orders',
                'side'=>'Orders',
                'down'=>'Orders'
            );
        }

        if($_POST['time']=='Monthly' && $_POST['graph']=='user'){
            $sql2 ='SELECT 
            SUM(IF(month = \'Jan\', total, 0)) AS \'Jan\',
            SUM(IF(month = \'Feb\', total, 0)) AS \'Feb\', 
            SUM(IF(month = \'Mar\', total, 0)) AS \'Mar\', 
            SUM(IF(month = \'Apr\', total, 0)) AS \'Apr\', 
            SUM(IF(month = \'May\', total, 0)) AS \'May\', 
            SUM(IF(month = \'Jun\', total, 0)) AS \'Jun\', 
            SUM(IF(month = \'Jul\', total, 0)) AS \'Jul\', 
            SUM(IF(month = \'Aug\', total, 0)) AS \'Aug\', 
            SUM(IF(month = \'Sep\', total, 0)) AS \'Sep\', 
            SUM(IF(month = \'Oct\', total, 0)) AS \'Oct\', 
            SUM(IF(month = \'Nov\', total, 0)) AS \'Nov\', 
            SUM(IF(month = \'Dec\', total, 0)) AS \'Dec\' 
            FROM ( 
            SELECT DATE_FORMAT(create_date, "%b") AS month, count(id) as total 
            FROM sm_users 
           ';

            $sql = $sql2 . ' WHERE user_type=\'U\' and deleted_at IS NULL and create_date <= NOW() and user_status = 1 and create_date >= Date_add(Now(),interval - 12 month) 
            GROUP BY DATE_FORMAT(create_date, "%m-%Y")) as sub';
            $query = $this->db->query($sql);
            $result = $query->result_array();
            if ($result[0]['Jan'] == null) {
                foreach ($result[0] as $key => $value) {
                    $result[0][$key] = "0";
                }
            }
            $response=array(
                'status'=>true,
                'time'=>'month',
                'data'=>$result,
                'heading'=>'Register Users',
                'side'=>'Users',
                'down'=>'Users'
            );
        }

        if($_POST['time']=='Monthly' && $_POST['graph']=='order'){
            $sql2 ='SELECT 
            SUM(IF(month = \'Jan\', total, 0)) AS \'Jan\',
            SUM(IF(month = \'Feb\', total, 0)) AS \'Feb\', 
            SUM(IF(month = \'Mar\', total, 0)) AS \'Mar\', 
            SUM(IF(month = \'Apr\', total, 0)) AS \'Apr\', 
            SUM(IF(month = \'May\', total, 0)) AS \'May\', 
            SUM(IF(month = \'Jun\', total, 0)) AS \'Jun\', 
            SUM(IF(month = \'Jul\', total, 0)) AS \'Jul\', 
            SUM(IF(month = \'Aug\', total, 0)) AS \'Aug\', 
            SUM(IF(month = \'Sep\', total, 0)) AS \'Sep\', 
            SUM(IF(month = \'Oct\', total, 0)) AS \'Oct\', 
            SUM(IF(month = \'Nov\', total, 0)) AS \'Nov\', 
            SUM(IF(month = \'Dec\', total, 0)) AS \'Dec\' 
            FROM ( 
            SELECT DATE_FORMAT(create_date, "%b") AS month, count(order_id) as total 
            FROM sm_orders 
           ';

            $sql = $sql2 . ' WHERE deleted_at IS NULL and create_date <= NOW() and create_date >= Date_add(Now(),interval - 12 month) 
            GROUP BY DATE_FORMAT(create_date, "%m-%Y")) as sub';
            $query = $this->db->query($sql);
            $result = $query->result_array();
            if ($result[0]['Jan'] == null) {
                foreach ($result[0] as $key => $value) {
                    $result[0][$key] = "0";
                }
            }
            $response=array(
                'status'=>true,
                'time'=>'month',
                'data'=>$result,
                'heading'=>'Total Orders',
                'side'=>'Orders',
                'down'=>'Orders'
            );
        }
        if($_POST['time']=='Yearly' && $_POST['graph']=='user'){
            $where=array(
                'user_type'=>'U',
                'deleted_at'=>NULL,
                'user_status'=>1
            );
            $this->db->select('create_date');
            $this->db->from('sm_users');
            $this->db->where($where);
            $query=$this->db->get();
            $result = $query->result_array();
            $year = [];
            $i = 0;
            foreach ($result as $res) {
                $timestam = strtotime($res['create_date']);
                $year[$i] = strtolower(date('Y', $timestam));
                $i++;
            }
            $data = array_map('strval', array_count_values($year));
            $response=array(
                'status'=>true,
                'time'=>'year',
                'data'=>$data,
                'heading'=>'Registered Users',
                'side'=>'Users',
                'down'=>'Users'
            );
        }
        if($_POST['time']=='Yearly' && $_POST['graph']=='order'){
            $where=array(
                'deleted_at'=>NULL
            );
            $this->db->select('create_date');
            $this->db->from('sm_orders');
            $this->db->where($where);
            $query=$this->db->get();
            $result = $query->result_array();
            $year = [];
            $i = 0;
            foreach ($result as $res) {
                $timestam = strtotime($res['create_date']);
                $year[$i] = strtolower(date('Y', $timestam));
                $i++;
            }
            $data = array_map('strval', array_count_values($year));
            $response=array(
                'status'=>true,
                'time'=>'year',
                'data'=>$data,
                'heading'=>'Total Orders',
                'side'=>'Orders',
                'down'=>'Orders'
            );
        }
        echo json_encode($response);
    }
    
    //add banner image
    public function add_banner(){
        if(isset($_POST['submit'])){
            extract($_POST);
            //uploading Image File
            $file_details = array('upload_path'=> "./uploads/banners/", 'allowed_types' => "jpg|jpeg|gif|png|svg", 'max_size'=>"50000",'max_width'=>"50000", 'max_height'=>"50000", 'filename'=>"Image");
            $uploadedFile = $this->uploadfile($file_details);

            $data = array('banner_name' => $uploadedFile);        
            $last_id = $this->Main_model->__callInsert('sm_banners',$data);

            if($last_id){
                $this->session->set_flashdata('success_msg', 'Banner image has been successfully inserted.');
            }
            else{
                $this->session->set_flashdata('error_msg', 'Sorry! Something went wrong.');
            }
            redirect(base_url().'pages/banner');
        }
    	$data['page_title'] = 'Add Website Banners';
		$data['page'] = 'static pages/add-banners';
		$this->load->view('content',$data);
    }

    //delete banner image
    public function delete($id = NULL){
        if($id != NULL)
        {
            //First get the image name and delete
            $option = array('where' => array('banner_id' => $id));
            $query = $this->Main_model->__callSelect('sm_banners', $option);
            if($query->num_rows() > 0)
            {
                $row = $query->row();
                $bannerImg = $row->banner_name;
                if(!empty($bannerImg)){
                    $imgPath = "./uploads/banners/{$bannerImg}";
                    unlink($imgPath);
                }

                $banner="delete from sm_banners where banner_id=$id";
                $delete_banner = $this->Main_model->__callMasterquery($banner);
                if($delete_banner){
                    $this->session->set_flashdata('success_msg', 'Banner image has been successfully deleted.');
                }
                else{
                    $this->session->set_flashdata('error_msg', 'Sorry! Something went wrong.');
                }
                redirect(base_url().'pages/banner');
            }
        }
    }

    //list banner images
    public function list_banner(){
        $sql="select * from sm_banners order by banner_id desc";
        $data['query']=$this->Main_model->__callMasterquery($sql);
    	$data['page_title'] = 'Add Website Banners';
		$data['page'] = 'static pages/banners';
		$this->load->view('content',$data);

    }
}
