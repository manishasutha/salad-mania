<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 
require_once APPPATH.'third_party/php-jwt/src/JWT.php';
require_once APPPATH.'third_party/php-jwt/src/BeforeValidException.php';
require_once APPPATH.'third_party/php-jwt/src/ExpiredException.php';
require_once APPPATH.'third_party/php-jwt/src/SignatureInvalidException.php';
use \Firebase\JWT\JWT;
require(APPPATH.'/libraries/REST_Controller.php');
header('Content-Type: application/json');
class Api extends REST_Controller {
	protected $access_token;
	protected $api_key;
	protected $login_user_id;
	protected $inputall;
	public function __construct(){
            parent::__construct();
            $this->load->helper('string');
            $this->load->model('api_model_v1');
            $this->load->model('rating_model');
            $this->load->model('customer_model');
            $this->load->model('category_model');
            $this->load->model('filter_model');
            $this->load->model('keyword_model');
            $this->load->model('menu_model');
            #It check token is valid or not.If token is not empty.
            $header = getallheaders();
            if(isset($header['access_token'])){
                $this->access_token =  $header['access_token'];
            }
            if(isset($header['api_key'])){
                $this->api_key =  $header['api_key'];
            }
            /*if(isset($this->access_token) && !empty($this->access_token)){
				$result = json_decode($this->verify_token($this->access_token));
				if($result->status == 'error'){
					$response['status'] = 'error';
					$response['message'] = $result->message;
					echo json_encode($response);
					exit;
				}else{
					$this->login_user_id = $result->user_id;
				}
			}*/
			# Get all request json data and assign it to variable.
            $json_data = file_get_contents('php://input');
            $valid_json = (json_last_error()===JSON_ERROR_NONE);
            if(!$valid_json){
            	$response['status'] = 'error';
				$response['message'] = 'Invalid data format.';
				echo json_encode($response);
				exit;
            }
		    $this->inputall = json_decode($json_data);
    }
    #This function is used for login app user
	public function login_post(){
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		if(!isset($this->inputall->login_type) || empty($this->inputall->login_type)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter login type.';
			echo json_encode($response);
			exit;
		}

		if(!isset($this->inputall->email_id) || empty($this->inputall->email_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter email  id.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->device_name) || empty($this->inputall->device_name)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter device name.';
			echo json_encode($response);
			exit;
		}
        $data['email'] = $this->inputall->email_id;
		$data['device_name'] = $this->inputall->device_name;
		$data['login_type'] = $this->inputall->login_type;
		if($this->inputall->login_type == 'Email' || $this->inputall->login_type == 'email'){
			if(!isset($this->inputall->password) || empty($this->inputall->password)){
				$response['status'] = 'error';
				$response['message'] = 'Please enter password.';
				echo json_encode($response);
				exit;
			}else{
				$data['password'] = $this->inputall->password;

			}
		}
		if(!$this->api_model_v1->checkEmail($data['email'])){
			$response['status'] = 'error';
			if(isset($language) && !empty($language) && $language == 'italian'){
				$response['message'] ="Inserisci l'e-mail non corrisponde dal mio database.";
			}else{
				$response['message'] = 'Enter email is not match from my database.';
			}
			echo json_encode($response);
			exit;
		}
		$result = $this->api_model_v1->login($data);
		if($result){
			if($result[0]['status'] == 0){
				$response['status'] = 'error';
				//$response['message'] = 'Your account is not active. Please contact to admin.';
				if(isset($language) && !empty($language) && $language == 'italian'){
					$response['message'] ="Il tuo account non è attivato, si prega di attivare dal link di registrazione.";
				}else{
					$response['message'] = "Your account isn't activated, Please activate from registration link.";
				}
				echo json_encode($response);
				exit;
			}
			if($result[0]['privacy_status'] == 3){
				$response['status'] = 'error';
				if(isset($language) && !empty($language) && $language == 'italian'){
					$response['message'] ="Il tuo account è stato cancellato. Si prega di contattare per l'amministratore.";
				}else{
					$response['message'] = "Your account have been deleted. Please contact to admin.";
				}
				echo json_encode($response);
				exit;
			}
			$key = base64_decode(JWT_SECRET_KEY);
			$tokenId    = base64_encode(random_bytes(32));
            $issuedAt   = time();
            $notBefore  = $issuedAt + 10;  //Adding 10 seconds
            $expire     = $notBefore + 7200; // Adding 60 seconds
            $serverName = base_url(); /// set your domain name 
            #user data
			$token_data = array(
				'user_id'  	=> $result[0]['user_id'],
		        'name'  	=> $result[0]['name'],
		        'email_id'	=> $result[0]['email_id'],
		        'device_name' => $data['device_name'],
			);

			#token data reletaed to jwt
			$jwt_data = array(
	            'iat'  => $issuedAt,         // Issued at: time when the token was generated
	            'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
	            'iss'  => $serverName,       // Issuer
	            'nbf'  => $notBefore,        // Not before
	            'exp'  => $expire,           // Expire
	            'data' => $token_data
	        );
	        #token created 
			$jwt_token = JWT::encode($jwt_data, $key);

			#create data for device token update or insert 
			$device_data = array(
				'token'=> $jwt_token,
				'device_name' => $data['device_name'],
				'user_id'  	=> $result[0]['user_id'],
				'created_at' =>date('y-m-d h:i:sa'),
				'updated_at' =>date('y-m-d h:i:sa')
			);

			#check login user have in device token or not
			if($this->api_model_v1->check_device_token_by_user_id($result[0]['user_id'])){
				$this->api_model_v1->update_device_token($device_data);
			}else{
				$this->api_model_v1->insert_device_token($device_data);
			}
			$response['status'] = 'success';
			$response['user_id'] = $result[0]['user_id'];
			$response['user_name'] = $result[0]['name'];
			$response['user_email'] = $result[0]['email_id'];
			$response['login_type'] = $data['login_type'];
			if(isset($language) && !empty($language) && $language == 'italian'){
				$response['message'] ="Accedi con successo.";
			}else{
				$response['message'] = 'Login successfully.';
			}
			$response['access_token'] = $jwt_token;
			//$decode_data = JWT::decode($jwt_token, $key, array('HS256'));
			//$response['user_id']   =$decode_data->data->user_id;
		}else{
			$response['status'] = 'error';
			if(isset($language) && !empty($language) && $language == 'italian'){
				$response['message'] = "Inserisci l'indirizzo email e la password corretti.";
			}else{
				$response['message'] ="Please enter correct email address and password.";
			}
			echo json_encode($response);
			exit;
		}
		echo json_encode($response);
		exit;
	}
	# This function is used for login after social signup for gmail or facefook .
	public function login_after_social_signup($email,$device_name,$login_type)
	{
		$data['email'] = $email;
		$data['device_name'] = $device_name;
		$data['login_type'] = $login_type;
		if(!$this->api_model_v1->checkEmail($data['email'])){
			$response['status'] = 'error';
			if(isset($language) && !empty($language) && $language == 'italian'){
				$response['message'] ="Inserisci l'e-mail non corrisponde dal mio database.";
			}else{
				$response['message'] = 'Enter email is not match from my database.';
			}
			echo json_encode($response);
			exit;
		}
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		$result = $this->api_model_v1->login($data);
		//print_r($result);exit;
		if($result){
			/*if($result[0]['status'] == 0){
				$response['status'] = 'error';
				$response['message'] = 'Your account is not active. Please contact to admin.';
				echo json_encode($response);
				exit;
			}*/
			if($result[0]['privacy_status'] == 3){
				$response['status'] = 'error';
				if(isset($language) && !empty($language) && $language == 'italian'){
					$response['message'] ="Il tuo account è stato cancellato. Si prega di contattare per l'amministratore.";
				}else{
					$response['message'] = "Your account have been deleted. Please contact to admin.";
				}
				echo json_encode($response);
				exit;
			}
			$key = base64_decode(JWT_SECRET_KEY);
			$tokenId    = base64_encode(random_bytes(32));
            $issuedAt   = time();
            $notBefore  = $issuedAt + 10;  //Adding 10 seconds
            $expire     = $notBefore + 7200; // Adding 60 seconds
            $serverName = base_url(); /// set your domain name 
            #user data
			$token_data = array(
				'user_id'  	=> $result[0]['user_id'],
		        'name'  	=> $result[0]['name'],
		        'email_id'	=> $result[0]['email_id'],
		        'device_name' => $data['device_name'],
			);

			#token data reletaed to jwt
			$jwt_data = array(
	            'iat'  => $issuedAt,         // Issued at: time when the token was generated
	            'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
	            'iss'  => $serverName,       // Issuer
	            'nbf'  => $notBefore,        // Not before
	            'exp'  => $expire,           // Expire
	            'data' => $token_data
	        );
	        #token created 
			$jwt_token = JWT::encode($jwt_data, $key);

			#create data for device token update or insert 
			$device_data = array(
				'token'=> $jwt_token,
				'device_name' => $data['device_name'],
				'user_id'  	=> $result[0]['user_id'],
				'created_at' =>date('y-m-d h:i:sa'),
				'updated_at' =>date('y-m-d h:i:sa')
			);

			#check login user have in device token or not
			if($this->api_model_v1->check_device_token_by_user_id($result[0]['user_id'])){
				$this->api_model_v1->update_device_token($device_data);
			}else{
				$this->api_model_v1->insert_device_token($device_data);
			}
			$response['status'] = 'success';
			$response['user_id'] = $result[0]['user_id'];
			$response['user_name'] = $result[0]['name'];
			$response['user_email'] = $result[0]['email_id'];
			$response['login_type'] = $data['login_type'];
			$response['message'] = 'Login successfully.';
			$response['access_token'] = $jwt_token;
			//$decode_data = JWT::decode($jwt_token, $key, array('HS256'));
			//$response['user_id']   =$decode_data->data->user_id;
		}else{
			$response['status'] = 'error';
			if(isset($language) && !empty($language) && $language == 'italian'){
				$response['message'] ="Inserisci la corretta email";
			}else{
				$response['message'] = 'Please Enter correct email.';
			}
			echo json_encode($response);
			exit;
		}
		echo json_encode($response);
		exit;
	}
	#This function is used for register new user app user
	public function signup_post(){
		if(!isset($this->inputall->register_type) || empty($this->inputall->register_type)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter register type.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->name) || empty($this->inputall->name)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter name.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->email_address) || empty($this->inputall->email_address)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter email id.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->device_name) || empty($this->inputall->device_name)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter device name.';
			echo json_encode($response);
			exit;
		}
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		$register_type = $this->inputall->register_type;
		$name = $this->inputall->name;
		$email = $this->inputall->email_address;
		$device_name = $this->inputall->device_name;
		if($register_type == 'email' || $register_type == 'Email'){
			if(!isset($this->inputall->password) || empty($this->inputall->password)){
				$response['status'] = 'error';
				$response['message'] = 'Please enter password.';
				echo json_encode($response);
				exit;
			}
			if(!isset($this->inputall->zip_code) || empty($this->inputall->zip_code)){
				$response['status'] = 'error';
				$response['message'] = 'Please enter zip code.';
				echo json_encode($response);
				exit;
			}
			if(!isset($this->inputall->terms_conditions) || empty($this->inputall->terms_conditions)){
				$response['status'] = 'error';
				$response['message'] = 'Please enter terms & conditions.';
				echo json_encode($response);
				exit;
			}
			if(!isset($this->inputall->news_updates) || empty($this->inputall->news_updates)){
				/*$response['status'] = 'error';
				$response['message'] = 'Please enter news updates.';
				echo json_encode($response);
				exit;*/
				$news_updates=1;
			}else{
				$news_updates = $this->inputall->news_updates;
			}
			$zip_code = $this->inputall->zip_code;
			$password = $this->inputall->password;
			$terms_conditions = $this->inputall->terms_conditions;
			if( strtolower($news_updates) == 'yes'){
				$news_updates_id = 1;
			}else{
				$news_updates_id =1;
			}
			$user_data = array(
				'name'		=> $name,
				'email_id'	=> $email,
				'zip_code'	=> $zip_code,
				'password'	=> md5($password),
				'terms_conditions' => $terms_conditions,
				'privacy_status'	=> $news_updates_id,
				'login_type'	=>$register_type,
				'creation_date' => date('y-m-d h:i:sa'),
				'last_updated_date' => date('y-m-d h:i:sa')
			);
		}else if($register_type == 'google' || $register_type == 'Google' || $register_type == 'Facebook' || $register_type == 'facebook'){
			$user_data = array(
				'name'		=> $name,
				'email_id'	=> $email,
				'login_type'	=>$register_type,
				'status'	=>1,
				'privacy_status'	=> 1,
				'creation_date' => date('y-m-d h:i:sa'),
				'last_updated_date' => date('y-m-d h:i:sa')
			);
		}else{
			$response['status'] = 'error';
			$response['message'] = 'Invalid register type.';
			echo json_encode($response);
			exit;
		}
				
		if(!$this->api_model_v1->checkEmail($email)){

			if($this->api_model_v1->add_user($user_data)){
				#Email send to user with login credencial
				//$to = 'backup.rkgupta@gmail.com';
				$to = $email;
				$from = 'commerciale@bee-o-veg.com';
				$subject = 'Bee-o-veg users credential';
				$data['agent'] = $_SERVER["HTTP_USER_AGENT"];
				$data['ip']= $_SERVER['REMOTE_ADDR'];
				$data['name'] = $name;
				$data['user_name'] = $email;
				$data['type'] = $register_type;
				if($register_type == 'email' || $register_type == 'Email'){
					$data['user_password'] = $password;
				}else{
					$data['user_password'] = '';
				}
				$data['activation_link'] = base_url().'appuser/account_conformation?id='.md5($email).'&email='.$email;
				$html  = $this->load->view('email/register',$data,true);
				$result = $this->send_email($to, $from, $subject,$html);
				if($register_type == 'Email' || $register_type == 'email'){
					$response['status'] = 'success';
					$response['name'] = $name;
					$response['email'] = $email;
					$response['register_type'] = $register_type;
					$response['password'] = $password;
					if(isset($language) && !empty($language) && $language == 'italian'){
						$response['message'] ="Il tuo account è stato creato con successo.";
					}else{
						$response['message'] = 'Your account has been created successfully.';
					}
					$response['access_token'] = $this->access_token;
					echo json_encode($response);
					exit;
				}else if($register_type == 'google' || $register_type == 'Google' || $register_type == 'Facebook' ||$register_type == 'facebook'){
					$this->login_after_social_signup($email,$device_name,$register_type);

				}
			}
		}else{
			$response['status'] = 'error';
			if(isset($language) && !empty($language) && $language == 'italian'){
				$response['message'] ="L'ID email è già registrato.";
			}else{
				$response['message'] = 'Email id is already registered.';
			}
			echo json_encode($response);
			exit;
		}
	}
	#this function is used for forget password reset link send to user email
	public function forgetpassword_post(){
		if(isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		if(!isset($this->inputall->email_id) || empty($this->inputall->email_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter email id.';
			echo json_encode($response);
			exit;
		}
    	$email = $this->inputall->email_id;
    	if(!$this->api_model_v1->checkEmail($email)){
			$response['status'] = 'error';
			if(isset($language) && !empty($language) && $language == 'italian'){
				$response['message'] ="Il tuo ID e-mail non è registrato.";
			}else{
				$response['message'] = 'Your email id is not registerd.';
			}
			echo json_encode($response);
			exit;		
    	}
    	if($this->api_model_v1->checkUserStatus($email)){
			$response['status'] = 'error';
			if(isset($language) && !empty($language) && $language == 'italian'){
				$response['message'] ="Il tuo account non è attivo. Si prega di confermare la registrazione o contattare l'amministratore";
			}else{
				$response['message'] = "Your account is not active. Please confirm your registration or contact to admin.";
			}
			echo json_encode($response);
			exit;		
    	}
    	if($this->api_model_v1->checkUserPrivacyStatus($email)){
			$response['status'] = 'error';
			if(isset($language) && !empty($language) && $language == 'italian'){
				$response['message'] ="Il tuo account è stato cancellato. Si prega di contattare per l'amministratore.";
			}else{
				$response['message'] = "Your account have been deleted. Please contact to admin.";
			}
			echo json_encode($response);
			exit;		
    	}
    	//$to = 'backup.rkgupta@gmail.com';
    	$to = $email;
		$from = 'commerciale@bee-o-veg.com';
		$subject = 'Forget password link';
		$token = random_string('alnum',20);
		$data['link'] = base_url().'auth/resetPassword?email='.$email.'&token='.$token.'&type=app_user';
		$data['agent'] = $_SERVER["HTTP_USER_AGENT"];
		$data['ip']= $_SERVER['REMOTE_ADDR'];
		$html  = $this->load->view('email/reset_password',$data,true);
		$this->db->set('token', $token);
		$this->db->set('last_updated_date', date('y-m-d h:i:sa'));
		$this->db->where('email_id', $email);
		$this->db->update('app_users');
		if($this->send_email($to, $from, $subject,$html)){
			$response['status'] = 'success';
			if(isset($language) && !empty($language) && $language == 'italian'){
				$response['message'] ="Il link di reset è stato inviato al tuo indirizzo email registrato.";
			}else{
				$response['message'] = 'Reset link has been sent to your registered email id.';
			}
			$response['access_token'] = $this->access_token;
		}else{
			$response['status'] = 'success';
			$response['message'] = 'Internal server error.';
		}
		echo json_encode($response);
		exit;
		
	}
	#This function is used for change password 
	public function changePassword_post(){
		/*if(isset($this->access_token) && !empty($this->access_token)){
			$result = json_decode($this->verify_token($this->access_token));
			if($result->status == 'error'){
				$response['status'] = 'error';
				$response['message'] = $result->message;
				echo json_encode($response);
				exit;
			}
		}else{
			$response['status'] = 'error';
			$response['message'] = 'Access token can not be empty.';
			echo json_encode($response);
			exit;

		}*/
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		if(empty($this->inputall->user_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter user id.';
			echo json_encode($response);
			exit;
		}else if(empty($this->inputall->old_password)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter email id.';
			echo json_encode($response);
			exit;
		}else if(empty($this->inputall->new_password)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter password.';
			echo json_encode($response);
			exit;
		}else if(empty($this->inputall->confirm_password)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter conf_password.';
			echo json_encode($response);
			exit;
		}else if($this->inputall->new_password != $this->inputall->confirm_password){
			$response['status'] = 'error';
			if(isset($language) && !empty($language) && $language == 'italian'){
				$response['message'] ="Conferma la password non corrisponde dalla nuova password.";
			}else{
				$response['message'] = 'Confirm password does not match from new password.';
			}
			echo json_encode($response);
			exit;
		}
		$user_id = $this->inputall->user_id;
		$old_password = $this->inputall->old_password;
		$new_password = $this->inputall->new_password;
		$conf_password =$this->inputall->confirm_password;
		#check app user exist or not
		if(!$this->api_model_v1->checkUserId($user_id)){
			$response['status'] = 'error';
			$response['message'] = 'Invalid user id.';
			echo json_encode($response);
			exit;
		}
		#check app customer exist or not
		if(!$this->api_model_v1->password_match_by_id($user_id,$old_password)){
			$response['status'] = 'error';
			if(isset($language) && !empty($language) && $language == 'italian'){
				$response['message'] ="La vecchia password non corrisponde.";
			}else{
				$response['message'] = 'Old password is not match.';
			}
			echo json_encode($response);
			exit;
		}
		$user_data = array(
			'user_id' 			=> $user_id,
			'new_password'		=>$new_password,
		);
		#update password 
		if($this->api_model_v1->updateUserPassword($user_data)){
			$response['status'] = 'success';
			if(isset($language) && !empty($language) && $language == 'italian'){
				$response['message'] ="La tua password è stata cambiata con successo.";
			}else{
				$response['message'] = 'Your password have been changed successfully.';
			}
			
			//$response['access_token'] = $this->access_token;
			echo json_encode($response);
			exit;
		}else{
			$response['status'] = 'error';
			$response['message'] = 'Internal server error!';
			echo json_encode($response);
			exit;
		}
	}
	
	#this function is used for get customer info based on  customer_id
	public function customerdetails_post(){
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}

		if(!isset($this->inputall->customer_id) || empty($this->inputall->customer_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter customer id.';
			echo json_encode($response);
			exit;
		}
		if(!isset($this->inputall->latitude) || empty($this->inputall->latitude)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter latitude.';
			echo json_encode($response);
			exit;
		}
		if(!isset($this->inputall->longitude) || empty($this->inputall->longitude)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter longitude .';
			echo json_encode($response);
			exit;
		}
		if(!isset($this->inputall->user_id) || empty($this->inputall->user_id)){
			$user_id='';
		}else{
			$user_id = $this->inputall->user_id;
		}
		$customer_id = $this->inputall->customer_id;
		$latitude = $this->inputall->latitude;
		$longitude = $this->inputall->longitude;

		if(!$this->api_model_v1->checkCustomerId($customer_id)){
			$response['status'] = 'error';
			$response['message'] = 'Invalid customer id.';
			echo json_encode($response);
			exit;
		}
		$favorite_status = $this->api_model_v1->check_my_favorites_status($user_id, $customer_id);
		$result = $this->api_model_v1->getCustomerDetailsById($customer_id, $latitude,$longitude, $language);
		$customer_category_name = $this->api_model_v1->get_cat_name_by_id($result[0]['category_id']);
		if($customer_category_name == 'Food Truck'){
			$menu_type = 'Menu';
		}elseif($customer_category_name == 'Farm') {
			$menu_type = 'Product';
		}else{
			$menu_type ='';
		}
		if(!empty($result) && count($result) >0){
			$response['status'] = 'success';
			$response['message'] = 'Data found.';
			$response['menu_type'] = $menu_type;
			$response['access_token'] = $this->access_token;
			//$result[0]['distance'] = $result[0]['distance']?number_format($result[0]['distance'],1):'0';
			$string = trim($result[0]['description']);
			if(strlen($string) > 400){
				$description = substr($string, 0, 400).'...';
			}else{
				$description =trim($result[0]['description']);
			}
			$menu = $this->api_model_v1->check_menu_exist_by_customer_id($customer_id);
			$result[0]['menu_available'] = $menu?'yes':'no';
			$result[0]['description'] = $description;
			$result[0]['my_favorites'] = $favorite_status;
			$response['details'] = $result[0];
			$response['images'] = $this->api_model_v1->get_customer_images_by_id($customer_id);
			$response['rating'] = $this->api_model_v1->get_avg_rating_by_customer_id($customer_id);
			$response['reviews'] = $this->api_model_v1->get_customer_comment_by_id($customer_id,$language);
		}else{
			$response['status'] = 'error';
			$response['message'] = 'Data not found.';
		}
		//print_r($response);
		echo json_encode($response);
		exit;
	}
	#This function is used for get menu deatils based on customer_id
	public function menulist_post(){
		if(!isset($this->inputall->customer_id) || empty($this->inputall->customer_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter customer id.';
			echo json_encode($response);
			exit;
		}
		$customer_id = $this->inputall->customer_id;
		if(!$this->api_model_v1->checkCustomerId($customer_id)){
			$response['status'] = 'error';
			$response['message'] = 'Invalid customer id.';
			echo json_encode($response);
			exit;
		}
		$language = $this->inputall->language;
		$result = $this->api_model_v1->getCustomerMenusItemsById($customer_id,$language);
		//print_r($result);exit;
		if(!empty($result) && count($result) >0){
			$menu_list  = array();
			$menu_images = array();
			$keywords_name= array();
			$keywords_ids = array();
			$filters_ids = array();
			$filters_name = array();
			$menu_list_item = array();
			$menu_cat_id = '';
			$customer_category_name = $this->api_model_v1->get_cat_name_by_id($result[0]['customer_category_id']);
			if($customer_category_name == 'Food Truck'){
				$menu_type = 'Menu';
			}elseif($customer_category_name == 'Farm') {
				$menu_type = 'Product';
			}else{
				$menu_type ='';
			}
			foreach ($result as $key => $menu) {
				if($menu_cat_id != $menu['category_id']){
					$category_name = $this->api_model_v1->get_meu_category_name_by_id($menu['category_id'], $language);
					$menu_list_item[$key]['category_name'] = $category_name;
			    }
				$menu_list_item[$key]['title'] = $menu['title'];
				$menu_list_item[$key]['description'] = $menu['description'];
				$menu_images_array =  array('image_pth' => $menu['image_path'] );
				$menu_images[$key] =  $menu_images_array;
				$keywords = json_decode($menu['keywords']);
				$filters = json_decode($menu['filters']);
				/*if(count($keywords)>0){
					foreach ($keywords as $key_inner => $keyword_id) {
						$name = $this->keyword_model->get_keywords_name_by_id($keyword_id);
						if(!in_array ( $name, $keywords_name)){
							$name_arr = array('keywords'=> $name);
							$keywords_name[$key_inner] = $name_arr;
						}
					}
				}*/
				/*if(count($filters)>0){
					foreach ($filters as $key_inner => $filter_id) {
						$name = $this->filter_model->get_filter_name_by_id($filter_id);
						if(!in_array ( $name, $filters_name)){
							$name_arr = array('filter'=> $name);
							$filters_name[] = $name_arr ;
						}
					}
				}*/
				if(count($keywords)>0){
					foreach ($keywords as $key_inner => $keyword_id) {
						if(!in_array ( $keyword_id, $keywords_ids)){
							$keywords_ids[$key_inner] = $keyword_id;
						}
					}
				}
				if(count($filters)>0){
					foreach ($filters as $key_inner => $filter_id) {
						if(!in_array ( $filter_id, $filters_ids)){
							$filters_ids[] = $filter_id ;
						}
					}
				}
				$menu_cat_id = $menu['category_id'];
				$menu_list = $menu_list_item;
			}
			#get keyword name into array.
			if(count($keywords_ids)>0){
				foreach ($keywords_ids as $key_inner => $keyword_id) {
					$name = $this->api_model_v1->get_keywords_name_by_id($keyword_id, $language);
					if(!in_array ( $name, $keywords_name)){
						$name_arr = array('keywords'=> $name);
						$keywords_name[$key_inner] = $name_arr;
					}
				}
			}
			#get filter name into array.
			if(count($filters_ids)>0){
				foreach ($filters_ids as $key_inner => $filter_id) {
					$name = $this->api_model_v1->get_filter_name_by_id($filter_id, $language);
					if(!in_array ( $name, $filters_name)){
						$name_arr = array('filter'=> $name);
						$filters_name[] = $name_arr ;
					}
				}
			}
			/*if(!empty($menu_list) && count($menu_list)>0){
				foreach ($menu_list as $key => $value) {
					if(isset($value['category_name']) && !empty($value['category_name'])){
						$response['menu'][]= array('category_name' => $value['category_name']);
					}
					$response['menu'][] = array('title' =>$value['title'] , 'description' =>$value['description']);
				}
			}else{
				$response['menu'] = $menu_list;
			}	*/		
			$response['status'] = 'success';
			$response['access_token'] = $this->access_token;
			$response['message'] = 'Data found.';
			$response['company_id'] = $result[0]['company_id'];
			$response['company_name'] = $result[0]['company_name'];
			$response['filters'] = $filters_name;
			$response['tags'] = $keywords_name;
			$response['menu_type'] = $menu_type;
			$response['menu'] = $menu_list;
			$response['images'] = $menu_images;
		}else{
			$response['status'] = 'error';
			$response['message'] = 'Data not found.';
		}
		echo json_encode($response);
		exit;
	}
	#This function is used for geting favorite customer information based on user id
	public function favoritecustomer_post(){
		if(!isset($this->inputall->latitude) || empty($this->inputall->latitude)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter latitude.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->longitude) || empty($this->inputall->longitude)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter longitude.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->user_id) || empty($this->inputall->user_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter user id.';
			echo json_encode($response);
			exit;
		}
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		$user_id = $this->inputall->user_id;
		$latitude = $this->inputall->latitude;
		$longitude = $this->inputall->longitude;
		if(!$this->api_model_v1->checkUserId($user_id)){
			$response['status'] = 'error';
			$response['message'] = 'Invalid user id.';
			echo json_encode($response);
			exit;
		}
		$result = $this->api_model_v1->get_all_favorite_customer_list_by_user_id($user_id, $latitude, $longitude,$language);
		if(count($result)>0 && !empty($result)){
			$response['status'] = 'success';
			$response['message'] = 'Favorite customer is found.';
			$response['customers'] = $result;
			echo json_encode($response);
			exit;
		}else{
			$response['access_token'] = $this->access_token;
			$response['status'] = 'error';
			$response['message'] = 'Favorite customer is not found.';
			echo json_encode($response);
			exit;
		}
	}
	#this function is used for get page content by page name
	public function pagecontent_post(){
		if(!isset($this->inputall->name) || empty($this->inputall->name)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter page name';
			echo json_encode($response);
			exit;
		}
		$pageName = $this->inputall->name;
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		if($pageName == 'about' || $pageName == 'privacy' || $pageName == 'terms' ){
			$result = $this->api_model_v1->get_page_content($pageName, $language);
			if(empty($result)){
				$response['status'] = 'error';
				$response['message'] = 'data not found';
			}else{
				$response['status'] = 'success';
				$response['access_token'] = $this->access_token;
				$response['message'] = 'Data found';
				$response['data'] = $result;
			}
			
		}else{
			$response['status'] = 'error';
			$response['message'] = 'Invalid page name';
		}		
		echo json_encode($response);
		exit;
	}
	#this function is used for rate to customer by user
	public function ratingtocustomer_post(){
		/*if(isset($this->access_token) && !empty($this->access_token)){
			$result = json_decode($this->verify_token($this->access_token));
			if($result->status == 'error'){
				$response['status'] = 'error';
				$response['message'] = $result->message;
				echo json_encode($response);
				exit;
			}
		}else{
			$response['status'] = 'error';
			$response['message'] = 'Access token can not be empty.';
			echo json_encode($response);
			exit;

		}*/
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		if(!isset($this->inputall->user_id) || empty($this->inputall->user_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter user id.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->customer_id) || empty($this->inputall->customer_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter customer_id';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->rating) || empty($this->inputall->rating)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter rating.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->comment) || empty($this->inputall->comment)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter comment.';
			echo json_encode($response);
			exit;
		}
		$user_id = $this->inputall->user_id;
		$customer_id = $this->inputall->customer_id;
		$comment =  $this->inputall->comment;
		$rating = $this->inputall->rating;
		if(!$this->api_model_v1->checkUserId($user_id)){
			$response['status'] = 'error';
			$response['message'] = 'Invalid user id.';
			echo json_encode($response);
			exit;
		}
		if(!$this->api_model_v1->checkCustomerId($customer_id)){
			$response['status'] = 'error';
			$response['message'] = 'Invalid customer id.';
			echo json_encode($response);
			exit;
		}
		/*if($this->api_model_v1->checkUserRating($user_id, $customer_id)){
			$response['status'] = 'error';
			$response['message'] = 'This user have already rated to customer.';
			echo json_encode($response);
			exit;
		}*/
		$rating_data = array(
			'fk_customer_id' =>	$customer_id,
			'fk_user_id'	=> $user_id,
			'rating'	=>	$rating,
			'comment'	=>	$comment,
			'status'	=>	1,
			'creation_date'	=> date('y-m-d h:i:sa'),
			'updated_date' =>	date('y-m-d h:i:sa')
		);
		if($this->api_model_v1->ratingToCustomer($rating_data)){
			$response['status'] = 'success';
			#Email send to user for update preferences
			$user_details = $this->api_model_v1->get_user_by_id($user_id); 
			$customer_details = $this->api_model_v1->get_customer_by_id($customer_id);
			//$to = 'backup.rkgupta@gmail.com';
			$to = $customer_details[0]['email'];
			$from = 'commerciale@bee-o-veg.com';
			$subject = 'Bee-o-veg Rating ';
			$data['user_name'] = $user_details[0]['name'];
			$data['company_name']  = $customer_details[0]['name'];
			$data['rating'] = $rating; 
			$data['comment'] = $comment;
			$data['agent'] = $_SERVER["HTTP_USER_AGENT"];
			$data['ip']= $_SERVER['REMOTE_ADDR'];
			$html  = $this->load->view('email/rating_to_customer',$data,true);
			$result = $this->send_email($to, $from, $subject,$html);
			//$response['access_token'] = $this->access_token;
			if(isset($language) && !empty($language) && $language == 'italian'){
				$response['message'] ='Grazie per il tuo feedback e valutazione!';
			}else{
				$response['message'] ='Thank you for your feedback and rating!';
			}
			//$response['message'] = $language?'Grazie per il tuo feedback e valutazione!':'Thank you for your feedback and rating!';
		}else{
			$response['status'] = 'error';
			$response['message'] = 'Internall server error.';
		}
		echo json_encode($response);
		exit;
	}
	public function notify_get(){
		$sender_id = $this->get('sender_id');
		$sender_type  = $this->get('sender_type');
		$recipient_id = $this->get('recipient_id');
		$recipient_type = $this->get('recipient_type');
		$message = $this->get('message');
		$type = $this->get('message_type');
		if(empty($sender_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter sender id.';
			echo json_encode($response);
			exit;
		}else if(empty($sender_type)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter sender_type';
			echo json_encode($response);
			exit;
		}else if(empty($recipient_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter recipient id.';
			echo json_encode($response);
			exit;
		}else if(empty($recipient_type)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter recipient_type.';
			echo json_encode($response);
			exit;
		}else if(empty($message)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter notification message.';
			echo json_encode($response);
			exit;
		}
		#check sender user is exist or not
		if($sender_type == 1){
			$sender_id = $this->api_model_v1->get_admin_id();
			if(!$this->api_model_v1->check_user_type1($sender_id)){
				$response['status'] = 'error';
				$response['message'] = 'Invalide sender  id';
				echo json_encode($response);
				exit;
			}
		}elseif($sender_type == 2) {
			if(!$this->api_model_v1->check_user_type2($sender_id)){
				$response['status'] = 'error';
				$response['message'] = 'Invalide sender  id';
				echo json_encode($response);
				exit;
			}
		}elseif($sender_type == 3) {
			if(!$this->api_model_v1->check_user_type3($sender_id)){
				$response['status'] = 'error';
				$response['message'] = 'Invalide sender id';
				echo json_encode($response);
				exit;
			}
		}
		#check recipient user is exist or not 
		if($recipient_type == 1){
			$recipient_id = $this->api_model_v1->get_admin_id();
			if(!$this->api_model_v1->check_user_type1($recipient_id)){
				$response['status'] = 'error';
				$response['message'] = 'Invalide recipient  id';
				echo json_encode($response);
				exit;
			}
		}elseif($recipient_type == 2) {
			if(!$this->api_model_v1->check_user_type2($recipient_id)){
				$response['status'] = 'error';
				$response['message'] = 'Invalide recipient  id';
				echo json_encode($response);
				exit;
			}
		}elseif($recipient_type == 3) {
			if(!$this->api_model_v1->check_user_type3($recipient_id)){
				$response['status'] = 'error';
				$response['message'] = 'Invalide recipient id';
				echo json_encode($response);
				exit;
			}
		}
		#notification data
		$notification_data = array(
			'sender_id'		=> $sender_id,
			'sender_type' 		=>$sender_type,
			'recipient_id'	=> $recipient_id,
			'recipient_type'	=> $recipient_type,
			'message'  			=> $message,
			'type'				=>$type,
			'creation_date' 	=> date('y-m-d h:i:sa'),
		);
		//print_r($notification_data);exit;
		#add notification ot notification table
		if($this->api_model_v1->notify($notification_data)){
			$response['status'] = 'success';
			$response['access_token'] = $this->access_token;
			$response['message'] = 'Notification send successfully.';
			echo json_encode($response);
			exit;
		}else{
			$response['status'] = 'error';
			$response['message'] = 'Internal server error.';
			echo json_encode($response);
			exit;

		}
	}
	#This function is used for sending email
	public function send_email($to, $from, $subject,$html){
		
		/*$config = Array(        
            'protocol' => SMTP,
            'smtp_host' => SMTP_HOST,
            'smtp_port' => SMTP_PORT,
            'smtp_user' => SMTP_USER,
            'smtp_pass' => SMTP_PASSWORD,
            'smtp_timeout' => '8',
            'mailtype'  => 'html', 
            'charset'   => 'utf-8',
            'starttls'  => true,
            'wordwrap' => TRUE
        );*/
        $email_setting = $this->api_model_v1->get_email_setting();
        if(isset($email_setting) && !empty($email_setting)){
        	$config = Array(        
	            'protocol' => $email_setting[0]['type'],
	            'smtp_host' => $email_setting[0]['smtp_host'],
	            'smtp_port' => $email_setting[0]['smtp_port'],
	            'smtp_user' => $email_setting[0]['smtp_user'],
	            'smtp_pass' => $email_setting[0]['smtp_password'],
	            'smtp_timeout' => '8',
	            'mailtype'  => 'html', 
	            'charset'   => 'utf-8',
	            'starttls'  => true,
         	); 
        }else{
        	$config = Array(        
	            'protocol' => SMTP,
	            'smtp_host' => SMTP_HOST,
	            'smtp_port' => SMTP_PORT,
	            'smtp_user' => SMTP_USER,
	            'smtp_pass' => SMTP_PASSWORD,
	            'smtp_timeout' => '8',
	            'mailtype'  => 'html', 
	            'charset'   => 'utf-8',
	            'starttls'  => true,
	            'wordwrap' => TRUE
        	);
        } 
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        //$this->email->from('commerciale@bee-o-veg.com', 'Bee-O-Veg');
        $this->email->from($from, 'Bee-O-Veg');
        $this->email->to($to);  
        $this->email->subject($subject);
        $this->email->message($html);
        if($this->email->send()){
        	return true;
        }else{
        	$error = $this->email->print_debugger();
        	return $error;
        }        
	}
	#This function is used for calculate distance based on latitude and longitude
	public function distance($lat1, $lon1, $lat2, $lon2, $unit) {
	  $theta = $lon1 - $lon2;
	  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	  $dist = acos($dist);
	  $dist = rad2deg($dist);
	  $miles = $dist * 60 * 1.1515;
	  $unit = strtoupper($unit);

	  if ($unit == "K") {
	      return ($miles * 1.609344);
	  } else if ($unit == "N") {
	      return ($miles * 0.8684);
	  } else{
	      return $miles;
	  }
	}
	#this function is used for get all category list from category table.
	public function categorylist_get(){
		$language = $this->input->get('language');
		$result = $this->api_model_v1->get_category_list($language);
		if($result){
			$response['status'] = 'success';
			$response['message'] = 'Data found';
			//$response['token'] = $this->access_token;
			$response['category'] =$result; 
		}else{
			$response['status'] = 'error';
			$response['message'] = 'Data not found';

		}
		echo json_encode($response);
		exit;
	}
	#this function is used for get all filter list .
	public function get_filter_list_by_category_id_post(){
		if(!isset($this->inputall->category_id) || empty($this->inputall->category_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter category id.';
			echo json_encode($response);
			exit;
		}
		$category_id = $this->inputall->category_id;
		$language = $this->inputall->language;
		$result = $this->api_model_v1->get_filter_list_by_category_id($category_id, $language);
		if($result){
			$response['status'] = 'success';
			$response['message'] = 'Data found';
			//$response['token'] = $this->access_token;
			$response['filters'] =$result; 
		}else{
			$response['status'] = 'error';
			$response['message'] = 'Data not found';

		}
		echo json_encode($response);
		exit;
	}
	#this function is used for return customer details based on filters parameters
	public function filter_post(){
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		if(!isset($this->inputall->latitude) && empty($this->inputall->latitude)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter latitude.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->longitude) && empty($this->inputall->longitude)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter longitude.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->category_id) && empty($this->inputall->category_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter category id.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->filter) && empty($this->inputall->filter)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter filter id.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->keyword) && empty($this->inputall->keyword)){
			$data['search_keyword']='';
		}else if(!isset($this->my_favorites) && empty($this->inputall->my_favorites)){
			$data['search_keyword']='';
		}
		if(!isset($this->inputall->user_id) && empty($this->inputall->user_id)){
			$data['user_id']='';
		}else{
			$data['user_id'] = $this->inputall->user_id;
		}
		$data['category_id'] = $this->inputall->category_id;
		$data['latitude'] = $this->inputall->latitude;
		$data['longitude'] = $this->inputall->longitude;
		$search_filter = $this->inputall->filter;
		$data['search_keyword'] = $this->inputall->keyword;
		$data['my_favorites'] = $this->inputall->my_favorites;
		$data['type'] = 'filter';
		
		if(!empty($search_filter) && count($search_filter)>0){
			foreach ($search_filter as $key => $value) {
				$data['search_filters'][] = $value->id;
			}
		}else{
			$data['search_filters']='';
		}
		//$result = $this->api_model_v1->filter($data);
		$result = $this->get_customer_view_by_distance_range($data['category_id'], $data['latitude'], $data['longitude'], 20, 'filter',$data);
		if($result){
			$response['status'] = 'success';
			$response['message'] = 'Data found';
			$response['category_id'] = $data['category_id'];
			$response['category_name'] = $this->api_model_v1->get_cat_name_by_id($data['category_id'], $language);
			//$response['token'] = $this->access_token;
			$response['customers'] =$result; 
		}else{
			$response['status'] = 'error';
			$response['category_name'] = $this->api_model_v1->get_cat_name_by_id($data['category_id'], $language);;
			$response['message'] = 'Data not found';

		}
		echo json_encode($response);
		exit;
	}
	public function add_to_favorites_post(){
	/*	if(isset($this->access_token) && !empty($this->access_token)){
			$result = json_decode($this->verify_token($this->access_token));
			if($result->status == 'error'){
				$response['status'] = 'error';
				$response['message'] = $result->message;
				echo json_encode($response);
				exit;
			}
		}else{
			$response['status'] = 'error';
			$response['message'] = 'Access token can not be empty.';
			echo json_encode($response);
			exit;

		}*/
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		if(!isset($this->inputall->customer_id) && empty($this->inputall->customer_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter customer id.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->user_id) && empty($this->inputall->user_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter user_id';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->my_favorites) && empty($this->inputall->my_favorites)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter my favorites.';
			echo json_encode($response);
			exit;
		}
		$user_id = $this->inputall->user_id;
		$customer_id = $this->inputall->customer_id;
		$my_favorites = $this->inputall->my_favorites;

		if(!$this->api_model_v1->checkUserId($user_id)){
			$response['status'] = 'error';
			$response['message'] = 'Invalid user id.';
			echo json_encode($response);
			exit;
		}
		if(!$this->api_model_v1->checkCustomerId($customer_id)){
			$response['status'] = 'error';
			$response['message'] = 'Invalid customer id.';
			echo json_encode($response);
			exit;
		}
		#check user is all ready added in favorites or not
		$favorite = $this->api_model_v1->get_all_favorites_by_user_id_and_customer_id($user_id, $customer_id);
		if($favorite && count($favorite)>0){
			$result =$this->api_model_v1->remove_from_favorites($user_id, $customer_id, $my_favorites);
		}else{
			$result =$this->api_model_v1->add_to_favorites($user_id, $customer_id, $my_favorites);
		}
		if($my_favorites == 'yes'){
			//$msg = $language?'Sei stato aggiunto ai miei preferiti.':'You have been added to my favorites.';
			if(isset($language) && !empty($language) && $language == 'italian'){
				$msg ='Sei stato aggiunto ai miei preferiti.';
			}else{
				$msg ='You have been added to my favorites.';
			}
		}else{
			//$msg = $language?'Sei stato rimosso dai miei preferiti.':'You have been removed from my favorites.';
			if(isset($language) && !empty($language) && $language == 'italian'){
				$msg ='Sei stato rimosso dai miei preferiti.';
			}else{
				$msg ='You have been removed from my favorites.';
			}
		}
		if($result){
			$response['status'] = 'success';
			$response['message'] = $msg;
			echo json_encode($response);
			exit;
		}else{
			$response['status'] = 'error';
			$response['message'] = 'Internal server error.';
			echo json_encode($response);
			exit;
		}
	}
	#this function is used for provide customer data for list view without login user..
	public function listview_post(){
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		if(!isset($this->inputall->latitude) && empty($this->inputall->latitude)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter latitude.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->longitude) && empty($this->inputall->longitude)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter longitude.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->category_id) && empty($this->inputall->category_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter category id.';
			echo json_encode($response);
			exit;
		}
		$latitude = $this->inputall->latitude;
		$longitude = $this->inputall->longitude;
		$category_id = $this->inputall->category_id;
		//$result = $this->api_model_v1->list_view($category_id, $latitude, $longitude);
		$result = $this->get_customer_view_by_distance_range($category_id, $latitude, $longitude,20,'listview');
		if(!empty($result) && count($result) >0){
			$response['status'] = 'success';
			$response['message'] = 'Data found';
			$response['category_id'] = $category_id;
			$response['category_name'] = $this->api_model_v1->get_cat_name_by_id($category_id, $language);
			//$response['access_token'] = $this->access_token;
			$response['customers'] = $result;
		}else{
			$response['status'] = 'error';
			$response['category_name'] = $this->api_model_v1->get_cat_name_by_id($category_id, $language);
			$response['message'] = 'Data not found';

		}
		//print_r($response);
		echo json_encode($response);
	}
	#this function is used for provide customer data for Map view without login user..
	public function mapview_post(){
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		if(!isset($this->inputall->latitude) && empty($this->inputall->latitude)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter latitude.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->longitude) && empty($this->inputall->longitude)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter longitude.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->category_id) && empty($this->inputall->category_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter category id.';
			echo json_encode($response);
			exit;
		}
		$latitude = $this->inputall->latitude;
		$longitude = $this->inputall->longitude;
		$category_id = $this->inputall->category_id;
		//$result = $this->api_model_v1->map_view($category_id, $latitude, $longitude);
		$result = $this->get_customer_view_by_distance_range($category_id, $latitude, $longitude,20,'mapview');
		if(!empty($result) && count($result) >0){
			$response['status'] = 'success';
			$response['message'] = 'Data found';
			$response['category_id'] = $category_id;
			$response['category_name'] = $this->api_model_v1->get_cat_name_by_id($category_id, $language);
			$response['customers'] = $result;
		}else{
			$response['status'] = 'error';
			$response['category_name'] = $this->api_model_v1->get_cat_name_by_id($category_id, $language);
			$response['message'] = 'Data not found';

		}
		//print_r($response);
		echo json_encode($response);
	}
	#this function is used for return customer list based on distance range.
	public function get_customer_view_by_distance_range($category_id, $latitude, $longitude,$distance_range,$type,$data=''){
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		if($distance_range <=100){
			if($type == 'mapview'){
				$result = $this->api_model_v1->map_view($category_id, $latitude, $longitude,$distance_range,$language);
			}else if($type == 'listview'){
				$result = $this->api_model_v1->list_view($category_id, $latitude, $longitude, $distance_range, $language);
			}else if($type == 'filter'){
				$data['distance'] = $distance_range;
				$result = $this->api_model_v1->filter($data, $language);;
			}else{
				$result = 0;
			}
			if(!$result){
				if($distance_range == 20){
					$distance_range = 40;
				}else if($distance_range == 40){
					$distance_range = 60;
				}else if($distance_range == 60){
					$distance_range = 100;
				}else if($distance_range == 100){
					$distance_range = 110;
				}
				$result = $this->get_customer_view_by_distance_range($category_id, $latitude, $longitude,$distance_range,$type,$data);
				return $result;
			}else{
				return $result;
			}
		}else{
			return 0;
		}
	}
	#This function is used for verify access token
	public function verify_token($token){
		try {
			$key = base64_decode(JWT_SECRET_KEY);
			$result = JWT::decode($token, $key, array('HS256'));
			$response['status'] = 'success';
			$response['user_id'] = $result->data->user_id;
			return json_encode($response);
			exit;
		} catch (\Exception $e) { 
		    $response['status'] = 'error';
			$response['message'] = $e->getMessage();
			return json_encode($response);
			exit;
		}
	}
	#start  date 21-06-2018 
	#This function is used for get preference list, which is used on sign up screen.
	public function preference_list_post(){
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		$result = $this->api_model_v1->get_preference_list($language);
		if(!empty($result) && count($result) >0){
			$response['status'] = 'success';
			$response['message'] = 'Data found';
			//$response['access_token'] = $this->access_token;
			$response['preferences_list'] = $result;
		}else{
			$response['status'] = 'error';
			$response['message'] = 'Data not found';

		}
		//print_r($response);
		echo json_encode($response);
	}
	#This function is used for get preference list by user id, which is used on preferences setting up screen.
	public function preference_list_by_user_post(){
		if(!isset($this->inputall->user_id) || empty($this->inputall->user_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter user id';
			echo json_encode($response);
			exit;
		}
		$user_id = $this->inputall->user_id;
		if(!$this->api_model_v1->checkUserId($user_id)){
			$response['status'] = 'error';
			$response['message'] = 'Invalid user id.';
			echo json_encode($response);
			exit;
		}
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		$result = $this->api_model_v1->get_preference_list_by_user_id($user_id, $language);
		if(!empty($result) && count($result) >0){
			$response['status'] = 'success';
			$response['message'] = 'Data found';
			//$response['device_token'] = $this->device_token;
			$response['preferences_list'] = $result;
		}else{
			$response['status'] = 'error';
			$response['message'] = 'Data not found';

		}
		echo json_encode($response);
	}
	#This function is used for get preference list by user id, which is used on preferences setting up screen.
	public function preference_save_post(){
		if (isset($this->inputall->language)){
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		if(!isset($this->inputall->user_id) || empty($this->inputall->user_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter user id';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->user_preferences) || empty($this->inputall->user_preferences)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter user id';
			echo json_encode($response);
			exit;
		}
		$user_id = $this->inputall->user_id;
		$user_preferences = $this->inputall->user_preferences;
		if(!$this->api_model_v1->checkUserId($user_id)){
			$response['status'] = 'error';
			$response['message'] = 'Invalid user id.';
			echo json_encode($response);
			exit;
		}
		$result = $this->api_model_v1->update_preferences($user_id,$user_preferences);
		if($result){
			$user_details = $this->api_model_v1->get_user_by_id($user_id);
			if($user_preferences == 1 || $user_preferences == 2){
				//$preferences_msg = $language ? 'Le tue preferenze sono state salvate.':'Your preferences have been  saved.';
				if(isset($language) && !empty($language) && $language == 'italian'){
					$preferences_msg ='Le tue preferenze sono state salvate.';
				}else{
					$preferences_msg ='Your preferences have been  saved.';
				}
				$admin_msg = $user_details[0]['name']." "." has changed his preferences.";
			}else if($user_preferences == 3){
				//$preferences_msg =$language ? 'Il tuo account è stato cancellato con successo.': 'Your account have been deleted successfully.';
				$this->api_model_v1->delete_all_rating_by_app_user_id($user_id);
				if(isset($language) && !empty($language) && $language == 'italian'){
					$preferences_msg ='Il tuo account è stato cancellato con successo.';
				}else{
					$preferences_msg ='Your account have been deleted.';
				}
				$admin_msg = $user_details[0]['name']." "." has deleted his account.";
			}else{
				//$preferences_msg =$language ? 'Le tue preferenze sono state salvate.': 'Your preferences have been  saved.';
				if(isset($language) && !empty($language) && $language == 'italian'){
					$preferences_msg ='Le tue preferenze sono state salvate.';
				}else{
					$preferences_msg ='Your preferences have been  saved.';
				}
				$admin_msg = $user_details[0]['name']." "." has changed his preferences.";
			} 
			#Email send to user for update preferences
			//$to = 'rgelebnis@gmail.com';
			//$to = 'ankita@yopmail.com';
			$to = $user_details[0]['email_id'];
			$from = 'privacy@bee-o-veg.com';
			//$from = 'commerciale@bee-o-veg.com';
			$subject = 'Bee-o-veg preference changes';
			$data['user_name'] = $user_details[0]['name'];
			$data['msg'] = $preferences_msg;
			$data['agent'] = $_SERVER["HTTP_USER_AGENT"];
			$data['ip']= $_SERVER['REMOTE_ADDR'];
			$html  = $this->load->view('email/update_preference',$data,true);
			$email_result = $this->send_email($to, $from, $subject,$html);
			#admin email params
			//$admin['email'] = 'rgelebnis@gmail.com';
			$admin['email'] = 'privacy@bee-o-veg.com';
			$admin['user_name'] = 'admin';
			$admin['agent'] = $_SERVER["HTTP_USER_AGENT"];
			$admin['ip']= $_SERVER['REMOTE_ADDR'];
			$admin['msg'] = $admin_msg;
			$html1  = $this->load->view('email/update_preference',$admin,true);
			$email_result1 = $this->send_email($admin['email'], $from, $subject,$html1);
			#response 
			$response['status'] = 'success';
			$response['user_id'] = $user_id;
			//$response['user_email'] = $user_details[0]['email_id'];
			$response['preference_id'] = $user_preferences;
			$response['message'] = $preferences_msg;
			$response['email'] = $email_result;
		}else{
			$response['status'] = 'error';
			$response['message'] = 'Data not found';

		}
		echo json_encode($response);
	}
	#this function is used for rating to company
	public function rating_to_company_post(){
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		if(!isset($this->inputall->user_id) || empty($this->inputall->user_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter user id.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->rating) || empty($this->inputall->rating)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter rating.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->comment) || empty($this->inputall->comment)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter comment.';
			echo json_encode($response);
			exit;
		}
		$user_id = $this->inputall->user_id;
		$comment = $this->inputall->comment;
		$rating = $this->inputall->rating;
		$type = 3;   //app user type
		if(!$this->api_model_v1->checkUserId($user_id)){
			$response['status'] = 'error';
			$response['message'] = 'Invalid user id.';
			echo json_encode($response);
			exit;
		}
		/*if($this->api_model_v1->check_company_rating_by_user($user_id, $type)){
			$response['status'] = 'error';
			$response['message'] = 'You user have already rated to company.';
			echo json_encode($response);
			exit;
		}*/
		$rating_data = array(
			'user_id'	=> $user_id,
			'rating'	=>	$rating,
			'comment'	=>	$comment,
			'type'		=> 3,
			'creation_date'	=> date('y-m-d h:i:sa'),
			'updated_date' =>	date('y-m-d h:i:sa')
		);
		if($this->api_model_v1->rating_to_company($rating_data)){
			$response['status'] = 'success';
			//$response['access_token'] = $this->device_token;
			//$response['message'] = $language?'Grazie per il tuo feedback. Useremo il tuo feedback per il miglioramento futuro!':'Thank you for your feedback. We will use your feedback for future improvement!';
			#Email send to user for update preferences
			$user_details = $this->api_model_v1->get_user_by_id($user_id); 
			$to = 'privacy@bee-o-veg.com';
			//$to = 'backup.rkgupta@gmail.com';
			$from = 'commerciale@bee-o-veg.com';
			$subject = 'Bee-o-veg Rating ';
			$data['user_name'] = $user_details[0]['name'];
			$data['rating'] = $rating; 
			$data['comment'] = $comment;
			$data['agent'] = $_SERVER["HTTP_USER_AGENT"];
			$data['ip']= $_SERVER['REMOTE_ADDR'];
			$html  = $this->load->view('email/rating_to_company',$data,true);
			$result = $this->send_email($to, $from, $subject,$html);
			if(isset($language) && !empty($language) && $language == 'italian'){
				$response['message'] ='Grazie per il tuo feedback. Useremo il tuo feedback per il miglioramento futuro!';
			}else{
				$response['message'] ='Thank you for your feedback. We will use your feedback for future improvement!';
			}
		}else{
			$response['status'] = 'error';
			$response['message'] = 'Internall server error.';
		}
		echo json_encode($response);
		exit;
	}
	public function is_user_exists_post(){
		if(!isset($this->inputall->email_id) || empty($this->inputall->email_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter email address.';
			echo json_encode($response);
			exit;
		}
		if($this->api_model_v1->checkEmail($this->inputall->email_id)){
			$response['status'] = 'success';
			$response['is_user_exists'] = 'yes';
			echo json_encode($response);
			exit;
		}else{
			$response['status'] = 'success';
			$response['is_user_exists'] = 'no';
			echo json_encode($response);
			exit;
		}
	}
	public function search_post(){
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		if(!isset($this->inputall->latitude) && empty($this->inputall->latitude)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter latitude.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->longitude) && empty($this->inputall->longitude)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter longitude.';
			echo json_encode($response);
			exit;
		}
		if(!isset($this->inputall->category_id) && empty($this->inputall->category_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter category id.';
			echo json_encode($response);
			exit;
		}/*else if(!isset($this->inputall->search_type) && empty($this->inputall->search_type)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter search type.';
			echo json_encode($response);
			exit;
		}*/
		if(!isset($this->inputall->search_key) && empty($this->inputall->search_key)){
			$search_value = '';
		}else{
			//$search_value = $this->inputall->search_key;
			$search_value =  htmlspecialchars($this->inputall->search_key);
		}
		$latitude = $this->inputall->latitude;
		$longitude = $this->inputall->longitude;
		$distance_range = 20;
		$category_id = $this->inputall->category_id;
		//$type = trim($this->inputall->search_type);
		//$result = $this->api_model_v1->search($category_id, $search_value);
		//$result = $this->api_model_v1->search($latitude, $longitude, $category_id, $search_value, $type, $distance_range);
		$type = '';
		$result = $this->search_customer_list_by_distance_range($category_id, $latitude, $longitude,$distance_range,$type,$search_value);
		//echo $search_value;exit;
		$top_array =array();
		$max_array= array();
		$min_array = array();
		if(!empty($result) && count($result) >0){
			$top_array[] = $result[0];
			$max = $result[0]['distance']+100;
			$min = $result[0]['distance']-400;
			foreach ($result as $key => $value) {
				if($key){
					if($value['distance'] < $max && $value['distance'] > $min){
						$max_array[] = $result[$key];
					}else{
						$min_array[] = $result[$key];
					}
				}
			}
			$final_result = array_merge($top_array, $max_array,$min_array);
			$response['status'] = 'success';
			$response['message'] = 'Data found';
			$response['category_id'] = $category_id;
			$response['category_name'] = $this->api_model_v1->get_cat_name_by_id($category_id, $language);
			$response['customers'] = $final_result;
			/*$response['new_result1'] = $top_array;
			$response['new_result2'] = $max_array;
			$response['new_result3'] = $min_array;
			$response['final_result'] = $final_result;*/
		}else{
			$response['status'] = 'error';
			$response['category_name'] = $this->api_model_v1->get_cat_name_by_id($category_id, $language);
			$response['message'] = 'Data not found';

		}
		//print_r($response);
		echo json_encode($response);
	}
	public function search_suggestion_post(){
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		if(!isset($this->inputall->latitude) && empty($this->inputall->latitude)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter latitude.';
			echo json_encode($response);
			exit;
		}else if(!isset($this->inputall->longitude) && empty($this->inputall->longitude)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter longitude.';
			echo json_encode($response);
			exit;
		}
		if(!isset($this->inputall->category_id) && empty($this->inputall->category_id)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter category id.';
			echo json_encode($response);
			exit;
		}/*else if(!isset($this->inputall->search_type) && empty($this->inputall->search_type)){
			$response['status'] = 'error';
			$response['message'] = 'Please enter search type.';
			echo json_encode($response);
			exit;
		}*/
		if(!isset($this->inputall->search_key) || empty($this->inputall->search_key)){
			$response['status'] = 'error';
			$response['category_name'] = $this->api_model_v1->get_cat_name_by_id($this->inputall->category_id);
			$response['message'] = 'Data not found';
			echo json_encode($response);
			exit;
		}else{
			$search_value = $this->inputall->search_key;
		}
		$latitude = $this->inputall->latitude;
		$longitude = $this->inputall->longitude;
		$category_id = $this->inputall->category_id;
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}		
		$result = $this->api_model_v1->search_suggestion($latitude, $longitude, $category_id, $search_value, $language);
		if(!empty($result) && count($result) >0){
			$response['status'] = 'success';
			$response['message'] = 'Data found';
			$response['category_id'] = $category_id;
			$response['category_name'] = $this->api_model_v1->get_cat_name_by_id($category_id, $language);
			$response['customers'] = $result;
		}else{
			$response['status'] = 'error';
			$response['category_name'] = $this->api_model_v1->get_cat_name_by_id($category_id, $language);
			$response['message'] = 'Data not found';

		}
		//print_r($response);
		echo json_encode($response);
	}
	#this function is used in  search function for searching result on distance basis.
	public function search_customer_list_by_distance_range($category_id, $latitude, $longitude,$distance_range,$type,$search_value){
		if (isset($this->inputall->language)) {
			$language = $this->inputall->language;
		}else{
			$language = '';
		}
		if($distance_range <=100){
			$result = $this->api_model_v1->search($latitude, $longitude, $category_id, $search_value, $type, $distance_range,$language);
			if(!$result){
				if($distance_range == 20){
					$distance_range = 40;
				}else if($distance_range == 40){
					$distance_range = 60;
				}else if($distance_range == 60){
					$distance_range = 100;
				}else if($distance_range == 100){
					$distance_range = 110;
				}
				$result = $this->search_customer_list_by_distance_range($category_id, $latitude, $longitude,$distance_range,$type, $search_value);
				return $result;
			}else{
				return $result;
			}
		}else{
			return 0;
		}
	}

}
