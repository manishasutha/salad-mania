<?php include"include/header.php" ?>
    <!-- WRAPPER -->
<div id="wrapper">
<!-- NAVBAR -->
<?php include "include/navbar.php"; ?>
<!-- END NAVBAR -->
<!-- LEFT SIDEBAR -->
<?php include"include/leftsidebar.php"; ?>

<!-- END LEFT SIDEBAR -->
<!-- MAIN -->
    <div class="main">
      <div class="subheader">
        <ul>
            <li>Customer Edit</li>
        </ul>
    </div>
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <?php
            if($this->session->flashdata('success_msg')){
            $msg = $this->session->flashdata('success_msg');
                echo '<div class="alert alert-success fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Success!</strong>'. $msg.'
                    </div>';
            }
        ?>
        <?php
            if($this->session->flashdata('error_msg')){
            $msg = $this->session->flashdata('error_msg');
                echo '<div class="alert alert-danger fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Success!</strong>'. $msg.'
                    </div>';
            }
        ?>
        <div class="container-fluid">
            
            <!-- END OVERVIEW -->
            <div class="row">
                <div class="col-md-12">
                    <!-- RECENT PURCHASES -->
                    <form class="col-md-12 form-panel" style="border:0" method="post" action="<?= base_url()?>customer/editAction" enctype="multipart/form-data" id="customerEditFrm" name="customerEditFrm">
                        <div class="row">
                            <div class="panel new-panel">
                                    <!-- <div class="panel-heading">
                                        <div class="col-md-12">
                                            <h3 class="panel-title">Add Customer</h3>
                                        </div>
                                        <br>
                                    </div> -->
                                  <div class="notice-area">
                                  </div>
                                    <div class="panel-body no-padding">
                                        <div class="panel-heading">
                                            <div class="col-md-12">
                                                <h3 class="panel-title">Customer Personal info</h3>
                                            </div>
                                            <!-- <input class="form-control" value="<?= $customer[0]['serial_number']?>" type="hidden" name="customer_id" id="customer_id">
                                             <input class="form-control" value="<?= $user[0]['serial_number']?>" type="hidden" name="user_id" id="user_id">
                                             <input class="form-control" value="<?= $user[0]['profile_image']?>" type="hidden" name="user_profile_pic_old" id="user_profile_pic_old">
                                             <input class="form-control" value="<?= $customer_image[0]['image_path']?>" type="hidden" name="customer_img_old" id="customer_img_old">
                                             <input class="form-control" value="<?= $customer_image[0]['image_name']?>" type="hidden" name="customer_image_name_old" id="customer_image_name_old">
                                             <input class="form-control" value="<?= $customer_image[0]['customer_image_id']?>" type="hidden" name="customer_image_id" id="customer_image_id">
                                             <input class="form-control" value="details" type="hidden" name="action_type" id="action_type"> -->
                                            <br>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Identification Number<span class="required error">*</span></label>
                                            <input class="form-control" value="<?= $customer[0]['identification_number']?>" type="text" name="identificatino_no" id="identificati_no" readonly>
                                            <br>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Company Name</label>
                                            <input class="form-control" type="text" name="customer_name_show" id="customer_name_show" value="<?= $customer[0]['name']?>">
                                            <?php echo form_error('customer_name', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Email<span class="required error">*</span></label>
                                            <input class="form-control"  type="text" name="user_email" id="user_email" value="<?= $user[0]['email_address']?>" readonly>
                                            <?php echo form_error('user_email', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        <div class="col-md-4">
                                            <label>First Name<span class="required error">*</span></label>
                                            <input class="form-control"  type="text" value="<?= $user[0]['first_name']?>" name="user_first_name" id="user_first_name">
                                            <?php echo form_error('user_first_name', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                         <div class="col-md-4">
                                            <label>Last name<span class="required error">*</span></label>
                                            <input class="form-control"  type="text" name="user_last_name" id="user_last_name" value="<?= $user[0]['last_name']?>">
                                            <?php echo form_error('user_last_name', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        <div class="col-md-2" id="user_profile_display">
                                            <img src="<?= $user[0]['profile_image']? base_url().$user[0]['profile_image'] : base_url().'assets/img/user-medium.png'?>" height="90" style="border-radius: 4px" />
                                        </div>
                                        <div class="col-md-10">
                                             <label>Change Picture</label>
                                            <input  type="file" name="user_profile_pic" id="user_profile_pic" onchange="readURL(this, 'user_profile_display');">
                                            <?php echo form_error('user_profile_pic', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>
                                        <div class="panel-heading">
                                            <div class="col-md-12">
                                                <h3 class="panel-title">Customer Details</h3>
                                                <input class="form-control" value="<?= $customer[0]['serial_number']?>" type="hidden" name="customer_id" id="customer_id">
                                                 <input class="form-control" value="<?= $user[0]['serial_number']?>" type="hidden" name="user_id" id="user_id">
                                                 <input class="form-control" value="<?= $user[0]['profile_image']?>" type="hidden" name="user_profile_pic_old" id="user_profile_pic_old">
                                                 <input class="form-control" value="<?= $customer_image[0]['image_path']?>" type="hidden" name="customer_img_old" id="customer_img_old">
                                                 <input class="form-control" value="<?= $customer_image[0]['image_name']?>" type="hidden" name="customer_image_name_old" id="customer_image_name_old">
                                                 <input class="form-control" value="<?= $customer_image[0]['customer_image_id']?>" type="hidden" name="customer_image_id" id="customer_image_id">
                                                 <input class="form-control" value="details" type="hidden" name="action_type" id="action_type">

                                                 <input type="hidden" name="old_deleted_image_arr" id="old_deleted_image_arr">
                                                </div>
                                            <br>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Identification Number<span class="required error">*</span></label>
                                            <input class="form-control" value="<?= $customer[0]['identification_number']?>" type="text" name="identificatino_no" id="identificati_no" readonly>
                                            <br>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Company Name<span class="required error">*</span></label>
                                            <input class="form-control" type="text" name="customer_name" id="customer_name" value="<?= $customer[0]['name']?>">
                                            <?php echo form_error('customer_name', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Phone Number<span class="required error">*</span></label>
                                            <input class="form-control" type="text" name="customer_phone" id="customer_phone" value="<?= $customer[0]['phone_number']?>">
                                            <?php echo form_error('customer_phone', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        <div class="col-md-3">
                                            <label>IMEI Number<span class="required error">*</span></label>
                                            <input class="form-control" type="text" name="customer_imei_no" id="customer_imei_no" value="<?= $customer[0]['imei_number']?>" readonly>
                                            <?php echo form_error('customer_imei_no', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Sim Number<span class="required error">*</span></label>
                                            <input class="form-control" type="text" name="customer_sim_no" id="customer_sim_no" value="<?= $customer[0]['sim_number']?>" readonly>
                                            <?php echo form_error('customer_sim_no', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Category<span class="required error">*</span></label>
                                            <select class="form-control" id="customer_category" name="customer_category" id="">
                                                    <option value="">Select Category</option>
                                                        <?php
                                                        if(count($category) > 0){
                                                            foreach ($category as $key => $cat) {
                                                                if($customer[0]['category_id'] == $cat['category_id']){
                                                                    $selected = 'selected';
                                                                }else{
                                                                    $selected = '';
                                                                }
                                                                echo '<option value="'.$cat['category_id'].'" '.$selected.'>'.$cat['name'].'</option>';
                                                            }
                                                        } 
                                                        ?>
                                            </select>
                                            <?php echo form_error('customer_category', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Filters<span class="required error">*</span></label>
                                            <select class="form-control select2" multiple="" name="customer_filter[]" id="customer_filter">
                                                <?php
                                                    if(count($filter) > 0 && !empty($filter)){

                                                        foreach ($filter as $key => $filter) {
                                                            $selected = "";
                                                            foreach (json_decode($customer[0]['filter_id']) as $key => $filter_arr) {
                                                                if($filter_arr == $filter['serial_number']){
                                                                    $selected = 'selected';
                                                                }
                                                            }
                                                            echo '<option value="'.$filter['serial_number'].'" '.$selected.'>'.$filter['title'].'</option>';
                                                        }
                                                    }else{
                                                        echo '<option value="">Filterss not available</option>';
                                                    } 
                                                ?>
                                            </select>
                                            <?php echo form_error('customer_filter', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Address<span class="required error">*</span></label>
                                            <input class="form-control" type="text" name="customer_address" id="customer_address" value="<?= $customer[0]['address']?>" onFocus="geolocate()">
                                           <?php echo form_error('customer_address', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <label>Latitude<span class="required error">*</span></label>
                                            <input class="form-control"  type="text" name="customer_lat" id="customer_lat" value="<?= $customer[0]['latitude']?>" readonly>
                                            <?php echo form_error('customer_lat', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Longitude<span class="required error">*</span></label>
                                            <input class="form-control"  type="text"  type="text" name="customer_long" id="customer_long" value="<?= $customer[0]['longitude']?>" readonly>
                                            <?php echo form_error('customer_long', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Country<span class="required error">*</span></label>
                                            <input class="form-control"  type="text" name="customer_country" id="country" value="<?= $customer[0]['country']?>" readonly>
                                            <?php echo form_error('customer_country', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                         <div class="col-md-4">
                                            <label>State<span class="required error">*</span></label>
                                            <input class="form-control"  type="text" name="customer_state" id="administrative_area_level_1" value="<?= $customer[0]['state']?>" readonly>
                                            <?php echo form_error('customer_state', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        <div class="col-md-4">
                                            <label>City<span class="required error">*</span></label>
                                            <input class="form-control"  type="text" name="customer_city" id="locality" value="<?= $customer[0]['city']?>" readonly>
                                            <?php echo form_error('customer_city', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        
                                        <div class="col-md-8">
                                            <label>Description</label>
                                            <textarea class="form-control" rows="4" cols="30" name="customer_description" id="customer_description"><?= $customer[0]['description']?>
                                            </textarea>
                                            <?php echo form_error('customer_description', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Short Description</label>
                                            <textarea class="form-control" rows="4" cols="10" name="customer_short_description" id="customer_short_description" ><?= $customer[0]['short_description']?></textarea>
                                            <?php echo form_error('customer_short_description', '<div class="error">', '</div>'); ?>
                                            <br>
                                        </div>
                                        
                                        <div class="clearfix"></div>
                                        <hr>
                                        <div class="col-md-12">
                                            <fieldset class="form-group">
                                                <a href="javascript:void(0)" onclick="$('#customer_img').click()">Upload Image (You can upload multiple images of 414x325px)</a>
                                                <input type="file" name="customer_img[]"  id="customer_img" style="display: none;" class="form-control" multiple>
                                                <span id="customer_img_error"></span>
                                                <?php echo form_error('customer_img', '<div class="error">', '</div>'); ?>
                                            </fieldset>
                                            <div class="preview-images-zone">
                                                <?php
                                                    if(!empty($customer_image) && count($customer_image) >0){
                                                       foreach ($customer_image as $key => $value) {
                                                            echo'<div class="preview-image preview-show-'.$value['customer_image_id'].'"> 
                                                            <div class="image-cancel" data-old_image_id="'.$value['customer_image_id'].'" data-no="'.$value['customer_image_id'].'">x</div> 
                                                            <div class="image-zone">
                                                            <img id="pro-img-'.$value['customer_image_id'].'" src="'.base_url().$value['image_path'].'">
                                                            </div>
                                                            </div>';
                                                       }
                                                    } 
                                                ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-md-12 text-right">
                                                    <button class="btn btn-primary" id="customerAddBtn" type="submit"> Submit </button>
                                                </div>
                                            </div>
                                    </div>
                            </div>
                        </div>
                    </form>
                    <!-- END RECENT PURCHASES -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
    <?php include"include/footer_content.php" ?>
</div>
    <!-- END MAIN -->
    <div class="clearfix"></div>

    </div>
    <!-- END WRAPPER -->
    <script>
        $(document).ready(function(){
            $('#datetimepicker1').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
            });

            $('#datetimepicker2').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
            });
             $('.select2').select2();
            $('.select-search').select2();
            $('#customer_description').val($('#customer_description').val().trim());
            $('#customer_short_description').val($('#customer_short_description').val().trim());
            //code for change filter according to category
            $("body" ).on( "change", "#customer_category", function(event){
                event.preventDefault();
                var category_id = $("#customer_category").val();
                $.get(base_url()+"filter/getFilterByCategoryId",{category_id:category_id}, function(result, status){
                    if(status == 'success'){
                        $('#customer_filter').html(result);
                    }
                });
            });
            //code for validate image size start here 11-07-2018
            var _URL = window.URL;
            $("#customer_img").change(function (e) {
                var file, img;
                // var maxWidth=415;
                // var maxHeight = 326;
                var error_msg = 'Aspect ratio of a image must be 3:1';
                var files = this.files;
                var error_status = 0;
                $("#customer_img_error").text('');
                for (let i = 0; i < files.length; i++) {
                    if ((file = this.files[i])) {
                        img = new Image();
                        img.onload = function () {
                            console.log("Width:" + this.width + "   Height: " + this.height);//this will give you image width and height and you can easily validate here....
                            var image_height = this.height;
                            var image_width = image_height * 3;
                            console.log("calculated height:"+image_height + " calculated:"+image_width);
                            if(image_width != this.width){
                                console.log(error_msg);
                                $("#customer_img_error").text(error_msg);
                                $('#customer_img_error').css("color",'red');
                                $(".edit_preview_images").remove();
                                $("#customer_img").val('');
                                error_status =1;
                                console.log('Error status is changes:'+error_status);
                                return;
                            }
                        };
                        img.src = _URL.createObjectURL(file);
                    }
                }
                setTimeout(function(){
                    if(error_status == 0){
                        console.log('read image function is call :'+error_status);
                        readImage(e);
                    }
                }, 500);
            });
            //code for validate image size end here 11-07-2018
        });
    //file upload code start here
    var old_deleted_image=[];
    $(document).ready(function() {
        //document.getElementById('customer_img').addEventListener('change',function(event){ readImage(event), false});
        $( ".preview-images-zone" ).sortable();

        $(document).on('click', '.image-cancel', function() {
            let no = $(this).data('no');
            let old_image_id = $(this).data('old_image_id');
            if(old_image_id !='' && typeof(old_image_id) !='undefined'){
                //alert(old_image_id);
                old_deleted_image.push(old_image_id);
                console.log(old_deleted_image);
                $('#old_deleted_image_arr').val(old_deleted_image);
            }
            $(".preview-image.preview-show-"+no).remove();
        });
    });
    var num = 4;
    function readImage(event) {
        if (window.File && window.FileList && window.FileReader) {
            var files = event.target.files; //FileList object
            var output = $(".preview-images-zone");
            //output.html('');
            for (let i = 0; i < files.length; i++) {
                var file = files[i];
                if (!file.type.match('image')) continue;
                
                var picReader = new FileReader();
                
                picReader.addEventListener('load', function (event) {
                    var picFile = event.target;
                    var html =  '<div class="preview-image edit_preview_images preview-show-' + num + '">' +
                                '<div class="image-cancel" data-no="' + num + '">x</div>' +
                                '<div class="image-zone"><img id="pro-img-' + num + '" src="' + picFile.result + '"></div>' +
                                '</div>';

                    output.append(html);
                    num = num + 1;
                });

                picReader.readAsDataURL(file);
            }
            //$("#customer_img").val('');
        } else {
            console.log('Browser not support');
        }
    }
    </script>
    <?php include"include/footer.php" ?>
</body>

</html>

