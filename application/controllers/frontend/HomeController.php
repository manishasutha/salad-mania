<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

    public function __construct(){
        
        parent::__construct();
        $this->load->model('General_model');
 if (!($this->session->userdata('language'))) {
            $data=$this->session->set_userdata('language', 'english');
            var_dump($this->session->userdata('language'));
        }   die;
       $data= $this->lang->load('content_form_' .$this->session->userdata('language'));

       
        
    }
    public function index(){
        
        $data['title']='Home';
        $data['page']='frontend/index';

        $select_fields=array(
            'cat_name','cat_id'
        );

        $where=array(
            'cat_type'=>'salad',
            'cat_status'=>1
        );
        
        $order_by    = 'cat_name desc';

        $data['categories']=$this->General_model->get_multiple_row('sm_categories',$select_fields,$where,$order_by);
        

        $this->load->view('frontend/home',$data,$user_language);
    }
    public function our_story(){
        $data['title']='Our Story';
        $data['page']='frontend/our_story';
        $this->load->view('frontend/home',$data);
    }
    public function make_your_own(){
        $data['title']='Make Your Own';
        $data['page']='frontend/make_your_own';
        $this->load->view('frontend/home',$data);
    }

//  public function change_language($lang = 'arabic') {
//         if($this->input->is_ajax_request()) {
//             $this->session->set_userdata('language',(($lang=='arabic') ? 'arabic' : 'english'));
//             echo true;
//         } else {
//             echo true;
//         }
//     }
}