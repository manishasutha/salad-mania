<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SaladController extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->model('General_model');

    }
    public function index(){
        //page title
        $data['title']='Menu';
        //view file path
        $data['page']='frontend/salad/index';
        
        //load categories 
        $select_fields=array(
            'cat_name','cat_id'
        );
        $where=array(
            'cat_type'=>'salad',
            'cat_status'=>1
        );
        $order_by    = 'cat_name desc';
        $data['categories']=$this->General_model->get_multiple_row('sm_categories',$select_fields,$where,$order_by);


        $this->load->view('frontend/home',$data);
    }
}