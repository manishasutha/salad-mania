<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiController extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('Main_model');
        $this->load->model('admin_model');
        $this->load->model('General_model');

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
    }

    //Get App Settings Function
    public function getSettings()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');

        $sql = "select * from sm_app_settings";
        $query = $this->Main_model->__callMasterquery($sql);

        $settings = [];
         $setting = [];

        if($query->num_rows() > 0 ) {
                $setting=$query->row_array();
                $setting['delivery_charge']=round($setting['delivery_charge'],'1');
                 $setting['tax_rate']=round($setting['tax_rate'],'1');
                 $settings[]=$setting;
            $result = array("status" => true, "message" => 'Settings get successfully',"message_ar" => 'الحصول على الإعدادات بنجاح', 'app_data' => $settings);
        }
        else
            $result = array("status" => false, "message" => 'No result found',"message_ar" => 'No result found');
        echo json_encode($result);
    }

    //Cities Listing Function
    public function getCities()
    {
        $sql = "select * from sm_cities order by city";
        $query = $this->Main_model->__callMasterquery($sql);

        $cities = array();

        if($query->num_rows() > 0 ) {
            foreach ($query->result() as $row) {
                $cities[] = $row;
            }

            $result = array("status" => true, "message" => 'Cities get successfully',"message_ar" => 'Cities get successfully', 'data' => $cities);
        }
        else
            $result = array("status" => false, "message" => 'No result found',"message_ar" => 'No result found');
        echo json_encode($result);
    }

    //Sign Up Function
    public function userSignup()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');

        extract($_POST);
        if(isset($full_name) && isset($email) && isset($phone) && isset($password)){

            //First check Email is already registered or not
            $option = array('where' => array('email' => $email));
            $emailQuery = $this->Main_model->__callSelect('sm_users',$option);

            //then check Phone is already registered or not
            $option = array('where' => array('phone' => $phone));
            $phoneQuery = $this->Main_model->__callSelect('sm_users',$option);

            if($emailQuery->num_rows() > 0 )
                $result = array("status" => false, "message" => 'Email already exist',"message_ar" => 'Email already exist');
            else if($phoneQuery->num_rows() > 0 )
                $result = array("status" => false, "message" => 'Phone No already exist',"message_ar" => 'Phone No already exist');
            else{
                //NOw Insert the record
                $password = encryptPassword($password);

                $userData = array('full_name' => $full_name,'email' => $email,'phone' => $phone, 'password'=>$password, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"));

                $last_id = $this->Main_model->__callInsert('sm_users',$userData);

                if($last_id){

                    //Create Entry in Address Table
                    $addData = array('user_id' => $last_id, 'addline1' => $addline1, 'addline2' => $addline2, 'city_id' => $city_id, 'add_type' => 'profile', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"));
                    $this->Main_model->__callInsert('sm_addresses',$addData);

                    $result = array("status" => true, "message" => 'User registered successfully',"message_ar" => 'User registered successfully');
                }
                else
                    $result = array("status" => false, "message" => 'Something went wrong.',"message_ar" => 'Something went wrong.');
            }
        }
        else
            $result = array("status" => false, "message" => 'Something went wrong.',"message_ar" => 'Something went wrong.');

        echo json_encode($result);
    }

    //Login Function
    public function login()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');

        extract($_POST);
        if(isset($email) && isset($password)){

            $password = encryptPassword($password);

            $option = array('where' => array('email' => $email,'password' => $password,));
            $query = $this->Main_model->__callSelect('sm_users',$option);

            if($query->num_rows() > 0 ){
                $row = $query->row();
                if($row->user_status == 1){
                    $result = array("status" => true, "message" => 'login successful',"message_ar" => 'login successful', 'user_id' => $row->id);
                }
                else
                    $result = array("status" => false, "message" => 'Account Deactivated or Email not verified',"message_ar" => 'Account Deactivated or Email not verified');

            }
            else
                $result = array("status" => false, "message" => 'Invalid Credentials', "message_ar" => 'Invalid Credentials');
        }
        else
            $result = array("status" => false, "message" => 'Something went wrong.',"message_ar" => 'Something went wrong.');

        echo json_encode($result);
    }

    //User Details Function
    public function userDetails()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Allow-Headers: Content-Type');
        header('content-type: application/json');

        extract($_POST);
        if(isset($user_id)){

            $option = array('where' => array('id' => $user_id));
            $query = $this->Main_model->__callSelect('sm_users',$option);

            if($query->num_rows() > 0 ){
                $row = $query->row();

                //Get Address Info
                $sql = "select a.add_id, a.addline1, a.addline2, a.city_id, c.city from sm_addresses a inner join sm_cities c on c.city_id = a.city_id where a.add_type = 'profile' AND a.user_id = ".$row->id;
                $query = $this->Main_model->__callMasterquery($sql);

                $arow = array();
                if($query->num_rows() > 0)
                    $arow = $query->row();

                $result = array("status" => 1, "message" => 'User info get successfully',"message_ar" => 'User info get successfully','data' => $row, 'address_data' => $arow);
            }
            else
                $result = array("status" => false, "message" => 'Invalid User ID',"message_ar" => 'Invalid User ID');
        }
        else
            $result = array("status" => false, "message" => 'Something went wrong.',"message_ar" => 'Something went wrong.');

        echo json_encode($result);
    }

    //Salads Listing Function
    public function getSaladListing()
    {
        //categories listing
        $table_name='sm_categories';
        $select_filds='cat_name,cat_name_arabic,cat_id,contains_make_own';
        $where_condition=array(
            'cat_status'=>1
        );
        $categories=$this->General_model->get_multiple_row($table_name, $select_filds, $where_condition);


        //salad listing
        if(isset($_POST['cat_id'])){
            $where=array(
                'cat_id'=>$_POST['cat_id'],
                'salad_status'=>1
            );
        }else{
            foreach ($categories as $cat){
                $cat_id=$cat->cat_id;
                break;
            }
            $where=array(
                'cat_id'=>$cat_id,
                'salad_status'=>1
            );
        }

        //get data from salad table
        $table_name='sm_salads';
        $select_filds='salad_id,salad_status,salad_name,salad_name_arabic,salad_desc,salad_desc_arabic,cat_id,salad_img,salad_price,salad_calories';
        $where_condition=$where;
        $salads=$this->General_model->get_multiple_row($table_name, $select_filds, $where_condition);
        $salad_data=array();
        if($salads){
            foreach ($salads as $salad){
                $salad_details=array();

                $this->db->select('');
                $this->db->from('sm_ingredients as i');
                $this->db->join('sm_salad_ingredients as si','si.ing_id=i.ing_id');
                $this->db->where('si.salad_id',$salad->salad_id);
                $this->db->where('i.ing_status',1);
                $query=$this->db->get();
                $ing_data=$query->result();
                //total ingrediat name
                $ing_name=array_column($ing_data,'ing_name');
                //total ingrediat name
                $ing_name_arabic=array_column($ing_data,'ing_name_arabic');
                //total ingrediant price
                $ing_price=array_sum(array_column($ing_data,'ing_price'));
                //total ingrediant calories
                $ing_calories=array_sum(array_column($ing_data,'ing_calories'));

                //fetching into array
                $salad_details['salad_id']=$salad->salad_id;
                $salad_details['salad_name']=$salad->salad_name;
                $salad_details['salad_name_arabic']=$salad->salad_name_arabic;
                $salad_details['salad_desc']=$salad->salad_desc;
                $salad_details['salad_desc_arabic']=$salad->salad_desc_arabic;
                $salad_details['cat_id']=$salad->cat_id;
                $salad_details['cat_name']=getSingleFieldDetail('cat_id',$salad->cat_id,'cat_name','sm_categories');
                $salad_details['cat_name_arabic']=getSingleFieldDetail('cat_id',$salad->cat_id,'cat_name_arabic','sm_categories');
                $salad_details['salad_price']=round($salad->salad_price,'1');
                $salad_details['totalCalories']=$salad->salad_calories;
                $salad_details['ing_name']=implode(',',$ing_name);
                $salad_details['ing_name_arabic']=implode(',',$ing_name_arabic);
                $salad_details['salad_img']=$salad->salad_img;


                //cart and favourties value check
                if(isset($_POST['user_id'])){
                    $table_name = 'sm_favourite_salads';
                    $select_filds = 'fav_id';
                    $where_condition = array(
                        'salad_id' => $salad->salad_id,
                        'user_id'=>$_POST['user_id']
                    );
                    $check_fav = $this->General_model->get_row($table_name, $select_filds, $where_condition);
                    if($check_fav){
                        $salad_details['is_favourite']=1;
                    }else{
                        $salad_details['is_favourite']=0;
                    }

                    $table_name = 'sm_carts';
                    $select_filds = 'cart_id';
                    $where_condition = array(
                        'salad_id' => $salad->salad_id,
                        'user_id'=>$_POST['user_id']
                    );
                    $cart_check = $this->General_model->get_row($table_name, $select_filds, $where_condition);
                    if($cart_check){
                        $salad_details['is_cart']=1;
                    }else{
                        $salad_details['is_cart']=0;
                    }

                }else{
                    $salad_details['is_favourite']=0;
                    $salad_details['is_cart']=0;
                }

                $salad_data[]=$salad_details;


            }
            $response=array(
                'status'=>true,
                'saladImgUrl'=>base_url().'uploads/salads/',
                'salad_data'=>$salad_data,
                'categories'=>$categories,
            );
        }
        else{
            $response=array(
                'status'=>true,
                'message'=>'No data found',
                'message_ar'=>'No data found',
                'saladImgUrl'=>base_url().'uploads/salads/',
                'salad_data'=>$salad_data,
                'categories'=>$categories,

            );
        }
        echo json_encode($response);
    }

    //Salads Details Function
    public function getSaladDetails()
    {
        if(isset($_POST['salad_id'])) {
            //get data from salad table
            $where = array(
                'salad_id' => $_POST['salad_id']
            );
            $table_name = 'sm_salads';
            $select_filds = 'salad_id,salad_name,salad_name_arabic,salad_desc,salad_desc_arabic,cat_id,salad_img,salad_price,salad_calories';
            $where_condition = $where;
            $salad = $this->General_model->get_row($table_name, $select_filds, $where_condition);
            if ($salad) {
                $salad_data['salad_id'] = $salad->salad_id;
                $salad_data['salad_name'] = $salad->salad_name;
                $salad_data['salad_name_arabic'] = $salad->salad_name_arabic;
                $salad_data['salad_desc'] = $salad->salad_desc;
                $salad_data['salad_desc_arabic'] = $salad->salad_desc_arabic;
                $salad_data['cat_id'] = $salad->cat_id;
                $salad_data['cat_name'] = getSingleFieldDetail('cat_id', $salad->cat_id, 'cat_name', 'sm_categories');
                $salad_data['cat_name_arabic'] = getSingleFieldDetail('cat_id', $salad->cat_id, 'cat_name_arabic', 'sm_categories');
                $salad_data['salad_img'] = $salad->salad_img;
                 $salad_data['salad_calories'] = $salad->salad_calories;


                $this->db->select('i.ing_id,i.ing_name,i.ing_name_arabic,si.quantity,i.ing_image,i.ing_price,i.ing_calories,i.ing_weight,i.unit_type');
                $this->db->from('sm_salad_ingredients  as si');
                // $this->db->join('sm_ingredients_subcats as is', 'si.subcat_id=is.subcat_id','LEFT');
                $this->db->join('sm_ingredients as i', 'i.ing_id=si.ing_id','LEFT');
                $this->db->where('si.salad_id', $salad->salad_id);
                $this->db->where('i.ing_status',1);
                 $this->db->group_by('si.data_id');
                $query = $this->db->get();
                $ing_data = $query->result();


               /*$this->db->select('i.ing_id,i.ing_name,i.ing_name_arabic,i.ing_weight,i.unit_type,i.ing_price,si.quantity,i.ing_calories,i.ing_image');
               $this->db->from('sm_ingredients as i');
               $this->db->join('sm_salad_ingredients as si', 'si.ing_id=i.ing_id');
               $this->db->where('si.salad_id', $salad->salad_id);
               $query = $this->db->get();
               $ing_data = $query->result();*/

                //total ingrediat name
                $ing_name = array_column($ing_data, 'ing_name');
                //total ingrediat name
                $ing_name_arabic = array_column($ing_data, 'ing_name_arabic');
                //total ingrediant price
                $ing_price = array_sum(array_column($ing_data, 'ing_price'));
                //total ingrediant calories
                $ing_calories = array_sum(array_column($ing_data, 'ing_calories'));
                $salad_ing_data=array();
                if ($ing_data) {
                    foreach ($ing_data as $ing) {
                        $salad_ing_data[]=$ing;
                    }
                }



                //fetching into array
                $salad_data['salad_id'] = $salad->salad_id;
                $salad_data['salad_name'] = $salad->salad_name;
                $salad_data['salad_name_arabic'] = $salad->salad_name_arabic;
                $salad_data['salad_desc'] = $salad->salad_desc;
                $salad_data['salad_desc_arabic'] = $salad->salad_desc_arabic;
                $salad_data['cat_id'] = $salad->cat_id;
                $salad_data['cat_name'] = getSingleFieldDetail('cat_id', $salad->cat_id, 'cat_name', 'sm_categories');
                $salad_data['cat_name_arabic'] = getSingleFieldDetail('cat_id', $salad->cat_id, 'cat_name_arabic', 'sm_categories');
                $salad_data['salad_price'] = round($salad->salad_price,'1');
                $salad_data['totalCalories'] = $salad->salad_calories;
                $salad_data['ing_name'] = implode(',', $ing_name);
                $salad_data['ing_name_arabic'] = implode(',', $ing_name_arabic);
                $salad_data['salad_img'] = $salad->salad_img;

                $response = array(
                    'status' => true,
                    'saladImgUrl' => base_url() . 'uploads/salads/',
                    'ingImgUrl' => base_url() . 'uploads/ingredients/',
                    'salad_data' => $salad_data,
                    'salad_ing_data' => $salad_ing_data
                );
            }
        }else{
            $response=array(
                'status'=>false,
                'message'=>'no data found',
                'message_ar'=>'no data found'
            );
        }

        echo json_encode($response);
    }

    //Add Item in Cart
    public function addItemInCart()
    {
        if(isset($_POST['salad_type'])){
            if($_POST['salad_type']=='ready_made'){
                if(isset($_POST['special_request'])){
                    $sepcial_request=$_POST['special_request'];
                }else{
                    $sepcial_request='';
                }
                if(isset($_POST['user_id']) && isset($_POST['salad_id']) && isset($_POST['dressing_mixed_in']) && isset($_POST['quantity'])  ){
                    $insert_data=array(
                        'user_id'=>$_POST['user_id'],
                        'salad_type'=>1,
                        'cat_id'=>$_POST['cat_id'],
                        'salad_image'=>getSingleFieldDetail('salad_id',$_POST['salad_id'],'salad_img','sm_salads'),
                        'salad_id'=>$_POST['salad_id'],
                        'dressing_mixed_in'=>$_POST['dressing_mixed_in'],
                        'quantity'=>$_POST['quantity'],
                        'special_request'=>$sepcial_request
                    );
                    $table_name='sm_carts';
                    $last_insert_id=$this->General_model->get_lastinsertid_after_insertdata($table_name,$insert_data);
                    if (isset($_POST['ingrediants'])) {
                        $ing_ids = json_decode($_POST['ingrediants']);
                        if ($ing_ids) {
                            foreach ($ing_ids as $ing_id) {
                                $table_name = 'sm_cart_ingredients';
                                $insert_details = array(
                                    'ing_id' => $ing_id->ing_id,
                                    'quantity' => $ing_id->ing_qty,
                                    'cart_id' => $last_insert_id,
                                    'subcat_id'=>getSingleFieldDetail('ing_id',$ing_id->ing_id,'subcat_id','sm_ingredients_subcats'),
                                );
                                $this->General_model->insert_details($table_name, $insert_details);
                            }
                        }
                    }
                    $response=array(
                        'status'=>true,
                        'message'=>'Successfully inserted into cart',
                        'message_ar'=>'Successfully inserted into cart'
                    );


                }else{
                    $response=array(
                        'status'=>false,
                        'message'=>'Please fill all data',
                        'message_ar'=>'Please fill all data'
                    );
                }

            }
            elseif ($_POST['salad_type'] == 'create_own'){
                if(isset($_POST['user_id']) && isset($_POST['salad_name']) && isset($_POST['dressing_mixed_in']) && isset($_POST['quantity'])) {
                    if(isset($_POST['special_request'])){
                        $epcial_request=$_POST['special_request'];
                    }else{
                        $epcial_request='';
                    }
                    $insert_data = array(
                        'user_id' => $_POST['user_id'],
                        'salad_type' => 2,
                        'salad_name' => $_POST['salad_name'],
                        'salad_image'=>'custom.png',
                        'cat_id'=>$_POST['cat_id'],
                        'dressing_mixed_in' => $_POST['dressing_mixed_in'],
                        'quantity' => $_POST['quantity'],
                        'special_request'=>$epcial_request
                    );
                    $table_name = 'sm_carts';
                    $last_insert_id = $this->General_model->get_lastinsertid_after_insertdata($table_name, $insert_data);

                    if (isset($_POST['ingrediants'])) {
                        $ing_ids = json_decode($_POST['ingrediants']);
                        if ($ing_ids) {
                            foreach ($ing_ids as $ing_id) {
                                $table_name = 'sm_cart_ingredients';
                                $insert_details = array(
                                    'ing_id' => $ing_id->ing_id,
                                    'quantity' => $ing_id->ing_qty,
                                    'cart_id' => $last_insert_id,
                                    'subcat_id'=>getSingleFieldDetail('ing_id',$ing_id->ing_id,'subcat_id','sm_ingredients_subcats'),
                                );
                                $this->General_model->insert_details($table_name, $insert_details);
                            }
                        }
                    }
                    $response=array(
                        'status'=>true,
                        'message'=>'Successfully inserted into cart',
                        'message_ar'=>'Successfully inserted into cart'
                    );
                }
                else{
                    $response=array(
                        'status'=>false,
                        'message'=>'Something went wrong!!Please try again',
                        'message_ar'=>'Something went wrong!!Please try again'
                    );
                }


            }else{
                $response=array(
                    'status'=>false,
                    'message'=>'Something went wrong!!Please try again',
                    'message_ar'=>'Something went wrong!!Please try again'
                );
            }
        }else{
            $response=array(
                'status'=>false,
                'message'=>'Something went wrong!!Please try again',
                'message_ar'=>'Something went wrong!!Please try again'
            );
        }

        echo json_encode($response);
    }



    //confirm Cart
    public function confirmCart()
    {
        if(isset($_POST['user_id']) ){
            if(isset($_POST['cart_items'])){
                $items = json_decode($_POST['cart_items'], true);
                foreach ($items as $key => $val) {

                    $update_data=array(
                        'dressing_mixed_in' => $val['dressing_mixed_in'],
                        'quantity' => $val['quantity'],
                        'updated_at' => date("Y-m-d H:i:s")
                    );
                    $where_condition=array('cart_id'=>$val['cart_id']);
                    $table_name='sm_carts';

                    $status=$this->General_model->update_details($table_name, $update_data, $where_condition);

                    if($status){
                        $response=array(
                            'status' => true,
                            'message' => 'cart successfully updated.',
                            'message_ar' => 'cart successfully updated.'
                        );
                    }
                }
            }else{
                $response=array(
                    'status' => true,
                    'message' => 'cart successfully updated.',
                     'message_ar' => 'cart successfully updated.'
                );
            }


        }else{
            $response=array(
                'status'=>false,
                'message'=>'Something went wrong',
                'message_ar'=>'Something went wrong'
            );
        }
        echo json_encode($response);
    }




    //Cart Listing Function
    public function getCartListing()
    {
        if(isset($_POST['user_id'])){
            $table_name='sm_carts';
            $select_filds='cart_id,user_id,salad_type,salad_id,salad_name,quantity,dressing_mixed_in,cat_id';
            $where_condition=array(
                'user_id'=>$_POST['user_id']
            );
            $salads=$this->General_model->get_multiple_row($table_name, $select_filds, $where_condition);
            if($salads){
                $data=array();
                foreach ($salads as $salad){
                    $salad_data=array();
                    $salad_data['cart_id']=$salad->cart_id;
                    if($salad->salad_type == 1){
                        $salad_data['is_custom']=0;
                        $salad_data['salad_id']=$salad->salad_id;
                        $salad_data['dressing_mixed_in']=$salad->dressing_mixed_in;


                        $table_name='sm_salads';
                        $select_filds='salad_id,salad_name,salad_name_arabic,salad_desc,salad_desc_arabic,salad_img,cat_id,salad_price,salad_calories';
                        $where_condition=array(
                            'salad_id'=>$salad->salad_id,
                        );
                        $get_salad_data=$this->General_model->get_row($table_name, $select_filds, $where_condition);
                        $salad_data['salad_name']=$get_salad_data->salad_name;
                        $salad_data['salad_name_arabic']=$get_salad_data->salad_name_arabic;
                        $salad_data['salad_desc']=$get_salad_data->salad_desc;
                        $salad_data['salad_desc_arabic']=$get_salad_data->salad_desc_arabic;
                        $salad_data['salad_img']=$get_salad_data->salad_img;
                        $salad_data['cat_id']=$get_salad_data->cat_id;
                        $salad_data['cat_name']=getSingleFieldDetail('cat_id',$get_salad_data->cat_id,'cat_name','sm_categories');
                        $salad_data['cat_name_arabic']=getSingleFieldDetail('cat_id',$salad->cat_id,'cat_name_arabic','sm_categories');

                        $where_condition=array(
                            'si.salad_id'=>$salad->salad_id,
                            'sc.subcat_status'=>1,
                        );

                        //salad ingrediant
                        $this->db->select('i.ing_id,i.ing_name,i.ing_name_arabic,sc.subcat_name,sc.subcat_name_arabic,i.ing_weight,i.unit_type,i.ing_price,si.quantity,i.ing_calories,i.ing_image');
                        $this->db->from('sm_ingredients as i');
                        $this->db->join('sm_salad_ingredients as si', 'si.ing_id=i.ing_id');
                         $this->db->join('sm_subcategories as sc', 'si.subcat_id=sc.id');
                        $this->db->where($where_condition);
                        $this->db->where('i.ing_status',1);
                        $query = $this->db->get();
                        $ing_data = $query->result();
                        //total ingrediat name
                        $ing_name = array_column($ing_data, 'ing_name');
                        //total ingrediat name
                        $ing_name_arabic = array_column($ing_data, 'ing_name_arabic');
                         //total ingrediat name
                        $subcat_name = array_column($ing_data, 'subcat_name');
                        //total ingrediat name
                        $subcat_name_arabic = array_column($ing_data, 'subcat_name_arabic');
                        //total ingrediant price
                        $ing_price = array_sum(array_column($ing_data, 'ing_price'));
                        //total ingrediant calories
                        $ing_calories = array_sum(array_column($ing_data, 'ing_calories'));

                       
                        //extra ingrediant
                        $where=array(
                            'ci.cart_id'=>$salad->cart_id,
                            'sc.subcat_status'=>1,

                        );
                        $this->db->select('i.ing_id,i.ing_name,i.ing_name_arabic,i.ing_weight,i.unit_type,sc.subcat_name,sc.subcat_name_arabic,i.ing_price,ci.quantity as ing_qty,i.ing_calories,i.ing_image');
                        $this->db->from('sm_cart_ingredients as ci');
                        $this->db->join('sm_ingredients as i', 'i.ing_id=ci.ing_id');
                         $this->db->join('sm_subcategories as sc', 'ci.subcat_id=sc.id');
                        $this->db->where($where);
                        $this->db->where('i.ing_status',1);
                        $query = $this->db->get();
                        $extraing_data = $query->result();
                        $extra_ing_name=array();
                        $extra_ing_name_arabic=array();
                        $extra_subcat_name=array();
                        $extra_subcat_name_arabic=array();
                        $extra_ing_price=0;
                        $extra_ing_calories=0;
                        if($extraing_data){
                            //total ingrediat name
                            $extra_ing_name = array_column($extraing_data, 'ing_name');
                            //total ingrediat name
                            $extra_ing_name_arabic = array_column($extraing_data, 'ing_name_arabic');
                              //total subcat name
                            $extra_subcat_name = array_column($extraing_data, 'subcat_name');
                            //total subcat name
                            $extra_subcat_name_arabic = array_column($extraing_data, 'subcat_name_arabic');
                            //total ingrediant price
                            $extra_ing_price = array_sum(array_column($extraing_data, 'ing_price'));
                            //total ingrediant calories
                            $extra_ing_calories = array_sum(array_column($extraing_data, 'ing_calories'));
                        }
                        $price=$get_salad_data->salad_price+$extra_ing_price;
                        $salad_data['ings']=$extraing_data;
                        $salad_data['item_total_price']=round($price,'1');
                        $salad_data['totalCalories']=$get_salad_data->salad_calories+$extra_ing_calories;
                        $salad_data['subcat_name']=implode(',',array_merge($subcat_name,$extra_subcat_name));
                        $salad_data['subcat_name_arabic']=implode(',',array_merge($subcat_name_arabic,$extra_subcat_name_arabic));
                        $salad_data['ing_name']=implode(',',array_merge($ing_name,$extra_ing_name));
                        $salad_data['ing_name_arabic']=implode(',',array_merge($ing_name_arabic,$extra_ing_name_arabic));
                        $salad_data['quantity']=$salad->quantity;

                        $data[]=$salad_data;
                    }elseif ($salad->salad_type ==2){
                        $salad_data['is_custom']=1;
                        $salad_data['salad_id']=0;
                        $salad_data['dressing_mixed_in']=$salad->dressing_mixed_in;
                        $salad_data['salad_name']=$salad->salad_name;
                        $salad_data['salad_name_arabic']=$salad->salad_name;
                        $salad_data['salad_desc']=' ';
                        $salad_data['salad_desc_arabic']=' ';
                        $salad_data['salad_img']='custom.png';


                        //extra ingrediant
                        $where=array(
                            'ci.cart_id'=>$salad->cart_id,
                            'sc.subcat_status'=>1,
                        );
                            $this->db->select('i.ing_id,i.ing_name,i.ing_name_arabic,sc.subcat_name,sc.subcat_name_arabic,i.ing_weight,i.unit_type,i.ing_price,ci.quantity as ing_qty,i.ing_calories,i.ing_image');
                        $this->db->from('sm_cart_ingredients as ci');
                        $this->db->join('sm_ingredients as i', 'i.ing_id=ci.ing_id');
                        $this->db->join('sm_subcategories as sc', 'ci.subcat_id=sc.id');
                        $this->db->where($where);
                        $query = $this->db->get();
                        $extraing_data = $query->result();
                        $extra_ing_name=array();
                        $extra_ing_name_arabic=array();
                        $extra_subcat_name=array();
                        $extra_subcat_name_arabic=array();
                        $extra_ing_price=0;
                        $extra_ing_calories=0;
                        if($extraing_data){
                            //total ingrediat name
                            $extra_ing_name = array_column($extraing_data, 'ing_name');
                            //total ingrediat name
                            $extra_ing_name_arabic = array_column($extraing_data, 'ing_name_arabic');
                              //total subcat name
                            $extra_subcat_name = array_column($extraing_data, 'subcat_name');
                            //total subcat name
                            $extra_subcat_name_arabic = array_column($extraing_data, 'subcat_name_arabic');
                            //total ingrediant price
                            $extra_ing_price = array_sum(array_column($extraing_data, 'ing_price'));
                            //total ingrediant calories
                            $extra_ing_calories = array_sum(array_column($extraing_data, 'ing_calories'));

                        }
                        $salad_data['ings']=$extraing_data;
                        $salad_data['item_total_price']=round($extra_ing_price,'1');
                        $salad_data['totalCalories']=$extra_ing_calories;
                        $salad_data['ing_name']=implode(',',$extra_ing_name);
                        $salad_data['ing_name_arabic']=implode(',',$extra_ing_name_arabic);
                        $salad_data['subcat_name']=implode(',',$extra_subcat_name);
                        $salad_data['subcat_name_arabic']=implode(',',$extra_subcat_name_arabic);
                        $salad_data['quantity']=$salad->quantity;
                        $salad_data['cat_id']=$salad->cat_id;
                        $salad_data['cat_name']=getSingleFieldDetail('cat_id',$salad->cat_id,'cat_name','sm_categories');
                        $salad_data['cat_name_arabic']=getSingleFieldDetail('cat_id',$salad->cat_id,'cat_name_arabic','sm_categories');


                        $data[]=$salad_data;

                    }

                }
                $response=array(
                    'status'=>true,
                    'message'=> "Cart listing get successfully",
                     'message_ar'=> "Cart listing get successfully",
                    'saladImgUrl'=> base_url()."uploads/salads/",
                    'data'=>$data
                );
            }else{
                $response=array(
                    'status'=>false,
                    'message'=>'No data found',
                    'message_ar'=>'No data found'
                );
            }



        }else{
            $response=array(
                'status'=>false,
                'message'=>'Please fill details',
                 'message_ar'=>'Please fill details'
            );
        }
        echo json_encode($response);


//        header('Access-Control-Allow-Origin: *');
//        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
//        header('Access-Control-Max-Age: 86400');
//        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
//        header("Access-Control-Allow-Headers: *");
//        header('content-type: application/json');
//
//        if(isset($_POST['user_id']))
//        {
//            extract($_POST);
//            $sql = "select c.id, c.user_id, c.salad_id, c.quantity, c.dressing_mixed_in, c.item_total_price, s.salad_name, s.salad_price, s.salad_img, s.custom_salad, s.ing_ids, s.salad_calories, s.cat_id from sm_carts as c inner join sm_salads as s on s.salad_id = c.salad_id where s.salad_status = 1 AND c.user_id = $user_id";
//            $query = $this->Main_model->__callMasterquery($sql);
//
//            if($query->num_rows() > 0 ){
//                $salad_data = array();
//                $ingName = array();
//                $totalCalories = 0;
//
//                foreach ($query->result() as $row){
//                    if($row->ing_ids != NULL){
//                        $ingIds = explode(',', $row->ing_ids);
//
//                        foreach ($ingIds as $id) {
//                            $sql = "select i.ing_id, i.ing_name, i.ing_calories from sm_ingredients as i where i.ing_id = $id";
//
//                            $query = $this->Main_model->__callMasterquery($sql);
//
//                            if($query->num_rows() > 0){
//                                $irow = $query->row();
//                                $ingName[] = $irow->ing_name;
//                            }
//                        }
//                    }
//
//                    $cat_name = '';
//                    if($row->cat_id != 0){
//                        $option = array('where' => array('cat_id' => $row->cat_id));
//                        $catData = $this->Main_model->__callSelect('sm_categories',$option);
//                        if($catData->num_rows() > 0){
//                            $rs = $catData->row();
//                            $cat_name = $rs->cat_name;
//                        }
//                    }
//
//                    $result = array('cart_id' => $row->id,'salad_id' => $row->salad_id,'salad_price' => round($row->salad_price), 'quantity' => $row->quantity, 'dressing_mixed_in' => $row->dressing_mixed_in, 'item_total_price' => round($row->item_total_price), 'salad_name' => $row->salad_name, 'salad_img' => $row->salad_img, 'is_custom' => $row->custom_salad, 'cat_name' => $cat_name, 'ing_name' => implode(', ', $ingName), 'totalCalories' => round($row->salad_calories));
//
//                    $salad_data[] = $result;
//                }
//
//                $imgUrl = base_url().'uploads/salads/';
//                $result = array("status" => true, "message" => 'Cart listing get successfully', 'saladImgUrl' => $imgUrl, 'data' => $salad_data);
//            }
//            else
//                $result = array("status" => false, "message" => 'No result found');
//        }
//        else
//            $result = array("status" => false, "message" => 'Something went wrong');
//
//        echo json_encode($result);
    }

    //Add Item in Farourite List
    public function addFavouriteItem()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');

        extract($_POST);
        if(isset($user_id) && isset($salad_id))
        {
            if($action == 'add'){ //Add in Favourite List
                $record = array('user_id' => $user_id, 'salad_id' => $salad_id, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"));
                $status = $this->Main_model->__callInsert('sm_favourite_salads',$record);
                $msg = 'Item successfully added into the favourite list';
                $msg_ar = 'Item successfully added into the favourite list';
            }
            else{ //Delete from Favourite List
                $where = array("user_id" => $user_id, "salad_id" => $salad_id);
                $status = $this->Main_model->__callDelete('sm_favourite_salads', $where);
                $msg = 'Item successfully deleted from the favourite list';
                $msg_ar = 'Item successfully deleted from the favourite list';
            }

            if($status)
                $result = array('status' => true, 'message' => $msg,'message_ar' => $msg_ar);
            else
                $result = array('status' => false, 'message' => 'Something went wrong.','message_ar' => 'Something went wrong.');
        }
        else
            $result = array("status" => false, "message" => 'Something went wrong',"message_ar" => 'Something went wrong');

        echo json_encode($result);
    }

    //Favourite Items Listing Function
    public function getFavouriteItems()
    {
        if(isset($_POST['user_id'])){
            if(isset($_POST['user_id'])){
                $table_name='sm_favourite_salads';
                $select_fields='';
                $where_condition=array(
                    'user_id'=>$_POST['user_id']
                );
                $salads=$this->General_model->get_multiple_row($table_name,$select_fields,$where_condition);
                if($salads){
                    foreach($salads as $salad){
                        $where = array(
                            'salad_id' => $salad->salad_id
                        );
                        $table_name = 'sm_salads';
                        $select_filds = 'salad_id,salad_name,salad_name_arabic,salad_desc,salad_desc_arabic,cat_id,salad_img,salad_price,salad_calories';
                        $where_condition = $where;
                        $salad = $this->General_model->get_row($table_name, $select_filds, $where_condition);
                        $salad_data=array();
                        if ($salad) {
                            $salad_data['salad_id'] = $salad->salad_id;
                            $salad_data['salad_name'] = $salad->salad_name;
                            $salad_data['salad_name_arabic'] = $salad->salad_name_arabic;
                            $salad_data['salad_desc'] = $salad->salad_desc;
                            $salad_data['salad_desc_arabic'] = $salad->salad_desc_arabic;
                            $salad_data['cat_id'] = $salad->cat_id;
                            $salad_data['cat_name'] = getSingleFieldDetail('cat_id', $salad->cat_id, 'cat_name', 'sm_categories');
                            $salad_data['cat_name_arabic'] = getSingleFieldDetail('cat_id', $salad->cat_id, 'cat_name_arabic', 'sm_categories');
                            $salad_data['salad_img'] = $salad->salad_img;


                            $this->db->select('i.ing_id,i.ing_name,i.ing_name_arabic,i.ing_weight,i.unit_type,i.ing_price,si.quantity,i.ing_calories,i.ing_image');
                            $this->db->from('sm_ingredients as i');
                            $this->db->join('sm_salad_ingredients as si', 'si.ing_id=i.ing_id');
                            $this->db->where('si.salad_id', $salad->salad_id);
                            $query = $this->db->get();
                            $ing_data = $query->result();
                            //total ingrediat name
                            $ing_name = array_column($ing_data, 'ing_name');
                            //total ingrediat name
                            $ing_name_arabic = array_column($ing_data, 'ing_name_arabic');
                            //total ingrediant price
                            $ing_price = array_sum(array_column($ing_data, 'ing_price'));
                            //total ingrediant calories
                            $ing_calories = array_sum(array_column($ing_data, 'ing_calories'));
                            $salad_ing_data = array();
                            if ($ing_data) {
                                foreach ($ing_data as $ing) {
                                    $salad_ing_data[] = $ing;
                                }
                            }


                            //fetching into array
                            $salad_data['salad_id'] = $salad->salad_id;
                            $salad_data['salad_name'] = $salad->salad_name;
                            $salad_data['salad_name_arabic'] = $salad->salad_name_arabic;
                             $salad_data['salad_desc'] = $salad->salad_desc;
                            $salad_data['salad_desc_arabic'] = $salad->salad_desc_arabic;
                            $salad_data['cat_id'] = $salad->cat_id;
                            $salad_data['cat_name'] = getSingleFieldDetail('cat_id', $salad->cat_id, 'cat_name', 'sm_categories');
                            $salad_data['cat_name_arabic'] = getSingleFieldDetail('cat_id', $salad->cat_id, 'cat_name_arabic', 'sm_categories');
                            $salad_data['salad_price'] = round($salad->salad_price,'1');
                            $salad_data['totalCalories'] = $salad->salad_calories;
                            $salad_data['ing_name'] = implode(',', $ing_name);
                            $salad_data['ing_name_arabic'] = implode(',', $ing_name_arabic);
                            $salad_data['salad_img'] = $salad->salad_img;

                        }
                        $data[]=$salad_data;
                    }
                    $response = array(
                        'status' => true,
                        'message' => "Cart listing get successfully",
                        'message_ar' => "Cart listing get successfully",
                        'saladImgUrl' => base_url() . "uploads/salads/",
                        'data' => $data
                    );



                }else{
                    $response=array(
                        'status'=>true,
                        'message'=>'No Record found',
                        'message_ar'=>'No Record found'
                    );
                }





            }
        }

        echo json_encode($response);


    }


    //Address Listing Function
    public function getAddresses()
    {
        if($_POST['user_id'] && $_POST['add_type']){
            $where=array(
                'add.user_id'=>$_POST['user_id'],
                'add.add_type'=>$_POST['add_type']
            );
            $this->db->select('');
            $this->db->from('sm_addresses as add');
            $this->db->join('sm_users as u','u.id=add.user_id');
            $this->db->order_by('add.add_id DESC');
            $this->db->where($where);
            $query=$this->db->get();
            $address=$query->row();
            if($address){
                $response=array(
                    'status'=>true,
                    'message'=>'Addresses get successfully.',
                     'message_ar'=>'Addresses get successfully.',
                    'address_data'=>$address
                );
            }else{
                $response=array(
                    'status'=>false,
                    'message'=>'no record found',
                     'message_ar'=>'no record found'
                );
            }
        }
        else{
            $response=array(
                'status'=>false,
                'message'=>'no record found',
                'message_ar'=>'no record found'
            );
        }
        echo json_encode($response);
    }

    //Edit Address - Submit
    public function updateAddress()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');

        //Add Address-submit
        extract($_POST);
        if(isset($user_id))
        {

            $addData = array('addline1' => $addline1, 'addline2' => $addline2, 'city_id' => $city_id,'user_id' => $user_id);
            $last_id  = $this->Main_model->__callInsert('sm_addresses',$addData);

            if( $last_id){
                $addressData = array();

                $sql = "select id,full_name,phone from sm_users where id = $user_id";
                $query = $this->Main_model->__callMasterquery($sql);
                $row = $query->row();
                $name=$row->full_name;$phone=$row->phone;

                $sql = "select * from sm_cities where city_id = $city_id";
                $query = $this->Main_model->__callMasterquery($sql);
                $cityrow = $query->row();
                $cityname=$cityrow->city;

                $result = array('user_id' => $user_id, 'add_id' => $last_id,'user_name' => $name,'Phone' => $phone,'addline1' => $addline1,'addline2' => $addline2, 'city' => $cityname);
                $addressData[] = $result;
                $result = array('status' => true, 'message' => 'Addresses updated successfully.','message_ar' => 'Addresses updated successfully.','address_data' => $addressData);

            }
            else{
                $result = array('status' => true, 'message' => 'Addresses not updated successfully.','message_ar' => 'Addresses not updated successfully.');

            }
        }

        else
            $result = array("status" => false, "message" => 'Something went wrong', "message_ar" => 'Something went wrong');

        echo json_encode($result);
    }


    //Order Listing Function
    public function getOrderListing()
    {
        if(isset($_POST['user_id'])){
	
            $where=array(
                'sm_orders.user_id'=>$_POST['user_id']
            );
            $this->db->select('sm_orders.*,sm_addresses.add_id,sm_addresses.user_id,sm_addresses.addline1,sm_addresses.addline2,sm_addresses.city_id,sm_addresses.mobile_no');
            $this->db->from('sm_orders');
            $this->db->join('sm_addresses','sm_orders.address_id=sm_addresses.add_id','left');
            $this->db->where($where);
            $this->db->order_by("order_id", "desc");
            $query=$this->db->get();
            $orders=$query->result_array();
            if($orders){
                $order_data=array();
               foreach ($orders as $order){
                   $where=array(
                       'order_id'=>$order['order_id']
                   );
                   $this->db->select('');
                   $this->db->from('sm_order_details');
                   $this->db->where($where);
                   $query=$this->db->get();
                   $salads=$query->result_array();
                   $salad_data=array();
                   foreach ($salads as $salad ){
                       if($salad['salad_id'] == 0){

                       }else {
                           $table = 'sm_salads';
                           $select = 'salad_desc,salad_desc_arabic';
                           $where = array(
                               'salad_id' => $salad['salad_id']
                           );
                           $salads = $this->General_model->get_row($table, $select, $where);
                           $salad['salad_desc'] = $salads->salad_desc;
                           $salad['salad_desc_arabic'] = $salads->salad_desc_arabic;
                       }
                       $table='sm_order_ingredients';
                       $select='ing_name,ing_name_arabic';
                       $where=array(
                           'odetail_id'=>$salad['odetail_id']
                       );
                       $ingrediants=$this->General_model->get_multiple_row($table,$select,$where);
                       $salad['ing_name']=implode(',',array_column($ingrediants,'ing_name'));
                       $salad['ing_name_arabic']=implode(',',array_column($ingrediants,'ing_name_arabic'));
                       $salad_data[]=$salad;
                   }
                   $order['salad_data']=$salad_data;
                   $order['orderId']='SM-'.encryptId($order['order_id']);
                   $order['ordered_on']=date('d-M-Y H:i:s', strtotime($order['created_at']));
                   //order status
                   if($order['o_status'] == 7)
                       $order['order_status'] = 'Failed';
                   else if($order['delivery_status'] == 1)
                       $order['order_status'] = 'Created';
                   else if($order['delivery_status'] == 2)
                       $order['order_status'] = 'Pending Pickup';
                   else if($order['delivery_status'] == 3)
                       $order['order_status'] = 'Picked';
                   else if($order['delivery_status'] == 4)
                       $order['order_status'] = 'Completed';
                   else if($order['delivery_status'] == 5)
                       $order['order_status'] = 'Canceled';
                   else
                       $order['order_status'] = 'Failed';


                   $order_data[]=$order;
               }
                $response=array(
                    'status'=>true,
                    'message'=>'order get successfully',
                    'message_ar'=>'order get successfully',
                    'order_data'=>$order_data,
                    'img_url'=>base_url().'uploads/salads/'
                );
            }else{
                $response=array(
                    'status'=>true,
                    'message'=>'No orders found',
                    'message_ar'=>'No orders found'
                );
            }

        }else{
            $response=array(
                'status'=>false,
                'message'=>'Something went wrong',
                'message_ar'=>'Something went wrong'
            );
        }
        echo json_encode($response);
    }

    //insert Order
    public function insertOrder()
    {
        if(isset($_POST['user_id']))
        {
            $record = array(
                'user_id' => $_POST['user_id'],
                'address_id' => $_POST['address_id'],
                'promo_id' => $_POST['promo_id'],
                'total_price'=>$_POST['total_price'],
                'tax_amt' => $_POST['tax_amt'],
                'discount_amt' => $_POST['discount_amt'],
                'delivery_charge'=>$_POST['delivery_charges'],
                'grand_total'=>$_POST['grand_total'],
                'payment_status'=>0,
                'payment_method'=>$_POST['payment_method'],
                'order_notes'=>$_POST['order_notes'],
                'in_store_pickup'=>$_POST['in_store_pickup'],
                'customer_name'=>$_POST['customer_name'],
                'customer_phone'=>$_POST['customer_phone'],'pickup_time'=> $_POST['pickup_time'],
                'pickup_date'=>date("Y-m-d",strtoTime($_POST['pickup_date'])),
                'created_at' => date("Y-m-d H:i:s"),
                'o_status'=>1,
                'updated_at' => date("Y-m-d H:i:s")
            );
            $order_id = $this->Main_model->__callInsert('sm_orders',$record);

            //insert into notification table
            $not_data=array(
                'noti_msg'=>'New order',
                'module_id'=>$order_id,
                'noti_for'=>'A',
            );
            $this->db->insert('sm_notifications',$not_data);


            if(isset($_POST['custom_salad'])){

                $order_items=json_decode($_POST['custom_salad']);

                foreach ($order_items as $items){
                    $data=array(
                        'order_id'=>$order_id,
                        'salad_name'=>$items->salad_name,
                        'salad_name_arabic'=>$items->salad_name_arabic,
                        'salad_img'=>$items->salad_img,
                        'cat_name'=>getSingleFieldDetail('cat_id',$items->cat_id,'cat_name','sm_categories'),
                        'cat_name_arabic'=>getSingleFieldDetail('cat_id',$items->cat_id,'cat_name_arabic','sm_categories'),
                        'quantity'=>$items->quantity,
                        'dressing_mixed_in'=>$items->dressing_mixed_in,
                        'item_total_price'=>$items->item_total_price,
                        'item_total_calories'=>$items->totalCalories,
                        'salad_type'=>2
                    );

                    $this->db->insert('sm_order_details',$data);
                    $order_detail_id=$this->db->insert_id();
                    foreach ($items->ings as $ing_data){
                        $data=array(
                            'odetail_id'=>$order_detail_id,
                            'ing_id'=>$ing_data->ing_id,
                            'ing_name'=>getSingleFieldDetail('ing_id',$ing_data->ing_id,'ing_name','sm_ingredients'),
                            'ing_name_arabic'=>getSingleFieldDetail('ing_id',$ing_data->ing_id,'ing_name_arabic','sm_ingredients'),
                            'quantity'=>$ing_data->ing_qty,
                            'ing_price'=>getSingleFieldDetail('ing_id',$ing_data->ing_id,'ing_price','sm_ingredients'),
                            'ing_calories'=>getSingleFieldDetail('ing_id',$ing_data->ing_id,'ing_calories','sm_ingredients'),
                        );
                        $this->db->insert('sm_order_ingredients',$data);
                    }
                }

            }
            if(isset($_POST['order_items'])){
                $order_items = json_decode($_POST['order_items']);
                foreach ($order_items as $items){
                    $data=array(
                        'order_id'=>$order_id,
                        'salad_id'=>$items->salad_id,
                        'salad_name'=>$items->salad_name,
                        'salad_name_arabic'=>$items->salad_name_arabic,
                        'salad_img'=>$items->salad_img,
                        'cat_name'=>getSingleFieldDetail('cat_id',$items->cat_id,'cat_name','sm_categories'),
                        'cat_name_arabic'=>getSingleFieldDetail('cat_id',$items->cat_id,'cat_name_arabic','sm_categories'),
                        'quantity'=>$items->quantity,
                        'dressing_mixed_in'=>$items->dressing_mixed_in,
                        'item_total_price'=>$items->item_total_price,
                        'item_total_calories'=>$items->totalCalories,
                        'salad_type'=>1
                    );

                    $this->db->insert('sm_order_details',$data);
                    $order_detail_id=$this->db->insert_id();
                    foreach ($items->ings as $ing_data){
                        $data=array(
                            'odetail_id'=>$order_detail_id,
                            'ing_type'=>'add-on',
                            'ing_id'=>$ing_data->ing_id,
                            'ing_name'=>getSingleFieldDetail('ing_id',$ing_data->ing_id,'ing_name','sm_ingredients'),
                            'ing_name_arabic'=>getSingleFieldDetail('ing_id',$ing_data->ing_id,'ing_name_arabic','sm_ingredients'),
                            'quantity'=>$ing_data->ing_qty,
                            'ing_price'=>getSingleFieldDetail('ing_id',$ing_data->ing_id,'ing_price','sm_ingredients'),
                            'ing_calories'=>getSingleFieldDetail('ing_id',$ing_data->ing_id,'ing_calories','sm_ingredients'),
                        );
                        $this->db->insert('sm_order_ingredients',$data);
                    }
                    $this->db->select('');
                    $this->db->from('sm_salad_ingredients as si');
                    $this->db->join('sm_ingredients as i','i.ing_id=si.ing_id');
                    $this->db->where('si.salad_id',$items->salad_id);
                    $this->db->where('i.ing_status',1);
                    $query=$this->db->get();
                    $salad_ings=$query->result();
                    if($salad_ings){
                        foreach ($salad_ings as $ing){
                            $data=array(
                                'odetail_id'=>$order_detail_id,
                                'ing_type'=>'salad',
                                'ing_id'=>$ing->ing_id,
                                'ing_name'=>$ing->ing_name,
                                'ing_name_arabic'=>$ing->ing_name_arabic,
                                'quantity'=>$ing->quantity,
                                'ing_price'=>$ing->ing_price,
                                'ing_calories'=>$ing->ing_calories,
                            );
                            $this->db->insert('sm_order_ingredients',$data);
                        }
                    }

                }
            }



            if($_POST['payment_method']==='online'){
                //update order status
                $table_name='sm_orders';
                $update_data=array(
                    'o_status'=>0,
                );
                $where_condition=array(
                    'order_id'=>$order_id
                );
                $this->General_model->update_details($table_name, $update_data, $where_condition);

                //getting user iformation from user table
                $table_name='sm_users';
                $select_filds='';

                $where_condition=array(
                    'id'=>$_POST['user_id']
                );

                $user=$this->General_model->get_row($table_name, $select_filds, $where_condition);

                $table_name='sm_orders';
                $select_filds='';
                $where_condition=array(
                    'order_id'=>$order_id
                );

                $order_detail=$this->General_model->get_row($table_name, $select_filds, $where_condition);




                require_once 'paytabs.php';

                $pt = new Payment();
                $pt->paytabs("ahmad@saladmania.net", "j4Xx3kgBO3rRDggTSTcLe8SYHgkxxXaQ28o5sEGhI79mopfhjs2yAUwhbQPIOZzQ7XWACCSkK2G9rWqLosjZmQhyH6gv5QmdNiOp");
                $name=explode(' ',$user->full_name);
                $firstname=$name[0];
                $lastname='sm';
                if(count($name)>1){
                    $lastname=$name[1];
                }
                $email='';
                $user_address='';
                if($user->user_type ==='U'){
                    $where=array(
                        'user_id'=>$_POST['user_id'],
                        'add_type'=>'profile'
                    );
                    $order_by='add_id DESC';
                    $address=$this->General_model->get_row('sm_addresses','',$where,$order_by);
                    $email=$user->email;
                    $user_address=$address->addline1.', '.$address->addline2.', Amman';
                }elseif ($user->user_type ==='G'){  $where=array(
                    'user_id'=>$_POST['user_id'],
                );
                    $order_by='add_id DESC';
                    $address=$this->General_model->get_row('sm_addresses','',$where,$order_by);
                    $email='ahmad@saladmania.net';
                    $user_address=$address->addline1.', Amman';
                }






                //$result = $pt->authentication();

                $result = $pt->create_pay_page(array(
                    //Customer's Personal Information
                    'merchant_email' => "ahmad@saladmania.net",
                    'secret_key' => "j4Xx3kgBO3rRDggTSTcLe8SYHgkxxXaQ28o5sEGhI79mopfhjs2yAUwhbQPIOZzQ7XWACCSkK2G9rWqLosjZmQhyH6gv5QmdNiOp",
                    // 'merchant_email' => "priyanshu@coretechies.com",
                    // 'secret_key' => "4ytpC6ATFkhNKY7rMwLX99qDtbdFcpAgX3Gz9gtPBLCmJJNwgztqJU3c5gSiGszDvfdFwHu7s6tbQhI7RbyA6CNcTGuz84dEjrkP",
                    'cc_first_name' =>  $firstname,          //This will be prefilled as Credit Card First Name
                    'cc_last_name' => $lastname,            //This will be prefilled as Credit Card Last Name
                    'cc_phone_number' => "00962",
                    'phone_number' => $user->phone,
                    'email' => $email,

                    //Customer's Billing Address (All fields are mandatory)
                    //When the country is selected as USA or CANADA, the state field should contain a String of 2 characters containing the ISO state code otherwise the payments may be rejected.
                    //For other countries, the state can be a string of up to 32 characters.
                    'billing_address' =>  $user_address,
                    'city' => "Amman",
                    'state' => "Amman",
                    'postal_code' => "11118",
                    'country' => "JOR",
                    // 'postal_code' => "00973",
                    // 'country' => "BHR",

                    //Customer's Shipping Address (All fields are mandatory)
                    'address_shipping' => $user_address,
                    'city_shipping' => "Amman",
                    'state_shipping' => "Amman",
                    'postal_code_shipping' => "11118",
                    'country_shipping' => "JOR",
                    // 'postal_code_shipping' => "00973",
                    // 'country_shipping' => "BHR",

                    //Product Information
                    "products_per_title" => "Salad",   //Product title of the product. If multiple products then add “||” separator
                    'quantity' => "1",                                    //Quantity of products. If multiple products then add “||” separator
                    'unit_price' =>  $order_detail->grand_total,                                  //Unit price of the product. If multiple products then add “||” separator.
                    "other_charges" => "0",                                     //Additional charges. e.g.: shipping charges, taxes, VAT, etc.
                    'amount' => $order_detail->grand_total,                                          //Amount of the products and other charges, it should be equal to: amount = (sum of all products’ (unit_price * quantity)) + other_charges
                    'discount'=>"0",                                                //Discount of the transaction. The Total amount of the invoice will be= amount - discount
                    'currency' => "JOD",                                            //Currency of the amount stated. 3 character ISO currency code

                    //Invoice Information
                    'title' => $user->full_name,          // Customer's Name on the invoice
                    "msg_lang" => "en",                 //Language of the PayPage to be created. Invalid or blank entries will default to English.(Englsh/Arabic)
                    "reference_no" => "1231231",        //Invoice reference number in your system

                    //Website Information
                    "site_url" => 'https://www.saladmania.net',      //The requesting website be exactly the same as the website/URL associated with your PayTabs Merchant Account
                    'return_url' => base_url()."payment-success/",
                    "cms_with_version" => "API USING PHP",
                    "paypage_info" => "1"
                ));

                $this->session->set_userdata('order_id',$order_id);

//                    $_SESSION['paytabs_api_key'] = $result->secret_key;

                $result=array(
                    'status'=>true,
                    'url'=>$result->payment_url,
                    'order_id'=>$order_id,
                    'mode'=>$_POST['payment_method'],
                    'message'=>'order successfully added',
                    'message_ar'=>'order successfully added'
                );
            }
            elseif ($_POST['payment_method']==='COD'){
                //get store pickup value
                $this->db->select('');
                $this->db->from('sm_orders');
                $this->db->where('order_id',$order_id);
                $query=$this->db->get();
                $order_data=$query->row();
                if($order_data){
                    //CAll the customer address, in case of delivery order
                    if($order_data->in_store_pickup == 0)
                    {
                        $table_name = 'sm_addresses';
                        $select_filds='';
                        $where_condition=array(
                            'add_id'=>$order_data->address_id
                        );
                        $address=$this->General_model->get_row($table_name,$select_filds,$where_condition);

                        $reciver_location=array(
                            'latitude'=> floatval($address->latitude),
                            'longitude'=> floatval($address->longitude)
                        );

                        $payment_method="";
                        if($order_data->payment_method =='COD'){
                            $payment_method="Cash";
                        }elseif ($order_data->payment_method =='online'){
                            $payment_method="Online";
                        }

                        //Crate data to send to Delivery API
                        $data_array =  array(
                            "username"=>"saladmania",
                            /*"password"=>"salad@mania321", //staging
                            "price_id"=>"FBiq4KMNEY",*/
                            "password"=>"saladmania4321", //live
                            "price_id"=>"dX1Bj42hvX",
                            "buyername"=>getSingleFieldDetail('id',$order_data->user_id,'full_name','sm_users'),
                            "order_price"=> (int)$order_data->grand_total,
                            "receiver_phone"=>getSingleFieldDetail('id',$order_data->user_id,'phone','sm_users'),
                            "receiver_name"=>getSingleFieldDetail('id',$order_data->user_id,'full_name','sm_users'),
                            "receiver_location"=>$reciver_location,
                            "receiver_image"=>"https://s3.amazonaws.com/utrac-parser-server/823265829cc7c7939be1e6336fd84bdf_if_profle_1055000.png",
                            "sender_name"=>"test",
                            "sender_phone"=>"+962790000000",
                            "sender_location"=>$reciver_location,
                            "sender_image"=>"https://images-genius-com.imgix.net/9584d5e3baae3add6ffa57e6bd34587b.1000x1000x1.jpg?dpr=1&fit=fill&frame=%2A&h=218&w=218",
                            "integrator_number"=>encryptId($order_data->order_id),
                            "receiver_city"=>"amman",
                            "receiver_area"=>$address->addline1,
                            "receiver_street"=>"",
                            "receiver_building"=>"",
                            "receiver_floor"=>"",
                            "receiver_apartment"=>"126",
                            "receiver_landmark"=>"khbp",
                            "receiver_note"=>"test",
                            "is_demo"=>false, //false on live orders
                            "payment_method"=>$payment_method,
                            "note"=>"testing",
                            "change"=>50
                        );

                        //staging.utracadmin.net = staging server & web = live
                        $make_call = $this->callAPI('POST', 'https://web.utracadmin.net/parse/functions/addIntegratorOrder', json_encode($data_array));
                        $response = json_decode($make_call, true);
                        //echo "<pre>"; print_r($response);exit;
                        //$response['code'] == '10' = already created

                        if($response['result']['orderId']){
                            $update_status = true;
                            $record_to_update = array(
                                'o_status'=>1,
                                'delivery_id'=>$response['result']['orderId'],
                                'delivery_status'=>1,
                            );
                            /*$wheres = array('order_id'=>$id);
                            $data = $this->Main_model->__callUpdate('sm_orders',$wheres,$record_to_update);
                            $this->session->set_flashdata('success_msg', 'Record is updated successfully');*/


                            //Send email to user
                            /*$this->db->select('');
                            $this->db->from('sm_orders');
                            $this->db->where('order_id',$id);
                            $query=$this->db->get();
                            $message['order'] = $query->row();

                            $msg = $this->load->view('email/order_template', $message, TRUE);
                            $userEmail = getSingleFieldDetail('id',$order_data->user_id,'email','sm_users');
                            sendmail($userEmail,'','',$msg, 'Salad Mania - Order Status');*/
                        }
                    }
                    else{
                        $update_status = true;
                        $record_to_update = array(
                            'o_status'=>0
                        );
                    }

                    //Now update the order
                    if(isset($update_status)){
                        //Update the order status
                        $wheres = array('order_id' => $order_id);
                        $data = $this->Main_model->__callUpdate('sm_orders',$wheres,$record_to_update);
                        $this->session->set_flashdata('success_msg', 'Record is updated successfully');

                        //Send email & SMS to user
                        $this->db->select('o.*, u.phone');
                        $this->db->from('sm_orders as o');
                        $this->db->join('sm_users as u', 'u.id = o.user_id', 'LEFT');
                        $this->db->where('order_id',$order_id);
                        $query=$this->db->get();
                        $message['order'] = $order = $query->row();
                        //echo "<pre>"; print_r($message['order']);exit;
                        if(!empty($order))
                        {
                            $result = array('status' => true, 'message' => 'order successfully added.', 'message_ar' => 'order successfully added.');
                        }
                    }
                }
                $where=array(
                    'user_id'=>$_POST['user_id']
                );
                $this->db->delete('sm_carts',$where);
                if($order_id)
                    $result = array('status' => true, 'message' => 'order successfully added.', 'message_ar' => 'order successfully added.');
            }else{
                $result = array('status' => false, 'message' => 'Order not added.','message_ar' => 'Order not added.');
            }
        }
        else {
            $result = array("status" => false, "message" => 'Something went wrong',"message_ar" => 'Something went wrong');
        }


        echo json_encode($result);
    }

    //contactUs
    public function contactUs(){
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');
        $contactemail = array();
        extract($_POST);

        $to = 'ahmad@saladmania.net'; //Default
        if($_POST['type'] === 'contact'){
            //$to='rupali@coretechies.com';
            $to=getSingleFieldDetail('id',1,'contact_email','sm_app_settings');
            $subj='Contact Us Enquery';

        }elseif ($_POST['type'] === 'dietician'){
            //$to='diksha@coretechies.com';
            $to=getSingleFieldDetail('id',1,'diet_email','sm_app_settings');
            $subj='Dietician Enquery';
        }

        $message = array(
            'type' => $_POST['type'],
            'name' => $_POST['user_name'],
            'email' => $_POST['user_email'],
            'subject' => $_POST['user_subject'],
            'message' => $_POST['user_message'],
        );

        $msg = $this->load->view('email/contact_us', $message, TRUE);
        $sendemail = sendmail($to,'','',$msg, 'Salad Mania : '.$subj);


//        $to = $user_email;
//        $subject = $user_subject;
//        $data['name'] =$user_name;
//        $data['email']= $user_email;
//        $data['msg']= $user_message;
//        $data['subject']= $user_subject;
//        $html  = $this->load->view('email/contact_us',$data,true);
//        $sendemail=sendmail($to,'','', $html,$subject);
        if($sendemail){
            $result = array('status' => true, 'message' => 'Email has send successfully to admin.','message_ar' => 'Email has send successfully to admin.');
        }
        else
        {
            $result = array("status" => false, "message" => 'Something went wrong',"message_ar" => 'Something went wrong');
        }
        echo json_encode($result);
    }

    //email send
    public function send_email($to, $from, $subject,$html){

        $email_setting = $this->admin_model->get_email_setting();
        if(isset($email_setting) && !empty($email_setting)){
            $config = Array(
                'protocol' => $email_setting[0]['type'],
                'smtp_host' => $email_setting[0]['smtp_host'],
                'smtp_port' => $email_setting[0]['smtp_port'],
                'smtp_user' => $email_setting[0]['smtp_user'],
                'smtp_pass' => $email_setting[0]['smtp_password'],
                'smtp_timeout' => '8',
                'mailtype'  => 'html',
                'charset'   => 'utf-8',
                'starttls'  => true,
            );
        }else{
            $config = Array(
                'protocol' => SMTP,
                'smtp_host' => SMTP_HOST,
                'smtp_port' => SMTP_PORT,
                'smtp_user' => SMTP_USER,
                'smtp_pass' => SMTP_PASSWORD,
                'smtp_timeout' => '8',
                'mailtype'  => 'html',
                'charset'   => 'utf-8',
                'starttls'  => true,
                'wordwrap' => TRUE
            );
        }
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('commerciale@bee-o-veg.com', 'Salad-Mania');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($html);
        if($this->email->send()){
            return true;
        }else{
            return false;
        }
    }

    //Custom Salad Creation Function
    public function createCustomSalad()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, POST, DELETE, OPTIONS");
        header('Access-Control-Max-Age: 86400');
        header("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header("Access-Control-Allow-Headers: *");
        header('content-type: application/json');

        extract($_POST);
        if(isset($user_id))
        {
            $record = array('salad_name' => $salad_name, 'salad_img'=>'custom.png','ing_ids'=> $ing_ids, 'salad_price'=>$salad_price,'salad_calories'=>$salad_calories,'custom_salad'=>1,'salad_status' => 1,'cat_id'=>0, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"));

            $last_id = $this->Main_model->__callInsert('sm_salads',$record);

            if($last_id){
                //Now Add this Item in Cart as Well
                $record = array('user_id' => $user_id, 'salad_id' => $last_id, 'quantity' => 1, 'dressing_mixed_in'=>0, 'item_total_price' => $salad_price, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"));
                $cart_id = $this->Main_model->__callInsert('sm_carts',$record);

                if($cart_id)
                    $result = array('status' => true, 'message' => 'salad successfully added.','message_ar' => 'salad successfully added.');
                else
                    $result = array('status' => false, 'message' => 'Salad created, but not added','message_ar' => 'Salad created, but not added');
            }
            else
                $result = array('status' => false, 'message' => 'salad not added.', 'message_ar' => 'salad not added.');
        }
        else
            $result = array("status" => false, "message" => 'Something went wrong',"message_ar" => 'Something went wrong');

        echo json_encode($result);
    }

    //Ingredient Listing Function
    public function getIngredients()
    {
        if(isset($_POST['contains_make_own'])){
            if($_POST['contains_make_own'] == 1){
                if(isset($_POST['cat_id'])){
                    $where=array(
                        'cat_id'=>$_POST['cat_id'],
                        'contains_make_own'=>1
                    );
                    $ing_price=[];
                    $ingredients_data=[];
                    $this->db->select('cat_id,cat_name,cat_name_arabic');
                    $this->db->from('sm_categories');
                    $this->db->where($where);
                    $query=$this->db->get();
                    $categories=$query->result();
                    if($categories){
                        $where=array(
                            's.parent_cat_id'=>$_POST['cat_id'],
                            's.salad_type'=>2,
                            's.subcat_status'=>1,
                        );
                        $this->db->select('is.sing_price as ing_price,is.sing_calories as ing_calories,s.id as sub_cat_id,s.subcat_status,i.ing_status,s.subcat_name,s.subcat_name_arabic,is.ing_id,is.qty_up_down,i.ing_name,i.ing_name_arabic,ing_image');
                        $this->db->from('sm_subcategories as s');
                        $this->db->join('sm_ingredients_subcats as is','is.subcat_id=s.id');
                        $this->db->join('sm_ingredients as i','i.ing_id=is.ing_id');
                        $this->db->where($where);
                        $this->db->where('i.ing_status',1);
                        $query=$this->db->get();
                        $ingredients_data=$query->result();
//                        $ing_price=$ingredients_data['sing_price'];
                        
                        //get categories
                        $this->db->select('id as sub_cat_id,subcat_name,subcat_name_arabic,max_limit,min_limit');
                        $this->db->from('sm_subcategories as s');
                        $this->db->where($where);
                        $query=$this->db->get();
                        $categories=$query->result();
                    }
                    $response=array(
                        'status'=>true,
                        'ingImgUrl'=>base_url().'uploads/ingredients/',
                        'ingdata'=>$ingredients_data,
                        'catdata'=>$categories
                    );

                }
            }
            elseif ($_POST['contains_make_own']==0){
                if(isset($_POST['salad_id'])){

                    $cat_id=getSingleFieldDetail('salad_id',$_POST['salad_id'],'cat_id','sm_salads');
                    $where=array(
                        's.parent_cat_id'=>$cat_id,
                        's.salad_type'=>1,
                       's.subcat_status'=>1,
                    );

                    $this->db->select('is.sing_price as ing_price,is.sing_calories as ing_calories, s.id as sub_cat_id,is.ing_id,is.qty_up_down,s.subcat_status,i.ing_status,s.subcat_name,s.subcat_name_arabic,i.ing_name,i.ing_name_arabic,ing_image');
                    $this->db->from('sm_subcategories as s');
                    $this->db->join('sm_ingredients_subcats as is','is.subcat_id=s.id');
                    $this->db->join('sm_ingredients as i','i.ing_id=is.ing_id');
                    $this->db->where($where);
                    $this->db->where('i.ing_status',1);
                    $query=$this->db->get();
                    $ingredients_data=$query->result();

                    $this->db->select('id as sub_cat_id,subcat_name,subcat_name_arabic,max_limit,min_limit');
                    $this->db->from('sm_subcategories as s');
                    $this->db->where($where);
                    $query=$this->db->get();
                    $categories=$query->result();
                    $response=array(
                        'status'=>true,
                        'ingImgUrl'=>base_url().'uploads/ingredients/',
                        'ingdata'=>$ingredients_data,
                        'catdata'=>$categories
                    );
                }
            }else{
                $response=array(
                    'status'=>false,
                    'message'=>'Something went wrong!! Please try again',
                     'message_ar'=>'Something went wrong!! Please try again'
                );
            }

        }else{
            $response=array(
                'status'=>false,
                'message'=>'Something went wrong!! Please try again',
                 'message_ar'=>'Something went wrong!! Please try again'
            );

        }





        echo json_encode($response);


//        //ingredient query
//        $sql = "select * from sm_ingredients where ing_status=1 order by ing_id ";
//        $query = $this->Main_model->__callMasterquery($sql);
//
//        $ingredient = array();
//        $category = array();
//
//        if($query->num_rows() > 0 ) {
//            foreach ($query->result() as $row) {
//                $record = array('ing_id' => $row->ing_id, 'ing_name' => $row->ing_name,  'ing_price'=> round($row->ing_price), 'ing_calories'=> round($row->ing_calories),'ing_image'=>$row->ing_image, 'ing_status'=>$row->ing_status, 'cat_id'=>$row->cat_id);
//                $ingredient[] = $record;
//            }
//            $catsql = "select * from sm_categories where cat_status=1 and cat_type='item' order by cat_id";
//            $catquery = $this->Main_model->__callMasterquery($catsql);
//            if($catquery->num_rows() > 0 ) {
//                foreach ($catquery->result() as $catrow) {
//                    $catrecord = array('cat_id'=>$catrow->cat_id,'cat_name'=>$catrow->cat_name);
//                    $category[] = $catrecord;
//                }
//                $imgUrl = base_url().'uploads/ingredients/';
//                $result = array("status" => true, "message" => 'data get successfully','ingImgUrl' => $imgUrl, 'ingdata' => $ingredient, 'catdata' => $category);
//            }
//        }
//        else
//            $result = array("status" => false, "message" => 'No result found');
//        echo json_encode($result);
    }

    //Guest user Sign Up
    public function guestUserSignup()
    {


        extract($_POST);
        if(isset($full_name) && isset($phone)){

            $userData = array('full_name' => $full_name,'user_type'=>'G','email' => NULL,'phone' => $phone,'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"));

            $last_id = $this->Main_model->__callInsert('sm_users',$userData);

            if($last_id){
                //Create Entry in Address Table
                $addData = array('user_id' => $last_id, 'addline1' => $address, 'city_id' => $city, 'add_type' => 'profile', 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s"));
                $add_id = $this->Main_model->__callInsert('sm_addresses',$addData);

                $result = array("status" => true, "message" => 'User registered successfully',"message_ar" => 'User registered successfully', "user_id"=>$last_id);

                //Pass the Address ID
                if($add_id)
                    $result['add_id'] = $add_id;

                $result['customer_name']=$full_name;
                $result['customer_phone']=$phone;
                $result['address']=$address;
            }
            else
                $result = array("status" => false, "message" => 'Something went wrong.', "message_ar" => 'Something went wrong.');

        }
        else
            $result = array("status" => false, "message" => 'Something went wrong.', "message_ar" => 'Something went wrong.');

        echo json_encode($result);
    }

    //forgot password
    public function forgotPassword($encrypted_id=null){
        extract($_POST);
        $where=array(
            'email'=>$email,
            'user_type'=>'U'
        );
        $if_exist=$this->General_model->get_row('sm_users','',$where);
        if($if_exist)
        {
            if($if_exist->user_status==1)
            {
                $message='<p>Hi</p>';
                $message.='<p>Please reset your password by clicking below button!! Link is active only for 10 minutes!!</p><br>';
                $message.='<p><a href="'.base_url().'forgot/'.encryptPassword($if_exist->id).'" class="btn btn-info">Verify</a></p><br>';
                $message.='<p>Regards</p>';
                $message.='<p>Salad Mania Team</p>';
                $status=sendmail($_POST['email'],'','',$message,'Verify Email:Salad Mania');
                if($status)
                {
                    $minutes_to_add = 10;
                    $now_date=date("Y-m-d h:i:s");
                    $time = new DateTime($now_date);
                    $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
                    $fine_time = $time->format('Y-m-d H:i');
                    $update_data=array(
                        'ch_pswd_status'=>1,
                        'reset_pswd_start_time'=>date("Y-m-d h:i:s"),
                        'reset_pswd_end_time'=>$fine_time
                    );
                    $updatestatus=$this->General_model->update_details('sm_users',$update_data,$where);
                    if($updatestatus)
                    {
                        $result = array("status" => true, "message" => 'We have send you email to reset your password!!',"message_ar" => 'We have send you email to reset your password!!');
                    }
                    else
                    {
                        $result = array("status" => false, "message" => 'Somthing went wrong on updating the data!!',"message_ar" => 'Somthing went wrong on updating the data!!');
                    }
                }
                else
                {
                    $result = array("status" => false, "message" => 'Somthing went wrong on sending email for verify!!',"message_ar" => 'Somthing went wrong on sending email for verify!!');
                }
            }
            else
            {
                $result = array("status" => false, "message" => 'E-mail is not verified!! Please try again!!',"message_ar" => 'E-mail is not verified!! Please try again!!');
            }
        }
        else
        {
            $result = array("status" => false, "message" => 'E-mail is not registered!! Please  try again!!',"message_ar" => 'E-mail is not verified!! Please try again!!');
        }
        echo json_encode($result);
    }

    //change password
    public function change_password(){
        extract($_POST);
        if(isset($user_id)){
             //user personal detail update
            if(isset($_POST['full_name']) && isset($_POST['phone'])){
                $personal_where=array(
                    'id'=>$user_id,
                );
                $personal_data_update=array(
                    'full_name'=>$full_name,
                    'phone'=>$phone
                );
                $changepersonaldetails=$this->General_model->update_details('sm_users',$personal_data_update,$personal_where);
                if($changepersonaldetails){
                    $result = array("status" => true, "message" => 'Profile updated successfully',"message_ar" => 'Profile updated successfully');
                }
                else{
                    $result = array("status" => false, "message" => 'Somthing went wrong on updating the data!!',"message_ar" => 'Somthing went wrong on updating the data!!');
                }
            }

             //user change password update
            if(isset($_POST['pswd']) && isset($_POST['new_pswd'])){
                $where=array(
                    'password'=>encryptPassword($pswd),
                    'user_type'=>'U',
                    'id'=>$user_id,
                );
                $if_exist=$this->General_model->get_row('sm_users','',$where);
                if($if_exist){
                    if($if_exist->user_status==1){
                        
                        if(isset($_POST['pswd'])){
                            $where=array(
                                'id'=>$user_id,
                            );
                            $update_data=array(
                                'password'=>encryptPassword($_POST['new_pswd'])
                            );
                            $changepaswrdstatus=$this->General_model->update_details('sm_users',$update_data,$where);
                            if($changepaswrdstatus){
                                $result = array("status" => true, "message" => 'Profile updated successfully',"message_ar" => 'Profile updated successfully');
                            }
                            else{
                                $result = array("status" => false, "message" => 'Somthing went wrong on updating the data!!',"message_ar" => 'Somthing went wrong on updating the data!!');
                            }
                        }
                        else{
                            $result = array("status" => false, "message" => 'Somthing went wrong', "message_ar" => 'Somthing went wrong');
                        }
                    }else{
                        $result = array("status" => false, "message" => 'User is not verified!! Please try again!!', "message_ar" => 'User is not verified!! Please try again!!');
                    }
                }else{
                    $result = array("status" => false, "message" => 'Entered old password is not correct', "message_ar" => 'Entered old password is not correct');
                }
            }
        }
        else{
            $result = array("status" => false, "message" => 'Somthing went wrong on updating the password!!',"message_ar" => 'Somthing went wrong on updating the password!!');
        }
        echo json_encode($result);
}
    public function RemoveCart(){
        if(isset($_POST['cart_id'])){
            $where=array(
                'cart_id'=>$_POST['cart_id']
            );
            $this->db->delete('sm_carts',$where);
            $status=$this->db->delete('sm_cart_ingredients',$where);
            if($status){
                $response=array(
                    'status'=>true,
                    'message'=>'Successfully Delete',
                    'message_ar'=>'Successfully Delete'
                );
            }else{
                $response=array(
                    'status'=>false,
                    'message'=>'Something went wrong!! Please try again',
                    'message_ar'=>'Something went wrong!! Please try again'
                );
            }

        }else{
            $response=array(
                'status'=>false,
                'message'=>'Something went wrong!! Please try again',
                'message_ar'=>'Something went wrong!! Please try again'
            );
        }

        echo json_encode($response);
    }

     //Send SMS Function
     public function send_sms(){
        //sms sending
        $mob_numbers='8209816674';
        $msg='Hi this is a test msg.';
        $msg_status=$this->SendSms($msg, $mob_numbers);
        print_r($msg_status);exit;
    }
 
        
    public function SendSms($msg, $mob_numbers){
        $curl = curl_init();

        //Msg91 Account configuration
        $senderId = '';
        $authKey = '2861A5p9VZEZqrz5d370d42';

       if($authKey == ''){
            //echo 'error', '-----------msg91_auth_key--------not found------sms not going';
        }else{
            //echo 'error', '-----------msg91_auth_key------- found------';
            try {
//                dd($mob_numbers);
//                $temp_manipulated_mob_numbers = implode(',',$mob_numbers);
//                dd($temp_manipulated_mob_numbers);
                //$temp_manipulated_mob_numbers = $mob_numbers;
                $curl = curl_init();
                $var =  curl_setopt_array($curl, array(
                    CURLOPT_HTTPHEADER => array(
                        "authkey: " .$authKey,
                        "content-type: application/json"
                    ),
                    CURLOPT_URL => "http://api.msg91.com/api/v2/sendsms",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS =>"{ \"sender\": \"SOCKET\", \"route\": \"4\", \"country\": \"91\", \"sms\": [ { \"message\": \"$msg\", 
        \"to\": [ $mob_numbers ] } ] }",
                    CURLOPT_SSL_VERIFYHOST => 0,
                    CURLOPT_SSL_VERIFYPEER => 0,
                ));
                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);
                if ($err) {
                    echo "cURL Error #:" . $err;
                    } else {
                    echo $response;
                    }
               
            }  catch (Exception $e) {
                echo "cURL Error #:" . $err; //echo "error:, '-----------catch (Exception $e)------- ------'.$e)";
            }

        }
    }
    public function AddDeliveryAddress(){
        if(isset($_POST['user_id'])){
            $data=array(
                'user_id'=>$_POST['user_id'],
                'addline1'=>$_POST['address'],
                'city_id'=>$_POST['city_id'],
                'latitude'=>$_POST['latitude'],
                'longitude'=>$_POST['longitude'],
                'add_type'=>'delivery'
            );
            $status=$this->db->insert('sm_addresses',$data);
            if($status){
                $response=array(
                    'status'=>true,
                    'message'=>'Address added sucessfully',
                     'message_ar'=>'Address added sucessfully'
                );
            }else{
                $response=array(
                    'status'=>false,
                    'message'=>'something went wrong',
                    'message_ar'=>'something went wrong'
                );
            }
        }else{
            $response=array(
                'status'=>false,
                'message'=>'something went wrong',
                'message_ar'=>'something went wrong'
            );
        }
        echo json_encode($response);
    }
    private function callAPI($method, $url, $data){
        $curl = curl_init();

        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'X-Parse-Application-Id: 9803886e4a724d87bc56b44bd7512d3e2093bd95',
            'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        if(!$result){die("Connection Failure");}
        curl_close($curl);
        return $result;
    }
}