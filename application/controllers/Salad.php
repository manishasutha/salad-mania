<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salad extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
           $this->load->helper('form');
            $this->load->library('form_validation');
				$this->load->helper('common_helper');
            $this->load->model('Main_model');
			$this->load->model('General_model');
		
			 $this->load->database();
        	$this->load->library('encryption');
        is_logged_in();
        if(!is_admin()){    
            redirect(base_url().('customer'));
            exit;
        }
    }
    
    //Add Salad Function
    public function add_salad($id = NULL){

        //Add Salad BAsic Info - First Step
        if(isset($_POST['submit']))
        {
            extract($_POST);
            //First check the salad name is already exist ot not
            $sql = "select * from sm_salads where salad_name = '$salad_name' AND cat_id = $cat_id";
            $exist = $this->Main_model->__callMasterquery($sql);
            if($exist->num_rows() == 0)
            {
                //uploading Excel File
                $file_details = array('upload_path'=> "./uploads/salads/", 'allowed_types' => "jpg|jpeg|gif|png|svg", 'max_size'=>"50000",'max_width'=>"50000", 'max_height'=>"50000", 'filename'=>"salad_img");
                $uploadedFile = $this->uploadfile($file_details);

                $data = array('salad_name' => $salad_name, 'salad_name_arabic'=>$salad_name_arabic,  'salad_desc' => $salad_desc, 'salad_desc_arabic'=>$salad_desc_arabic, 'cat_id' => $cat_id, 'salad_status' => 0, 'salad_img' => $uploadedFile);        
                $last_id = $this->Main_model->__callInsert('sm_salads',$data);
                
                if($last_id){
                    $this->session->set_flashdata('success_msg', 'Now please add the ingredients in recipe');
                    redirect(base_url().'add-salad/'.$last_id.'?cat_id='.$cat_id);
                }
                else
                    $this->session->set_flashdata('error_msg', 'Something went wrong.');
            }
            else
                $this->session->set_flashdata('error_msg', '<b>'. $salad_name.'</b> is already exist.');
            
            redirect(base_url().'add-salad');
        }

        //Add Salad Ingredients - Second Step
        if(isset($_POST['submitIng']))
        {
            //echo "<pre>"; print_r($_POST);
            extract($_POST);

            $quantity = array_filter($quantity); //Remove Empty index
            $quantity = array_values($quantity);  //Rearrange index form 0 to ...

            //First check the salad name is already exist ot not
            if(count($ing_id) == count($quantity))
            {
                for($i=0; $i < count($ing_id); $i++)
                {
                    $ids = explode('-', $ing_id[$i]);
                    $ingId = $ids[0];
                    $subcat_id = $ids[1];

                    $data = array('ing_id' => $ingId, 'quantity'=>$quantity[$i], 'salad_id' => $id ,'subcat_id' => $subcat_id);        
                    $last_id = $this->Main_model->__callInsert('sm_salad_ingredients',$data);
                    
                    if($last_id)
                        $status = true;
                }

                if(isset($status))
                {
                    //Update the Salad Status to active
                    $record = array('salad_status' => 1,'salad_price'=>$salad_price, 'salad_calories'=>$salad_calories);
                    $where = array('salad_id' => $id);
                    $this->Main_model->__callUpdate('sm_salads', $where, $record);

                    //update subcat last update time in sm_update_timing
                    $time_data=array(
                        'salad_update_time'=>date('Y-m-d H:i:s')
                    );
                    $time_where=array(
                        'id'=>1
                    );
                    $result = $this->Main_model->__callUpdate('sm_update_timing',$time_where,$time_data);

                    $this->session->set_flashdata('success_msg', 'Recipe has been successfully created.');
                    redirect(base_url().'view-salads');
                }
                else
                    $this->session->set_flashdata('error_msg', 'Something went wrong.');
            }
            else
                $this->session->set_flashdata('error_msg', 'Some data is missing');
        }

    	//Calling the subcategories of main category
        if(isset($_GET['cat_id']) && $_GET['cat_id'] != ''){
            extract($_GET);
    		$sql = "select id, subcat_name, max_limit from sm_subcategories where parent_cat_id = $cat_id AND subcat_status = 1 AND salad_type = 2";
    		$data['subcats']=$this->Main_model->__callMasterquery($sql);

            $option = array('where' => array('cat_id' => $cat_id));
            $data['catRow'] = $this->Main_model->__callSelect('sm_categories', $option);
        }

        //Calling the Ingredients
        $sql = "select ing_id,ing_name,ing_name_arabic, unit_type from sm_ingredients where ing_status = 1";
        $data['ingredients']=$this->Main_model->__callMasterquery($sql);

        $sql = "select cat_id,cat_name from sm_categories where cat_status = 1 AND cat_type = 'item' order by cat_name";
		$data['catname']=$this->Main_model->__callMasterquery($sql);
        
        $data['id']=$id; //Last Created Salad ID
        $data['page_title'] = 'Add Recipe';
		$data['page'] = 'salad/add_salad';
		$this->load->view('content',$data);
    }

    //View Salads Function
    public function view_salads(){
        $data['page_title'] = 'Salad List';
        $sql="select sal.salad_id,sal.salad_name,sal.salad_name_arabic,sal.cat_id,sal.salad_img,sal.salad_status,sal.created_at,cat.cat_name from sm_salads as sal join sm_categories as cat on sal.cat_id=cat.cat_id  order by sal.salad_id desc ";
        $data['query'] = $this->Main_model->__callMasterquery($sql);
        $data['page'] = 'salad/list_salad';
		$this->load->view('content',$data);
    }

    //Load Salad Details to Edit
    public function edit($id = NULL){

        if(isset($_POST['submit'])){
            //echo "<pre>"; print_r($_POST);exit;
            extract($_POST);

            //First check the salad name is already exist ot not
            $sql = "select * from sm_salads where salad_name = '$salad_name' AND cat_id = $cat_id AND salad_id != $id";
            $exist = $this->Main_model->__callMasterquery($sql);
            if($exist->num_rows() == 0)
            {
                //uploading Excel File
                $file_details = array('upload_path'=> "./uploads/salads/", 'allowed_types' => "jpg|jpeg|gif|png|svg",  'max_size'=>"50000",'max_width'=>"50000", 'max_height'=>"50000", 'filename'=>"salad_img");
                $uploadedFile = $this->uploadfile($file_details);
                
                $where = array('salad_id'=>$id);
                $data = array('salad_name' => $salad_name,'salad_name_arabic'=>$salad_name_arabic,'salad_price'=>$salad_price, 'salad_calories'=>$salad_calories, 'salad_desc' => $salad_desc, 'salad_desc_arabic'=>$salad_desc_arabic,'cat_id'=>$cat_id);

                //Previous Image delete from folder
                if($uploadedFile != ''){
                    /*if($old_img != ''){
                        $saladImg = "./uploads/salads/{$old_img}";
                        unlink($saladImg);
                    }*/
                    $data['salad_img']=$uploadedFile;
                }  
                $update = $this->Main_model->__callUpdate('sm_salads',$where,$data);

                //update subcat last update time in sm_update_timing
                $time_data=array(
                    'salad_update_time'=>date('Y-m-d H:i:s')
                );
                $time_where=array(
                    'id'=>1
                );
                $result = $this->Main_model->__callUpdate('sm_update_timing',$time_where,$time_data);
                
                //First Delete the previous ingredients
                $where = array("salad_id" => $id);
                $delete = $this->Main_model->__callDelete('sm_salad_ingredients' ,$where);

                //Now Insert the ingredients as new
                $quantity = array_filter($quantity); //Remove Empty index
                $quantity = array_values($quantity);  //Rearrange index form 0 to ...

                //First check the salad name is already exist ot not
                if(count($ing_id) == count($quantity))
                {
                    for($i=0; $i < count($ing_id); $i++)
                    {
                        $ids = explode('-', $ing_id[$i]);
                        $ingId = $ids[0];
                        $subcat_id = $ids[1];

                        $data = array('ing_id' => $ingId, 'quantity'=>$quantity[$i], 'salad_id' => $id ,'subcat_id' => $subcat_id);        
                        $last_id = $this->Main_model->__callInsert('sm_salad_ingredients',$data);
                    }
                }
                $this->session->set_flashdata('success_msg', ' Recipe has been updated successfully.');
                redirect(base_url().'view-salads');
            }
            else
                $this->session->set_flashdata('error_msg', '<b>'. $salad_name.'</b> is already exist.');
        }
       
        //Calling the salads with Ingredeints
        $sql="select salad_id, salad_name, salad_name_arabic, salad_desc,salad_price, salad_calories, salad_desc_arabic ,cat_id, salad_img from sm_salads where salad_id=$id ";
        $data['saladInfo']=$this->Main_model->__callMasterquery($sql);

        //Calling the subcategories of main category
        if(isset($_GET['cat_id']) && $_GET['cat_id'] != ''){
            extract($_GET);
            $sql = "select id, subcat_name, max_limit from sm_subcategories where parent_cat_id = $cat_id AND subcat_status = 1 AND salad_type = 2";
            $data['subcats']=$this->Main_model->__callMasterquery($sql);
        }
        
        $data['id'] = $id;
        $data['page_title'] = 'Edit Salad';
        $data['page'] = 'salad/edit_salad';
        $this->load->view('content',$data);
    }

    //Check Salad is unique or not
    public function check_salad_unique(){
        //echo "<pre>"; print_r($_POST);exit;
        if(isset($_POST['salad_name']))
        {
            extract($_POST);
            //First check the salad name is already exist ot not
            if(isset($id)) //In case of edit
                $sql = "select * from sm_salads where salad_name = '$salad_name' AND cat_id = $cat_id AND salad_id != $id";
            else //In case of insert
                $sql = "select * from sm_salads where salad_name = '$salad_name' AND cat_id = $cat_id";
            $exist = $this->Main_model->__callMasterquery($sql);
            if($exist->num_rows() == 0)
                $result = true;
            else
                $result = false;

            echo json_encode($result);
        }
    }
    
    //get unit type
    public function getUnitType(){
        $id = $this->input->get('id');
        $sum=0;  $calory=0;
        foreach($id as $ii)
        {
            $sql="select ing_price,ing_calories from sm_ingredients where ing_id =$ii AND ing_status=1";
            $result=$this->Main_model->__callMasterquery($sql);
            if($result->num_rows()>0)
            {
                $row = $result->row(); 
                $sum = $sum+ $row->ing_price;
                $calory = $calory+ $row->ing_calories;
            }
        }
        $response['error'] = 0;
        $response['salad_price'] =  $sum;
        $response['calories_data'] = $calory;
        print_r(json_encode($response));
        exit;
    }
     
    //enable disable salad
    public function disable($id){
          
		 $deliveryadd=array(
        'salad_status'=>0
        );
        $wheres = array('salad_id'=>$id);
        $data = $this->Main_model->__callUpdate('sm_salads',$wheres,$deliveryadd); 
   		if($data){

            //update salad last update time in sm_update_timing
            $time_data=array(
                'salad_update_time'=>date('Y-m-d H:i:s')
            );
            $time_where=array('id'=>1);
            $this->Main_model->__callUpdate('sm_update_timing',$time_where,$time_data);

   			$this->session->set_flashdata('success_msg', 'Recipe Disable Successfully');
   			redirect(base_url()."view-salads/");
   		}
    }
     public function enable($id){
         $sql = "select salad_price from sm_salads where salad_id=$id";
        $exist = $this->Main_model->__callMasterquery($sql);
        $price=$exist->row();
        $price=$price->salad_price;
        if($price==0){
           $this->session->set_flashdata('error_msg', 'Please select ingredients in salad first');
			redirect(base_url()."view-salads/");
        }
    	$deliveryadd=array(
            'salad_status'=>1
        );
        $wheres = array('salad_id'=>$id);
        $data = $this->Main_model->__callUpdate('sm_salads',$wheres,$deliveryadd); 
		if($data){

            //update salad last update time in sm_update_timing
            $time_data=array(
                'salad_update_time'=>date('Y-m-d H:i:s')
            );
            $time_where=array('id'=>1);
            $this->Main_model->__callUpdate('sm_update_timing',$time_where,$time_data);
            
			$this->session->set_flashdata('success_msg', 'Recipe Enable Successfully');
			redirect(base_url()."view-salads/");
		} 
		
    }
 

    public function deleteing($id){
        $this->db->where('salad_ing_id', $id);
        $wheres = array('salad_ing_id'=>$id);
        if($this->db->delete('sm_salads_ingredients')){
   			$this->session->set_flashdata('success_msg', 'Salads ingedient deleted Successfully');
   			redirect(base_url()."view-salads");
   		} 
    }
    //salad details
    public function details($id)
	{	
		if(!is_admin()){    
            redirect(base_url().('customer'));
            exit;
        }
        $data['page_title'] = 'Salad Details';
      
		$sql="select salad_id,salad_desc,salad_name,ing_ids,salad_price,salad_img,salad_name_arabic from sm_salads where salad_id=$id";
		$ingsql="select saling.salad_ing_id,saling.salad_id,saling.ing_id,saling.si_weight,saling.si_calories,ing.ing_id,ing.ing_name,ing.ing_name_arabic,ing.unit_type,ing.ing_image from sm_salads_ingredients as saling  join sm_ingredients as ing on saling.ing_id=ing.ing_id where saling.salad_id=$id";
		
		   $data['query'] = $this->Main_model->__callMasterquery($sql);
			$data['ingredient'] = $this->Main_model->__callMasterquery($ingsql);
		   $data['page'] = 'salad/salad_details';
		$this->load->view('content',$data);
	
    }
    
    
    // Uploading file
    public function uploadfile($file_details = array())
    {  
        $file_name = $file_details['filename']; 
        $ext = pathinfo($_FILES[$file_name]['name'], PATHINFO_EXTENSION);
        $config['upload_path'] = $file_details['upload_path'];
        $config['allowed_types'] = $file_details['allowed_types'];
        $config['max_size']  = $file_details['max_size'];    
        $config['max_width']  = $file_details['max_width'];
        $config['max_height']  = $file_details['max_height'];
        $config['file_name']= md5((time()*rand())).".".$ext;

        //$file_name = 'group_photo';

        $this->load->library('Upload',$config);             
        $this->upload->initialize($config);

        $upload = $this->upload->do_upload($file_name);
        if($_FILES[$file_name]['size'] != 0)
        {
            if($upload == NULL)
            {
                $this->session->set_flashdata('error_msg', $this->upload->display_errors());
                redirect($_SERVER['HTTP_REFERER']);
                //$response_data = array("status" => 0,"msg" => $this->upload->display_errors(),"url" => '');
            }
            else{
                $file_data = $this->upload->data();  
                $this->resizeImage($file_data['full_path']);   
                return $imgName = $file_data['file_name'];
            }
        }
    }
    public function resizeImage($filename)
   {
		$this->load->library('image_lib');
		$resized_path = './uploads/thumbnail/salads/';
    	 $config = array(
            'source_image' => $filename, //path to the uploaded image
            'new_image' => $resized_path,
            'maintain_ratio' => true,
            'width' => 100,
            'height' => 100
        );
		$image=$this->image_lib->initialize($config);
		$this->image_lib->resize();
      if (!$this->image_lib->resize()) {
		  echo $this->image_lib->display_errors();
	  }
	   
	
      $this->image_lib->clear();
   }
}
