<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SaladController extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->model('General_model');
        $this->load->library('session');
        if (!($this->session->userdata('language'))) {
            $this->session->set_userdata('language', 'English');
        }
        $this->lang->load('content_form_' .$this->session->userdata('language'));
        //echo($this->session->userdata('language'));exit;
    }
    public function index(){
        //page title
        $data['title']='Menu';
        //view file path
        $data['page']='frontend/salad/index';

        //load categories
        $select_fields=array(
            'cat_name','cat_id','cat_name_arabic','contains_make_own',
        );
        $where=array(
            'cat_status'=>1
        );
        $order_by    = 'cat_id asc';

        $data['categories']=$this->General_model->get_multiple_row('sm_categories',$select_fields,$where,$order_by);

        $this->load->view('frontend/home',$data);
    }

    public function add_to_cart(){
        $where=array(
            's.salad_id'=>$_POST['id']
        );
        $this->db->select('s.salad_name,s.salad_name_arabic,s.salad_price,i.ing_name_arabic,c.cat_name,c.cat_name_arabic,i.cat_id,i.ing_name');
        $this->db->from('sm_salads as s');
        $this->db->join('sm_salads_ingredients as si',' si.salad_id=s.salad_id');
        $this->db->join('sm_ingredients as i','i.ing_id=si.ing_id');
        $this->db->join('sm_categories as c','c.cat_id=i.cat_id');
        $this->db->where($where);
        $this->db->where('i.ing_status',1);
        $query=$this->db->get();
        $data=$query->result();
        if($data){


            $salad_name = array_column($data, 'salad_name');
            $salad_price = array_column($data, 'salad_price');
            $ing_names = array_column($data, 'ing_name');
            $cat_names = array_unique(array_column($data, 'cat_name'));
            $cat_ids = array_unique(array_column($data, 'cat_id'));
            $category_name=array_combine($cat_names,$cat_ids);
            $language= $this->session->userdata('language');
            if($language=='Arabic'){
                $salad_name = array_column($data, 'salad_name_arabic');
                $ing_names = array_column($data, 'ing_name_arabic');
                $cat_names = array_unique(array_column($data, 'cat_name_arabic'));
                $category_name=array_combine($cat_names,$cat_ids);
                $ing_name=$ing->ing_name_arabic;
            }
            $loadHtml='';
            $loadHtml.=' <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="myModalLabel"><span style="font-weight:800">'.$salad_name[0].'</span>
                    <div class="bi-qty" style="float: right">
                        Qty:
                        <button type="button" id="sub" class="sub">-</button>
                        <input type="number" id="1" value="1" min="1" max="9" />
                        <button type="button" id="add" class="add">+</button>
                        &nbsp;&nbsp; <span style="color:#ccc"> |</span> &nbsp;&nbsp;
                        '.$salad_price[0].'
                    </div>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin: 0">
                    <div class="tab">';
            $i=1;
            foreach ($category_name as $name =>$id){
                $loadHtml.='<button class="tablinks" onclick="openCity(event, \'t'.$id.'\')" id="defaultOpen">'.$name.'</button>';
                $i++;
            }
            $loadHtml.=' 
                    </div>';
            foreach ($cat_ids as $id){
                $loadHtml.='
                        <div id="t'.$id.'" class="tabcontent">
                        <h5>Ingredients</h5>';
                foreach ($data as $ing){
                    if($ing->cat_id==$id){
                        $loadHtml.=' <div class="item-selection">
                                    <label class="selection">
                                        <img src="img/makeown/Corn.jpg" height="48"/>'.$ing_name.' 1 <b>10 '.$this->lang->line('cal').'</b>
        
                                        <input class="checked" type="checkbox" value="d1" name="dressings">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>';
                    }

                }
                $loadHtml.=' </div>';

            }
            $loadHtml.=' 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">'.$this->lang->line('cancel').'</button>
                    <button type="button" class="btn btn-primary"><i class="fa fa-shopping-bag"></i>'.$this->lang->line('add_bag').'</button>
                </div>
            </div>
        </div>

    </div>';
            $response=array(
                'status'=>true,
                'data'=>$loadHtml
            );
        }else{

        }
        echo json_encode($response);

    }

    public function make_your_own(){
//        $this->session->unset_userdata('cart_data');
//
//        $this->session->set_userdata('cart_data');
//        die;
        $select_fields=array(
            'cat_name','cat_id','cat_name_arabic',
        );

        $where=array(
            'cat_status'=>1
        );

        $order_by    = 'cat_id asc';

        $data['categories']=$this->General_model->get_multiple_row('sm_categories',$select_fields,$where,$order_by);
        $cart_array=array();
        
       
        $data['user_id']=null;

        if($this->session->userdata('user_login_session')){
            $login=$this->session->userdata('user_login_session');
            $data['user_id']=$login['id'];
           
            $where=array(
                'c.user_id'=>$login['id']
            );
            $this->db->select('c.cart_id,c.user_id,c.salad_id,c.cat_id as cust_cat_id,c.salad_type,c.quantity,c.salad_name as cust_salad_name,s.salad_price,c.salad_image as cust_salad_image,s.salad_name,s.salad_name_arabic,s.salad_img');
            $this->db->from('sm_carts c');
            $this->db->join('sm_salads s','s.salad_id=c.salad_id','left');
            $this->db->where($where);
            $query=$this->db->get();
            $carts=$query->result_array();

            if($carts){
                foreach ($carts as $cart){
                    if($cart['salad_type'] == 1){
                        $where=array(
                            'si.salad_id'=>$cart['salad_id']
                        );
                        $this->db->select('');
                        $this->db->group_by('subcat_id');
                        $this->db->from('sm_salad_ingredients as si');
                        $this->db->join('sm_ingredients as i','i.ing_id=si.ing_id');
                        $this->db->where($where);
                        $this->db->where('i.ing_status',1);
                        $query=$this->db->get();
                        $main_ingrediants=$query->result_array();
                        $cart['main_ingrediants']=$main_ingrediants;
                        $main_ingrediant_price=0;
                        if(count($cart['main_ingrediants']) > 0){
                            foreach ($cart['main_ingrediants'] as $ing){
                                $price=0;
                                $price=$cart['salad_price'];
                            }
                             $main_ingrediant_price=$main_ingrediant_price+$price;
                            
                        }

                        $where=array(
                            'cart_id'=>$cart['cart_id']
                        );
                        $this->db->select('');
                        $this->db->from('sm_cart_ingredients as ci');
                        $this->db->join('sm_ingredients as i','i.ing_id=ci.ing_id');
                        $this->db->where($where);
                        $this->db->where('i.ing_status',1);
                        $query=$this->db->get();
                        $extra_ingrediants=$query->result_array();
                        $cart['extra_ingrediants']=$extra_ingrediants;

                        $extra_ingrediant_price=0;
                        if(count($cart['extra_ingrediants']) > 0){
                            foreach ($cart['extra_ingrediants'] as $ing){
                                $price=0;
                                //$price=$ing['ing_price'] * $ing['quantity'];
                                
                                //Query to Get ing price with subcategory
                                $ing_id = $ing['ing_id'];
                                $subcat_id = $ing['subcat_id'];
                                $sql = "select sing_price from sm_ingredients_subcats where ing_id = $ing_id AND subcat_id = $subcat_id";
                                $query = $this->db->query($sql);
                                if($query->num_rows() > 0){
                                    $rs = $query->row();
                                    $price = $rs->sing_price;
                                }

                                $price = $price * $ing['quantity'];
                                $extra_ingrediant_price=$extra_ingrediant_price+$price;
                               
                            }
                            
                        }

                        $cart['total_price']=($main_ingrediant_price+$extra_ingrediant_price)*$cart['quantity'];
                   

                    }elseif ($cart['salad_type'] == 2){
                        $where=array(
                            'cart_id'=>$cart['cart_id']
                        );
                        $this->db->select('');
                        $this->db->from('sm_cart_ingredients as ci');
                        $this->db->join('sm_ingredients as i','i.ing_id=ci.ing_id');
                        $this->db->where($where);
                        $this->db->where('i.ing_status',1);
                        $query=$this->db->get();
                        $extra_ingrediants=$query->result_array();
                        $cart['extra_ingrediants']=$extra_ingrediants;
                        $main_ingrediant_price=0;
                        if(count($cart['extra_ingrediants']) > 0){
                            foreach ($cart['extra_ingrediants'] as $ing){
                                $price=0;
                                //$price=$ing['ing_price'] * $ing['quantity'];

                                //Query to Get ing price with subcategory
                                $ing_id = $ing['ing_id'];
                                $subcat_id = $ing['subcat_id'];
                                $sql = "select sing_price from sm_ingredients_subcats where ing_id = $ing_id AND subcat_id = $subcat_id";
                                $query = $this->db->query($sql);
                                if($query->num_rows() > 0){
                                    $rs = $query->row();
                                    $price = $rs->sing_price;
                                }
                                
                                $price = $price * $ing['quantity'];
                                $main_ingrediant_price=$main_ingrediant_price+$price;
                            }
                            
                        }
                        $cart['total_price']=$main_ingrediant_price*$cart['quantity'];

                    }

                    $cart_array[]=$cart;
                }
            }

        }
        elseif ($this->session->userdata('cart_data')){

            $session_data=$this->session->userdata('cart_data');
          // echo'<pre>'; print_r($session_data);
            if($session_data){
                $cart=array();
                foreach ($session_data as $key=>$sess){
                    $where=array(
                        'salad_id'=>$sess['salad_id']
                    );
                    $this->db->select('');
                    $this->db->from('sm_salads');
                    $this->db->where($where);
                    $query=$this->db->get();
                    $cart=$query->row_array();
                   
                    $cart['salad_type']=$sess['salad_type'];
                  
                
                    $cart['quantity']=$sess['quantity'];
                    $cart['key']=$key;

                    if($sess['salad_type'] ==1){
                          $cart['salad_price']=$cart['salad_price'];
                        $where=array(
                            'si.salad_id'=>$sess['salad_id']
                        );
                        $this->db->select('');
                        $this->db->group_by('subcat_id');
                        $this->db->from('sm_salad_ingredients as si');
                        $this->db->join('sm_ingredients as i','i.ing_id=si.ing_id');
                        $this->db->where($where);
                        $this->db->where('i.ing_status',1);
                        $query=$this->db->get();
                        $main_ingrediants=$query->result_array();
                        $cart['main_ingrediants']=$main_ingrediants;
                        $main_ingrediant_price=0;
                        if(count($cart['main_ingrediants']) > 0){
                            foreach ($cart['main_ingrediants'] as $ing){
                                $price = 0;
                                $price = $cart['salad_price'];
                            }
                            $main_ingrediant_price=$main_ingrediant_price+$price;
                        }
                        $cart['extra_ingrediants']=$sess['extra_ingrediants'];

                        $extra_ingrediant_price=0;
                        if(count($sess['extra_ingrediants']) > 0){
                            foreach ($sess['extra_ingrediants'] as $ing){

                                $price=0;
                                //$price=getSingleFieldDetail('ing_id',$ing['ing_id'],'ing_price','sm_ingredients') * $ing['quantity'];

                                //Query to Get ing price with subcategory
                                $ing_id = $ing['ing_id'];
                                $subcat_id = $ing['subcat_id'];
                                $sql = "select sing_price from sm_ingredients_subcats where ing_id = $ing_id AND subcat_id = $subcat_id";
                                $query = $this->db->query($sql);
                                if($query->num_rows() > 0){
                                    $rs = $query->row();
                                    $price = $rs->sing_price;
                                }

                                $extra_ingrediant_price=$extra_ingrediant_price+$price;
                            }
                             
                        }

                        $cart['total_price']=($main_ingrediant_price+$extra_ingrediant_price)*$sess['quantity'];
                    }
                    elseif ($cart['salad_type'] == 2){
                        $cart['cust_salad_name']=$sess['salad_name'];
                        $cart['cust_salad_name_arabic']=$sess['salad_name'];
                        $cart['cust_salad_image']='custom.png';
                        $cart['extra_ingrediants']=$sess['extra_ingrediants'];

                        $extra_ingrediant_price=0;
                        if(count($sess['extra_ingrediants']) > 0){
                            foreach ($sess['extra_ingrediants'] as $ing){

                                $price=0;
                                //$price=getSingleFieldDetail('ing_id',$ing['ing_id'],'ing_price','sm_ingredients') * $ing['quantity'];

                                //Query to Get ing price with subcategory
                                $ing_id = $ing['ing_id'];
                                $subcat_id = $ing['subcat_id'];
                                $sql = "select sing_price from sm_ingredients_subcats where ing_id = $ing_id AND subcat_id = $subcat_id";
                                $query = $this->db->query($sql);
                                if($query->num_rows() > 0){
                                    $rs = $query->row();
                                    $price = $rs->sing_price;
                                }

                               $extra_ingrediant_price=$extra_ingrediant_price+$price;
                            }
                             
                        }

                        $cart['total_price']=($extra_ingrediant_price)*$sess['quantity'];

                    }

                    $cart_array[]=$cart;
                }
//                echo '<pre>';
//                print_r($cart_array);
//                die;
            }
        }else{
            $data['cart_details']=null;
        }
        
        if($this->session->userdata('guest_user_session')){
            $login=$this->session->userdata('guest_user_session');
            $data['user_id']=$login['id'];
        }
      
        $data['carts']=$cart_array;
       //echo '<pre>';print_r( $data['carts']);exit;
//        echo '<pre>';
//        print_r($data['carts']);
//        die;
        $data['title']='Make Your Own';
        $data['page']='frontend/make_your_own';
        $this->load->view('frontend/home',$data);
    }

    public function insert_data_into_cart(){
        $login=$this->session->userdata('user_login_session');
        if($login){
            if($_POST['salad_type'] ==='ready_made'){
                $data=array(
                    'user_id'=>$login['id'],
                    'salad_type'=>1,
                    'salad_id'=>$_POST['salad_id'],
                    'cat_id'=>$_POST['cat_id'],
                    'quantity'=>$_POST['qty'],
                    'dressing_mixed_in'=>0,
                    'special_request'=>$_POST['special_request']

                );
                $last_id=$this->General_model->get_lastinsertid_after_insertdata('sm_carts',$data);
                if($last_id){
                    if(isset($_POST['ingrediants']) && $_POST['ingrediants'] >0){
                        foreach ($_POST['ingrediants'] as $ings){
                            $data=array(
                                'ing_id'=>$ings['ing_id'],
                                'quantity'=>$ings['quantity'],
                                'subcat_id'=>$ings['cat_id'],
                                'cart_id'=>$last_id
                            );
                            $this->db->insert('sm_cart_ingredients',$data);
                        }
                    }
                    $response=array(
                        'status'=>true
                    );

                }else{
                    $response=array(
                        'status'=>false
                    );
                }


            }elseif ($_POST['salad_type'] ==='create_own'){
                $data=array(
                    'user_id'=>$login['id'],
                    'salad_type'=>2,
                    'salad_id'=>$_POST['salad_id'],
                    'salad_name'=>$_POST['salad_name'],
                    'salad_image'=>'custom.png',
                    'cat_id'=>$_POST['cat_id'],
                    'quantity'=>$_POST['qty'],
                    'dressing_mixed_in'=>0,
                    'special_request'=>$_POST['special_request']

                );
                $last_id=$this->General_model->get_lastinsertid_after_insertdata('sm_carts',$data);
                if($last_id){
                    if(isset($_POST['ingrediants']) && $_POST['ingrediants'] >0){
                        foreach ($_POST['ingrediants'] as $ings){
                            $data=array(
                                'ing_id'=>$ings['ing_id'],
                                'quantity'=>$ings['quantity'],
                                'subcat_id'=>$ings['cat_id'],
                                'cart_id'=>$last_id
                            );
                            $this->db->insert('sm_cart_ingredients',$data);
                        }
                    }
                    $response=array(
                        'status'=>true
                    );

                }else{
                    $response=array(
                        'status'=>false
                    );
                }
            }else{
                $response=array(
                    'status'=>false
                );
            }

        }else{
            if($_POST['salad_type'] ==='ready_made'){
               
                $data=array(
                    'user_id'=>0,
                    'salad_type'=>1,
                    'salad_id'=>$_POST['salad_id'],
                    'cat_id'=>$_POST['cat_id'],
                    'quantity'=>$_POST['qty'],
                    'extra_ingrediants'=>array(),
                    'dressing_mixed_in'=>0,
                    'special_request'=>$_POST['special_request']

                );

                if(isset($_POST['ingrediants']) && count($_POST['ingrediants']) >0){
                    $extra=array();
                    foreach ($_POST['ingrediants'] as $ings){
                        $extra_ingrediants=array(
                            'ing_id'=>$ings['ing_id'],
                            'ing_name'=>getSingleFieldDetail('ing_id',$ings['ing_id'],'ing_name','sm_ingredients'),
                            'ing_name_arabic'=>getSingleFieldDetail('ing_id',$ings['ing_id'],'ing_name_arabic','sm_ingredients'),
                            'quantity'=>$ings['quantity'],
                            'subcat_id'=>$ings['cat_id'],
                        );
                        $extra[]=$extra_ingrediants;
                    }
                    $data['extra_ingrediants']=$extra;
                }
                if($this->session->userdata('cart_data')){
                    $previous_cart_data=$this->session->userdata('cart_data');
                    array_push($previous_cart_data,$data);

                }else{
                    $previous_cart_data=array();
                    array_push($previous_cart_data,$data);
                }
                $this->session->set_userdata('cart_data',$previous_cart_data);
                if( $this->session->userdata('cart_data')){
                    $response=array(
                        'status'=>true,

                    );
                }else{
                    $response=array(
                        'status'=>false,
                    );
                }




            }elseif ($_POST['salad_type'] ==='create_own') {
              
                $data = array(
                    'user_id' => 0,
                    'salad_type' => 2,
                    'salad_id' => $_POST['salad_id'],
                    'salad_name'=>$_POST['salad_name'],
                    'cat_id' => $_POST['cat_id'],
                    'quantity' => $_POST['qty'],
                    'extra_ingrediants' => array(),
                    'dressing_mixed_in' => 0,
                    'special_request' => $_POST['special_request']

                );

                if (isset($_POST['ingrediants']) && count($_POST['ingrediants']) > 0) {
                    $extra = array();
                    foreach ($_POST['ingrediants'] as $ings) {
                        $extra_ingrediants = array(
                            'ing_id' => $ings['ing_id'],
                            'ing_name' => getSingleFieldDetail('ing_id', $ings['ing_id'], 'ing_name', 'sm_ingredients'),
                            'ing_name_arabic' => getSingleFieldDetail('ing_id', $ings['ing_id'], 'ing_name_arabic', 'sm_ingredients'),
                            'quantity' => $ings['quantity'],
                            'subcat_id' => $ings['cat_id'],
                        );
                        $extra[] = $extra_ingrediants;
                    }
                    $data['extra_ingrediants'] = $extra;
                }
                if ($this->session->userdata('cart_data')) {
                    $previous_cart_data = $this->session->userdata('cart_data');
                    array_push($previous_cart_data, $data);

                } else {
                    $previous_cart_data = array();
                    array_push($previous_cart_data, $data);
                }
                $this->session->set_userdata('cart_data', $previous_cart_data);
                if ($this->session->userdata('cart_data')) {
                    $response = array(
                        'status' => true,

                    );
                } else {
                    $response = array(
                        'status' => false,
                    );
                }
            }

        }

        echo json_encode($response);
    }

    public function confirm_order($delivery_type){
        if($this->session->userdata('user_login_session') && ($delivery_type=='delivery' || $delivery_type=='pickup')){
            $login=$this->session->userdata('user_login_session');
            $data['delivery_type']=$delivery_type;

            $where=array(
                'c.user_id'=>$login['id']
            );
            $this->db->select('c.cart_id,c.user_id,c.salad_id,c.cat_id as cust_cat_id,c.salad_type,c.quantity,c.salad_name as cust_salad_name,c.salad_image as cust_salad_image,s.salad_name,s.salad_price,s.salad_name_arabic,s.salad_img');
            $this->db->from('sm_carts c');
            $this->db->join('sm_salads s','s.salad_id=c.salad_id','left');
            $this->db->where($where);
            $query=$this->db->get();
            $carts=$query->result_array();
            $cart_array=array();
            if($carts){
                foreach ($carts as $cart){
                    if($cart['salad_type'] ==1){
                        $where=array(
                            'si.salad_id'=>$cart['salad_id']
                        );
                        $this->db->select('');
                        $this->db->group_by('subcat_id');
                        $this->db->from('sm_salad_ingredients as si');
                        $this->db->join('sm_ingredients as i','i.ing_id=si.ing_id');
                        $this->db->where($where);
                        $this->db->where('i.ing_status',1);
                        $query=$this->db->get();
                        $main_ingrediants=$query->result_array();
                        $cart['main_ingrediants']=$main_ingrediants;
                        $main_ingrediant_price=0;
                        if(count($cart['main_ingrediants']) > 0){
                            foreach ($cart['main_ingrediants'] as $ing){
                                $price=0;
                               $price=$cart['salad_price'];
                                
                            }
                            $main_ingrediant_price=$main_ingrediant_price+$price;
                        }


                        $where=array(
                            'cart_id'=>$cart['cart_id']
                        );
                        $this->db->select('');
                        $this->db->from('sm_cart_ingredients as ci');
                        $this->db->join('sm_ingredients as i','i.ing_id=ci.ing_id');
                        $this->db->where($where);
                        $this->db->where('i.ing_status',1);
                        $query=$this->db->get();
                        $extra_ingrediants=$query->result_array();
                        $cart['extra_ingrediants']=$extra_ingrediants;

                        $extra_ingrediant_price=0;
                        if(count($cart['extra_ingrediants']) > 0){
                            foreach ($cart['extra_ingrediants'] as $ing){
                                $price=0;
                                  //Query to Get ing price with subcategory
                                $ing_id = $ing['ing_id'];
                                $subcat_id = $ing['subcat_id'];
                                $sql = "select sing_price from sm_ingredients_subcats where ing_id = $ing_id AND subcat_id = $subcat_id";
                                $query = $this->db->query($sql);
                                if($query->num_rows() > 0){
                                    $rs = $query->row();
                                    $price = $rs->sing_price;
                                }
                                $price=$price * $ing['quantity'];
                                 $extra_ingrediant_price=$extra_ingrediant_price+$price;
                               
                            }
                            
                        }

                        $cart['total_price']=($main_ingrediant_price+$extra_ingrediant_price)*$cart['quantity'];
                    }
                    elseif ($cart['salad_type'] == 2){

                        $where=array(
                            'cart_id'=>$cart['cart_id']
                        );
                        $this->db->select('');
                        $this->db->from('sm_cart_ingredients as ci');
                        $this->db->join('sm_ingredients as i','i.ing_id=ci.ing_id');
                        $this->db->where($where);
                        $this->db->where('i.ing_status',1);
                        $query=$this->db->get();
                        $extra_ingrediants=$query->result_array();
                        $cart['extra_ingrediants']=$extra_ingrediants;
                        $main_ingrediant_price=0;
                        if(count($cart['extra_ingrediants']) > 0){
                            foreach ($cart['extra_ingrediants'] as $ing){
                                $price=0;
                                  //Query to Get ing price with subcategory
                                $ing_id = $ing['ing_id'];
                                $subcat_id = $ing['subcat_id'];
                                $sql = "select sing_price from sm_ingredients_subcats where ing_id = $ing_id AND subcat_id = $subcat_id";
                                $query = $this->db->query($sql);
                                if($query->num_rows() > 0){
                                    $rs = $query->row();
                                    $price = $rs->sing_price;
                                }
                                $price=$price * $ing['quantity'];
                                $main_ingrediant_price=$main_ingrediant_price+$price;
                            }
                        }
                        $cart['total_price']=$main_ingrediant_price*$cart['quantity'];

                    }

                    $cart_array[]=$cart;
                }
            }

            $data['carts']=$cart_array;
            if($this->session->userdata('confirm_address')) {

                $where = array(
                    'add_id'=>$this->session->userdata('confirm_address')
                );

                $this->db->select('');
                $this->db->where($where);
                $this->db->from('sm_addresses');
                $query = $this->db->get();
                $data['address'] = $query->row();
            }else{
                $data['address']=array();
            }
            $data['delivery_type']=$delivery_type;
        } elseif ($this->session->userdata('cart_data')){
                $session_data = $this->session->userdata('cart_data');
                if ($session_data) {
                    $cart = array();
                    foreach ($session_data as $sess) {
                        $where = array(
                            'salad_id' => $sess['salad_id']
                        );
                        $this->db->select('');
                        $this->db->from('sm_salads');
                        $this->db->where($where);
                        $query = $this->db->get();
                        $cart = $query->row_array();
                        $cart['salad_type'] = $sess['salad_type'];

                        $cart['quantity'] = $sess['quantity'];
                        //  $cart['salad_price']=$cart['salad_price'];

                        if ($sess['salad_type'] == 1) {
                            $where = array(
                                'si.salad_id' => $sess['salad_id']
                            );
                            $this->db->select('');
                            $this->db->group_by('subcat_id');
                            $this->db->from('sm_salad_ingredients as si');
                            $this->db->join('sm_ingredients as i', 'i.ing_id=si.ing_id');
                            $this->db->where($where);
                            $this->db->where('i.ing_status',1);
                            $query = $this->db->get();
                            $main_ingrediants = $query->result_array();
                            $cart['main_ingrediants'] = $main_ingrediants;
                            $main_ingrediant_price = 0;
                            if (count($cart['main_ingrediants']) > 0) {
                                foreach ($cart['main_ingrediants'] as $ing) {
                                    $price = 0;
                                    $price =  $cart['salad_price'];
                                   
                                }
                                 $main_ingrediant_price = $main_ingrediant_price + $price;
                            }
                            $cart['extra_ingrediants'] = $sess['extra_ingrediants'];

                            $extra_ingrediant_price = 0;
                            if (count($sess['extra_ingrediants']) > 0) {
                                foreach ($sess['extra_ingrediants'] as $ing) {

                                    $price = 0;
                                    // $price = getSingleFieldDetail('ing_id', $ing['ing_id'], 'ing_price', 'sm_ingredients') * $ing['quantity'];
                                    //Query to Get ing price with subcategory
                                    $ing_id = $ing['ing_id'];
                                    $subcat_id = $ing['subcat_id'];
                                    $sql = "select sing_price from sm_ingredients_subcats where ing_id = $ing_id AND subcat_id = $subcat_id";
                                    $query = $this->db->query($sql);
                                    if($query->num_rows() > 0){
                                        $rs = $query->row();
                                        $price = $rs->sing_price;
                                    }
                                    $extra_ingrediant_price = $extra_ingrediant_price + $price;
                                }
                                
                            }

                            $cart['total_price'] = ($main_ingrediant_price + $extra_ingrediant_price) * $sess['quantity'];

                        } elseif ($cart['salad_type'] == 2) {
                            $cart['cust_salad_name'] = $sess['salad_name'];
                            $cart['cust_salad_name_arabic'] = $sess['salad_name'];
                            $cart['cust_salad_image'] = 'custom.png';
                            $cart['extra_ingrediants'] = $sess['extra_ingrediants'];

                            $extra_ingrediant_price = 0;
                            if (count($sess['extra_ingrediants']) > 0) {
                                foreach ($sess['extra_ingrediants'] as $ing) {
                                    $price = 0;
                                    // $price = getSingleFieldDetail('ing_id', $ing['ing_id'], 'ing_price', 'sm_ingredients') * $ing['quantity'];
                                    //Query to Get ing price with subcategory
                                    $ing_id = $ing['ing_id'];
                                    $subcat_id = $ing['subcat_id'];
                                    $sql = "select sing_price from sm_ingredients_subcats where ing_id = $ing_id AND subcat_id = $subcat_id";
                                    $query = $this->db->query($sql);
                                    if($query->num_rows() > 0){
                                        $rs = $query->row();
                                        $price = $rs->sing_price;
                                    }
                                    $extra_ingrediant_price = $extra_ingrediant_price + $price;
                                }
                                  
                            }

                            $cart['total_price'] = ($extra_ingrediant_price) * $sess['quantity'];

                        }

                        $cart_array[] = $cart;
                    }
//                echo '<pre>';
//                print_r($cart_array);
//                die;

                }


            if($this->session->userdata('confirm_address')) {

                $where = array(
                    'add_id'=>$this->session->userdata('confirm_address')
                );

                $this->db->select('');
                $this->db->where($where);
                $this->db->from('sm_addresses');
                $query = $this->db->get();
                $data['address'] = $query->row();
            }else{
                $data['address']=array();
            }
            $data['carts'] = $cart_array;
            $data['delivery_type']=$delivery_type;
        }else{
            $data['carts']=array();
        }

        //page title
        $data['title']='Confirm Order';
        //view file path
        $data['page']='frontend/confirm_order';

        $this->load->view('frontend/home',$data);
    }

    //Update Order pricing in statics table to call in order instertion
    public function insert_order_pricing(){
        
        if($this->session->userdata('user_login_session')){
            $login=$this->session->userdata('user_login_session');
        }
        elseif ($this->session->userdata('guest_user_session')){
            $login=$this->session->userdata('guest_user_session');
        }

        if(isset($login['id']))
        {
            extract($_POST);

            $user_id = $login['id'];
            $option = array('where' => array('user_id' => $user_id));
            $exist = $this->Main_model->__callSelect('sm_user_order_statics',$option);
                
            //update the data
            if($exist->num_rows() > 0)
            {
                $where = array('user_id' => $user_id);
                $data = array('sub_total' => $sub_total, 'tax_charges' => $tax_charges, 'delivery_charges' => $delivery_charges, 'total_price' => $total_price, 'updated_at' => date('Y-m-d H:i:s'));
                $result = $this->Main_model->__callUpdate('sm_user_order_statics', $where, $data);
            }
            //insert as new
            else{
                $data = array('user_id' => $user_id, 'sub_total' => $sub_total, 'tax_charges' => $tax_charges, 'delivery_charges' => $delivery_charges, 'total_price' => $total_price, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'));
                $result = $this->Main_model->__callInsert('sm_user_order_statics', $data);
            }

            echo 'true';exit;
            //$response=array('status'=>true);
            //echo json_encode($response);
        }
    }


    public function address_update(){
        if($this->session->userdata('user_login_session')){
            $login=$this->session->userdata('user_login_session');
        }elseif ($this->session->userdata('guest_user_session')){
            $login=$this->session->userdata('guest_user_session');
        }

        $addData = array(
            'user_id' => $login['id'],
            'addline1' => $_POST['address1'],
            'latitude' => $_POST['latitude'],
            'longitude' => $_POST['logitude'],
            'city_id' => 1,
            'add_type' => 'delivery',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        );
        $slast_insert_id=$this->General_model->get_lastinsertid_after_insertdata('sm_addresses',$addData);


        if($slast_insert_id){
            $this->session->set_userdata('confirm_address',$slast_insert_id);
            $response=array(
                'status'=>true,
                'msg'=>$this->lang->line('update_address_success'),
            );
        }else{
            $response=array(
                'status'=>false,
                'msg'=>$this->lang->line('update_address_error'),
            );
        }

        echo json_encode($response);
    }

    public function checkout_order(){
        if(isset($_POST['checkout'])){
            $dataString = $_POST['checkout'];
            $data = json_decode(json_encode($dataString),true);
            if($data[0]['delivery_type']=='pickup'){
                $checkout=array(
                    'deliver_type'=>$data[0]['delivery_type'],
                    'date'=>$data[0]['pickup_date'],
                    'time'=>$data[0]['pickup_time'],
                    'name'=>$data[0]['name'],
                    'mobile'=>$data[0]['mobile'],
                );

            }elseif ($data[0]['delivery_type']=='delivery'){
                $checkout=array(
                    'deliver_type'=>$data[0]['delivery_type'],
                );
            }

            $this->session->set_userdata('checkout_session',$checkout);
            $response=array(
                'status'=>true
            );
        }else{
            $response=array(
                'status'=>false
            );
        }

        echo json_encode($response);
    }
    public function create_order(){
        $insertId=0;
        $email=false;
        extract($_POST);

        if(isset($_POST['order_type'])){

            if($this->session->userdata('user_login_session')){

                $login=$this->session->userdata('user_login_session');

                //Get the order pricing form User order statics 
                $user_id = $login['id'];
                $option = array('where' => array('user_id' => $user_id));
                $exist = $this->Main_model->__callSelect('sm_user_order_statics',$option);
                    
                if($exist->num_rows() > 0)
                {
                    $row = $exist->row();
                    $sub_total = $row->sub_total;
                    $tax_charges = $row->tax_charges;
                    $delivery_charges = $row->delivery_charges;
                    $total_price = $row->total_price;
                }

                $checkout_session=$this->session->userdata('checkout_session');
                //echo "<pre>"; print_r($checkout_session);exit;

                if($checkout_session['deliver_type'] === 'delivery'){
                    $data=array(
                        'user_id'=>$login['id'],
                        'total_price'=>$sub_total,
                        'discount_amt'=>0,
                        'tax_amt'=>$tax_charges,
                        'delivery_charge'=>$delivery_charges,
                        'grand_total'=>$total_price,
                        'payment_status'=>0,
                        'payment_method'=>$_POST['order_type'],
                        'in_store_pickup'=>0,
                        'address_id'=>$this->session->userdata('confirm_address'),
                        'customer_name'=>getSingleFieldDetail('id',$login['id'],'full_name','sm_users'),
                        'create_date'=>date("Y-m-d"),
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    );
                }
                elseif ($checkout_session['deliver_type'] === 'pickup'){
                    $data=array(
                        'user_id'=>$login['id'],
                        'total_price'=>$sub_total,
                        'discount_amt'=>0,
                        'tax_amt'=>$tax_charges,
                        'delivery_charge'=>$delivery_charges,
                        'grand_total'=>$total_price,
                        'payment_status'=>0,
                        'payment_method'=>$_POST['order_type'],
                        'in_store_pickup'=>1,
                        'customer_name'=>$checkout_session['name'],
                        'customer_phone'=>$checkout_session['mobile'],
                        'pickup_date'=>$checkout_session['date'],
                        'pickup_time'=>$checkout_session['time'],
                        'create_date'=>date("Y-m-d"),
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    );
                }

                $status=$this->db->insert('sm_orders',$data);
                
                //order id
                $insertId = $this->db->insert_id();
               
                if($insertId){

                    //Delete this order pricing form statics table form this user
                    $where = array("user_id" => $login['id']);
                    $this->Main_model->__callDelete('sm_user_order_statics' ,$where);
    
                    //insert into notification table
                    $order_id=strtoupper(encryptId($insertId));
                    $not_data=array(
                        'noti_msg'=>'You have a new order: SM-'.$order_id,
                        'module_id'=>$insertId,
                        'noti_for'=>'A',
                    );
                    $this->db->insert('sm_notifications',$not_data);


                    $where=array(
                        'c.user_id'=>$login['id']
                    );
                    $this->db->select('c.cart_id,c.user_id,c.salad_id,c.cat_id as cust_cat_id,s.salad_price,s.salad_calories,c.salad_type,c.quantity,c.salad_name as cust_salad_name,c.salad_image as cust_salad_image,s.salad_name,s.salad_name_arabic,s.salad_img,s.cat_id');
                    $this->db->from('sm_carts c');
                    $this->db->join('sm_salads s','s.salad_id=c.salad_id','left');
                    $this->db->where($where);
                    $query=$this->db->get();
                    $carts=$query->result_array();
                    $cart_array=array();
                    if($carts){
                        foreach ($carts as $cart){
                            if($cart['salad_type'] ==1){
                                $where=array(
                                    'si.salad_id'=>$cart['salad_id']
                                );
                                $this->db->select('');
                                $this->db->group_by('subcat_id');
                                $this->db->from('sm_salad_ingredients as si');
                                $this->db->join('sm_ingredients as i','i.ing_id=si.ing_id');
                                $this->db->where($where);
                                $this->db->where('i.ing_status',1);
                                $query=$this->db->get();
                                $main_ingrediants=$query->result_array();
                                $cart['main_ingrediants']=$main_ingrediants;
                                $main_ingrediant_price=0;
                                $main_ingrediant_calories=0;
                                if(count($cart['main_ingrediants']) > 0){
                                    foreach ($cart['main_ingrediants'] as $ing){
                                        $price=0;
                                        $price=$cart['salad_price'];
                                        $calories=0;
                                        $calories= $cart['salad_calories'];
                                    }
                                    $main_ingrediant_price=$main_ingrediant_price+$price;
                                    $main_ingrediant_calories=$main_ingrediant_calories+$calories;

                                }


                                $where=array(
                                    'cart_id'=>$cart['cart_id']
                                );
                                $this->db->select('');
                                $this->db->from('sm_cart_ingredients as ci');
                                $this->db->join('sm_ingredients as i','i.ing_id=ci.ing_id');
                                $this->db->where($where);
                                $this->db->where('i.ing_status',1);
                                $query=$this->db->get();
                                $extra_ingrediants=$query->result_array();
                                $cart['extra_ingrediants']=$extra_ingrediants;

                                $extra_ingrediant_price=0;
                                $extra_ingrediant_calories=0;
                                if(count($cart['extra_ingrediants']) > 0){
                                    foreach ($cart['extra_ingrediants'] as $ing){
                                        $price=0;
                                        $calories=0;
                                        //Query to Get ing price with subcategory
                                      $ing_id = $ing['ing_id'];
                                      $subcat_id = $ing['subcat_id'];
                                      $sql = "select sing_price,sing_calories from sm_ingredients_subcats where ing_id = $ing_id AND subcat_id = $subcat_id";
                                      $query = $this->db->query($sql);
                                      if($query->num_rows() > 0){
                                          $rs = $query->row();
                                          $price = $rs->sing_price;
                                          $calories = $rs->sing_calories;
                                      }
                                       $extra_ingrediant_price=$extra_ingrediant_price+$price;
                                        $extra_ingrediant_calories=$extra_ingrediant_calories+$calories;
                                    }
                                }

                                $cart['total_price']=($main_ingrediant_price+$extra_ingrediant_price)*$cart['quantity'];

                                $cart['total_calories']=($main_ingrediant_calories + $extra_ingrediant_calories) * $cart['quantity'];

                                $data=array(
                                    'order_id'=>$insertId,
                                    'salad_id'=>$cart['salad_id'],
                                    'salad_name'=>$cart['salad_name'],
                                    'cat_name'=>getSingleFieldDetail('cat_id',$cart['cat_id'],'cat_name','sm_categories'),
                                    'cat_name_arabic'=>getSingleFieldDetail('cat_id',$cart['cat_id'],'cat_name_arabic','sm_categories'),
                                    'salad_name_arabic'=>$cart['salad_name_arabic'],
                                    'salad_img'=>$cart['salad_img'],
                                    'quantity'=>$cart['quantity'],
                                    'dressing_mixed_in'=>0,
                                    'item_total_price'=>$cart['total_price'],
                                    'item_total_calories'=>$cart['total_calories'],
                                    'salad_type'=>1
                                );

                                $this->db->insert('sm_order_details',$data);
                                //order detail id
                                $order_detail_id = $this->db->insert_id();

                                if($order_detail_id){
                                    if(count($cart['extra_ingrediants']) > 0) {
                                        foreach ($cart['extra_ingrediants'] as $ing) {
                                            //Query to Get ing price with subcategory
                                            $ing_id = $ing['ing_id'];
                                            $subcat_id = $ing['subcat_id'];
                                            $sql = "select sing_price,sing_calories from sm_ingredients_subcats where ing_id = $ing_id AND subcat_id = $subcat_id";
                                            $query = $this->db->query($sql);
                                            if($query->num_rows() > 0){
                                                $rs = $query->row();
                                                $price = $rs->sing_price;
                                                $calories = $rs->sing_calories;
                                            }
                                            $data=array(
                                                'odetail_id'=>$order_detail_id,
                                                'ing_type'=>'extra',
                                                'ing_id'=>$ing['ing_id'],
                                                'ing_name'=>$ing['ing_name'],
                                                'ing_name_arabic'=>$ing['ing_name_arabic'],
                                                'ing_price'=>$price,
                                                'ing_calories'=>$calories,
                                                'quantity'=>1,
                                            );
                                            $this->db->insert('sm_order_ingredients',$data);
                                        }
                                    }
                                }
                            }
                            elseif ($cart['salad_type'] == 2){
                                $where=array(
                                    'cart_id'=>$cart['cart_id']
                                );
                                $this->db->select('');
                                $this->db->from('sm_cart_ingredients as ci');
                                $this->db->join('sm_ingredients as i','i.ing_id=ci.ing_id');
                                $this->db->where($where);
                                $this->db->where('i.ing_status',1);
                                $query=$this->db->get();
                                $extra_ingrediants=$query->result_array();
                                $cart['extra_ingrediants']=$extra_ingrediants;
                                $main_ingrediant_price=0;
                                $main_ingrediant_calories=0;
                                if(count($cart['extra_ingrediants']) > 0){
                                    foreach ($cart['extra_ingrediants'] as $ing){
                                        $price=0; $calories=0;
                                        $ing_id = $ing['ing_id'];
                                            $subcat_id = $ing['subcat_id'];
                                            $sql = "select sing_price,sing_calories from sm_ingredients_subcats where ing_id = $ing_id AND subcat_id = $subcat_id";
                                            $query = $this->db->query($sql);
                                            if($query->num_rows() > 0){
                                                $rs = $query->row();
                                                $price = $rs->sing_price;
                                                $calories = $rs->sing_calories;
                                            }
                                        $main_ingrediant_price=$main_ingrediant_price+$price;
                                        $main_ingrediant_calories=$main_ingrediant_calories+$calories;
                                    }
                                }
                                $cart['total_price']=$main_ingrediant_price*$cart['quantity'];


                                $cart['total_calories']=$main_ingrediant_calories * $cart['quantity'];

                                $data=array(
                                    'order_id'=>$insertId,
                                    'salad_id'=>$cart['salad_id'],
                                    'salad_name'=>$cart['cust_salad_name'],
                                    'cat_name'=>getSingleFieldDetail('cat_id',$cart['cust_cat_id'],'cat_name','sm_categories'),
                                    'cat_name_arabic'=>getSingleFieldDetail('cat_id',$cart['cust_cat_id'],'cat_name_arabic','sm_categories'),
                                    'salad_name_arabic'=>$cart['cust_salad_name'],
                                    'salad_img'=>$cart['salad_img'],
                                    'quantity'=>$cart['quantity'],
                                    'dressing_mixed_in'=>0,
                                    'item_total_price'=>$cart['total_price'],
                                    'item_total_calories'=>$cart['total_calories'],
                                    'salad_type'=>1
                                );

                                $this->db->insert('sm_order_details',$data);

                                $order_detail_id = $this->db->insert_id();

                                if($order_detail_id){
                                    if(count($cart['extra_ingrediants']) > 0) {
                                        foreach ($cart['extra_ingrediants'] as $ing) {
                                               //Query to Get ing price with subcategory
                                            $ing_id = $ing['ing_id'];
                                            $subcat_id = $ing['subcat_id'];
                                            $sql = "select sing_price,sing_calories from sm_ingredients_subcats where ing_id = $ing_id AND subcat_id = $subcat_id";
                                            $query = $this->db->query($sql);
                                            if($query->num_rows() > 0){
                                                $rs = $query->row();
                                                $price = $rs->sing_price;
                                                 $calories = $rs->sing_calories;
                                            }
                                            $data=array(
                                                'odetail_id'=>$order_detail_id,
                                                'ing_type'=>'extra',
                                                'ing_id'=>$ing['ing_id'],
                                                'ing_name'=>$ing['ing_name'],
                                                'ing_name_arabic'=>$ing['ing_name_arabic'],
                                                'ing_price'=>$price,
                                                'ing_calories'=>$calories,
                                                 'quantity'=>1,
                                            );
                                            $this->db->insert('sm_order_ingredients',$data);
                                        }
                                    }

                                }

                            }

                            $cart_array[]=$cart;
                        }
                    }
                }
                if($status){
                   
                    //$this->session->unset_userdata('checkout_session');
                    if($_POST['order_type']==='online'){
                        //make session for order for payment
                        $this->session->set_userdata('order_id',$insertId);
                        $email=false;

                        //update order status
                        $table_name='sm_orders';
                        $update_data=array(
                            'o_status'=>0,
                        );
                        $where_condition=array(
                            'order_id'=>$insertId
                        );
                        $this->General_model->update_details($table_name, $update_data, $where_condition);

                        $response=array(
                            'status'=>true,
                            'order_type'=>$_POST['order_type'],
                        );
                        
                    }
                    elseif ($_POST['order_type']==='COD'){

                        //get store pickup value
                        $this->db->select('');
                        $this->db->from('sm_orders');
                        $this->db->where('order_id',$insertId);
                        $query=$this->db->get();
                        $order_data=$query->row();
                        if($order_data){
                            //CAll the customer address, in case of delivery order
                            if($order_data->in_store_pickup == 0)
                            {
                                $table_name = 'sm_addresses';
                                $select_filds='';
                                $where_condition=array(
                                    'add_id'=>$order_data->address_id
                                );
                                $address=$this->General_model->get_row($table_name,$select_filds,$where_condition);

                                $reciver_location=array(
                                    'latitude'=> floatval($address->latitude),
                                    'longitude'=> floatval($address->longitude)
                                );

                                $payment_method="";
                                if($order_data->payment_method =='COD'){
                                    $payment_method="Cash";
                                }elseif ($order_data->payment_method =='online'){
                                    $payment_method="Online";
                                }

                                //Get app setting for the Restaurant info
                                $sender_location = array();
                                $sender_name = '';
                                $sender_phone = '';
                                
                                $settings = getAppSettings();
                                if($settings){
                                    //Restaurant Location
                                    $sender_location=array(
                                        'latitude'=> floatval($settings->latitude),
                                        'longitude'=> floatval($settings->longitude)
                                    );
                                    $sender_name = $settings->app_name;
                                    $sender_phone = $settings->contact_phone;
                                }

                                //Crate data to send to Delivery API
                                $data_array =  array(
                                    "username"=>"saladmania",
                                    /*"password"=>"salad@mania321", //staging
                                    "price_id"=>"FBiq4KMNEY",*/
                                    "password"=>"saladmania4321", //live
                                    "price_id"=>"dX1Bj42hvX",
                                    "buyername"=>getSingleFieldDetail('id',$order_data->user_id,'full_name','sm_users'),
                                    "order_price"=> (int)$order_data->grand_total,
                                    "receiver_phone"=>getSingleFieldDetail('id',$order_data->user_id,'phone','sm_users'),
                                    "receiver_name"=>getSingleFieldDetail('id',$order_data->user_id,'full_name','sm_users'),
                                    "receiver_location"=>$reciver_location,
                                    "receiver_image"=>"https://s3.amazonaws.com/utrac-parser-server/823265829cc7c7939be1e6336fd84bdf_if_profle_1055000.png",
                                    "sender_name"=>$sender_name,
                                    "sender_phone"=>$sender_phone,
                                    "sender_location"=>$sender_location,
                                    "sender_image"=>base_url().'assets/frontend/img/logo_h.png',
                                    "integrator_number"=>encryptId($order_data->order_id),
                                    "receiver_city"=>"amman",
                                    "receiver_area"=>$address->addline1,
                                    "receiver_street"=>"",
                                    "receiver_building"=>"",
                                    "receiver_floor"=>"",
                                    "receiver_apartment"=>"126",
                                    "receiver_landmark"=>"khbp",
                                    "receiver_note"=>"test",
                                    "is_demo"=>false, //false on live orders
                                    "payment_method"=>$payment_method,
                                    "note"=>"testing",
                                    "change"=>50
                                );

                                //staging.utracadmin.net = staging server
                                $make_call = $this->callAPI('POST', 'https://web.utracadmin.net/parse/functions/addIntegratorOrder', json_encode($data_array));
                                $response = json_decode($make_call, true);
                               
                                //print_r($response);

                                if(isset($response['result']) && $response['result']['orderId']){
                                    $update_status = true;
                                    $record_to_update = array(
                                        'o_status'=>1,
                                        'delivery_id'=>$response['result']['orderId'],
                                        'delivery_status'=>1,
                                    );

                                    /*$wheres = array('order_id'=>$id);
                                    $data = $this->Main_model->__callUpdate('sm_orders',$wheres,$record_to_update);
                                    $this->session->set_flashdata('success_msg', 'Record is updated successfully');*/


                                    //Send email to user
                                    /*$this->db->select('');
                                    $this->db->from('sm_orders');
                                    $this->db->where('order_id',$id);
                                    $query=$this->db->get();
                                    $message['order'] = $query->row();

                                    $msg = $this->load->view('email/order_template', $message, TRUE);
                                    $userEmail = getSingleFieldDetail('id',$order_data->user_id,'email','sm_users');
                                    sendmail($userEmail,'','',$msg, 'Salad Mania - Order Status');*/
                                }
                                else{
                                    //Deliery API is not working
                                    $this->session->set_flashdata('error_msg', 'Something went wrong, Please try again later.');
                                }
                            }
                            else{
                                $update_status = true;
                                $record_to_update = array(
                                    'o_status'=>1
                                );
                            }

                            //Now update the order
                            if(isset($update_status)){
                                $response=array(
                                    'status'=>true,
                                    'msg'=>$this->lang->line('order_success_msg'),
                                );
                                //Update the order status
                                $wheres = array('order_id' => $insertId);
                                $data = $this->Main_model->__callUpdate('sm_orders',$wheres,$record_to_update);
                                $this->session->set_flashdata('success_msg', 'Record is updated successfully');

                                //Send email & SMS to user
                                $this->db->select('o.*, u.phone');
                                $this->db->from('sm_orders as o');
                                $this->db->join('sm_users as u', 'u.id = o.user_id', 'LEFT');
                                $this->db->where('order_id',$insertId);
                                $query=$this->db->get();
                                $message['order'] = $order = $query->row();
                                //echo "<pre>"; print_r($message['order']);exit;
                                if(!empty($order))
                                {
                                    $email=true;

                                }
                            }
                        }
//                        //update order status
//                        $table_name='sm_orders';
//                        $update_data=array(
//                            'o_status'=>1,
//                        );
//                        $where_condition=array(
//                            'order_id'=>$insertId
//                        );
//                        $this->General_model->update_details($table_name, $update_data, $where_condition);



                    }
                }else{
                    $response=array(
                        'status'=>false,
                        'msg'=>$this->lang->line('order_error_msg'),
                    );
                }
            }
            //If guest user
            elseif ($this->session->userdata('guest_user_session')){

                $login=$this->session->userdata('guest_user_session');

                //Get the order pricing form User order statics 
                $user_id = $login['id'];
                $option = array('where' => array('user_id' => $user_id));
                $exist = $this->Main_model->__callSelect('sm_user_order_statics',$option);
                    
                if($exist->num_rows() > 0)
                {
                    $row = $exist->row();
                    $sub_total = $row->sub_total;
                    $tax_charges = $row->tax_charges;
                    $delivery_charges = $row->delivery_charges;
                    $total_price = $row->total_price;
                }

                $checkout_session=$this->session->userdata('checkout_session');

                if($checkout_session['deliver_type'] === 'delivery'){
                    $data=array(
                        'user_id'=>$login['id'],
                        'total_price'=>$sub_total,
                        'discount_amt'=>0,
                        'tax_amt'=>$tax_charges,
                        'delivery_charge'=>$delivery_charges,
                        'grand_total'=>$total_price,
                        'payment_status'=>0,
                        'address_id'=>$this->session->userdata('confirm_address'),
                        'payment_method'=>$_POST['order_type'],
                        'in_store_pickup'=>0,
                        'customer_name'=>getSingleFieldDetail('id',$login['id'],'full_name','sm_users'),
                        'create_date'=>date("Y-m-d"),
                        
                    );
                }elseif ($checkout_session['deliver_type'] === 'pickup'){
                    $data=array(
                        'user_id'=>$login['id'],
                        'total_price'=>$sub_total,
                        'discount_amt'=>0,
                        'tax_amt'=>$tax_charges,
                        'delivery_charge'=>$delivery_charges,
                        'grand_total'=>$total_price,
                        'payment_status'=>0,
                        'payment_method'=>$_POST['order_type'],
                        'in_store_pickup'=>1,
                        'customer_name'=>$checkout_session['name'],
                        'customer_phone'=>$checkout_session['mobile'],
                        'pickup_date'=>$checkout_session['date'],
                        'pickup_time'=>$checkout_session['time'],
                        'create_date'=>date("Y-m-d"),
                         'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    );
                }

                $status=$this->db->insert('sm_orders',$data);
                //order id
                $insertId = $this->db->insert_id();

                if($insertId) {
                    
                    //Delete this order pricing form statics table form this user
                    $where = array("user_id" => $login['id']);
                    $this->Main_model->__callDelete('sm_user_order_statics' ,$where);

                    $order_id=strtoupper(encryptId($insertId));
                    $not_data=array(
                        'noti_msg'=>'You have a new order: SM-'.$order_id,
                        'module_id'=>$insertId,
                        'noti_for'=>'A',
                    );
                    $this->db->insert('sm_notifications',$not_data);

                    $session_data = $this->session->userdata('cart_data');
                    if($session_data){
                        foreach ($session_data as $cart){
                            if($cart['salad_type'] ==1){
                                $where=array(
                                    'si.salad_id'=>$cart['salad_id']
                                );
                                $this->db->select('');
                                $this->db->group_by('subcat_id');
                                $this->db->from('sm_salad_ingredients as si');
                                $this->db->join('sm_ingredients as i','i.ing_id=si.ing_id');
                                $this->db->where($where);
                                $this->db->where('i.ing_status',1);
                                $query=$this->db->get();
                                $main_ingrediants=$query->result_array();
                                $cart['main_ingrediants']=$main_ingrediants;
                                $main_ingrediant_price=0;
                                $main_ingrediant_calories=0;
                                if(count($cart['main_ingrediants']) > 0){
                                    foreach ($cart['main_ingrediants'] as $ing){
                                        $price=0;
                                        $price=$ing['ing_price'] * $ing['quantity'];
                                        $main_ingrediant_price=$main_ingrediant_price+$price;

                                        $calories=0;
                                        $calories=$ing['ing_calories'] * $ing['quantity'];
                                        $main_ingrediant_calories=$main_ingrediant_calories+$calories;
                                    }
                                }

                                $extra_ingrediant_price=0;
                                $extra_ingrediant_calories=0;
                                if(count($cart['extra_ingrediants']) > 0){
                                    foreach ($cart['extra_ingrediants'] as $ing){
                                        $price=0;
                                        $price=getSingleFieldDetail('ing_id',$ing['ing_id'],'ing_price','sm_ingredients') * $ing['quantity'];
                                        $extra_ingrediant_price=$extra_ingrediant_price+$price;

                                        $calories=0;
                                        $calories=getSingleFieldDetail('ing_id',$ing['ing_id'],'ing_calories','sm_ingredients') * $ing['quantity'];
                                        $extra_ingrediant_calories=$extra_ingrediant_calories+$calories;
                                    }
                                }

                                $cart['total_price']=($main_ingrediant_price+$extra_ingrediant_price)*$cart['quantity'];

                                $cart['total_calories']=($main_ingrediant_calories + $extra_ingrediant_calories) * $cart['quantity'];

                                $data=array(
                                    'order_id'=>$insertId,
                                    'salad_id'=>$cart['salad_id'],
                                    'salad_name'=>getSingleFieldDetail('salad_id',$cart['salad_id'],'salad_name','sm_salads'),
                                    'cat_name'=>getSingleFieldDetail('cat_id',$cart['cat_id'],'cat_name','sm_categories'),
                                    'cat_name_arabic'=>getSingleFieldDetail('cat_id',$cart['cat_id'],'cat_name_arabic','sm_categories'),
                                    'salad_name_arabic'=>getSingleFieldDetail('salad_id',$cart['salad_id'],'salad_name_arabic','sm_salads'),
                                    'salad_img'=>getSingleFieldDetail('salad_id',$cart['salad_id'],'salad_img','sm_salads'),
                                    'quantity'=>$cart['quantity'],
                                    'dressing_mixed_in'=>0,
                                    'item_total_price'=>$cart['total_price'],
                                    'item_total_calories'=>$cart['total_calories'],
                                    'salad_type'=>1
                                );

                                $this->db->insert('sm_order_details',$data);
                                //order detail id
                                $order_detail_id = $this->db->insert_id();

                                if($order_detail_id){
                                    if(count($cart['extra_ingrediants']) > 0) {
                                        foreach ($cart['extra_ingrediants'] as $ing) {
                                            //Query to Get ing price with subcategory
                                            $ing_id = $ing['ing_id'];
                                            $subcat_id = $ing['subcat_id'];
                                            $sql = "select sing_price,sing_calories from sm_ingredients_subcats where ing_id = $ing_id AND subcat_id = $subcat_id";
                                            $query = $this->db->query($sql);
                                            if($query->num_rows() > 0){
                                                $rs = $query->row();
                                                $price = $rs->sing_price;
                                                $calories = $rs->sing_calories;
                                            }
                                            $data=array(
                                                'odetail_id'=>$order_detail_id,
                                                'ing_type'=>'extra',
                                                'ing_id'=>$ing['ing_id'],
                                                'ing_name'=>getSingleFieldDetail('ing_id',$ing['ing_id'],'ing_name','sm_ingredients'),
                                                'ing_name_arabic'=>getSingleFieldDetail('ing_id',$ing['ing_id'],'ing_name_arabic','sm_ingredients'),
                                                'ing_price'=>$price,
                                                'ing_calories'=>$calories,
                                                'quantity'=>1
                                            );
                                            $this->db->insert('sm_order_ingredients',$data);
                                        }
                                    }
                                }




                            }elseif ($cart['salad_type'] == 2){
                                $main_ingrediant_price=0;
                                $main_ingrediant_calories=0;

                                if(count($cart['extra_ingrediants']) > 0){
                                    foreach ($cart['extra_ingrediants'] as $ing){
                                        $price=0;
                                        $price=getSingleFieldDetail('ing_id',$ing['ing_id'],'ing_price','sm_ingredients') * $ing['quantity'];
                                        $main_ingrediant_price=$main_ingrediant_price+$price;

                                        $calories=0;
                                        $calories=getSingleFieldDetail('ing_id',$ing['ing_id'],'ing_calories','sm_ingredients') * $ing['quantity'];
                                        $main_ingrediant_calories=$main_ingrediant_calories+$calories;
                                    }
                                }
                                
                                
                                $cart['total_price']=$main_ingrediant_price*$cart['quantity'];


                                $cart['total_calories']=$main_ingrediant_calories * $cart['quantity'];

                                $data=array(
                                    'order_id'=>$insertId,
                                    'salad_id'=>$cart['salad_id'],
                                    'salad_name'=>$cart['salad_name'],
                                    'cat_name'=>getSingleFieldDetail('cat_id',$cart['cat_id'],'cat_name','sm_categories'),
                                    'cat_name_arabic'=>getSingleFieldDetail('cat_id',$cart['cat_id'],'cat_name_arabic','sm_categories'),
                                    'salad_name_arabic'=>$cart['salad_name'],
                                    'salad_img'=>'custom.png',
                                    'quantity'=>$cart['quantity'],
                                    'dressing_mixed_in'=>0,
                                    'item_total_price'=>$cart['total_price'],
                                    'item_total_calories'=>$cart['total_calories'],
                                    'salad_type'=>1
                                );

                                $this->db->insert('sm_order_details',$data);

                                $order_detail_id = $this->db->insert_id();

                                if($order_detail_id){
                                    if(count($cart['extra_ingrediants']) > 0) {
                                        foreach ($cart['extra_ingrediants'] as $ing) {
                                            //Query to Get ing price with subcategory
                                            $ing_id = $ing['ing_id'];
                                            $subcat_id = $ing['subcat_id'];
                                            $sql = "select sing_price,sing_calories from sm_ingredients_subcats where ing_id = $ing_id AND subcat_id = $subcat_id";
                                            $query = $this->db->query($sql);
                                            if($query->num_rows() > 0){
                                                $rs = $query->row();
                                                $price = $rs->sing_price;
                                                $calories = $rs->sing_calories;
                                            }
                                            $data=array(
                                                'odetail_id'=>$order_detail_id,
                                                'ing_type'=>'extra',
                                                'ing_id'=>$ing['ing_id'],
                                                'ing_name'=>$ing['ing_name'],
                                                'ing_name_arabic'=>$ing['ing_name_arabic'],
                                                'ing_price'=>$price,
                                                'ing_calories'=>$calories,
                                                'quantity'=>1
                                            );
                                            $this->db->insert('sm_order_ingredients',$data);
                                        }
                                    }

                                }

                            }

                            $cart_array[]=$cart;
                        }
                    }

                }
                if($status){

                    if($_POST['order_type']==='online'){
                        $email=false;
                        //make session for order for payment
                        $this->session->set_userdata('order_id',$insertId);

                        //update order status
                        $table_name='sm_orders';
                        $update_data=array(
                            'o_status'=>0,
                        );
                        $where_condition=array(
                            'order_id'=>$insertId
                        );
                        $this->General_model->update_details($table_name, $update_data, $where_condition);

                        $response=array(
                            'status'=>true,
                            'order_type'=>$_POST['order_type'],
                        );
                    }elseif ($_POST['order_type']==='COD'){

                        //get store pickup value
                        $this->db->select('');
                        $this->db->from('sm_orders');
                        $this->db->where('order_id',$insertId);
                        $query=$this->db->get();
                        $order_data=$query->row();
                        if($order_data){
                                //CAll the customer address, in case of delivery order
                                if($order_data->in_store_pickup == 0)
                                {
                                    $table_name = 'sm_addresses';
                                    $select_filds='';
                                    $where_condition=array(
                                        'add_id'=>$order_data->address_id
                                    );
                                    $address=$this->General_model->get_row($table_name,$select_filds,$where_condition);

                                    $reciver_location=array(
                                        'latitude'=> floatval($address->latitude),
                                        'longitude'=> floatval($address->longitude)
                                    );

                                    $payment_method="";
                                    if($order_data->payment_method =='COD'){
                                        $payment_method="Cash";
                                    }elseif ($order_data->payment_method =='online'){
                                        $payment_method="Online";
                                    }

                                    //Get app setting for the Restaurant info
                                    $sender_location = array();
                                    $sender_name = '';
                                    $sender_phone = '';
                                    
                                    $settings = getAppSettings();
                                    if($settings){
                                        //Restaurant Location
                                        $sender_location=array(
                                            'latitude'=> floatval($settings->latitude),
                                            'longitude'=> floatval($settings->longitude)
                                        );
                                        $sender_name = $settings->app_name;
                                        $sender_phone = $settings->contact_phone;
                                    }

                                    //Crate data to send to Delivery API
                                    $data_array =  array(
                                        "username"=>"saladmania",
                                        /*"password"=>"salad@mania321", //staging
                                        "price_id"=>"FBiq4KMNEY",*/
                                        "password"=>"saladmania4321", //live
                                        "price_id"=>"dX1Bj42hvX",
                                        "buyername"=>getSingleFieldDetail('id',$order_data->user_id,'full_name','sm_users'),
                                        "order_price"=> (int)$order_data->grand_total,
                                        "receiver_phone"=>getSingleFieldDetail('id',$order_data->user_id,'phone','sm_users'),
                                        "receiver_name"=>getSingleFieldDetail('id',$order_data->user_id,'full_name','sm_users'),
                                        "receiver_location"=>$reciver_location,
                                        "receiver_image"=>"https://s3.amazonaws.com/utrac-parser-server/823265829cc7c7939be1e6336fd84bdf_if_profle_1055000.png",
                                        "sender_name"=>$sender_name,
                                        "sender_phone"=>$sender_phone,
                                        "sender_location"=>$sender_location,
                                        "sender_image"=>base_url().'assets/frontend/img/logo_h.png',
                                        "integrator_number"=>encryptId($order_data->order_id),
                                        "receiver_city"=>"amman",
                                        "receiver_area"=>$address->addline1,
                                        "receiver_street"=>"",
                                        "receiver_building"=>"",
                                        "receiver_floor"=>"",
                                        "receiver_apartment"=>"126",
                                        "receiver_landmark"=>"khbp",
                                        "receiver_note"=>"test",
                                        "is_demo"=>false, //false on live orders
                                        "payment_method"=>$payment_method,
                                        "note"=>"testing",
                                        "change"=>50
                                    );

                                    $make_call = $this->callAPI('POST', 'https://web.utracadmin.net/parse/functions/addIntegratorOrder', json_encode($data_array));
                                    $response = json_decode($make_call, true);
                                    //echo "<pre>"; print_r($response);exit;

                                    if($response['result']['orderId']){
                                        $update_status = true;
                                        $record_to_update = array(
                                            'o_status'=>1,
                                            'delivery_id'=>$response['result']['orderId'],
                                            'delivery_status'=>1,
                                        );
                                        /*$wheres = array('order_id'=>$id);
                                        $data = $this->Main_model->__callUpdate('sm_orders',$wheres,$record_to_update);
                                        $this->session->set_flashdata('success_msg', 'Record is updated successfully');*/


                                        //Send email to user
                                        /*$this->db->select('');
                                        $this->db->from('sm_orders');
                                        $this->db->where('order_id',$id);
                                        $query=$this->db->get();
                                        $message['order'] = $query->row();

                                        $msg = $this->load->view('email/order_template', $message, TRUE);
                                        $userEmail = getSingleFieldDetail('id',$order_data->user_id,'email','sm_users');
                                        sendmail($userEmail,'','',$msg, 'Salad Mania - Order Status');*/
                                    }
                                    else{
                                        $this->session->set_flashdata('error_msg', 'Deliery API is not working. Please try again later.');
                                    }
                                }
                                else{
                                    $update_status = true;
                                    $record_to_update = array(
                                        'o_status'=>1
                                    );
                                }

                                //Now update the order
                                if(isset($update_status)){
                                    //Update the order status
                                    $wheres = array('order_id' => $insertId);
                                    $data = $this->Main_model->__callUpdate('sm_orders',$wheres,$record_to_update);
                                    $this->session->set_flashdata('success_msg', 'Record is updated successfully');

                                    //Send email & SMS to user
                                    $this->db->select('o.*, u.phone');
                                    $this->db->from('sm_orders as o');
                                    $this->db->join('sm_users as u', 'u.id = o.user_id', 'LEFT');
                                    $this->db->where('order_id',$insertId);
                                    $query=$this->db->get();
                                    $message['order'] = $order = $query->row();
                                    //echo "<pre>"; print_r($message['order']);exit;
                                    if(!empty($order))
                                    {
                                        $email=true;
                                    }
                                }
                        }

                        $response=array(
                            'status'=>true,
                            'msg'=>$this->lang->line('order_success_msg'),
                        );
                    }
                }else{
                    $response=array(
                        'status'=>false,
                        'msg'=>$this->lang->line('order_error_msg'),
                    );
                }

            }else{
                $response=array(
                    'status'=>false,
                    'msg'=>$this->lang->line('order_error_msg'),
                );
            }
        }
        else{
            $response=array(
                'status'=>false,
                'msg'=>$this->lang->line('order_error_msg'),
            );
        }
        if($email ==true){
            //Send email to user
            $this->db->select('');
            $this->db->from('sm_orders');
            $this->db->where('order_id',$insertId);
            $query=$this->db->get();
            $message['order'] = $query->row();
            
            //send mail
            $msg = $this->load->view('email/order_template', $message, TRUE);
            $userEmail = getSingleFieldDetail('id',$login['id'],'email','sm_users');
            $mail=sendmail($userEmail,'','',$msg, 'Salad Mania - Order Placed');
            if($mail){
                if($this->session->userdata('user_login_session')){
                    $login=$this->session->userdata('user_login_session');

                }elseif ($this->session->userdata('guest_user_session')){
                    $login=$this->session->userdata('guest_user_session');
                }

                if ($this->session->userdata('guest_user_session')){
                    $this->session->unset_userdata('guest_user_session');
                    $this->session->unset_userdata('user_login_session');
                    $this->session->unset_userdata('cart_session');
                    $this->session->unset_userdata('checkout_session');
                    $this->session->unset_userdata('set_userdata');
                    $this->session->sess_destroy();
                }
                if($this->session->userdata('user_login_session')){
                    $login= $this->session->userdata('user_login_session');
                    $where=array(
                        'user_id'=>$login['id']
                    );
                    $this->db->where($where);
                    $this->db->delete('sm_carts');
                }
            }
            $orderNo= strtoupper(encryptId($insertId));
                 //Send SMS Notification
                if($checkout_session['deliver_type'] === 'delivery'){
                    $msg = "Proudly , we received your order , estimated time for delivery is 45min max. Your order number : $orderNo.";
                }elseif ($checkout_session['deliver_type'] === 'pickup'){
                    $msg = "Proudly  , we received your order, your order will be ready in 10 min.Your order number : $orderNo.";
                }
            $userPhone = getSingleFieldDetail('id',$login['id'],'phone','sm_users');
            $msg_id=SendSms($msg,$userPhone);

        }

        echo json_encode($response);
    }

    public function DeleteCartFromSession(){
        $filter = $this->session->userdata('cart_session');
        unset($filter[$_POST['key']]);
        $this->session->set_userdata('cart_session', $filter);
        $response=array(
            'status'=>true
        );
        echo json_encode($response);
    }

    public function remove_cart_values(){
        if($this->session->userdata('user_login_session')){
            $where=array(
                'cart_id'=>$_POST['cart_id']
            );
            $status=$this->db->delete('sm_carts',$where);
        }
        elseif($this->session->userdata('cart_data')){
            $data=$this->session->userdata('cart_data');
            //   echo '<pre>';print_r($data);exit;
            unset($data[$_POST['cart_id']]);
           
            $this->session->set_userdata('cart_data',$data);
            // $data1=$this->session->userdata('cart_data');
            // echo '<pre>';print_r($data1);

            $status=true;
        }else{
            $status=false;
        }

        if($status){
            $response=array(
                'status'=>true
            );
        }else{
            $response=array(
                'status'=>false
            );
        }
        echo json_encode($response);
    }

    public function update_quantity(){
        if($this->session->userdata('user_login_session')){
            $where=array(
                'cart_id'=>$_POST['cart_id']
            );
            $data=array(
                'quantity'=>$_POST['quantity']
            );
            $this->db->where($where);
            $status=$this->db->update('sm_carts',$data);
        }
        elseif($this->session->userdata('cart_data')){
            $data=$this->session->userdata('cart_data');

            $data[$_POST['cart_id']]['quantity']=$_POST['quantity'];
            $this->session->set_userdata('cart_data',$data);
            $status=true;
        }else{
            $status=false;
        }


        if($status){
            $response=array(
                'status'=>true
            );
        }else{
            $response=array(
                'status'=>false
            );
        }
        echo json_encode($response);
    }

    public function get_address(){
        $login=false;
        if($this->session->userdata('user_login_session')){
            $login=$this->session->userdata('user_login_session');

        }elseif ($this->session->userdata('guest_user_session')){
            $login=$this->session->userdata('guest_user_session');
        }

        if($login){
            $where=array(
                'user_id'=>$login['id'],
                'add_type'=>'delivery'
            );

            $this->db->select('');
            $this->db->where($where);
            $this->db->from('sm_addresses');
            $query=$this->db->get();
            $address=$query->result();
            $loadHtml='';
            if($address){
                $loadHtml.='<h4 class="op-section-title" style="margin-bottom: 20px;">Select Delivery Address</h4>';
                foreach ($address as $add){
                    $checked='';
                    if($this->session->userdata('confirm_address')){
                        if($this->session->userdata('confirm_address') == $add->add_id){
                            $checked='checked';
                        }else{
                            $checked='';
                        }
                    }
                    $loadHtml.='<div class="row ">';
                        $loadHtml.=' <div class="col-lg-12">';
                            $loadHtml.='<label class="container2">';
                                $loadHtml.=' <p class="show-address">'.$add->addline1.' , '.$add->addline2.' <br> Amman </p>';
                                $loadHtml.='<input type="radio" '.$checked.' value="'.$add->add_id.'" name="address_id">';
                                $loadHtml.=' <span class="checkmark2"></span>';
                            $loadHtml.='</label>';
                        $loadHtml.='</div>';
                    $loadHtml.='</div>';
                }
            }else{
                $loadHtml.='<h4 class="op-section-title" style="margin-bottom: 20px;">Please Add Address</h4>';
            }

            $response=array(
                'status'=>true,
                'data'=>$loadHtml,
                'address'=>$address,
            );

        }else{
            $response=array(
                'status'=>false
            );
        }

        echo json_encode($response);
    }

    public function confirm_address(){
        if(isset($_POST['address_id'])){
            $this->session->set_userdata('confirm_address',$_POST['address_id']);
            $response=array(
                'status'=>true
            );
        }else{
            $response=array(
                'status'=>false
            );
        }
        echo json_encode($response);
    }

     public function order_success(){
          $this->load->view('frontend/payment_success');
     }

      public function payment_type(){
       $session=  $this->session->set_userdata('payment_type',$_POST['payment_type']);
       if($this->session->userdata('payment_type')){
            $response=array(
                'status'=>true
            );
        }else{
            $response=array(
                'status'=>false
            );
        }
        echo json_encode($response);
     }

    private function callAPI($method, $url, $data){
        $curl = curl_init();

        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'X-Parse-Application-Id: 9803886e4a724d87bc56b44bd7512d3e2093bd95',
            'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        if(!$result){die("Connection Failure");}
        curl_close($curl);
        return $result;
    }

}