<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PaymentController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('General_model');
        $this->load->library('session');
        if (!($this->session->userdata('language'))) {
            $this->session->set_userdata('language', 'english');
        }
        $this->lang->load('content_form_' .$this->session->userdata('language'));

    }
    public function index(){
        // print_r($_POST);exit;
        if( $this->session->userdata('order_id')){

            if($this->session->userdata('guest_user_session')){
                $login=$this->session->userdata('guest_user_session');
                $user_id=$login['id'];
            }elseif ($this->session->userdata('user_login_session')){
                $login=$this->session->userdata('user_login_session');
                $user_id=$login['id'];
            }else{
                redirect(base_url());
            }

            //getting user iformation from user table
            $table_name='sm_users';
            $select_filds=' ';

            $where_condition=array(
                'id'=>$user_id
            );
            $user=$this->General_model->get_row($table_name, $select_filds, $where_condition);

            $table_name='sm_orders';
            $select_filds='';
            $where_condition=array(
                'order_id'=>$this->session->userdata('order_id')
            );

            $order_detail=$this->General_model->get_row($table_name, $select_filds, $where_condition);
            $address='Salad Mania office, Jordan, Amman';

            if($user_id){

                if($this->session->userdata('user_login_session')){
                    $where=array(
                        'user_id'=>$user_id,
                        'add_type'=>'profile'
                    );
                    $order_by='add_id DESC';
                    $address=$this->General_model->get_row('sm_addresses','',$where,$order_by);
                    $address=$address->addline1.', '.$address->addline2.', Amman';
                }
                else if ($this->session->userdata('guest_user_session')){
                    if($order_detail->address_id){
                        $where=array(
                            'add_id'=>$order_detail->address_id,
                        );
                        $order_by='add_id DESC';
                        $address=$this->General_model->get_row('sm_addresses','',$where,$order_by);

                        $address=$address->addline1.', '.$address->addline2.', Amman';
                    }
                }
            }

            require_once 'paytabs.php';

            $pt = new Payment();
             $pt->paytabs("ahmad@saladmania.net", "j4Xx3kgBO3rRDggTSTcLe8SYHgkxxXaQ28o5sEGhI79mopfhjs2yAUwhbQPIOZzQ7XWACCSkK2G9rWqLosjZmQhyH6gv5QmdNiOp");
            //$pt->paytabs("priyanshu@coretechies.com", "4ytpC6ATFkhNKY7rMwLX99qDtbdFcpAgX3Gz9gtPBLCmJJNwgztqJU3c5gSiGszDvfdFwHu7s6tbQhI7RbyA6CNcTGuz84dEjrkP");    
            $name = explode(' ',$user->full_name);
            $firstname=$name[0];
            $lastname='sm';
            if(count($name)>1){
                $lastname=$name[1];
            }
            //$result = $pt->authentication();

            $result = $pt->create_pay_page(array(
                //Customer's Personal Information
                'merchant_email' => "ahmad@saladmania.net",
                'secret_key' => "j4Xx3kgBO3rRDggTSTcLe8SYHgkxxXaQ28o5sEGhI79mopfhjs2yAUwhbQPIOZzQ7XWACCSkK2G9rWqLosjZmQhyH6gv5QmdNiOp",
                // 'merchant_email' => "priyanshu@coretechies.com",
                // 'secret_key' => "4ytpC6ATFkhNKY7rMwLX99qDtbdFcpAgX3Gz9gtPBLCmJJNwgztqJU3c5gSiGszDvfdFwHu7s6tbQhI7RbyA6CNcTGuz84dEjrkP",
                'cc_first_name' =>  $firstname,          //This will be prefilled as Credit Card First Name
                'cc_last_name' => $lastname,            //This will be prefilled as Credit Card Last Name
                'cc_phone_number' => "00962",
                'phone_number' => $user->phone,
                'email' => $user->email,

                //Customer's Billing Address (All fields are mandatory)
                //When the country is selected as USA or CANADA, the state field should contain a String of 2 characters containing the ISO state code otherwise the payments may be rejected.
                //For other countries, the state can be a string of up to 32 characters.
                'billing_address' =>  $address,
                'city' => "Amman",
                'state' => "Amman",
                'postal_code' => "11118",
                'country' => "JOR",
                // 'postal_code' => "00973",
                // 'country' => "BHR",

                //Customer's Shipping Address (All fields are mandatory)
                'address_shipping' => $address,
                'city_shipping' => "Amman",
                'state_shipping' => "Amman",
                'postal_code_shipping' => "11118",
                'country_shipping' => "JOR",
                    // 'postal_code_shipping' => "00973",
                    // 'country_shipping' => "BHR",

                //Product Information
                "products_per_title" => "Salad",   //Product title of the product. If multiple products then add “||” separator
                'quantity' => "1",                                    //Quantity of products. If multiple products then add “||” separator
                'unit_price' =>  $order_detail->grand_total,                                  //Unit price of the product. If multiple products then add “||” separator.
                "other_charges" => "0",                                     //Additional charges. e.g.: shipping charges, taxes, VAT, etc.
                'amount' => $order_detail->grand_total,                                          //Amount of the products and other charges, it should be equal to: amount = (sum of all products’ (unit_price * quantity)) + other_charges
                'discount'=>"0",                                                //Discount of the transaction. The Total amount of the invoice will be= amount - discount
                'currency' => "JOD",   //JOD                                        //Currency of the amount stated. 3  //character ISO currency code

                //Invoice Information
                'title' => $user->full_name,          // Customer's Name on the invoice
                "msg_lang" => "en",                 //Language of the PayPage to be created. Invalid or blank entries will default to English.(Englsh/Arabic)
                "reference_no" => "1231231",        //Invoice reference number in your system

                //Website Information
                "site_url" => 'https://www.saladmania.net',      //The requesting website be exactly the same as the website/URL associated with your PayTabs Merchant Account
                'return_url' => base_url()."payment-success/",
                "cms_with_version" => "API USING PHP",
                "paypage_info" => "1"
            ));

            //echo "/FOLLOWING IS THE RESPONSE: <br />"; print_r ($result);exit;
            echo '<script type="text/javascript">window.location = "'.$result->payment_url.'"</script>';
            $_SESSION['paytabs_api_key'] = $result->secret_key;
        }
        else{
            redirect(base_url());
        }
    }

    public function success(){
        require_once 'paytabs.php';
        $pt = new Payment();
        $pt-> paytabs("ahmad@saladmania.net", "j4Xx3kgBO3rRDggTSTcLe8SYHgkxxXaQ28o5sEGhI79mopfhjs2yAUwhbQPIOZzQ7XWACCSkK2G9rWqLosjZmQhyH6gv5QmdNiOp");
        $order_id=$this->session->userdata('order_id');
        //$pt->paytabs("priyanshu@coretechies.com", "4ytpC6ATFkhNKY7rMwLX99qDtbdFcpAgX3Gz9gtPBLCmJJNwgztqJU3c5gSiGszDvfdFwHu7s6tbQhI7RbyA6CNcTGuz84dEjrkP");
        
        $result = $pt->verify_payment($_POST['payment_reference']);
        //echo "<pre>"; print_r($result);exit;

        //$result->response_code ==='100'(response code if payment is success )
        if($result->response_code ==='100'){
            $login=false;
            if($this->session->userdata('user_login_session')){
                $login=$this->session->userdata('user_login_session');

            }elseif ($this->session->userdata('guest_user_session')){
                $login=$this->session->userdata('guest_user_session');
            }

            $order_id=$this->session->userdata('order_id');

            //get detail from order details
            $this->db->select('');
            $this->db->from('sm_orders');
            $this->db->where('order_id',$order_id);
            $query=$this->db->get();
            $message['order'] = $query->row();
            
            if($message['order']){
                //CAll the customer address, in case of delivery order
                if($message['order']->in_store_pickup == 0)
                {
                    $table_name = 'sm_addresses';
                    $select_filds='';
                    $where_condition=array(
                        'add_id'=>$message['order']->address_id
                    );
                    $address=$this->General_model->get_row($table_name,$select_filds,$where_condition);

                    $reciver_location=array(
                        'latitude'=> floatval($address->latitude),
                        'longitude'=> floatval($address->longitude)
                    );

                    $payment_method="";
                    if($message['order']->payment_method =='COD'){
                        $payment_method="Cash";
                    }elseif ($message['order']->payment_method =='online'){
                        $payment_method="Online";
                    }

                    //Get app setting for the Restaurant info
                        $sender_location = array();
                        $sender_name = '';
                        $sender_phone = '';
                        
                        $settings = getAppSettings();
                        if($settings){
                            //Restaurant Location
                            $sender_location=array(
                                'latitude'=> floatval($settings->latitude),
                                'longitude'=> floatval($settings->longitude)
                            );
                            $sender_name = $settings->app_name;
                            $sender_phone = $settings->contact_phone;
                        }

                    //Crate data to send to Delivery API
                    $data_array =  array(
                        "username"=>"saladmania",
                        /*"password"=>"salad@mania321", //staging
                        "price_id"=>"FBiq4KMNEY",*/
                        "password"=>"saladmania4321", //live
                        "price_id"=>"dX1Bj42hvX",
                        "buyername"=>getSingleFieldDetail('id',$message['order']->user_id,'full_name','sm_users'),
                        "order_price"=> (int)$message['order']->grand_total,
                        "receiver_phone"=>getSingleFieldDetail('id',$message['order']->user_id,'phone','sm_users'),
                        "receiver_name"=>getSingleFieldDetail('id',$message['order']->user_id,'full_name','sm_users'),
                        "receiver_location"=>$reciver_location,
                        "receiver_image"=>"https://s3.amazonaws.com/utrac-parser-server/823265829cc7c7939be1e6336fd84bdf_if_profle_1055000.png",
                        "sender_name"=>$sender_name,
                        "sender_phone"=>$sender_phone,
                        "sender_location"=>$sender_location,
                        "sender_image"=>base_url().'assets/frontend/img/logo_h.png',
                        "integrator_number"=>encryptId($message['order']->order_id),
                        "receiver_city"=>"amman",
                        "receiver_area"=>$address->addline1,
                        "receiver_street"=>"",
                        "receiver_building"=>"",
                        "receiver_floor"=>"",
                        "receiver_apartment"=>"126",
                        "receiver_landmark"=>"khbp",
                        "receiver_note"=>"test",
                        "is_demo"=>false, //false on live orders
                        "payment_method"=>$payment_method,
                        "note"=>"testing",
                        "change"=>50
                    );

                    //staging.utracadmin.net = staging server & web = live
                    $make_call = $this->callAPI('POST', 'https://web.utracadmin.net/parse/functions/addIntegratorOrder', json_encode($data_array));
                    $response = json_decode($make_call, true);
                    //echo "<pre>"; print_r($response);exit;
                    //$response['code'] == '10' = already created
                    
                    /*if($response['code'] == 10){
                        redirect(base_url('payment-failed'));
                    }*/

                    if($response['result']['orderId']){
                        $update_status = true;
                        $record_to_update = array(
                            'o_status'=>1,
                            'delivery_id'=>$response['result']['orderId'],
                            'delivery_status'=>1,
                        );
                        /*$wheres = array('order_id'=>$id);
                        $data = $this->Main_model->__callUpdate('sm_orders',$wheres,$record_to_update);
                        $this->session->set_flashdata('success_msg', 'Record is updated successfully');*/


                        //Send email to user
                        /*$this->db->select('');
                        $this->db->from('sm_orders');
                        $this->db->where('order_id',$id);
                        $query=$this->db->get();
                        $message['order'] = $query->row();

                        $msg = $this->load->view('email/order_template', $message, TRUE);
                        $userEmail = getSingleFieldDetail('id',$order_data->user_id,'email','sm_users');
                        sendmail($userEmail,'','',$msg, 'Salad Mania - Order Status');*/
                    }
                    else{
                        $this->session->set_flashdata('error_msg', 'Deliery API is not working. Please try again later.');
                    }
                }
                else{
                    $update_status = true;
                    $record_to_update = array(
                        'o_status'=>0
                    );
                }

                //Now update the order
                if(isset($update_status)){
                    //Update the order status
                    $wheres = array('order_id' => $order_id);
                    $data = $this->Main_model->__callUpdate('sm_orders',$wheres,$record_to_update);
                    $this->session->set_flashdata('success_msg', 'Record is updated successfully');

                    //Send email & SMS to user
                    $this->db->select('o.*, u.phone');
                    $this->db->from('sm_orders as o');
                    $this->db->join('sm_users as u', 'u.id = o.user_id', 'LEFT');
                    $this->db->where('order_id',$order_id);
                    $query=$this->db->get();
                    $message['order'] = $order = $query->row();
                    //echo "<pre>"; print_r($message['order']);exit;
                    if(!empty($order))
                    {
                        //sending email
                        $msg = $this->load->view('email/order_template', $message, TRUE);
                        $userEmail = getSingleFieldDetail('id',$login['id'],'email','sm_users');
                        //send mail on payment success
                        sendmail($userEmail,'','',$msg, 'Salad Mania - Order Placed');

                        //Send SMS Notification
                        $orderNo = 'SM-'.strtoupper(encryptId($order_id));
                        $checkout_session = $this->session->userdata('checkout_session');

                        if($checkout_session['deliver_type'] === 'delivery'){
                            $msg = "Proudly, we received your order, estimated time for delivery is 45min max. Your order number: $orderNo";
                        }
                        elseif ($checkout_session['deliver_type'] === 'pickup'){
                            $msg = "Proudly, we received your order, your order will be ready in 10 min. Your order number: $orderNo";
                        }
                        $userPhone = getSingleFieldDetail('id',$login['id'],'phone','sm_users');
                        $msg_id = SendSms($msg,$userPhone);
                        
                        $table_name='sm_orders';
                        $update_data=array(
                            'payment_status'=>1,
                            'o_status'=>1,
                            'transaction_id'=>$result->transaction_id
                        );
                        $where_condition=array(
                            'order_id'=>$order_id
                        );
                        $this->General_model->update_details($table_name, $update_data, $where_condition);
                        $this->session->unset_userdata('order_id');

                        //$this->session->sess_destroy();
                        if ($this->session->userdata('guest_user_session')){
                            $this->session->unset_userdata('guest_user_session');
                            $this->session->unset_userdata('user_login_session');
                            $this->session->unset_userdata('cart_session');
                            $this->session->unset_userdata('checkout_session');
                            $this->session->unset_userdata('set_userdata');
                            $this->session->sess_destroy();
                        }
                        if($this->session->userdata('user_login_session')){
                            $login= $this->session->userdata('user_login_session');
                            $where=array(
                                'user_id'=>$login['id']
                            );
                            $this->db->where($where);
                            $this->db->delete('sm_carts');
                        }
                        $this->load->view('frontend/payment_success');
                    }
                }
            }
        }
        else if($result->response_code === '0'){
            $table_name='sm_orders';
            $update_data=array(
                'payment_status'=>2,
                'o_status'=>7,
            );
            $where_condition=array(
                'order_id'=>$order_id
            );
            $this->General_model->update_details($table_name, $update_data, $where_condition);
            redirect(base_url('make-your-own'));
        }
        else{
            $table_name='sm_orders';
            $update_data=array(
                'payment_status'=>2,
                'o_status'=>7,
            );
            $where_condition=array(
                'order_id'=>$order_id
            );
            $this->General_model->update_details($table_name, $update_data, $where_condition);
            redirect(base_url('payment-failed'));
        }

    }

    public function payment_failed()
    {
        $this->load->view('frontend/payment_failed');
    }
    
    private function callAPI($method, $url, $data){
        $curl = curl_init();

        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'X-Parse-Application-Id: 9803886e4a724d87bc56b44bd7512d3e2093bd95',
            'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        if(!$result){die("Connection Failure");}
        curl_close($curl);
        return $result;
    }

}
