<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegisterLoginController extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->model('General_model');
         $this->load->library('email');
        if (!($this->session->userdata('language'))) {
            $this->session->set_userdata('language', 'english');
        }
        $this->lang->load('content_form_' .$this->session->userdata('language'));
    }

    public function register(){
        $address=$_POST['build_no'].','.$_POST['street_no'].','.$_POST['city'];

        $data=array(
            'full_name'=> ucwords(strtolower($_POST['name'])),
            'phone'=>$_POST['mobile'],
            'email'=>$_POST['email'],
            'user_status'=>0,
            'create_date' => date("Y-m-d"),
            'password'=>encryptPassword($_POST['pswd']),
        );

        $slast_insert_id=$this->General_model->get_lastinsertid_after_insertdata('sm_users',$data);
        if($slast_insert_id){
            $addData = array(
                'user_id' => $slast_insert_id,
                'addline1' => $_POST['build_no'],
                'addline2' => $_POST['street_no'],
                'city_id' => 1,
                'add_type' => 'profile',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            );
            $add_id=$this->General_model->get_lastinsertid_after_insertdata('sm_addresses',$addData);
            if($slast_insert_id){
//                $message='<p>Hi</p>';
//                $message.='<p>Please Verify the email by clicking below button</p><br>';
//                $message.='<p><a href="'.base_url().'verify-email/'.encryptPassword($slast_insert_id).'" class="btn btn-info">Verify</a></p><br>';
//                $message.='<p>Regards</p>';
//                $message.='<p>Salad Mania Team</p>';
//                $msg=$this->load->view('email/register_user',$message,TRUE);
//                $status=sendmail($_POST['email'],'','',$msg,'Verify Email:Salad Mania');


                $table='sm_users';
                $select='';
                $where=array(
                    'id'=>$slast_insert_id
                );
             
                $message['user'] = $this->General_model->get_row($table,$select,$where);
                $message['type'] = 'signup';
                $msg = $this->load->view('email/register_user',$message, TRUE);
                $status = sendmail($_POST['email'],'','',$msg,'Verify Email:Salad Mania');

                if($status){
                    $response=array(
                        'status'=>true,
                        'msg'=>$this->lang->line('email_login'),
                    );
                }else{
                    $response=array(
                        'status'=>false,
                        'msg'=>$this->lang->line('error_email_send'),
                    );
                }
            }
        }else{
            $response=array(
                'status'=>false,
                'msg'=>$this->lang->line('data_insert_error'),
            );
        }

        echo json_encode($response);


    }
    public function login(){
        $email=$_POST['email'];
        $pswd=$_POST['pswd'];
        $where=array(
            'email'=>$email,
            'user_type'=>'U'

        );
        $if_exist=$this->General_model->get_row('sm_users','',$where);
        //echo "<pre>"; print_r($if_exist);exit;
        if($if_exist){
            if($if_exist->user_status==1){
                if(dycryptPassword($if_exist->password)===$pswd){
                    $user_session_data=array(
                        'id'=>$if_exist->id
                    );
                    $this->session->set_userdata('user_login_session',$user_session_data);

                    //cart seesion data moce to salad table and cart table
                    if($this->session->userdata('cart_data')){
                        $login=$this->session->userdata('user_login_session');
                        $salad_data=$this->session->userdata('cart_data');
                        if($salad_data){
                            foreach ($salad_data as $salad){
                                if($salad['salad_type'] == 1) {
                                    $data = array(
                                        'user_id' => $login['id'],
                                        'salad_type' => 1,
                                        'salad_id' => $salad['salad_id'],
                                        'cat_id' => $salad['cat_id'],
                                        'quantity' => $salad['quantity'],
                                        'dressing_mixed_in' => 0,
                                    );
                                    $last_id = $this->General_model->get_lastinsertid_after_insertdata('sm_carts', $data);

                                    if ($last_id) {
                                        if (isset($salad['extra_ingrediants']) && count($salad['extra_ingrediants']) > 0) {
                                            foreach ($salad['extra_ingrediants'] as $ings) {
                                                $data = array(
                                                    'ing_id' => $ings['ing_id'],
                                                    'quantity' => $ings['quantity'],
                                                    'subcat_id' => $ings['subcat_id'],
                                                    'cart_id' => $last_id
                                                );
                                                $this->db->insert('sm_cart_ingredients', $data);
                                            }
                                        }
                                        $response = array(
                                            'status' => true
                                        );

                                    } else {
                                        $response = array(
                                            'status' => false
                                        );
                                    }
                                }
                                elseif($salad['salad_type'] == 2){
                                    $data=array(
                                        'user_id'=>$login['id'],
                                        'salad_type'=>2,
                                        'salad_id'=>0,
                                        'salad_name'=>$salad['salad_name'],
                                        'salad_image'=>'custom.png',
                                        'cat_id'=>$salad['cat_id'],
                                        'quantity'=>$salad['quantity'],
                                        'dressing_mixed_in'=>0,
                                    );
                                    $last_id=$this->General_model->get_lastinsertid_after_insertdata('sm_carts',$data);
                                    if($last_id){
                                        if(isset($salad['extra_ingrediants']) && count($salad['extra_ingrediants']) >0){
                                            foreach ($salad['extra_ingrediants'] as $ings){
                                                $data=array(
                                                    'ing_id'=>$ings['ing_id'],
                                                    'quantity'=>$ings['quantity'],
                                                    'subcat_id'=>$ings['subcat_id'],
                                                    'cart_id'=>$last_id
                                                );
                                                $this->db->insert('sm_cart_ingredients',$data);
                                            }
                                        }
                                        $response=array(
                                            'status'=>true
                                        );

                                    }else{
                                        $response=array(
                                            'status'=>false
                                        );
                                    }
                                }
                            }
                        }
                    }

                    $response=array(
                        'status'=>true,
                        'msg'=>''
                    );

                }else{
                    $response=array(
                        'status'=>false,
                        'msg'=>$this->lang->line('incorrect_pass'),
                    );
                }
            }else{
                $response=array(
                    'status'=>false,
                    'msg'=>$this->lang->line('email_verify_error') ,
                );
            }

        }else{
            $response=array(
                'status'=>false,
                'msg'=>$this->lang->line('not_register_email') ,
            );
        }
        echo json_encode($response);
    }

    public function logout(){
        $this->session->unset_userdata('user_login_session');
        $this->session->unset_userdata('cart_session');
        $this->session->unset_userdata('checkout_session');
        $this->session->unset_userdata('set_userdata');
        $this->session->sess_destroy();
        redirect(base_url());
    }
    public function forgot($encrypted_id=null){
        if($encrypted_id){
            $where=array(
                "id"=>dycryptPassword($encrypted_id),
                "user_type"=>"U",
                "ch_pswd_status"=>1,
            );
            $this->db->select('*');

            $this->db->from('sm_users');
            $this->db->where('deleted_at =', NULL);
            $this->db->where($where);
            $this->db->where('reset_pswd_start_time  <=',date('Y-m-d h:i:s'));
            $this->db->where('reset_pswd_end_time >=',date('Y-m-d h:i:s'));
            $query = $this->db->get();
            $if_exist = $query->row();
            if($if_exist){
                $data['encrypted_id']=$encrypted_id;
                $data['title']='Reset Password';
                $data['page']='frontend/reset_password';
                $this->load->view('frontend/home',$data);
            }else{
                redirect(base_url());
            }


        }else{
            $email=$_POST['email'];
            $where=array(
                'email'=>$email,
                'user_type'=>'U'
            );
            $if_exist=$this->General_model->get_row('sm_users','',$where);
            if($if_exist){
                if($if_exist->user_status==1){

                    $message['user_id'] = $if_exist->id;
                    $message['type'] = 'resetPassword';

                    $msg=$this->load->view('email/register_user',$message,TRUE);
                    $status=sendmail(getSingleFieldDetail('id',$if_exist->id,'email','sm_users'),'','',$msg,'Salad Mania:Verify Email');
                    if($status){
                        $minutes_to_add = 10;
                        $now_date=date("Y-m-d h:i:s");
                        $time = new DateTime($now_date);
                        $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
                        $fine_time = $time->format('Y-m-d H:i');
                        $update_data=array(
                            'ch_pswd_status'=>1,
                            'reset_pswd_start_time'=>date("Y-m-d h:i:s"),
                            'reset_pswd_end_time'=>$fine_time
                        );
                        $status=$this->General_model->update_details('sm_users',$update_data,$where);
                        if($status){
                            $response=array(
                                'status'=>true,
                                'msg'=>$this->lang->line('email_reset_pass'),
                            );
                        }
                        else{
                            $response=array(
                                'status'=>false,
                                'msg'=>$this->lang->line('data_update_error'),
                            );
                        }

                    }else{
                        $response=array(
                            'status'=>false,
                            'msg'=>$this->lang->line('error_email_send'),
                        );
                    }

                }else{
                    $response=array(
                        'status'=>false,
                        'msg'=>$this->lang->line('email_verify_error') ,
                    );
                }

            }else{
                $response=array(
                    'status'=>false,
                    'msg'=>$this->lang->line('not_register_email') ,
                );
            }
            echo json_encode($response);
        }
    }
    public function reset_password(){
        if(isset($_POST['encrypted_id'])){
            $where=array(
                'id'=>dycryptPassword($_POST['encrypted_id']),
                'user_type'=>'U'
            );
            $update_data=array(
                'ch_pswd_status'=>0,
                'reset_pswd_start_time'=>null,
                'reset_pswd_end_time'=>null,
                'password'=>encryptPassword($_POST['pswd'])
            );
            $status=$this->General_model->update_details('sm_users',$update_data,$where);
            if($status){
                $this->session->set_flashdata('success', $this->lang->line('reset_pass_success'));
                $response=array(
                    'status'=>true,
                    'msg'=>$this->lang->line('reset_pass_success'),
                );
            }
            else{
                $response=array(
                    'status'=>false,
                    'msg'=>$this->lang->line('data_update_error'),
                );
            }
        }else{
            $response=array(
                'status'=>false,
                'msg'=>$this->lang->line('went_wrong') ,
            );
        }
        echo json_encode($response);
    }
    public function guest_register(){
        $data=array(
            'full_name'=>$_POST['name'],
            'phone'=>$_POST['mobile'],
            'email'=>$_POST['email'],
            'user_status'=>1,
            'user_type'=>'G'
        );
        $slast_insert_id=$this->General_model->get_lastinsertid_after_insertdata('sm_users',$data);
        $data=array(
            'id'=>$slast_insert_id
        );
        $this->session->set_userdata('guest_user_session',$data);
        if($slast_insert_id){
            // $message='<p>Hi</p>';
            // $message.='<p>Thank You for register as a guest user! Now you can place order!!</p><br>';
            // $message.='<p>Regards</p>';
            // $message.='<p>Salad Mania Team</p>';
            // $table='sm_users';
            // $select='';
            // $where=array(
            //     'id'=>$slast_insert_id
            // );
            
            // $message['user'] = $this->General_model->get_row($table,$select,$where);
            // $message['type'] = 'guest';
            // $msg = $this->load->view('email/register_user',$message, TRUE);
            // $status=sendmail($_POST['email'],'','',$msg,'Salad Mania');
            // if($status){
                $response=array(
                    'status'=>true,
                    'msg'=>$this->lang->line('guest_register_success'),
                );
            // }else{
            //     $response=array(
            //         'status'=>false,
            //         'msg'=>$this->lang->line('error_email_send'),
            //     );
            // }
        }else{
            $response=array(
                'status'=>false,
                'msg'=>$this->lang->line('data_insert_error'),
            );
        }

        echo json_encode($response);
    }


}