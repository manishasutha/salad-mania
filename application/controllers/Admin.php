<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct(){
            parent::__construct();
            $this->load->helper('form');
            $this->load->library('form_validation');
            $this->load->model('admin_model');
            // $this->load->model('notification_model');
            $this->load->model('privacydetail_model');
            //$this->lang->load('italian', 'italian');
            is_logged_in();
    }
    public function index(){
        if(!is_admin()){    
            redirect(base_url().'customer');
            exit;
        }
        $data['page_title'] ='Dashboard';
        $data['total_category'] = $this->admin_model->count_total('category');
        $data['total_pending'] = $this->admin_model->count_total('pending_customer');
        $data['total_users'] = $this->admin_model->count_total('app_users');
        $data['total_customers'] = $this->admin_model->count_total('customer');
        $data['users'] = $this->admin_model->get_recent_users('users');
        $data['app_users'] = $this->admin_model->get_recent_users('app_users');
        $data['total_ratings'] = $this->admin_model->count_total('rating_comment');
        //print_r($data['users']);
        //exit;
        $this->load->view('dashboard',$data);
    }
    public function email_setting(){
        if(!is_admin()){    
            redirect(base_url().'customer');
            exit;
        }
        $data['page_title'] ='Email Setting';
        $data['email'] = $this->admin_model->get_email_setting();
        $this->load->view('email_setting',$data);
    }
    public function email_setting_action(){
        $this->form_validation->set_rules('smtp_type', 'smtp type', 'required');
        $this->form_validation->set_rules('smtp_host', 'host name', 'required');
        $this->form_validation->set_rules('smtp_port', 'port number', 'required');
        $this->form_validation->set_rules('smtp_email', 'email address', 'required');
        $this->form_validation->set_rules('smtp_password', 'password', 'required');
        if ($this->form_validation->run() == FALSE){
            //$error = validation_errors();
            $data['page_title']='Email Setting';
            $data['email'] = $this->admin_model->get_email_setting();
            $this->load->view('email_setting',$data); 
        }else{
            $check_email = $this->admin_model->get_email_setting();
            $email_data = array(
                'type' => $this->input->post('smtp_type'),
                'smtp_host'=> $this->input->post('smtp_host'),
                'smtp_port'=> $this->input->post('smtp_port'),
                'smtp_user' => $this->input->post('smtp_email'),
                'smtp_password' => $this->input->post('smtp_password')
            );
            //print_r($email_data);exit;
            if($check_email){
                $id = $this->input->post('id');
                $this->admin_model->update_email_setting($email_data,$id);
                $this->session->set_flashdata('success_msg', ' Email setting has been updated successfully.');
            }else{
                $this->admin_model->add_email_setting($email_data);
                $this->session->set_flashdata('success_msg', ' Email setting has been saved successfully.');
            }
            redirect(base_url()."admin/email_setting");
            exit;
    
        }
    }

}
