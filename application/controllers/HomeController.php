<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

    public function __construct(){
        
        parent::__construct();
        $this->load->model('General_model');
         $this->load->model('Main_model');
          if (!($this->session->userdata('language'))) {
          $this->session->set_userdata('language', 'English');
        }  
       $this->lang->load('content_form_' .$this->session->userdata('language'));
        //echo($this->session->userdata('language'));exit;
    }
    public function index($encrypt_id=null){
        if($encrypt_id){
            $if_exist=getSingleFieldDetail('id',dycryptPassword($encrypt_id),'id','sm_users');

            if($if_exist){
                 // echo'false';
                $update_data=array(
                    'user_status'=>1,
                );
                 $where=array(
                    'id'=>dycryptPassword($encrypt_id)
                );
                $status=$this->General_model->update_details('sm_users',$update_data,$where);
                if($status){
                    $data['verified']=true;
                }
                
            }
        }
        $data['title']='Home';
        $select_fields=array(
            'cat_name','cat_id','cat_name_arabic','contains_make_own',
        );

        $where=array(
            'cat_status'=>1
        );
        $order_by    = 'cat_id asc';

        $data['categories']=$this->General_model->get_multiple_row('sm_categories',$select_fields,$where,$order_by);
        $data['page']='frontend/index';
         $this->load->view('frontend/home',$data);
    }


public function send_msg()
{
    $msg = "hi this is testing sms";
    $NO='962799275012';
    $msg=SendSmsapi($msg, $NO);
    print_r($msg);
}

    public function myorderdetail($id)
    {
        $login_id = $this->session->userdata('user_login_session');
        $userid = $login_id['id'];
        if ($userid == '') {
            redirect(base_url());
        }
        $myarray = $this->session->userdata();
        $array = array_column($myarray, 'id');
        $userid = $array[0];

        //$ordersql = "select u.full_name,u.address,u.phone,o.* from sm_orders as o join sm_users as u on o.user_id = u.id  where o.user_id = $userid and o.order_id= $id";
        $sql = "select o.*, u.full_name, u.phone from sm_orders as o join sm_users as u on o.user_id = u.id where o.order_id = $id ";
        $order = $this->Main_model->__callMasterquery($sql);
        if ($order->num_rows() > 0) {
            $row = $order->row();
            $status = '';
            //Delivery Status from our DB in case of delivery
            if ($row->in_store_pickup == 0) {
                if ($row->o_status == 7 || $row->o_status == 0)
                    $status = $this->lang->line('failed');
                else if ($row->delivery_status == 1)
                    $status = $this->lang->line('created');
                else if ($row->delivery_status == 2)
                    $status = $this->lang->line('pending');
                else if ($row->delivery_status == 3)
                    $status = $this->lang->line('picked');
                else if ($row->delivery_status == 4)
                    $status = $this->lang->line('completed');
                else if ($row->delivery_status == 5)
                    $status = $this->lang->line('canceled');
                else
                    $status = $this->lang->line('not_available');
            }
            if ($row->in_store_pickup == 1) {
                if ($row->o_status == 7 || $row->o_status == 0)
                    $status = $this->lang->line('failed');
                else if ($row->o_status == 1)
                    $status = $this->lang->line('placed');
                else if ($row->o_status == 2)
                    $status = $this->lang->line('created');
                else if ($row->o_status == 6)
                    $status = $this->lang->line('canceled');
            }

            //get user address
            $address_id = $row->address_id;
            if (empty($address_id)) {
                $user_id = $row->user_id;
                $addsql = "select a.addline1,a.addline2,c.city from sm_addresses a  join sm_cities c on c.city_id = a.city_id where a.user_id = $user_id";
            } else if (!empty($address_id)) {
                $address_id = $row->address_id;
                $addsql = "select a.addline1,a.addline2,c.city from sm_addresses a  join sm_cities c on c.city_id = a.city_id where a.add_id = $address_id";
            }
            $add = $this->Main_model->__callMasterquery($addsql);
            $address = '';
            if ($add->num_rows() > 0) {
                $addrow = $add->row();
                //Creating Order detail HTML
                $address = $addrow->addline1 . ' ' . $addrow->addline2 . ' ,' . $addrow->city;
            }

            $html = '<div class="col-sm-6 ">  
                        <h4 class="section-title">' . $this->lang->line('order_detail') . '</h4>
                    </div>
                    <div class="col-sm-6 ">  
                        <h4 class="section-title">' . $this->lang->line('shipping_detail') . '</h4>
                    </div>
                    <div class="clearfix"></div>
                        <div class="col-sm-6 ">
                            <div class="shipping-address">
                                <p> Payment: <b id="method_pay">' . $row->payment_method . '</b></p>
                                <p> Status: <b id="statuschange">' . $status . '</b></p>
                                <p> Date: <b> ' . date("d-m-Y", strtotime($row->created_at)) . '</b> 
                                </p>
                            </div>
                        </div>
                        <div class="col-sm-6 ">
                            <div class="shipping-address">
                                <h4>' . ucwords(strtolower($row->full_name)) . '</h4>
                                <p>' . $address . '</p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-10 order" id="order_head">
                            <h4 class="section-title">' . $this->lang->line('order_items') . '</h4>
                        </div>';

            $deliveryDateTime = '';
            if ($row->order_delivered_at != NULL)
                $deliveryDateTime = date("d M Y h:i a", strtotime($row->order_delivered_at));

            //Creating Query
            $saladsql="select ord.order_id, ord.user_id, s.salad_desc,s.salad_desc_arabic ,ord.total_price, ord.discount_amt, ord.tax_amt, ord.delivery_charge, ord.grand_total, ord.payment_status, ord.payment_method, ord.order_notes, ord.in_store_pickup, ord.customer_name, ord.customer_phone, ord.pickup_date, ord.pickup_time, ord.o_status, ord.address_id,ord.promo_id, ord.transaction_id, ord.delivery_id, ord.create_date, ord.created_at, ord.order_delivered_at, dor.odetail_id, dor.order_id, dor.salad_id,dor.salad_name, dor.cat_name, dor.cat_name_arabic, dor.salad_name_arabic, dor.salad_img, dor.quantity,	dor.dressing_mixed_in, dor.item_total_price, dor.item_total_calories, dor.salad_type from sm_orders as ord join sm_order_details as dor on ord.order_id = dor.order_id left join sm_salads as s on dor.salad_id = s.salad_id where ord.order_id=$id ";
            $query = $this->Main_model->__callMasterquery($saladsql);
            if($query->num_rows() > 0) {
                $odetail_id = $query->row();
                $i = 1;
                foreach ($query->result() as $srow)
                {
                    if($srow->salad_id == 0){
                        $salad_desc ='';
                    }else {
                        $table = 'sm_salads';
                        $select = 'salad_desc,salad_desc_arabic';
                        $where = array(
                            'salad_id' => $srow->salad_id
                        );
                        $salads = $this->General_model->get_row($table, $select, $where);
                        $salad_desc = $salads->salad_desc;
                        $language= $this->session->userdata('language');
                        if($language=='Arabic') {
                            $salad_desc = $salads->salad_desc_arabic;
                        }
                    }
                    $salad_name=$srow->salad_name;
                    $language= $this->session->userdata('language');
                    if($language=='Arabic'){
                        $salad_name=$srow->salad_name_arabic;
                    }
                    $total_price=$srow->item_total_price;$quantity=$srow->quantity;
                    $total=$total_price*$quantity;
                    //if no salad image
                    if($srow->salad_img == NULL)
                    {
                        $srow->salad_img = 'custom.png';
                    }
                    $saladImg = INCLUDE_SALAD_IMAGE_PATH.$srow->salad_img;
                    $html.='<div class="col-sm-12"> 
                                <div class="order-detail">
                                <div class="order-media">
                                    <div class="row">
                                        <div class="col-lg-2 padding-0">
                                            <div class="o-img order-img">
                                                <img src="'.$saladImg.'"/>
                                            </div>
                                        </div>

                                         <div class="col-lg-10 padding-0">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6">
                                                    <h4>'.$salad_name.'</h4>
                                                </div>

                                                <div class="col-lg-6 col-md-6">
                                                   <span class="price"> '.$srow->quantity.' Qty | '.$srow->item_total_calories.' Cal | <span class="price-green">'.round($total,'1').' JD</span></span> 
                                                </div>
                                            </div>
                                            <div class="order-ing" style="padding: 0px">
                                                <p>'.$salad_desc.'</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>';
                                $detail_id=$odetail_id->odetail_id;
                                 //Creating Query
                                $ordsql="select * from sm_order_ingredients where odetail_id =$srow->odetail_id group by ing_id";
                                $ordercat=$this->Main_model->__callMasterquery($ordsql);
                                if(!empty($ordercat) && $ordercat->num_rows() > 0) {
                                    $i = 1;
                                    $html.='<div class="order-addons">
                                              <h5>'.$this->lang->line('add_on').'</h5>
                                              <table>';
                                    foreach($ordercat->result() as $oirow){
                                        $ing_name=$oirow->ing_name;
                                        $language= $this->session->userdata('language');
                                        if($language=='Arabic'){
                                            $ing_name=$oirow->ing_name_arabic;
                                        }
                                        $html.='<tr>
                                                   <td>'.$ing_name.'</td>
                                                   <td>'.round($oirow->ing_price,'1').$this->lang->line('jd').'</td>
                                                   <td>'.$oirow->ing_calories.' Cal</td> 
                                                 </tr>';
                                    }
                                    $html.='  </table>
                                            </div>';
                                    }
                    $html.='</div></div>';
                }
            }
            //Append amount detail in HTML
            $html .= '<div class="order-detail">
                         <div class="col-sm-12 " >
                             <div class="table-responsive order-charges">
                                <table class="table table-striped">
                                    <tr>
                                        <td>'.$this->lang->line('total').'</td>
                                        <th>'.round($row->total_price,'1').' JD</th>
                                     </tr>
                                    <tr>
                                        <td>'.$this->lang->line('taxes').'</td>
                                        <th>'.round($row->tax_amt,'1').$this->lang->line('jd').'</th>
                                    </tr>';
            if($row->promo_id != NULL){
                $html .= '<tr>
                            <td><small>'.$this->lang->line('promo_code').'</small></td>
                            <th>- '.round($row->discount_amt,'1').' JD</th>
                        </tr>';
            }
            $html .= '<tr>
                        <td>'.$this->lang->line('delivery_charges').'</td>
                        <th>'.round($row->delivery_charge,'1').$this->lang->line('jd').'</th>
                     </tr>
                     </table>
                     <table class="table table-striped">    
                         <tr>
                            <td>'.$this->lang->line('grand_total').'</td>
                            <th><span class="green">'.round($row->grand_total,'1').$this->lang->line('jd').'</span></th>
                         </tr>
                     </table>
                 </div>
                 </div>
                 </div>';

            $response['error'] = 0;
            $response['htmlContent'] =  $html;
            }
            else{
                $response['error'] = 1;
                $response['msg'] = $this->lang->line('no_record');
            }
            echo json_encode($response);
        // exit;
        }



    public function our_story(){
        $data['title']='Our Story';
        $data['page']='frontend/our_story';
        $this->load->view('frontend/home',$data);
    }
    public function change_password(){
       
        if(isset($_POST['pswd'])){
            $user_id=$this->session->userdata('user_login_session');
            $where=array(
                'id'=>$user_id['id'],
            );
            $update_data=array(
                'ch_pswd_status'=>1,
                'password'=>encryptPassword($_POST['pswd'])
            );
            $status=$this->General_model->update_details('sm_users',$update_data,$where);
            if($status){
                $this->session->sess_destroy();
              
                $response=array(
                    'status'=>true,
                    'msg'=>$this->lang->line('reset_pass_success'),
                );
            }
            else{
                $response=array(
                    'status'=>false,
                    'msg'=>$this->lang->line('data_update_error'),
                );
            }
            echo json_encode($response);
        }else{
            redirect(base_url());
        }

    }

    public function myProfile(){
        $login_id=$this->session->userdata('user_login_session');
        $id=$login_id['id'];
        if($id==''){
            redirect(base_url());
        }
        $where=array(
          'id'=>$id
        );
        $data['userdata']=$this->General_model->get_row('sm_users','',$where);

        $where=array(
            'user_id'=>$id,
            'add_type'=>'profile',
        );
        $order_by='add_id DESC';
        $data['address']=$this->General_model->get_row('sm_addresses','',$where,$order_by);

        //Call Orders
        $select_fields=array('order_id');
        $where=array(
            'user_id'=>$id
        );
        
        $order_by  = 'order_id desc';
        $data['orders'] = $this->General_model->get_multiple_row('sm_orders',$select_fields,$where,$order_by);        
      
        $data['title']='My Profile';
        $data['page']='frontend/my_profile';
        $this->load->view('frontend/home',$data);
       
    }
    
  
   //profile update
   public function profile_update(){
         $login_id=$this->session->userdata('user_login_session');
        $id=$login_id['id'];
       if($id==''){
            redirect(base_url());
        }
        $data=array(
            'full_name'=>$_POST['name'],
            'phone'=>$_POST['phone'],
        );
         $user_id=$this->session->userdata('user_login_session');
            $where=array(
                'id'=>$user_id['id'],
            );
        $slast_insert_id=$this->Main_model->__callUpdate('sm_users',$where,$data);
        if($slast_insert_id){
          
            $addData = array(
                'addline1' => $_POST['build_no'],
                'addline2' => $_POST['street_no'],  
                'updated_at' => date("Y-m-d H:i:s")
            );
            $where=array(
                'user_id'=>$user_id['id'],
                'add_id'=>$_POST['add_id'],
            );
            $slast_add_id=$this->Main_model->__callUpdate('sm_addresses',$where,$addData);
            if($slast_add_id){
                $response=array(
                    'status'=>true,
                    'msg'=>$this->lang->line('record_update'),
                );
            }
            $response=array(
                    'status'=>true,
                    'msg'=>$this->lang->line('record_update'),
                );
        }else{
            $addData = array(
                'addline1' => $_POST['build_no'],
                'addline2' => $_POST['street_no'],  
                'updated_at' => date("Y-m-d H:i:s")
            );
            $where=array(
                'user_id'=>$user_id['id'],
                'add_id'=>$_POST['add_id'],
            );
            $slast_add_id=$this->Main_model->__callUpdate('sm_addresses',$where,$addData);
            if($slast_add_id){
                $response=array(
                    'status'=>true,
                    'msg'=>$this->lang->line('record_update'),
                );
            }
            $response=array(
                'status'=>true,
                'msg'=>$this->lang->line('record_update'),
            ); 
        }     
        echo json_encode($response);
    }
      

    public function about_us(){
        $data['title']='About Us';
        $data['page']='frontend/about_us';
        
        $this->load->view('frontend/home',$data);
    }
    public function contact_us(){
        $data['title']='Contact us';
        $data['page']='frontend/template/contact';
        
        $this->load->view('frontend/home',$data);
    }
    
    public function contact_us_submit(){

        //echo "<pre>"; print_r($_POST);exit;
        extract($_POST);
        
        $to = 'ahmad@saladmania.net'; //Default
        if($_POST['type'] === 'contact'){
            //$to='rupali@coretechies.com';
            $to=getSingleFieldDetail('id',1,'contact_email','sm_app_settings');
            $subj=$this->lang->line('contact_enquery');
            
        }elseif ($_POST['type'] === 'dietician'){
            //$to='rupali@coretechies.com';
            $to=getSingleFieldDetail('id',1,'diet_email','sm_app_settings');
            $subj=$this->lang->line('dietician_enquery');
        }
        
        $message = array(
            'type' => $_POST['type'],
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'subject' => $_POST['subject'],
            'message' => $_POST['message'],
        );
        $msg = $this->load->view('email/register_user', $message, TRUE);
        $status = sendmail($to,'','',$msg, 'Salad Mania : '.$subj);

        if($status){
            $response=array(
                'status'=>true,
                'msg'=>'Email has been sent successfully',
            );
        }else{
            $response=array(
                'status'=>false,
                'msg'=>$this->lang->line('error_email_send'),
            );
        }

        echo json_encode($response);
    }

    public function terms(){
        $sql = "select tc_page,tc_page_arabic from sm_app_settings ";
        $data['terms'] = $this->Main_model->__callMasterquery($sql);
        $data['title']='Term & Condition';
        $data['page']='frontend/terms';

        $this->load->view('frontend/home',$data);
    }
    public function policy(){
        $data['title']='Policy';
        $sql = "select privacy_page,privacy_page_arabic from sm_app_settings ";
        $data['privacy'] = $this->Main_model->__callMasterquery($sql);
        $data['page']='frontend/privacy';

        $this->load->view('frontend/home',$data);

    }	

    //change language
    public function change_language($lang) {
        //echo $lang;exit;
        if($lang == 'arabic') {
             $this->session->set_userdata('language','Arabic');
            //echo  $this->session->userdata('language');
        } else {
            $this->session->set_userdata('language','English');
            //echo  $this->session->userdata('language');
        }
    }

    private function callAPI($method, $url, $data){
        $curl = curl_init();

        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'X-Parse-Application-Id: 9803886e4a724d87bc56b44bd7512d3e2093bd95',
            'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        if(!$result){die("Connection Failure");}
        curl_close($curl);
        return $result;
    }
}