<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	public function __construct()
    {
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('profile_model');
		 $this->load->model('customer_model');
		// $this->load->model('notification_model');
		is_logged_in();
    }
	public function changePassword(){
		$data['page_title']='Password Change';
		$user_id = $this->session->userdata('user_id');
		$data['customer'] = $this->customer_model->get_customer_by_fk_user_id($user_id);
		$this->load->view('changePassword',$data);
	}
	public function profile_change(){
		$data['page_title']='Profile change';
		$user_id = $this->session->userdata('user_id');
		$data['customer'] = $this->customer_model->get_customer_by_fk_user_id($user_id);
		$data['user'] = $this->profile_model->get_user_by_id($user_id);
		$this->load->view('profile',$data);
	}
	public function user(){
		$data['page_title']='User';
		$this->load->view('user',$data);
	}
	public function updateProfile(){
		if(empty($_REQUEST['name'])){
			$response['error'] = 1;
			//$response['msg'] = 'Please Enter email  id';
			$response['msg'] = $this->error_msg_html('Please Enter Name.',1);
			print_r(json_encode($response));
			exit;
		}else if(empty($_REQUEST['surname'])){
			$response['error'] = 1;
			//$response['msg'] = 'Please Enter password';
			$response['msg'] = $this->error_msg_html('Please Enter surname.',1);
			print_r(json_encode($response));
			exit;
		}else if(empty($_REQUEST['phone'])){
			$response['error'] = 1;
			//$response['msg'] = 'Please Enter password';
			$response['msg'] = $this->error_msg_html('Please Enter phone number.',1);
			print_r(json_encode($response));
			exit;
		}/*else if(empty($_REQUEST['email'])){
			$response['error'] = 1;
			//$response['msg'] = 'Please Enter password';
			$response['msg'] = $this->error_msg_html('Please Enter Email.',1);
			print_r(json_encode($response));
			exit;
		}else if(empty($_REQUEST['image'])){
			$response['error'] = 1;
			//$response['msg'] = 'Please Enter password';
			$response['msg'] = $this->error_msg_html('Please Select profile image.',1);
			print_r(json_encode($response));
			exit;
		}*/else{
			$data['user_id'] = $this->session->userdata('user_id');
			$data['name'] = $_REQUEST['name'];
			$data['surname'] = $_REQUEST['surname'];
			$data['phone'] = $_REQUEST['phone'];
			//file uuload start 
			if(!empty($_FILES['image']['name'])){
				$config['upload_path'] = './uploads/';
				$config['allowed_types']= 'gif|jpg|png|jpeg|JPEG';
				// $config['max_size']    = '10000';
				// $config['max_width']   = '1024';
				// $config['max_height']  = '768';
				$this->load->library('upload', $config);
				if(!$this->upload->do_upload('image')){
					$error = $this->upload->display_errors();
					//print_r($error);exit;
					$response['error'] = 1;
					$response['msg'] = $this->error_msg_html($error,1);
					print_r(json_encode($response));
					exit;

				}else{
					//$data = array('upload_data'=>$this->upload->data());
					$data_uploaded = $this->upload->data();
					$filename = $data_uploaded['file_name'];
					//$uploaded_status = true;
					$data['imagepath'] = 'uploads/'.$filename;
				}
			}else{
				//$uploaded_status = false;
				$data['imagepath'] = $_REQUEST['old_image'];
			}
			//file upload end
			if($this->profile_model->update_profile_by_id($data)){
				$response['error'] = 0;
				$response['imagepath'] = $data['imagepath'];
				$this->update_session_login_data($data['user_id']);
				$response['msg'] = $this->error_msg_html('Profile updated Successfully.',0);
				$this->session->set_flashdata('success_msg', ' Profile updated Successfully');
			}else{
				$response['error'] = 1;
				$response['msg'] = $this->error_msg_html('Some things went wrong, Please try again.',1);
			}
			print_r(json_encode($response));
			exit;
		}
	}
	public function changePasswordAction(){
		//echo md5(123456);exit;
		$user_id = $this->session->userdata('user_id');
		if(empty($_REQUEST['old_password'])){
			$response['error'] = 1;
			$response['msg'] = $this->error_msg_html('Please Enter old password.',1);
			print_r(json_encode($response));
			exit;
		}else if(empty($_REQUEST['password'])){
			$response['error'] = 1;
			$response['msg'] = $this->error_msg_html('Please Enter password.',1);
			print_r(json_encode($response));
			exit;
		}else if(empty($_REQUEST['conf_password'])){
			$response['error'] = 1;
			$response['msg'] = $this->error_msg_html('Please Enter confirm Password.',1);
			print_r(json_encode($response));
			exit;
		}else if($_REQUEST['password'] != $_REQUEST['conf_password']){
			$response['error'] = 1;
			$response['msg'] = $this->error_msg_html('Password and confirm password does not match.',1);
			print_r(json_encode($response));
			exit;
		}else if(!$this->profile_model->password_match_by_id($user_id, $_REQUEST['old_password'])){
			$response['error'] = 1;
			$response['msg'] = $this->error_msg_html('Please enter correct old password.',1);
			print_r(json_encode($response));
			exit;
		}else{
			if($this->profile_model->change_password_by_id($user_id, $_REQUEST['password'])){
				$response['error'] = 0;
				$response['msg'] = $this->error_msg_html('Password updated Successfully.',0);
				$this->session->set_flashdata('success_msg', ' Password updated Successfully');
			}else{
				$response['error'] = 1;
				$response['msg'] = $this->error_msg_html('Some things went wrong, Please try again.',1);
			}
			print_r(json_encode($response));
			exit;
		}

	}
	function error_msg_html($msg,$status){
	    if($status){
        	$html = '<div class="alert alert-danger fade in">
	        <a href="#" class="close" data-dismiss="alert">&times;</a>
	        <strong>Error!</strong>'." ". $msg.'
	    	</div>';
	    }else{
	    	$html = '<div class="alert alert-success fade in">
        		<a href="#" class="close" data-dismiss="alert">&times;</a>
        		<strong>Success!</strong>'." ". $msg.'
        		</div>';
	    }
	    return $html;
	}
	public function update_session_login_data($id){
		$result = $this->profile_model->get_user_by_id($id);
		if($result){
			$newdata = array(
		        'user_id'  			=> $result[0]['serial_number'],
		        'user_role'			=> $result[0]['fk_role_id'],
		        'user_email'    	=> $result[0]['email_address'],
		        'user_first_name'   => $result[0]['first_name'],
		        'user_last_name'	=> $result[0]['last_name'],
		        'user_profile_image'=> $result[0]['profile_image'],
		        'logged_in' => TRUE
			);
			$this->session->set_userdata($newdata);
		}
	}
}
