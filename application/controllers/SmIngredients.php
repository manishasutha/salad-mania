<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SmIngredients extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
		$this->load->helper('common_helper');
			$this->load->model('Main_model');
			$this->load->model('General_model');
			 $this->load->database();
        	$this->load->library('encryption');
        is_logged_in();
        if(!is_admin()){    
            redirect(base_url().('customer'));
            exit;
        }
    }
   public function index()
	{	
		if(!is_admin()){    
            redirect(base_url().('customer'));
            exit;
        }
        $data['page_title']='Ingredient';
        $sql="select ing_id,ing_name,ing_name_arabic,ing_weight,unit_type,ing_price,ing_calories,ing_image,ing_status from sm_ingredients  order by ing_id desc ";
       	$data['query'] = $this->Main_model->__callMasterquery($sql);
		$this->load->view('sm_ingredients',$data);
	}

	//disable Ingedient
	public function disable($id){
		if($id){
			$delete_id=$id;
			//First check the ingredient is used or not is salads
			// $sql = "select salad_id from sm_salads where find_in_set('$delete_id',ing_ids)";
			// $query = $this->Main_model->__callMasterquery($sql);
			// if($query->num_rows() == 0)
			// {
				$deliveryadd=array(
	            	'ing_status'=>0
	            );
	            $wheres = array('ing_id'=>$delete_id);
	            $data = $this->Main_model->__callUpdate('sm_ingredients',$wheres,$deliveryadd); 
		   		if($data){

		   			//update ing last update time in sm_update_timing
					$time_data=array(
						'ing_update_time'=>date('Y-m-d H:i:s'),
						'salad_update_time'=>date('Y-m-d H:i:s')
					);
					$time_where=array('id'=>1);
					$this->Main_model->__callUpdate('sm_update_timing',$time_where,$time_data);

		   			$this->session->set_flashdata('success_msg', 'Ingredients Disabled Successfully');
		   			redirect(base_url()."ingredient/");
		   		}
		   	else{
				$this->session->set_flashdata('error_msg', 'Something went wrong.');
	   			redirect(base_url()."ingredient/");		   		
		   	}
		}
	}
	//enable Ingedient
	public function enable($id){
		if($id){
			 $delete_id=$id;
			 $deliveryadd=array(
            'ing_status'=>1
            );
            $wheres = array('ing_id'=>$delete_id);
            $data = $this->Main_model->__callUpdate('sm_ingredients',$wheres,$deliveryadd); 
	   		if($data){

	   			//update ing last update time in sm_update_timing
				$time_data=array(
					'ing_update_time'=>date('Y-m-d H:i:s'),
					'salad_update_time'=>date('Y-m-d H:i:s')
				);
				$time_where=array('id'=>1);
				$this->Main_model->__callUpdate('sm_update_timing',$time_where,$time_data);
				
	   			$this->session->set_flashdata('success_msg', 'Ingredients Enable Successfully');
	   			redirect(base_url()."ingredient/");
	   		} 
		}

	}

	//edit Ingedient
	public function edit($ingredid){
		if(isset($_POST['update'])){
			//echo "<pre>"; print_r($_POST);exit;
			extract($_POST);
			if(empty($this->input->post('price'))){
				$response['error'] = 1;
				$response['msg'] = 'Please enter Price';
				print_r(json_encode($response));
				exit;
			}else if(empty($this->input->post('unit_type'))){
				$response['error'] = 1;
				$response['msg'] = 'Please select Unit Type';
				print_r(json_encode($response));
				exit;
			}else if(empty($this->input->post('ing_name'))){
				$response['error'] = 1;
				$response['msg'] = 'please enter Ing Name';
				print_r(json_encode($response));
				exit;
			}else if(empty($this->input->post('ing_name_arabic'))){
				$response['error'] = 1;
				$response['msg'] = 'please enter Ing Name Arabic';
				print_r(json_encode($response));
				exit;
			}else if(empty($this->input->post('weight'))){
				$response['error'] = 1;
				$response['msg'] = 'please enter weight';
				print_r(json_encode($response));
				exit;
			}else if(empty($this->input->post('calories'))){
				$response['error'] = 1;
				$response['msg'] = 'please enter Calories';
				print_r(json_encode($response));
				exit;
			}
			else{
				$calories = $this->input->post('calories');
				$weight = $this->input->post('weight');
				$ing_name = $this->input->post('ing_name');
				$ing_name_arabic = $this->input->post('ing_name_arabic');
				$unit_type = $this->input->post('unit_type');
				$price = $this->input->post('price');
							
				if(!empty($_FILES['Image']['name'])){
					$file_details = array('upload_path'=> "./uploads/ingredients/", 'allowed_types' => "jpg|jpeg|gif|png|svg", 'max_size'=>"50000",'max_width'=>"50000", 'max_height'=>"50000", 'filename'=>"Image");
					$user_image = $this->uploadfile($file_details);
				}else{
					$Image = $this->input->post('ingimage');
					$user_image=$Image;
				}
				$data = array(
						'ing_calories'	=> $calories,
						'ing_weight'	=> $weight,
						'ing_name' 		=> $ing_name,
						'ing_name_arabic'=>$ing_name_arabic,
						'unit_type'		=> $unit_type,
						'ing_price'		=> $price,
						'ing_image' 	=> $user_image,
						'updated_at' 	=> date('Y-m-d H:i:s'),
				);
				$where=array('ing_id'=>$ingredid);
				//update ingedients
				$result = $this->Main_model->__callUpdate('sm_ingredients',$where,$data);
				
				//update ing last update time in sm_update_timing
				$time_data=array(
					'ing_update_time'=>date('Y-m-d H:i:s'),
					'salad_update_time'=>date('Y-m-d H:i:s')
				);
				$time_where=array(
					'id'=>1
				);
				$result = $this->Main_model->__callUpdate('sm_update_timing',$time_where,$time_data);
				
				//first delete the oldrecord
				$deletesql="delete from sm_ingredients_subcats where ing_id=$ingredid";
				$is_delete = $this->Main_model->__callMasterquery($deletesql);
				if($is_delete){
					$length=count($id);	
					for($i=0;$i<$length;$i++)
					{		
						$qty_up=0;
						if(isset($qty_up_down[$id[$i]])){
							$qty_up=1;
						}

						//Ingredient weight in the category
						$sing_weight=0;
						if(isset($ing_weight[$id[$i]])){
							$sing_weight = $ing_weight[$id[$i]];
						}

						//Ingredient sub unit type in the category
						$sunit_type = NULL;
						if(isset($sub_unit_type[$id[$i]])){
							$sunit_type = $sub_unit_type[$id[$i]];
						}

						//Ingredient price in the category
						$sing_price = 0;
						if(isset($ing_price[$id[$i]])){
							$sing_price = $ing_price[$id[$i]];
						}

						//Ingredient calories in the category
						$sing_calories = 0;
						if(isset($ing_cal[$id[$i]])){
							$sing_calories = $ing_cal[$id[$i]];
						}

						$subcatdata=array(
							'ing_id'=>$ingredid,
							'subcat_id'=>$id[$i],
							'qty_up_down'=>$qty_up,
							'sing_weight'=>$sing_weight,
							'sunit_type'=>$sunit_type,
							'sing_price'=>$sing_price,
							'sing_calories'=>$sing_calories,
						);

						//insert new data
						$subcatadd = $this->Main_model->__callInsert('sm_ingredients_subcats',$subcatdata);
					}
					if($subcatadd){	
						$response['error'] = 0;
						$response['msg'] = 'Data updated Successfully';
						$this->session->set_flashdata('success_msg', ' Data updated Successfully');
					}else{
						$response['error'] = 1;
						$response['msg'] = 'somethings went rong.';
					}
				}
				else{
					$response['error'] = 1;
					$response['msg'] = 'somethings went rong.';
				}
				$response['error'] = 0;
				$response['msg'] = 'Data updated Successfully';
				$this->session->set_flashdata('success_msg', ' Data updated Successfully');
				//$response['msg'] = $this->error_msg_html('Category added Successfully.',0);
			
				/*else{
					$deletesql="delete from sm_ingredients_subcats where ing_id=$ingredid";
					$is_delete = $this->Main_model->__callMasterquery($deletesql);
					if($is_delete){
						$length=count($id);	
						for($i=0;$i<$length;$i++)
						{		
							$qty_up=0;
							if(isset($qty_up_down[$id[$i]])){
								$qty_up=1;
							}
							$subcatdata=array(
								'ing_id'=>$ingredid,
								'subcat_id'=>$id[$i],
								'qty_up_down'=>$qty_up,
							);
							$subcatadd = $this->Main_model->__callInsert('sm_ingredients_subcats',$subcatdata);
						}
						if($subcatadd){	
							$response['error'] = 0;
							$response['msg'] = 'Data updated Successfully';
							$this->session->set_flashdata('success_msg', ' Data updated Successfully');
						}else{
							$response['error'] = 1;
							$response['msg'] = 'somethings went rong.';
						}
					}else{
								$response['error'] = 1;
								$response['msg'] = 'somethings went rong.';
							}
				}*/
				redirect(base_url().('ingredient/'));
		}
	}
		//call view file
		$sql="select ing.ing_id,ing.ing_name,ing.ing_name_arabic,ing.ing_weight,ing.unit_type,ing.ing_price,ing.ing_calories,ing.ing_image from sm_ingredients as ing  where ing.ing_id=$ingredid AND ing_status=1";
		$data['ingredient'] = $this->Main_model->__callMasterquery($sql);
		$sql = "select * from sm_categories where cat_status = 1";
    	$data['category']=$this->Main_model->__callMasterquery($sql);
		$data['unit_type']= unit_type();
        $data['page_title']='Ingredient';
        $this->load->view('edit_ingredient', $data);
    }
		
	//insert ingredient
	public function add(){
		if(isset($_POST['submit'])){
			//echo '<pre>'; print_r($_POST); exit;
			extract($_POST);

			if(empty($this->input->post('price'))){
				$response['error'] = 1;
				$response['msg'] = 'Please enter Price';
				print_r(json_encode($response));
				exit;
			}else if(empty($this->input->post('unit_type'))){
				$response['error'] = 1;
				$response['msg'] = 'Please select Unit Type';
				print_r(json_encode($response));
				exit;
			}else if(empty($this->input->post('ing_name'))){
				$response['error'] = 1;
				$response['msg'] = 'please enter Ing Name';
				print_r(json_encode($response));
				exit;
			}else if(empty($this->input->post('ing_name_arabic'))){
				$response['error'] = 1;
				$response['msg'] = 'please enter Ing Name Arabic';
				print_r(json_encode($response));
				exit;
			}else if(empty($this->input->post('weight'))){
				$response['error'] = 1;
				$response['msg'] = 'please enter weight';
				print_r(json_encode($response));
				exit;
			}else if(empty($this->input->post('calories'))){
				$response['error'] = 1;
				$response['msg'] = 'please enter Calories';
				print_r(json_encode($response));
				exit;
			}
			else{
				$calories = $this->input->post('calories');
				$weight = $this->input->post('weight');
				$ing_name = $this->input->post('ing_name');
				$ing_name_arabic = $this->input->post('ing_name_arabic');
				$unit_type = $this->input->post('unit_type');
				$price = $this->input->post('price');
				$Image = $this->input->post('Image');
				
				$file_details = array('upload_path'=> "./uploads/ingredients/", 'allowed_types' => "jpg|jpeg|gif|png|svg", 'max_size'=>"50000",'max_width'=>"50000", 'max_height'=>"50000", 'filename'=>"Image");
				$user_image = $this->uploadfile($file_details);
				
				$data = array(
						'ing_calories'			=> $calories,
						'ing_weight'	=> $weight,
						'ing_name' 	=> $ing_name,
						'ing_name_arabic'=>$ing_name_arabic,
						'unit_type'			=> $unit_type,
						'ing_price'	=> $price,
						'ing_image' 	=> $user_image,
						'created_at'	=> date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s'),
				);
				$result = $this->Main_model->__callInsert('sm_ingredients',$data);

				//update ing last update time in sm_update_timing
				$time_data=array(
					'ing_update_time'=>date('Y-m-d H:i:s'),
					'salad_update_time'=>date('Y-m-d H:i:s')
				);
				$time_where=array(
					'id'=>1
				);
				$result = $this->Main_model->__callUpdate('sm_update_timing',$time_where,$time_data);

				if($result){
					$subcat=array();
					extract($_POST);
					$ingid=$this->db->insert_id();
					if(isset($id)){
						$length=count($id);	
						for($i=0;$i<$length;$i++)
                    	{		
							//Qty Up-down 
							$qty_up=0;
							if(isset($qty_up_down[$id[$i]])){
								$qty_up=1;
							}

							//Ingredient weight in the category
							$sing_weight=0;
							if(isset($ing_weight[$id[$i]])){
								$sing_weight = $ing_weight[$id[$i]];
							}

							//Ingredient sub unit type in the category
							$sunit_type = NULL;
							if(isset($sub_unit_type[$id[$i]])){
								$sunit_type = $sub_unit_type[$id[$i]];
							}

							//Ingredient price in the category
							$sing_price = 0;
							if(isset($ing_price[$id[$i]])){
								$sing_price = $ing_price[$id[$i]];
							}

							//Ingredient calories in the category
							$sing_calories = 0;
							if(isset($ing_cal[$id[$i]])){
								$sing_calories = $ing_cal[$id[$i]];
							}

							$subcatdata=array(
								'ing_id'=>$ingid,
								'subcat_id'=>$id[$i],
								'qty_up_down'=>$qty_up,
								'sing_weight'=>$sing_weight,
								'sunit_type'=>$sunit_type,
								'sing_price'=>$sing_price,
								'sing_calories'=>$sing_calories,
							);
							$subcatadd = $this->Main_model->__callInsert('sm_ingredients_subcats',$subcatdata);
						}
						if($subcatadd){	
							$response['error'] = 0;
							$response['msg'] = 'Data inserted Successfully';
							$this->session->set_flashdata('success_msg', ' Ingredients inserted Successfully');
						}else{
							$response['error'] = 1;
							$response['msg'] = 'some things went wrong.';
						}
					}
					$response['error'] = 0;
					$response['msg'] = 'Data inserted Successfully';
					$this->session->set_flashdata('success_msg', ' Ingredients inserted Successfully');
					//$response['msg'] = $this->error_msg_html('Category added Successfully.',0);
				}else{
					$response['error'] = 1;
					$response['msg'] = 'somethings went rong.';
					//$response['msg'] = $this->error_msg_html('Some things went wrong.Please Try again..',0);
				}
				redirect(base_url().('ingredient/'));
			}
		}
		//call view file of add
		$sql = "select * from sm_categories where cat_status = 1";
    	$data['category']=$this->Main_model->__callMasterquery($sql);
		$data['unit_type']= unit_type();
        $data['page_title']='Ingredient';
        $this->load->view('add_ingredient', $data);
    }

	public function add_check_ingredient(){
		if(!empty($_POST['ing_id'])){
			$where=array(
				'ing_name'=>$_POST['ing_name'],
				'cat_id'=>$_POST['ing_type']
			);
			$this->db->select('ing_id');
			$this->db->from('sm_ingredients');
			$this->db->where($where);
			$this->db->where('cat_id != ',$_POST['ing_type'],FALSE);
			$query=$this->db->get();
			$if_exist=$query->row();
			if($if_exist){
				$data= false;
			}else{
				$data= true;
			}
		}else{
			$where=array(
				'ing_name'=>$_POST['ing_name'],
				'cat_id'=>$_POST['ing_type']
			);
			$this->db->select('ing_id');
			$this->db->from('sm_ingredients');
			$this->db->where($where);
			$query=$this->db->get();
			$if_exist=$query->row();
			if($if_exist){
				$data= false;
			}else{
				$data= true;
			}
		}


		echo json_encode($data);
	}

	
		// Uploading file
    public function uploadfile($file_details = array())
    {  
        $file_name = $file_details['filename']; 
        $ext = pathinfo($_FILES[$file_name]['name'], PATHINFO_EXTENSION);
        $config['upload_path'] = $file_details['upload_path'];
        $config['allowed_types'] = $file_details['allowed_types'];
        $config['max_size']  = $file_details['max_size'];    
        $config['max_width']  = $file_details['max_width'];
        $config['max_height']  = $file_details['max_height'];
        $config['file_name']= md5((time()*rand())).".".$ext;

        //$file_name = 'group_photo';

        $this->load->library('Upload',$config);             
        $this->upload->initialize($config);

		$upload = $this->upload->do_upload($file_name);
		
				
        if($_FILES[$file_name]['size'] != 0)
        {
            if($upload == NULL)
            {
                $this->session->set_flashdata('error_msg', $this->upload->display_errors());
                redirect($_SERVER['HTTP_REFERER']);
                //$response_data = array("status" => 0,"msg" => $this->upload->display_errors(),"url" => '');
            }
            else{
				$file_data = $this->upload->data();   
				$this->resizeImage($file_data['full_path']); 
                return $imgName = $file_data['file_name'];
            }
        }
	}
	public function resizeImage($filename)
   {
	  
		$this->load->library('image_lib');
		$resized_path = './uploads/thumbnail/ingredients';
    	 $config = array(
            'source_image' => $filename, //path to the uploaded image
            'new_image' => $resized_path,
            'maintain_ratio' => true,
            'width' => 100,
            'height' => 100
        );
		$image=$this->image_lib->initialize($config);
		$this->image_lib->resize();
      if (!$this->image_lib->resize()) {
		  echo $this->image_lib->display_errors();
	  }
	   
	
      $this->image_lib->clear();
   }
}