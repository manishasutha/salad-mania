<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery extends CI_Controller {
	public function __construct()
    {
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
			$this->load->helper('common_helper');
		$this->load->helper('string');
		$this->load->model('Main_model');
		$this->load->model('General_model');
    }


	//Update Order Status - Call Back URL for UTrack 
	public function updateOrderStatus()
	{
		echo encryptId(56);exit;
		$request = json_decode(file_get_contents("php://input"), true);
		if(!empty($request)){
			extract($request);
			$order_id = decryptId($integrator_number);

			//echo "Date: ".date('Y-m-d h:i:s', $timestamp);

			//First check order is exist or not with Integrator Number & Order Id
			$sql = "select order_id from sm_orders where order_id = $order_id AND delivery_id = '$utrac_id'";
		    $exist = $this->Main_model->__callMasterquery($sql);
		    //echo "\nExist: ". $exist->num_rows();exit;

		    if($exist->num_rows() > 0){
		    	$data = array(
					'delivery_status' => $status_code,
					'updated_at' => date('Y-m-d h:i:s'),
				);

		    	//Update the payment status in case of completed
				if($status_code == 4)
					$data['payment_status'] = 1;

				//If drvier info is received, then update it as well
				if(isset($driver_name) && $driver_name != '')
					$data['driver_name'] = ucwords(strtolower($driver_name));
				
				if(isset($driver_phone) && $driver_phone != '')
					$data['driver_phone'] = $driver_phone;

				$where = array('order_id' => $order_id);
		    	$result = $this->Main_model->__callUpdate('sm_orders',$where,$data);
		    	if($result){
		    		//Send email & SMS to user
					$this->db->select('o.*, u.phone, u.email');
					$this->db->from('sm_orders as o');
					$this->db->join('sm_users as u', 'u.id = o.user_id', 'LEFT');
					$this->db->where('order_id',$order_id);
					$query=$this->db->get();
					$message['order'] = $order = $query->row();
					if(!empty($order))
					{
						$msg = $this->load->view('email/order_template', $message, TRUE);
						$userEmail = $order->email;
						sendmail($userEmail,'','',$msg, 'Salad Mania - Order Status');

						//Send SMS Notification
						if($status_code == 1)
                            $msg = "Proudly, we have accepted your order.";
                        else if($status_code == 2)
                            $msg = "Proudly, we are preparing your food, and pickup is pending.";
                        else if($status_code == 3)
                            $msg = "Proudly, we have prepared your food and the driver has picked your order.";
                        else if($status_code == 4)
                            $msg = "Proudly, Your order has been delivered successfully.";
                        else if($status_code == 5)
                            $msg = "Your order has been Cancelled. We apologise for the inconvenience.";
		                
		                if(isset($msg))
		                	SendSms($msg,$order->phone);
		            }
		    	}
		    }
		}
		
	}

	private function callAPI($method, $url, $data){
		$curl = curl_init();

		switch ($method){
			case "POST":
				curl_setopt($curl, CURLOPT_POST, 1);
				if ($data)
					curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
				break;
			case "PUT":
				curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
				if ($data)
					curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
				break;
			default:
				if ($data)
					$url = sprintf("%s?%s", $url, http_build_query($data));
		}

		// OPTIONS:
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'X-Parse-Application-Id: 9803886e4a724d87bc56b44bd7512d3e2093bd95',
			'Content-Type: application/json',
		));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

		// EXECUTE:
		$result = curl_exec($curl);
		if(!$result){die("Connection Failure");}
		curl_close($curl);
		return $result;
	}
	

}




	