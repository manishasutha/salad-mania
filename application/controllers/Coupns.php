<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coupns extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
          $this->load->helper('form');
            $this->load->library('form_validation');
			
			$this->load->model('Main_model');
			// $this->load->model('notification_model');
			 $this->load->database();
        	$this->load->library('encryption');
        is_logged_in();
        if(!is_admin()){    
            redirect(base_url().('customer'));
            exit;
        }
    }
    
    //Add Salad Function
    public function add_coupns()
    {

        if(isset($_POST['submit'])){
            extract($_POST);
            $janamTarikh = explode("-", $start_date);
            $start_date = $janamTarikh[2].'-'.$janamTarikh[1].'-'.$janamTarikh[0];
            //end date convert into mysqli format

            $janamTarikh = explode("-", $end_date);
            $end_date = $janamTarikh[2].'-'.$janamTarikh[1].'-'.$janamTarikh[0];
           
            $data = array('offer_title' => $coupon_title,'offer_desc'=>$coupon_desc,'discount_rate'=>$discount_rate,'coupon_code'=>$coupon_code, 'start_date' => $start_date,'end_date'=>$end_date);        
            $last_id = $this->Main_model->__callInsert('sm_offers',$data);

            if($last_id){
               
                $this->session->set_flashdata('success_msg', 'Coupon has been successfully created.');
            }
            else
                $this->session->set_flashdata('error_msg', 'Something went wrong.');
            
            redirect(base_url().'coupons');
        }
    
        //Calling the Ingredients
        //$sql="select salad_id,salad_name from sm_salads";
        //$data['ingredients']=$this->Main_model->__callMasterquery($sql);

    	$data['page_title'] = 'Add Coupons';
		$data['page'] = 'coupons/add_coupn';
		$this->load->view('content',$data);
    }

    //View Salads Function
    public function list_coupns(){
        $data['page_title'] = 'Coupons List';
        $sql="select offer_id,offer_status,offer_title,offer_desc,coupon_code,discount_rate,start_date,end_date from sm_offers order by offer_id desc";
        $data['query'] = $this->Main_model->__callMasterquery($sql);
        $data['page'] = 'coupons/list_coupn';
		$this->load->view('content',$data);
    }

//enable disable salad
    public function delete($id){
			 $deliveryadd=array(
            'offer_status'=>0
            );
            $wheres = array('offer_id'=>$id);
            $data = $this->Main_model->__callUpdate('sm_offers',$wheres,$deliveryadd); 
	   		if($data){
	   			$this->session->set_flashdata('success_msg', 'Offers Disable Successfully');
	   			redirect(base_url()."coupons/");
	   		} 
		
    }
    
    //enable disable coupns
    public function enable($id){
			 $deliveryadd=array(
            'offer_status'=>1
            );
            $wheres = array('offer_id'=>$id);
            $data = $this->Main_model->__callUpdate('sm_offers',$wheres,$deliveryadd); 
	   		if($data){
	   			$this->session->set_flashdata('success_msg', 'Offers Enable Successfully');
	   			redirect(base_url()."coupons/");
	   		} 
		
    }
    
       //salad details
    public function edit($id){

        if(isset($_POST['submit']))
        {
            extract($_POST);
            $janamTarikh = explode("-", $start_date);
            $start_date = $janamTarikh[2].'-'.$janamTarikh[1].'-'.$janamTarikh[0];
            $janamTarikh = explode("-", $end_date);
            $end_date = $janamTarikh[2].'-'.$janamTarikh[1].'-'.$janamTarikh[0];

            $where = array('offer_id'=>$id);
            $data = array('offer_title' => $coupon_title,'offer_desc'=>$coupon_desc,'discount_rate'=>$discount_rate,'coupon_code'=>$coupon_code, 'start_date' => $start_date,'end_date'=>$end_date);        
            $update = $this->Main_model->__callUpdate('sm_offers',$where,$data);

            if($update){
                $this->session->set_flashdata('success_msg', 'Coupon has been successfully Updated.');
            }
            else
                $this->session->set_flashdata('error_msg', 'Sorry! Something went wrong.');
            
            redirect(base_url().'coupons');
        }


    	$sql="select offer_title,offer_desc,salads_ids,discount_rate,coupon_code,start_date,end_date from sm_offers where offer_id = $id";
        $data['offer']=$this->Main_model->__callMasterquery($sql);

        //Calling the Ingredients
        $sql="select salad_id,salad_name from sm_salads where salad_status =1";
        $data['salad']=$this->Main_model->__callMasterquery($sql);

    	$data['page_title'] = 'Edit Coupons';
		$data['page'] = 'coupons/edit_coupn';
		$this->load->view('content',$data);

    }
    // Uploading file
    public function uploadfile($file_details = array())
    {  
        $file_name = $file_details['filename']; 
        $ext = pathinfo($_FILES[$file_name]['name'], PATHINFO_EXTENSION);
        $config['upload_path'] = $file_details['upload_path'];
        $config['allowed_types'] = $file_details['allowed_types'];
        $config['max_size']  = $file_details['max_size'];    
        $config['max_width']  = $file_details['max_width'];
        $config['max_height']  = $file_details['max_height'];
        $config['file_name']= md5((time()*rand())).".".$ext;

        //$file_name = 'group_photo';

        $this->load->library('Upload',$config);             
        $this->upload->initialize($config);

        $upload = $this->upload->do_upload($file_name);
        if($_FILES[$file_name]['size'] != 0)
        {
            if($upload == NULL)
            {
                $this->session->set_flashdata('error_msg', $this->upload->display_errors());
                redirect($_SERVER['HTTP_REFERER']);
                //$response_data = array("status" => 0,"msg" => $this->upload->display_errors(),"url" => '');
            }
            else{
                $file_data = $this->upload->data();    
                return $imgName = $file_data['file_name'];
            }
        }
    }
}
