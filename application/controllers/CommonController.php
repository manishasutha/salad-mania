<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CommonController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('General_model');
        if (!($this->session->userdata('language'))) {
            $this->session->set_userdata('language', 'english');
        }
       $this->lang->load('content_form_' .$this->session->userdata('language'));
    }
    //get single row
    public function get_single_row(){
        $data= $this->General_model->get_row($_POST['table'],'',$_POST['where']);
        $response=array(
            'status'=>true,
            'data'=>$data
        );
        echo json_encode($response);
    }

    //to get particular row  from any table
    public function get_detail_ajax(){
        header("Content-type: application/json");
        $table = $this->input->post('table');
        $where = $this->input->post('where');

        $data = $this->General_model->get_listing_details($table,'',$where);
        $return_message = array(
            'status' => true,
            'data' => $data,
        );
        echo json_encode($return_message);
    }
    //to get joined row from  tables
    public function get_detail_ajax_join(){
        header("Content-type: application/json");
        $table = $this->input->post('table');
        $join_with_table = $this->input->post('join_with_table');
        $join_condition = $this->input->post('join_condition');
        $select_fields = $this->input->post('select_fields');
        $where = $this->input->post('where');

        $data = $this->General_model->get_join_listing_details($table,$join_with_table,$join_condition,$select_fields,$where);
        $return_message = array(
            'status' => true,
            'data' => $data,
        );
        echo json_encode($return_message);
    }
    //to get joined row from two tables
    public function get_row_with_join_two_tables(){
        header("Content-type: application/json");
        $table = $this->input->post('table');
        $join_with_table = $this->input->post('join_with_table');
        $join_condition = $this->input->post('join_condition');
        $select_fields = $this->input->post('select_fields');
        $where = $this->input->post('where');

        $data = $this->General_model->get_row_with_join_two_tables($table,$join_with_table,$join_condition,$select_fields,$where);
        $return_message = array(
            'status' => true,
            'data' => $data,
        );
        echo json_encode($return_message);
    }
    //get single field details
    public function get_single_field() {
        header("Content-type: application/json");
        $table = $this->input->post('table');
        $primary_key_name = $this->input->post('primary_key_name');
        $where = $this->input->post('where');
        $required_field = $this->input->post('required_field');

        $data = getSingleFieldDetail($primary_key_name,$where,$required_field,$table);
        $return_message = array(
            'status' => true,
            'data' => $data,
        );
        echo json_encode($return_message);
    }

    //to soft delete any row from any table
    public function soft_delete(){
        header("Content-type: application/json");
        $check=0;
        $table = $this->uri->segment(2);
        $id = $this->uri->segment(3);
        $where_condition = array('id' => $id);
        if($table == 'users'){
            $this->db->select('*');
            $this->db->from($table);
            $this->db->where($where_condition);
            $query=$this->db->get();
            $row = $query->row_array();
            if($row['user_type'] == 'driver'){
                $where_condition_services = array('driver_user_id' => $id);
                $this->db->select('*');
                $this->db->from('services');
                $this->db->where($where_condition_services);
                $query=$this->db->get();
                $row=$query->row_array();
                if($row){
                    $this->session->set_flashdata('response_message','This driver already exist in any service, So cannot delete this driver name');
                    $return_message = array(
                        'status' => true,
                    );
                    $check=1;
                }
            }
        }
        if($check==0) {
            $insert_detail = array('deleted_at' => date('Y-m-d H:i:s'));
            $this->General_model->general_updateDetails($table, $insert_detail, $where_condition);
            $this->session->set_flashdata('response_message', 'Deleted Successfully');
            $return_message = array(
                'status' => true,
            );
        }
        echo json_encode($return_message);
    }

    //to soft delete any row from any table(table name,field name,field value)
    public function soft_delete_by_field(){
        header("Content-type: application/json");

        $table = $this->uri->segment(2);
        $field_name = $this->uri->segment(3);
        $field_value = $this->uri->segment(4);
        $where_condition = array($field_name => $field_value);
        $insert_detail = array('deleted_at' => date('Y-m-d H:i:s'));
        $this->General_model->general_updateDetails($table,$insert_detail,$where_condition);
        $this->session->set_flashdata('response_message','Deleted Successfully');
        $return_message = array(
            'status' => true,
        );
        echo json_encode($return_message);
    }
    //to permanent-delete any row from any table
    public function permanent_delete(){
        header("Content-type: application/json");
        $table = $this->uri->segment(2);
        $id = $this->uri->segment(3);
        $where_condition = array('id' => $id);
        $this->db->delete($table, $where_condition);
        $this->session->set_flashdata('response_message',$this->lang->line('delete_success'));
        $return_message = array(
            'status' => true,
        );
        echo json_encode($return_message);
    }
    //to permanent-delete any row from any table by sending field name
    public function permanent_delete_by_field_name(){
        header("Content-type: application/json");
        $table = $this->uri->segment(2);
        $id = $this->uri->segment(3);
        $field_name = $this->uri->segment(4);
        $where_condition = array($field_name => $id);
        $this->db->delete($table, $where_condition);
        $this->session->set_flashdata('response_message',$this->lang->line('delete_success'));
        $return_message = array(
            'status' => true,
        );
        echo json_encode($return_message);
    }
    //to get joined row from  tables
    public function get_salad_details_for_home_page(){
        header("Content-type: application/json");
        //get data from javascript file
        $table = $this->input->post('table');
        $join_with_table = $this->input->post('join_with_table');
        $join_condition = $this->input->post('join_condition');
        $select_fields = $this->input->post('select_fields');
        $where = $this->input->post('where');
        $order_by = $this->input->post('order_by');
        $limit=$_POST['limit'];
        $cat_id=$_POST['cat_id'];
      
        //join data
        $salads = $this->General_model->get_join_listing_details($table,$join_with_table,$join_condition,$select_fields,$where,$order_by,$limit);
        //checking make your own true or false in the salad category
        $status=getSingleFieldDetail('cat_id',$cat_id,'contains_make_own','sm_categories');
        $loadHtml='';
        if($status ==1){

            $loadHtml.='<input type="hidden" value="" id="salad_0"><div class="col-md-6 " style="margin-bottom: 20px"> <div class="media"> <div class="media-img">';
            $loadHtml.=' <img onclick="add_to_cart('."'create_own',$cat_id,0,'Make Your Own'".')" class="media-object" style="cursor: pointer" src="'.INCLUDE_SALAD_IMAGE_PATH.'custom.png'.'" alt="img">';
            $loadHtml.='</div><div style="text-align: center" class="media-body  text-center">
            <h4 onclick="add_to_cart('."'create_own',$cat_id,0,'Make Your Own'".')" class="media-heading text-center" style="text-align: center!important; width: 100%;">'.$this->lang->line("make_own_title").'</h4>';
            $loadHtml.='  <h5 class="media-category text-center" style="text-align: center!important; width: 100%;">---</h5>    ';
            $loadHtml.= '<div class="mu-ingredients text-center" style="margin-top: 16px;text-align: center!important;"></div>';
            // $this->lang->line("make_now_text")
            $loadHtml.='   <a onclick="add_to_cart('."'create_own',$cat_id,0,'Make Your Own'".')" style="    float: none;" class="media-add text-center">'.$this->lang->line("make_now").'</a> </div></div></div>';
        }

        //if salad in this category then show
        if($salads){
            foreach($salads as $salad){
                $where=array(
                    'cat_id'=>$salad->cat_id
                );
                $category_name=getSingleFieldDetailWhere($where,'cat_name','sm_categories');

//                $ing_ids=explode(',',$salad->ing_ids);

               
                
                //getting ingrediant data witn linking of salad id

                $this->db->select('');
                $this->db->from('sm_ingredients as i');
                $this->db->join('sm_salad_ingredients as si','si.ing_id=i.ing_id');
                $this->db->where('si.salad_id',$salad->salad_id);
                $this->db->where('i.ing_status',1);
                $query=$this->db->get();
                $ing_data=$query->result();
                //total ingrediat name
                $ing_name=array_column($ing_data,'ing_name');
                //total ingrediant price
                $ing_price=array_sum(array_column($ing_data,'ing_price'));
                //total ingrediant calories
                $ing_calories=array_sum(array_column($ing_data,'ing_calories'));

                 //check language in session
                $language= $this->session->userdata('language');
                $salad_name=$salad->salad_name;
                $salad_desc=$salad->salad_desc;
                if($language=='Arabic'){
                    $salad_name=$salad->salad_name_arabic;
                    $salad_desc=$salad->salad_desc_arabic;
                    $category_name=getSingleFieldDetailWhere($where,'cat_name_arabic','sm_categories');
                }
                
                $loadHtml.='<input type="hidden" value="" id="salad_'.$salad->salad_id.'"><div class="col-md-6" style="margin-bottom: 20px"> <div class="media"> <div class="media-img">';
                $loadHtml.=' <img onclick="add_to_cart('."'ready_made',$cat_id,$salad->salad_id,'$salad_name'".')" class="media-object" style="cursor: pointer" src="'.INCLUDE_SALAD_IMAGE_PATH.$salad->salad_img.'" alt="img">';
                $loadHtml.='</div><div onclick="add_to_cart('."'ready_made',$cat_id,$salad->salad_id,'$salad_name'".')" class="media-body"><h4 class="media-heading">'.$salad_name.'</h4>';
                $loadHtml.='  <h5 class="media-category">'.$category_name.'</h5>    ';
                $loadHtml.= '<div class="mu-ingredients" style="margin-top: 16px">'.$salad_desc.'</div>';

                $loadHtml.='<span class="mu-menu-energy">'.$this->lang->line("cal") . $salad->salad_calories. '</span>&nbsp;|&nbsp;<span class="mu-menu-price">Qty: 1</span>  <span class="media-price">'.round($salad->salad_price,'1').' JD</span>';

                $loadHtml.='   <a onclick="add_to_cart('."'ready_made',$cat_id,$salad->salad_id,'$salad_name'".')" class="media-add">'.$this->lang->line("add").'</a> </div></div></div>';
            }
            $return_message = array(
                'status' => true,
                'data' => $loadHtml,
            );
            
        }else{
            $return_message = array(
                'status' => true,
                'data' => $loadHtml,
            );
        }
        echo json_encode($return_message);
    }

    public function check_email(){
        $where=array(
            'email'=>$_POST['reg_email'],
            'user_type'=>'U'
        );

        $if_exist=getSingleFieldDetailWhere($where,'email','sm_users');
        if($if_exist){
            $data=false;
        }else{
            $data=true;
        }
        echo json_encode($data);
    }
    public function check_mobile(){
        $where=array(
            'phone'=>$_POST['mobile'],
            'user_type'=>'U'
        );

        $if_exist=getSingleFieldDetailWhere($where,'phone','sm_users');
        if($if_exist){
            $data=false;
        }else{
            $data=true;
        }
        echo json_encode($data);
    }
    public function menu_load_data(){
        $table = $this->input->post('table');
        $select_fields = $this->input->post('select_fields');
        $where = $this->input->post('where');
        $order_by=$_POST['order_by'];
        $limit = $this->input->post('limit');
        $start = $this->input->post('start');
        $cat_id=$_POST['cat_id'];
        $loadHtml = '';
        $page = $this->input->post('page');
        
        $salads=$this->General_model->get_multiple_rows_stat_end_limit($table,$select_fields,$where,$order_by,$limit,$start);
        // if($page == 'menu'){
        //   //checking make your own true or false in the salad category
        //  $status=getSingleFieldDetail('cat_id',$cat_id,'contains_make_own','sm_categories');
        //     if($status ==1){ 
        //         $loadHtml .= '<input type="hidden" value="" id="salad_0"><div class="col-md-6"> <div class="media"> <div class="media-img">';
        //         $loadHtml .= '<img onclick="add_to_cart('."'create_own',$cat_id,0,'Make Your Own'".')" class="media-object" style="cursor: pointer" src="'.INCLUDE_SALAD_IMAGE_PATH.'custom.png'.'" alt="img">';
        //         $loadHtml .= '</div><div style="text-align: center" class="media-body  text-center">';
        //         $loadHtml .= '<h4 onclick="add_to_cart('."'create_own',$cat_id,0,'Make Your Own'".')" class="media-heading text-center" style="text-align: center!important; width: 100%;">Make Your Own</h4>';
        //         $loadHtml .= '<h5 class="media-category text-center" style="text-align: center!important; width: 100%;">---</h5>  ';
        //         $loadHtml .= '<div class="mu-ingredients text-center" style="margin-top: 16px;text-align: center!important;">'.$this->lang->line("make_now_text").'</div>';
        //         $loadHtml .= '<a onclick="add_to_cart('."'create_own',$cat_id,0,'Make Your Own'".')" style="    float: none;" class="media-add text-center">'.$this->lang->line("make_now") .' </a> </div></div></div>';
        //     } 
        // }
        if($salads->num_rows() > 0) {

                foreach ($salads->result() as $salad) {
                    $where = array(
                        'cat_id' => $salad->cat_id
                    );
                    $category_name = getSingleFieldDetailWhere($where, 'cat_name', 'sm_categories');
                     $contain_make_own = getSingleFieldDetailWhere($where, 'contains_make_own', 'sm_categories');


                    $this->db->select('');
                    $this->db->from('sm_ingredients as i');
                    $this->db->join('sm_salad_ingredients as si', 'si.ing_id=i.ing_id');
                    $this->db->where('si.salad_id', $salad->salad_id);
                    $this->db->where('i.ing_status',1);
                    $query = $this->db->get();
                    $ing_data = $query->result();
                    //total ingrediat name
                    $ing_name = array_column($ing_data, 'ing_name');
                    //total ingrediant price
                    $ing_price = array_sum(array_column($ing_data, 'ing_price'));
                    //total ingrediant calories
                    $ing_calories = array_sum(array_column($ing_data, 'ing_calories'));

                    //check language in session
                    $language= $this->session->userdata('language');
                    $salad_name=$salad->salad_name;
                    $salad_desc=$salad->salad_desc;
                    if($language=='Arabic'){
                        $salad_name=$salad->salad_name_arabic;
                        $salad_desc=$salad->salad_desc_arabic;
                        $category_name=getSingleFieldDetailWhere($where,'cat_name_arabic','sm_categories');
                    }
                    $loadHtml .= '<input type="hidden" value='.$contain_make_own.' id="cat_id">';
                    $loadHtml .= '<input type="hidden" value="" id="salad_' . $salad->salad_id . '"><div class="col-md-6" style="margin-bottom: 20px"> <div class="media"> <div class="media-img">';
                    $loadHtml .= ' <img onclick="add_to_cart(' . "'ready_made',$cat_id,$salad->salad_id,'$salad_name'" . ')" style="cursor: pointer" class="media-object" src="' . INCLUDE_SALAD_IMAGE_PATH . $salad->salad_img . '" alt="img">';
                    $loadHtml .= '</div><div class="media-body"><h4 onclick="add_to_cart(' . "'ready_made',$cat_id,$salad->salad_id,'$salad_name'" . ')" class="media-heading">' . $salad_name . '</h4>';
                    $loadHtml .= '  <h5 class="media-category">' . $category_name . '</h5>    ';
                    $loadHtml .= '<div class="mu-ingredients" style="margin-top: 16px">' . $salad_desc . '</div>';

                    $loadHtml .= '<span class="mu-menu-energy">' . $this->lang->line('cal'). $salad->salad_calories.'</span>&nbsp;|&nbsp;<span class="mu-menu-price">'.$this->lang->line('qty').': 1</span>  <span class="media-price">' .round($salad->salad_price,'1'). $this->lang->line('jd').'</span>';

                    $loadHtml .= '   <a onclick="add_to_cart(' . "'ready_made',$cat_id,$salad->salad_id,'$salad_name'" . ')" class="media-add">'.$this->lang->line('add').'</a> </div></div></div>';
                }
            }

//            foreach($salads->result() as $salad)
//            {
//                $where=array(
//                    'cat_id'=>$salad->cat_id
//                );
//                $category_name=getSingleFieldDetailWhere($where,'cat_name','sm_categories');
//
//                $ing_ids=explode(',',$salad->ing_ids);
//
//                $this->db->select('ing_name');
//                $this->db->from('sm_ingredients');
//                $this->db->where_in('ing_id',$ing_ids);
//                $query=$this->db->get();
//                $ing_names=$query->result();
//                $ing_name=array_column($ing_names,'ing_name');
//
//
//
//
//                $ingrdients=$this->General_model->get_multipel_data('sm_ingredients','ing_calories','ing_id',$ing_ids);
//                $totalingrdients=0;
//                foreach ($ingrdients as $ingrdient){
//                    $totalingrdients=$totalingrdients+$ingrdient->ing_calories;
//                }
//                $loadHtml.='<input type="hidden" value="'.$salad->ing_ids.'" id="salad_'.$salad->salad_id.'"><div class="col-md-6"> <div class="media"> <div class="media-img">';
//                $loadHtml.=' <img class="media-object" src="'.INCLUDE_SALAD_IMAGE_PATH.$salad->salad_img.'" alt="img">';
//                $loadHtml.='</div><div class="media-body"><h4 class="media-heading">'.$salad->salad_name.'</h4>';
//                $loadHtml.='  <h5 class="media-category">'.$category_name.'</h5>    ';
//                $loadHtml.= '<div class="mu-ingredients" style="margin-top: 16px">'.implode(', ',$ing_name).'</div>';
//
//                $loadHtml.='<span class="mu-menu-energy">'.$totalingrdients.'Cal</span>&nbsp;|&nbsp;<span class="mu-menu-price">Qty: 1</span>  <span class="media-price">'.$salad->salad_price.'JD</span>';
//
//                $loadHtml.='   <a onclick="add_to_cart('."'salad',$salad->salad_id,'$salad->salad_name'".')" class="media-add">ADD</a> </div></div></div>';
//            }
//        }
        echo $loadHtml;
        
    }

    public function custom_load_data(){
        $table = $this->input->post('table');
        $join_with_table = $this->input->post('join_with_table');
        $join_condition = $this->input->post('join_condition');
        $select_fields = $this->input->post('select_fields');
        $where = $this->input->post('where');
        $limit = $this->input->post('limit');
        $start = $this->input->post('start');
        $loadHtml = '';
        $salads = $this->General_model->get_join_listing_load_details($start,$limit,$table,$join_with_table,$join_condition,$select_fields,$where);

        if($salads->num_rows() > 0)
        {
            foreach($salads->result() as $ing)
            {
                
                $where=array(
                    'cat_id'=>$ing->cat_id
                );
                $category_name=getSingleFieldDetailWhere($where,'cat_name','sm_categories');
                $ing_name=$ing->ing_name;
                $language= $this->session->userdata('language');
                if($language=='Arabic'){
                    $category_name=getSingleFieldDetailWhere($where,'cat_name_arabic','sm_categories');
                    $ing_name=$ing->ing_name_arabic;
                }

                $loadHtml.='<input type="hidden" value="'.$ing->ing_id.'" id="ing_'.$ing->ing_id.'"><div class="col-md-6"> <div class="media"> <div class="media-img">';
                $loadHtml.=' <img onclick="add_to_cart('."'ing',$ing->ing_id,''".')" class="media-object" src="'.INCLUDE_ING_IMAGE_PATH.$ing->ing_image.'" alt="img">';
                $loadHtml.='</div><div class="media-body"><h4 style="cursor: pointer" onclick="add_to_cart('."'ing',$ing->ing_id,''".')" class="media-heading">'.$ing_name.'</h4>';
                $loadHtml.='  <h5 class="media-category">'.$category_name.'</h5>    ';
                $loadHtml.= '<div class="mu-ingredients" style="margin-top: 16px"></div>';
                $loadHtml.='<span class="mu-menu-energy">'.$ing->salad_calories.'Cal</span>&nbsp;|&nbsp;<span class="mu-menu-price">'.$this->lang->line('qty').': 1</span>  <span class="media-price">'.round($ing->salad_price,'1').$this->lang->line('jd').'</span>';
                $loadHtml.='   <a onclick="add_to_cart('."'ing',$ing->ing_id,''".')" class="media-add">'.$this->lang->line('add').'</a> </div></div></div>';
            }
        }
        echo $loadHtml;
    }

     public function menu_create_own(){
        $loadHtml = '';
                $loadHtml .= '<input type="hidden" value="" id="salad_0"><div class="col-md-6"> <div class="media"> <div class="media-img">';
                $loadHtml .= '<img onclick="add_to_cart('."'create_own',$cat_id,0,'Make Your Own'".')" class="media-object" style="cursor: pointer" src="'.INCLUDE_SALAD_IMAGE_PATH.'custom.png'.'" alt="img">';
                $loadHtml .= '</div><div style="text-align: center" class="media-body  text-center">';
                $loadHtml .= '<h4 onclick="add_to_cart('."'create_own',$cat_id,0,'Make Your Own'".')" class="media-heading text-center" style="text-align: center!important; width: 100%;">'.$this->lang->line("make_own_title") .'</h4>';
                $loadHtml .= '<h5 class="media-category text-center" style="text-align: center!important; width: 100%;">---</h5>  ';
                $loadHtml .= '<div class="mu-ingredients text-center" style="margin-top: 16px;text-align: center!important;">'.$this->lang->line("make_now_text").'</div>';
                $loadHtml .= '<a onclick="add_to_cart('."'create_own',$cat_id,0,'Make Your Own'".')" style="    float: none;" class="media-add text-center">'.$this->lang->line("make_now") .' </a> </div></div></div>';
        echo $loadHtml;
        
    }
    public function get_model_data(){
        $type=$_POST['type'];
        $salad_name=$_POST['name'];
        $cat_id=$_POST['cat_id'];
        $loadHtml='';
        $data=false;
        if($type =='ready_made'){
            $table_name='sm_subcategories';
            $select_filds='';
            $where_condition=array(
                'parent_cat_id'=>$cat_id,
                'salad_type'=>1,
                'subcat_status'=>1,
            );
            $sub_categories=$this->General_model->get_multiple_row($table_name,$select_filds,$where_condition);

            $sub_cat_id=array_column($sub_categories,'id');

            $this->db->select('');
            $this->db->from('sm_ingredients_subcats as is');
            $this->db->join('sm_ingredients i','i.ing_id=is.ing_id');
            $this->db->where_in('is.subcat_id',$sub_cat_id);
            $this->db->where('i.ing_status',1);
            $query=$this->db->get();
            $subcat_ingredients=$query->result();

            $this->db->select('');
            $this->db->from('sm_salad_ingredients as si');
            $this->db->join('sm_ingredients as i','i.ing_id=si.ing_id');
            $this->db->where('si.salad_id',$_POST['salad_id']);
            $query=$this->db->get();
            $salad_ing=$query->result();
           // $total_price=array_sum(array_column($salad_ing,'ing_price'));

            $this->db->select('');
            $this->db->from('sm_salads');
            $this->db->where('salad_id',$_POST['salad_id']);
            $query=$this->db->get();
            $salad_data=$query->row();

            $salad_name=$salad_data->salad_name;
            $language= $this->session->userdata('language');
            if($language=='Arabic'){
                $salad_name=$salad_data->salad_name_arabic;

            }
            $total_price=$salad_data->salad_price;
            $total_calories=$salad_data->salad_calories;

            if($subcat_ingredients){
                $data=true;
            }
        }elseif ($type =='create_own'){
            $table_name='sm_subcategories';
            $select_filds='';
            $where_condition=array(
                'parent_cat_id'=>$cat_id,
                'salad_type'=>2,
                'subcat_status'=>1,
            );
            $sub_categories=$this->General_model->get_multiple_row($table_name,$select_filds,$where_condition);

            $sub_cat_id=array_column($sub_categories,'id');

            $this->db->select('');
            $this->db->from('sm_ingredients_subcats as is');
            $this->db->join('sm_ingredients i','i.ing_id=is.ing_id');
            $this->db->where_in('is.subcat_id',$sub_cat_id);
            $this->db->where('i.ing_status',1);
            $query=$this->db->get();
            $subcat_ingredients=$query->result();
            
            $total_price=0;
            $total_calories=0;
            if($subcat_ingredients) {
                $data = true;
            }
        }

        if($data === true){


            $loadHtml.='<div class="modal-content" id="salad_data" salad_id="'.$_POST['salad_id'].'" salad_type="'.$_POST['type'].'" cat_id="'.$_POST['cat_id'].'">';
            $loadHtml.='<form id="saladName" method="post" action="javascript:void(0);">';
            $loadHtml.='<div class="form" >';
            $loadHtml.='<h4 class="modal-title" id="myModalLabel">';
            $loadHtml.='<div class="row" style="margin-bottom: 5px;" >';
            if($type === 'ready_made') {
                $loadHtml.='<div class="col-md-7 col-sm-6  border-bottom" ><p id="salad-textbox" style=" white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"  class="form-control-edit-name2"  name="custome-salad">'.$salad_name.'</p> </div>';
            }else{
                $loadHtml.='<div class="col-md-5 col-sm-4 col-xs-9 border-bottom" ><input type="text" id="salad-textbox" maxlength="50" onclick="addsave()" placeholder="'.$this->lang->line('enter_recipe_name').'"  class="form-control-edit-name" readonly name="custome-salad"  >
                <span id="salad_name_error" class="text text-danger"></span></div>';
            }

            if($type ==='create_own'){
                $loadHtml.='<div id="name_button" class="col-md-2 col-sm-2 col-xs-3">';
                $loadHtml.='<button  id="savebtn" onclick="addname()" style="display: none" class="btn btn-primary">'.$this->lang->line('save').'</button>';
                $loadHtml.='</div>';
            }

            $loadHtml.='<div class=" col-xs-12 margin-top">';
            $loadHtml.='<div class="bi-qty  bi-qty-top" style="float: right">';

            $loadHtml.=$this->lang->line('qty').':<button type="button" style="margin-left: 10px"  id="sub" class="sub">-</button>';
            $loadHtml.='<input type="text" readonly class="edit_qty" id="ql" value="1" min="1" max="9" onkeypress="return isNumber(event)" onchange="calculate_price('.$total_price.')"  /> <button type="button" id="add" class="add">+</button> &nbsp; <span style="color:#ccc; font-size: 22px"> |</span> &nbsp;';


            $loadHtml.='<span id="total_price"  fixed_price="'.$total_price.'" >'.round($total_price,'1').'  </span> '.$this->lang->line('jd').' &nbsp;<span style="color:#ccc; font-size: 22px">| </span>&nbsp;'.$this->lang->line('cal').' <span id="total_calories" fixed_calories="'.$total_calories.'"> '.$total_calories.'</span> &nbsp;&nbsp;&nbsp;';
            $loadHtml.='</div>';
            $loadHtml.='</div>';
            $loadHtml.='</h4>';
            $loadHtml.='</div>';
            $loadHtml.='  <div class="modal-body" >';
            $loadHtml.='<div class="row" style="margin: 0">';
            $loadHtml.='<div class="tab" style="overflow-y: auto; height:400px">';

            $break=1;
            foreach ($sub_categories as $cat){
                //check language in session
                $language= $this->session->userdata('language');
                $subcat_name= $cat->subcat_name;
                if($language=='Arabic'){
                    $subcat_name= $cat->subcat_name_arabic;
                }
                if($break ==1) {
                    $where=array(
                        'id'=>$cat->id,
                        'subcat_status'=>1,
                    );
                    $loadHtml .= '<button class="tablinks active" onclick="openCity(event,' . "'cat_$cat->id'" . ')" >' . $subcat_name . ' &nbsp;' . '<span id="count_'.$cat->id.'">' . 0 . '</span>' . '/' . '<span id="max_limit_'.$cat->id.'">' .getSingleFieldDetailWhere($where,'max_limit','sm_subcategories'). '</span>' . '</button>';
                    $break++;
                    
                }else{
                    $where=array(
                        'id'=>$cat->id,
                        'subcat_status'=>1,
                    );
                    $loadHtml .= '<button class="tablinks" onclick="openCity(event,' . "'cat_$cat->id'" . ')" id="tab_'.$cat->id.'">' . $subcat_name .' &nbsp;' . '<span id="count_'.$cat->id.'">' . 0 . '</span>' . '/' . '<span id="max_limit_'.$cat->id.'">' .getSingleFieldDetailWhere($where,'max_limit','sm_subcategories'). '</span>' . '</button>';;
                }

            }
            $loadHtml.='<button class="tablinks" onclick="openCity(event,'."'req_bos'".')" id="defaultOpen">'.$this->lang->line('special_request').'</button>';
            $loadHtml.=' </div>';
            $break=1;
            foreach ($sub_categories as $cat){
                $where=array(
                    'id'=>$cat->id,
                    'subcat_status'=>1,
                );
                $loadHtml.='<input type="hidden" name="sub_cat_'.$cat->id.'" value="'.getSingleFieldDetailWhere($where,'max_limit','sm_subcategories').'"  />';
               
                if($break == 1) {
                    $break++;
                    $loadHtml.='<div id="cat_'.$cat->id.'" class="tabcontent"  style="overflow-y: auto; display:block" >';
                    
                }else{
                    $loadHtml.='<div id="cat_'.$cat->id.'" class="tabcontent"  style="overflow-y: auto;" >';
                }


                $loadHtml.='<h5>'.$this->lang->line('ingredients').'</h5>
                 <span id="salad_price_error" class="text text-danger"></span>';
                foreach ($subcat_ingredients as $ing){
                    if($ing->subcat_id === $cat->id){
                        $ing_name=$ing->ing_name;
                        $language= $this->session->userdata('language');
                        if($language=='Arabic'){
                            $ing_name=$ing->ing_name_arabic;

                        }

                        $loadHtml.=' <div class="item-selection" id="ing_row_'.$ing->ing_id.'">';
                        $loadHtml.='<input type="hidden" id="catTotalItems" value="0">';
                        $loadHtml.=' <label class="selection" >';
                        $loadHtml.='<img width="70px" class="salad-img-cart" src="'.base_url().'uploads/thumbnail/ingredients/'.$ing->ing_image.'" height="48"/>';
                        $loadHtml.=' <p class="salad-name-cart">'.$ing_name.'</p>';
                        $loadHtml.=' <p class="salad-qty-cart"><span id="price-'.$ing->ing_id.'-'.$cat->id.'">'.round($ing->sing_price,'1').'</span>'.$this->lang->line('jd').'<span class="remove-cal">| </span><span class="remove-cal"> '.$this->lang->line('cal').' </span> <span id="calories-'.$ing->ing_id.'-'.$cat->id.'" class="remove-cal">'.$ing->sing_calories.' </span> </p>';
                        $loadHtml.='<input class="checked" onClick="Manuplation_model('."$ing->ing_id".','."$cat->id".')" type="checkbox" value="'.$ing->ing_id.'-'.$cat->id.'" id="ing_id-'.$ing->ing_id.'-'.$cat->id.'" />';
                        $loadHtml.='<span class="checkmark"></span>';
                        $loadHtml.='<input type="hidden" id="qty-'.$ing->ing_id.'-'.$cat->id.'" value="1" />';
                        $loadHtml.='</label>';
                        if($ing->qty_up_down  == 1){
                            $loadHtml.=' <div class="bi-qty " style="float: right">';
                            $loadHtml.=' <p>'.$this->lang->line('qty').':</p>';
                            $loadHtml.='<div class="bt-qty-pick">';
                            $loadHtml.=' <button type="button" id="sub" class="ing_sub">-</button>';
                            $loadHtml.= ' <input  value="1" min="1" max="9" class="ingQty" name="ing_id[]" type="text" onkeypress="return isNumber(event)" id="qty_'.$ing->ing_id.'-'.$cat->id.'" data-id="'.$cat->id.'" value="'.$ing->ing_id.'-'.$cat->id.'" readonly>';
                            $loadHtml.=' <button type="button" id="add" class="ing_add">+</button>';
                            $loadHtml.=' </div>';
                            $loadHtml.=' </div>';
                        }

                        $loadHtml.='</div>';
                    }

                }
                $loadHtml.='</div>';
                $loadHtml.='<div id="req_bos" class="tabcontent">';
                $loadHtml.='  <h5>'.$this->lang->line('special_request').'</h5>';
                $loadHtml.='<textarea id="special_request" name="special_request" class="form-control"></textarea>';
                $loadHtml.='</div>';

            }
            $loadHtml.='</div>';
            $loadHtml.='<div class="modal-footer">';
            $loadHtml.='<button type="button" class="btn btn-default" data-dismiss="modal">'.$this->lang->line('cancel').'</button>';
            $loadHtml.='<button type="button" onclick="AddToBag()" class="btn btn-primary"><i class="fa fa-shopping-bag"></i> '.$this->lang->line('add_bag').' </button>';
            $loadHtml.='</div>';
            $loadHtml.='</div>';
            $loadHtml.='</form>';
            $loadHtml.='</div>';

            $response=array(
                'status'=>true,
                'data'=>$loadHtml
            );
        }else{
            $response=array(
                'status'=>false
            );
        }
        echo json_encode($response);
    }
}
?>
