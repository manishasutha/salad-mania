<?php include "include/header.php"; ?>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include "include/navbar.php"; ?>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include "include/leftsidebar.php" ?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			 
			<!--  MAIN CONTENT -->
			<div class="main-content">
				<?php
					if($this->session->flashdata('success_msg')){
					$msg = $this->session->flashdata('success_msg');
						echo '<div class="alert alert-success fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Success!</strong>'. $msg.'
			        		</div>';
		        	}
				?>
				<div class="container-fluid">
					
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Add User </h3>
									
								</div>
								<div class="panel-body no-padding">
								<form enctype="multipart/form-data" class="col-lg-12" id="form" action="<?=base_url()?>users/add" method="post">
			<div class="" role="document">
		        <div class="">
		            
		            <div class="row">
		                <div class="col-md-6 md-form mb-3 ">
		                	<label data-error="wrong" data-success="right" for="cat_edit_name">User Name</label>
		                	<br><span id="name_error"></span>
		                    <input type="text" id="user_name" name="user_name" class="form-control validate user_name">
		   
		                </div>
		                <div class="col-md-6 md-form mb-3 ">
		                	<label data-error="wrong" data-success="right" for="cat_edit_name">User Email</label>
		                	<br><span id="email_error"></span>
		                    <input type="text" id="email" name="email" class="form-control email validate">
		                </div>
						
						<div class="col-md-6 md-form mb-3" style="margin-top:20px;">
		                	<label data-error="wrong" data-success="right" for="cat_edit_name">User Phone</label>
		                	<br><span id="phone_error"></span>
		                    <input type="text" id="phone" name="phone" class="form-control validate">
		                </div>
						
		            </div>
		            <div class="d-flex button justify-content-center" style="margin-top:20px;">
		                  <button class="btn-primary add-ing-button" id="button" name="submit" type="submit">Submit</button>
 							<button class="btn-primary add-ing-button" id="button" name="submit" type="reset"><a style="color:white" href="<?=base_url()?>users/add">Cancle</a></button>

				    </div>
		        </div>
		    </div>
			</form>
								</div>
								<div class="panel-footer">
									<div class="row">
	                                    <div class=" text-left">
	                                        
	                                    </div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
                        <?php include"include/footer_content.php" ?>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		
	</div>
	
	<?php include"include/footer.php"; ?>
		<script>
		$(document).ready(function(){
		
		$(".close").click(function(){
			$("#editCatModal").hide();
			});
			$('form[id="form"]').validate({
            rules: {
                user_name: {
                    required : true,
                    minlength: 2,
                    maxlength: 30,
                    letterswithspace: true,
                },
                email: 
                {  
                    	required:true,
						minlength: 2,
						maxlength: 50,
						 email: true,
        				regex: /^\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i,
				},
				 phone: 
                {  
                    	required:true,
						minlength: 5,
						maxlength: 11,
						number: true,
				},
			
            },
            messages: {
                //salad_name: "Please enter salad name",
                cat_edit_type: "Please select Category",
                 phone: {
                    required: "Please enter phone no",
                    number: "Phone no should be in digits only",
					maxlength:"please enter no less than 11"
				},
				email: {         
					email: "Please enter a valid email address",
				},
            },
            submitHandler: function(form) {
                //Here we can any custom work like AJAX calling

                if(false){ } 
                else {
                form.submit();
                }
            }
        });

        $.validator.addMethod("letterswithspace", function(value, element) {
            return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
        }, "Please enter only alphabets");
        $.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z]+$/i.test(value);
        }, "Please enter only alphabets");
         $.validator.addMethod("date", function ( value, element ) {
                var bits = value.match( /([0-9]+)/gi ), str;
                if ( ! bits )
                    return this.optional(element) || false;
                str = bits[ 1 ] + '/' + bits[ 0 ] + '/' + bits[ 2 ];
                return this.optional(element) || !/Invalid|NaN/.test(new Date( str ));
            },
            "Please enter a date in the format dd/mm/yyyy"
        );
        jQuery.validator.addMethod("alphanumeric", function(value, element) {
            return this.optional(element) || /^[\w.]+$/i.test(value);
        }, "Letters, numbers, and underscores only please");
	});
		
		</script>