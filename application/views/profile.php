<?php include"include/header.php" ?>

	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include"include/navbar.php" ?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include"include/leftsidebar.php" ?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                      <div class="subheader">
                        <ul>
                            <li><?=$this->lang->line('Profile')?$this->lang->line('Profile'):'Profile'?> </li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
                <div id="custom_error">
                    
                </div>
                <?php
                    if($this->session->flashdata('success_msg')){
                    $msg = $this->session->flashdata('success_msg');
                        echo '<div class="alert alert-success fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong>'. $msg.'
                            </div>';
                    }
                ?>
				<div class="container-fluid">
					
					<!-- END OVERVIEW -->
					<div class="row" >
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
                            <form class="col-md-10 form-panel" style="border:0" method="post" action="<?= base_url()?>profile/updateProfile" id="profileUpdateFrm" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="panel new-panel">
                                            <div class="panel-heading">
                                                <div class="col-md-12">
                                                    <h3 class="panel-title"><?=$this->lang->line('my_profile')?$this->lang->line('my_profile'):'My Profile'?> </h3>
                                                </div>
                                                <br>
                                            </div>
                                      
                                            <div class="panel-body no-padding">
                                                <div class="col-md-4">
                                                    <label><?=$this->lang->line('name')?$this->lang->line('name'):'Name'?> </label>
                                                    <input class="form-control" value="<?= $user[0]['first_name'] ? $user[0]['first_name']:'' ?>" type="text" id="name" name="name">
                                                    <br>
                                                </div>
                                                <div class="col-md-4">
                                                    <label><?=$this->lang->line('Surname')?$this->lang->line('Surname'):'Surname'?> </label>
                                                    <input class="form-control" value="<?= $user[0]['last_name'] ? $user[0]['last_name']:'' ?>" type="text" id="surname" name="surname">
                                                    <br>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-md-4">
                                                     <label><?=$this->lang->line('Change Picture')?$this->lang->line('Change Picture'):'Change Picture'?> </label>
                                                    <input  type="file" name="image" id="image">
                                                    <input  type="hidden" value="<?= $user[0]['profile_image'] ? $user[0]['profile_image']:'' ?>" name="old_image" id="old_image">
                                                    <br>
                                                </div>
                                                <div class="col-md-5">
                                                    <img src="<?= base_url() ?><?= $user[0]['profile_image'] ? $user[0]['profile_image']:'assets/img/user-medium.png' ?>" height="90" style="border-radius: 4px" id="profileImage"/>
                                                </div>
                                                <div class="clearfix"></div>
                                                <hr>
                                                <div class="col-md-4">
                                                    <label><?=$this->lang->line('email')?$this->lang->line('email'):'Email'?> </label>
                                                    <input class="form-control" value="<?= $user[0]['email_address'] ? $user[0]['email_address']:'' ?>" type="text" id="email" name="email" disable>
                                                    <br>
                                                </div>
                                                <div class="col-md-3">
                                                    <label><?=$this->lang->line('Phone')?$this->lang->line('Phone'):'Phone'?> </label>
                                                    <input class="form-control" value="<?= $user[0]['contact_number'] ? $user[0]['contact_number']:'' ?>"  type="text" id="phone" name="phone">
                                                    <br>
                                                </div>
                                               
                                               <div class="clearfix"></div>                                                                                          
                                            </div>
                                            <div class="panel-footer">
                                                    <div class="row">
                                                        <div class="col-md-12 text-right"><!-- <a href="ticket_add.html" class="btn btn-primary" id="profileUpdateFrm"> Update </a> -->
                                                        <button type="submit" class="btn btn-primary " id="profileUpdateBtn"><?=$this->lang->line('Update')?$this->lang->line('Update'):'Update'?> </button>
                                                        </div>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                            </form>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
                          <?php include"include/footer_content.php" ?>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		
	</div>
	<!-- END WRAPPER -->
	<?php include"include/footer.php" ?>
