<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title>Beeoveg</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- ICONS -->
	
</head>

<body style="background: #f1f3f4; font-family: 'Arial', 'Calibri'; padding: 20px; font-size: 14px">
	<div class="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
                            <div class="logo_outer" style="text-align: center; margin-bottom: 20px"><img src="<?=base_url()?>assets/email/logo_outer.png" alt="logo_image"/></div>
				<div class="email_mn_bx effect6" style="width: 768px;text-align: center;margin: 0 auto;border-radius: 8px;background-color: #fff; box-shadow: 0 8px 12px -6px black;">
					<div class="header">
					<!--<div class="logo text-center"></div>-->
						<div class="main_1" style="padding: 34px 0px;">
							<p class="lead_1" style="margin-bottom: 20px;
			font-size: 16px;
			font-weight: 700;">Congratulations !</p>
							<div class="content_mn" style="padding: 0px 60px;">
                                                            <p class="cont_1">
                                                                Thanks for signing up for <b>BeeOVeg</b>. You have been registered to Bee-O-Veg <span></span>
                                                            </p>
                                                            <?php if($type == 'email' || $type == 'Email'){ ?>
                                                            <p>Your credentials are</p>
                                                            <div style="border:1px dashed #ffb200; background: rgba(255,178,0,0.2); padding: 15px ;margin-bottom: 32px">
                                                                Username &nbsp;<b><?=$user_name?></b> <br>
                                                                Password &nbsp;<b><?=$user_password?></b>
                                                            </div>
                                 <a href="<?=$activation_link?>" class="btn btn-lg submit_bt" style="color: #fff;    box-shadow: 0px 1px 2px #000;
			background-color: #ff8209;
			border-color: #ffb200; border-radius: 4px;padding: 10px 60px;  margin-top: 20px; margin-bottom: 20px">Confirm Registration</a>
                                                                <p class="cont_1">
                                                                or copy the raw link into browser
                                                            </p>
                                                            <a href='<?=$activation_link?>'><?=$activation_link?></a>
                                                       
                                                                <p class="cont_1">
                                                                      <br/>
                                                                If you didn't sign up, you can ignore this email.
                                                            </p>
                                                             <?php } ?>
                                                              <p class="cont_2">
                                                                Best Wishes <br/>
                                                                
                                                            </p>
							</div>
						</div>
					</div>
				</div>
				<div class="bottom" style="text-align:center; margin-top: 20px">
					
					<p class="st_2" style="font-size: 13px">Bee-O-Veg Team</p>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

</html>
