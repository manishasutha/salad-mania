<?php include "include/header.php"; ?>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include "include/navbar.php"; ?>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include "include/leftsidebar.php" ?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
            <div class="subheader">
                <ul>
                    <li> <?=$this->lang->line('admin')?$this->lang->line('admin'):'Admin';?> / Users List</li>
                </ul>
                <div class="right" style="float: right">
                    <a  id="adduser"  class="btn add btn-primary button-fix"> Add User</a>
                </div>
            </div>
			<!--  MAIN CONTENT -->
			<div class="main-content">
				<?php
					if($this->session->flashdata('success_msg')){
					$msg = $this->session->flashdata('success_msg');
						echo '<div class="alert alert-success fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Success!</strong>'. $msg.'
			        		</div>';
		        	}
		        	if($this->session->flashdata('error_msg')){
					$msg = $this->session->flashdata('error_msg');
						echo '<div class="alert alert-danger fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Error!</strong>'. $msg.'
			        		</div>';
		        	}
				?>
				<div class="container-fluid">
					
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading2">
									<h3 class="panel-title2">Users List</h3>

								</div>
								<div class="panel-body no-padding">
								<div class="table-responsive">
									<table class="table table-striped datatable">
										<thead>
											<tr>
												<th>S.No</th>
												<th>Name</th>
												<th>Email</th>
												<th>Phone</th>
												<th>status</th>
												 <th>Registerd On</th>
												<th>Action</th>
												
											</tr>
										</thead>
										<tbody>
												<?php
												if(!empty($query) && $query->num_rows() > 0){
													$enable = $this->lang->line('enable')?$this->lang->line('enable'):'Enable';
													$disable = $this->lang->line('disable')?$this->lang->line('disable'):'Disable';
													
													foreach ($query->result() as $user) {
														if($user->user_status){
															$status_span = '<span class="label label-success">'.$enable.'</span>';
														}else{
															$status_span = '<span class="label label-danger">'.$disable.'</span>';
														}?>
													<tr>
															<th>ID</th>
															 <td><?php echo $user->full_name;?></td>
															 <td><?php echo $user->email;?></td>
															<td><?php echo $user->phone;?></td>
															 <td><a><?php echo $status_span ?></a></td>
															
															   <td><?php echo date("d-m-Y", strtotime($user->created_at)) ;?></td>
															 <td>
                                                			 	<a  title="Edit User!" class="btn btn-xs btn-primary edit" id="edituser" data-cat-id="<?php echo $user->id?>"><i class="fa fa-pencil"></i>
                                                			 	</a>
																 <a  title="View User!" class="btn btn-xs btn-info" href="<?=base_url()?>users/details/<?=$user->id;?>"  data-cat-id="<?php echo $user->id?>"><i class="fa fa-info-circle" ></i>
                                                			 	</a>
                                                			 	<?php if($user->user_status==1){
																				?>
																					<a  title="Disable User!" class="btn btn-xs btn-danger deleteItem" href="<?=base_url()?>users/delete/<?=$user->id;?>">
																							<i class='fa fa-eye-slash'></i>
																						</a>
																				<?php }
																				else{
																				?>
																					 <a style=""  title="Enable User!" class="btn btn-xs btn-info enableItem" href="<?=base_url()?>users/enable/<?=$user->id;?>">
                                                			 		<i  class="fa fa-eye"></i>
                                                			 	</a>
																				<?php } ?>
																				
																				
																				
                                                			 </td>
                                                			
                                                			 </tr>
															
												<?php	}
												}
												?>									
										</tbody>
									</table>
								</div>
								</div>
								<div class="panel-footer">
									<div class="row">
	                                    <div class=" text-left">
	                                        
	                                    </div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
                        <?php include"include/footer_content.php" ?>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		
	</div>
	<!-- END WRAPPER -->
	<!-- Edit category model start added by ravindra -->
	
		<div class="modal" id="editCatModal"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <form enctype="multipart/form-data" id="userform" action="<?=base_url()?>users/add" method="post">
			<div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header text-left">
		                <h4 class="modal-title w-100 font-weight-bold"><?=$this->lang->line('Edit Category')?$this->lang->line('Edit Category'):'Edit User'?> </h4>
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -27px;opacity: 0.6;">
		                   &times;
		                </button>
		            </div>
		            <div class="modal-body mx-2">
		                <div class="md-form mb-3 ">
		                	<label data-error="wrong" data-success="right" for="cat_edit_name">User Name</label>
		                	<br><span id="name_error"></span>
		                    <input  type="text" id="name" name="name" class="form-control validate">
		                    <input type="hidden" name="id" id="cat_edit_id">
		                </div>
		                <div class="md-form mb-3 ">
		                	<label data-error="wrong" data-success="right" for="cat_edit_name">User Email</label>
		                	<br><span id="email_error"></span>
		                    <input type="text" id="email" name="email" class="form-control validate">
		                </div>
						
						<div class="md-form mb-3 ">
		                	<label data-error="wrong" data-success="right" for="cat_edit_name">User Phone</label>
		                	<br><span id="phone_error"></span>
							<div class="row">
								<div class="col-lg-4">
									<input  value="+962" class="form-control" readonly>
								</div>
								<div class="col-lg-8">
										<input maxlength="9" type="text" id="phone" name="phone" class="form-control validate">
								</div>
							</div>
		                </div>
						
		            </div>
		            <div class="modal-footer d-flex button justify-content-center">
		                  <button class="btn-primary add-ing-button" id="button" name="submit" type="submit">Submit</button>

				    </div>
		        </div>
		    </div>
			</form>
		</div>
	<!-- Edit category model end -->
	<?php include"include/footer.php"; ?>
		<script>
		$(document).ready(function(){
		
		$(".close").click(function(){
			$("#editCatModal").hide();
			window.location.reload();
			});
 
			$('form[id="userform"]').validate({
				
            rules: {
                name: {
					required:true,
					 minlength:3,
					maxlength:30,
                    letterswithspace: true,
                },
                email: 
                {  
                    	required:true,
						minlength: 2,
						maxlength: 50,
						 email: true,
        				// regex: /^\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i,
				},
				 phone: 
                {  
						required:true,
						number: true,
						minlength: 9,
						maxlength: 9,
					
				},
			
            },
             messages: {
            //     //salad_name: "Please enter salad name",
            //     name:  {
            //         required: "Please enter name",
			// 		maxlength:"Please enter less than 30 characters ",
			// 		minlength:"Please enter greater than 3 characters"
			// 	},
                 phone: {
                    required: "Please enter Phone no",
					number: "Phone no should be in digits only",
					maxlength:"Please enter no of 9 digits",
					minlength:"Please enter no of 9 digits"
				},
			// 	email: {         
			// 		email: "Please enter a valid email address",
			// 	},
             },
            submitHandler: function(form) {
                //Here we can any custom work like AJAX calling

                if(false){ } 
                else {
                form.submit();
                }
			}
			
        });

        $.validator.addMethod("letterswithspace", function(value, element) {
            return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
        }, "Please enter only alphabets");
     
	});
		
		</script>