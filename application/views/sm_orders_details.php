<?php include"include/header.php" ?>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include "include/navbar.php"; ?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include"include/leftsidebar.php"; ?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<div class="subheader">
				<ul>
					<li> <?=$this->lang->line('admin')?$this->lang->line('admin'):'Admin';?> / Order Detail</li>
				</ul>
			</div>
			<!--  MAIN CONTENT -->
			<div class="main-content">
				<?php
				if($this->session->flashdata('success_msg')){
					$msg = $this->session->flashdata('success_msg');
					echo '<div class="alert alert-success fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Success!</strong>'. $msg.'
			        		</div>';
				}
				else if($this->session->flashdata('error_msg')){
					$msg = $this->session->flashdata('error_msg');
					echo '<div class="alert alert-danger fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Error!</strong>'. $msg.'
			        		</div>';
				}
				?>
				<div class="container-fluid">					
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel" >
								<div class="row" style="padding: 20px 30px;">
									<div class="col-lg-6">
										<h3 class="panel-title">Order details</h3>
									</div>
									<?php
									if(!empty($query) && $query->num_rows() > 0){

									foreach ($query->result() as $odetail) {

									?>
									<?php if ($odetail->in_store_pickup==1 )  { if($odetail->o_status == 1){ ?>
                                    <div class="col-lg-6">
                                        <form id="form" action="<?=base_url()?>orders/change_status"  method="post">
                                            <input type="hidden" name="order_id" id="order_id" value="<?= $odetail->order_id ?>">
                                            <input type="hidden" name="status" id="orderstatus">
                                            <div style="float: right">
                                                <button class="btn btn-success status_change accept" id="button" name="submit" type="submit"><i class="lni-check-mark-circle"></i> &nbsp; Accept</button>
                                                <button class="btn btn-danger status_change cancel" id="button" name="submit" type="submit"><i class="lni-cross-circle"></i> &nbsp;Cancel</button>
                                            </div>
                                        </form>
                                    </div>
									<?php } }?>
								</div>

								<div class="panel-body" style="margin-bottom: 20px; padding: 20px">

								<div class="table-responsive">


									<table class="table table-striped datatable">
										
										<tbody>

														   <tr>
														    <th>Order Id</th>
															<td>SM-<?php echo strtoupper(encryptId($odetail->order_id));?></td>
															<th>Ordered By</th>
															<td><?php 
															$row = $users->row();
															$username=$row->full_name;
															echo $username;?></td>
															</tr>
														<tr>
															<th>Delivery Status</th>
															<td><?php if ($odetail->in_store_pickup==0 ){
																if($odetail->o_status==7 || $odetail->o_status==0) { ?>
																	<span  class="label label-danger"> Failed </span>
																<?php } else if ($odetail->in_store_pickup == 0 && isset($delivery_status)) { ?>
																<span  class="label label-primary"><?php echo $delivery_status;?></span></td>
															<?php }
															} ?>
																<?php if ($odetail->in_store_pickup==1 ) {
																if($odetail->o_status==1 ){ ?>
																		<span  class="label label-warning"> Placed </span>
																<?php } else if ($odetail->o_status==2 ) { ?>
																		<span  class="label label-success">Accepted </span>
																<?php } else if ($odetail->o_status==3 ) { ?>
																		<span  class="label label-success"> Ready to Ship </span>
																<?php } else if ($odetail->o_status==4 ) { ?>
																		<span  class="label label-success"> Out for Delivery </span>
																<?php } else if($odetail->o_status==5) { ?>
																		<span  class="label label-success"> Delivered</span>
																<?php } else if($odetail->o_status==6) { ?>
																		<span  class="label label-danger"> Canceled </span>
																<?php } else if($odetail->o_status==7 || $odetail->o_status==0) { ?>
																		<span  class="label label-danger"> Failed </span>
																<?php } }?>
															</td>
															<th>Store Pick Up</th>
															<td><?php if ($odetail->in_store_pickup==1 ) : ?>
																		Yes
																	<?php else : ?>
																		No
																	<?php endif; ?>
																				
															</td>
															
														</tr>
														 <tr>
														 
														    <th>Promo Applied</th>
															<td><?php if($odetail->promo_id==0)
																				{
																					echo "Not Applied";
																				} 
																			else
																				{
																					echo "Applied";
																				}
																				?></td>
																				<th>Date</th>
															<td><?php echo date("d-m-Y, h:i A", strtotime($odetail->created_at));?></td>

															
														</tr>
														
														
														<tr> 
														 <th>Payment Method</th>
															<td><?php echo $odetail->payment_method;?></td>
														    <th>Payment Status</th>
															<td>
															<?php if($odetail->payment_method == 'Online' || $odetail->delivery_status==4)
																	echo "Paid";
																else if($odetail->payment_status == 2)
																	echo "Failed";
																else
																	echo "Not Paid";
															?>	
														    </td>
                                                            </tr>
															<tr>
															
																<?php 
																	if($odetail->in_store_pickup == 0)
																	{
																	$sql = "select * from sm_addresses where add_id = $odetail->address_id";
																	$address=$this->Main_model->__callMasterquery($sql);
																	
																	if($address->num_rows() > 0){
																		foreach ($address->result() as $adddata) {?>
																			<th>Address</th>
																			<td><?= $adddata->addline1.''.$adddata->addline2;?></td> 
																<?php }  } } ?>
																<?php if ($odetail->in_store_pickup==1 ) : ?>
																
																<th>Pickup Person</th>
																<td>
																	<?php echo $odetail->customer_name.'('.$odetail->customer_phone.')';?>
																		
																</td>

																<th>Pickup Date</th>
																<td>
																	<?php echo date("d-m-Y h:i:A", strtotime($odetail->pickup_date.' '.$odetail->pickup_time)) ?>
																		
																</td>

																<?php else : ?>
																<?php endif; ?>
																
															</tr>
															<tr>


															</tr>
												<?php 	}	} ?>	
										</tbody>
									</table>
								</div>
								</div>

                                <h3 class="panel-title" style="font-size: 18px; margin-left: 20px; font-weight: 800;padding-top: 20px;">Ordered Recipes</h3>
                                       <?php if(!empty($salad_query) && $salad_query->num_rows() > 0){
												foreach ($salad_query->result() as $salad_detail) { ?>
                                <div class="row " style="  border: 1px solid #eee; margin: 20px 20px;">
                                    <div class="col-lg-2 col-md-2 col-sm-2">
										<?php
										$salad_img=$salad_detail->salad_img;
										if($salad_detail->salad_img == NULL){
											$salad_img='custom.png';
										}?>

                                    <img style="height:100%;width:100%; " class="post-thumb" src="<?php echo base_url(); ?>uploads/salads/<?= $salad_img ?>?>"> </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10" style="padding:20px">
                                        <div>
                                            <p> <span class="ssname"> Recipe Name: </span> <span class="ssname2"><?= $salad_detail->salad_name;?></span> </p>
                                        </div>
                                        <div>
                                            <p> <span class="ssname"> Quantity: </span> <span class="ssname2"><?= $salad_detail->quantity ?></span> </p>
                                        </div>
                                        <div>
										<?php $total_price=$salad_detail->item_total_price;$quantity=$salad_detail->quantity;
										 $total=$total_price*$quantity;?>
                                            <p> <span class="ssname"> Total Price: </span> <span class="ssname2"><?= $total ?>JD</span> </p>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="col-lg-6 col-md-6">
										<?php $this->db->select('sing.ing_id,sing.quantity,sing.salad_id,ing.ing_name');
											$this->db->from('sm_salad_ingredients sing');
											$this->db->join('sm_ingredients ing','ing.ing_id = sing.ing_id','left');
											$this->db->where('sing.salad_id',$salad_detail->salad_id);
											 $this->db->group_by('sing.ing_id');
											$query=$this->db->get();
											// $mainsql=$query->result_array();
											if($query->num_rows() > 0){ ?>
                                            <div style="">
                                                <h3 class="panel-title" style="font-size: 18px; font-weight: 800; padding-top: 20px;">Ingredient</h3>
                                            </div>
                                            <?php foreach ($query->result() as $sirow) { 
												?>
                                                <div>
                                                     <!--<h3 class="panel-title ing-name" ><?= $ocrow->subcat_name?></h3> -->
                                                   
                                                       <p><span class="ssname"> - <?= $sirow->ing_name?> </span> <span class="ssname2"><?= $sirow->quantity ?></span>  
														<!-- <span class="ssname2"><?= $sirow->ing_price?> JD</span> -->
														</p>
                                                </div>
                                            <?php   } } ?>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
										  <?php $ordsql="select * from sm_order_ingredients where odetail_id =$salad_detail->odetail_id group by ing_id";
												   $ordercat=$this->Main_model->__callMasterquery($ordsql);
                                                    if(!empty($ordercat) && $ordercat->num_rows() > 0){ ?>
                                            <div style="">
                                                <h3 class="panel-title" style="font-size: 18px; font-weight: 800; padding-top: 20px;">Add-On Ingredient</h3>
                                            </div>
                                          <?php foreach ($ordercat->result() as $ocrow) { ?>
                                                <div>
                                                     <!--<h3 class="panel-title ing-name" ><?= $ocrow->subcat_name?></h3> -->
                                                   
                                                        <p><span class="ssname"> - <?= $ocrow->ing_name?> </span> <span class="ssname2"><?= $ocrow->quantity ?></span> 
														<!-- <span class="ssname2"><?= $oing->ing_price?> JD</span> -->
														 </p>
                                                </div>
                                            <?php   } }  ?>
                                        </div>
                                    </div>
	
                                </div>
                                            <?php } } ?>

                               
                                <h3 class="panel-title" style="font-size: 18px; font-weight: 800; margin-left: 20px; padding-top: 20px;">Payment Details</h3>
                                <div class="row" style=" padding: 20px 20px">
                                <div class="col-lg-4" >
                                        <table style="" class="table table-striped">
                                            <tbody>
                                            <tr>
                                                <th>Total Price:</th>
												<?php $total_price=$odetail->total_price;$quantity=$salad_detail->quantity;
														 $total=$total_price*$quantity;?>
                                                <td> <?php echo $total;?> JD</td>
                                            </tr>
                                            <tr>
											<?php $taxes = getSingleFieldDetail('id',1,'tax_rate','sm_app_settings'); ?>
                                                <th>Tax Amount(<?= round($taxes)?>%):</th>
                                                <td><?php echo $odetail->tax_amt;?> JD</td>
                                            </tr>
											<?php if($odetail->discount_amt==0){
											?>
											<?php } else { ?>
                                            <tr>
                                                <th>Discount</th>
                                                <td><?php echo $odetail->discount_amt;?> JD</td>
                                            </tr>
											<?php } ?>
                                            <tr>
                                                <th>Delivery Charges:</th>
                                                <td><?php echo round($odetail->delivery_charge,1);?> JD</td>
                                            </tr>
                                            <tr>
                                                <th>Grand Total:</th>
                                                <td><?php echo $odetail->grand_total;?>JD</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

								<div class="panel-footer">
									<div class="row" style="margin-top:20px;margin-left:10px;">
										<a href="<?= base_url() ?>orders/"  class="btn add btn-info"><i class="fa fa-backward"></i> <span> GO BACK </span></a>
									</div>
								</div>
							</div>
						
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
             <?php include"include/footer_content.php" ?>
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		
	</div>
	<!-- END WRAPPER -->
	<?php include"include/footer.php" ?>
<script>
	$("button.accept").click(function () {
		$("input[name='status']").val('2');
		var r = confirm("Are sure to accept this order!");
		if (r == true) {
			error=false;
		}else{
			error=true;
		}
		if(error==true){
			return false;
		}else{
			return true;
		}
	});
	$("button.cancel").click(function () {
		$("input[name='status']").val('6');
		var r = confirm("Are sure to cancel this order!");
		if (r == true) {
			error=false;
		}else{
			error=true;
		}
		if(error==true){
			return false;
		}else{
			return true;
		}
	});


</script>