<?php include"include/header.php" ?>

	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include"include/navbar.php" ?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include"include/leftsidebar.php" ?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                      <div class="subheader">
                        <ul>
                            <li><?=$this->lang->line('Admin')?$this->lang->line('Admin'):'Admin'?> </li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
                <div id="custom_error">
                    
                </div>
                <?php
                    if($this->session->flashdata('success_msg')){
                    $msg = $this->session->flashdata('success_msg');
                        echo '<div class="alert alert-success fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong>'. $msg.'
                            </div>';
                    }
                ?>
                <?php
                    if($this->session->flashdata('error_msg')){
                    $msg = $this->session->flashdata('error_msg');
                        echo '<div class="alert alert-danger fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong>'. $msg.'
                            </div>';
                    }
                ?>
				<div class="container-fluid">
					
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
                            <form class="col-md-10 form-panel" style="border:0" method="post" action="<?= base_url()?>admin/email_setting_action" id="email_setting_frm" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="panel new-panel">
                                            <div class="panel-heading">
                                                <div class="col-md-12">
                                                    <h3 class="panel-title"><?=$this->lang->line('Email Setting')?$this->lang->line('Email Setting'):'Email Setting'?> </h3>
                                                </div>
                                                <br>
                                            </div>
                                      
                                            <div class="panel-body no-padding">
                                                <input type="hidden" name="id" value="<?= $email[0]? $email[0]['id']:'' ?>">
                                                <div class="col-md-2">
                                                    <label>Smtp<span class="required error">*</span></label>
                                                    <input class="form-control" value="<?= $email[0]? $email[0]['type']:'' ?>" type="text" id="smtp_type" name="smtp_type" placeholder="smtp">
                                                    <?php echo form_error('smtp_type', '<div class="error">', '</div>'); ?>
                                                    <br>
                                                </div>

                                                <div class="col-md-2">
                                                    <label><?=$this->lang->line('Port No')?$this->lang->line('Port No'):'Port No'?> <span class="required error">*</span></label>
                                                    <input class="form-control" value="<?= $email[0]? $email[0]['smtp_port']:'' ?>" type="text" id="smtp_port" name="smtp_port" placeholder="465">
                                                    <?php echo form_error('smtp_port', '<div class="error">', '</div>'); ?>
                                                    <br>
                                                </div>
                                                 <div class="col-md-4">
                                                    <label><?=$this->lang->line('Host')?$this->lang->line('Host'):'Host'?> <span class="required error">*</span></label>
                                                    <input class="form-control" value="<?= $email[0]? $email[0]['smtp_host']:'' ?>" type="text" id="smtp_host" name="smtp_host" placeholder="ssl://smtp.gmail.com">
                                                    <?php echo form_error('smtp_host', '<div class="error">', '</div>'); ?>
                                                    <br>
                                                </div>
                                                 <div class="clearfix"></div>
                                                <div class="col-md-4">
                                                    <label><?=$this->lang->line('Email')?$this->lang->line('Email'):'Email'?> <span class="required error">*</span></label>
                                                    <input class="form-control" value="<?= $email[0]? $email[0]['smtp_user']:'' ?>"  type="text" id="smtp_email" name="smtp_email" placeholder="bee-o-veg@gmail.com">
                                                    <?php echo form_error('smtp_email', '<div class="error">', '</div>'); ?>
                                                    <br>
                                                </div>
                                               <div class="col-md-4">
                                                    <label><?=$this->lang->line('Password')?$this->lang->line('Password'):'Password'?> <span class="required error">*</span></label>
                                                    <input class="form-control" value="<?= $email[0]? $email[0]['smtp_password']:'' ?>"  type="Password" id="smtp_password" name="smtp_password" placeholder="Password">
                                                    <?php echo form_error('smtp_password', '<div class="error">', '</div>'); ?>
                                                    <br>
                                                </div>
                                               <div class="clearfix"></div>                                                                                          
                                            </div>
                                            <div class="panel-footer">
                                                    <div class="row">
                                                        <div class="col-md-12 text-right"><!-- <a href="ticket_add.html" class="btn btn-primary" id="profileUpdateFrm"> Update </a> -->
                                                        <button type="submit" class="btn btn-primary " id="email_setting_btn"><?=$this->lang->line('Submit')?$this->lang->line('Submit'):'Submit'?></button>
                                                        </div>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                            </form>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
                          <?php include"include/footer_content.php" ?>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		
	</div>
	<!-- END WRAPPER -->
    <script type="text/javascript">
        $( document ).ready(function() {
            $('form[id="email_setting_frm"]').validate({
            rules: {
                    smtp_type: 'required',
                    smtp_host: 'required',
                    smtp_port: 'required',
                    smtp_email: 'required',
                    smtp_password: 'required',
                },
                messages: {
                    smtp_type: 'Please enter type',
                    smtp_host: 'Please enter host name',
                    smtp_port: 'Please enter port number',
                    smtp_email: 'Please enter email',
                    smtp_password: 'Please enter Password',
                },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
    </script>
	<?php include"include/footer.php" ?>
