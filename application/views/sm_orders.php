<?php include "include/header.php"; ?>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include "include/navbar.php"; ?>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include "include/leftsidebar.php" ?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
            <div class="subheader">
                <ul>
                    <li> <?=$this->lang->line('admin')?$this->lang->line('admin'):'Admin';?> / Orders List</li>
                </ul>
            </div>
			<!--  MAIN CONTENT -->
			<div class="main-content">
				<?php
					if($this->session->flashdata('success_msg')){
					$msg = $this->session->flashdata('success_msg');
						echo '<div class="alert alert-success fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Success!</strong>'. $msg.'
			        		</div>';
					}
					else if($this->session->flashdata('error_msg')){
					$msg = $this->session->flashdata('error_msg');
						echo '<div class="alert alert-danger fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Error!</strong>'. $msg.'
			        		</div>';
		        	}
				?>
				<div class="container-fluid">

					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading2">
									<h3 class="panel-title2">Orders List</h3>

								</div>
								<div class="panel-body no-padding">
								<div class="table-responsive">

									<table class="table table-striped datatable">
										<thead>
											<tr>
												<th>S.No</th>
												<th>Order Id</th>
												<th>Ordered By</th>
												<th>Store Pick Up</th>
												<th>Payment Method</th>
												<th>Grand Total</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
										
												<?php
															// $row = $encryptId->row();
															// $order_id=$row->order_id;
												if(!empty($query) && $query->num_rows() > 0){
													foreach ($query->result() as $order) {?>
													<tr>
														<th>ID</th>
														<td><?php echo 'SM-'.strtoupper(encryptId($order->order_id));?></td>
														<td><?php echo $order->full_name;?></td>
															  <td>
																<?php if ($order->in_store_pickup==1 ) : ?>
																		Yes
																	<?php else : ?>
																		No
																	<?php endif; ?>
															</td>

															<td><?php echo $order->payment_method;?></td>
															 <td><?php echo $order->grand_total;?></td>
															<td>
																<!-- for delivery -->
																 <?php  if ($order->in_store_pickup==0 ){
																	if($order->o_status==7 || $order->o_status==0 ){ ?>
																	 <span  class="label label-warning">Failed</span>
																 <?php } elseif($order->delivery_status==1){ ?>
																	 <span  class="label label-success">Created</span>
																<?php } elseif($order->delivery_status==2){ ?>
																	 <span  class="label label-danger">Pending Pickup</span>
																<?php } elseif($order->delivery_status==3 ){ ?>
																	<span  class="label label-danger">Picked</span>
																<?php } elseif($order->delivery_status==4 ){ ?>
																	 <span  class="label label-danger">Completed</span>
																 <?php } elseif($order->delivery_status==5 ){ ?>
																	 <span  class="label label-danger">Canceled</span>
																 <?php }else { ?>
																	 <span  class="label label-danger">Not Available</span>
																 <?php } } ?>
																<!-- for pick up -->
																<?php  if ($order->in_store_pickup==1 ){
																	if($order->o_status==7 || $order->o_status==0 ){ ?>
																		<span  class="label label-warning">Failed</span>
																	<?php } elseif($order->o_status==1){ ?>
																		<span  class="label label-warning"> Placed </span>
																	<?php } else if ($order->o_status==2 ) { ?>
																		<span  class="label label-success">Accepted </span>
																	<?php }elseif($order->o_status==6){ ?>
																		<span  class="label label-danger">Cancled</span>
																	<?php } } ?>
															 </td>
															 <td>
																<?php  if ($order->in_store_pickup==1 ){
																	if($order->o_status == 1){
																		?>
																		<a onclick="change_status('<?=$order->o_status?>','<?=$order->order_id?>')"   class="btn btn-xs btn-primary changeorder" id="changeorder" >change status</a>
																		<?php
																	} }
																 ?>


                                                			 	<a  title="View Order!" class="btn btn-xs btn-info " name="detail" href="<?=base_url()?>orders/details/<?=$order->order_id;?>">
                                                			 		<i class="fa fa-info-circle" ></i><input type="hidden" name="orderid" value="<?php echo $order->order_id;?>" id="cat_edit_id">
                                                			 	</a>
                                                			 </td>
                                                			 </tr>

												<?php	}
												}
												?>
										</tbody>
									</table>
								</div>
								</div>
								<div class="panel-footer">
									<div class="row">
	                                    <div class=" text-left">

	                                    </div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
                        <?php include"include/footer_content.php" ?>
		<!-- END MAIN -->
		<div class="clearfix"></div>

	</div>
		</div>
	<!-- END WRAPPER -->
	<!-- Edit category model start added by ravindra -->
		<div class="modal" id="change_status_model"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <form id="form" action="<?=base_url()?>orders/change_status"  onsubmit="return confirmation_alert()" method="post">
			<div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header text-left">
		                <h4 class="modal-title w-100 font-weight-bold">Change Status</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
		                   &times;
		                </button>
		            </div>
					   <input type="hidden" name="order_id" id="order_id">
		            <div class="modal-body mx-2">
						<div class="md-form mb-3 ">
		                	<label data-error="wrong" data-success="right" for="cat_edit_name">Order Status</label>
		                	<br><span id="unit_type_error"></span>
		                <select  id="status" name="status" class="form-control validate">
								<option  value='1'>Placed</option>
								<option  value='2'>Accept</option>
								<option  value='6'>Cancel</option>
						</select>

								</div>
		            </div>
		            <div class="modal-footer d-flex button justify-content-center">
		                  <button class="btn-primary status_change" id="button" name="submit" type="submit">Submit</button>

				    </div>
		        </div>
		    </div>
			</form>
		</div>
	<!-- Edit category model end -->
	<?php include"include/footer.php"; ?>
		<script>
		$(document).ready(function(){

		$(".close").click(function(){
				$("#change_status_model").hide();
			});

		 $('form[id="form"]').validate({
            rules: {

				status: "required",

            },
            messages: {
                //salad_name: "Please enter salad name",
                status: "Please select status",
                 calories: {
                    required: "Please enter calory",
                    number: "Calory should be in digits only"
                },
            },
        });

        $.validator.addMethod("letterswithspace", function(value, element) {
            return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
        }, "Please enter only alphabets");
        $.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z]+$/i.test(value);
        }, "Please enter only alphabets");
         $.validator.addMethod("date", function ( value, element ) {
                var bits = value.match( /([0-9]+)/gi ), str;
                if ( ! bits )
                    return this.optional(element) || false;
                str = bits[ 1 ] + '/' + bits[ 0 ] + '/' + bits[ 2 ];
                return this.optional(element) || !/Invalid|NaN/.test(new Date( str ));
            },
            "Please enter a date in the format dd/mm/yyyy"
        );
        jQuery.validator.addMethod("alphanumeric", function(value, element) {
            return this.optional(element) || /^[\w.]+$/i.test(value);
        }, "Letters, numbers, and underscores only please");
	});
		</script>
		<script>
			function change_status(order_status,order_id){
				$('#order_id').val(order_id);
				var status=order_status;
				$("#change_status_model").show();
			}
		</script>

		<script>
			function confirmation_alert(){
				var error=true;
				var status=$('#status').val();
				if(status){
					if(status == 1){
						alert('Please select accept or delice the order!');
						error=true;
					}
					else if(status == 2){
						var r = confirm("Are sure to accept this order!");
						if (r == true) {
							error=false;
						}else{
							error=true;
						}
					}
					else if(status == 6){
						var r = confirm("Are sure to cancel this order!");
						if (r == true) {
							error=false;
						}else{
							error=true;
						}
					}else{
						error=true;
					}
				}else{
					error=true;
				}

				if(error==true){
					return false;
				}else{
					return true;
				}
			}
		</script>

