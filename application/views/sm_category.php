<?php include "include/header.php"; ?>
<!-- WRAPPER -->
<div id="wrapper">
	<!-- NAVBAR -->
	<?php include "include/navbar.php"; ?>
	</nav>
	<!-- END NAVBAR -->
	<!-- LEFT SIDEBAR -->
	<?php include "include/leftsidebar.php" ?>
	<!-- END LEFT SIDEBAR -->
	<!-- MAIN -->
	<div class="main">
		<div class="subheader">
			<ul>
				<li> <?=$this->lang->line('admin')?$this->lang->line('admin'):'Admin';?> / <?=$this->lang->line('category_list')?$this->lang->line('category_list'):'Category List';?> </li>
			</ul>
		</div>
		<!--  MAIN CONTENT -->
		<div class="main-content">
			<?php
			if($this->session->flashdata('success_msg')){
				$msg = $this->session->flashdata('success_msg');
				echo '<div class="alert alert-success fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Success!</strong>'. $msg.'
			        		</div>';
			}
			if($this->session->flashdata('error_msg')){
				$msg = $this->session->flashdata('error_msg');
				echo '<div class="alert alert-danger fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Error!</strong>'. $msg.'
			        		</div>';
			}
			?>
			<div class="container-fluid">

				<!-- END OVERVIEW -->
				<div class="row">
					<div class="col-md-12">
						<!-- RECENT PURCHASES -->
						<div class="panel">
							<div class="panel-heading2">
								<h3 class="panel-title2"><?=$this->lang->line('category_list')?$this->lang->line('category_list'):'Category List';?></h3>
							</div>
							<div class="panel-body no-padding">
								<div class="table-responsive">
									<table class="table table-striped datatable">
										<thead>
										<tr>
											<th>S.No</th>
											<th><?=$this->lang->line('cat_name')?$this->lang->line('cat_name'):'Name';?></th>
											<th><?=$this->lang->line('name_Arabic')?$this->lang->line('name_Arabic'):'Name Arabic';?></th>
										</tr>
										</thead>
										<tbody>
										<?php
										if($categoreis){
											$enable = $this->lang->line('enable')?$this->lang->line('enable'):'Enable';
											$disable = $this->lang->line('disable')?$this->lang->line('disable'):'Disable';
											foreach ($categoreis as $cat) {

												if($cat->cat_status){
													$status_span = '<span class="label label-success">'.$enable.'</span>';
												}else{
													$status_span = '<span class="label label-danger">'.$disable.'</span>';
												}
												?>
												<tr>
													<td></td>
													<td><?php echo $cat->cat_name ?></td>
													<td><?php echo $cat->cat_name_arabic ?></td>
												</tr>
												<?php
											}
										}
										?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="panel-footer">
								<div class="row">
									<div class=" text-left">

									</div>
								</div>
							</div>
						</div>
						<!-- END RECENT PURCHASES -->
					</div>
				</div>
			</div>
		</div>
		<!-- END MAIN CONTENT -->
		<?php include"include/footer_content.php" ?>
		<!-- END MAIN -->
		<div class="clearfix"></div>

	</div>
	<!-- END WRAPPER -->
	<?php include"include/footer.php"; ?>

	