<?php include"include/header.php" ?>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include "include/navbar.php"; ?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include"include/leftsidebar.php"; ?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			 
			<!--  MAIN CONTENT -->
			<div class="main-content">
				 <?php
		            if($this->session->flashdata('success_msg')){
		            $msg = $this->session->flashdata('success_msg');
		                echo '<div class="alert alert-success fade in">
		                    <a href="#" class="close" data-dismiss="alert">&times;</a>
		                    <strong>Success!</strong>'. $msg.'
		                    </div>';
		            }
		        ?>
				<div class="container-fluid">					
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="row panel-heading">
								<div class="col-lg-6">
								<h3 class="panel-title">User Details</h3>
								</div>
								
								</div>
								<div class="panel-body no-padding">

								<div class="table-responsive">
									<table class="table table-striped datatable">
										
										<tbody>
										<?php
												if(!empty($query) && $query->num_rows() > 0){
												
													foreach ($query->result() as $user) {
													
				?>		                	   <h4>	User Detail</h4>
												<tr>
													<th>Name</th>
													<td><?php echo $user->full_name;?></td>
													<th>Email</th>
													<td><?php echo $user->email;?></td>
												</tr>
												<tr>
													<th>Phone No</th>
													<td><?php echo $user->phone;?></td>
													<th>Registered On</th>
													 <td><?php echo date("d-m-Y", strtotime($user->created_at)) ;?></td>
												</tr>
												<?php }} ?>
												</tbody>
												</table>

										<?php
												if(!empty($address) && $address->num_rows() > 0){?>
										<h4 style="margin-top:40px;">Address Details</h4>
										<table class="table table-striped datatable" style="margin-top:10px;" >
										<th>Addresses</th>
										<!-- <th>Address Type</th> -->
										<tbody>
										<?php
													foreach ($address->result() as $add) {
												?>	
												<tr>
													<td><?php echo $add->addline1.' '.$add->addline2.' , '.$add->city;?></td>
												     
													  <!-- <td><?php echo $add->add_type;?></td> -->
												</tr>
												<?php }?>
												</tbody>
													</table><?php }?>

										<?php
												if(!empty($orders) && $orders->num_rows() > 0){?>
										<h4 style="margin-top:40px;">Order Details</h4>
										<table class="table table-striped datatable" style="margin-top:10px;" >
										<!-- <th>ID</th> -->
										<th>Order Id</th>
										<th>Delivery Type</th>
										<th>Payment Status</th>
										<th>Payment Method</th>
										<th>Grand Total</th>
										<th>Status</th>
										<th>Action</th>
										<tbody>
										<?php
										$i='';
											$i=1;
													foreach ($orders->result() as $order) {
													
												?>	
												<tr>
													
													<td><?php echo strtoupper(encryptId($order->order_id));?>
													</td>
															<td>
																<?php if ($order->in_store_pickup==1 ) : ?>
																		Pickup
																	<?php else : ?>
																		Delivered
																	<?php endif; ?>
															</td>
															 <td>
																<?php if ($order->payment_status==0) : ?>
																		Not Paid
																		<?php else : ?>
																			Paid
																		<?php endif; ?></td>
														    <td><?php echo $order->payment_method;?></td>
															 <td><?php echo $order->grand_total;?></td>
															 <td> <span class="label label-success"><?php if($order->o_status==1 )
																				{
																					echo "Placed";
																				}
																				else if ($order->o_status==2 ) 
																				{
																					echo "Preparing";
																				}
																				else if ($order->o_status==3 ) 
																				{
																				echo "Ready to Ship";
																				}
																				else if ($order->o_status==4 ) 
																				{
																				echo "Out for Delivery";
																				}
																				else
																				{
																					echo "Delivered";
																				}?></span></td>
																		 <!-- <td><?php echo date("d-m-Y, h:i A", strtotime($order->order_delivered_at)) ;?></td>		 -->
																			<td> <a class="btn btn-xs btn-info" href="<?=base_url()?>orders/details/<?=$order->order_id;?>"  data-cat-id="<?php echo $user->id?>"><i class="fa fa-info-circle" ></i>
                                                			 	</a></td>
															
															 </tr>
															
												<?php 	$i++;
												}
												}
												?>									
									
										</tbody>
									</table>
								</div>
								
								</div>
								
								<div class="panel-footer">
									<div class="row">
                                        	<div class="" style="margin-left:10px;">
										<a href="<?= base_url() ?>users/" id="coupon" class="btn add btn-info"><i class="fa fa-backward"></i> <span> GO BACK </span></a>
									</div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
             <?php include"include/footer_content.php" ?>
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		
	</div>
	<!-- END WRAPPER -->
	<?php include"include/footer.php" ?>
