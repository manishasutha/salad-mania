<?php include"include/header.php";?>
    <!-- WRAPPER -->
    <div id="wrapper">
        <!-- NAVBAR -->
        <?php include "include/navbar.php"; ?>
        <!-- END NAVBAR -->
        <!-- LEFT SIDEBAR -->
        <?php include"include/leftsidebar.php"; ?>
        <!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                      <?php
                        if($menu[0]['type'] == 'product'){
                            $page = 'Product';
                        }else{
                            $page = 'Menu';
                        }
                      ?>
                      <div class="subheader">
                        <ul>
                            <li><?=$this->lang->line($page)?$this->lang->line($page):$page?> </li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
                <?php
                    if($this->session->flashdata('success_msg')){
                    $msg = $this->session->flashdata('success_msg');
                        echo '<div class="alert alert-success fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong>'. $msg.'
                            </div>';
                    }
                ?>
                <?php
                    if($this->session->flashdata('error_msg') || !empty($error)){
                        if(!empty($error)){
                            $msg = $error;

                        }else{
                            $msg = $this->session->flashdata('error_msg');
                        }
                        echo '<div class="alert alert-danger fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Error!</strong>'. $msg.'
                            </div>';
                    }
                ?>
				<div class="container-fluid">
					
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
                            <form class="col-md-12 form-panel" style="border:0" method="post" action="<?=base_url()?>menu/editAction" id="menuEditFrm" name="menuEditFrm" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="panel new-panel">
                                        <div class="panel-heading">
                                            <div class="col-md-12">
                                                <h3 class="panel-title"><?=$this->lang->line($page.' Edit')?$this->lang->line($page.' Edit'):$page.' Edit'?> </h3>
                                            </div>
                                            <br>
                                        </div>
                                        <input type="hidden" name="customer_id" value="<?=$menu[0]['vendor_id']?>">
                                        <input type="hidden" name="menu_id" value="<?=$menu[0]['menu_id']?>">
                                         <input type="hidden" name="type" value="<?=$menu[0]['type']?>">
                                       <input type="hidden" name="old_image_path" value="<?=$menu[0]['image_path']?>" id="old_image_path">
                                        <input type="hidden" name="old_image_name" value="<?=$menu[0]['image_name']?>">
                                        <!--<div class="notice-area">
                                            <div class="alert alert-info alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <i class="fa fa-info-circle"></i> The system is running well
                                            </div>
                                            <div class="alert alert-success alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <i class="fa fa-check-circle"></i> Your settings have been succesfully saved
                                            </div>
                                            <div class="alert alert-warning alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <i class="fa fa-warning"></i> Warning, check your permission settings
                                            </div>
                                            <div class="alert alert-danger alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <i class="fa fa-times-circle"></i> Your account has been suspended
                            		      </div>
                                        </div>-->
                                        <div class="panel-body no-padding">
                                            <div class="col-md-4">
                                                <label><?=$this->lang->line($page.' Category')?$this->lang->line($page.' Category'):$page.' Category'?><span class="required error">*</span></label>
                                                <select class="form-control select2" name="menu_category" id="menu_category">
                                                    <!-- <option value="">Select menu category</option> -->
                                                    <?php
                                                        if(count($menu_category) > 0 && !empty($menu_category)){
                                                            foreach ($menu_category as $key => $menu_cat) {
                                                                if($menu[0]['category_id'] == $menu_cat['menu_category_id']){
                                                                    $selected = 'selected';
                                                                }else{
                                                                    $selected = '';
                                                                }
                                                                echo '<option value="'.$menu_cat['menu_category_id'].'"'.$selected.'>'.$menu_cat['name'].'</option>';
                                                            }
                                                        }else{
                                                            echo '<option value="">Menu category not available</option>';
                                                        } 
                                                    ?>
                                                </select>
                                                 <?php echo form_error('menu_category', '<div class="error">', '</div>'); ?>
                                                <br>
                                            </div>
                                             <!-- <div class="col-md-1">
                                                <label style="margin-top: 30px">OR</label>
                                                <br>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Create New Category</label>
                                                <input class="form-control"  type="text">
                                                <br>
                                            </div> -->
                                            <div class="clearfix"></div>
                                            <hr> 
                                            <div class="menu-clone">
                                                <?php
                                                    $Name = $this->lang->line($page.' Name')?$this->lang->line($page.' Name'):$page.' Name';
                                                    echo'<div class="col-md-5">
                                                         <label>'.$Name.'<span class="required error">*</span></label>
                                                         <input class="form-control"  type="text" name="menu_name[]" id="menu_name" value="'.$menu[0]['title'].'">
                                                         <br>
                                                         '.form_error('menu_title', '<div class="error">', '</div>').'
                                                        </div>';
                                                    $Name_italian = $this->lang->line($page.' Name')?$this->lang->line($page.' Name').'(Italiano)':$page.' Name(Italian)';
                                                    echo'<div class="col-md-5">
                                                         <label>'.$Name_italian.'<span class="required error">*</span></label>
                                                         <input class="form-control"  type="text" name="menu_name_italian[]" id="menu_name_italian" value="'.$menu[0]['title_italian'].'">
                                                         <br>
                                                         '.form_error('menu_title', '<div class="error">', '</div>').'
                                                        </div>';
                                                     $Description = $this->lang->line($page.' Description')?$this->lang->line($page.' Description'):$page.' Description';
                                                    echo'<div class="col-md-5">
                                                        <label>'.$Description.'<span class="required error">*</span></label>
                                                          <div class="form-group" style="margin: 0">
                                                               <input class="form-control"  type="text" name="menu_description[]" id="menu_description" value="'.$menu[0]['description'].'">
                                                        </div>
                                                        '.form_error('menu_description', '<div class="error">', '</div>').'
                                                        <br>
                                                    </div>';
                                                    $description_italian = $this->lang->line($page.' Description')?$this->lang->line($page.' Description').'(Italiano)':$page.' Description(Italian)';
                                                    echo'<div class="col-md-5">
                                                        <label>'.$description_italian.'<span class="required error">*</span></label>
                                                          <div class="form-group" style="margin: 0">
                                                               <input class="form-control"  type="text" name="menu_description_italian[]" id="menu_description_italian" value="'.$menu[0]['description_italian'].'">
                                                        </div>
                                                        '.form_error('menu_description_italian', '<div class="error">', '</div>').'
                                                        <br>
                                                    </div>';
                                                ?>
                                                <div class="" id="add_menu_texbox_div">
                                                </div>
                                            <div class="clearfix"></div>
                                            <!-- <div class="col-md-3 "><a class="btn btn-primary" id="addMoreMenuButton"> Add More Menu</a></div> -->
                                            <div class="clearfix"></div>
                                            </div>
                                            <hr>
                                            <div class="col-md-4">
                                                <label><?=$this->lang->line('Tags')?$this->lang->line('Tags'):'Tags'?> <span class="required error">*</span></label>
                                                <select class="form-control select2" multiple="" name="menu_keywords[]" id="menu_keywords">
                                                    <!-- <option value="">Select Keywords</option> -->
                                                    <?php
                                                        if(count($keywords) > 0 && !empty($keywords)){
                                                            foreach ($keywords as $key => $keywords) {
                                                                $selected = "";
                                                                foreach (json_decode($menu[0]['keywords']) as $key => $menu_keywords) {
                                                                    if($menu_keywords == $keywords['keywords_id']){
                                                                        $selected = 'selected';
                                                                    }
                                                                }
                                                                echo '<option value="'.$keywords['keywords_id'].'"'.$selected.'>'.$keywords['keyword_name'].'</option>';
                                                            }
                                                        }else{
                                                            echo '<option value="">Keywords not available</option>';
                                                        } 
                                                    ?>
                                                </select>
                                                 <?php echo form_error('menu_keywords', '<div class="error">', '</div>'); ?>
                                                <br>
                                            </div>
                                            <div class="col-md-4">
                                                <label><?=$this->lang->line('Filters')?$this->lang->line('Filters'):'Filters'?> <span class="required error">*</span></label>
                                                <select class="form-control select2" multiple="" name="menu_filters[]" id="menu_filters">
                                                    <!-- <option value="">Select Keywords</option> -->
                                                    <?php
                                                        if(count($filters) > 0 && !empty($filters)){
                                                            foreach ($filters as $key => $filters) {
                                                                $selected = "";
                                                                foreach (json_decode($menu[0]['filters']) as $key => $menu_filters) {
                                                                    if($menu_filters == $filters['serial_number']){
                                                                        $selected = 'selected';
                                                                    }
                                                                }
                                                                echo '<option value="'.$filters['serial_number'].'"'.$selected.'>'.$filters['title'].'</option>';
                                                            }
                                                        }else{
                                                            echo '<option value="">Filters not available</option>';
                                                        } 
                                                    ?>
                                                </select>
                                                <?php echo form_error('menu_filters', '<div class="error">', '</div>'); ?>
                                                <br>
                                            </div>
                                            <div class="clearfix"></div>
                                            <br>
                                            <div class="col-md-12">
                                                <fieldset class="form-group">                                               
                                                <a href="javascript:void(0)" onclick="$('#menu_images').click()"><?=$this->lang->line('Upload Image')?$this->lang->line('Upload Image'):'Upload Image'?> (<?=$this->lang->line('You can upload multiple images of 414x325px')?$this->lang->line('You can upload multiple images of 414x325px'):'You can upload multiple images of 414x325px'?> )</a>
                                                 <?php echo form_error('menu_images', '<div class="error">', '</div>'); ?>
                                                <input type="file" id="menu_images" name="menu_images" style="display: block;" class="form-control">
                                                 <span id="menu_images_error"></span>
                                                </fieldset>
                                                <div class="preview-images-zone">
                                                    <?php
                                                    echo'<div class="preview-image preview-show-'.$menu[0]['menu_id'].'"> 
                                                            <div class="image-cancel" data-old_image_id="'.$menu[0]['menu_id'].'" data-no="'.$menu[0]['menu_id'].'">x</div> 
                                                            <div class="image-zone">
                                                            <img id="pro-img-'.$menu[0]['menu_id'].'" src="'.base_url().$menu[0]['image_path'].'">
                                                            </div>
                                                            </div>';
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            </div>
                                            <div class="panel-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-right"><button type="submit" class="btn btn-primary"> <?=$this->lang->line('Submit')?$this->lang->line('Submit'):'Submit'?></button></div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </form>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
            <?php include"include/footer_content.php" ?>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		
	</div>
	<!-- END WRAPPER -->
    <script>
    $(document).ready(function(){
        $('#datetimepicker1').datetimepicker({
          format: 'YYYY-MM-DD HH:mm:ss'
        });

        $('#datetimepicker2').datetimepicker({
          format: 'YYYY-MM-DD HH:mm:ss'
        });
        $('.select-search').select2();
        //code for validate image size start here 11-07-2018
        var _URL = window.URL;
        $("#menu_images").change(function (e) {
            var file, img;
            var maxWidth=415;
            var maxHeight = 326;
            var error_msg = 'Please upload 414x325 image dimention'
            var files = this.files;
            $("#menu_images_error").text('');
            for (let i = 0; i < files.length; i++) {
                if ((file = this.files[i])) {
                    img = new Image();
                    img.onload = function () {
                        console.log("Width:" + this.width + "   Height: " + this.height);//this will give you image width and height and you can easily validate here....
                        if(this.width >maxWidth || this.height >maxHeight){
                            console.log(error_msg);
                            $("#menu_images_error").text(error_msg);
                            $('#menu_images_error').css("color",'red');
                            //$(".preview-images-zone").remove();
                            old_image_url=$('#old_image_path').val();
                            console.log('old image url:'+old_image_url);
                            setTimeout(function(){ 
                                $(".preview-images-zone img").attr("src",base_url()+old_image_url);
                            }, 700);
                            $("#menu_images").val('');
                            return true;
                        }
                    };
                    img.src = _URL.createObjectURL(file);
                }
            }
        });
        //code for validate image size end here 11-07-2018
    });
    /* file uploading code */
    $(document).ready(function() {
        $('.select2').select2();
        document.getElementById('menu_images').addEventListener('change', function(event){ readImage(event), false});
        $( ".preview-images-zone" ).sortable();
        $(document).on('click', '.image-cancel', function() {
        let no = $(this).data('no');
        $(".preview-image.preview-show-"+no).remove();
        });
        // Add more menu event fire on add more button 
        $( "body" ).on( "click", "#addMoreMenuButton", function(e) {
            console.log('add more button is click in add menu page');
            $('#add_menu_texbox_div').append('<div class="col-md-4"><label>Menu Name</label><input class="form-control"  type="text" name="menu_name[]" required><br></div><div class="col-md-6"><label>Menu Description</label><div class="form-group" style="margin: 0"><input class="form-control"  type="text" name="menu_description[]" required></div><br></div><br></div>');
        });
    });
    var num = 4;
    function readImage(event) {
        if (window.File && window.FileList && window.FileReader) {
            var files = event.target.files; //FileList object
            var output = $(".preview-images-zone");
                output.html('');
            for (let i = 0; i < files.length; i++) {
                var file = files[i];
                if (!file.type.match('image')) continue;
                
                var picReader = new FileReader();
                
                picReader.addEventListener('load', function (event) {
                    var picFile = event.target;
                    var html =  '<div class="preview-image preview-show-' + num + '">' +
                                '<div class="image-cancel" data-no="' + num + '">x</div>' +
                                '<div class="image-zone"><img id="pro-img-' + num + '" src="' + picFile.result + '"></div>' +
                                '</div>';

                    output.append(html);
                    num = num + 1;
                });

                picReader.readAsDataURL(file);
            }
            //$("#menu_images").val('');
        } else {
            console.log('Browser not support');
        }
    }
    </script>
	<!-- Javascript -->
	<?php include"include/footer.php" ?> 
    
</body>

</html>

