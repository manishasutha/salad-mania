
		<div class="main">
            <div class="subheader">
                <ul>
                    <li> <?=$this->lang->line('admin')?$this->lang->line('admin'):'Admin';?> / <?=$this->lang->line('Add Terms & Conditions')?$this->lang->line('Add Terms & Conditions'):'Add Terms & Conditions'?></li>
                </ul>
            </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
                <div id="custom_error">
                </div>
                <?php
                    if($this->session->flashdata('success_msg')){
                    $msg = $this->session->flashdata('success_msg');
                        echo '<div class="alert alert-success fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong>'. $msg.'
                            </div>';
                    }
                ?>
                <?php
                    if($this->session->flashdata('error_msg')){
                    $msg = $this->session->flashdata('error_msg');
                        echo '<div class="alert alert-danger fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Error!</strong>'. $msg.'
                            </div>';
                    }
                ?>
				<div class="container-fluid">
					
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
                            <form class="col-md-12 form-panel" style="border:0" method="post" id="PageFrm">
                                <div class="row">
                                    <div class="panel new-panel">
                                            <div class="panel-heading">
                                                <div class="col-md-12">
                                                    <h3 class="panel-title"><?=$this->lang->line('Add Terms & Conditions')?$this->lang->line('Add Terms & Conditions'):'Add Terms & Conditions'?> </h3>
                                                </div>
                                                <br>
                                            </div>
                                            
                                             
                                            <div class="notice-area"></div>
                                            <div class="panel-body no-padding">
                                                <div class="col-md-12">
                                                    <?php  $row = $tc->row();
															$tcpage=$row->tc_page;?>
                                                 <label><?=$this->lang->line('description')?$this->lang->line('description'):'Terms and conditions'?></label>
                                                <textarea name="description" id="description"><?php echo $tcpage;?></textarea>
                                                </div>
                                            </div>
                                               <div class="clearfix"></div>
                                            <div class="panel-body no-padding" style="margin-top:15px;">
                                                <div class="col-md-12">
                                                    <?php  $row = $tc->row();
															$tc_page_arabic=$row->tc_page_arabic;?>
                                                 <label><?=$this->lang->line('description_arabic')?$this->lang->line('description_arabic'):'Terms and conditions(Arabic)'?></label>
                                                <textarea name="description_arabic" id="description_arabic"><?php echo $tc_page_arabic;?></textarea>
                                                </div>
                                            </div>
                                             <!-- <div class="panel-body no-padding" style="margin-top: 10px;">
                                                <div class="col-md-12">
                                                     <label></label>
                                                    <textarea name="description_italian" id="description_italian"></textarea>
                                                </div>
                                            </div> -->
                                            <div class="panel-footer">
                                           <div class="row">
                                        <div class="col-md-12 text-right"><button type="submit" name="submit" class="btn btn-primary">Update</button></div>
                                    </div>
                                            </div>
                                            </div>
                                    </div>
                                </div>
                            </form>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			
		<div class="clearfix"></div>
		
	</div>
	<!-- END WRAPPER -->
	<script>
        $(document).ready(function(){
            //$('.datatable').DataTable();

            $('#datetimepicker1').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
            });

            $('#datetimepicker2').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
            });
            $('.select-search').select2();
            CKEDITOR.replace( 'description' );
            CKEDITOR.replace( 'description_arabic' );
            
        });
    </script>
