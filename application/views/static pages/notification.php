
		<div class="main">
            <div class="subheader">
                <ul>
                    <li> <?=$this->lang->line('admin')?$this->lang->line('admin'):'Admin';?> / <?=$this->lang->line('notification_list')?$this->lang->line('notification_list'):'Notification List';?></li>
                </ul>
            </div>
			<!--  MAIN CONTENT -->
			<div class="main-content">
				 <?php
		            if($this->session->flashdata('success_msg')){
		            $msg = $this->session->flashdata('success_msg');
		                echo '<div class="alert alert-success fade in">
		                    <a href="#" class="close" data-dismiss="alert">&times;</a>
		                    <strong>Success!</strong>'. $msg.'
		                    </div>';
		            }
		        ?>
				<div class="container-fluid">					
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading2">
									<h3 class="panel-title2"><?=$this->lang->line('notification_list')?$this->lang->line('notification_list'):'Notification List';?></h3>
									<!-- <div class="right">
										<a href="<?= base_url() ?>menu/addCategory" class="btn btn-primary"><i class="fa fa-plus"></i> Add menu Category</a>
									</div> -->
								</div>
								<div class="panel-body no-padding">
								<div class="table-responsive">
									<table class="table table-striped datatable">
										<thead>
											<tr>
												<th>S.No</th>
												<th>Notification</th>
												<th>Date</th>
												<th>Action</th>
											</tr>
										</thead>
										<?php
										
												if(!empty($notification) && $notification->num_rows() > 0){
													foreach ($notification->result() as $noti) {
													 $not_url=base_url().'orders/details/'.$noti->module_id.'?notid='.$noti->noti_id;
																
														?>
														<tr>
															<th>ID</th>
															<input type="hidden" name="id" value="<?php echo $noti->noti_id?>">
															<td><a id="seen" name="seen" href="<?php echo $not_url?>"><?php echo $noti->noti_msg?> </a></td>															
															<td><?php echo date("d-m-Y, h:i A", strtotime($noti->created_at));?></td>
															<td>	<a title="Delete Notification!" class="btn btn-xs btn-danger delete" href="<?=base_url()?>notification/delete/<?=$noti->noti_id;?>"><i class="fa fa-trash"></i>
                                                			 	</a>
															 		
															 </td>
															<?php 
													}
												}
												?>				
										<tbody>
											
										</tbody>
									</table>
								</div>
								</div>
								<div class="panel-footer">
									<div class="row">
                                        <div class=" text-left">
                                            
                                        </div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
         
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		
	</div>
	<!-- END WRAPPER -->
	
