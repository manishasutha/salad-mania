
	<!-- MAIN -->
	<div class="main">
		<div class="subheader">
			<ul>
				<li> <?=$this->lang->line('admin')?$this->lang->line('admin'):'Admin';?> / Website Banners List</li>
			</ul>
			<div class="right" style="float: right">
				<a  id="add_banner" class="btn add btn-primary button-fix"><i class="fa fa-plus"></i> <?=$this->lang->line('add_banner')?$this->lang->line('add_banner'):'Add Website Banners';?></a>
			</div>
		</div>
		<!--  MAIN CONTENT -->
		<div class="main-content">
			<?php
			if($this->session->flashdata('success_msg')){
				$msg = $this->session->flashdata('success_msg');
				echo '<div class="alert alert-success fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Success!</strong>'. $msg.'
			        		</div>';
			}
			if($this->session->flashdata('error_msg')){
				$msg = $this->session->flashdata('error_msg');
				echo '<div class="alert alert-danger fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Error!</strong>'. $msg.'
			        		</div>';
			}
			?>
			<div class="container-fluid">

				<!-- END OVERVIEW -->
				<div class="row">
					<div class="col-md-12">
						<!-- RECENT PURCHASES -->
						<div class="panel">
							<div class="panel-heading2">
								<h3 class="panel-title2">Website Banners List</h3>

							</div>
							<div class="panel-body no-padding">
								<div class="table-responsive">
									<table class="table table-striped datatable">
										<thead>
										<tr>
											<th>S.No</th>
											<th>Banner</th>
											<th>Action</th>
										</tr>
										</thead>
										<tbody>
                                        	<?php
										    if(!empty($query) && $query->num_rows() > 0){
											    $i=1;
											    foreach ($query->result() as $banner) { ?>
                                                    <tr>
                                                        <th><?php echo $i?></th>
                                                        <td> 
                                                            <?php if( $banner->banner_name!=''){ ?>
                                                                <img style="height:50px;width:50px;" class="post-thumb" src="<?php echo base_url(); ?>uploads/banners/<?php echo $banner->banner_name; ?>"></td>
                                                        <?php } else { ?>
                                                                    <h4> N/A</h4>
                                                        <?php } ?>
                                                        <td>
                                                            <a  title="Delete Banner!" class="btn btn-xs btn-danger delete" href="<?=base_url()?>pages/banner/delete/<?=$banner->banner_id;?>">
                                                                <i class='fa fa-trash'></i>
                                                            </a>
                                                        </td>
                                                    </tr>
											<?php $i++;} } ?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="panel-footer">
								<div class="row">
									<div class=" text-left">

									</div>
								</div>
							</div>
						</div>
						<!-- END RECENT PURCHASES -->
					</div>
				</div>
			</div>
		</div>
		
		<!-- END MAIN -->
		<div class="clearfix"></div>

	</div>
	<!-- END WRAPPER -->
	<!--Add website banner model start added -->
	<div class="modal" id="addbannerModel"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
            <form enctype="multipart/form-data" id="add_banner" action="<?=base_url()?>pages/banner/add" method="post">
				<div class="modal-content">
					<div class="modal-header text-left">
						<h4 class="modal-title w-100 font-weight-bold"><?=$this->lang->line('Add Website Banners')?$this->lang->line('Add Website Banners'):'Add Website Banners'?> </h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -27px;opacity: 0.6;">
							&times;
						</button>
					</div>
					<div class="modal-body mx-2">
                        <div class="row" style="margin-bottom: 30px">
                            <div class="col-lg-12">
                                <div class="md-form mb-3 ">
                                    <label for="image">Website Banner Image</label>
									<br>
										<label class="text text-danger">Note: Banner Image width:1349px and height:620px</label>
                                    <input type="file" id="image" name="Image" class="form-control validate" style="color:black">
                                </div>
                            </div>
                        </div>
					</div>
					<div class="modal-footer d-flex button justify-content-center">
                                <button type="submit" name="submit" class="btn btn-primary">Submit</button></div>
					</div>
			</form>
		</div>
	</div>
	<!-- Add website banner model end -->

    <script>
        $(".close").click(function(){
            $("#addbannerModel").hide();
        });
    </script>
	<script>
		$(document).ready(function(){

			$('form[id="add_banner"]').validate({
				rules: {
					Image:
					{
						required:true,
					},
				},
				messages: {
					Image: {
						required: "Image must not be empty",
					},
				}
			});
		});
	</script>