
		<div class="main">
                    <div class="subheader">
                        <ul>
                            <li> <?=$this->lang->line('admin')?$this->lang->line('admin'):'Admin';?> </li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title"><?=$this->lang->line('dashboard')?$this->lang->line('dashboard'):'Dashboard';?></h3>
						</div>
						<div class="panel-body">
							<div class="row">
							<a href="<?=base_url().'users/'?>">
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-users"></i></span>
										<p>
											<span class="number"><?= $total_users?$total_users:0 ?></span>
											<span class="title"><?=$this->lang->line('app_users')?$this->lang->line('app_users'):'Total Users';?></span>
										</p>
									</div>
								</div>
								</a>
								<a href="<?=base_url().'orders'?>">
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-rocket"></i></span>
										<p>
											<span class="number"><?= $total_orders?$total_orders:0 ?></span>
											<span class="title"><?=$this->lang->line('orders')?$this->lang->line('orders'):'Total Orders';?></span>
										</p>
									</div>
								</div>
								</a>
								<a href="<?=base_url().'orders'?>">
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-bar-chart"></i></span>
										
										<?php $counting = 0;
											foreach ($collect->result() as $count) {
												 $counting = $counting + $count->grand_total;
												
											}  ?> <p> <span  class="number"> <?= $counting?$counting:0 ?></span>
											<span class="title">Todays Sale</span>
										</p>
									</div>
								</div>
								</a>
									<a href="<?=base_url().'view-salads'?>">
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-tag"></i></span>
										<p>
											<span class="number"><?= $total_salads?$total_salads:0 ?></span>
										<span class="title"><?=$this->lang->line('salads')?$this->lang->line('salads'):'Total Recipes';?></span>
										</p>
									</div>
								</div>
								</a>
								
								
							</div>
						</div>
					</div>

                    <!-- CHART -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- RECENT PURCHASES -->
                            <div class="panel">
                                <div class="panel-heading">
                                    <!--                                    <h3 class="panel-title chart">CHART</h3>-->
                                    <div class="nav-border">
                                        <div>
                                            <select id="graph" name="graph" class="select-box">
                                                <option value="user">User</option>
                                                <option value="order">Order</option>
                                            </select>
                                        </div>

                                        <div class="right right-menu">
                                            <div class="col-xl-12 ">
                                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                                    <li class="nav-item active">

                                                        <a class="nav-link " id="weekly-tab" data-toggle="tab" href="#weekly-active-account" role="tab" aria-controls="home" aria-selected="false">Weekly</a>

<!--                                                        <a class="nav-link" id="weekly-tab" data-toggle="tab" href="#weekly-active-account" role="tab" aria-controls="home" aria-selected="false">Weekly</a>-->

                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="monthly-tab" data-toggle="tab" href="#monthly-active-account" role="tab" aria-controls="Monthly" aria-selected="false">Monthly</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="yearly-tab" data-toggle="tab" href="#yearly-active-account" role="tab" aria-controls="Yearly" aria-selected="false">Yearly</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-body padding">
                                    <div id="weekly-active-account" style="height: 401px" >
                                        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END CHART -->

					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<!-- <div class="panel-heading">
									<h3 class="panel-title"><?=$this->lang->line('recent_users')?$this->lang->line('recent_users'):'Recent Orders';?></h3>
									<div class="right">
										<button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
										<button type="button" class="btn-remove"></button>
									</div>
								</div> -->
								<div class="panel-body no-padding">
								<div class="table-responsive">
									<table class="table table-striped datatable">
										<thead>
										<!-- <tr>
												<th>S.No</th>
												<th>Order Id</th>
												<th>Ordered By</th>
												<th>Store Pick Up</th>
												<th>Payment Method</th>
												<th>Grand Total</th>
												<th>Status</th>
												<th>Action</th>
											</tr> -->
										</thead>
										<tbody>
									<!-- 
												<?php
															// $row = $encryptId->row();
															// $order_id=$row->order_id;
												if(!empty($query) && $query->num_rows() > 0){
													foreach ($query->result() as $order) {?>
													<tr>
														<th>ID</th>
														<td><?php echo strtoupper(encryptId($order->order_id));?></td>
														<td><?php echo $order->full_name;?></td>
															  <td>
																<?php if ($order->in_store_pickup==1 ) : ?>
																		Yes
																	<?php else : ?>
																		Delivered
																	<?php endif; ?>
															</td>
															
															<td><?php echo $order->payment_method;?></td>
															 <td><?php echo $order->grand_total;?></td>
															 <td> <span class="label label-success"><?php if($order->o_status==1 )
																				{
																					echo "Placed";
																				}
																				else if ($order->o_status==2 )
																				{
																					echo "Preparing";
																				}
																				else if ($order->o_status==3 )
																				{
																				echo "Ready to Ship";
																				}
																				else if ($order->o_status==4 )
																				{
																				echo "Out for Delivery";
																				}
																				else if ($order->o_status==5 )
																				{
																					echo "Delivered";
																				}
																				else if ($order->o_status==6 )
																				{
																					echo "Cancelled";
																				}?></span></td>
															
															 <td>
                                                			 	<a  class="btn btn-xs btn-primary edit" id="changeorder" data-cat-id="<?php echo $order->order_id;?>">change status
                                                			 	<input type="hidden" name="id" value="<?php echo $order->order_id;?>" id="cat_edit_id">
																 </a>
                                                			
                                                			 	<a title="View Order!" class="btn btn-xs btn-info " name="detail" href="<?=base_url()?>orders/details/<?=$order->order_id?>">
                                                			 		<i class="fa fa-info-circle" ></i><input type="hidden" name="orderid" value="<?php echo $order->order_id;?>" id="cat_edit_id">
                                                			 	</a>
                                                			 </td>
                                                			 </tr>
															
												<?php	}
												}
												?>									 -->
										</tbody>
									</table>
								</div>
								</div>
								<!-- <div class="panel-footer">
									<div class="row">
										<div class=" text-right"><a href="<?=base_url()?>orders" class="btn btn-primary"><?=$this->lang->line('view_all_orders')?$this->lang->line('view_all_orders'):'View All Orders';?></a></div>
									</div>
								</div> -->
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>



				</div>
			</div>
			<!-- END MAIN CONTENT -->
                       
		<!-- END MAIN -->
		<div class="clearfix"></div>
			<div class="modal" id="editCatModal"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <form id="form" action="<?=base_url()?>orders/change_status" method="post">
			<div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header text-left">
		                <h4 class="modal-title w-100 font-weight-bold">Change Status </h4>
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -27px;opacity: 0.6;">
		                   &times;
		                </button>
		            </div>
					   <input type="hidden" name="id" id="cat_edit_id">
		            <div class="modal-body mx-2">
						<div class="md-form mb-3 ">
		                	<label data-error="wrong" data-success="right" for="cat_edit_name">Order Status</label>
		                	<br><span id="unit_type_error"></span>
		                <select type="text" id="status" name="status" class="form-control validate">
						<option value="">Choose status</option>
							<option value="1">Placed</option>
							<option value="2">Preparing</option>';
							<option value="3">Ready to Ship</option>';
							<option value="4">Out for Delivery</option>';
							<option value="5">Delivered</option>';
								</select>
								
								</div>
		            </div>
		            <div class="modal-footer d-flex button justify-content-center">
		                  <button class="btn-primary status_change" id="button" name="submit" type="submit">Submit</button>

				    </div>
		        </div>
		    </div>
			</form>
		</div>
	</div>


		<script type="text/javascript">

			$( document ).ready(function() {
				//$('body').load(function(){
					var graph = $('#graph').val();
					var time = $('.active .nav-link').html();
					loadGraph(graph, time); //Function calling
				//});
			});

			$("a.nav-link").click(function(){
				var graph=$('#graph').val();
				var time=$(this).html();
				loadGraph(graph, time); //Function calling					
			});

			$("select#graph").change(function(){
				var graph=$('#graph').val();
				var time = $('.active .nav-link').html();
				loadGraph(graph, time); //Function calling					
			});
			

			//Graph method defination
			function loadGraph(graph,time){
				if(graph!=="" && time!==""){
					$.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>Stapages/load_graph",
						data: {graph: graph,time:time},
						dataType: "json",
						success: function (response) {
							/**** active user ****/

							if(response['status']===true && response['time']==='week'){
								var data=response['data'];

								var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
								var d = new Date(data[0]['create_date']);
								var one = days[d.getDay()];

								var d = new Date(data[1]['create_date']);
								var two = days[d.getDay()];

								var d = new Date(data[2]['create_date']);
								var three = days[d.getDay()];

								var d = new Date(data[3]['create_date']);
								var four = days[d.getDay()];

								var d = new Date(data[4]['create_date']);
								var five = days[d.getDay()];

								var d = new Date(data[5]['create_date']);
								var six = days[d.getDay()];

								var d = new Date(data[6]['create_date']);
								var seven = days[d.getDay()];

								Highcharts.chart('container', {
									chart: {
										type: 'column'
									},
									title: {
										text: response['heading']
									},
									xAxis: {
										categories: [one,two,three,four,five,six,seven],

									},
									yAxis: {
										min: 0,
										title: {
											text: response['side']
										}
									},
									exporting: { enabled: false },

									plotOptions: {
										column: {
											pointPadding: 0.2,
											borderWidth: 0
										}
									},
									colors: [
										'#91cc34'
									],
									series: [{
										name: response['down'],
										data: [ Number(data[0]['users']), Number(data[1]['users']), Number(data[2]['users']), Number(data[3]['users']), Number(data[4]['users']), Number(data[5]['users']), Number(data[6]['users'])],

									}]
								});



							}
							if(response['status']===true && response['time']==='month'){
								data=response['data'][0];

								Highcharts.chart('container', {
									chart: {
										type: 'column'
									},
									title: {
										text: response['heading']
									},

									xAxis: {
										categories: [
											'Jan',
											'Feb',
											'Mar',
											'Apr',
											'May',
											'Jun',
											'Jul',
											'Aug',
											'Sep',
											'Oct',
											'Nov',
											'Dec'
										],

									},
									yAxis: {
										min: 0,
										title: {
											text: response['side']
										}
									},
									exporting: { enabled: false },

									plotOptions: {
										column: {
											pointPadding: 0.2,
											borderWidth: 0
										}
									},
									colors: [
										'#91cc34'
									],
									series: [{
										name: response['down'],
										data: [ data['Jan'],data['Feb'],data['Mar'],data['Apr'],data['May'],data['Jun'],data['Jul'],data['Aug'],data['Sept'],data['Oct'],data['Nov'],data['Dec'] ],

									}]
								});

							}
							if(response['status']===true && response['time']==='year'){
								data=response['data'];


								Highcharts.chart('container', {
									chart: {
										type: 'column'
									},
									title: {
										text: response['heading']
									},
									xAxis: {
										labels: [2019],

									},
									yAxis: {
										min: 0,
										title: {
											text: response['side']
										}
									},
									exporting: { enabled: false },

									plotOptions: {
										column: {
											pointPadding: 0.2,
											borderWidth: 0
										}
									},
									colors: [
										'#91cc34'
									],
									series: [{
										name: response['down'],
										data: [data['2019']]

									}]
								});

							}

						}
					});
				}else{
					alert('Please select graph and time ')
				}
			}

		</script>

		<script type="text/javascript">
			$("a.nav-link").click(function(){
				var graph=$('#graph').val();
				var time=$(this).html();


				if(graph!=="" && time!==""){
					$.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>Stapages/load_graph",
						data: {graph: graph,time:time},
						dataType: "json",
						success: function (response) {
							/**** active user ****/

							if(response['status']===true && response['time']==='week'){

								var data=response['data'];
								var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
								var d = new Date(data[0]['create_date']);
								var one = days[d.getDay()];

								var d = new Date(data[1]['create_date']);
								var two = days[d.getDay()];

								var d = new Date(data[2]['create_date']);
								var three = days[d.getDay()];

								var d = new Date(data[3]['create_date']);
								var four = days[d.getDay()];

								var d = new Date(data[4]['create_date']);
								var five = days[d.getDay()];

								var d = new Date(data[5]['create_date']);
								var six = days[d.getDay()];

								var d = new Date(data[6]['create_date']);
								var seven = days[d.getDay()];

								Highcharts.chart('container', {
									chart: {
										type: 'column'
									},
									title: {
										text: response['heading']
									},
									xAxis: {
										categories: [one,two,three,four,five,six,seven]

									},
									yAxis: {
										min: 0,
										title: {
											text: response['side']
										}
									},
									exporting: { enabled: false },

									plotOptions: {
										column: {
											pointPadding: 0.2,
											borderWidth: 0
										}
									},
									colors: [
										'#91cc34'
									],
									series: [{
										name: response['down'],
										data: [Number(data[0]['users']), Number(data[1]['users']), Number(data[2]['users']), Number(data[3]['users']), Number(data[4]['users']), Number(data[5]['users']), Number(data[6]['users'])],
										backgroundColor: ['#91cc34','#91cc34','#91cc34','#91cc34','#91cc34','#91cc34','#91cc34']

									}]
								});



							}
							if(response['status']===true && response['time']==='month'){
								data=response['data'][0];

								Highcharts.chart('container', {
									chart: {
										type: 'column'
									},
									title: {
										text: response['heading']
									},
									xAxis: {
										categories: [
											'Jan',
											'Feb',
											'Mar',
											'Apr',
											'May',
											'Jun',
											'Jul',
											'Aug',
											'Sep',
											'Oct',
											'Nov',
											'Dec'
										],

									},
									yAxis: {
										min: 0,
										title: {
											text: response['side']
										}
									},
									exporting: { enabled: false },

									plotOptions: {
										column: {
											pointPadding: 0.2,
											borderWidth: 0
										}
									},
									colors: [
										'#91cc34'
									],
									series: [{
										name: response['down'],
										data: [ Number(data['Jan']),Number(data['Feb']),Number(data['Mar']),Number(data['Apr']),Number(data['May']),Number(data['Jun']),Number(data['Jul']),Number(data['Aug']),Number(data['Sept']),Number(data['Oct']),Number(data['Nov']),Number(data['Dec']) ],

									}]
								});

							}
							if(response['status']===true && response['time']==='year'){
								data=response['data'];


								Highcharts.chart('container', {
									chart: {
										type: 'column'
									},
									title: {
										text: response['heading']
									},
									xAxis: {
										labels: [2019],

									},
									yAxis: {
										min: 0,
										title: {
											text: response['side']
										}
									},
									exporting: { enabled: false },

									plotOptions: {
										column: {
											pointPadding: 0.2,
											borderWidth: 0
										}
									},
									colors: [
										'#91cc34'
									],
									series: [{
										name: response['down'],
										data: [Number(data['2019'])]

									}]
								});

							}

						}
					});
				}else{
					alert('Please select graph and time ')
				}
			});
		</script>
	