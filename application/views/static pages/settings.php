<!-- MAIN -->
<div class="main">
    <div class="subheader">
        <ul>
            <li> <?=$this->lang->line('admin')?$this->lang->line('admin'):'Admin';?> / Add Settings</li>
        </ul>
    </div>
	<!-- MAIN CONTENT -->
	<div class="main-content">
        <?php
            if($this->session->flashdata('success_msg')){
            $msg = $this->session->flashdata('success_msg');
                echo '<div class="alert alert-success fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Success!</strong>'. $msg.'
                    </div>';
            }
        ?>
        <?php
            if($this->session->flashdata('error_msg') || !empty($error)){
                if(!empty($error)){
                    $msg = $error;

                }else{
                    $msg = $this->session->flashdata('error_msg');
                }
                echo '<div class="alert alert-danger fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Error!</strong>'. $msg.'
                    </div>';
            }
        ?>
		<div class="container-fluid">
			
			<!-- END OVERVIEW -->
			<div class="row">
				<div class="col-md-12">

					<!-- RECENT PURCHASES -->
                    <form class="col-md-12 form-panel" id="settings" method="post" enctype="multipart/form-data">
                        <div class="row">
                           <div class="panel new-panel">
                                <div class="panel-heading">
                                    <div class="col-md-12">
                                        <h3 class="panel-title">Add Settings</h3>
                                        
                                    </div>
                                    <br>
                                </div>
                                  <?php 
                                $row = $about->row();$app_name=$row->app_name; $app_logo=$row->app_logo; $contact_email=$row->contact_email; $contact_phone=$row->contact_phone; $tax_rate=$row->tax_rate;
                                $delivery_charge=$row->delivery_charge;$fb_url=$row->fb_url;$twitter_url=$row->twitter_url;$gplus_url=$row->gplus_url;$insta_url=$row->insta_url;$linkedin_url=$row->linkedin_url;
                                $address=$row->address;$monday_friday_hrs=$row->monday_friday_hrs;$saturday_hrs=$row->saturday_hrs;$sunday_hrs=$row->sunday_hrs;$about_us_link=$row->about_us_link;$our_story_link=$row->our_story_link;?>
                                <div class="notice-area"></div>
                                 <div class="panel-body no-padding">
                                    <div class="col-md-6">
                                        <label>App Name<span class="required error">*</span></label>
                                        <input class="form-control" autocomplete="off" name="app_name" id="app_name" value="<?php echo $app_name;?>" maxlength="30" type="text">
                                    </div> 
                                    <!--<div class="col-md-6">
                                        <label>App Logo<span class="required error">*</span></label>
                                        <input class="form-control" value="<?php echo $app_logo;?>" name="app_logo" id="app_logo" type="file">
                                        <input name="image" type="hidden" value="<?php echo $app_logo;?>">
                                        <img style="height:50px;width:50px;" src="<?= base_url(); ?>/uploads/logo/<?php echo $app_logo; ?>">
                                    </div>-->
                                    <div class="col-md-6">
                                        <label>Address<span class="required error">*</span></label>
                                        <input class="form-control" autocomplete="off" name="address" id="address" value="<?php echo $address;?>" maxlength="50" type="text">
                                    </div> 
                                    <div class="clearfix"></div><br>

                                    <div class="col-md-4">
                                        <label>Sunday-Thursday Hrs<span class="required error">*</span></label>
                                        <input class="form-control" autocomplete="off" name="monday_friday_hrs" id="monday_friday_hrs" value="<?php echo $monday_friday_hrs;?>" maxlength="30" type="text">
                                    </div> 
                                    
                                    <div class="col-md-4">
                                        <label>Friday Hrs<span class="required error">*</span></label>
                                        <input class="form-control" autocomplete="off" name="saturday_hrs" id="saturday_hrs" value="<?php echo $saturday_hrs;?>" maxlength="30" type="text">
                                    </div> 
                                     
                                    <div class="col-md-4">
                                        <label>Saturday Hrs<span class="required error">*</span></label>
                                        <input class="form-control" autocomplete="off" name="sunday_hrs" id="sunday_hrs" value="<?php echo $sunday_hrs;?>" maxlength="30" type="text">
                                    </div> 
                                   

                                    <div class="clearfix"></div><br>

                                    <div class="col-md-4">
                                        <label>Dietician Email</label>
                                        <input class="form-control" autocomplete="off" name="diet_email" value="<?php echo $row->diet_email; ?>" id="diet_email" maxlength="255" type="text">
                                    </div> 

                                    <div class="col-md-4">
                                        <label>Contact Email</label>
                                        <input class="form-control" autocomplete="off" name="contact_email" value="<?php echo $contact_email;?>" id="contact_email" maxlength="255" type="text">
                                    </div> 
                                    <div class="col-md-4">
                                        <label>Contact Phone</label>
                                        <input class="form-control" autocomplete="off" name="contact_phone" value="<?php echo $contact_phone;?>" id="contact_phone" maxlength="255" type="text">
                                    </div> 
                                    <div class="clearfix"></div><br>

                                    <!--Ingredients Box Start-->
                                    <div class="col-md-6">
                                        <label>Tax Rate </label>
                                        <input class="form-control" autocomplete="off" name="tax_rate" value="<?php echo $tax_rate;?>" id="tax_rate" maxlength="255" type="text">
                                    </div> 
                                    <div class="col-md-6">
                                        <label>Delivery Charges</label>
                                        <input class="form-control" autocomplete="off" name="delivery_charge" value="<?php echo $delivery_charge;?>" id="delivery_charge" maxlength="255" type="text">
                                    </div>
                                    <div class="clearfix"></div><br>

                                    <!--<div class="col-md-6">
                                         <label>About-us-link</label>
                                         <input class="form-control" autocomplete="off" name="about_us_link" value="<?php echo $about_us_link;?>" id="about_us_link" maxlength="255" type="text">
                                     </div>-->

                                    <div class="col-md-4">
                                         <label>Our Story Video Link</label>
                                         <input class="form-control" autocomplete="off" name="our_story_link" value="<?php echo $our_story_link;?>" id="our_story_link" maxlength="255" type="text">
                                     </div>

                                    <div class="col-md-4">
                                        <label>Fb Url</label>
                                        <input class="form-control" autocomplete="off" name="fb_url" value="<?php echo $fb_url;?>" id="fb_url" maxlength="255" type="text">
                                    </div> 
                                    <div class="col-md-4">
                                        <label>Twitter Url</label>
                                        <input class="form-control" autocomplete="off" name="twitter_url" value="<?php echo $twitter_url;?>" id="twitter_url" maxlength="255" type="text">
                                    </div> 

                                    <div class="clearfix"></div><br>
                                        
                                    <div class="col-md-4">
                                        <label>Google Plus Url</label>
                                        <input class="form-control" autocomplete="off" name="gplus_url" value="<?php echo $gplus_url;?>" id="gplus_url" maxlength="255" type="text">
                                    </div> 

                                    <div class="col-md-4">
                                        <label>Insta Url</label>
                                        <input class="form-control" autocomplete="off" name="insta_url" value="<?php echo $insta_url;?>" id="insta_url" maxlength="255" type="text">
                                    </div> 
                                    <!--Ingredients Box End-->
                                    
                                    <div class="col-md-4">
                                        <label>Linkedin Url</label>
                                        <input class="form-control" autocomplete="off" name="linkedin_url" value="<?php echo $linkedin_url;?>" id="linkedin_url" maxlength="255" type="text">
                                    </div> 
                                   
                                     <!-- <div class="col-md-6">
                                             <select id="boot-multiselect-demo" class="form-control select2" multiple>
                                                    <option value="jQuery">jQuery Tutorials</option>
                                                    <option value="Bootstrap">Bootstrap Framework</option>
                                                    <option value="HTML">HTML</option>
                                                    <option value="CSS" >CSS</option>
                                                    <option value="Angular">Angular</option>
                                                    <option value="Angular">javascript</option>
                                                    <option value="Java">Java</option>
                                                    <option value="Python">Python</option>
                                                    <option value="MySQL">MySQL</option>
                                                    <option value="Oracle">Oracle</option>

                                            </select>
                                            </div> -->


                                   
                                    <hr>
                                </div>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-md-12 text-right"><button type="submit" name="submit" class="btn btn-primary">UPDATE</button></div>
                                    </div>
                                      </div>
                        </div>
                    </form>
					<!-- END RECENT PURCHASES -->
				</div>
			</div>
		</div>
	</div>

 
    <script>
  
    $(document).ready(function() {
                             

        //Form Validation STart
        $('form[id="settings"]').validate({
            rules: {
                app_name: {
                    required : true,
                    minlength: 2,
                    maxlength: 30,
                },
                address:
                 {
                    required : true,
                    minlength: 2,
                    maxlength: 50,
                },
                monday_friday_hrs:
                {
                    required : true,
                    minlength: 2,
                    maxlength: 30,
                    alphanumeric: true,
                },
                saturday_hrs:
                {
                    required : true,
                    minlength: 2,
                    maxlength: 30,
                    alphanumeric: true,
                },
                /*about_us_link:{
                  required:false,
                    url:true
                },
                our_story_link:{
                    required:false,
                    url:true
                },*/
                sunday_hrs:
                {
                    required : true,
                    minlength: 2,
                    maxlength: 30,
                    alphanumeric: true,
                },
                contact_email: 
                {  
                    	required:true,
						minlength: 2,
						maxlength: 50,
						 email: true,
				},
				 contact_phone: 
                {  
                    	required:true,
						minlength: 5,
						maxlength: 14,
						number: true,
				},
               
                tax_rate: {
                    required: true,
                     number: true
                },
                delivery_charge: {
                    required: true,
                    number: true
                },
                /*fb_url: {
                    required: true,
                    facebook:true,
                },
                twitter_url: {
                    required: true,
                //    twitter:true,
                   
                },
                gplus_url: {
                    required: true,
                     url: true
                  
                },
                 insta_url: {
                    required: true,
                    // instagram:true,
                   
                },
                 linkedin_url: {
                    required: true,
                    // linkedin:true,
                   
                }*/
            },
            messages: {
                //salad_name: "Please enter salad name",
               contact_phone: {
                    required: "Please enter Phone no",
                    number: "Phone no should be in digits only",
				},
				contact_email: {         
					email: "Please enter a valid email address",
				},
            },
            submitHandler: function(form) {
                //Here we can any custom work like AJAX calling

                if(false){ } 
                else {
                form.submit();
                }
            }
        });

        $.validator.addMethod("letterswithspace", function(value, element) {
            return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
        }, "Please enter only alphabets");
        $.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z]+$/i.test(value);
        }, "Please enter only alphabets");
        $.validator.addMethod("alphanumeric", function(value, element) {
            return this.optional(element) || /^[A-za-z0-9.\s]+$/i.test(value);
        }, "Letters, numbers, and underscores only please");
      $.validator.addMethod("number", function(value, element) {
            return this.optional(element) || /^[0-9+.\s]+$/i.test(value);
        }, "Numbers allow only please");
         $.validator.addMethod("facebook", function(value, element) {
            return this.optional(element) || /^[(http|https)\:\/\/www.facebook.com\/.*]+$/i.test(value);
        }, "Facebook url must be in valid format");

        $.validator.addMethod("url", function(value, element) {
            return this.optional(element) || /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi.test(value);
        }, "Please enter valid url");



            
    });
    
    
    
</script>

<!-- Javascript -->
