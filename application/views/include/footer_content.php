
    <div class="subfooter">
            <div class="col-sm-4">
                <ul>
                    <li><p class="copyright copyright-left">&copy;<?php echo date("Y")?> <a href="http://www.salad-mania.com/" target="_blank">SaladMania</a></p></li>
                </ul>
            </div>
            <div class="col-sm-8">
                <ul>
                    <li><p class="copyright"><?=$this->lang->line('All_Rights')?$this->lang->line('All_Rights'):'All Rights Reserved: Salad Mania '?> </p></li>
                </ul>
            </div>
        </div>
    </div>
