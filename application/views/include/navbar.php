<nav class="navbar navbar-default navbar-fixed-top">
	<div class="brand">
		<a href="<?= base_url()?>admin"> <img height="50px" src="<?= base_url(); ?>assets/frontend/img/logo.png"/></a>
	</div>
	<div class="container-fluid">
		<div class="navbar-btn">
			<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-menu"></i></button>
		</div>
		<form class="navbar-form navbar-left">
			<ul>
				<!-- <li>
                                        	<?php
				$role = $this->session->userdata('user_role');
				$user_id = $this->session->userdata('user_id');
				$last_login = $this->session->userdata('last_login');
				$recipient_id = 0;
				if($role == 1){
					echo 'Administrator';
					$recipient_id = $user_id;
				}else if($role == 2){
					if(!empty($customer)){
						echo $customer[0]['name'];
						$recipient_id = $customer[0]['serial_number'];
					}
				}
				?>
                                        </li> -->
				<?php
				if($this->session->userdata('site_lang') == 'italian'){
					//$selected_language = $this->lang->line('italian')?$this->lang->line('italian'):'English';
					$selected_language = "Italian";
				}else{

					$selected_language = "English";
				}

				?>
				<!-- <li class="hidden-xs"><?=$this->lang->line('last_login')?$this->lang->line('last_login'):'Last Login'?> : <span><?=$last_login?$last_login:date('y-m-d h:i:sa') ?></span></li>
                                        <li class="hidden-xs"><?=$this->lang->line('language')?$this->lang->line('language'):'Language'?>: <span><?=ucfirst($selected_language);?></span></li> -->
			</ul>
		</form>
		<div id="navbar-menu">
			<ul class="nav navbar-nav navbar-right">
				<?php
				$sql="select noti_id,noti_msg,module_id,noti_for,seen_status,created_at,updated_at from sm_notifications where noti_for='A' AND seen_status='0' order by noti_id desc limit 0,20";
				$notifications=$this->Main_model->__callMasterquery($sql);
				//	print_r($notifications);exit;

				if(!empty($notifications) && $notifications->num_rows() >0){
					$total_noti = $notifications->num_rows();
				}else{
					$total_noti = '';
				}
				?>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
						<i class="lnr lnr-alarm"></i>
						<span class="badge bg-danger"><?=$total_noti?></span>
					</a>
					<ul class="dropdown-menu notifications">
						<?php

						if(!empty($notifications) && $notifications->num_rows() >0){
							foreach ($notifications->result() as $noti) {

								$not_url=base_url().'orders/details/'.$noti->module_id.'?notid='.$noti->noti_id;

								echo '<li><a href="'.$not_url.'">'.$noti->noti_msg .'<br>'.date("d-m-Y, h:i A", strtotime($noti->created_at)).' </a></li>';

							}
							echo '<li><a href="'.base_url().'notification" class="more">See all notifications</a></li>';
						}else{
							echo '<li><a class="more">Notifications not avilable.</a></li>';
						}
						?>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle users" data-toggle="dropdown"><img src="<?= base_url() ?><?= $this->session->userdata('user_profile_image')?>"  class="img-circle" alt="Avatar"> <span>
							<?php $sql="select app_name from sm_app_settings";
							$name=$this->Main_model->__callMasterquery($sql);
							$row = $name->row();
							$app_name=$row->app_name;
							?>
							<?= $app_name; ?><i class="icon-submenu lnr lnr-chevron-down"></i></a>
					<ul class="dropdown-menu">
						<!--<li><a href="profile/profile_change"><i class="lnr lnr-user"></i> <span>'My Profile'</span></a></li>-->
						<li><a href="<?= base_url() ?>profile/changePassword"><i class="fa fa-key"></i> <span><?=$this->lang->line('change_password')?$this->lang->line('change_password'):'Change Password'?></span></a></li>
						<!-- <li><a href="#"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li> -->
						<li><a href="<?= base_url() ?>auth/logout"><i class="lnr lnr-exit"></i> <span><?=$this->lang->line('logout')?$this->lang->line('logout'):'Logout'?></span></a></li>
					</ul>
				</li>
				<!-- <li class="dropdown">
                    <a href="#" class=" icon-menu" >
                        <i class="lnr lnr-power-switch"></i>
                    </a>

                </li> -->
			</ul>
		</div>
	</div>
</nav>