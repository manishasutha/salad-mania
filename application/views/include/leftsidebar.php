<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<?php
						if($role == 1){
							?>
							
							 <!-- <li>
	                        	
								<a href="#settingMenu" data-toggle="collapse" class="<?=$setting?'active':'collapsed'?>"><i class="lnr lnr-cog"></i> <span><?=$this->lang->line('category')?$this->lang->line('category'):'Category'?> </span> <i class="icon-submenu lnr lnr-chevron-right"></i></a>
								<div id="settingMenu" class="collapse <?=$setting?'in':''?>">
									<ul class="nav">
										<li><a href="<?= base_url() ?>category/" class="<?=$page_title == 'Category'?'active':''?><?=$page_title == 'Add Category'?'active':''?>"><i class="lnr lnr-briefcase"></i> <span><?=$this->lang->line('category')?$this->lang->line('category'):'Category List'?>  </span></a></li>
									
									</ul>
								</div>
							</li>            -->
							
							
							<li>
							<a href="<?= base_url() ?>admin" class="<?=$page_title == 'Admin'?'active':''?>"><i class="lnr lnr-home"></i> <span><?=$this->lang->line('Dashboard')?$this->lang->line('Dashboard'):'Dashboard'?> </span>
							</a>
							</li>
							<!--Category start-->
							<li>
	                        	<?php
	                        		if($page_title =='category' || $page_title =='sub-category'){
	                        			$setting = true;
	                        		}else{
	                        			$setting = false;
	                        		} 
	                        	?>
								<a href="#category" data-toggle="collapse" class="<?=$setting?'active':'collapsed'?>"><i class="lnr lnr-briefcase"></i>  <span>Categories</span> <i class="icon-submenu lnr lnr-chevron-right"></i></a>
								<div id="category" class="collapse <?=$setting?'in':''?>">
									<ul class="nav">
										<li><a href="<?= base_url() ?>category/" class="<?=$page_title == 'Category'?'active':''?>"><i class="lnr lnr-briefcase"></i> <span>Main Category</a></li>					
										<li><a href="<?= base_url() ?>sub-category/" class="<?=$page_title == 'SubCategory'?'active':''?>"><i class="lnr lnr-briefcase"></i> <span>Sub Category</a></li>					
									</ul>
								</div>
							</li>
							<!--category end-->
							<!--list Ingredient-->
							<li>
	                        	<?php
	                        		if($page_title =='ingredient'){
	                        			$setting = true;
	                        		}else{
	                        			$setting = false;
	                        		} 
	                        	?>
										<a href="<?= base_url() ?>ingredient/" class="<?=$page_title == 'List Ingredient'?'active':''?>"><i class="lnr lnr-tag"></i> <span>Ingredient</a>
									
							</li>
					<!--end Ingredient list-->
					<!--list ordes-->
							<li>
	                        	<?php
	                        		if($page_title =='orders'){
	                        			$setting = true;
	                        		}else{
	                        			$setting = false;
	                        		} 
	                        	?>
								
								<a href="<?= base_url() ?>orders/" class="<?=$page_title == 'List Orders'?'active':''?>"><i class="lni-cart"></i> <span>Orders</a>
									
							</li>
					<!--end order list-->
					<!--ofers start-->
							<li>
	                        	<?php
	                        		if($page_title =='add-coupons' || $page_title =='coupons'){
	                        			$setting = true;
	                        		}else{
	                        			$setting = false;
	                        		} 
	                        	?>
								<a href="#coupons" data-toggle="collapse" class="<?=$setting?'active':'collapsed'?>"><i class="lni-offer"></i> <span>Coupons</span> <i class="icon-submenu lnr lnr-chevron-right"></i></a>
								<div id="coupons" class="collapse <?=$setting?'in':''?>">
									<ul class="nav">
									<li><a href="<?= base_url() ?>add-coupons" class="<?=$page_title == 'Add Coupons'?'active':''?>"><i class="fa fa-plus"></i><span>Add Coupons</a></li>
										<li><a href="<?= base_url() ?>coupons" class="<?=$page_title == 'View Coupons'?'active':''?>"><i class="fa fa-eye"></i> <span>View Coupons</a></li>
								
									</ul>
								</div>
							</li>
						<!--ofers end-->
						<!--user start-->
							<li>
	                        	<?php
	                        		if($page_title =='users'){
	                        			$setting = true;
	                        		}else{
	                        			$setting = false;
	                        		} 
	                        	?>
										<li><a href="<?= base_url() ?>users/" class="<?=$page_title == 'Users'?'active':''?>"><i class="lnr lnr-users"></i><span>Users</a></li>
									
							</li>
						<!--user end-->
							<li>
	                        	<?php
	                        		if($page_title =='add-salad' || $page_title =='view-salads'){
	                        			$setting = true;
	                        		}else{
	                        			$setting = false;
	                        		} 
	                        	?>
								<a href="#saladMenu" data-toggle="collapse" class="<?=$setting?'active':'collapsed'?>"><i class="lni-service"></i> <span>Recipes</span> <i class="icon-submenu lnr lnr-chevron-right"></i></a>
								<div id="saladMenu" class="collapse <?=$setting?'in':''?>">
									<ul class="nav">
										<li><a href="<?= base_url() ?>add-salad/" class="<?=$page_title == 'Add Recipe'?'active':''?>"> <i class="fa fa-plus"></i> <span>Add Recipe</a></li>
										<li><a href="<?= base_url() ?>view-salads/" class="<?=$page_title == 'View Recipe'?'active':''?>"><i class="fa fa-eye"></i> <span>View Recipe</a></li>
										<!--<li><a href="<?= base_url() ?>custom-view-salads/" class="<?=$page_title == 'View Custom Recipe'?'active':''?>"><i class="fa fa-eye"></i> <span>View Custom Salad</a></li>-->
									</ul>
								</div>
							</li>


	                        <li>
	                        	<?php
	                        		if($page_title =='Terms & Conditions' || $page_title =='Privacy'){
	                        			$pages = true;
	                        		}else{
	                        			$pages = false;
	                        		} 
	                        	?>
								<a href="#subPages3" data-toggle="collapse" class="<?=$pages?'active':'collapsed'?>"><i class="lnr lnr-pencil"></i><span><?=$this->lang->line('Configuration')?$this->lang->line('Configuration'):'Configuration'?></span> <i class="icon-submenu lnr lnr-chevron-right"></i></a>
								<div id="subPages3" class="collapse <?=$pages?'in':''?>">
									<ul class="nav">
										<li><a href="<?= base_url() ?>pages/banner" class="<?=$page_title == 'Website Banner'?'active':''?>"><i class="fa fa-info"></i> <span>Website Banner</span></a></li>
										<!-- <li><a href="<?= base_url() ?>pages/about" class="<?=$page_title == 'About'?'active':''?>"><i class="fa fa-info"></i> <span>About</span></a></li> -->
										<li><a href="<?= base_url() ?>pages/terms_and_condition" class="<?=$page_title == 'Terms & Conditions'?'active':''?>"><i class="fa fa-list"></i><span> Terms & Conditions </span></a></li>
										<li><a href="<?= base_url() ?>pages/privacy" class="<?=$page_title == 'Privacy'?'active':''?>"><i class="fa fa-lock"></i> <span>Privacy</span></a></li>
										<li><a href="<?= base_url() ?>pages/settings" class="<?=$page_title == 'Settings'?'active':''?>"><i class="lnr lnr-cog"></i><span> Settings</span></a></li>
									</ul>
								</div>
							</li>
							<li>
							<a href="<?= base_url() ?>notification" class="<?=$page_title == 'Notifications'?'active':''?>"><i class="lnr lnr-alarm"></i><span><?=$this->lang->line('Notifications')?$this->lang->line('Notifications'):'Notifications'?> </span>
							</a>
							</li>
							
							
						<?php
						}
						?>
						
					</ul>
				</nav>
			</div>
                    <footer>
			<div class="container-fluid">
				
			</div>
                    </footer>
		</div>