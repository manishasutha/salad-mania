<!-- Javascript -->

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>    
	<script src="<?= base_url() ?>assets/Charts/jquery.barChart.js"></script>
	<script src="<?= base_url() ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?= base_url() ?>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?= base_url() ?>assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="<?= base_url() ?>assets/vendor/chartist/js/chartist.min.js"></script>
	<!-- Data table library -->
    <script src="<?= base_url() ?>assets/scripts/DataTables/DataTables-1.10.16/js/jquery.dataTables.js"></script>
    <script src="<?= base_url() ?>assets/scripts/DataTables/DataTables-1.10.16/js/dataTables.buttons.min.js"></script>
    <script src="<?= base_url() ?>assets/scripts/DataTables/DataTables-1.10.16/js/buttons.flash.min.js"></script>
    <script src="<?= base_url() ?>assets/scripts/DataTables/DataTables-1.10.16/js/jszip.min.js"></script>
    <script src="<?= base_url() ?>assets/scripts/DataTables/DataTables-1.10.16/js/pdfmake.min.js"></script>
    <script src="<?= base_url() ?>assets/scripts/DataTables/DataTables-1.10.16/js/vfs_fonts.js"></script>
    <script src="<?= base_url() ?>assets/scripts/DataTables/DataTables-1.10.16/js/buttons.html5.min.js"></script>
    <script src="<?= base_url() ?>assets/scripts/DataTables/DataTables-1.10.16/js/buttons.print.min.js"></script>
    <!-- Data table library end -->
	<script src="<?= base_url() ?>assets/scripts/klorofil-common.js"></script>
	<script src="<?= base_url() ?>assets/scripts/moment.js"></script>
	<script src="<?= base_url() ?>assets/scripts/select.min.js"></script>
	<script src="<?= base_url() ?>assets/scripts/datepicker.js"></script>
	<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>   
	<script src="<?= base_url() ?>assets/custom/formvalidation.js"></script>
	<script src="<?= base_url() ?>assets/bootbox/bootbox.js"></script>
	<script src="<?= base_url() ?>assets/custom/custom.js"></script>
	 <script src="<?= base_url() ?>assets/custom/datepicker.js"></script> 
	<script src="https://www.jquery-az.com/boots/js/bootstrap-multiselect/bootstrap-multiselect.js"></script>
	<!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAiJlRv6H0jkz6Q_MLgwvBMYP35K4Ma0OI"> -->
	<!-- <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script> -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAiJlRv6H0jkz6Q_MLgwvBMYP35K4Ma0OI&libraries=places&callback=initAutocomplete" async defer></script>
    </script>

	<script>
    $(document).ready(function(){
        var dt = $('.datatable').DataTable({
        	dom: 'Bfrtip',
	        buttons: [{
	            extend: 'excel', className: 'btn btn-success',
	            text: '<?=$this->lang->line('custom_export_button')?$this->lang->line('custom_export_button'):'Export data in excel'?>'
		    }],
		    "columnDefs": [
			    //{ "width": "20%", "targets": 1 }
			],
			"language": {
			    "search": "<?=$this->lang->line('search')?$this->lang->line('search'):'Search'?>",
			    "info": "<?=$this->lang->line('data_table_info')?$this->lang->line('data_table_info'):'Showing page _PAGE_ of _PAGES_'?>",
			    "paginate": {
						      "previous": "<?=$this->lang->line('previous')?$this->lang->line('previous'):'Previous'?>",
						      "next": "<?=$this->lang->line('next')?$this->lang->line('next'):'Next'?>"
						    }
			},
			//"dom": '<"wrapper"flipt>',
			//"dom": '<"top"Bf>rt<"bottom"lip><"clear">'

        });
	    dt.on( 'order.dt search.dt', function () {
	        dt.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	        } );
	    } ).draw();
        $(".show_password").focus(function(){
		    this.type = "text";
		}).blur(function(){
		    this.type = "password";
		});
		//$('div.dataTables_filter').appendTo("#filterdata");
    });
	
	</script>
</body>

</html>
