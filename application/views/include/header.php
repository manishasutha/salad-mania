<!doctype html>
<html lang="en">

<head>
	<title><?=$page_title?> | Salad Mania</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/chartist/css/chartist-custom.css">
    <!-- Datatbale cass start -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/scripts/DataTables/DataTables-1.10.16/css/jquery.dataTables.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/scripts/DataTables/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/alertify/css/alertify.min.css">
    <!-- Datatbale cass start -->
    <!--select multiple-->
    <link rel="stylesheet" href="https://www.jquery-az.com/boots/css/bootstrap-multiselect/bootstrap-multiselect.css">
	
    <link href="<?= base_url() ?>assets/datepicker/datepicker.css">
   
    
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/main.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/demo.css">
    <link href="<?= base_url() ?>assets/Charts/jquery.barCharts.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url() ?>assets/custom/custom.css">

    <script
            src="https://code.jquery.com/jquery-3.4.1.js"
            integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
            crossorigin="anonymous"></script>
<!--    <script src="--><?//= base_url() ?><!--assets/vendor/jquery/jquery.min.js"></script>-->
<!--    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>-->
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>-->
<!--	<!-- ICONS -->-->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <link rel="icon" type="image/png"  href="<?=base_url()?>assets/img/fevicon_bee_o_veg.ico">
    <!-- CHART -->
    <script src="<?= base_url() ?>assets/chart/chart.js"></script>

    <script src="<?= base_url() ?>assets/custom/datepicker.js"></script>
 <link rel="stylesheet" href="https://cdn.lineicons.com/1.0.1/LineIcons.min.css">
    <!-- <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url()?>assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?=base_url()?>assets/img/favicon.png"> -->
	<script type="text/javascript">
		var base_url = "<?= base_url()?>"
	</script>
   <script type="text/javascript">
    $(document).ready(function () {
        $('#pickyDate').datepicker({
            format: "dd/mm/yyyy"
        });
    });
</script>

<style type="text/css">
    .preview-images-zone {
        width: 100%;
        border: 1px solid #ddd;
        min-height: 180px;
        /* display: flex; */
        padding: 5px 5px 0px 5px;
        position: relative;
        overflow:auto;
    }
    /*.preview-images-zone > .preview-image:first-child {
        height: 185px;
        width: 185px;
        position: relative;
        margin-right: 5px;
    }*/
    .preview-images-zone > .preview-image {
        height: 90px;
        width: 90px;
        position: relative;
        margin-right: 5px;
        float: left;
        margin-bottom: 5px;
    }
    .preview-images-zone > .preview-image > .image-zone {
        width: 100%;
        height: 100%;
    }
    .preview-images-zone > .preview-image > .image-zone > img {
        width: 100%;
        height: 100%;
    }
    .preview-images-zone > .preview-image > .tools-edit-image {
        position: absolute;
        z-index: 100;
        color: #fff;
        bottom: 0;
        width: 100%;
        text-align: center;
        margin-bottom: 10px;
        display: none;
    }
    .preview-images-zone > .preview-image > .image-cancel {
        font-size: 18px;
        position: absolute;
        top: 0;
        right: 0;
        font-weight: bold;
        margin-right: 10px;
        cursor: pointer;
        display: none;
        z-index: 100;
    }
    .preview-image:hover > .image-zone {
        cursor: move;
        opacity: .5;
    }
    .preview-image:hover > .tools-edit-image,
    .preview-image:hover > .image-cancel {
        display: block;
    }
    .ui-sortable-helper {
        width: 90px !important;
        height: 90px !important;
    }

    .container {
        padding-top: 50px;
    }
    .menu-clone{background: #f7f8f9; padding: 15px; margin-bottom: 5px}
    .select2-container--default .select2-selection--single .select2-selection__rendered{line-height: 32px}
    .select2-container--default .select2-selection--single{    background: #f2f6fa;    border-radius: 2px; height: 34px;    border-color: #eaeaea;  box-shadow: none;}
    
    .foodtruck h4{font-weight: 600; font-size: 24px; color:#000}
    .stars{color: #FFCC36}
    .action-btns .btn{padding: 2px 5px;}
    
    .menu .menu-category{color:#4cb050; text-transform: uppercase; font-weight: 600}
    .menu .menu-item {border-bottom: 1px solid rgba(76,176,80,0.1)}
    .menu .menu-item h4{color:#333; font-weight: 600}
    .menu .menu-item p{font-style: italic}
    div.dt-buttons {
        float: right;
        margin-left:10px;
        /*width: 147px;*/
        margin-top: 12px;
    }
    .modelDivSpace{
        margin-top: 20px;
    }
   
</style>
<script>
 var url="<?= base_url()?>"
</script>

 <script>
        //alert hide after some time
        $(document).ready(function(){
            setInterval(function(){ $("div.alert").hide(); }, 5000)
        });
    </script>
</head>

<body   >




<!--<script>-->
<!--    $(window).scroll(function() {-->
<!--        alert("hello");-->
<!--    });-->
<!---->
<!--</script>-->