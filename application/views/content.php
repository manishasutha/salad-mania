<?php include"include/header.php"; ?>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include "include/navbar.php"; ?>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include"include/leftsidebar.php" ?>
		<!-- END LEFT SIDEBAR -->
		
		<!--Loading View-->
		<?php $this->load->view($page); ?>
		
	</div>
	<!-- END WRAPPER -->
<?php include"include/footer.php"; ?>