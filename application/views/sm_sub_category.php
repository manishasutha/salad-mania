<?php include "include/header.php"; ?>
<!-- WRAPPER -->
<div id="wrapper">
	<!-- NAVBAR -->
	<?php include "include/navbar.php"; ?>
	</nav>
	<!-- END NAVBAR -->
	<!-- LEFT SIDEBAR -->
	<?php include "include/leftsidebar.php" ?>
	<!-- END LEFT SIDEBAR -->
	<!-- MAIN -->
	<div class="main">
		<div class="subheader">
			<ul>
				<li> <?=$this->lang->line('admin')?$this->lang->line('admin'):'Admin';?> / <?=$this->lang->line('subcategory_list')?$this->lang->line('subcategory_list'):'SubCategory List';?> </li>
			</ul>
			<div class="right" style="float: right">
				<a id="addSubCategory" class="btn add btn-primary button-fix"><i class="fa fa-plus"></i> <?=$this->lang->line('add_subCategory')?$this->lang->line('add_subcategory'):'Add SubCategory';?></a>
			</div>
		</div>
		<!--  MAIN CONTENT -->
		<div class="main-content">
			<?php
			if($this->session->flashdata('success_msg')){
				$msg = $this->session->flashdata('success_msg');
				echo '<div class="alert alert-success fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Success!</strong>'. $msg.'
			        		</div>';
			}
			if($this->session->flashdata('error_msg')){
				$msg = $this->session->flashdata('error_msg');
				echo '<div class="alert alert-danger fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Error!</strong>'. $msg.'
			        		</div>';
			}
			?>
			<div class="container-fluid">

				<!-- END OVERVIEW -->
				<div class="row">
					<div class="col-md-12">
						<!-- RECENT PURCHASES -->
						<div class="panel">
							<div class="panel-heading2">
								<h3 class="panel-title2"><?=$this->lang->line('subcategory_list')?$this->lang->line('subcategory_list'):'SubCategory List';?></h3>
							</div>
							<div class="panel-body no-padding">
								<div class="table-responsive">
									<table class="table table-striped datatable">
										<thead>
										<tr>		
											<th>S.No</th>
											<th><?=$this->lang->line('subcat_name')?$this->lang->line('subcat_name'):'Name';?></th>
											<th><?=$this->lang->line('subcat_name_arabic')?$this->lang->line('subcat_name_arabic'):'Name Arabic';?></th>
											<th><?=$this->lang->line('category')?$this->lang->line('category'):'Category';?></th>
											<th><?=$this->lang->line('salad_type')?$this->lang->line('salad_type'):'Recipe Type';?></th>
											<th><?=$this->lang->line('status')?$this->lang->line('status'):'Status';?></th>
											<th><?=$this->lang->line('edit')?$this->lang->line('edit'):'Action';?></th>

										</tr>
										</thead>
										<tbody>
										<?php	$i=1;
										if($subCategories){
											$enable = $this->lang->line('enable')?$this->lang->line('enable'):'Enable';
											$disable = $this->lang->line('disable')?$this->lang->line('disable'):'Disable';
											foreach ($subCategories as $cat) {

												if($cat->subcat_status){
													$status_span = '<span class="label label-success">'.$enable.'</span>';
												}else{
													$status_span = '<span class="label label-danger">'.$disable.'</span>';
												}
												?>
												<tr>
													<td></td>
													<td><?php echo $cat->subcat_name ?></td>
													<td><?php echo $cat->subcat_name_arabic ?></td>
													<td><?php echo $cat->cat_name ?></td>
													<td><?php
															if($cat->salad_type == 1){
																echo "Pre";
															}
															else if($cat->salad_type == 2){
																echo "Create Own";
															}
														?>
													</td>
													<td><a><?php echo $status_span?></a></td>
													<td>
														<a  title="Edit SubCategory!" class="btn btn-xs btn-primary edit"  id="editSubCategory_<?php echo $i ?>" data-cat-id="<?php echo $cat->id?>"><i class="fa fa-pencil"></i>
														</a>
														<?php if($cat->subcat_status==1){
															?>
															<a  title="Disable SubCategory!" class="btn btn-xs btn-danger disableItem" href="<?=base_url()?>sub-category/disable/<?=$cat->id?>">
																<i class='fa fa-eye-slash'></i>
															</a>
														<?php }
														else{
															?>
															<a  title="Enable SUbCategory!" style="" class="btn btn-xs btn-info enableItem" href="<?=base_url()?>sub-category/enable/<?=$cat->id?>">
																<i  class="fa fa-eye"></i>
															</a>
														<?php } ?>
													</td>

												</tr>
												<?php
												$i++;
											}
										}
										?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="panel-footer">
								<div class="row">
									<div class=" text-left">

									</div>
								</div>
							</div>
						</div>
						<!-- END RECENT PURCHASES -->
					</div>
				</div>
			</div>
		</div>
		<!-- END MAIN CONTENT -->
		<?php include"include/footer_content.php" ?>
		<!-- END MAIN -->
		<div class="clearfix"></div>

	</div>
	</div>
	<!-- END WRAPPER -->

	<!--Add Subcategory model start added -->
	<div class="modal" id="addSubCategoryModel"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<form id="SubAddForm"  action="<?=base_url()?>sub-category/add" method="post">
				<div class="modal-content">
					<div class="modal-header text-left">
						<h4 class="modal-title w-100 font-weight-bold"><?=$this->lang->line('Add SubCategory')?$this->lang->line('Add SubCategory'):'Add SubCategory'?> </h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -27px;opacity: 0.6;">
							&times;
						</button>
					</div>
					<div class="modal-body mx-2">
						<div class="row">
							<div class="col-md-6 md-form mb-3 ">
								<label data-error="wrong" data-success="right" for="sub_cat_name"><?=$this->lang->line('sub_cat_name')?$this->lang->line('sub_cat_name'):'Name'?></label>
								<input type="text" id="sub_cat_name" name="sub_cat_name" class="form-control validate">
								<input type="hidden" name="sub_cat_id" id="subcat_id">
							</div>
								<div class="col-md-6 md-form mb-3 ">
								<label data-error="wrong" data-success="right" for="cat_edit_name"><?=$this->lang->line('sub_cat_name_arabic')?$this->lang->line('sub_cat_name_arabic'):'Name(Arabic)'?></label>
								<input type="text" id="sub_cat_name_arabic" name="sub_cat_name_arabic" class="form-control validate">
							</div>
						</div>
						<div class="clearfix"></div><br>
						
						<div class="md-form mb-3 ">
							<label data-error="wrong" data-success="right" for="max_limit"><?=$this->lang->line('max_limit')?$this->lang->line('max_limit'):'Max ingredients in this subcategory'?></label>
								<input type="number" id="maxlimit" name="max_limit"  min="1" class="form-control validate">
						</div>
						
						<div class="clearfix"></div><br>

						<div class="md-form mb-3 ">
							<label data-error="wrong" data-success="right" for="cat_edit_name"><?=$this->lang->line('salad_type')?$this->lang->line('salad_type'):'Recipe Type'?></label>
							<select type="text" id="salad_type" name="salad_type" class="form-control validate" style="color:black">
								<option value="">Select Recipe Type</option>
								<option value="1">Pre</option>
								<option value="2">Create Own </option>
							</select>
							<span id="type"></span>
						</div>
						<div class="md-form mb-3 ">
							<label data-error="wrong" data-success="right" for="parent_cat_id"><?=$this->lang->line('parent_cat_id')?$this->lang->line('parent_cat_id'):'Category'?></label>
							<?php $where=array('cat_status'=>1); $category = $this->General_model->get_multiple_row('sm_categories', '',$where)?>
							<select type="text" id="parent_cat_id" name="parent_cat_id" class="form-control validate" style="color:black">
								<option value="">Select Category</option>
								 <?php foreach( $category as $catname){?>
								<option value="<?=$catname->cat_id?>"><?=$catname->cat_name?></option>
								<?php } ?> 
							</select>
							<span id="type"></span>
						</div>
						
					</div>
					<div class="modal-footer d-flex button justify-content-center">
						<button class="btn-primary add-button" id="button" name="submit" type="submit">Submit</button>

					</div>
				</div>
			</form>
		</div>
	</div>
	<!-- Add Subcategory model end -->

	<!--Edit Subcategory model start added -->
	<div class="modal" id="editSubCategoryModel"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<form id="SubeditForm"  action="<?=base_url()?>sub-category/edit" method="post">
				<div class="modal-content">
					<div class="modal-header text-left">
						<h4 class="modal-title w-100 font-weight-bold"><?=$this->lang->line('Edit SubCategory')?$this->lang->line('Edit SubCategory'):'Edit SubCategory'?> </h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -27px;opacity: 0.6;">
							&times;
						</button>
					</div>
					<div class="modal-body mx-2">
						<div class="row mb-3 md-form">
							<div class="col-md-6 md-form mb-3 ">
								<label data-error="wrong" data-success="right" for="sub_cat_name"><?=$this->lang->line('sub_cat_name')?$this->lang->line('sub_cat_name'):'Name'?></label>
							 <span id="name_error"></span>
								<input type="text" id="subcat_name" name="sub_cat_name" class="form-control validate">
								<input type="hidden" name="sub_cat_id" id="sub_cat_id">
							</div>
							<div class="col-md-6 md-form mb-3 ">
								<label data-error="wrong" data-success="right" for="cat_edit_name"><?=$this->lang->line('sub_cat_name_arabic')?$this->lang->line('sub_cat_name_arabic'):'Name(Arabic)'?></label>
								<span id="name_italian_error"></span>
								<input type="text" id="subcat_name_arabic" name="sub_cat_name_arabic" class="form-control validate">
							</div>
						</div>
						<br>
						<div class="md-form mb-3 ">
							<label data-error="wrong" data-success="right" for="max_limit"><?=$this->lang->line('max_limit')?$this->lang->line('max_limit'):'Max ingredients in this subcategory'?></label>
							<span id="max_limit_error"></span>
								<input type="number" id="number" name="max_limit" min="1" class="form-control validate">
						</div>
						<br>
						<div class="md-form mb-3 ">
							<label data-error="wrong" data-success="right" for="cat_edit_name"><?=$this->lang->line('salad_type')?$this->lang->line('salad_type'):'Recipe Type'?></label>
						  <span id="name_italian_error"></span>
							<select type="text" id="saladtype" name="salad_type" class="form-control validate" style="color:black">
								<option value="">Select Recipe Type</option>
								<option value="1">Pre </option>
								<option value="2">Create Own </option>
							</select>
							<span id="type"></span>
						</div>
						<div class="md-form mb-3 ">
							<label data-error="wrong" data-success="right" for="parent_cat_id"><?=$this->lang->line('parent_cat_id')?$this->lang->line('parent_cat_id'):'Category'?></label>
							<span id="name_italian_error"></span>
							<?php $where=array('cat_status'=>1); $category = $this->General_model->get_multiple_row('sm_categories', '',$where);?>
							<select type="text" id="parentcat_id" name="parent_cat_id" class="form-control validate" style="color:black">
								<option value="">Select Category</option>
								<?php foreach( $category as $catname){?>
								<option value="<?=$catname->cat_id?>"><?=$catname->cat_name?></option>
								<?php } ?>
							</select>
							<span id="type"></span>
						</div>
					</div>
					<div class="modal-footer d-flex button justify-content-center">
						<button class="btn-primary add-button" id="button" name="update" type="submit">Update</button>

					</div>
				</div>
			</form>
		</div>
	</div>
	<!-- EDit Subcategory model end -->
	
	<?php include"include/footer.php"; ?>
	
	
	<script>
		$(document).ready(function(){

//

			$(".close").click(function(){
				$("#addSubCategoryModel").hide();
			});
			$(".close").click(function(){
				$("#editSubCategoryModel").hide();
			});
			
			//add validation js
			//Form Validation STart
			$('form[id="SubAddForm"]').validate({
				rules: {
					sub_cat_name: {
						required : true,
						minlength: 2,
						maxlength: 30,
						letterswithspace: true,
						remote: {
							url: "<?=base_url()?>add-check-category",
							type: "post",
							data:{
								id:function() {
									return $( "#subcat_id" ).val();
								},
								parent_cat_id:function() {
									return $( "#parent_cat_id" ).val();
								},
								salad_type:function() {
									return $( "#salad_type" ).val();
								},
							}
						}
					},
					sub_cat_name_arabic:
					{
						required:true,
					},
					max_limit:
					{
						required:true,
					},
					parent_cat_id: "required",
					salad_type: "required",
				},
				messages: {
					//salad_name: "Please enter salad name",
					parent_cat_id: "Please select Category",
					salad_type: "Please select Salad Type",
					discount_rate: {
						required: "Please enter salad price",
						number: "Amount should be in digits only"
					},
					sub_cat_name:{
						remote:"Category name already exist"
					}
				},
			});

			$.validator.addMethod("letterswithspace", function(value, element) {
				return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
			}, "Please enter only alphabets");
			$.validator.addMethod("lettersonly", function(value, element) {
				return this.optional(element) || /^[a-z]+$/i.test(value);
			}, "Please enter only alphabets");
			jQuery.validator.addMethod("alphanumeric", function(value, element) {
				return this.optional(element) || /^[\w.]+$/i.test(value);
			}, "Letters, numbers, and underscores only please");
	
		//add validation js end

		//edit validation js start
			//Form Validation STart
			$('form[id="SubeditForm"]').validate({
				rules: {
					sub_cat_name: {
						required : true,
						minlength: 2,
						maxlength: 30,
						letterswithspace: true,
						// remote: {
						// 	url: "<?=base_url()?>add-check-category",
						// 	type: "post",
						// 	data:{
						// 		id:function() {
						// 			return $( "#sub_cat_id" ).val();
						// 		},
						// 		parent_cat_id:function() {
						// 			return $( "#parent_cat_id" ).val();
						// 		},
						// 		salad_type:function() {
						// 			return $( "#salad_type" ).val();
						// 		},
						// 	}
						// }
					},
					sub_cat_name_arabic:
					{
						required:true,
					},
						max_limit:
					{
						required:true,
					},
					parent_cat_id: "required",
					salad_type: "required",
				},
				messages: {
					//salad_name: "Please enter salad name",
					parent_cat_id: "Please select Category",
					salad_type: "Please select Salad Type",
					discount_rate: {
						required: "Please enter salad price",
						number: "Amount should be in digits only"
					},
					sub_cat_name:{
						remote:"Category name already exist"
					}
				},
			});

			$.validator.addMethod("letterswithspace", function(value, element) {
				return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
			}, "Please enter only alphabets");
			$.validator.addMethod("lettersonly", function(value, element) {
				return this.optional(element) || /^[a-z]+$/i.test(value);
			}, "Please enter only alphabets");
			jQuery.validator.addMethod("alphanumeric", function(value, element) {
				return this.optional(element) || /^[\w.]+$/i.test(value);
			}, "Letters, numbers, and underscores only please");
			//edit validation js end
		});

  

</script>

