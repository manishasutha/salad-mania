<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title>Beeoveg</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- ICONS -->
	
</head>

<body style="background: #f1f3f4; font-family: 'Arial', 'Calibri'; padding: 20px; font-size: 14px">
	<div class="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
                            <div class="logo_outer" style="text-align: center; margin-bottom: 20px"><img src="assets/logo_outer.png" alt="."/></div>
				<div class="email_mn_bx effect6" style="width: 768px;text-align: center;margin: 0 auto;border-radius: 8px;background-color: #fff; box-shadow: 0 8px 12px -6px black;">
					<div class="header">
					<!--<div class="logo text-center"></div>-->
						<div class="main_1" style="padding: 34px 0px;">
							<p class="lead_1" style="margin-bottom: 20px;
			font-size: 16px;
			font-weight: 700;">Hello <?=$name?></p>
							<p class="cont_1">Your login details are:</p>
							<div class="content_mn" style="padding: 0px 60px;">
                            <p class="cont_1">
                              <span>User Name: <?=$user_name?></span><br>
                            </p>
                             <p class="cont_1">
                              <span>Use Password: <?=$user_password?></span><br>
                            </p>
							</div>
						</div>
					</div>
				</div>
				<div class="bottom" style="text-align:center; margin-top: 20px">
					<p class="cont_2">
                                Best Wishes <br/>
                                
                             </p>
					<p class="st_2" style="font-size: 13px">Bee-O-Veg Team</p>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

</html>
