<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title>Beeoveg</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- ICONS -->
	
</head>

<body style="background: #f1f3f4; font-family: 'Arial', 'Calibri'; padding: 20px; font-size: 14px">
	<div class="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
                            <div class="logo_outer" style="text-align: center; margin-bottom: 20px"><img src="<?=base_url()?>assets/img/logo_outer.png" alt="."/></div>
				<div class="email_mn_bx effect6" style="width: 768px;text-align: center;margin: 0 auto;border-radius: 8px;background-color: #fff; box-shadow: 0 8px 12px -6px black;">
					<div class="header">
					<!--<div class="logo text-center"></div>-->
						<div class="main_1" style="padding: 34px 0px;">
							<p class="lead_1" style="margin-bottom: 20px;
			font-size: 16px;
			font-weight: 700;">Hi !</p>
							<div class="content_mn"style="padding: 0px 60px;">
                                                            <p class="cont_1">You've received rating from <b><?=$user_name?></b></p>
                                                            <br/>
                                                            <span class="rating">
                                                            	<?php
                                                            		/*if($rating <=1.5){
                                                            			echo '<img src="<?=base_url()?>assets/img/star_full.png"/>';
                                                                
                                                            		}else if($rating <= 2.5){
                                                            			echo '<img src="<?=base_url()?>assets/img/star_full.png"/>
                                                            			<img src="<?=base_url()?>assets/img/star_full.png"/>
                                                            			';
                                                            		}else if($rating <= 3.5){
                                                            			echo '<img src="<?=base_url()?>assets/img/star_full.png"/>
                                                            			<img src="<?=base_url()?>assets/img/star_full.png"/>
                                                            			<img src="<?=base_url()?>assets/img/star_full.png"/>';
                                                            		}else if($rating <= 4.5){
                                                            			echo '<img src="<?=base_url()?>assets/img/star_full.png"/>
                                                            			<img src="<?=base_url()?>assets/img/star_full.png"/>
                                                            			<img src="<?=base_url()?>assets/img/star_full.png"/>
                                                            			<img src="<?=base_url()?>assets/img/star_full.png"/>'
                                                            			;
                                                            		}else if($rating <= 5){
                                                            			echo '<img src="<?=base_url()?>assets/img/star_full.png"/>
                                                            			<img src="<?=base_url()?>assets/img/star_full.png"/>
                                                            			<img src="<?=base_url()?>assets/img/star_full.png"/>
                                                            			<img src="<?=base_url()?>assets/img/star_full.png"/>
                                                            			<img src="<?=base_url()?>assets/img/star_full.png"/>'
                                                            			;
                                                            		}*/
                                                            	 ?>
                                                             Rating:
                                                            </span><span class='rating_point'><?=$rating?></span>
                                                               
                                                         
                                                         
                                                                <p class="cont_3" style="font-style: italic; opacity: 0.8; font-size: 13px; padding: 0 40px"><?=$comment?></p>
                                                            
                                                                <div class="org_info" style="font-weight: 600; margin-top: 20px ">
                                                                    <img src="<?=base_url()?>assets/img/ft_3_optimized.jpg" alt='.' height="42" style="border-radius: 3px; "/><br/> <?=$company_name?>
                                                                </div>
							</div>
						</div>
					</div>
				</div>
				<div class="bottom" style="text-align:center; margin-top: 20px">
					
					<p class="st_2" style="font-size: 13px">Bee-O-Veg Team</p>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

</html>
