<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
    <title>Salad Mania</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- ICONS -->

</head>

<body style="background: #f1f3f4; font-family: 'Arial', 'Calibri'; padding: 20px; font-size: 15px">
<div class="wrapper">
    <div class="vertical-align-wrap">
        <div class="vertical-align-middle">
            <div class="logo_outer" style="text-align: center; margin-bottom: 20px"><img src="<?=base_url()?>assets/frontend/img/logo.png" height="100px" alt="."/></div>
            <div class="email_mn_bx effect6" style="width: 50%;text-align: left;margin: 0 auto;border-radius: 8px;background-color: #fff; box-shadow: 0px 0.175em 0.5em rgba(2, 8, 20, 0.3)">
                <div class="header">
                    <div class="main_1" style="padding: 34px 0px;">
                        <div class="content_mn" style="padding: 0px 20px; ">
                            <p class="lead_1" style="margin-bottom: 10px;font-size: 16px;font-weight: 700;">Hello <?=getSingleFieldDetail('id',$order->user_id,'full_name','sm_users')?><span></span></p>
                            <p class="cont_1" >
                                <!--<span style="color: #86c819; font-weight: 600"></span>-->
                                    <?php
                                    
                                    $orderNo = 'SM-'.strtoupper(encryptId($order->order_id));
                                    
                                    if($order->o_status == 1){
                                        if($order->in_store_pickup == 0){
                                            echo "Proudly, we received your order. Your order ID is: $orderNo.";
                                        }else if ($order->in_store_pickup == 1 ){
                                             echo "Proudly, we received your order, your order will be ready in 10 min. Your order ID is: $orderNo.";
                                        }
                                    }
                                    else if($order->o_status == 2){
                                        if($order->in_store_pickup == 0){
                                            //Message based on Delivery status
                                            if($order->delivery_status == 1)
                                                echo "Proudly, we have accepted your order. Your order ID is: $orderNo.";
                                            else if($order->delivery_status == 2)
                                                echo "Proudly, we are preparing your food, and pickup is pending.";
                                            else if($order->delivery_status == 3)
                                                echo "Proudly, we have prepared your food and the driver has picked your order.";
                                            else if($order->delivery_status == 4)
                                                echo "Proudly, Your order has been delivered successfully.";
                                            else if($order->delivery_status == 5)
                                                echo "Your order has been Canceled. We apologise for the inconvenience.";
                                        }
                                        else if ($order->in_store_pickup == 1 ){
                                             echo "Proudly, we have accepted your order, your order will be ready in 10 min. Your order ID is: $orderNo.";
                                        }
                                    }
                                    else if($order->o_status == 6){
                                        echo "Your order has been Canceled. We apologise for the inconvenience.";
                                    }?>
                            </p>

                            <p class="cont_1"></p>

                            <p class="lead_1" style="margin-bottom: 10px; text-transform: uppercase; font-size: 16px;font-weight: 600;">Order Details: <span></span></p>
                            <p class="cont_1">
                                <b>Order ID:</b> SM-<?=strtoupper(encryptId($order->order_id))?><br>
                                <b>Total Amount:</b> <?=$order->grand_total?> JD <br>
                                <b>Payment Type:</b>  <?=$order->payment_method?><br>

                                <?php if($order->in_store_pickup == 0){ ?>
                                <b>Delivery Type:</b> Delivery</p>
                                
                                <p class="cont_1"><b>Delivery Address:</b><br> <?php echo getSingleFieldDetail('add_id',$order->address_id,'addline1','sm_addresses') ?> </p>

                                <?php if($order->delivery_status == 3){ ?>
                                <p style="margin-bottom: 10px;font-size: 16px; text-transform: uppercase; font-weight: 600;">Delivery Person:<span></span></p>
                                <p class="cont_1">
                                    <b>Name:</b> <?php echo $order->driver_name ?><br>
                                    <b>Phone No:</b> <?php echo $order->driver_phone ?><br>
                                </p>
                                
                                <?php } } else {
                               ?>
                                <b>Delivery Type:</b> Pickup</p>
                                <p class="cont_1"><b> Pickup Person:</b> <?=$order->customer_name?><br>
                                <b>Pickup Time</b>: <?php echo date("d-m-Y h:i:A", strtotime($order->pickup_date.' '.$order->pickup_time)) ?></p>
                            <?php
                            }?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom" style="text-align:center; margin-top: 20px">
                <p class="st_2" style="font-size: 13px">Regards</p>
                <p class="st_2" style="font-size: 13px">Salad Mania Team</p>
            </div>
        </div>
    </div>
</div>
<!-- END WRAPPER -->
</body>

</html>
