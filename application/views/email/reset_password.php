<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title>Salad Mania</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- ICONS -->

</head>

<body style="background: #f1f3f4; font-family: 'Arial', 'Calibri'; padding: 20px; font-size: 15px">
<div class="wrapper">
	<div class="vertical-align-wrap">
		<div class="vertical-align-middle">
			<div class="logo_outer" style="text-align: center; margin-bottom: 20px"><img src="<?=base_url()?>assets/frontend/img/logo.png" height="100px" alt="."/></div>
			<div class="email_mn_bx effect6" style="width: 50%;text-align: left;margin: 0 auto;border-radius: 8px;background-color: #fff; box-shadow: 0px 0.175em 0.5em rgba(2, 8, 20, 0.3)">
				<div class="header">
					<div class="main_1" style="padding: 34px 0px;">
						<div class="content_mn" style="padding: 0px 60px;">
						<p class="lead_1" style="margin-bottom: 20px;font-size: 16px;font-weight: 700;">Hello Admin</p>

							<p class="cont_1">Your login credentials are given below  <span></span></p>
							<p class="lead_1" style="margin-bottom: 10px;font-size: 16px;font-weight: 700;">Details: <span></span></p>
							<p class="cont_1">
								<b>E-mail:</b> <?=$email?> <br>
								<b>Subject:</b>  <?=$password?><br>
							</p>
							<p><a href="<?= base_url()?>auth/" class="btn btn-info"><button type="submit" class="btn btn-lg submit_bt" style="color: #fff;box-shadow: 0px 0.175em 0.5em rgba(2, 8, 20, 0.3); background-color: #86c819; border-color: #86c819; border-radius: 4px;padding: 10px 60px;  margin-top: 20px; margin-bottom: 20px">Login</button></a></p>
						</div>
					</div>
				</div>
			</div>
			<div class="bottom" style="text-align:center; margin-top: 20px">
				<p class="st_2" style="font-size: 13px">Regards</p>
				<p class="st_2" style="font-size: 13px">Salad Mania Team</p>
			</div>
		</div>
	</div>
</div>
<!-- END WRAPPER -->
</body>

</html>



