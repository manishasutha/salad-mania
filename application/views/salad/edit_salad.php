<!-- MAIN -->
<style>
    ul.multiselect-container{
        width:100%;
    }
</style>

<div class="main">
    <div class="subheader">
        <ul>
            <li> <?=$this->lang->line('admin')?$this->lang->line('admin'):'Admin';?> / Edit Recipe</li>
        </ul>
    </div>
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <?php
        if($this->session->flashdata('success_msg')){
            $msg = $this->session->flashdata('success_msg');
            echo '<div class="alert alert-success fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Success!</strong>'. $msg.'
                    </div>';
        }
        ?>
        <?php
        if($this->session->flashdata('error_msg') || !empty($error)){
            if(!empty($error)){
                $msg = $error;

            }else{
                $msg = $this->session->flashdata('error_msg');
            }
            echo '<div class="alert alert-danger fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Error!</strong>'. $msg.'
                    </div>';
        }
        ?>
        <div class="container-fluid">
       
            <!-- END OVERVIEW -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                    
                        <div class="panel-heading2">
                            <h3 class="panel-title2">Edit Recipe</h3>
                        </div>
                        <div class="panel-body" style="margin-top: 50px;">
                            <?php
                                if($saladInfo->num_rows() > 0)
                                {
                                    $salad = $saladInfo->row();
                            ?>
                            <form enctype="multipart/form-data" id="saladAddForm" method="post">
                                <input type="hidden" name="id" id="id" value="<?= $salad->salad_id ?>">
                                <!--Salad Basic Info Box Start-->
                                <div class=salad_main>
                                    <div class="row" style="margin-bottom: 20px">
                                        <div class="col-lg-6">
                                            <div class="md-form mb-3 ">
                                                <label for="salad_name">Name</label>
                                                <input type="text" name="salad_name" id="salad_name" class="salad_name form-control validate" value="<?php echo $salad->salad_name ?>" onchange="checkSaladExist()">
                                                <label class="error" id="salad_name_error"></label>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="md-form mb-3">
                                                <label for="salad_name_arabic">Name(Arabic)</label>
                                                <input type="text"  name="salad_name_arabic" id="salad_name_arabic" class="salad_name_arabic form-control validate" value="<?php echo $salad->salad_name_arabic ?>">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div><br>

                                        <div class="col-lg-6">
                                            <div class="md-form mb-3">
                                                <label for="salad_name_arabic">Category</label>
                                                <select style="color:black" type="text" id="cat_id" name="cat_id" class="form-control validate" readonly>
                                                    <option style="color:black" value="">Select Category</option>
                                                    <?php $category = $this->General_model->get_multiple_row('sm_categories', '');

                                                        if(!empty($category))
                                                        {
                                                            foreach( $category as $catname)
                                                            {
                                                                $selected = '';
                                                                if(isset($_GET['cat_id']) && $_GET['cat_id'] == $catname->cat_id)
                                                                    $selected = 'selected="selected"';
                                                            ?>
                                                            <option style="color:black" <?php echo $selected ?> value="<?=$catname->cat_id?>"><?=$catname->cat_name?></option>
                                                        <?php }} ?>
                                                </select>
                                            </div>
                                        </div>
                                    
                                        <div class="col-lg-6">
                                            <div class="md-form mb-3 ">
                                                <label for="salad_img">Image</label>
                                                <?php if($salad->salad_img != NULL){ ?>
                                                    <img src="<?php echo INCLUDE_SALAD_IMAGE_PATH.$salad->salad_img ?>" style="width: 40px;height: 40px;position: absolute;right: 15px; bottom: 35px;" >
                                                <?php } ?>
                                                <input type="hidden" name="old_img" value="<?php echo $salad->salad_img ?>" >
                                                <input type="file" name="salad_img" id="salad_img" class="form-control validate">
                                                
                                            </div>
                                        </div>
                                        <div class="clearfix"></div><br>

                                         <div class="col-lg-6">
                                            <div class="md-form mb-3 ">
                                                <label for="salad_desc">Description</label>
                                                <textarea  rows="8" cols="20" name="salad_desc" id="salad_desc" class="salad_desc form-control validate"> <?= $salad->salad_desc ?> </textarea>
                                                <label class="error" id="salad_desc_error"></label>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="md-form mb-3">
                                                <label for="salad_desc_arabic">Description(Arabic)</label>
                                                <textarea rows="8" cols="20"  name="salad_desc_arabic" id="salad_desc_arabic" class="salad_desc_arabic form-control validate"> <?= $salad->salad_desc_arabic ?> </textarea>
                                            </div>
                                        </div>
                                    </div>
                                <div>
                                
                                <!--Salad Ingredients Info Box Start-->
                                <?php
                                if(isset($subcats) && $subcats->num_rows() > 0)
                                {
                                    $j = 1;
                                ?>
                                
                                <label>Ingredients</label>  
                                <div class="main-nav" style="border: 1px solid #eee ; height: 500px;">
                                    <ul class="nav nav-pills" style="height: 500px; background-color: #eee; padding: 15px 6px; overflow-y: auto;">
                                        
                                        <?php 
                                            foreach($subcats->result() as $srow) 
                                            { 
                                                //Count total Ingredients used in this category
                                                $sql="select sum(quantity) as usedQty from sm_salad_ingredients where salad_id = $id AND subcat_id = $srow->id group by salad_id";
                                                $query =$this->Main_model->__callMasterquery($sql);
                                                $usedQty = 0;
                                                if($query->num_rows() > 0){
                                                    $scrow = $query->row();
                                                    $usedQty = $scrow->usedQty;
                                                }
                                        ?>
                                            <li class="border-top <?php if($j == 1) echo 'active' ?>">
                                                <a data-toggle="pill" href="#tab_<?= $srow->id ?>"> <?= $srow->subcat_name ?>
                                                    <span class="pull-right">
                                                        (<span id="count_<?php echo $srow->id ?>"><?php echo $usedQty ?></span> /
                                                    <span id="max_limit_<?php echo $srow->id ?>"><?php echo $srow->max_limit  ?></span>)
                                                    </span>
                                                </a> 
                                            </li>
                                        <?php $j++; } ?>
                                    </ul>

                                    <div class="tab-content">
                                        <?php 
                                            $i = 1;
                                            $salad_price = 0; $salad_calories = 0;
                                            foreach($subcats->result() as $srow) 
                                            { 
                                                //Get the Ingredeints of the subcategory
                                                $sql = "select iss.ing_id, iss.qty_up_down, i.ing_price, i.ing_calories, i.ing_name from sm_ingredients_subcats iss inner join sm_ingredients i on i.ing_id = iss.ing_id where iss.subcat_id = $srow->id AND i.ing_status = 1";
                                                $ings = $this->Main_model->__callMasterquery($sql);
                                        ?>
                                        <div id="tab_<?php echo $srow->id ?>" class="tab-pane fade <?php if($i == 1) echo 'in active'; ?>">
                                            <h3><?php echo $srow->subcat_name ?></h3>
                                            <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th colspan="2">Ingredient</th>
                                                    <th>Select</th>
                                                    <th>Qty</th>
                                                    <th>Unit Price</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                    if($ings->num_rows() > 0)
                                                    {
                                                        foreach ($ings->result() as $irow) 
                                                        {
                                                            $readonly = '';
                                                            if($irow->qty_up_down == 0)
                                                                $readonly = 'readonly="readonly"';

                                                            //Get the used Qty of ingredient in salad
                                                            $sql="select quantity from sm_salad_ingredients where salad_id = $id AND subcat_id = $srow->id AND ing_id = $irow->ing_id";
                                                            $query =$this->Main_model->__callMasterquery($sql);
                                                            $totalQty = '';
                                                            if($query->num_rows() > 0){
                                                                $sirow = $query->row();
                                                                $totalQty = $sirow->quantity;
                                                            }
                                                            $checked = ''; 
                                                            if($totalQty != ''){
                                                                $checked = 'checked="checked"';
                                                                $salad_price = $salad->salad_price ;
                                                                $salad_calories = $salad->salad_calories;
                                                            }
                                                             $salad_price = $salad->salad_price ;
                                                            $salad_calories =  $salad->salad_calories;
                                                ?>
                                                <tr class="ing-select" id="ing_row_<?php echo $irow->ing_id ?>" >
                                                    <input type="hidden" id="catTotalItems" value="0">
                                                    <td colspan="2"><p><?php echo $irow->ing_name ?></p></td>
                                                    <td>
                                                        <label class="ing-check-width">
                                                            <input type="checkbox" <?php echo $checked ?> class="ingIds" name="ing_id[]" data-id="<?php echo $srow->id ?>" value="<?php echo $irow->ing_id.'-'.$srow->id ?>" >
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control ingQty" name="quantity[]" data-id="<?php echo $srow->id ?>" min="0" max="<?php echo $srow->max_limit ?>" <?php echo $readonly ?> value="<?php echo $totalQty ?>" style="width: 80px;">
                                                    </td>
                                                    <td>
                                                        <span class="ing_price"><?php echo $irow->ing_price ?></span>
                                                        JD
                                                        <span style="display:none ;" class="ing_calories"><?php echo $irow->ing_calories ?></span>
                                                    </td>
                                                </tr>
                                                <?php }} else { ?>
                                                <tr class="ing-select">
                                                    <td colspan="4">No Ingredients Found!!</td>
                                                </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                            </div>
                                        </div>
                                        <?php $i++; } ?>
                                    </div>
                                </div>

                                <div class="clearfix"></div><br>

                                <div class="row">
                                    <div class="col-lg-3" id="saladPriceBox">
                                        <label>Recipe Price</label> <div class="clearfix"></div>
                                        <input type="text"  class="form-control" name="salad_price" value="<?php echo $salad_price ?>" id="salad_price">
                                    </div>

                                    <div class="col-lg-3" id="saladPriceBox" >
                                        <label>Calories</label> <div class="clearfix"></div>
                                         <input  class="form-control" name="salad_calories" type="text" value="<?php echo $salad_calories ?>" id="salad_calories">
                                    </div>
                                </div>

                                <?php } else{ ?>
                                <h4>No Ingredients Found!!</h4>
                                <?php } ?>

                                <div class="row" style="margin-top: 20px">
                                    <div class="col-md-6 text-left">
                                        <label id="ing_ids_error" class="error"></label>
                                    </div>

                                    <div class="col-md-6 text-right">
                                        <button type="submit" id="submit" name="submit" class="btn btn-primary">Update</button>
                                        <button type="button" onclick="history.go(-1)" class="btn btn-secondary">Cancel</button>
                                    </div>
                                </div>
                                <!--Salad Ingredients Info Box End-->
                            </form>
                            <?php } else{ ?>
                            <h4>No Record Found!!</h4>
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
    <!-- END MAIN -->
    <div class="clearfix"></div>

    <script>
    
        /* file uploading code */
        var textbox_id =1;
        $(document).ready(function() {
            //Form Validation STart - first step
            $('form[id="saladAddForm"]').validate({
                rules: {
                    salad_name: {
                        required : true,
                        letterswithspace: true,
                        minlength: 2,
                        maxlength: 50,
                        /*remote: {
                            url: "<?=base_url()?>check-salad-unique",
                            type: "post",
                            data:{
                                cat_id:function() {
                                    return $( "#cat_id" ).val();
                                },
                                salad_id:function() {
                                    return $("#id").val();
                                },
                            }
                        }*/
                    },
                    salad_name_arabic:{
                        required:true,
                        minlength: 2,
                        maxlength: 50,
                    },
                    cat_id: "required",
                    salad_desc:{
                        required:true,
                        minlength: 2,
                        maxlength:200,
                    },
                     salad_desc_arabic:{
                        required:true,
                    },
                   salad_price: {
						required: true,
						number: true,
						min:0,
						max:100000,
						maxlength: 6,
					},
                    salad_calories: {
						required: true,
						number: true,
						min:0,
						max:100000,
						maxlength: 6,

                    },
                },
                 messages: {
                    salad_name_arabic: {
                        minlength: "Recipe name must be greater than 2 characters",
                        maxlength: "Recipe name must be less than 50 characters",
                    },
                    salad_name: {
                        minlength: "Recipe name must be greater than 2 characters",
                        maxlength: "Recipe name must be less than 50 characters",
                        remote: "Recipe name is already exist"
                    },
                     salad_price: {
						required: "Please enter Amount",
						number: "Amount should be numeric only",
						max:"Amount must not be greater than 1000",
						min:"Amount must not be less than 1"
					},
					salad_calories: {
						required: "Please enter calories",
						number: "calories should be numeric only",
						max:"calories must not be greater than 1000",
						min:"calories must not be less than 1"
					},
                },
            });
        //Form Validation STart - first two
            $('form[id="saladAddForm"]').submit(function(){
                var salad_price = $("#salad_price").val();
                //alert(salad_price);
                //return false;
                if(salad_price > 0){
                    $('#ing_ids_error').hide().text('');
                    return true;
                }
                else{
                    $('#ing_ids_error').show().text('Please select Ingredients');
                    return false;
                }
            });

            $.validator.addMethod("letterswithspace", function(value, element) {
                return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
            }, "Please enter only alphabets");
            $.validator.addMethod("lettersonly", function(value, element) {
                return this.optional(element) || /^[a-z]+$/i.test(value);
            }, "Please enter only alphabets");
        

            //Realod page with category ID in GET request
            $('#cat_id').change(function(){
                var id = $(this).val();
                var url = window.location.href.split('?')[0];
                url = url+'?cat_id='+id;
                window.location = url;
            });

            //====Manage Limit of the Category on Selection of the Ingredient===
            $('.ingIds').change(function(){
                var subcat_id = $(this).attr('data-id');
                var ing_row_id = $(this).parent().parent().parent().attr('id');
                var count = parseInt($("#count_"+subcat_id).text());
                var max_limit = parseInt($("#max_limit_"+subcat_id).text());
                
                if($(this).prop("checked") == true){
                    $("#tab_"+subcat_id+" #"+ing_row_id+ " td input.ingQty").val('1'); //Qty to 1
                    count++; //Increase the count
                }
                else if($(this).prop("checked") == false){
                    var qty = parseInt($("#tab_"+subcat_id+" #"+ing_row_id+ " td input.ingQty").val());
                    count = count-qty;
                    $("#tab_"+subcat_id+" #"+ing_row_id+ " td input.ingQty").val('');
                }
                
                $("#count_"+subcat_id).text(count);
                //Disable-Enable the rest checkboxes if count reached to the limit
                if(count == max_limit){
                    $("#tab_"+subcat_id+" .ingIds:not(:checked)").attr('disabled', 'disabled');
                    $("#tab_"+subcat_id+" .ingQty").attr('readonly', 'readonly');
                    alert("You can't select more Ingredients of this category");
                }
                else{
                    $("#tab_"+subcat_id+" .ingIds:not(:checked)").removeAttr('disabled');
                    $("#tab_"+subcat_id+" .ingQty").removeAttr('readonly');
                }

                calculateSaladPrice();
            });

            //====Manage Limit of the Category on changing the quantity===
            $('.ingQty').change(function(){
                var currentQty = $(this).val();
                var subcat_id = $(this).attr('data-id');
                var ing_row_id = $(this).parent().parent().attr('id');

                //var count = parseInt($("#count_"+subcat_id).text());
                var max_limit = parseInt($("#max_limit_"+subcat_id).text());
                var totalItem = parseInt($("#"+ing_row_id+ " #catTotalItems").val());

                $("#tab_"+subcat_id+ " td input.ingQty").each(function() {                
                    var qty = parseInt($(this).val());
                    if(qty > 0)
                        totalItem = totalItem + qty;
                });

                var count = totalItem;
                $("#count_"+subcat_id).text(count);

                //check-uncheck the checkbox of the row
                if(currentQty > 0)
                    $("#tab_"+subcat_id+" #"+ing_row_id+ " input.ingIds").prop('checked', true);
                else
                    $("#tab_"+subcat_id+" #"+ing_row_id+ " input.ingIds").prop('checked', false);

                //Disable-Enable the rest checkboxes if count reached to the limit
                if(count == max_limit){
                    $("#tab_"+subcat_id+" .ingIds:not(:checked)").attr('disabled', 'disabled');
                    $("#tab_"+subcat_id+" .ingQty").attr('readonly', 'readonly');
                    alert("You can't select more Ingredients of this category");
                }
                else{
                    $("#tab_"+subcat_id+" .ingIds:not(:checked)").removeAttr('disabled');
                    $("#tab_"+subcat_id+" .ingQty").removeAttr('readonly');
                }

                calculateSaladPrice();
            });
        });

        //This function is used to calcualte salad price & calories
        function calculateSaladPrice(){
            var salad_price = 0;
            var salad_calories = 0;
            $("input.ingIds").each(function() {                
                var subcat_id = $(this).attr('data-id');
                var ing_row_id = $(this).parent().parent().parent().attr('id');
                var qty = parseInt($("#tab_"+subcat_id+" #"+ing_row_id+ " td input.ingQty").val());
                var ing_price = parseFloat($("#tab_"+subcat_id+" #"+ing_row_id+ " td span.ing_price").text());
                var ing_calories = parseFloat($("#tab_"+subcat_id+" #"+ing_row_id+ " td span.ing_calories").text());
                if($(this).prop("checked") == true){
                    salad_price = salad_price + (ing_price*qty); 
                    salad_calories = salad_calories + (ing_calories*qty); 
                }
            });
            
            $("input#salad_price").val(salad_price.toFixed(2));
            $("input#salad_calories").val(salad_calories.toFixed(2));
        }
    
        //This Function check salad name is unique OR not
        function checkSaladExist(){
            var cat_id = $("#cat_id").val();
            var id = $("#id").val();
            var salad_name = $("#salad_name").val();
            //$result = true;
            $.post("<?=base_url()?>check-salad-unique",  { salad_name : salad_name, cat_id:cat_id, id:id }, function(data){
                if(data == 'true')
                {
                    //alert("no duplicate");
                    $('#salad_name_error').hide().text(''); 
                    $("#submit").removeAttr("disabled");
                }
                else
                {
                    //alert("duplicate");
                    $('#salad_name_error').show().text('Recipe name is already in use.'); 
                    $("#submit").attr("disabled","disabled");
                    //return false;
                }
                console.clear();
            });
        }
    </script>
    <!-- Javascript -->
