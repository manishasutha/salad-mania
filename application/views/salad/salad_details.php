
		<div class="main">
            <div class="subheader">
                <ul>
                    <li> <?=$this->lang->line('admin')?$this->lang->line('admin'):'Admin';?> / Recipe Detail</li>
                </ul>
            </div>
			<!--  MAIN CONTENT -->
			<div class="main-content">
				 <?php
		            if($this->session->flashdata('success_msg')){
		            $msg = $this->session->flashdata('success_msg');
		                echo '<div class="alert alert-success fade in">
		                    <a href="#" class="close" data-dismiss="alert">&times;</a>
		                    <strong>Success!</strong>'. $msg.'
		                    </div>';
		            }
		        ?>
				<div class="container-fluid">					
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="row panel-heading">
                                    <div class="col-lg-6">
								        <h3 class="panel-title">Recipe Detail</h3>
                                    </div>
									<div  class="col-lg-6 ">
										<?php
												if(!empty($query) && $query->num_rows() > 0){
												
													foreach ($query->result() as $salad) {?>
                                        <a  title="Edit Salad" href="<?= base_url() ?>view-salads/edit/<?=$salad->salad_id;?>" id="detailsalad" class="right btn add btn-primary"><i class="fa fa-pencil"></i> <span>Edit </span></a>
                                    </div>
                                </div>
								<div class="row panel-body no-padding"  style="margin-top:20px;">
                                    <div class="col-md-3">
                                        <img class="post-thumb img-responsive" width="100%" height="220px"  src="<?php echo base_url(); ?>uploads/salads/<?php echo $salad->salad_img; ?>">
                                    </div >
                                    <div class="col-md-9">
								        <div class="table-responsive">
									        <table class="table table-striped datatable">
										
										        <tbody>
                                                <tr>
														    <th>Name</th>
															<td><?php echo $salad->salad_name;?></td></tr>
															  <tr><th>Price</th>
															<td><?php echo $salad->salad_price;?></td></tr>
															  <tr><th>Arabic Name</th>
															<td><?php echo $salad->salad_name_arabic;?></td></tr>
															 <tr><th>Description</th>
															<td><?php echo $salad->salad_desc;?></td></tr>
                                                </tbody>
                                            </table>
								        </div>
								    </div>
								</div>
								<?php if($salad->ing_ids!='NULL'){?>
								<h3 style="margin-top:25px; margin-left: 20px;"> Ingredient Details</h3>
								<div style="margin-top:20px;" class="row">
									<?php
												$i=1;
														$salad= explode(',',$salad->ing_ids);
														if(!empty($salad)){
													  foreach($salad as $id){
																$ingsql="select  ing_id,ing_name,ing_name_arabic,ing_calories,ing_price,ing_image from sm_ingredients  where ing_id=$id";
																 $query = $this->Main_model->__callMasterquery($ingsql);
																 	foreach ($query->result() as $ing) {
													
														?>
													<div style="margin-top:20px;" class="col-md-4 table-responsive">
													<table class="table table-striped datatable" style="border: 1px solid #f2f2f2;">
													<tbody>
													<tr><img class="post-thumb img-responsive" width="100%" style="border: 1px solid #f2f2f2;" src="<?php echo base_url(); ?>uploads/ingredients/<?php echo $ing->ing_image; ?>"></tr>
													<tr style="margin-top:40px;"><th>Name</th><td><?php echo $ing->ing_name;?></td></tr>
													<tr><th>Arabic Name</th><td><?php echo $ing->ing_name_arabic;?></td></tr>
													<tr><th>Calories</th><td><?php echo $ing->ing_calories;?></td></tr>
													<tr><th>Price</th><td><?php echo $ing->ing_price?></td></tr>
													</tbody>
													</table>
													</div>
														
													
												<?php	
												if($i%3 == 0){?>
												<div class="clearfix"></div>
												<?php } $i++ ;}
														}
													}
											
											 	}
												}
												?>						
										</tbody>
									</table>
									
								<div class="panel-footer">
								<?php } ?>
											
								</div>
									
							</div>
								<div class="row">
											
											<div  style="margin-left:20px;margin-bottom:20px;">
												<button type="submit" id="detailsalad" name="detailsalad" class="btn add btn-info"><i class="fa fa-backward"></i> <span> GO BACK </span></button>
											</div>    
										
									</div>
							<!-- END RECENT PURCHASES -->
						</div>
										
					</div>
					
				</div>
			</div>
			<!-- END MAIN CONTENT -->
          
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		
	</div>
	<script>
	$("button[name='detailsalad']").click(function(){
		parent.history.back();
	});
	</script>
