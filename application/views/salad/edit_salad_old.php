<style>
ul.multiselect-container{
    width:100%;
}
</style><!-- MAIN -->
<div class="main">
	<!-- MAIN CONTENT -->
    <div class="subheader">
        <ul>
            <li> <?=$this->lang->line('admin')?$this->lang->line('admin'):'Admin';?> / Edit Recipe</li>
        </ul>
    </div>
	<div class="main-content">
        <?php
            if($this->session->flashdata('success_msg')){
            $msg = $this->session->flashdata('success_msg');
                echo '<div class="alert alert-success fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Success!</strong>'. $msg.'
                    </div>';
            }
        ?>
        <?php
            if($this->session->flashdata('error_msg') || !empty($error)){
                if(!empty($error)){
                    $msg = $error;

                }else{
                    $msg = $this->session->flashdata('error_msg');
                }
                echo '<div class="alert alert-danger fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Error!</strong>'. $msg.'
                    </div>';
            }
        ?>
        <!-- '; -->
		<div class="container-fluid">
			
			<!-- END OVERVIEW -->
			<div class="row">
				<div class="col-md-12">

					<!-- RECENT PURCHASES -->
                    <form class="col-md-12 form-panel" id="saladAddForm" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="panel new-panel">
                                <div class="panel-heading">
                                    <div class="col-md-12">
                                        <h3 class="panel-title">Edit Salad</h3>
                                        <!-- <div class="right">
                                        <a href="<?= base_url() ?>view-salads" class="btn btn-primary"><i class="fa fa-list"></i> View Salads</a>
                                        </div> -->
                                    </div>
                                    <br>
                                </div>
                            <?php foreach($salads->result() as $salad){?>
                                <div class="notice-area"></div>
 <input class="form-control" type="hidden" name="id" id="id" value="<?php echo $salad->salad_id;?>">
                                <div class="panel-body no-padding">
                                    <div class="col-md-6">
                                        <label>Salad Name<span class="required error">*</span></label>
                                        <input maxlength="50" class="form-control" autocomplete="off" name="salad_name" id="salad_name" value="<?php echo $salad->salad_name;?>" maxlength="30" type="text">
                                    </div> 
                                    <div class="col-md-6">
                                        <label>Salad Name in Arabic</label>
                                        <input maxlength="50" class="form-control" autocomplete="off" name="salad_name_arabic" value="<?php echo $salad->salad_name_arabic;?>" id="salad_name_arabic" maxlength="255" type="text">
                                    </div> 
                                    <div class="clearfix"></div><br>

                                    <div class="col-md-6"><!--select2-->
                                        <label>Category<span class="required error">*</span></label>
                                        <select class="form-control"  style="color:black" name="cat_id" id="cat_id">
                                            <option value="">Select Category</option>
                                            <?php 
                                            if($cats->num_rows() > 0){
                                                foreach ($cats->result() as $crow) {
                                                     $issel="";
                                                        if($salad->cat_id == $crow->cat_id)
                                                            $issel='selected="selected"';
                                            ?> 
                                            <option <?php echo $issel;?> value="<?php echo $crow->cat_id ?>"><?php echo ucwords($crow->cat_name) ?></option>        
                                            <?php }} ?>
                                        </select>
                                    </div>

                                    <div class="col-md-6">
                                        <label>Salad Image<span class="required error">*</span></label>
                                        <input style="color:black" class="form-control" value="<?php echo $salad->salad_img;?>" name="salad_img" id="salad_img" type="file">
                                        <input name="image" type="hidden" value="<?php echo $salad->salad_img;?>">
                                        <img style="height:50px;width:50px"; src="<?php echo base_url(); ?>uploads/salads/<?php echo $salad->salad_img;?>">
                                    </div>

                                    <div class="clearfix"></div><br>

                                    <!--Ingredients Box Start-->
                                    <div class="menu-clone">
                                        <div class="col-md-12">
                                            <h4><b>Select Ingredients</b></h4>
                                        <div style="margin-top:5px;margin-bottom:5px;" class="error" id="ing_error"></div>
                                        <span style="margin-top:5px;margin-bottom:5px;" class="error" id="ing_id_error"></span>
                                       
                                        </div>
                                        <div class="clearfix"></div>
                                         <div id="ingBox1">
                                         <?php 
                                                if($catname->num_rows() > 0){
                                                foreach ($catname->result() as $name) {
                                            ?>
                                            <div class="col-md-4" name="ing_id">
                                                <div class="form-group">
                                                
                                                    <label><?php echo ucwords($name->cat_name)?> <span class="required error">*</span></label>
                                                     <div class="clearfix"></div>
                                                      <select  multiple="multiple"  onselect="validateIngredients()" style="color:black" class="boot-multiselect-demo" name="ing_id[]" id="ing_id">
                                                     
                                                        <?php 
                                                           $saladid= explode(',',$salad->ing_ids);
                                                                //Calling the Ingredients
                                                            $sql="select ing_id,ing_name,ing_price,ing_calories from sm_ingredients where ing_status = 1 and cat_id = $name->cat_id";
                                                            $ingred=$this->Main_model->__callMasterquery($sql);
                                                       
                                                        if($ingred->num_rows() > 0){
                                                              
                                                            foreach ($ingred->result() as $ing) {
                                                                 $issel="";
                                                                if(in_array($ing->ing_id, $saladid))
                                                                 $issel='selected="selected"';
                                                        ?> 
                                                        <option  <?php echo $issel ?> value="<?php echo $ing->ing_id ?>"><?php echo ucwords($ing->ing_name) ?></option>        
                                                        <?php } } else{ ?>
                                                        <?php } ?>
                                                    </select>
                                                  
                                                </div>
                                            </div>
                                              
                                             <?php }} ?>
                                           </div>
                         
                                                      
                                        <div class="clearfix"></div>

                                       
                                    </div>
                                    <!--Ingredients Box End-->

                                    

                                    <br>
                                    <div>
                                    <div class="col-md-6">
                                        <label>Salad Price<span class="required error">*</span></label>
                                        <input class="form-control"   ReadOnly value="<?php echo $salad->salad_price;?>" name="salad_price" id="salad_price" type="text">
                                    </div>
                                      <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Calories <span class="required error">*</span></label>
                                                    <input value="<?php echo $salad->salad_calories;?>"  ReadOnly class="form-control" type="text" name="calories" id="calories">
                                                   
                                                   
                                                </div>
                                            </div>
                                            <div>
                                    <div class="clearfix"></div>
                                       <div class="notice_area salad_desc" name="salad_desc" style="margin-top:10px;"></div>
                                            
                                                   <div class="col-md-12" style="margin-top:20px;">
                                                 <label>Salad Description</label>
                                                <textarea rows="5" name="salad_desc" class="form-control{{ $errors->has('salad_desc') ? ' border border-danger' : '' }}" id="salad_desc"><?php echo $salad->salad_desc?></textarea>
                                           
                                           
                                          <input type="hidden" name="salad_length"  id="salad_length">
                                          <div class="error" id="desc"> </div>
                                         </div>
                                              <div class="clearfix"></div>
                                   
                                    <hr>
                                </div>
                                                <?php }?>
                                <div class="panel-footer">
                                <div class="row">
                             
                                      <button type="button" name="cancel" class="btn btn-primary">Cancel</button>
                                       <button type="submit" name="submit" class="btn btn-primary">Update</button>
                                   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
					<!-- END RECENT PURCHASES -->
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
<!-- END MAIN -->
<div class="clearfix"></div>

<script>
    /* file uploading code */
    var textbox_id =1;
    $(document).ready(function() {

            $('.select-search').select2();
            CKEDITOR.replace( 'salad_desc' );


         $('form[id="saladAddForm"]').submit(function(){
                var desc_length = CKEDITOR.instances['salad_desc'].getData().replace(/<[^>]*>/gi, '').length;
                   $("#salad_length").val(desc_length);
               if(desc_length>0){
                if(desc_length < 10 || desc_length > 250)
                 {
                    $('#desc').text('Salad description must be between 10 to 250');
                     return false;
                 }
                 else{
                         $('#desc').text('');
                        return true;
                 }
                }
                });


        $("button[name='cancel']").click(function(){
            parent.history.back();
            $(".panel-heading").html('<div class="alert alert-success fade in">&times;</a> <strong> Success!</strong>Changes are canceled by you</div>');
        });
        $("button[name='submit']").click(function(){
           
            var ing= $("select.boot-multiselect-demo").val();
            
           if(ing == null){
           $("#ing_error").text('Ingredient  must not be empty');
         
             return false;
           }
        
             else
             {
                  $("#ing_error").text('');
               
                   return true;
             }
             counter++;
        });
           $("button[name='submit']").click(function(){
            var weight= $("#weight").val();
            var calories= $("#calories").val();
           var ing=$("select.ing_id").val();
           if(weight == ""){
           $("#wei_error").text('Weight must not be empty');
             return false;
           }
            else if(calories == ""){
                $("#calory_error").text('Calories must not be empty');
                return false;
             }
             else if(ing == ""){
                $("#ing_error").text('Ingredient must not be empty');
                return false;
             }
             else
              return true;
        });
        $('.select2').select2();
        //document.getElementById('menu_images').addEventListener('change', function(event){ readImage(event), false});
        $( ".preview-images-zone" ).sortable();
        $(document).on('click', '.image-cancel', function() {
        let no = $(this).data('no');
            $(".preview-image.preview-show-"+no).remove();
        });
        
        // Add more menu event fire on add more button 
        $( "body" ).on( "click", "#addMoreMenuButton", function(e) {
            console.log('add more button is click in add menu page');
        if(textbox_id>40){
               $('div#sorry').show();
                return false;
            }
            //var ingHtml = $("#ingBox1").html();
            //$('#add_ing_box').append(ingHtml);

            var newIngHtml = '<div id="textbox_'+textbox_id+'"><div class="col-md-4">'
            newIngHtml += '<div class="form-group"><select  style="color:black" class="form-control select2 unit_type ing_id" name="ing_id[]" id="ing_id"><option value="">Select Ingredients</option>';
                    <?php 
                    if($ingredients->num_rows() > 0){
                        foreach ($ingredients->result() as $irow) {
                    ?> 
                    newIngHtml += '<option value="<?php echo $irow->ing_id ?>"><?php echo ucwords($irow->ing_name) ?></option>';
                    <?php }} ?>
                newIngHtml += '</select><span class="error" id="ing_id_error"></span>';

            newIngHtml += '</div></div><div class="col-md-4"><div class="form-group input-group"><input class="form-control weight" type="text" name="weight[]" id="weight" placeholder="Ingredient Weight"><span id="unit_type" name="unit_type[]" class="input-group-addon">Unit </span></div><span class="error" id="weight_error"></span></div><div class="col-md-4"><div class="form-group"><input class="form-control calories" type="text" name="calories[]" id="calories" placeholder="Ingredient Calories"><span class="error" id="calories_error"></span></div><i class="fa fa-trash-o removeMoreIcon" data-texbox_id="textbox_'+textbox_id+'" style="float: right;position: relative;top: -40px;color: red;right: -20px;"></i></div></div>';

            $('#add_ing_box').append(newIngHtml);
 
            textbox_id = textbox_id +1;
        });

        //Remove the Ingredient Items
        $( "body" ).on( "click", ".removeMoreIcon", function(event) {
            console.log('removeMoreIcon');
            var id=$(this).data('texbox_id');
            //alert(id);
            $('#'+id).remove();
        });

        //Form Validation STart
        $('form[id="saladAddForm"]').validate({
            rules: {
                salad_name: {
                    required : true,
                    minlength: 2,
                    maxlength: 50,
                },
              
               salad_name_arabic: 
                {  
                    	required:true,
						minlength: 2,
						maxlength: 50,
				},
                salad_price: {
                    required: true,
                    number: true,
                    min:1,
                   
                },
            ing_id: {
                    required: true
                },
                weight: {
                    required: true,
                    number: true
                },
               	 
				
            },
            messages: {
                //salad_name: "Please enter salad name",
                cat_id: "Please select category",
                salad_price: {
                    required: "Please enter salad price",
                    number: "Amount should be numeric only",
                    	max:"Amount must not be greater than 1000",
					min:"Amount must not be less than 1"
                },
                 salad_name_arabic: {
                    minlength: "Salad name must be greater than 2 characters",
                    maxlength: "Salad name must be less than 30 characters",
                    
                },
                 salad_name: {
                    minlength: "Salad name must be greater than 2 characters",
                    maxlength: "Salad name must be less than 30 characters",
                    
                },
            },
            submitHandler: function(form) {
                //Here we can any custom work like AJAX calling

                if(false){ } 
                else {
                form.submit();
                }
            }
        });

        $.validator.addMethod("letterswithspace", function(value, element) {
            return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
        }, "Please enter only alphabets");
        $.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z]+$/i.test(value);
        }, "Please enter only alphabets");

        //Custom Validation on Dynamic Ingredient
        $("body").on("change", ".ing_id", function(e) {
            var parent_id = $(this).parent().parent().parent().attr('id');
            validateIngredients(parent_id);
        });

        $("body").on("keyup", ".weight", function(e) {
            var parent_id = $(this).parent().parent().parent().attr('id');
            validateIngredients(parent_id);
        });

        $("body").on("keyup", ".calories", function(e) {
            var parent_id = $(this).parent().parent().parent().attr('id');
            validateIngredients(parent_id);
        });

    });
    
    //Validation function for Ingredients
    function validateIngredients(parent_id){
        var name_regex = /^[\sa-zA-Z]+$/;
        var number_regex = /^[0-9]+$/;
        var float_number = /^[+-]?\d+(\.\d+)?$/;

        var ing_id = $("#"+parent_id+" #ing_id").val();
        var weight = $("#"+parent_id+" #weight").val();
        var calories = $("#"+parent_id+" #calories").val();

        /*if(ing_id != '' &&  weight != '' && calories != ''){
            $("#"+parent_id+" #ing_id_error").hide().html('');
            $("#"+parent_id+" #weight_error").hide().html('');
            $("#"+parent_id+" #calories_error").hide().html('');
        }
        else{*/
            
            if(ing_id == '')
                $("#"+parent_id+" #ing_id_error").show().html('Please select ingredient');
            else
                $("#"+parent_id+" #ing_id_error").hide().html('');

            if(weight == '')
                $("#"+parent_id+" #weight_error").show().html('Please enter weight');
            else if (!weight.match(float_number))
                $("#"+parent_id+" #weight_error").show().html('Please enter digits only');
            else
                $("#"+parent_id+" #weight_error").hide().html('');

            if(calories == '')
                $("#"+parent_id+" #calories_error").show().html('Please enter calories');
            else if (!calories.match(float_number))
                $("#"+parent_id+" #calories_error").show().html('Please enter digits only');
            else
                $("#"+parent_id+" #calories_error").hide().html('');
        //}
    }
    
    
    
</script>
 <script type="text/javascript">
        $(document).ready(function() {
            $('.boot-multiselect-demo').multiselect({
            nonSelectedText: 'Select Ingredient',
            buttonWidth: 300,
            ulWidth: 300,
            enableFiltering: false
        });
        });
    </script>
<!-- Javascript -->
