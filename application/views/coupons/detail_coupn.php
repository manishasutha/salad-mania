
		<div class="main">
			 
			<!--  MAIN CONTENT -->
			<div class="main-content">
				 <?php
		            if($this->session->flashdata('success_msg')){
		            $msg = $this->session->flashdata('success_msg');
		                echo '<div class="alert alert-success fade in">
		                    <a href="#" class="close" data-dismiss="alert">&times;</a>
		                    <strong>Success!</strong>'. $msg.'
		                    </div>';
		            }
		        ?>
				<div class="container-fluid">					
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="row panel-heading">
								<div class="col-lg-4">
								<h3 class="panel-title">Salad Detail</h3>
								</div>
									<div class="col-lg-6 right" style="margin-left:50px;">
										<a href="<?= base_url() ?>coupons" id="coupon" class="btn add btn-info"><i class="fa fa-backward"></i> <span> GO BACK </span></a>
									</div>
								</div>
								<div class="panel-body no-padding">

								<div class="table-responsive">
									<table class="table table-striped datatable">
										
										<tbody>
										<?php
												if(!empty($query) && $query->num_rows() > 0){
												
													foreach ($query->result() as $salad) {
													
				?>		                	  
														   <tr>
														   
														    <th>Salad Name</th>
															<td><?php echo $salad->salad_name;?></td>
															<th>Salad Price</th>
															<td><?php echo $salad->salad_price;?></td>
															<th>Salad Name Arabic</th>
															<td><?php echo $salad->salad_name_arabic;?></td>
														 <th>
														<img style="margin-left:30px; " class="post-thumb" src="<?php echo base_url(); ?>uploads/salads/<?php echo $salad->salad_img; ?>">	</th>
														</tr>
														
															
												<?php 	}
												}
												?>									
									
										</tbody>
									</table>
								</div>
								
								</div>
								<h3 style="margin-top:25px;"> Ingredient Details</h3>
								<div style="margin-top:20px;" class="row">
									<?php
												if(!empty($ingredient) && $ingredient->num_rows() > 0){
													foreach ($ingredient->result() as $ing) {?>
													<div style="margin-top:20px;" class="col-lg-4 table-responsive">
													<table class="table table-striped datatable">
													<tbody>
													<tr><img style="height:200px;width:340px;" class="post-thumb" src="<?php echo base_url(); ?>uploads/ingridents/<?php echo $ing->ing_image; ?>"></tr>
													<tr style="margin-top:40px;"><th>Name</th><td><?php echo $ing->ing_name;?></td></tr>
													<tr><th>Arabic Name</th><td><?php echo $ing->ing_name_arabic;?></td></tr>
													<tr><th>Calories</th><td><?php echo $ing->si_calories;?></td></tr>
													<tr><th>Weight</th><td><?php echo $ing->si_weight.' '.$ing->unit_type;?></td></tr>
													</tbody>
													</table>
													</div>
														
													
												<?php	}
												}
												?>									
										</tbody>
									</table>
								<div class="panel-footer">
									<div class="row">
                                        <div class=" text-left">
                                            
                                        </div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
          
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		
	</div>
