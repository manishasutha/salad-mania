<!-- MAIN -->
<div class="main">
<div class="subheader">
        <ul>
            <li> <?=$this->lang->line('admin')?$this->lang->line('admin'):'Admin';?> / Edit Coupon</li>
        </ul>
    </div>
	<!-- MAIN CONTENT -->
	<div class="main-content">
        <?php
            if($this->session->flashdata('success_msg')){
            $msg = $this->session->flashdata('success_msg');
                echo '<div class="alert alert-success fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Success!</strong>'. $msg.'
                    </div>';
            }
        ?>
        <?php
            if($this->session->flashdata('error_msg') || !empty($error)){
                if(!empty($error)){
                    $msg = $error;

                }else{
                    $msg = $this->session->flashdata('error_msg');
                }
                echo '<div class="alert alert-danger fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Error!</strong>'. $msg.'
                    </div>';
            }
        ?>
		<div class="container-fluid">
			
			<!-- END OVERVIEW -->
			<div class="row">
				<div class="col-md-12">
                            <?php
                            $i=1; 
                            foreach($offer->result() as $off){?>
                                      
					<!-- RECENT PURCHASES -->
                    <form class="col-md-12 form-panel" id="saladAddForm" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="panel new-panel">
                                <div class="panel-heading">
                                    <div class="col-md-12">
                                        <h3 class="panel-title">Edit Coupon</h3>
                                        <!-- <div class="right">
                                        <a href="<?= base_url() ?>coupons" class="btn btn-primary"><i class="fa fa-list"></i> View Coupons</a>
                                        </div> -->
                                    </div>
                                    <br>
                                </div>
                                
                                <div class="notice-area"></div>

                                <div class="panel-body no-padding">
                                    <div class="col-md-6">
                                        <label>Coupon Title<span class="required error">*</span></label>
                                        <input class="form-control validate" value="<?php echo $off->offer_title?>" autocomplete="off" name="coupon_title" id="coupon_title" maxlength="30" type="text">
                                    </div> 
                                    <div class="col-md-6">
                                        <label>Coupon Code<span class="required error">*</span></label>
                                        <input class="form-control validate" value="<?php echo $off->coupon_code?>" autocomplete="off" name="coupon_code" id="coupon_code" maxlength="30" type="text">
                                    </div> 
                                    
                                    <div class="clearfix"></div><br>

                                   <div class="col-md-6">
                                        <label>Start Date<span class="required error">*</span></label>
                                        <input min="<?php echo date('d-m-Y')?>" class="form-control start_date date1 validate" autocomplete="off"  name="start_date" id="start_date" maxlength="30" type="text" value="<?php echo date("d-m-Y", strtotime($off->start_date)) ;?>">
                                        <span class="startdate"></span>
                                        <span id="startdateError" class="text text-danger"> <?php echo form_error('start_date', '<div class="error">', '</div>'); ?></span>

                                    </div>
                                    <div class="col-md-6">
                                        <label>End Date<span class="required error">*</span></label>
                                        <input class="form-control end_date date2 validate" value="<?php echo date("d-m-Y", strtotime($off->end_date)) ;?>"  autocomplete="off" id="end_date" name="end_date" maxlength="30" type="text">
                                        <span class="text-danger enddate"></span>
                                        <span id="enddateError" class="text text-danger end"> <?php echo form_error('end_date', '<div class="error">', '</div>'); ?></span>

                                    </div>

                                    <!-- <div class="col-md-6">
                                        <label>Start Date<span class="required error">*</span></label>
                                        <input class="form-control date1 validate" id="datetimepicker1" name="start_date" value="<?php echo date("d-m-Y", strtotime($off->start_date)) ;?>">
                                      <span id="startdate" class="startdate"></span>
                                      </div>     
                                     <div class="col-md-6">
                                        <label>End Date<span class="required error">*</span></label>
                                        <input class="form-control date2 validate" value="<?php echo date("d-m-Y", strtotime($off->end_date)) ;?>" autocomplete="off" name="end_date" id="datetimepicker2" maxlength="30" type="text">
                                        <span id="enddate" class="text-danger enddate"></span>
                                        <span id="enddateError" class="text text-danger end"> <?php echo form_error('end_date', '<div class="error">', '</div>'); ?></span>
                                      </div>                   -->
                                                    
                                <div class="clearfix"></div><br>
                                    <div class="col-md-6">
                                        <label>Discount Rate <span>(%)</span><span class="required error">*</span></label>
                                        <input maxlength=3 class="form-control validate" value="<?php echo round($off->discount_rate)?>" autocomplete="off" name="discount_rate" id="discount_rate" maxlength="30" type="text">
                                    </div> 
                                   
                                   
                                  <br>

                                    <div class="clearfix"></div>
                                    <div class="notice-area" style="margin-top:10px;"></div>
                                            <div class="notice-area" style="margin-top:10px;"></div>
                                            
                                                   <div class="col-md-12" style="margin-top:20px;">
                                                 <label><?=$this->lang->line('description')?$this->lang->line('description'):'Coupon Description'?></label>
                                                <textarea name="coupon_desc" id="coupon_desc"><?php echo $off->offer_desc?></textarea>
                                              <input type="hidden" name="desc_length"  id="desc_length">
                                          <div class="error" id="desc"> </div>
                                            </div>
                                        
                                            
                                                  
                                            
                                            </div>
                                        
                                    <!--Ingredients Box End-->

                                </div>
                                <div class="panel-footer" style="margin-top:20px;">
                                    <div class="row">
                                       <button type="submit" name="submit" class="btn btn-primary">Update</button>
                                        <button type="submit" name="cancle" class="btn btn-primary">Cancel</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                                                <?php }?>
					<!-- END RECENT PURCHASES -->
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
<!-- END MAIN -->
<div class="clearfix"></div>
<script>
    /* file uploading code */
  
    $(document).ready(function() {
         $(document).ready(function(){
            //date picker show js
            var date = new Date();
            date.setDate(date.getDate());

            $("#start_date").datepicker({
                numberOfMonths: 2,
                format: 'dd-mm-yyyy',
                startDate: date,
                onSelect: function(selected) {
                    $("#end_date").datepicker("option","minDate", selected)
                }
            });
            $("#end_date").datepicker({
                numberOfMonths: 2,
                format: 'dd-mm-yyyy',
                startDate: date,
                onSelect: function(selected) {
                    $("#start_date").datepicker("option","maxDate", selected)
                }
            });


        });
        var x = true;

        //ck editor validation with Start/End Dates
        $('form[id="saladAddForm"]').submit(function(){
            var desc_length = CKEDITOR.instances['coupon_desc'].getData().replace(/<[^>]*>/gi, '').length;
            $("#desc_length").val(desc_length);
            if(desc_length > 0){
                if(desc_length < 10 || desc_length > 250)
                {
                    $('#desc').text('Coupon description must be between 10 to 250 characters');
                    x = false;
                }
                else{
                    $('#desc').text('');
                    x = true;
                    //return true;
                }
            }

            var start_date = $("#start_date").val();
            var end_date = $("#end_date").val();
            if(start_date != '' && end_date != '')
            {
                start_date = start_date.split("-");
                //alert(start_date[1]); alert(start_date[0]); alert(start_date[2]);
                var startDate = new Date(start_date[1] + "/" + start_date[0] + "/" + start_date[2]);
                
                end_date = end_date.split("-");
                var endDate = new Date(end_date[1] + "/" + end_date[0] + "/" + end_date[2]);
                if (startDate <= endDate){
                    //alert("proceed");
                    $("span#enddateError").html("");
                    x = true;
                    //return true;
                }
                else{
                    //alert("not proceed");
                    $("span#enddateError").html("End date should be greater than start date");
                    x = false;
                    //return false;
                }
            }

            if(x)
                return true;
            else
                return false;

        });


        //Form Validation STart
        $('form[id="saladAddForm"]').validate({
            rules: {
                coupon_title: {
                    required : true,
                    minlength: 2,
                    maxlength: 30,
                    alphanumeric: true,
                },
                coupon_code:
                {   required : true,
                    minlength: 2,
                    maxlength: 30,
                    alphanumeric: true,

                },
                start_date: "required",
                end_date: {
                    required:true,
                },

                discount_rate: {
                    required: true,
                    number: true,
                    min:1,
                    max:100,
                },
            },
        });

        $.validator.addMethod("letterswithspace", function(value, element) {
            return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
        }, "Please enter only alphabets");

        $.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z]+$/i.test(value);
        }, "Please enter only alphabets");

        $.validator.addMethod("alphanumeric", function(value, element) {
            return this.optional(element) || /^[A-za-z0-9\s]+$/i.test(value);
        }, "Letters, numbers, and underscores only please");


        //ck editor show js
        $('.select-search').select2();
        CKEDITOR.replace( 'coupon_desc' );
        CKEDITOR.replace( 'description_italian' );

    });
    </script>
    <!-- Javascript -->
