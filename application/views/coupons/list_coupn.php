
	
		<div class="main">
			 
			<!--  MAIN CONTENT -->
			<div class="main-content">
				<?php
					if($this->session->flashdata('success_msg')){
					$msg = $this->session->flashdata('success_msg');
						echo '<div class="alert alert-success fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Success!</strong>'. $msg.'
			        		</div>';
		        	}
				?>
				<div class="container-fluid">
					
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading2">
									<h3 class="panel-title2">Coupon List</h3>
							</div>
								<div class="panel-body no-padding">
								<div class="table-responsive">
									<table class="table table-striped datatable">
										<thead>
											<tr>
												<th>S.No</th>
												<th>Coupon Title</th>
												<th>Coupon Code</th>
												<th>Discount</th>
												<th>Start Date</th>
												<th>End Date</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
												<?php
												if(!empty($query) && $query->num_rows() > 0){
													$enable = $this->lang->line('enable')?$this->lang->line('enable'):'Enable';
													$disable = $this->lang->line('disable')?$this->lang->line('disable'):'Disable';
													
													foreach ($query->result() as $offer) {
														if($offer->offer_status){
															$status_span = '<span class="label label-success">'.$enable.'</span>';
														}else{
															$status_span = '<span class="label label-danger">'.$disable.'</span>';
														}?>
													<tr>
															 <td></td>
															 <td><?php echo $offer->offer_title;?></td>
															 <td><?php echo $offer->coupon_code;?></td>
														
															  <td><?php echo round($offer->discount_rate).'%'; ?></td>
															   <td><?php echo date("d-m-Y", strtotime($offer->start_date)) ;?></td>
															    <td><?php echo date("d-m-Y", strtotime($offer->end_date)) ;?></td>
																<td><a><?php echo $status_span ?></a></td>
															 <td>
                                                			 	<a class="btn btn-xs btn-primary" title="Edit Coupon!" href="<?=base_url()?>coupons/edit/<?=$offer->offer_id;?>" data-cat-id="<?php echo $offer->offer_id?>"><i class="fa fa-pencil"></i>
                                                			 	</a>
                                                			 	
                                                			 	 <?php if($offer->offer_status==1){
																				?>
																					<a class="btn btn-xs btn-danger deleteItem" title="Disable Coupon!" href="<?=base_url()?>coupons/delete/<?=$offer->offer_id;?>">
																							<i class='fa fa-eye-slash'></i>
																						</a>
																				<?php }
																				else{
																				?>
																					 <a style="" class="btn btn-xs btn-info enableItem" title="Enable Coupon!" href="<?=base_url()?>coupons/enable/<?=$offer->offer_id;?>">
                                                			 		<i  class="fa fa-eye"></i>
                                                			 	</a>
																				<?php } ?>
																				
                                                			 </td>
                                                			
                                                			 </tr>
															
												<?php	}
												}
												?>									
										</tbody>
									</table>
								</div>
								</div>
								<div class="panel-footer">
									<div class="row">
	                                    <div class=" text-left">
	                                        
	                                    </div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
                      
		<!-- END MAIN -->
		<div class="clearfix"></div>
		
	</div>
	<!-- END WRAPPER -->
	<!-- Edit category model start added by ravindra -->
		<div class="modal" id="editCatModal"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <form enctype="multipart/form-data" id="form" action="<?=base_url()?>users/edit" method="post">
			<div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header text-left">
		                <h4 class="modal-title w-100 font-weight-bold"><?=$this->lang->line('Edit Category')?$this->lang->line('Edit Category'):'Edit User'?> </h4>
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -27px;opacity: 0.6;">
		                   &times;
		                </button>
		            </div>
		            <div class="modal-body mx-2">
		                <div class="md-form mb-3 ">
		                	<label data-error="wrong" data-success="right" for="cat_edit_name">User Name</label>
		                	<br><span id="name_error"></span>
		                    <input type="text" id="name" name="user_name" class="form-control validate">
		                    <input type="hidden" name="id" id="cat_edit_id">
		                </div>
		                <div class="md-form mb-3 ">
		                	<label data-error="wrong" data-success="right" for="cat_edit_name">User Email</label>
		                	<br><span id="email_error"></span>
		                    <input type="text" id="email" name="email" class="form-control validate">
		                </div>
						
						<div class="md-form mb-3 ">
		                	<label data-error="wrong" data-success="right" for="cat_edit_name">User Phone</label>
		                	<br><span id="phone_error"></span>
		                    <input type="text" id="phone" name="phone" class="form-control validate">
		                </div>
						
		            </div>
		            <div class="modal-footer d-flex button justify-content-center">
		                  <button class="btn-primary add-ing-button" id="button" name="submit" type="submit"><i class="fa fa-plus"></i>update</button>

				    </div>
		        </div>
		    </div>
			</form>
		</div>
	<!-- Edit category model end -->

		<script>
		$(document).ready(function(){
		
		$(".close").click(function(){
			$("#editCatModal").hide();
			});
			
		});
		</script>