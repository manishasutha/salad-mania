
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google Analytics Start -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-8C9870NPDC"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-8C9870NPDC');
    </script>
    <!--Google Analytics End-->
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Salad Mania | <?=$title?></title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?=INCLUDE_FRONT_ASSETS?>img/apple-icon-180x180.png" type="image/x-icon">

    <link rel="stylesheet" href="<?=base_url()?>assets/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/vendors/css/vendor.bundle.addons.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">

    <!-- Font awesome -->
<!--    <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">-->
    <link href="<?=INCLUDE_FRONT_ASSETS?>css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<?=INCLUDE_FRONT_ASSETS?>css/bootstrap.css" rel="stylesheet">
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="<?=INCLUDE_FRONT_ASSETS?>css/slick.css">
    <!-- Date Picker -->
    <link rel="stylesheet" type="text/css" href="<?=INCLUDE_FRONT_ASSETS?>css/bootstrap-datepicker.css">
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="<?=INCLUDE_FRONT_ASSETS?>css/jquery.fancybox.css" type="text/css" media="screen" />
    <!-- Theme color -->
    <link id="switcher" href="<?=INCLUDE_FRONT_ASSETS?>css/theme-color/green-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="<?=INCLUDE_FRONT_ASSETS?>css/style.css" rel="stylesheet">

<!--    <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">-->

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Tangerine' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Prata' rel='stylesheet' type='text/css'>

    <link href="<?=INCLUDE_FRONT_ASSETS?>toggle/toggle.css" rel="stylesheet">
    <script src="<?=INCLUDE_FRONT_ASSETS?>toggle/toggle.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.googlemap/1.5.1/jquery.googlemap.min.js.map"></script>

    <link rel="stylesheet" href="https://www.paytabs.com/express/express.css">
    <script src="https://www.paytabs.com/theme/express_checkout/js/jquery-1.11.1.min.js"></script>
    <script src="https://www.paytabs.com/express/express_checkout_v3.js"></script>

    <link href="<?=INCLUDE_FRONT_ASSETS?>css/HoldOn.css" rel="stylesheet" type="text/css">


    <!-- Toggle -->
    <link href="<?=INCLUDE_FRONT_ASSETS?>toggle/toggle.css" rel="stylesheet">
    <script src="<?=INCLUDE_FRONT_ASSETS?>toggle/toggle.js"></script>

<!--    <link href="https://cdn.lineicons.com/1.0.1/LineIcons.min.css" rel="stylesheet">-->



    <!-- jQuery library -->
<!--    <script src="--><?//=INCLUDE_FRONT_ASSETS?><!--js/jquery.min.js"></script>-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=INCLUDE_FRONT_ASSETS?>js/bootstrap.js"></script>
    
    <!--language button toggel -->
    <link href="<?=INCLUDE_FRONT_ASSETS?>toggle/toggle.css" rel="stylesheet" type="text/css">
    <script src="<?=INCLUDE_FRONT_ASSETS?>toggle/toggle.js"></script>


    <!-- validation file-->
    <script src="<?=INCLUDE_FRONT_ASSETS?>js/jquery.validate.min.js"></script>
    <script type="text/javascript">
        google_ad_client = "ca-pub-2783044520727903";
        google_ad_slot = "2780937993";
        google_ad_width = 728;
        google_ad_height = 90;
    </script>
    <style>
        @-webkit-keyframes placeHolderShimmer {
            0% {
                background-position: -468px 0;
            }
            100% {
                background-position: 468px 0;
            }
        }

        @keyframes placeHolderShimmer {
            0% {
                background-position: -468px 0;
            }
            100% {
                background-position: 468px 0;
            }
        }
        label.error  {
            display: block;
            margin-top: 0px;
            margin-bottom: 10px !important;
            color: red;
            height: 0px !important;
            font-size: 12px;
            line-height: 12px;
            font-weight: normal;
        }
        #sub, #add {
            background: #eee;
            border: 0;
            font-size: 14px;
            font-weight: 600;
            color: #555;
            padding: 5px;
            width: 30px;}

        .bi-qty input{border:0; width: 28px; padding-left: 7px}

        /* Style the tab */
        .tab {
            float: left;
            border-top: 0;
            background-color: #f4f4f4;
            width: 25%;
            height: 300px;
        }

        /* Style the buttons inside the tab */
        .tab button {
            display: block;
            background-color: inherit;
            color: #888;
            padding: 10px 15px;
            width: 100%;
            border: none;
            outline: none;
            text-align: left;
            cursor: pointer;
            transition: 0.3s;
            font-size: 13px;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            color:#000; font-weight: 700
        }

        /* Create an active/current "tab button" class */
        .tab button.active {
            background-color: #fff; border-right: 0; color:#000; font-weight: 700
        }

        /* Style the tab content */
        .tabcontent {
            float: left;
            padding: 0px 12px;

            width: 75%;
            border-left: none;
            height: 300px;
            border-top: 0
        }

        .modal-body{padding:0}
        .modal-footer{background: #fff}
    </style>

    <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    
    

    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    


    <![endif]-->
   

    <script type="text/javascript">
        var base_url = '<?php echo base_url() ?>';
      
    </script>
    <?php
    if(isset($verified)){
       ?>
        <script>
        var verified =true;
        </script>
    <?php
    }else{
        ?>
        <script>
            var verified =false;
        </script>
    <?php
    }
    ?>
    
    
    <?php

    if($this->session->userdata('user_login_session')){
        $login_id=$this->session->userdata('user_login_session');
        $id=$login_id['id'];

        ?>
        <script>
            var login_status =true;
            var encrypt_id='<?=encryptPassword($id)?>';
        </script>
    <?php
    }elseif ($this->session->userdata('guest_user_session')){
        
        $login_id=$this->session->userdata('guest_user_session');
        $id=$login_id['id'];
        ?>
        <script>
            var login_status =true;
            var encrypt_id='<?= ($id)?>';
        </script>
        <?php
    }
    else{
        ?>
        <script>
            var login_status =false;
        </script>
    <?php
    }
    ?>
</head>
<body >

<?php
    $this->load->view('frontend/template/header');
    $this->load->view('frontend/template/cart_model');
    $this->load->view($page);

    if($page =='frontend/index'){
        $this->load->view('frontend/template/contact');
    }

    $this->load->view('frontend/template/fotter');
?>

<!-- Slick slider -->
<script type="text/javascript" src="<?=INCLUDE_FRONT_ASSETS?>js/slick.js"></script>
<!-- Counter -->
<script type="text/javascript" src="<?=INCLUDE_FRONT_ASSETS?>js/waypoints.js"></script>
<script type="text/javascript" src="<?=INCLUDE_FRONT_ASSETS?>js/jquery.counterup.js"></script>
<!-- Date Picker -->
<script type="text/javascript" src="<?=INCLUDE_FRONT_ASSETS?>js/bootstrap-datepicker.js"></script>
<!-- Mixit slider -->
<script type="text/javascript" src="<?=INCLUDE_FRONT_ASSETS?>js/jquery.mixitup.js"></script>
<!-- Add fancyBox -->
<script type="text/javascript" src="<?=INCLUDE_FRONT_ASSETS?>js/jquery.fancybox.pack.js"></script>

<!-- Custom js -->
<script src="<?=INCLUDE_FRONT_ASSETS?>js/custom.js"></script>

<!-- calendar -->
<!-- End Subscription section -->
<!-- jQuery library -->

<script src="<?=INCLUDE_FRONT_ASSETS?>custom/js/common.js"></script>
<script src="<?=INCLUDE_FRONT_ASSETS?>js/HoldOn.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="<?=INCLUDE_FRONT_ASSETS?>Date-Time-Picker-Bootstrap-4/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    $(function () {
        // var mon=$("#mon").val();
        // var sat=$("#sat").val();
        // var sun=$("#sun").val();
        // var monday=mon.split('to');
        // var mondays1=monday[1].split(' ');
        // var mondays2=monday[0].split(' ');


        $('#datetimepicker').datetimepicker({
//            minDate:new Date()
            format : 'MM/DD/YYYY HH:mm',
//            Format: 'd MM, y',
            minDate: moment().add(1, 'h'),enabledHours:[] ,
            maxDate: moment().add(2, 'days')
        });

        $('#datetimepicker1').datetimepicker({
//            minDate:new Date()
            format : 'MM/DD/YYYY HH:mm',
//            Format: 'd MM, y',
            minDate: moment().add(1, 'h'),enabledHours:[] ,
            maxDate: moment().add(2, 'days')
        });
    });



</script>


<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
    $(document).ready(function(){
        $('#contact_us').on('click',function(){
         
            $('#contact-us').show();
            $('#contact-dietician').hide();
            // $('#contact-us').slideDown('show');
        });
        $('#dietician').on('click',function(){
        
            $('#contact-us').hide();
            $('#contact-dietician').show();
            //$('#contact-dietician').slideDown('show');
        });
    });

    $('a.scroller').click(function() {
        var val = $(this).attr('href');
        var target = val.substr(val.indexOf("#") + 1);
        console.log(target);
        $('html, body').animate({
            scrollTop: $('#'+target).offset().top
        }, 1000);
        return true;
    });

    if ($(window).width() > 768) {
        $(window).bind('scroll', function() {
            var target_on = $('#mu-title-mark').offset().top;
            if ($(window).scrollTop() > target_on) {
                $('.sticky-footer').hide();
            }
            else if ($(window).scrollTop() < target_on) {
                $('.sticky-footer').show();
            }
        });
    }
    else {

    }
    function openCity(evt, cityName) {
        
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
    document.getElementById("defaultOpen2").click();
</script>


<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();


</script>

</body>
</html>