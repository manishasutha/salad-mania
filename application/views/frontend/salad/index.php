<!-- Start Restaurant Menu -->
<div class="mu-title" id="mu-title-mark">

</div>

<section id="mu-restaurant-menu" class="menu-img" >
    <div class="">
        <div class="row">
            <div class="col-md-12" style='float: none; margin: auto'>
                <div class="mu-restaurant-menu-area">
                    <div class="mu-title" style="    margin-top: -20px;">
                        <!--<span class="mu-subtitle">Our Menu</span>-->
                        <h2><?=$this->lang->line('menu');?></h2>
                        <a href="#" target="_blank" id="nutri-filter"><i class="fa fa-heartbeat"></i><?=$this->lang->line('filter_nutrition');?> </a>
                        <div class="menu-tags">
                            <?php
                            $first = true;
                            foreach ($categories as $cat){
                                 $language= $this->session->userdata('language');
                                 $cat_id=$cat->cat_id;
                                $cat_name=$cat->cat_name;
                                if($language=='Arabic'){
                                    $cat_name=$cat->cat_name_arabic;
                                }
                                if ( $first ) {
                                    $first = false;

                                    ?>
                                    <a href="#" class="active cat_id" data-id="<?= $cat->cat_id ?>" id="<?= $cat->cat_id ?>"><?= $cat_name ?></a>

                                    <?php
                                }else{
                                    ?>
                                    <a href="#" class="cat_id" data-id="<?= $cat->cat_id ?>" id="<?= $cat->cat_id ?>"><?= $cat_name ?></a>
                                    <?php

                                }
                            }
                            ?>
                    </div>
                        <br />   
                         
                    <div class="row mu-restaurant-menu-content" >
                        <div id="menu_data" class="mu-restaurant-menu-content ">
                         <?php
                            foreach ($categories as $cat){
                                // echo $cat_id;
                                    if($cat->contains_make_own == 1){ ?>
                                    <div id="cat_id_1">
                                        <input type="hidden" value="1" id="cat_id">
                                        <input type="hidden" value="" id="salad_0"><div class="col-md-6"> <div class="media"> <div class="media-img">
                                        <img onclick="add_to_cart('create_own',1,0,'Make Your Own')" class="media-object" style="cursor: pointer" src="<?=INCLUDE_SALAD_IMAGE_PATH ?>custom.png" alt="img">
                                        </div><div style="text-align: center" class="media-body  text-center">
                                        <h4 onclick="add_to_cart('create_own',1,0,'Make Your Own')" class="media-heading text-center" style="text-align: center!important; width: 100%;"><?= $this->lang->line("make_own_title") ?></h4>
                                        <h5 class="media-category text-center" style="text-align: center!important; width: 100%;">---</h5>
                                        <div class="mu-ingredients text-center" style="margin-top: 16px;text-align: center!important;"></div>
                                        <a onclick="add_to_cart('create_own',1,0,'Make Your Own')" style="    float: none;" class="media-add text-center"><?= $this->lang->line("make_now") ?> </a> </div></div></div>
                                    </div>
                                     <div id="cat_id_5" style="display:none">
                                        <input type="hidden" value="5" id="cat_id">
                                        <input type="hidden" value="" id="salad_0"><div class="col-md-6"> <div class="media"> <div class="media-img">
                                        <img onclick="add_to_cart('create_own',5,0,'Make Your Own')" class="media-object" style="cursor: pointer" src="<?=INCLUDE_SALAD_IMAGE_PATH ?>custom.png" alt="img">
                                        </div><div style="text-align: center" class="media-body  text-center">
                                        <h4 onclick="add_to_cart('create_own',5,0,'Make Your Own')" class="media-heading text-center" style="text-align: center!important; width: 100%;"><?= $this->lang->line("make_own_title") ?> </h4>
                                        <h5 class="media-category text-center" style="text-align: center!important; width: 100%;">---</h5>
                                        <div class="mu-ingredients text-center" style="margin-top: 16px;text-align: center!important;"></div>
                                        <a onclick="add_to_cart('create_own',5,0,'Make Your Own')" style="    float: none;" class="media-add text-center"><?= $this->lang->line("make_now") ?> </a> </div></div></div>
                                    </div>
                            <?php break; } } ?>
                        </div>



                        <div id="load_data" class="mu-restaurant-menu-content" >
                        

                        </div>
                        </div>
<!--                    <div id="load_data_message" class="mu-restaurant-menu-content"></div>-->
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </div>
            </div>
        </div>
    </div>
</section>

<script src="<?=INCLUDE_FRONT_ASSETS?>custom/js/menu.js"></script>

<script>
 $(document).on('click','.cat_id',function(){
var id=$(this).data('id');
if(id==1){
$('#cat_id_1').show();
$('#cat_id_5').hide();
}
else if(id==5){
$('#cat_id_5').show();
$('#cat_id_1').hide();
}
else{
   $('#cat_id_1').hide();
   $('#cat_id_5').hide();
}
});
</script>