<!-- Start Contact section -->
<section id="mu-contact" style="background: #fff; background-image: url(assets/img/shopbag.png); background-repeat: no-repeat; background-position: left bottom; padding: 30px 0">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-contact-area">
                    <div class="mu-title" id="mu-title-mark">
                        <!--              <span class="mu-subtitle">Get In Touch</span>-->
                        <!-- <h2>Terms And Conditions</h2> -->
                    </div>
                    <div class="mu-contact-content">
						<br>
						<br>
                        <div class="row">
							<?php  $row = $terms->row();
								 $terms=$row->tc_page;
								$language= $this->session->userdata('language');
                                if($language=='Arabic'){
                                    $terms=$row->tc_page_arabic;
                                }
							 ?>
                            <div class="col-md-12" >
                                <div class="mu-contact-right">
                                   <p align="center"><strong>Salad Mania Co </strong></p>
								<p align="center"><strong>TERMS OF USE AGREEMENT</strong></p>
								<?= $terms ?>
								
                                </div>
                            </div>
                        </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Contact section -->