<script src="https://maps.googleapis.com/maps/api/js?libraries=places,drawing,geometry&.js&key=AIzaSyCJxQJ2c7U_YJTuWuseTNo7fxgA16F4htc" async defer></script>
                                               
<?php print_r('hello');?>
<!-- Start Restaurant Menu -->
<section id="mu-restaurant-menu"  class="menu-img">
    <div class="container">
        <?php
                    if($this->session->flashdata('success_msg')){
                        $msg = $this->session->flashdata('success_msg');
                        echo '<div class="alert alert-success fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong>'. $msg.'
                            </div>';
                    }
        else if($this->session->flashdata('error_msg')){
                        $msg = $this->session->flashdata('error_msg');
                        echo '<div class="alert alert-danger fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Error!</strong>'. $msg.'
                            </div>';
        }
        ?>
     <div style="display:none" id="error-div" class="alert alert-success"></div>
        <div class="row">
            <div class="col-md-6 col-xs-12" style="float: none; margin: auto">
                    <?php
                    if($carts){
                        $total_price =0;
                        ?>
                        <div class="order-preview order-preview1">
                            <div class="op-section-head">
                                <h4 class="bag-title"><?=$this->lang->line('order');?></h4>
                                <p>
                                    <?php
                                    $date=date("Y/m/d");
                                    $yrdata= strtotime($date);
                                    ?>
                                    <?=date('d M Y', $yrdata)?>
                                </p>
                            </div>
                            <div id="address_error_message">

                            </div>
                            <div class="op-section hide-on-change-add">

                                <div class="row">

                                    <div class="col-md-5">

                                        <h4 class="op-section-title"><?=$this->lang->line('delivery');?>/<?=$this->lang->line('pick_up');?></h4>
                                        <table>

                                            <tr>
                                                <?php
                                                if($delivery_type =='delivery') {
                                                    ?>
                                                    <td>
                                                        <img src="<?= INCLUDE_FRONT_ASSETS ?>img/delivery_icon.png"/>
                                                    </td>
                                                    <td><?=$this->lang->line('delivery');?></td>
                                                    <?php
                                                }elseif($delivery_type =='pickup') {
                                                    ?>
                                                    <td>
                                                        <img src="<?= INCLUDE_FRONT_ASSETS ?>img/pickup_icon.png"/>
                                                    </td>
                                                    <td><?=$this->lang->line('pick_up');?></td>
                                                    <?php
                                                }
                                                ?>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="address_div" <?php if($address){ ?> check_address="true" <?php }else{ ?> check_address="false" <?php } ?> class="col-md-7">
                                        <?php
                                        if($delivery_type=='delivery'){
                                            ?>
                                            <h4 class="op-section-title"><?=$this->lang->line('delivery_address');?></h4>
                                            <?php
                                            if($address){
                                                ?>
                                                <?=$address->addline1.', Amman'?>
                                                <?php
                                            }
                                            ?>
                                            <br>

                                            <?php
                                            if($this->session->userdata('user_login_session')){
                                                $login=$this->session->userdata('user_login_session');
                                                ?>
                                                <button type="button" onclick="confirm_address()"  class="confirm_address1 btn btn-finish btn-primary" name="finish" ><?php if($address){ echo $this->lang->line("change_address"); } else { echo $this->lang->line('confirm_address'); } ?></button>
                                                <?php
                                            }else if($this->session->userdata('cart_data')){
                                                $login=$this->session->userdata('guest_user_session');
                                                ?>
                                                <button type="button" onclick="confirm_address()" class="confirm_address1 btn btn-finish btn-primary" name="finish" ><?php if($address){ echo $this->lang->line('change_address'); } else { echo $this->lang->line('confirm_address'); } ?> </button>
                                                <?php
                                            }
                                            ?>


                                            <?php
                                        }elseif($delivery_type =='pickup'){
                                            ?>
                                            <h4 class="op-section-title"><?=$this->lang->line('pickup_add');?></h4>
                                            <?php
                                            $address=getSingleFieldDetail('id',1,'address','sm_app_settings');
                                            ?>
                                            <p id="address"><?=$address?> </p>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>


                            </div>

                            <hr>

                            <?php
                            if($delivery_type =='pickup'){
                                $checkout_values=$this->session->userdata('checkout_session');
                                ?>

                                <div class="op-section">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4 class="op-section-title text-left"><?=$this->lang->line('pickup_details');?></h4>
                                                <p class="col-md-4 font-weight-bold text-uppercase "><?=$this->lang->line('date');?>:</p>
                                                <p class="col-md-8 pickup-color"><?=$checkout_values['date']?></p>
                                                <p class="col-md-4 font-weight-bold text-uppercase "><?=$this->lang->line('time');?></p>
                                                <p class="col-md-8 pickup-color"><?=$checkout_values['time']?></p>

                                                <p class="col-md-4 font-weight-bold text-uppercase "><?=$this->lang->line('name');?>:</p>
                                                <p class="col-md-8 pickup-color"><?=$checkout_values['name']?></p>
                                                <p class="col-md-4 font-weight-bold text-uppercase "><?=$this->lang->line('mobile');?>:</p>
                                                <p class="col-md-8 pickup-color">+962 <?=$checkout_values['mobile']?></p>
                                        </div>

                                    </div>
                                </div>
                                <?php
                            }
                            ?>


                            <div class="op-section" id="loginModal2">
                                <form action="javascript:void(0);" id="confirm_address_from" method="post">
                                   
                                    <div  class="address-padding address-detail"  >
                                        <label id="address_id-error" class="error text-center" for="address_id"></label>
                                        <div id="all_addresses" style="height: 160px; overflow-y: auto; overflow-x: hidden">

                                        </div>

                                        <div style="margin-top: 10px">
                                            <button type="button" class="btn btn-default hide-deliver-form"><?=$this->lang->line('cancel');?></button>
                                            <button type="submit"  class="hide-loginmodal btn btn-primary"><?=$this->lang->line('confirm');?></button>

                                            <button type="button" style="float: right" class="btn btn-primary show-address-form"><?=$this->lang->line('add_new_address');?></button>

                                        </div>
                                    </div>
                                </form>

                                <div class="address-form address-padding" style="margin-bottom: 40px">
                                    <form id="address_update" class="mu-contact-form text-center" method="post" >
                                         <?php
                                        if($delivery_type =='delivery') {?>
                                             <input type="hidden" value="delivery" id="deliveryType">
                                    <?php } else if($delivery_type =='pickup') { ?>
                                            <input type="hidden" value="pickup" id="deliveryType">
                                     <?php } ?>
                                        <h4 class="op-section-title" style="    margin-bottom: 20px;"><?= $this->lang->line('add_address');?></h4>

                                        <h4 class="alert alert-info" role="alert"><?=$this->lang->line('delivery_in_jordon');?> </h4>
                                        <div class="mu-contact-left">
                                            <div class="form-group">
                                                <input type="text" value="Amman" readonly class="form-control" name="city" id="city"   placeholder="<?=$this->lang->line('city');?>City">
                                            </div>

                                            <div class="form-group">
                                                <input type="text" id="location" name="location" class="form-control" placeholder="<?=$this->lang->line('address');?>">
                                            </div>
                                            <div  style="margin-bottom: 45px;">
                                                <div style="float: right">
                                                    <button type="button"  class="btn btn-default hide-address-form" ><?= $this->lang->line('cancel');?></button>
                                                    <button type="submit"  class="btn btn-primary "><?= $this->lang->line('save');?></button>
                                                </div>
                                            </div>
                                            <div class="row"  id="first_map_div" style="display: none; margin-top: 65px;">
                                                <div class="map" id="map_canvas" style="width: 100%; height: 350px; "></div>
                                                <div class="form_area">
                                                   <input type="hidden" readonly class="form-control" name="location" id="location">
                                                    <div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="hidden" class="form-control" name="city_latitude_first" placeholder="City/Place Latitude" readonly id="city_latitude_first"/>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <input type="hidden" class="form-control"  name="city_longitude_first" placeholder="City/Place Longitude" readonly id="city_longitude_first"/>
                                                            </div>
                                                        </div>
                                                        <span style="font-size: 10px; color: red;" id="city_longitude_first-error"
                                                              class="error"></span>
                                                       <span style="font-size: 10px; color: red;" id="marker_position_origin-error"
                                                                  class="error"></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                        

                                    </form>
                                </div>
                            </div>

                            <div class="op-section">
                                <h4 class="op-section-title"><?=$this->lang->line('items');?></h4>
                                <?php
                                if($carts){
                                    foreach ($carts as $cart){
                                        if($cart['salad_type'] ==1) {
                                            $salad_name= $cart['salad_name'];
                                            $language= $this->session->userdata('language');
                                            if($language=='Arabic'){
                                                $salad_name= $cart['salad_name_arabic'];
                                            }
                                            ?>
                                            <div class="bag-item">
                                                <div class="bag-item-group">
                                                    <div class="bi-img"><img
                                                                src="<?= INCLUDE_SALAD_IMAGE_THUMB_PATH . $cart['salad_img'] ?>"/></div>
                                                    <div class="bi-title"><?=$salad_name?></div>
                                                     <div class="bi-qty" style="display: flex"><?=$cart['quantity']?></div> 
                                                    <div class="bi-price"><span class="salad_price"><?=round($cart['total_price'],'1')?><?=$this->lang->line('jd');?></span></div>
                                                </div>

                                                <!-- <?php
                                                if(count($cart['main_ingrediants']) > 0){
                                                    $subcat_id=array_unique(array_column($cart['main_ingrediants'],'subcat_id'));
                                                    ?>
                                                    <div class="bi-title">Main</div>
                                                    <div class="bag-item-addons">
                                                        <?php
                                                        foreach ($subcat_id as $id) {
                                                            ?>
                                                            <div class="bi-title"><?=getSingleFieldDetail('id',$id,'subcat_name','sm_subcategories')?></div>
                                                            <?php
                                                            foreach ($cart['main_ingrediants'] as $ing){
                                                                $language= $this->session->userdata('language');
                                                                $ing_name=$ing['ing_name'];
                                                                if($language=='Arabic'){
                                                                    $ing_name=$ing['ing_name_arabic'];
                                                                }
                                                                if($ing['subcat_id'] == $id ){
                                                                    ?>
                                                                    <div class="d-flex">
                                                                        <div class="bi-sub">- <?= $ing_name ?></div>
                                                                        <div class="bi-qty"> <?=$ing['quantity']?> <?= $this->lang->line('qty') ?></div>
                                                                    </div>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>

                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <?php
                                                }
                                                ?> -->
                                                <?php
                                                if(count($cart['extra_ingrediants']) > 0){
                                                    $subcat_id=array_unique(array_column($cart['extra_ingrediants'],'subcat_id'));
                                                    ?>
                                                    <div class="bi-title"><?= $this->lang->line('add_on') ?></div>
                                                    <div class="bag-item-addons">
                                                        <?php
                                                        foreach ($subcat_id as $id) {
                                                            $subcat_name=getSingleFieldDetail('id',$id,'subcat_name','sm_subcategories');
                                                            $language= $this->session->userdata('language');
                                                            if($language=='Arabic'){
                                                                $subcat_name=getSingleFieldDetail('id',$id,'subcat_name_arabic','sm_subcategories');
                                                            }
                                                            ?>
                                                            <div class="bi-title"><?=$subcat_name?></div>
                                                            <?php
                                                            foreach ($cart['extra_ingrediants'] as $ing){
                                                                $language= $this->session->userdata('language');
                                                                $ing_name=$ing['ing_name'];
                                                                if($language=='Arabic'){
                                                                    $ing_name=$ing['ing_name_arabic'];
                                                                }
                                                                if($ing['subcat_id'] == $id ){
                                                                    ?>
                                                                    <div class="d-flex">
                                                                        <div class="bi-sub">- <?= $ing_name ?></div>
                                                                        <div class="bi-qty"> <?=$ing['quantity']?> </div>
                                                                    </div>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>

                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                            </div>

                                            <?php
                                        }elseif ($cart['salad_type'] ==2){
                                            ?>
                                            <div class="bag-item">
                                                <div class="bag-item-group">
                                                    <div class="bi-img"><img
                                                                src="<?= INCLUDE_SALAD_IMAGE_THUMB_PATH . $cart['cust_salad_image'] ?>"/></div>
                                                    <div class="bi-title"><?=$cart['cust_salad_name']?></div>
                                                     <div class="bi-qty" style="display: flex"><?=$cart['quantity']?> <?= $this->lang->line('qty'); ?></div> 
                                                    <div class="bi-price"><span class="salad_price"><?=round($cart['total_price'],'1')?><?= $this->lang->line('jd') ?> </span></div>
                                                </div>
                                                <?php
                                                if(count($cart['extra_ingrediants']) > 0){
                                                    $subcat_id=array_unique(array_column($cart['extra_ingrediants'],'subcat_id'));
                                                    ?>
                                                    <div class="bag-item-addons">
                                                        <?php
                                                        foreach ($subcat_id as $id) {
                                                            $language= $this->session->userdata('language');
                                                            $sub_cat_name=getSingleFieldDetail('id',$id,'subcat_name','sm_subcategories');
                                                            if($language=='Arabic'){
                                                                $sub_cat_name= getSingleFieldDetail('id',$id,'subcat_name_arabic','sm_subcategories');
                                                            }
                                                            ?>
                                                            <div class="bi-title"><?= $sub_cat_name ?></div>
                                                             <input type="hidden" name="subcat_name" value="<?= $sub_cat_name ?>">
                                                            <?php
                                                            foreach ($cart['extra_ingrediants'] as $ing){
                                                                $language= $this->session->userdata('language');
                                                                $ing_name=$ing['ing_name'];
                                                                if($language=='Arabic'){
                                                                    $ing_name=$ing['ing_name_arabic'];
                                                                }
                                                                if($ing['subcat_id'] == $id ){
                                                                    ?>
                                                                    <div class="d-flex">
                                                                        <div class="bi-sub">- <?= $ing_name ?></div>
                                                                       <div class="bi-qty"> <?=$ing['quantity']?> </div> 
                                                                    </div>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <?php
                                        }
                                    }
                                }

                                ?>

                            </div>

                            <hr>
                            <form action="javascript:void(0);" id="confirm_order_form" method="post" >
                                <div id="delivery_type" delivery_type="<?=$delivery_type?>" class="op-footer">
                                    <div><span class="bag-price-title"><?=$this->lang->line('subtotal');?></span>
                                        <span class="bag-price"><span id="sub_total"></span><?= $this->lang->line("jd")?></span></div>
                                        <?php $taxes = getSingleFieldDetail('id',1,'tax_rate','sm_app_settings'); ?>
                                    <div><span class="bag-price-title"><?=$this->lang->line('taxes');?> (<span id="tax_percentage"><?=round($taxes,'1')?></span>%)</span>
                                        <span class="bag-price"><span id="tax_charges"></span> </span></span></div>
                                    <?php
                                    if($delivery_type ==='delivery'){
                                        $delivery_charges= getSingleFieldDetail('id',1,'delivery_charge','sm_app_settings');?>
                                        <div id="delivery_module"><span class="bag-price-title"><?= $this->lang->line("delivery_charge")?></span>
                                            <span class="bag-price"><span id="delivery_charges"><?=round($delivery_charges,'1')?><?= $this->lang->line("jd")?> </span></span></div>
                                        <?php
                                    }
                                    ?>
                                    <div class="total-border" id="paymentMethod"><span class="bag-price-title "><?=$this->lang->line('total');?></span>
                                        <span class="bag-price" style="font-size: 20px"><span id="total_price"><?=$this->lang->line('jd');?></span></span></div>
                                    <br>
                                    <?php   $cod=''; $online='';
                                    if($this->session->userdata('payment_type')){
                                        $paymentType=$this->session->userdata('payment_type');
                                           if($paymentType== "COD") {
                                                $cod='selected="selected"';
                                            } else if($paymentType== "online") {
                                                $online='selected="selected"';
                                            }
                                        }?>
                                    <select name="payment_type" id="payment_type" class="form-control mb-12">
                                        <option value=""><?=$this->lang->line('payment_type');?> </option>
                                        <option <?= $cod ?> value="COD"><?=$this->lang->line('cod');?> </option>
                                        <option <?= $online ?> value="online"><?=$this->lang->line('online');?></option>
                                    </select>

                                    <button class="btn btn-checkout" style="margin-top: 20px"  type="submit"><?=$this->lang->line('confirm');?> <i class="fa fa-angle-right"></i></button>
                                </div>
                            </form>
                        </div>
                    <?php } else {
                        ?>
                        <div class="order-preview">
                            <div class="op-section-head">
                                <h4 class="bag-title"><?=$this->lang->line('order');?></h4>

                            </div>
                            <div class="op-section">
                                <div class="row">
                                    <h4 class=" text-center op-section-title"><?=$this->lang->line('no_item_cart');?></h4>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>




            </div>
        </div>
    </div>
</section>

<!--Address Modal-->
<!--<div id="deliveryaddressModal" class="modal fade" role="dialog">-->
<!--    <div class="modal-dialog">-->
<!---->
<!--        Modal content-->
<!--        <div class="modal-content">-->
<!--            <div class="modal-header">-->
<!--                <button type="button" class="close" data-dismiss="modal">&times;</button>-->
<!--                <h4 class="modal-title" ><span style="font-weight:600">Add Address</span>                </h4>-->
<!--            </div>-->
<!--            <div class="modal-body">-->
<!--                <form action="javascript:void(0);" id="confirm_address_from" method="post">-->
<!--                    <div  class="address-padding address-detail">-->
<!--                        <label id="address_id-error" class="error text-center" for="address_id"></label>-->
<!--                        <div id="all_addresses">-->
<!---->
<!--                        </div>-->
<!---->
<!--                        <div>-->
<!--                            <button type="submit"  class="btn btn-primary">Confirm</button>-->
<!---->
<!--                            <button type="button" class="btn btn-primary show-address-form">Add New Address</button>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </form>-->
<!---->
<!---->
<!--                <div class="address-form address-padding" style="margin-bottom: 40px">-->
<!--                    <form id="address_update" class="mu-contact-form text-center" method="post" >-->
<!--                        <h4 class="op-section-title" style="    margin-bottom: 20px;">Add Address</h4>-->
<!---->
<!--                        <h4 class="alert alert-info" role="alert">Delivery is only in JORDAN </h4>-->
<!--                        <div class="mu-contact-left">-->
<!--                            <div class="form-group">-->
<!--                                <input type="text" value="Amman" readonly class="form-control" name="city" id="city"   placeholder="--><!--City">-->
<!--                            </div>-->
<!---->
<!--                            <div class="form-group">-->
<!--                                <input type="text" id="search_address" class="form-control" placeholder="--><!--">-->
<!--                            </div>-->
<!--                            <div class="row"  id="first_map_div" style="display: none">-->
<!--                                <div class="map" id="map_canvas" style="width: 700px; height: 500px;"></div>-->
<!--                                <div class="form_area">-->
<!--                                    <br>-->
<!--                                    <br><input type="text" readonly class="form-control" name="location" id="location">-->
<!--                                    <div>-->
<!---->
<!--                                        <div id ="enter_city_name_origin_div" style="display: none">-->
<!--                                            <br><input type="text" class="form-control" name="enter_city_name_first"-->
<!--                                                       id="enter_city_name_first"-->
<!--                                                       placeholder="Enter New City/Place Name"-->
<!--                                                       value=""  >-->
<!--                                            <span style="font-size: 10px; color: red;" id="enter_city_name_first-error"-->
<!--                                                  class="error"></span>-->
<!--                                        </div>-->
<!--                                        <div class="row">-->
<!--                                            <div class="col-md-6">-->
<!--                                                <input type="text" class="form-control" name="city_latitude_first" placeholder="City/Place Latitude" readonly id="city_latitude_first"/>-->
<!--                                            </div>-->
<!--                                            <div class="col-md-6">-->
<!--                                                <input type="text" class="form-control"  name="city_longitude_first" placeholder="City/Place Longitude" readonly id="city_longitude_first"/>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <span style="font-size: 10px; color: red;" id="city_longitude_first-error"-->
<!--                                              class="error"></span>-->
<!--                                        <br><span style="font-size: 10px; color: red;" id="marker_position_origin-error"-->
<!--                                                  class="error"></span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!---->
<!--                        </div>-->
<!---->
<!---->
<!--                        <div  style="margin-top: 20px;">-->
<!--                            <div style="float: right">-->
<!--                                <button type="button" class="btn btn-default hide-address-form" >Cancel</button>-->
<!--                                <button type="submit" class="btn btn-primary">Save</button>-->
<!--                            </div>-->
<!--                        </div>-->
<!---->
<!--                    </form>-->
<!--                </div>-->
<!---->
<!---->
<!---->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--    </div>-->
<!--</div>-->


<script src="<?=INCLUDE_FRONT_ASSETS?>custom/js/order_preview.js"></script>


<script type="text/javascript">
    //    ------------------------------------DESTINATION-----------------------------
    //on change of city drop down change
    $(document).on('keyup','#location', function () {
        $('#city_latitude_second').val('');
        $('#city_longitude_second').val('');
        $('#location_second').val('');

        var geocoder = new google.maps.Geocoder();
        var el = $('#location').val();
        if(el){
            geocoder.geocode({ 'address': el }, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    initialize_second(latitude,longitude);
                }
            });
            $('#first_map_div').css('display', '');
        }

    });

</script>

<script type="text/javascript">
    /* script for map second*/
    function initialize_second(lat,long) {
        var latlng_second = new google.maps.LatLng(lat,long);
        var map_second = new google.maps.Map(document.getElementById("map_canvas"), {
            center: latlng_second,
            zoom: 13
        });
        var marker_second = new google.maps.Marker({
            map: map_second,
            position: latlng_second,
            draggable: true,
            anchorPoint: new google.maps.Point(0, 0)
        });
        var input_second = document.getElementById('location');
        //        map_second.controls[google.maps.ControlPosition.TOP_LEFT].push(input_second);
        var geocoder_second = new google.maps.Geocoder();
        var autocomplete_second = new google.maps.places.Autocomplete(input_second);
        autocomplete_second.bindTo('bounds', map_second);
        var infowindow_second = new google.maps.InfoWindow();
        autocomplete_second.addListener('place_changed', function () {
            infowindow_second.close();
            marker_second.setVisible(false);
            var place_second = autocomplete_second.getPlace();
            if (!place_second.geometry) {
//                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place_second.geometry.viewport) {
                map_second.fitBounds(place_second.geometry.viewport);
            } else {
                map_second.setCenter(place_second.geometry.location);
                map_second.setZoom(17);
            }

            marker_second.setPosition(place_second.geometry.location);
            marker_second.setVisible(true);

            bindDataToFormSecond(place_second.formatted_address, place_second.geometry.location.lat(), place_second.geometry.location.lng());
            infowindow_second.setContent(place_second.formatted_address);
            infowindow_second.open(map_second, marker_second);

        });
        // this function will work on marker_second move event into map
        google.maps.event.addListener(marker_second, 'dragend', function () {
            geocoder_second.geocode({'latLng': marker_second.getPosition()}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        bindDataToFormSecond(results[0].formatted_address,marker_second.getPosition().lat(), marker_second.getPosition().lng());
                        infowindow.setContent(results[0].formatted_address);
                        infowindow_second.open(map_second, marker_second);
                    }
                }
            });
        });
        initialize_add_fence(map_second,'destination');

    }

    function bindDataToFormSecond( address,lati, lngi) {
        console.log(address);
        document.getElementById('location').value = address;
        document.getElementById('city_latitude_first').value = lati.toFixed(8);
        document.getElementById('city_longitude_first').value = lngi.toFixed(8);
    }

    //    google.maps.event.addDomListener(window, "load", initialize);
</script>


<script>
    var map;

    var infowindow;
    var markers = [];
    var goo;

    function initialize_add_fence(map_in,origin_destination) {
        if(origin_destination == 'origin'){
            var field_name = 'marker_position_origin';
        }else{
            var field_name = 'marker_position_destination';
        }

        goo = google.maps,
            shapes = [],
            selected_shape = null,
            drawman = new goo.drawing.DrawingManager({
                map: map_in,
                drawingControlOptions: {
                    position: goo.ControlPosition.TOP_CENTER,
//          drawingModes: [ 'circle', 'polygon', 'polyline' ]
                    drawingModes: [ 'circle' ]
                },
            }),

            byId = function ( s ) {return document.getElementById(s)},
            clearSelection = function () {

                if ( selected_shape ) {
                    selected_shape.set((selected_shape.type=== google.maps.drawing.OverlayType.MARKER ) ? 'draggable' : 'editable', false);
                    selected_shape = null;
                }
            },
            setSelection = function ( shape ) {
                clearSelection();
                clearShapes();
                selected_shape = shape;

                selected_shape.set((selected_shape.type
                    ===
                    google.maps.drawing.OverlayType.MARKER
                ) ? 'draggable' : 'editable', true);

            },
            clearShapes = function () {
                //e.preventDefault();
                for ( var i = 0; i < shapes.length; ++i ) {
                    shapes[ i ].setMap(null);
                }
                shapes = [];
                $('#'+field_name).val('');
            };
        goo.event.addListener(drawman, 'drawingmode_changed', function ( e ) {

            clearShapes();
            $('#'+field_name).val('');
        });


        goo.event.addListener(drawman, 'circlecomplete', function ( circle ) {

            var position = circle.getCenter();
            var rad = circle.getRadius();
            $('#'+field_name).val('CIRCLE+' + position + rad);
        });
        goo.event.addListener(drawman, 'polygoncomplete', function ( polygon ) {

            var position = polygon.getPath();
            $('#'+field_name).val('POLYGON+' + position.getArray());
        });

        goo.event.addListener(drawman, 'polylinecomplete', function ( polyline ) {

            var position = polyline.getPath();
            $('#'+field_name).val('LINESTRING+' + position.getArray());
        });

        goo.event.addListener(drawman, 'rectanglecomplete', function ( rectangle ) {
            var position = rectangle.getBounds();
            $('#'+field_name).val('rectangle => ' + position);
        });

        goo.event.addListener(drawman, 'overlaycomplete', function ( e ) {
            //alert('xxxxx');
            var shape = e.overlay;
            shape.type = e.type;

            //drawman.drawingManager.setDrawingMode(null);
            goo.event.addListener(shape, 'click', function () {

                clearShapes();
                setSelection(this);

            });
            setSelection(shape);
            shapes.push(shape);
        });

        goo.event.addListener(map_in, 'click', clearSelection);
        goo.event.addDomListener(byId('clear_shapes'), 'click', clearShapes);
    }

    //  google.maps.event.addDomListener(window, 'load', initialize);


</script>







