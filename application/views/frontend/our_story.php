<section style="margin-top: 68px; background: #607d8b; background-image: url(<?=INCLUDE_FRONT_ASSETS?>img/storybg.jpg); background-size: cover; background-repeat: no-repeat">
    <div class="container-fluid">
        <div class="row">
            <div class="video-iframe">
                <div class="content text-center">

                    <?php
                    if($this->session->userdata('language') ==='English'){
                        ?>

                        <h4>Our Story</h4>
                        <p >
                            Founded in 2019, SaladMania is a destination for healthy food. We believe the choices we make about what we eat, where it comes from, and how it’s made have effects on the health of individuals and our communities. That’s why we’re proud to partner with some local farmers to bring you the freshest, high-quality ingredients possible. We still have long way to achieve our future goals, to build a healthy supply community and to be Jordan’s number one icon for healthy food. We believe that we need to do things differently, putting our Guests as top one priority moving forward.
                        </p>
                        <p>
                            We don’t believe our guests should have to pay a premium on bettering their health. In a market where our competitors price novelty ingredients/dishes steeply, we keep our prices reasonable so that you get the same great quality at a better value.
                        </p>
                            <?php
                    }else if($this->session->userdata('language') ==='Arabic'){
                        ?>
                        <h4>قصتنا</h4>
                        <p> تأسست سالاد مانيا في عام 2019 كواجهة للطعام الصحي. نحن نؤمن بأن الخيارات التي نتخذها بشأن ما نتناوله من طعام ، ومن أين أتى ، وكيف تم تحضيره ، لها تأثير على صحة الأفراد ومجتمعاتنا. لهذا، نحن فخورون بالشراكة مع بعض المزارعين المحليين لنقدم لكم المكونات الطازجة والعالية الجودة يوميا وتحضريها بشكل صحي و" شهي" بما يرضي شرائح كبيرة من الافراد. لا يزال أمامنا طريق طويل لتحقيق أهدافنا المستقبلية، وهي تغيير النظرة للطعام الصحي بحيث يكون متوفر لكافة الشرائح و أن نكون الرمز الأول للأغذية الصحية في الأردن. نحن نؤمن بأننا بحاجة إلى القيام بالأشياء بشكل مختلف، مع إعطاء ضيوفنا الأولوية القصوى للمضي قدمًا.
                        </p>
                        <p>
                        نحن لا نعتقد أن على ضيوفنا دفع سعر إضافي من اجل تحسين صحتهم. في السوق حيث يقوم منافسونا بتسعير المنتجات الصحية بشكل مفرط وحاد ، نحافظ على اسعار معقولة بحيث تحصل على نفس الجودة الرائعة بقيمة أفضل</p>
                    <?php
                    }
                    ?>
                    <br>
                    <a href="<?=getSingleFieldDetail('id',1,'our_story_link','sm_app_settings')?>">
                        <i class="fa fa-play-circle-o"></i>
                        WATCH THE VIDEO
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>