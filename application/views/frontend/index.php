<div class="mu-title" id="mu-title-mark">

</div>
<!-- Start slider  -->
<?php
    $sql = "select * from sm_banners order by banner_id";
    $banners = $this->Main_model->__callMasterquery($sql);
    if($banners->num_rows() > 0)
    {
?>

<a href="<?=base_url()?>menu">
<section id="mu-slider" style="display:block;     float: none;">
    <div class="mu-slider-area"   >
 
        <!-- Top slider -->
        <div class="mu-top-slider">
            <!-- Top slider single slide -->
 
            <?php foreach ($banners->result() as $brow) { ?>
            <div class="mu-top-slider-single" >
              
                     <img src="<?php echo base_url().'uploads/banners/'.$brow->banner_name ?>" alt="Salad Mania" class="banner-img">
              
            </div>
            <?php } ?>

            <!-- / Top slider single slide -->
        </div>

        <!--  <div style="text-align: center; padding-top:50px;z-index: 9999999999">
         <a class="order-menu" href="<?=base_url()?>menu" > Order Now </a>
        </div> -->
    </div>
</section>
 </a>




<?php } ?>
<!-- End slider  -->



<!-- Start Restaurant Menu -->
<section id="mu-restaurant-menu" class="log-more-padding" >
    <div class="container">
        <div class="row">
            <div class="" style='float: none; margin: auto'>
                <div class="mu-restaurant-menu-area">
                    <div class="mu-title">
                        <!--<span class="mu-subtitle">Our Menu</span>-->
                        <h2><?=$this->lang->line('menu');?></h2>
                        <a href="#" target="_blank" id="nutri-filter"><i class="fa fa-heartbeat"></i><?=$this->lang->line('filter_nutrition');?> </a>
                        <div class="menu-tags">
                            <?php
                            $first = true;
                            foreach ($categories as $cat){
                                 //check language in session
                                $language= $this->session->userdata('language');
                                $cat_name=$cat->cat_name;
                                if($language=='Arabic'){
                                    $cat_name=$cat->cat_name_arabic;
                                }
                                if ( $first ) {
                                    $first = false;
                                    ?>
                                    <a href="#" class="active" id="<?=$cat->cat_id?>"><?=$cat_name?></a>
                                    <?php
                                }else{
                                    ?>
                                    <a href="#" id="<?=$cat->cat_id?>"><?=$cat_name?></a>
                                <?php

                                }
                            }
                            ?>

                        </div>
                    </div>
                    <div id="salads" class="mu-restaurant-menu-content" >
                        
                    </div>
                    <div id="load_data_message" class="mu-restaurant-menu-content text-center"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<div style="text-align: center;">
<a href="<?=base_url()?>menu" class="load-more" id="1">Load More Items</a>
</div>


<!-- Start Subscription section -->
<!--<section id="mu-subscription" style="background:#091e2f; background-image: url(assets/img/bluebg.jpg); background-size: auto 100%; background-repeat: no-repeat; padding: 120px 0">-->
<!--    <div class="container-fluid">-->
<!--        <div class="row">-->
<!--            <div class="col-md-5 ">-->
<!--                <img src="--><?//=INCLUDE_FRONT_ASSETS?><!--img/flying.png" style="height: 440px; margin-left: 25px; position: absolute" class="custom-img anim-updown"/>-->
<!--            </div>-->
<!--            <div class="col-md-6 remove-padding" >-->
<!--                <div class="mu-subscription-area" style="background-image: url(assets/img/wood.jpg); background-size: cover">-->
<!--                    <div class="mu-title">-->
<!--                        <h2 style='color:#fff'>Do you wish a customized salad?</h2>-->
<!---->
<!--                    </div>-->
<!--                    <form class="mu-subscription-form text-center">-->
<!--                        <a href='--><?//=base_url()?><!--make-your-own' class="mu-readmore-btn" >MAKE YOUR OWN</a>-->
<!--                    </form>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->

<!-- Custom js -->

<script src="<?=INCLUDE_FRONT_ASSETS?>custom/js/home.js"></script>
