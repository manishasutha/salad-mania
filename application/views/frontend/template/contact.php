<div class="mu-title" id="mu-title-mark">

</div>
<?php
$this->db->select('');
$this->db->from('sm_app_settings');
$this->db->where('id',1);
$query=$this->db->get();
$setting=$query->row();
?>

<!-- Start Contact section -->
<section id="mu-contact" style="background: #fff; background-image: url(assets/img/shopbag.png); background-repeat: no-repeat; background-position: left bottom; padding: 80px 0px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-contact-area">
                    <div class="mu-title" id="mu-title-mark">
                        <!--              <span class="mu-subtitle">Get In Touch</span>-->
                        <h2><?=$this->lang->line('contact');?></h2>
                    </div>
                    <div class="mu-contact-content">
                        <div class="row">

                            <div class="col-md-12" >
                                <div class="mu-contact-right">
                                    <div class="mu-contact-widget">
                                        <h3><?=$this->lang->line('office_add');?></h3>
                                        <address>
                                            <p><?=$this->lang->line('place');?></p>
                                            <p><i class="fa fa-phone"></i><?=$setting->contact_phone?></p>
                                            <p><i class="fa fa-envelope-o"></i><?=$setting->contact_email?></p>
                                            <p><i class="fa fa-map-marker"></i><?=$setting->address?> </p>
                                        </address>
                                    </div>
                                    <div class="mu-contact-widget">
                                        <h3><?=$this->lang->line('open_hr');?></h3>
                                        <address>
                                            <p><span><?=$this->lang->line('sun_thu');?></span> <?=$setting->monday_friday_hrs?></p>
                                            <p><span><?=$this->lang->line('fri');?></span><?=$setting->saturday_hrs?></p>
                                            <p><span><?=$this->lang->line('sat');?></span> <?=$setting->sunday_hrs?></p>
                                        </address>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a href="#contact-us1" class="contact-btn a" id="contact_us"><?=$this->lang->line('contact');?></a>
                                <a href="#contact-us1" class="contact-btn b" id="dietician"><?=$this->lang->line('dietician_title');?></a>
                            </div>
                            <div class="col-md-12 text-center">
                                <span class="alert alert-success" id="success" style="display:none"></span>
                                <span class="alert alert-danger" id="error" style="display:none"></span>
                            </div>

                            <div class="clearfix"></div>
                            <div id="contact-us1" >
                                <div id="contact-us" >
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6" style="padding: 0;  margin: 20px auto; border-top:1px solid #eee; padding-top: 20px">
                                        <h4 style="font-weight: 600; text-align: center; font-size: 21px"><?=$this->lang->line('contact');?></h4>
                                        <div class="mu-contact-left">

                                            <form class="mu-contact-form text-center" id="contact_us_form" action="javascript:void(0);" method="post">
                                                <div class="form-group" style="margin-bottom: 20px">
                                                    <!--<label for="name">Your Name</label>-->
                                                    <input type="text" class="form-control mb-12" name="contact_name" id="contact_name" placeholder="<?=$this->lang->line('name');?>">
                                                </div>
                                                <div class="form-group" style="margin-bottom: 20px">
                                                    <!--<label for="email">Email address</label>-->
                                                    <input type="text" class="form-control mb-12" name="contact_email" id="contact_email" placeholder="<?=$this->lang->line('email');?>">
                                                </div>
                                                <div class="form-group" style="margin-bottom: 20px">
                                                    <!--<label for="subject">Subject</label>-->
                                                    <input type="text" class="form-control mb-12" name="contact_subject" id="contact_subject" placeholder="<?=$this->lang->line('subject');?>">
                                                </div>
                                                <div class="form-group" style="margin-bottom: 20px">
                                                    <!--<label for="message">Message</label>-->
                                                    <textarea class="form-control mb-12" id="contact_message" name="contact_message" cols="30" rows="3" placeholder="<?=$this->lang->line('type_msg');?>"></textarea>
                                                </div>
                                                <button type="submit" class="mu-send-btn"><?=$this->lang->line('send_msg');?></button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>

                                <div id="contact-dietician">
                                    <div class="col-md-6" style="padding: 0; float: none; margin: 20px auto; border-top:1px solid #eee; padding-top: 20px">
                                        <h4 style="font-weight: 600; text-align: center; font-size: 21px"><?=$this->lang->line('dietician_title');?></h4>
                                        <div class="mu-contact-left">
                                            
                                            <form class="mu-contact-form text-center" id="dietician_form" action="javascript:void(0);" method="post">
                                                <div class="form-group " style="margin-bottom: 20px">
                                                    <!--<label for="name">Your Name</label>-->
                                                    <input type="text" class="form-control mb-12" id="dietician_name" name="dietician_name" placeholder="<?=$this->lang->line('name');?>">
                                                </div>
                                                <div class="form-group"  style="margin-bottom: 20px">
                                                    <!--<label for="email">Email address</label>-->
                                                    <input type="email" class="form-control  mb-12" id="dietician_email" name="dietician_email" placeholder="<?=$this->lang->line('email');?>">
                                                </div>
                                                <div class="form-group" style="margin-bottom: 20px">
                                                    <!--<label for="subject">Subject</label>-->
                                                    <input type="text" class="form-control  mb-12" id="dietician_subject" name="dietician_subject" placeholder="<?=$this->lang->line('subject');?>">
                                                </div>
                                                <div class="form-group" style="margin-bottom: 20px">
                                                    <!--<label for="message">Message</label>-->
                                                    <textarea class="form-control  mb-12" id="dietician_message" name="dietician_message" cols="30" rows="3" placeholder="<?=$this->lang->line('type_msg');?>"></textarea>
                                                </div>
                                                <button type="submit" class="mu-send-btn"><?=$this->lang->line('send_msg');?></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>




                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){
        $('#contact_us').on('click',function(){
            $('#contact-us1').css({'height':'520'});
            $('#contact-dietician').hide();
            $('#contact-us').slideDown('show');
        });
        $('#dietician').on('click',function(){
            $('#contact-us1').css({'height':'520'});
            $('#contact-us').hide();
            $('#contact-dietician').slideDown('show');
        });
    });
</script>
<!-- End Contact section -->