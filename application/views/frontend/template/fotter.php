
<!-- Start Footer -->
<footer id="mu-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-1"><a href="<?=base_url()?>" class="footer-logo"><img src="<?=INCLUDE_FRONT_ASSETS?>img/logo.png"/></a></div>
            <div class="col-md-5">
                <ul class="footer-inline-nav">
                    <!--<li><a href="<?=base_url()?>about-us"><?=$this->lang->line('about_us');?></a></li>-->
                    <li><a href="<?=base_url()?>contact-us"><?=$this->lang->line('contact');?></a></li>
                    <li><a href="<?=base_url()?>terms"><?=$this->lang->line('terms');?></a></li>
                    <li><a href="<?=base_url()?>policy"><?=$this->lang->line('policy');?></a></li>
                </ul>
                <div class="mu-footer-area">
                    <div class="mu-footer-social">
                        <a href="<?=getSingleFieldDetail('id',1,'fb_url','sm_app_settings')?>"><span class="fa fa-facebook"></span></a>
                        <a href="<?=getSingleFieldDetail('id',1,'twitter_url','sm_app_settings')?>"><span class="fa fa-twitter"></span></a>
                        <a href="<?=getSingleFieldDetail('id',1,'gplus_url','sm_app_settings')?>"><span class="fa fa-google-plus"></span></a>
                        <a href="<?=getSingleFieldDetail('id',1,'linkedin_url','sm_app_settings')?>"><span class="fa fa-linkedin"></span></a>
                        <a href="<?=getSingleFieldDetail('id',1,'insta_url','sm_app_settings')?>"><i class="fa fa-instagram"></i></a>
                    </div>

                </div>
            </div>
            <div class="col-md-3 ">
                <div class="footer-app">
                    <p><?=$this->lang->line('download');?></p>
                    <a href="#"><img src="<?=INCLUDE_FRONT_ASSETS?>img/appstore.png"/></a>
                    <a href="#"><img src="<?=INCLUDE_FRONT_ASSETS?>img/playstore.png"/></a>
                </div>
            </div>
            <div class="col-md-3 ">
                <div class="footer-app">
                    <p><?=$this->lang->line('pay');?></p>
                    <a href="#"><img src="<?=INCLUDE_FRONT_ASSETS?>img/paysafe2.png"/></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->


<div class="sticky-footer">
    <div class="col-md-3 col-xs-3">
        <ul class="left">
            <li><a href="<?=getSingleFieldDetail('id',1,'fb_url','sm_app_settings')?>"><i class="fa fa-facebook"></i></a></li>
            <li><a href="<?=getSingleFieldDetail('id',1,'twitter_url','sm_app_settings')?>"><i class="fa fa-twitter"></i></a></li>
            <li><a href="<?=getSingleFieldDetail('id',1,'insta_url','sm_app_settings')?>"><i class="fa fa-instagram"></i></a></li>
        </ul>
    </div>
    <div class="col-md-9 col-xs-9">
        <ul class="right">
            <!--<li><a href="<?=base_url()?>about-us"><?=$this->lang->line('about_us');?></a></li>-->
            <li><a href="<?=base_url()?>contact-us"><?=$this->lang->line('contact');?></a></li>
            <li><a href="<?=base_url()?>terms"><?=$this->lang->line('terms');?></a></li>
            <li><a href="<?=base_url()?>policy"><?=$this->lang->line('policy');?></a></li>
        </ul>
    </div>
</div>
<!-- Call Modal -->



<!-- Login Modal -->
<div  class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModall">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4  style="float: left" class="modal-title" id="loginModall"><span style="font-weight:600"><?=$this->lang->line('hello_user');?>!</span></h4>
                <h6 class="text-center" ><span style="color: #00aa00;" id="success_msg"></span></h6>
                <h6 class="text-center" ><span style="color: red;" id="error_msg"></span></h6>

            </div>
            <div class="modal-body">
                <div class="row" style="margin: 0">


                    <div class="tab" style="min-height: 478px; ">
                        <button class="tablinks active" onclick="openCity(event, 'tlogin')" id="defaultOpen2"><?=$this->lang->line('login');?></button>
                        <button class="tablinks" onclick="openCity(event, 'tregister')"><?=$this->lang->line('register');?></button>

                        <button class="tablinks" onclick="openCity(event, 'gregister')" ><?=$this->lang->line('guest_login');?></button>
                    </div>
                        

                    <div id="tlogin" class="tabcontent"  style="display: block;">
                        <div class="col-md-10" style="padding: 0; float: none; margin: 20px auto;  padding-top: 0px">
<!--                            <p style="color: #91cc34; font-weight: bold; font-size: 24px; text-align: center ">Login</p>-->
                            <div class="mu-contact-left">
                                <p style="font-weight:600; font-size: 20px; margin-bottom: 20px"><?=$this->lang->line('login');?></p>
                                <form class="mu-contact-form text-center" id="login_form" action="javascript:void(0);" method="post">

                                    <div id="mlogin" style="margin-top: 50px;">
<!--                                        <p style="font-weight:600; font-size: 20px; margin-bottom: 20px">Login</p>-->
                                        <div class="form-group">
                                            <!--<label for="email">Email address</label>-->
                                            <input type="email" class="form-control" name="log_email" id="log_email" placeholder="<?=$this->lang->line('email');?>">
                                        </div>
                                        <div class="form-group">
                                            <!--<label for="subject">Subject</label>-->
                                            <input type="password" class="form-control" name="log_pswd" id="log_pswd" placeholder="<?=$this->lang->line('password');?>">
                                        </div>
                                        <br>

                                        <a style="cursor: pointer; text-align: left" onclick="forget()"><?=$this->lang->line('forgot_pass');?> </a>
                                        <br><br><br><br><br> <br><br> <br><br> <br>

                                        <div style=" position: absolute;width: 100%;  bottom: 0px; padding: padding: 20px 0px 0px 0px">
                                            <div style="float: right">

                                                <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('cancel');?></button>
                                                <button type="submit" name="log_sub" id="log_sub"  class="btn btn-primary"><?=$this->lang->line('login');?></button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                                <div id="tforget" style="display: none">
                                    <div class="col-md-10" style="padding: 0; float: none; margin: 20px auto;  padding-top: 0px">
                                        <div class="mu-contact-left">
                                            <form class="mu-contact-form text-center" id="forgot_form" action="javascript:void(0);" method="post">
                                                <div class="form-group">
                                                    <!--<label for="email">Email address</label>-->
                                                    <input type="email" class="form-control" name="forgot_email" id="forgot_email" placeholder="<?=$this->lang->line('email');?>">
                                                </div>
                                                <br>

                                                <div style=" position: absolute;width: 100%;  bottom: 0px; padding: padding: 20px 0px 0px 0px">
                                                    <div style="float: left">

                                                        <button type="submit" name="fog_sub" id="fog_sub" class="btn btn-primary"><?=$this->lang->line('submit');?></button>
                                                        <button type="button" class="btn btn-default" onclick="login()" ><?=$this->lang->line('login');?></button>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                                <script>
                                    function forget() {
                                        document.getElementById("mlogin").style.display = "none";
                                        document.getElementById("tforget").style.display = "block";

                                    }
                                    
                                    function login() {
                                        document.getElementById("mlogin").style.display = "block";
                                        document.getElementById("tforget").style.display = "none";
                                    }
                                </script>
                            </div>
                            <br>
                        </div>
                    </div>


                    <!--Regsiter Form Start-->
                    <div id="tregister" class="tabcontent"  style="display: none;">
                        <div class="col-md-10" style="padding: 0; float: none; margin: 20px auto;  padding-top: 0px">
                            <div class="mu-contact-left">
                                <p style="font-weight:600; font-size: 20px; margin-bottom: 20px"><?=$this->lang->line('register');?></p>
                                <form id="register_form" class="mu-contact-form text-center" action="javascript:void(0)" method="post">
                                    <div class="row">
                                        <div class="col-md-6 mb-12">
                                            <div class="form-group">
                                                <!--<label for="email">Email address</label>-->
                                                <input type="text" class="form-control" name="name" id="name" placeholder="<?=$this->lang->line('full_name');?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-12">
                                            <div class="form-group">
                                                <!--<label for="subject">Subject</label>-->
                                                <input type="text" class="form-control" id="build_no" name="build_no" placeholder="<?=$this->lang->line('building_no');?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-12">
                                            <div class="form-group">
                                                <!--<label for="subject">Subject</label>-->
                                                <input type="text" class="form-control" id="street_no" name="street_no"  placeholder="<?=$this->lang->line('street_name');?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-12">
                                            <div class="form-group">
                                                <!--<label for="subject">Subject</label>-->
                                                <input type="text" class="form-control" id="city" name="city"  value="Amman" ReadOnly placeholder="<?=$this->lang->line('city');?>">
                                                <!-- <input type="text" value="city" class="form-control" id="city"   name="city" readonly> -->
                                            </div>
                                        </div>
                                        <div class="col-md-6 ">
                                            <div class="form-group">
                                                <div class="row" >
                                                    <div class="col-md-4 padding-mobile" >
                                                        <input type="text" id="mobile_code" style="padding: 6px 10px;" class="form-control" value="+962" readonly>
                                                    </div>

                                                    <div class="col-md-8">
                                                        <input type="text" maxlength="9" class="form-control" id="mobile" name="mobile" placeholder="<?=$this->lang->line('mobile');?>">
                                                    </div>
                                                </div>
                                                <!--<label for="subject">Subject</label>-->


                                            </div>
                                        </div>

                                        <div class="col-md-6 mb-12">
                                            <div class="form-group">
                                                <!--<label for="subject">Subject</label>-->
                                                <input type="text" class="form-control" id="reg_email" name="reg_email" placeholder="<?=$this->lang->line('email');?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-12">
                                            <div class="form-group">
                                                <!--<label for="subject">Subject</label>-->
                                                <input type="password" class="form-control" id="pswd" name="pswd" placeholder="<?=$this->lang->line('password');?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-12">
                                            <div class="form-group">
                                                <!--<label for="subject">Subject</label>-->
                                                <input type="password" class="form-control" id="conf_pswd" name="conf_pswd" placeholder="<?=$this->lang->line('confirm_pass');?>">
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row margin-bottom-400">
                                        <div class='col-md-12' style='color:#888'>
                                             <?=$this->lang->line('agree_terms_condition');?> <a  target="_blank" href="<?=base_url()?>terms"> <?=$this->lang->line('terms_condition');?></a>
                                        </div>
                                    </div>
                                    <br><br><br>

                                    <div  style=" position: absolute;width: 100%;  bottom: 0px; padding: padding: 20px 0px 0px 0px">
                                        <div style="float: right">
                                            <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('cancel');?></button>
                                            <button name="create_user" id="create_user"  type="submit" class="btn btn-primary"><?=$this->lang->line('create_account');?></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!--Guest Form Start-->
                    <div id="gregister" class="tabcontent" style="display: none;">

                        <div class="col-md-10" style="padding: 0; float: none; margin: 20px auto;  padding-top: 0px">
                            <div class="mu-contact-left" style="margin-top: 30px">
                                <p style="font-weight:600; font-size: 20px; margin-bottom: 20px"><?=$this->lang->line('guest_login');?></p>

                                <form id="guest_register_form" class="mu-contact-form text-center" action="javascript:void(0)" method="post">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <!--<label for="email">Email address</label>-->
                                                <input type="text" class="form-control" name="guest_name" id="guest_name" placeholder="<?=$this->lang->line('full_name');?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <!--<label for="subject">Subject</label>-->
                                                <input type="text" class="form-control" id="guest_email" name="guest_email" placeholder="<?=$this->lang->line('email');?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <!--<label for="subject">Subject</label>-->
                                                <div class="row" >
                                                    <div class="col-md-4" >
                                                         <input type="text" id="mobile_code" style="padding: 6px 10px;" class="form-control" placeholder="+962" value="+962" readonly>
                                                    </div>

                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" maxlength="9" id="guest_mobile" name="guest_mobile" placeholder="<?=$this->lang->line('mobile');?>">
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class='col-md-12' style='color:#888'>
                                               <?=$this->lang->line('agree_terms_condition');?> <a  target="_blank" href="<?=base_url()?>terms"> <?=$this->lang->line('terms_condition');?></a> </div>
                                    </div>
                                    <br><br><br><br><br><br> <br><br> <br>

                                    <div style=" position: absolute;width: 100%;  bottom: 0px; padding: 20px 0px 0px 0px">
                                        <div style="float: right">
                                            <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('cancel');?></button>
                                            <button type="submit" class="btn btn-primary"><?=$this->lang->line('guest_login');?></button>
                                        </div>

                                    </div>
                                </form>
                            </div>


                        </div>
                    </div>
                </div>
<!--                <div class="modal-footer">-->
<!--                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>-->
<!--                    <button type="button" onclick="openCity(event, 'gregister')" class="btn btn-primary">Continue as Guest</button>-->
<!--                </div>-->
            </div>
        </div>

    </div>
</div>


<script>
    if ($(window).width() > 768) {

        $(window).bind('scroll', function() {

            var target_on = $('#mu-title-mark').offset().top;
            if ($(window).scrollTop() > target_on) {

                $('.sticky-footer').hide();
            }
            else if ($(window).scrollTop() < target_on) {
                $('.sticky-footer').show();
            }
        });
    }
</script>
<script>


//    $('.add').click(function () {
//        if ($(this).prev().val() < 9) {
//            $(this).prev().val(+$(this).prev().val() + 1);
//
//        }
//    });
//    $('.sub').click(function () {
//        if ($(this).next().val() > 1) {
//            if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
//
//        }
//    });



</script>


<?php 
    //Chat box only on Home and Menu Page
    $currentPage = $this->uri->segment(1);
    if($currentPage == '' || $currentPage == 'menu')
    {
?>
<!-- Start of Async Drift Code -->
<script>
"use strict";

!function() {
  var t = window.driftt = window.drift = window.driftt || [];
  if (!t.init) {
    if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
    t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ],
    t.factory = function(e) {
      return function() {
        var n = Array.prototype.slice.call(arguments);
        return n.unshift(e), t.push(n), t;
      };
    }, t.methods.forEach(function(e) {
      t[e] = t.factory(e);
    }), t.load = function(t) {
      var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
      o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
      var i = document.getElementsByTagName("script")[0];
      i.parentNode.insertBefore(o, i);
    };
  }
}();
drift.SNIPPET_VERSION = '0.3.1';
drift.load('yctftyg8ithx');
</script>
<!-- End of Async Drift Code -->
<?php } ?>


