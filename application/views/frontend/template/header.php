
<!--<a href="index.html#mu-restaurant-menu" class="quick-salads"><img src="assets/img/quick-salads.png"/></a>-->
<!--<a href="makeyourown.html" class="quick-make"><img src="assets/img/quick-make.png"/></a>-->

<!--Pre Loader -->
<div id="aa-preloader-area">
   <div class="mu-preloader">
       <!--<img src="assets/img/preloader.gif" alt=" loader img">-->
    </div>
</div>

<!--START SCROLL TOP BUTTON -->
<a class="scrollToTop" id="top" href="#">
    <i class="fa fa-angle-up" style=" font-size: 38px;"></i>
    <span></span>
</a>
<!-- END SCROLL TOP BUTTON -->

<!-- Start header section -->
<header id="mu-header" class="sticky">
    <nav class="navbar navbar-default mu-main-navbar" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- LOGO -->

                <a class="navbar-brand" href="<?=base_url()?>"><img src="<?=INCLUDE_FRONT_ASSETS?>img/logo_h.png" alt="Logo img"></a>

                <div class="nav-menu">
                    <div class="dropdown user-login" style="margin-top: 5px; margin-right: 10px">
                        <button class="btn btn-green dropdown-toggle" type="button" data-toggle="dropdown">
                            <i style="font-size: 18px" class="fa fa-user" aria-hidden="true"></i> 
                            <!--Display Username in case of login-->
                        </button> 
                        <ul class="dropdown-menu dropdown-main" style=" border: none; margin-top: 8px;">

                            <li><a class="dropdown-space" href="#" id="login_signup" data-toggle="modal" data-target="#loginModal">Login/SignUp</a></li>
                            
                        </ul>
                    </div>

                    <div class="dropdown user-login" style="margin-top: 5px; margin-right: 10px">
                            <button class="btn btn-green dropdown-toggle" type="button" data-toggle="dropdown">
                                <i style="font-size: 18px" class="fa fa-globe" aria-hidden="true"></i>
                                </button>
                            <ul class="dropdown-menu dropdown-main" style=" border: none; margin-top: 8px; ">
                                <li><a class="dropdown-space change_language english" id="english" href="#">English</a></li>
                                <li><a class="dropdown-space change_language  arabic" id="arabic" href="#">عربي</a></li>
                            </ul>
                    </div>
<?php
                $cart_count=0;
                if($this->session->userdata('user_login_session')){
                    $login=$this->session->userdata('user_login_session');
                    $where=array(
                        'user_id'=>$login['id'],
                    );

                    $this->db->select('cart_id');
                    $this->db->from('sm_carts');
                    $this->db->where($where);
                    $query=$this->db->get();
                    $cart=$query->result();
                    if($cart){
                        $cart_count=count($cart);
                    }

                }else if($this->session->userdata('cart_data')){
                    $cart_count=count($this->session->userdata('cart_data'));
                }
                ?>

                    <div style="margin-top: 8px; margin-right: 10px">
                         <a title="Order Now" class="ordernow ordernow-show margin-bottom-30-768" <?php if($cart_count>0){ ?> style="background-color: #92cc34;color: #fff;border: 1px solid #a3c22b;" <?php } ?>href="<?=base_url()?>make-your-own"> <i class='fa fa-shopping-bag'></i> <span><?=$cart_count?></span></a>
                    </div>

                </div>

        
            </div>

            <div id="navbar" class="navbar-collapse collapse">
                <ul id="top-menu" class="nav navbar-nav navbar-right mu-main-nav">

                    <!--<li><a class="scroller" href="<?=base_url()?>about-us"><?=$this->lang->line('about_us');?></a></li>-->
                    <li><a class="scroller" href="<?=base_url()?>menu"><?=$this->lang->line('menu');?></a></li>
                    <li><a class="scroller" href="<?=base_url()?>our-story"><?=$this->lang->line('story');?></a></li>
                    <li ><a class="scroller" href="<?=base_url()?>contact-us"><?=$this->lang->line('contact');?></a></li>
<!--                    <li><a class="scroller"  href="#">OFFERS</a></li>-->
                    <li>

                        <div class="dropdown user-login hide-768" style="margin-top: 18px; margin-right: 10px">
                            <button class="btn btn-green dropdown-toggle" type="button" data-toggle="dropdown">
                                <i style="font-size: 18px" class="fa fa-user" aria-hidden="true"></i> 
                                <!--Display Username in case of login-->
                                <?php if($this->session->userdata('user_login_session')) 
                                {
                                    $loginUser = $this->session->userdata('user_login_session');
                                    $sql = "select full_name from sm_users where id = ".$loginUser['id'];
                                    $query = $this->Main_model->__callMasterquery($sql);
                                    if($query->num_rows() > 0){
                                        $urow = $query->row();
                                        echo ucwords(strtolower($urow->full_name));
                                    }
                                } ?>
                                </button> 
                            <ul class="dropdown-menu dropdown-main" style=" border: none; margin-top: 8px;">

                                <?php if(!$this->session->userdata('user_login_session')){?>
                                    <li><a class="dropdown-space"  href="#" id="login_signup" data-toggle="modal" data-target="#loginModal"><?=$this->lang->line('login_sign');?></a></li>
                                <?php } else { ?>
                                    <li><a class="dropdown-space"  href="<?=base_url()?>myaccount"><?=$this->lang->line('my_account');?></a></li>
                                    <li><a  class="dropdown-space"  href="<?=base_url()?>logout" ><?=$this->lang->line('logout');?></a></li>
                                <?php } ?>

                            </ul>
                        </div>
                    </li>

                    <li>
                        <div class="dropdown user-login hide-768" style="margin-top: 18px; margin-right: 10px">
                            <button class="btn btn-green dropdown-toggle margin-bottom-30-768" type="button" data-toggle="dropdown">
                                <i style="font-size: 18px" class="fa fa-globe" aria-hidden="true"></i>
                                </button>
                            <ul class="dropdown-menu dropdown-main" style=" border: none; margin-top: 8px; ">
                                <li><a class="dropdown-space change_language english" id="english" href="#"><?=$this->lang->line('english');?></a></li>
                                <li><a class="dropdown-space change_language margin-bottom-30-768 arabic" id="arabic" href="#"><?=$this->lang->line('arabic');?></a></li>
                            </ul>
                        </div>
                    </li>

                </ul>
                <?php
                $cart_count=0;
                if($this->session->userdata('user_login_session')){
                    $login=$this->session->userdata('user_login_session');
                    $where=array(
                        'user_id'=>$login['id'],
                    );

                    $this->db->select('cart_id');
                    $this->db->from('sm_carts');
                    $this->db->where($where);
                    $query=$this->db->get();
                    $cart=$query->result();
                    if($cart){
                        $cart_count=count($cart);
                    }

                }else if($this->session->userdata('cart_data')){
                    $cart_count=count($this->session->userdata('cart_data'));
                }
                ?>

            
                <a title="Order Now" class="ordernow ordernow-show hide-768 margin-bottom-30-768" <?php if($cart_count>0){ ?> style="background-color: #92cc34;color: #fff;border: 1px solid #a3c22b;" <?php } ?>href="<?=base_url()?>make-your-own"> <i class='fa fa-shopping-bag'></i> <span><?=$cart_count?></span></a>
        </div><!--/.nav-collapse -->
    </nav>
</header>

<script>
$(".change_language").click(function()
{
    var base_url = '<?=base_url()?>';
    var lang = $(this).attr('id');
    //alert(lang);exit;
    if(lang != '')
    {
        $.ajax
        ({
            'type': 'GET',
            'url': base_url + 'change_language/'+lang,
            'success': function(response){
                //  alert('lang');
                 window.location.reload();
            }
        });
    }
});

/*$("#english").click(function()
{
   var base_url = '<?=base_url()?>';
  var lang= $('.english').text();
        $.ajax
            ({
                'type': 'GET',
                'url': base_url + 'change_language/'+lang,
                'success': function(response){
                    //  alert('lang');
                     window.location.reload();
                }
            });
});
$("#arabic").click(function()
{
    var alang= $('.arabic').text();
        $.ajax
        ({
            'type': 'GET',
            'url': base_url + 'change_language/'+alang,
            'success': function(response){
                //  alert('lang');
              window.location.reload();
            }
        });
});*/
</script>
<!-- End header section -->

