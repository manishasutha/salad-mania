<!-- calendar -->


<link rel="stylesheet" href="<?=INCLUDE_FRONT_ASSETS?>Date-Time-Picker-Bootstrap-4/build/css/bootstrap-datetimepicker.min.css">
<div class="mu-title" id="mu-title-mark">

</div>
<?php
//Call the official timings from Database
$this->db->select('monday_friday_hrs,saturday_hrs,sunday_hrs');
$this->db->from('sm_app_settings');
$this->db->where('id',1);
$query=$this->db->get();
$row = $query->result_array();
//echo "<pre>"; print_r($row);

$currentDay = strtolower(date('l', strtotime('Y-m-d')));
if($currentDay == 'friday')
    $officialTime = $row[0]['saturday_hrs'];
else if($currentDay == 'saturday')
    $officialTime = $row[0]['sunday_hrs'];
else
    $officialTime = $row[0]['monday_friday_hrs'];

$openHr = ''; $closeHr = '';
if(isset($officialTime)){
    $officialTime = str_replace(' ', '', $officialTime); //Remove spacename
    $timeSlot = explode('to', $officialTime);

    $openHr =  (int)str_replace(' ', '', $timeSlot[0]);
    $closeHr = (int)str_replace(' ', '', $timeSlot[1]);

    $mrng = strtolower(substr($timeSlot[0], -2));
    $noon = strtolower(substr($timeSlot[1], -2));

    if($mrng == 'pm'){
        if($openHr != 12)
            $openHr = $openHr+12;
    }

    if($noon == 'pm'){
        if($closeHr != 12)
            $closeHr = $closeHr+12;
    }

    $orderTimeSlots = array();
    for($i=$openHr; $i <= $closeHr; $i++){
        $orderTimeSlots[] = $i;
    }
    //$orderTimeSlots = implode(', ', $orderTimeSlots);
}
?>
<?php //} ?>
<!-- Start Restaurant Menu -->
<section id="mu-restaurant-menu" class=" menu-img"  >
    <div class="container">
        <div class="row" style="    margin-top: 0px;">
            <div class="bag-footer2">
                <button class="btn btn-checkout btn-bag" > <i class="fa fa-shopping-bag"></i> <?=$this->lang->line('bag');?> </button>
            </div>
            <div class="col-md-8" >
                <div class="mu-restaurant-menu-area menu-item" id="menu-res" >
                    <div class="mu-title " style=" margin-top: 20px;" >
                        <!--<span class="mu-subtitle">Our Menu</span>-->
                        <h2><?=$this->lang->line('menu');?></h2>
                        <a href="#" target="_blank" id="nutri-filter"><i class="fa fa-heartbeat"></i><?=$this->lang->line('filter_nutrition');?> </a>
                        <div class="menu-tags">
                            <?php
                            $first = true;
                            foreach ($categories as $cat){
                                //check language in session
                                $language= $this->session->userdata('language');
                                $cat_name=$cat->cat_name;
                                if($language=='Arabic'){
                                    $cat_name=$cat->cat_name_arabic;
                                }
                                if ( $first ) {
                                    $first = false;
                                    ?>
                                    <a href="#" class="active" id="<?= $cat->cat_id?>"><?=$cat_name?></a>
                                    <?php
                                }else{
                                    ?>
                                    <a href="#" id="<?= $cat->cat_id ?>"><?= $cat_name ?></a>
                                    <?php

                                }
                            }
                            ?>
                        </div>
                    </div>
                    <br />
                    <div id="load_data" class="mu-restaurant-menu-content " ></div>
                    <!--                    <div id="load_data_message" class="mu-restaurant-menu-content"></div>-->
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </div>
            </div>
            <div class="col-md-4">

                <form id="pickup_detail_form" method="post" ">
                <div class="bag">
                    <div class="bag-heading">
                        <h4 class="bag-title"><?= $this->lang->line('your_order') ?></h4>
                        <!--<input type="text" class="bag-title-input">-->
                        <!--<p>300 Cal</p>-->
                        <button type="button" class="btn btn-close-modal"> X </button>
                        <!--<button class="btn btn-change-title-done">Save</button>-->
                    </div>
                    <div class="bag-delivery bag-delivery1">
                        <div class="chosen-wrapper">
                            <label class="chosen hide-time"  >
                                <img src="<?=INCLUDE_FRONT_ASSETS?>img/delivery_icon.png" height="38"/> <?= $this->lang->line('delivery') ?>
                                <input class="checked" id="firstcheck"  type="radio" value="delivery" name="delivery" checked>
                                <span class="chosemark"></span>
                            </label>
                            <label class="chosen show-time">
                                <img src="<?=INCLUDE_FRONT_ASSETS?>img/pickup_icon.png" height="38"/><?= $this->lang->line('pick_up') ?>
                                <input class="checked" id="secondcheck"  onclick="showtime()" type="radio" value="pickup" name="delivery">
                                <span class="chosemark"></span>
                            </label>

                        </div>

                        <div class="time-wrapper time-view">
                            <input type="text" class="form-control valid" name="date_time" id="datetimepicker" style=" border: none;box-shadow:none;" aria-required="true" aria-invalid="false">
                            <!--                            <select class="form-control date" name="date">-->
                            <!--                                <option value="17 May">17 May</option>-->
                            <!--                                <option value="18 May">18 May</option>-->
                            <!--                            </select>-->
                            <!--                            <select class="form-control time" name="time">-->
                            <!--                                <option value="10:00 AM">10:00 AM</option>-->
                            <!--                                <option value="10:15 AM" >10:15 AM</option>-->
                            <!--                            </select>-->
                        </div>

                        <div class="time-wrapper time-view" style="display: flex;">
                            <label class="chosen1">
                                <div class="time-wrapper" style="    margin-bottom: 5px;">
                                    <input type="text"  class="form-control" <?php if($user_id){?> value="<?=getSingleFieldDetail('id',$user_id,'full_name','sm_users')?>" <?php } ?> placeholder="Name" id="name" name="name"  style=" border: none;box-shadow:none;font-weight: normal;">
                                </div>
                                <label id="name-error" class="error" for="name"></label>

                            </label>
                            <label class="chosen1">
                                <div class="time-wrapper" style="    margin-bottom: 5px;">
                                <!-- <input type="text"  class="form-control" maxlength="9" placeholder=" Mobile" id="mobile" <?php if($user_id){?> value="<?=getSingleFieldDetail('id',$user_id,'phone','sm_users')?>" <?php } ?> name="mobile" style=" border: none;box-shadow:none;font-weight: normal;width:60%;">
                                 -->
                                <label id="" class="" style="padding: 8px 10px 4px 20px; font-size: 14px; font-weight: normal;" for="mobile">+962</label>
                                    <input type="text"  class="form-control" maxlength="9" placeholder=" Mobile" id="mobile" <?php if($user_id){?> value="<?=getSingleFieldDetail('id',$user_id,'phone','sm_users')?>" <?php } ?> name="mobile" style=" border: none;box-shadow:none;font-weight: normal;width:60%;    padding: 6px 4px;">
                               </div>  
                                <label id="mobile-error" class="error" for="mobile"></label>

                            </label>
                        </div>
                        <div style="padding: 10px 10px 0px">
                            <a class="btn btn-checkout2 view-detail" id="done"> <?= $this->lang->line('done') ?></a>
                        </div>
                    </div>

                    <div class="bag-delivery show-deatil" id="delivery" style="display: none">
                        <img src="<?=INCLUDE_FRONT_ASSETS?>img/delivery_icon.png" height="38"/>
                        <h5 class="order-type"><?= $this->lang->line('delivery') ?></h5>
                        <div style="padding: 0px 10px">
                            <a class="btn btn-checkout2 view-delivery"><?= $this->lang->line('edit_detail') ?></a>
                        </div>
                    </div>

                    <div class="bag-delivery show-deatil" id="Pickup">
                        <img src="<?=INCLUDE_FRONT_ASSETS?>img/pickup_icon.png" height="38"/>
                        <h5 class="order-type"><?= $this->lang->line('pick_up') ?></h5>
                        <p class="order-date">On <span id="order-date"></span> </p>
                        <p class="order-time" id="pickup-name"> </p>
                        <p class="order-time" id="pickup-mobile"></p>
                        <span class="text text-danger" display="block" id="mobile-error"></span>
                        <span class="text text-danger" display="block" id="name-error"></span>
                        <div style="padding: 0px 10px">
                            <a class="btn btn-checkout2 view-delivery"   ><?= $this->lang->line('edit_detail') ?></a>
                        </div>
                    </div>

                    <div class="bag-body">
                        <?php
                        if($carts){
                            foreach ($carts as $cart){
                                if($cart['salad_type'] ==1) {
                                    if($this->session->userdata('user_login_session')){
                                        $cart_id=$cart['cart_id'];
                                    }else {
                                        $cart_id = $cart['key'];
                                    }
                                    $salad_name= $cart['salad_name'];
                                    $language= $this->session->userdata('language');
                                    if($language=='Arabic'){
                                        $salad_name= $cart['salad_name_arabic'];
                                    }
                                    ?>
                                    <div class="bag-item">
                                        <div class="bag-item-group">
                                            <div class="bi-img"><img
                                                        src="<?=INCLUDE_SALAD_IMAGE_THUMB_PATH .$cart['salad_img']?>" /></div>
                                            <div class="bi-title"><?=$salad_name?></div>
                                            <div class="bi-qty" style="display: flex">
                                                <button type="button" id="sub" class="sub">-</button>
                                                <input type="text" readonly style="height: 22px" onkeypress="return isNumber(event)" cart_id="<?=$cart_id?>"  id="1" value="<?=$cart['quantity']?>" min="1" max="9"/>
                                                <button type="button" id="add" class="add">+</button>
                                            </div>
                                            <div class="bi-price"><span class="salad_price"><?=round($cart['total_price'],'1')?><?= $this->lang->line('jd') ?></span></div>
                                            <div class="bi-remove" onclick="DeleteCartValues(<?=$cart_id?>)"><i class="fa fa-times"></i></div>

                                        </div>

                                        <!-- <?php
                                        if(count($cart['main_ingrediants']) > 0){
                                            $subcat_id=array_unique(array_column($cart['main_ingrediants'],'subcat_id'));
                                            ?>
                                            <div class="bi-title"><?= $this->lang->line('main') ?></div>
                                            <div class="bag-item-addons">
                                                <?php
                                            foreach ($subcat_id as $id) {
                                                ?>
                                                    <div class="bi-title"><?=getSingleFieldDetail('id',$id,'subcat_name','sm_subcategories')?></div>
                                                    <?php
                                                foreach ($cart['main_ingrediants'] as $ing){
                                                    $ing_name= $ing['ing_name'];
                                                    $language= $this->session->userdata('language');
                                                    if($language=='Arabic'){
                                                        $ing_name= $ing['ing_name_arabic'];
                                                    }
                                                    if($ing['subcat_id'] == $id ){
                                                        ?>
                                                            <div class="d-flex">
                                                                <div class="bi-sub">- <?=$ing_name?></div>
                                                                <div class="bi-qty"> <?=$ing['quantity']?></div>
                                                            </div>
                                                            <?php
                                                    }
                                                }
                                                ?>

                                                    <?php
                                            }
                                            ?>
                                            </div>
                                            <?php
                                        }
                                        ?> -->
                                        <?php
                                        if(count($cart['extra_ingrediants']) > 0){
                                            $subcat_id=array_unique(array_column($cart['extra_ingrediants'],'subcat_id'));
                                            ?>
                                            <div class="bi-title"><?= $this->lang->line('add_on') ?></div>
                                            <div class="bag-item-addons">
                                                <?php
                                                foreach ($subcat_id as $id) {
                                                    $subcat_name=getSingleFieldDetail('id',$id,'subcat_name','sm_subcategories');
                                                    $language= $this->session->userdata('language');
                                                    if($language=='Arabic'){
                                                        $subcat_name=getSingleFieldDetail('id',$id,'subcat_name_arabic','sm_subcategories');
                                                    }
                                                    ?>
                                                    <div class="bi-title"><?=$subcat_name?></div>
                                                    <?php
                                                    foreach ($cart['extra_ingrediants'] as $ing){
                                                        $ing_name= $ing['ing_name'];
                                                        $language= $this->session->userdata('language');
                                                        if($language=='Arabic'){
                                                            $ing_name= $ing['ing_name_arabic'];
                                                        }
                                                        if($ing['subcat_id'] == $id ){
                                                            ?>
                                                            <div class="d-flex">
                                                                <div class="bi-sub">- <?= $ing_name?></div>
                                                                <div class="bi-qty"> <?=$ing['quantity']?> </div>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                    ?>

                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <?php
                                        }
                                        ?>

                                    </div>

                                    <?php
                                }elseif ($cart['salad_type'] ==2){
                                    if($this->session->userdata('user_login_session')){
                                        $cart_id=$cart['cart_id'];
                                    }else {
                                        $cart_id = $cart['key']; //$key;
                                    }
                                    ?>
                                    <div class="bag-item">
                                        <div class="bag-item-group">
                                            <div class="bi-img"><img
                                                        src="<?= INCLUDE_SALAD_IMAGE_THUMB_PATH . $cart['cust_salad_image'] ?>"/></div>
                                            <div class="bi-title"><?=$cart['cust_salad_name']?></div>
                                            <div class="bi-qty" style="display: flex">
                                                <button type="button" id="sub" class="sub">-</button>
                                                <input type="number" id="1" cart_id="<?=$cart_id?>" value="<?=$cart['quantity']?>" min="1" max="9"/>
                                                <button type="button" id="add" class="add">+</button>
                                            </div>
                                            <div class="bi-price"><span class="salad_price"><?=round($cart['total_price'],'1')?><?=$this->lang->line('jd') ?></span></div>
                                            <div class="bi-remove" onclick="DeleteCartValues(<?=$cart_id?>)" ><i class="fa fa-times"></i></div>
                                        </div>
                                        <?php
                                        if(count($cart['extra_ingrediants']) > 0){
                                            $subcat_id=array_unique(array_column($cart['extra_ingrediants'],'subcat_id'));
                                            ?>
                                            <div class="bag-item-addons">
                                                <?php
                                                foreach ($subcat_id as $id) {
                                                    $subcat_name=getSingleFieldDetail('id',$id,'subcat_name','sm_subcategories');
                                                    $language= $this->session->userdata('language');
                                                    if($language=='Arabic'){
                                                        $subcat_name=getSingleFieldDetail('id',$id,'subcat_name_arabic','sm_subcategories');
                                                    }
                                                    ?>
                                                    <div class="bi-title"><?=$subcat_name?></div>
                                                    <?php
                                                    foreach ($cart['extra_ingrediants'] as $ing){
                                                        $ing_name= $ing['ing_name'];
                                                        $language= $this->session->userdata('language');
                                                        if($language=='Arabic'){
                                                            $ing_name= $ing['ing_name_arabic'];
                                                        }
                                                        if($ing['subcat_id'] == $id ){
                                                            ?>
                                                            <div class="d-flex">
                                                                <div class="bi-sub">- <?=$ing_name?></div>
                                                                <div class="bi-qty"> <?=$ing['quantity']?> </div>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                            <div class="bag-footer">
                                <div><span class="bag-price-title"><?= $this->lang->line('total') ?></span>
                                    <span class="bag-price"><span id="sub_total"><?= $this->lang->line('jd') ?></span></span></div>
                                <?php $taxrate=getSingleFieldDetail('id',1,'tax_rate','sm_app_settings');?>
                                <div><span class="bag-price-title"><?=$this->lang->line('taxes');?>(<span id="tax_percentage"><?=round($taxrate,'1')?></span>%)</span>
                                    <span class="bag-price"><span id="tax_charges"><?= $this->lang->line('jd') ?></span></span></span></div>
                                <div id="delivery_module"><span class="bag-price-title"><?= $this->lang->line('delivery_charges') ?></span>
                                    <?php $delivery_charges= getSingleFieldDetail('id',1,'delivery_charge','sm_app_settings');?>
                                    <span class="bag-price"><span id="delivery_charges"><?=round($delivery_charges,'1')?><?= $this->lang->line('jd') ?> </span> </span></div>
                                <div class="total-border"><span class="bag-price-title "><?= $this->lang->line('grand_total') ?></span>
                                    <span class="bag-price" style="font-size: 20px"><span id="total_price"><?= $this->lang->line('jd') ?></span> </span></div>

                                <br>
                                <!--<small>Extra charges may apply</small>-->
                            </div>
                            <?php
                        }else{
                            ?>
                            <h4 class="text-center"><?= $this->lang->line('no_cart_items') ?></h4>
                            <?php
                        }

                        ?>
                    </div>


                    <div style="padding: 0px 10px 10px">
                        <button <?php if(!$carts){echo "disabled"; }?> id="checkout" class="btn btn-checkout"> <?= $this->lang->line('checkout') ?> <i class="fa fa-angle-right"></i></button>
                    </div>



                </div>
                </form>



            </div>
        </div>
    </div>
</section>

<!-- End Restaurant Menu -->
<script>
    $(document).ready(function(){

        $("button#checkout").click(function () {
            var pickup= $("input[name='delivery']:checked").val();
           if(pickup== 'pickup') {
               var name = $("input#name").val();
               var phone = $("input#mobile").val();
               var name_filter = /^[a-zA-Z\s]+$/;
               var phone_filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;

               if (name == '' || phone == '') {
                   $("span#name-error").text('Name and phone no is required');
                   return false;
               } else if (name_filter.test(name) && phone_filter.test(phone)) {
                   if (name.length < 2 || name.length > 40) {
                       $("span#name-error").text("Name must be between 2 to 40 characters");
                       return false;
                   }
                   if (phone.length < 9 || phone.length > 9) {
                       $("span#name-error").text("PhoneNo must be of 9 digits");
                       return false;
                   }
               } else {
                   $("span#name-error").text("Field must be in valid format");
                   return false;
               }
           }
        });
        $(".view-detail").click(function () {
            var pickup= $("input[name='delivery']:checked").val();
            if(pickup== 'pickup') {
                var name = $("input#name").val();
                var phone = $("input#mobile").val();
                var name_filter = /^[a-zA-Z\s]+$/;
                var phone_filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;

                if (name == '' || phone == '') {
                    $("span#name-error").text('Name and phone no is required');
                    return false;
                } else if (name_filter.test(name) && phone_filter.test(phone)) {
                    if (name.length < 2 || name.length > 40) {
                        $("span#name-error").text("Name must be between  2 to 40 characters");
                        return false;
                    }else if (phone.length <9  || phone.length > 9) {
                        $("span#name-error").text("PhoneNo must be of 9 digits");
                        return false;
                    }else{
                        $("span#name-error").text('');
                        return true;
                    }
                } else {
                    if (name_filter.test(name) && phone_filter.test(phone)) {
                        $("span#name-error").text('');
                        return true;
                    }
                    else {
                        $("span#name-error").text("Field must be in valid format");
                        return false;
                    }

                }
            }
        });


            $(".time-view").hide();

        $(".view-detail").click(function(){
            var radioValue = $("input[name='delivery']:checked").val();

            if(radioValue === 'delivery'){
                $("#delivery").show();
                $("#Pickup").hide();
                $(".hide-delivery").hide();
                $(".view-detail").hide();
                $(".bag-delivery1").hide();
            }
            else
            {
                $("#Pickup").show();
                $("#delivery").hide();
                $(".hide-delivery").hide();
                $(".view-detail").hide();
                $(".bag-delivery1").hide();
            }
            var $name = document.getElementById("name").value;
            var $mobile = document.getElementById("mobile").value;

            document.getElementById("pickup-name").innerHTML = $name;
            document.getElementById("pickup-mobile").innerHTML = '+962 '+$mobile;

            dateVariable = $("#datetimepicker").val();
            document.getElementById("order-date").innerHTML = dateVariable;
        });

        $("select.date").change(function(){
            var selectedCountry = $(this).children("option:selected").val();
//                                alert("You have selected the country - " + selectedCountry);
            $("#order-date").text(selectedCountry);
        });

        $("select.time").change(function(){
            var selectedCountry2 = $(this).children("option:selected").val();
//                                alert("You have selected the country - " + selectedCountry2);
            $("#order-time").text(selectedCountry2);
        });

        $(".show-time").click(function(){
            $(".time-view").show();
            $('#delivery_module').hide();
            $('.bag-body').css("max-height", "calc(100vh - 470px)");
            var sub_total=0;
            $( ".salad_price" ).each(function( i ) {
                var price=parseFloat($(this).html());
                sub_total=sub_total+price;
            });
            $('#sub_total').html(sub_total);
            var tax_percentage=parseFloat($('#tax_percentage').html());
            var tax_charges=tax_percentage*sub_total/100;
            $('#tax_charges').html(tax_charges.toFixed(2));
            var delivery_charges=parseFloat($('#delivery_charges').html());
            var total_price=sub_total+tax_charges;
            $('#total_price').html(total_price.toFixed(2));
        });

        $(".hide-time").click(function(){
            $(".time-view").hide();
            $('#delivery_module').show();
            $('.bag-body').css("max-height", "calc(100vh - 370px)");

            var sub_total=0;
            $( ".salad_price" ).each(function( i ) {
                var price=parseFloat($(this).html());
                sub_total=sub_total+price;
            });
            $('#sub_total').html(sub_total);
            var tax_percentage=parseFloat($('#tax_percentage').html());
            var tax_charges=tax_percentage*sub_total/100;
            $('#tax_charges').html(tax_charges.toFixed(2));
            var delivery_charges=parseFloat($('#delivery_charges').html());
            var total_price=sub_total+tax_charges+delivery_charges;
            $('#total_price').html(total_price.toFixed(2));

        });

        $('.view-delivery').on('click',function(){
            $('.bag-delivery1').show();
            $('.show-deatil').hide();
            $('.view-detail').show();
        });


    });
</script>

<script>

    function sidemenu() {
//        alert("hello");
        document.getElementById("sidemenu-scroll").style.height="160px";
    }

    function sidemenu2() {
//        alert("hello");
        document.getElementById("sidemenu-scroll").style.height="230px";
    }


    $(document).ready(function(){
        $(".btn-bag").click(function(){
            $(".bag").show();
            $(".menu-item").css("margin-top", "550px");
            $(".btn-close-modal").show();
            $(".btn-bag").hide();
        });
    });
</script>

<script>


    $('.view-detail').on('click',function(){
        $('.hide-delivery').hide();
        $('.show-deatil').show();
        $('.view-delivery').show();
    });

    $('.btn-close-modal').on('click',function () {
        $('.bag').hide();
        $(".menu-item").css("margin-top", "0px");
        $(".btn-bag").show();
    });
    jQuery(function($) {
        function fixDiv() {
            var $cache = $('.bag');
            if ($(window).scrollTop() > 120)
                $cache.css({
                    'position': 'fixed',
                    'width': '380px',
                    'top': '20px'
                });

            else
                $cache.css({
                    'position': 'relative',
                    'top': 'auto',
                    'width': '380px',
                    'right':'auto'
                });
        }
        $(window).scroll(fixDiv);
        fixDiv();
    });
</script>
<script>

    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
    document.getElementById("defaultOpen2").click();

    $('.add').click(function () {
        if ($(this).prev().val() < 9) {
            $(this).prev().val(+$(this).prev().val() + 1);

        }
    });
    $('.sub').click(function () {
        if ($(this).next().val() > 1) {
            if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);

        }
    });
</script>

<script src="<?=INCLUDE_FRONT_ASSETS?>custom/js/make_your_own.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="<?=INCLUDE_FRONT_ASSETS?>Date-Time-Picker-Bootstrap-4/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    $(function () {
        /*var sun_th = $("#sun_th").val();
         var friday = $("#friday").val();
         var saturday = $("#saturday").val();
         var sunTh = sun_th.split('to');
         var sunThStart = sunTh[1].split(' ');
         var sunThEnd = sunTh[0].split(' ');
         */

        var orderTimeSlots = new Array();
        <?php foreach($orderTimeSlots as $key => $val){ ?>
        orderTimeSlots.push('<?php echo $val; ?>');
        <?php } ?>

        $('#datetimepicker').datetimepicker({
            //minDate:new Date()
            format : 'MM/DD/YYYY HH:mm',
            //Format: 'd MM, y',
            minDate: moment().add(1, 'h'),enabledHours: orderTimeSlots, //[17,18,19]
            maxDate: moment().add(2, 'days')
        });
    });
    window.onscroll = function() {myFunction()};
    function myFunction() {
        $("#mu-footer").hide();
        $("#mu-header").removeClass("sticky");

    }

</script>