<?php

/*
 * Trying to create a pay page
 */

require_once 'template/paytabs.php';

 $pt = new Payment();
 $pt-> paytabs("rupali@coretechies.com", "CbX4j8BMuxcKyUYZ49FR1jPa4euUuYr7aobtxNf2ksMTmLTqUzhVS65XlARPzjXJH8deVsggU3ahO4HIDZDw6JtrklhUEeEmmRBi");

 $name=explode(' ',$user->full_name);
$firstname=$name[0];
$lastname='sm';
if(count($name)>1){
  $lastname=$name[1];
}



//$result = $pt->authentication();

$result = $pt->create_pay_page(array(
    //Customer's Personal Information
    'merchant_email' => "rupali@coretechies.com",
    'secret_key' => "CbX4j8BMuxcKyUYZ49FR1jPa4euUuYr7aobtxNf2ksMTmLTqUzhVS65XlARPzjXJH8deVsggU3ahO4HIDZDw6JtrklhUEeEmmRBi",
      'cc_first_name' =>  $firstname,          //This will be prefilled as Credit Card First Name
      'cc_last_name' => $lastname,            //This will be prefilled as Credit Card Last Name
      'cc_phone_number' => "+962",
      'phone_number' => $user->phone,
      'email' => $user->email,


    //Customer's Billing Address (All fields are mandatory)
    //When the country is selected as USA or CANADA, the state field should contain a String of 2 characters containing the ISO state code otherwise the payments may be rejected.
    //For other countries, the state can be a string of up to 32 characters.
    'billing_address' =>  $user->address,
    'city' => "Amman",
    'state' => "Amman",
    'postal_code' => "11118",
    'country' => "JOR",

    //Customer's Shipping Address (All fields are mandatory)
    'address_shipping' => $user->address,
    'city_shipping' => "Amman",
    'state_shipping' => "Amman",
    'postal_code_shipping' => "11118",
    'country_shipping' => "JOR",

    //Product Information
    "products_per_title" => "Salad",   //Product title of the product. If multiple products then add “||” separator
    'quantity' => "1",                                    //Quantity of products. If multiple products then add “||” separator
    'unit_price' =>  $order_detail->grad_total,                                  //Unit price of the product. If multiple products then add “||” separator.
    "other_charges" => "0",                                     //Additional charges. e.g.: shipping charges, taxes, VAT, etc.
    'amount' => $order_detail->grad_total,                                          //Amount of the products and other charges, it should be equal to: amount = (sum of all products’ (unit_price * quantity)) + other_charges
    'discount'=>"0",                                                //Discount of the transaction. The Total amount of the invoice will be= amount - discount
    'currency' => "JD",                                            //Currency of the amount stated. 3 character ISO currency code



    //Invoice Information
    'title' => $user->full_name,          // Customer's Name on the invoice
    "msg_lang" => "en",                 //Language of the PayPage to be created. Invalid or blank entries will default to English.(Englsh/Arabic)
    "reference_no" => "1231231",        //Invoice reference number in your system


    //Website Information
    "site_url" => "http://localhost/salad-mania/make-your-own",      //The requesting website be exactly the same as the website/URL associated with your PayTabs Merchant Account
    'return_url' => "http://localhost/salad-mania/payment-success/",
    "cms_with_version" => "API USING PHP",
    "paypage_info" => "1"
));

//echo "FOLLOWING IS THE RESPONSE: <br />";
//print_r ($result);
echo '<script type="text/javascript">
            window.location = "'.$result->payment_url.'"
       </script>';
$_SESSION['paytabs_api_key'] = $result->secret_key;