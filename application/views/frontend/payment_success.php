  <?php  $login=$this->session->userdata('user_login_session');
            ?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Order Placed</title>

    <style>
        .media-add
        {
            border: 1px solid #ccc;
            font-size: 15px;
            color: #fff;
            letter-spacing: 1px;
            font-weight: 700;
            background: #92cc34;
            padding: 10px 100px;
            text-decoration:none;
        }
    </style>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body style=" background-color: #fff;      height: 100%;  font-family: 'Montserrat', sans-serif;">
<div style="height: 100%; background-color: #fff; padding: 150px 200px; ">
    <div style="background-color: #fff;text-align: center;  position: relative; box-shadow: 3px 3px 15px rgba(0,0,0,0.1);">
        <div style="    position: absolute; text-align: center;    width: 100%;top: -68px;    font-size: 120px; color: #92cc34;">
            <i style="background-color: white;" class="fas fa-check-circle"></i>
        </div>
        <div style=" margin-top: 50px; padding: 50px">

            <h1 style="color: #28343d; font-size: 48px;     text-transform: uppercase;    margin-top: 30px;margin-bottom: 30px;">Thank You!</h1>
            <h1 style="color: #999; font-size: 26px; font-weight: 500;  margin-bottom: 60px;">Your order has been successfully placed. You will get notification when the item will be shipped.</h1>
            
          <?php if(empty($login['id'])){ ?>
                <a href="<?=base_url()?>" class="media-add"><?=$this->lang->line('home');?></a>
            <?php } else if (!empty($login['id'])){ ?>
                <a class="media-add"  href="<?=base_url()?>myaccount"><?=$this->lang->line('my_account');?></a>
            <?php } ?>
           

        </div>

    </div>

</div>
</body>
</html>