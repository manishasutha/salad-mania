<div class="mu-title" id="mu-title-mark">

</div>
<!-- Start Restaurant Menu -->
<section id="mu-restaurant-menu" style="background: #fff; background-image: url(assets/img/cbg.jpg); background-size: cover; padding: 40px 0">
    <div class="container" id="mu-make-own">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-restaurant-menu-area">

                    <div class="mu-restaurant-menu-content">

                        <div class="mu-tab-content-area">
                            <div class="row">
                                <div class="col-md-8">

                                </div>

                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6" style="padding: 0; float: none; margin: 20px auto; border-top:1px solid #eee; padding-top: 20px">
                                <h4 style="font-weight: 600; text-align: center; font-size: 21px"><?=$this->lang->line('change_pass');?> </h4>
                                <div class="mu-contact-left">
                                    <form class="mu-contact-form text-center" action="javascript:void(0);" method="post" id="ch_pswd_form">
                                        <div class="form-group">
                                            <!--<label for="name">Your Name</label>-->
                                            <input type="password" class="form-control" id="pswd" name="pswd" placeholder="<?=$this->lang->line('password');?> ">
                                        </div>
                                        <div class="form-group">
                                            <!--<label for="email">Email address</label>-->
                                            <input type="password" class="form-control" id="conf_pswd" name="conf_pswd" placeholder="<?=$this->lang->line('confirm_pass');?>">
                                        </div>
                                        <button type="submit"  name="ch_pswd_form_submit" id="ch_pswd_form_submit"  class="mu-send-btn"><?=$this->lang->line('change_pass');?> </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- End Restaurant Menu -->