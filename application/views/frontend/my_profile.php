
        <!-- Start Contact section -->
        <section id="mu-contact" style="     min-height: 690px; background: #f5f5f5; background-repeat: no-repeat; background-position: left bottom">
            <div class="container">
                <div class="row" style="    margin-top: 80px;">
                    <div class="col-md-12">
                        <div class="mu-contact-area">
                            <div class="mu-title" id="mu-title-mark">
                                <!--              <span class="mu-subtitle">Get In Touch</span>-->
                                <!-- <h2>Terms And Conditions</h2> -->
                            </div>

                            <div class="mu-contact-content" style="margin-top: 0">
                                <div class="row">
                                    <div class="col-md-3" >
                                        <div class="loggedin-user">
                                            <div class="lu-image">
                                                <div class="picture-container">
                                                    <div class="picture">
                                                    </div>
                                                </div>
                                                
                                                <h6 id="fullname"><?php echo $userdata->full_name ?></h6>
                                            </div>
                                            <div class="lu-detail">
                                                <ul>
                                                    <li>
                                                        <small><?=$this->lang->line('email');?></small>
                                                        <p><?php echo $userdata->email ?></p>
                                                    </li>
                                                    <li>
                                                        <small><?=$this->lang->line('phone');?></small>
                                                        <p class="phone"><?php echo $userdata->phone ?></p>
                                                    </li>
                                                    <?php if(empty($address)){ ?>
                                                    <li>
                                                        <small><?=$this->lang->line('address');?></small>
                                                       <p class="add"> <?='No address exist'; ?><p>
                                                    </li>
                                                    <?php }else { ?>
                                                    <li>
                                                        <small><?=$this->lang->line('address');?></small>
                                                        <?php $city= getSingleFieldDetail('city_id',$address->city_id,'city','sm_cities');?>
                                                        <p class="add"><?=$address->addline1.', '.$address->addline2.', '.$city ?></p>
                                                    </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9" >
                                        <div class="custom-tabs">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?=$this->lang->line('my_profile');?> </a></li>
                                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><?=$this->lang->line('my_order');?></a></li>
                                                <!--<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">My Bag</a></li>-->
                                                <li role="presentation"><a href="#change" aria-controls="change" role="tab" data-toggle="tab"><?=$this->lang->line('change_pass');?></a></li>

                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                              <div role="tabpanel" class="tab-pane active" id="home">
                                                    <div class="profile-container" style="padding: 20px;">
                                                     <div><span style="color: #00aa00;margin-left:50px;" id="success_msg"></span></div>
                                                        <div><span style="color: red;margin-left:50px;" id="error_msg"></span></div>
                                              
                                                        <form  class="mu-contact-form" action="javascript:void(0);" method="post" id="user_form">
                                                            <br>
                                                            <div class="row">
                                                              
                                                                <div class="col-sm-10 col-sm-offset-1">  <h4 class="section-title"><?=$this->lang->line('my_profile_heading');?></h4></div>
                                                                
                                                                <div class="col-sm-5 col-sm-offset-1">
                                                                
                                                                    <div class="form-group">
                                                                        <label><?=$this->lang->line('full_name');?> :</label>
                                                                        <input name="name" id="name" type="text" value="<?=$userdata->full_name?>" class="form-control" placeholder="<?=$this->lang->line('full_name');?>">
                                                                    </div>
                                                                    <?php if($address==''){ ?>
                                                                        <div class="form-group">
                                                                            <label><?=$this->lang->line('building_no');?> :</label>
                                                                            <input name="build_no" id="build_no" type="text" value="" class="form-control build_no" placeholder="<?=$this->lang->line('building_no');?>">
                                                                        </div>
                                                                    <?php }else{ ?>
                                                                        <div class="form-group">
                                                                            <label><?=$this->lang->line('building_no');?> :</label>
                                                                            <input name="build_no" id="build_no" type="text" value="<?=$address->addline1 ?>" class="form-control build_no" placeholder="<?=$this->lang->line('building_no');?>">
                                                                        </div>
                                                                    <?php } ?>
                                                                </div>

                                                                <div class="col-sm-5 ">
                                                                    <div class="form-group">
                                                                        <label><?=$this->lang->line('phone');?> :</label>
                                                                        <div class="row">
                                                                            <div class="col-lg-4">
                                                                                <input  value="+962" class="form-control" readonly>
                                                                            </div>
                                                                            <div class="col-lg-8">
                                                                                <input name="phone" id="phone" type="text" maxlength="9" value="<?=$userdata->phone?>" class="form-control" placeholder="<?=$this->lang->line('phone');?>">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <?php if($address==''){ ?>
                                                                    <div class="form-group">
                                                                        <label><?=$this->lang->line('street_name');?>:</label>
                                                                        <input name="street_no" id="street_no" type="text" value="" class="form-control street_no" placeholder="<?=$this->lang->line('street_name');?>">
                                                                    </div>
                                                                    <?php } else { ?>
                                                                    <div class="form-group">
                                                                        <label><?=$this->lang->line('street_name');?>:</label>
                                                                        <input name="street_no" id="street_no" type="text" value="<?=$address->addline2?>" class="form-control street_no" placeholder="<?=$this->lang->line('street_name');?>">
                                                                    </div>
                                                                    <?php } ?>
                                                                </div>


                                                             <div class="col-sm-5 col-sm-offset-1">
                                                                 <?php if($address==''){ ?>
                                                                 <div class="form-group">
                                                                    <label><?=$this->lang->line('city');?> :</label>
                                                                    <input readonly  id="city" type="text"  value="" class="form-control city" placeholder="city">
                                                                </div>
                                                                 <?php } else { ?>
                                                                 <div class="form-group">
                                                                     <label><?=$this->lang->line('city');?> :</label>
                                                                     <input readonly  id="city" type="text"  value="<?=getSingleFieldDetail('city_id',$address->city_id,'city','sm_cities')?>" class="form-control city" placeholder="city">
                                                                 </div>
                                                                     <div class="form-group">
                                                                         <input name="add_id" id="add_id" type="hidden" value="<?=$address->add_id?>" class="form-control add_id" placeholder="<?=$this->lang->line('add_id');?>">
                                                                     </div>
                                                                 <?php } ?>
                                                            </div>


                                                                <div class="clearfix"></div>
                                                            <div class="col-sm-5 col-sm-offset-1">
                                                                <br>
                                                                   <!-- <button type="submit" name="log_sub" id="log_sub"  class="mu-send-btn">Login</button> -->
                                                                 <button type="submit" class='btn btn-finish btn-primary' name="submit"><?=$this->lang->line('update');?> </button>
                                                            </div>
                                                            </div>
                                                        </form>
                                                        <br>

                                                        <br>
                                                        <br>
                                                    </div>
                                                </div>

           
                                                <div role="tabpanel" class="tab-pane" id="profile">
                                                  
                                                     <div class="profile-container" style="padding: 20px">
                                                        <div class="row">
                                                              
                                                            <div class="col-sm-5">
                                                                <?php
                                                                //echo "<pre>";
                                                                //print_r($orders);

                                                                ?>

                                                                <div class="form-group">
                                                                    <label><?=$this->lang->line('choose_order');?>:</label>
                                                                    <select class="form-control" name="orderid" id="orderid">
                                                                     <option value=""><?=$this->lang->line('choose_id');?></option>
                                                                        <?php if(!empty($orders)){
                                                                            foreach ($orders as $row) {
                                                                          $id = $row->order_id;
                                                                          $orderId = "SM-".strtoupper(substr(md5($id), 0, 6).dechex($id));?>
                                                                        <option value="<?php echo $id ?>"><?php echo $orderId ?></option>
                                                                         <?php }} ?>
                                                                    </select>
                                                                </div>
                                                               
                                                            </div>  

                                                            <div class="clearfix"></div>

                                                            <!--Please wait message-->
                                                            <p style="display: none;" id="pleasewait" class="col-sm-12">Please wait...</p>

                                                            <div id="orderDetailBox">
                                                            </div>
                                                           
                                                            <!-- <div class="col-sm-12">
                                                                <div class="order-detail"> 
                                                                    <div class="order-media">
                                                                        <div class="row">
                                                                            <div class="col-lg-2 padding-0">
                                                                                <div class="o-img">
                                                                                    <img src="assets/img/salads/athena.jpg"/>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-lg-10 padding-0">
                                                                                <div class="o-info d-flex">
                                                                                    <div>
                                                                                        <h4>Athene Salad</h4>
                                                                                    </div>

                                                                                    <div>
                                                                                        <span class="price">  2 Qty | 30 Cal | <span class="price-green">JD 20.00</span></span>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="order-ing">
                                                                                    <p>salad, olive, onion, salad, olive, onion, salad, olive, onion,
                                                                                        salad, olive, onion, salad, olive, onion, salad, olive, onion, salad, olive, onion, salad, olive, onion,</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                
                                                            
                                                             <div class="order-addons">
                                                                        <h5>Extras</h5>
                                                                        <table>
                                                                            <tr>

                                                                            <tr>
                                                                                <td>Bluces</td>
                                                                                <td>x1</td>
                                                                                <td>JD 2.00</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Tomatoes</td>
                                                                                <td>x2</td>
                                                                                <td>JD 2.00</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Chillies</td>
                                                                                <td>x2</td>
                                                                                <td>JD 2.00</td>
                                                                            </tr>
                                                                            </table>
                                                            </div>
                                                           
</div>

                                                           
                                                            <div class="col-sm-12 " >
                                                            <div class="order-detail"> 
                                                                <div class="table-responsive order-charges">
                                                                    <table class="table table-striped">
                                                                        <tr>
                                                                            <td>Delivery Charges</td>
                                                                            <th>JD 5.00</th>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Taxes </td>
                                                                            <th>JD 1.00</th>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>TOTAL </td>
                                                                            <th>JD 56.00</th>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><small>Promo Code applied</small> </td>
                                                                            <th>- JD 6.00</th>
                                                                        </tr>
                                                                    </table>
                                                                    <table class="table table-striped">
                                                                        <tr>
                                                                            <td>Grand Total</td>
                                                                            <th><span class="green">JD 50.00</span></th>
                                                                        </tr>

                                                                    </table>
                                                                </div>
                                                            </div>
                                                            </div>
                                                        </div>
                                                            
                                                    </div> 
                                                    
                                                    
                                                </div> -->
                                              <!-- <div role="tabpanel" class="tab-pane" id="messages">
                                                    <div class="profile-container" style="padding: 20px">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <span class="badge badge-warning">2</span> Items in your bag
                                                        </div>
                                                    <div class="col-md-6">
                                                         <div class="order-detail">
                                                                    <div class="order-media">
                                                                        <img class="delete-item" src="assets/img/cross@1x.png"/>
                                                                        <div class="o-img">
                                                                            <img src="assets/img/salads/athena.jpg"/>
                                                                        </div>
                                                                        <div class="o-info">
                                                                            <h4>Athene Salad</h4>
                                                                            <p>30 Cal | JD 10.00</p>
                                                                            <p>Qty: <b>2</b></p>
                                                                            <span class="price">JD 20.00</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="order-media">
                                                                        <img  class="delete-item" src="assets/img/cross@1x.png"/>
                                                                        <div class="o-img">
                                                                            <img src="assets/img/salads/cobb.jpg"/>
                                                                        </div>
                                                                        <div class="o-info">
                                                                            <h4>My Salad</h4>
                                                                            <p>30 Cal | JD 10.00</p>
                                                                            <p>Qty: <b>3</b></p>
                                                                            <span class="price">JD 30.00</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                    </div>-->
                                                    </div>
                                                    </div>
                                                </div>

                                                <div role="tabpanel" class="tab-pane" id="change">
                                                    
                                                    <div class="profile-container" style="padding: 20px;">
                                                      <div><span style="color: #00aa00;margin-left:50px;" id="success_msg"></span></div>
                                                     <div><span style="color: red;margin-left:50px;" id="error_msg"></span></div>
                                              
                                                        <form id="ch_pswd_form" class="mu-contact-form" action="javascript:void(0);" method="POST" >
                                                            <br>
                                                            <div class="row">
                                                              
                                                                <div class="col-sm-10 col-sm-offset-1">  <h4 class="section-title"><?=$this->lang->line('change_pass');?> </h4></div>
                                                            <div class="col-sm-5 col-sm-offset-1">
                                                                
                                                               <!-- <div class="form-group">
                                                                    <label>Current Password :</label>
                                                                      <input type="password" class="form-control" id="newpswd" name="newpswd" placeholder="Password ">
                                       
                                                                </div> -->
                                                                <div class="form-group mb-20">
                                                                    <label><?= $this->lang->line('new_pass') ?> :</label>
                                                                    <input type="password" class="form-control" id="pswd" name="pswd" placeholder="<?= $this->lang->line('new_pass') ?> ">
                                                                </div>
                                                                <div class="form-group mb-20">
                                                                    <label><?= $this->lang->line('confirm_new_pass') ?> :</label>
                                                                      <input type="password" class="form-control" id="conf_pswd" name="conf_pswd" placeholder="<?= $this->lang->line('confirm_new_pass') ?>">
                                       
                                                                </div>
                                                               
                                                            </div>  

                                                                <div class="clearfix"></div>
                                                            <div class="col-sm-5 col-sm-offset-1">
                                                                <br>
                                                                 <button type="submit"  name="ch_pswd_form_submit" id="ch_pswd_form_submit"  class="btn btn-finish btn-primary"><?=$this->lang->line('change_pass');?> </button>
                                                                <!-- <input type='button' class='btn btn-finish btn-primary' name='finish' value='Update' /> -->
                                                            </div>
                                                            </div>
                                                            <br>



                                                            <br>
                                                        </form>

                                                    </div>
                                                
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div><!-- end row -->


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Contact section -->
      