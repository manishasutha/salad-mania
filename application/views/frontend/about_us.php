<div class="mu-title" id="mu-title-mark">
</div>

<section style="margin-top: 68px; height: 76vh; background: #607d8b; background-image: url(<?=INCLUDE_FRONT_ASSETS?>img/storybg.jpg); background-size: cover; background-repeat: no-repeat">
    <div class="container-fluid">
        <div class="row">
            <div class="video-iframe">
                <div class="content text-center">
                    <h4><?=$this->lang->line('about_us');?></h4>
                    <p>Founded in 2019, SaladMania is a destination for healthy food. We believe its all about the choices we make about what we eat, we still have long way to achieve our future goals, to build a healthy supply community and be  one Jordan icons for healthy food.</p>
                    <p>We believe that we need to do things differently ,putting our customers as top 1 priority moving forward .</p>
                    <br>
                    <a href="<?=getSingleFieldDetail('id',1,'about_us_link','sm_app_settings')?>">
                        <i class="fa fa-play-circle-o"></i>
                        WATCH THE VIDEO
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>