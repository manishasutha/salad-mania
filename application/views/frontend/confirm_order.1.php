<div class="mu-title" id="mu-title-mark">

</div>
<!-- Start Restaurant Menu -->
<section id="mu-restaurant-menu" class="menu-img" >
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12" style="float: none; margin: auto">


                <form action="javascript:void(0);" id="confirm_order_form" method="post">
                    <?php
                    if($cart_details){
                        ?>
                        <div class="order-preview">
                            <div class="op-section-head">
                                <h4 class="bag-title"><?=$this->lang->line('order');?></h4>
                                <p>
                                    <?php
                                    $date=date("Y/m/d");
                                    $yrdata= strtotime($date);
                                    ?>
                                    <?=date('d M Y', $yrdata)?>
                                </p>
                            </div>

                            <div class="op-section">
                                <div class="row">

                                    <div class="col-md-5">
                                        <h4 class="op-section-title"><?=$this->lang->line('delivery');?>/<?=$this->lang->line('pick_up');?></h4>
                                        <table>

                                            <tr>
                                                <?php
                                                if($delivery_type =='delivery') {
                                                    ?>
                                                    <td>
                                                        <img src="<?= INCLUDE_FRONT_ASSETS ?>img/delivery_icon.png"/>
                                                    </td>
                                                    <td><?=$this->lang->line('delivery');?></td>
                                                    <?php
                                                }elseif($delivery_type =='pickup') {
                                                    ?>
                                                    <td>
                                                        <img src="<?= INCLUDE_FRONT_ASSETS ?>img/pickup_icon.png"/>
                                                    </td>
                                                    <td><?=$this->lang->line('pick_up');?></td>
                                                    <?php
                                                }
                                                ?>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-7">
                                        <?php
                                        if($delivery_type=='delivery'){
                                            ?>
                                            <h4 class="op-section-title"><?=$this->lang->line('delivery_address');?></h4>
                                            <?php
                                                if($this->session->userdata('user_login_session')){
                                                    $login=$this->session->userdata('user_login_session');
                                                    if(!empty($address)){
                                                        ?>
                                                        <p id="address"><?=$address->addline1.', '.$address->addline2.','. $this->lang->line("amman")?></p>
                                                        <p><?=getSingleFieldDetail('id',$login['id'],'phone','sm_users')?></p>
                                                        <?php
                                                    }if(!empty($address)){
                                                        ?>
                                                        <button data-toggle="modal"  onclick="AddressModel()"  class="btn btn-xs btn-finish btn-primary" name="finish" value=""><?=$this->lang->line('edit');?></button>
                                                        <?php
                                                    }else{
                                                        ?>
                                                        <button  data-toggle="modal"  onclick="AddressModel()"  class="btn btn-xs btn-finish btn-primary" name="finish" value="Address"><?=$this->lang->line('address');?></button>
                                                        <?php
                                                    }
                                                }else if($this->session->userdata('cart_session')){
                                                    $login=$this->session->userdata('guest_user_session');
                                                    if(!empty($address)){
                                                        ?>
                                                        <p id="address"><?=$address->addline1.', '.$address->addline2.','. $this->lang->line("amman")?></p>
                                                        <p><?=getSingleFieldDetail('id',$login['id'],'phone','sm_users')?></p>
                                                        <?php
                                                    }if(!empty($address)){
                                                        ?>
                                                        <button  data-toggle="modal" class="btn btn-xs btn-finish btn-primary" onclick="AddressModel()" name="finish" value="Edit"><?=$this->lang->line('edit');?></button>
                                                        <?php
                                                    }else{
                                                        ?>
                                                        <button data-toggle="modal"  onclick="AddressModel()"  class="btn btn-xs btn-finish btn-primary" name="finish" value="Address"><?=$this->lang->line('address');?></button>
                                                        <?php
                                                    }
                                                }
                                            ?>


                                            <?php
                                        }elseif($delivery_type =='pickup'){
                                            ?>
                                            <h4 class="op-section-title"><?=$this->lang->line('pickup_add');?></h4>
                                            <?php
                                            $address=getSingleFieldDetail('id',1,'address','sm_app_settings');
                                            ?>
                                            <p id="address"><?=$address?> </p>
                                            <?php
                                        }
                                        ?>


                                    </div>
                                </div>


                            </div>

                            <hr>

                            <?php
                            if($delivery_type =='pickup'){
                                $checkout_values=$this->session->userdata('checkout_session');
                                ?>

                                <div class="op-section">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4 class="op-section-title text-center"><?=$this->lang->line('pickup_details');?></h4>
                                            <table class="" >
                                                <tr >
                                                    <td class="font-weight-bold text-uppercase col-md-2"><?=$this->lang->line('date');?>:</td>
                                                    <td class="col-md-4"><?=$checkout_values['date']?></td>
                                                </tr>
                                                <tr>
                                                    <td class="font-weight-bold text-uppercase col-md-2"><?=$this->lang->line('name');?>:</td>
                                                    <td class="col-md-4"><?=$checkout_values['name']?></td>
                                                    <td class="font-weight-bold text-uppercase col-md-2"><?=$this->lang->line('mobile');?>:</td>
                                                    <td class="col-md-4"><?=$checkout_values['mobile']?></td>

                                                </tr>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            if($cart_details){
                                $total_price=0;
                                ?>

                                <div class="op-section">
                                    <h4 class="op-section-title"><?=$this->lang->line('items');?></h4>
                                    <?php
                                    foreach ($cart_details as $cart) {
                                        $total_price = +$total_price + $cart['item_total_price'];
                                        ?>
                                        <div class="bag-item">


                                            <div class="bag-item-group">
                                                <div class="bi-img"><img
                                                            src="<?=INCLUDE_SALAD_IMAGE_PATH.$cart['salad_img']?>"/></div>
                                                <div class="bi-title"><?=$cart['salad_name']?></div>
                                                <div class="bi-qty">
                                                    Qty: <?=$cart['quantity']?>
                                                </div>
                                                <div class="bi-price"><?=$cart['item_total_price']?><?= $this->lang->line("jd")?></div>
                                            </div>
                                            <?php
                                            $ing_ds=explode(',',$cart['ing_ids']);
                                            foreach ($ing_ds as $id){
                                                  $language= $this->session->userdata('language');
                                                    $ing_name=getSingleFieldDetail('ing_id',$id,'ing_name','sm_ingredients');
                                                    if($language=='Arabic'){
                                                        $ing_name=getSingleFieldDetail('ing_id',$id,'ing_name_arabic','sm_ingredients');
                                                    }
                                                ?>
                                                <div class="bag-item-addons">
                                                    <div class="bi-title"><?= $ing_name ?></div>
                                                    <div class="bi-price"><?=getSingleFieldDetail('ing_id',$id,'ing_price','sm_ingredients')?><?$this->lang->line('jd')?></div>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <?php
                            }
                            ?>
                            <hr>
                            <div class="op-footer">
                                <div><span class="bag-price-title"><?=$this->lang->line('subtotal');?></span>
                                    <span class="bag-price"><span id="sub_total"><?=floatval($total_price)?><?= $this->lang->line("jd")?></span></span></div>
                                <div><span class="bag-price-title"><?=$this->lang->line('taxes');?>(<span id="tax_rate"><?=getSingleFieldDetail('id',1,'tax_rate','sm_app_settings  ')?></span>%)</span>
                                    <span class="bag-price"><span id="tax_price"></span><?= $this->lang->line("jd")?></span></div>
                                <div><span class="bag-price-title"><?=$this->lang->line('delivery_charge');?></span>
                                    <span class="bag-price"><span id="delivery_charge"><?=getSingleFieldDetail('id',1,'delivery_charge','sm_app_settings')?></span><?= $this->lang->line("jd")?></span></div>

                                <div class="grand"><span class="bag-price-title"><?=$this->lang->line('total');?></span>
                                    <span class="bag-price"><span id="total"></span><?= $this->lang->line("jd")?></span></div>
                                <br>




                                <select name="payment_type" id="payment_type" class="form-control mb-12">
                                    <option value="" selected><?=$this->lang->line('payment_type');?> </option>
                                    <option value="COD"><?=$this->lang->line('cod');?> </option>
                                    <option value="online"><?=$this->lang->line('online');?></option>
                                </select>

                                <button class="btn btn-checkout"  type="submit"><?=$this->lang->line('confirm');?> <i class="fa fa-angle-right"></i></button>
                            </div>
                        </div>
                        <?php
                    }else{
                        ?>
                    <div class="order-preview">
                        <div class="op-section-head">
                            <h4 class="bag-title"><?=$this->lang->line('order');?></h4>

                        </div>
                        <div class="op-section">
                            <div class="row">
                                <h4 class=" text-center op-section-title"><?=$this->lang->line('no_item_cart');?></h4>
                            </div>
                        </div>
                    </div>
                    <?php
                    }
                    ?>

                </form>


            </div>
        </div>
    </div>
</section>








<!-- Address Modal -->
<div class="modal fade" id="addressModal" tabindex="-1" role="dialog" aria-labelledby="addressModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="addressModalt"><span style="font-weight:600"><?=$this->lang->line('edit_add');?></span>

                </h4>
            </div>
            <div class="modal-body">
                <form class="mu-contact-form text-center" action="javascript:void(0);" id="address_update" method="post">
                    <div class="row" style="margin: 0">
                        <div class="col-md-7" style="padding: 0; float: none; margin: 40px auto;  padding-top: 0px">
                            <div class="mu-contact-left">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="edit_build_no" id="edit_build_no"   placeholder="<?=$this->lang->line('building_no');?>">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="edit_street_no"  id="edit_street_no"  placeholder="<?=$this->lang->line('street_name');?>">
                                </div>
                                <div class="form-group">
                                    <input type="text" value="Amman" readonly class="form-control" name="city" id="city"   placeholder="<?=$this->lang->line('city');?>City">
                                </div>
                            </div>
                            <br>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('cancel');?></button>
                        <button type="submit" class="btn btn-primary"><?=$this->lang->line('save');?></button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>