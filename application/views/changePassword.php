<?php include"include/header.php" ?>

	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include"include/navbar.php" ?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include"include/leftsidebar.php" ?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                     
			<!-- MAIN CONTENT -->
			<div class="main-content">
                <div id="custom_error"></div>
                <?php
                    if($this->session->flashdata('success_msg')){
                    $msg = $this->session->flashdata('success_msg');
                        echo '<div class="alert alert-success fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong>'. $msg.'
                            </div>';
                    }
                ?>
				<div class="container-fluid">
					
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
                            <form class="col-md-10 form-panel" style="border:0" method="post" action="<?= base_url()?>profile/changePassword" id="passwordChangeFrm">
                                <div class="row">
                                    <div class="panel new-panel">
                                            <div class="panel-heading">
                                                <div class="col-md-12">
                                                    <h3 class="panel-title"><?=$this->lang->line('Change Password')?$this->lang->line('Change Password'):'Change Password'?></h3>
                                                </div>
                                                <br>
                                            </div>
                                      
                                            <div class="panel-body no-padding">
                                                <div class="col-md-4">
                                                    <label><?=$this->lang->line('Old Password')?$this->lang->line('Old Password'):'Old Password'?> </label>
                                                    <input class="form-control" value="" name="old_password" type="password" id="old_password">
                                                    <br>
                                                </div>
                                                <div class="col-md-4">
                                                    <label><?=$this->lang->line('Password')?$this->lang->line('Password'):'Password'?> </label>
                                                    <input class="form-control" value="" name="password" type="password" id="password">
                                                    <br>
                                                </div>
                                                <div class="col-md-4">
                                                    <label><?=$this->lang->line('Confirm Password')?$this->lang->line('Confirm Password'):'Confirm Password'?> </label>
                                                    <input class="form-control" value="" name="conf_password" type="password" id="conf_password">
                                                    <br>
                                                </div>
                                               
                                               <div class="clearfix"></div>                                                                                          
                                            </div>
                                            <div class="panel-footer">
                                                    <div class="row">
                                                        <div class="col-md-12 text-right"><!-- <a href="ticket_add.html" class="btn btn-primary" id="profileUpdateFrm"> Update </a> -->
                                                        <button type="submit" class="btn btn-primary  " id="passwordChangeBtn"><?=$this->lang->line('Update')?$this->lang->line('Update'):'Update'?> </button>
                                                        </div>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                            </form>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
                          <div class="subfooter">
                          <?php include"include/footer_content.php" ?>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		
	</div>
	<!-- END WRAPPER -->
	<?php include"include/footer.php" ?>
