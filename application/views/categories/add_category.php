<?php include"include/header.php";?>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include "include/navbar.php"; ?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include"include/leftsidebar.php"; ?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
                     <div class="subheader">
                        <ul>
                            <li><?=$this->lang->line('setting')?$this->lang->line('setting'):'Setting';?></li>
                        </ul>
                    </div>
			<!-- MAIN CONTENT -->
			<div class="main-content">
                <div id="custom_error">
                </div>
				<div class="container-fluid">
					
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
                            <form class="col-md-12 form-panel" style="border:0" method="post" id="addCategoryFrm" action="<?= SITE_URL?>category/addAction">
                                <div class="row">
                                    <div class="panel new-panel">
                                            <div class="panel-heading">
                                                <div class="col-md-12">
                                                    <h3 class="panel-title"><?=$this->lang->line('add_category')?$this->lang->line('add_category'):'Add Category';?></h3>
                                                </div>
                                                <br>
                                                <span id="menu_images_error"></span>
                                            </div>
                                            <div class="col-md-6">
                                                    <label><?=$this->lang->line('name')?$this->lang->line('name'):'Name'?> <span class="required error">*</span></label>
                                                    <br><span id="name_error"></span>
                                                    <input class="form-control"  type="text" name="name" id="name" value="">
                                                    <br>
                                            </div>
                                             <div class="col-md-6">
                                                    <label><?=$this->lang->line('name')?$this->lang->line('name').'(Italiano)':'Name(Italian)'?> <span class="required error">*</span></label>
                                                    <br><span id="name_italian_error"></span>
                                                    <input class="form-control"  type="text" name="name_italian" id="name_italian" value="">
                                                    <br>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="panel-heading">
                                                <div class="panel-body no-padding">
                                                    <div class="col-md-12">
                                                        <label><?=$this->lang->line('description')?$this->lang->line('description'):'Description'?><span class="required error">*</span></label><br><span id="description_cat_error"></span><br>
                                                         <textarea rows="5" cols="51" name="description_cat" id="description_cat">         
                                                        </textarea><br>
                                                   </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-md-12 text-right"><button class="btn btn-primary" type="Submit"><i class="fa fa-plus"></i> <?=$this->lang->line('Submit')?$this->lang->line('Submit'):'Submit'?></button>
                                            </div>
                                            </div>
                                            </div>
                                    </div>
                                </div>
                            </form>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
	
                <?php include"include/footer_content.php" ?>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		
	</div>
	<!-- END WRAPPER -->
	<script>
        $(document).ready(function(){
            //$('.datatable').DataTable();

            $('#datetimepicker1').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
            });

            $('#datetimepicker2').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
            });
            $('.select-search').select2();
            $('#description_cat').val($('#description_cat').val().trim());
        });
    </script>
    <?php include"include/footer.php" ?>
