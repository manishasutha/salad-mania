<?php include "include/header.php"; ?>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include "include/navbar.php"; ?>
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include "include/leftsidebar.php" ?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			  <div class="subheader">
                        <ul>
                            <li><?=$this->lang->line('setting')?$this->lang->line('setting'):'Setting';?></li>
                        </ul>
                    </div>
			<!--  MAIN CONTENT -->
			<div class="main-content">
				<?php
					if($this->session->flashdata('success_msg')){
					$msg = $this->session->flashdata('success_msg');
						echo '<div class="alert alert-success fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Success!</strong>'. $msg.'
			        		</div>';
		        	}
				?>
				<div class="container-fluid">
					
					<!-- END OVERVIEW -->
					<div class="row">
						<div class="col-md-12">
							<!-- RECENT PURCHASES -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title"><?=$this->lang->line('category_list')?$this->lang->line('category_list'):'Category List';?></h3>
									<div class="right">
										<a href="<?=SITE_URL?>category/add" class="btn btn-primary"><i class="fa fa-plus"></i> <?=$this->lang->line('add_category')?$this->lang->line('add_category'):'Add Category';?></a>
									</div>
								</div>
								<div class="panel-body no-padding">
								<div class="table-responsive">
									<table class="table table-striped datatable">
										<thead>
											<tr>
												<th>#<?=$this->lang->line('id')?$this->lang->line('id'):'ID';?></th>
												<th><?=$this->lang->line('cat_name')?$this->lang->line('cat_name'):'Name';?></th>
												<th><?=$this->lang->line('name_Arabic')?$this->lang->line('name_Arabic'):'Name Arabic';?></th>
												<th><?=$this->lang->line('cat_type')?$this->lang->line('cat_type'):'Type';?></th>
												<th><?=$this->lang->line('cat_status')?$this->lang->line('cat_status'):'Status';?></th>
												<th><?=$this->lang->line('edit')?$this->lang->line('edit'):'Edit';?></th>
                                                <th><?=$this->lang->line('delete')?$this->lang->line('delete'):'Delete';?></th>
                                                <th><?=$this->lang->line('updated_at')?$this->lang->line('updated_at'):'Update Date';?></th>
											</tr>
										</thead>
										<tbody>
												<?php
												if(!empty($query) && count($query)){
													$enable = $this->lang->line('enable')?$this->lang->line('enable'):'Enable';
													$disable = $this->lang->line('disable')?$this->lang->line('disable'):'Disable';
													foreach ($query as $key => $cat) {
														if($cat['status']){
															$status_span = '<span class="label label-success">'.$enable.'</span>';
														}else{
															$status_span = '<span class="label label-danger">'.$disable.'</span>';
														}
														
														echo'<tr>
															<td></td>
															 <td>'.$cat['cat_name'].'</td>
															  <td>'.$cat['name_Arabic'].'</td>
															 <td>'.$cat['cat_type'].'</td>
															 <td><a href="'.SITE_URL.'?id='.$cat['cat_id'].'&status='.$cat['status'].'">'.$status_span.'</a></td>
                                                			 <td>
                                                			 	<a class="btn btn-xs btn-primary" href="'.SITE_URL.'?id='.$cat['cat_id'].'" id="editCategory" data-cat-id="'.$cat['cat_id'].'"><i class="fa fa-pencil"></i>
                                                			 	</a>
                                                			 </td>
                                                			 <td>
                                                			 	<a class="btn btn-xs btn-danger deleteItem" href="'.SITE_URL.'?id='.$cat['cat_id'].'">
                                                			 		<i class="fa fa-trash"></i>
                                                			 	</a>
                                                			 </td>
                                                			 <td>'.$cat['last_updated_date'].'</td>
                                                			 </tr>
															';
													}
												}
												?>									
										</tbody>
									</table>
								</div>
								</div>
								<div class="panel-footer">
									<div class="row">
	                                    <div class=" text-left">
	                                        
	                                    </div>
									</div>
								</div>
							</div>
							<!-- END RECENT PURCHASES -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
                        <?php include"include/footer_content.php" ?>
		<!-- END MAIN -->
		<div class="clearfix"></div>
		
	</div>
	<!-- END WRAPPER -->
	<!-- Edit category model start added sdsdsd ravindra -->
		<div class="modal" id="editCatModal"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header text-left">
		                <h4 class="modal-title w-100 font-weight-bold"><?=$this->lang->line('Edit Category')?$this->lang->line('Edit Category'):'Edit Category'?> </h4>
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -27px;opacity: 0.6;">
		                    <span aria-hidden="true">&times;</span>
		                </button>
		            </div>
		            <div class="modal-body mx-2">
		                <div class="md-form mb-3 ">
		                	<label data-error="wrong" data-success="right" for="cat_edit_name"><?=$this->lang->line('name')?$this->lang->line('name'):'Name'?></label>
		                	<br><span id="name_error"></span>
		                    <input type="text" id="cat_edit_name" name="cat_edit_name" class="form-control validate">
		                    <input type="hidden" name="cat_edit_id" id="cat_edit_id">
		                </div>
		                <div class="md-form mb-3 ">
		                	<label data-error="wrong" data-success="right" for="cat_edit_name"><?=$this->lang->line('name')?$this->lang->line('name').'(Italiano)':'Name(Italian)'?></label>
		                	<br><span id="name_italian_error"></span>
		                    <input type="text" id="cat_edit_name_italian" name="cat_edit_name_italian" class="form-control validate">
		                </div>
		                <div class="md-form mb-3 modelDivSpace">
		                	<label data-error="wrong" data-success="right" for="cat_edit_name"><?=$this->lang->line('description')?$this->lang->line('description'):'Description'?></label>
		                	<br><span id="description_cat_error"></span><br>
		                   <!--  <input type="text" id="cat_edit_description"  name="cat_edit_description" class="form-control validate"> -->
	                         <textarea rows="5" cols="25" name="cat_edit_description" id="cat_edit_description" class="form-control validate">
	                         </textarea>
		                </div>
		            </div>
		            <div class="modal-footer d-flex justify-content-center">
		                <button class="btn-primary" type="button" id="editCatSubmitBtn"><i class="fa fa-plus"></i><?=$this->lang->line('Submit')?$this->lang->line('Submit'):'Submit'?></button>
		            </div>
		        </div>
		    </div>
		</div>
	<!-- Edit category model end -->
	<?php include"include/footer.php"; ?>