<?php include "include/header.php"; ?>
<!-- WRAPPER -->
<div id="wrapper">
    <!-- NAVBAR -->
    <?php include "include/navbar.php"; ?>
    </nav>
    <!-- END NAVBAR -->
    <!-- LEFT SIDEBAR -->
    <?php include "include/leftsidebar.php" ?>
    <!-- END LEFT SIDEBAR -->
    <!-- MAIN -->
    <div class="main">
        <div class="subheader">
            <ul>
                <li> <?=$this->lang->line('admin')?$this->lang->line('admin'):'Admin';?> / Add Ingredient</li>
            </ul>
            	<?php
			if($this->session->flashdata('success_msg')){
				$msg = $this->session->flashdata('success_msg');
				echo '<div class="alert alert-success fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Success!</strong>'. $msg.'
			        		</div>';
			}
			if($this->session->flashdata('error_msg')){
				$msg = $this->session->flashdata('error_msg');
				echo '<div class="alert alert-danger fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Error!</strong>'. $msg.'
			        		</div>';
			}
			?>
        </div>

        <div class="main-content">

            <div class="container-fluid">

                <!-- END OVERVIEW -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- RECENT PURCHASES -->
                        <div class="panel">
                            <div class="panel-heading2">
                                <h3 class="panel-title2">Add Ingredient</h3>
                            </div>
                            <div class="panel-body" style="    margin-top: 50px;">
                               <form enctype="multipart/form-data" id="add_ing" action="<?=base_url()?>ingredient/add" method="post">
                                   <div class="row" style="margin-bottom: 20px">
                                       <div class="col-lg-6">
                                           <div class="md-form mb-3 ">
                                               <label for="ing_name">Name</label>
                                               <input type="text" id="ing_name" name="ing_name" class="form-control validate">
                                           </div>
                                       </div>

                                       <div class="col-lg-6">
                                           <div class="md-form mb-3">
                                               <label for="ing_name_arabic">Name(Arabic)</label>
                                               <input type="text" id="ing_name_arabic" name="ing_name_arabic" class="form-control validate">
                                           </div>
                                       </div>
                                   </div>

                                   <div class="row" style="margin-bottom: 20px">
                                       <div class="col-lg-3">
                                           <div class="md-form mb-3 ">
                                               <label for="weight">Weight</label>
                                               <input type="text" id="weight" name="weight" class="form-control validate" onchange="removeQuantity()">
                                           </div>
                                       </div>


                                       <div class="col-lg-3">
                                           <div class="md-form mb-3 ">
                                               <label for="weight">Unit</label>
                                                <select type="text" style="color:black;" id="unit_type" name="unit_type" class="form-control" onchange="removeQuantity()">
                                                    <option value="">Select Unit</option>
                                                    <?php
                                                        foreach ($unit_type as $unit) {
                                                            echo'<option value="'.$unit.'">'.$unit.'</option>';
                                                        }
                                                        ?>
                                                </select>
                                           </div>
                                       </div>

                                       <div class="col-lg-6">
                                           <div class="md-form mb-3 ">
                                               <label for="Price">Price</label>
                                               <input name="price" id="price" type="text" class="form-control validate" onchange="removeQuantity()">
                                           </div>
                                       </div>
                                   </div>

                                   <div class="row" style="margin-bottom: 30px">
                                       <div class="col-lg-6">
                                           <div class="md-form mb-3 ">
                                               <label for="image">Image</label>
                                               <input type="file" id="image" name="Image" class="form-control validate">
                                           </div>
                                       </div>

                                       <div class="col-lg-6">
                                           <div class="md-form mb-3 ">
                                               <label for="Price">Calories</label>
                                               <input type="text"  id="calories" name="calories" class="form-control validate">
                                           </div>
                                       </div>
                                   </div>
                                    <div class="row" style="">
                                        <div class="col-lg-6">
                                             <button type="button" id="recipies" name="submit" class="btn btn-primary-active">Categories to create Recipes</button>
                                             <button type="button" id="add-on" name="submit" class="btn btn-primary-disable">Add-Ons</button>
                                        </div>
                                    </div>
                                   
                                   <!--Add-ons Sub-cats start-->
                                   <div class="main-nav" id="add-on-cat" style="border: 1px solid #eee ; height: 500px;margin-top:25px;display: none;">
                                       <div class="subheader">
                                           <ul>
                                               <li style="font-size: 22px;" >Add-Ons </li>
                                           </ul>
                                       </div>
                                       <ul class="nav nav-pills" style=" height: 500px; background-color: #eee; padding: 0px 6px; overflow: auto;">
                                            <?php if($category->num_rows() > 0){
                                                 $j=1; 
                                                foreach( $category->result() as $catname){?>
                                                    <li id="<?=$catname->cat_id?>-pre"   class="border-top <?php if($j == 1) echo 'active' ?>"><a data-toggle="pill"  href="#tab_<?=$catname->cat_id?>-pre"><?=$catname->cat_name?></a></li>
                                            <?php $j++; }  ?> 
                                        </ul>
                                        <div class="tab-content">
                                            <?php 
                                                $i = 1;
                                                foreach($category->result() as $crow) 
                                                { 
                                                    //Get the Subcategory of the category
                                                    $sql = "select * from sm_subcategories subcat where subcat.parent_cat_id = $crow->cat_id  AND subcat.salad_type=1 AND subcat.subcat_status = 1";
                                                    $Subcategory = $this->Main_model->__callMasterquery($sql);    
                                             ?>
                                                <div id="tab_<?php echo $crow->cat_id ?>-pre" class="tab-pane fade <?php if($i == 1) echo 'in active'; ?>">
                                                    <h3><?php echo $crow->cat_name ?></h3>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th style="width: 25%">Sub Category Name</th>
                                                                <th style="width: 10%">Select</th>
                                                                <th style="width: 15%">Qty Up down</th>
                                                                <th style="width: 25%">How Much Unit</th>
                                                                <th style="width: 25%">Price (JD) & Cal</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php  if($Subcategory->num_rows()>0){ 
                                                                    $cat_id=[];
                                                                    foreach($Subcategory->result() as $subcat){ 
                                                                         $cat_id= $subcat->id ;?>
                                                                    <tr class="ing-select" id="ing_row_<?php echo $subcat->id ?>">
                                                                      <input type="hidden" id="catTotalItems" value="0">
                                                                        <td><p><?= $subcat->subcat_name?></p></td>
                                                                        <td>
                                                                            <label class="ing-check-width">
                                                                                <input data-id="<?php echo $crow->cat_id ?>" class="subIds" value="<?= $subcat->id?>" name="id[]" type="checkbox" >
                                                                                <span class="checkmark"></span>
                                                                            </label>
                                                                        </td>
                                                                        <td>
                                                                            <label class=" ing-check-width">
                                                                               
                                                                                <input type="checkbox" data-id="<?php echo $crow->cat_id ?>" class="subQty" name="qty_up_down[<?= $cat_id?>][]">
                                                                                <span class="checkmark"></span>
                                                                            </label>
                                                                        </td>
                                                                        <td>
                                                                          <input type="number" data-id="<?php echo $crow->cat_id ?>" id="ing_weight" class="form-control totalWeightDefined howMuch" name="ing_weight[<?= $cat_id?>]" disabled="disabled" min="0" style="width: 70px;float: left;">

                                                                          <input type="text" data-id="<?php echo $crow->cat_id ?>" id="sub_unit_type" name="sub_unit_type[<?= $cat_id?>]" class="form-control sub_unit_type" disabled="disabled" style="width: 70px;float: left;">
                                                                        </td>
                                                                        <td>
                                                                          <input type="number" data-id="<?php echo $crow->cat_id ?>" id="ing_price" name="ing_price[<?= $cat_id?>]" class="form-control ing_price" min="0" disabled="disabled" style="width: 70px;float: left;">

                                                                          <input type="number" data-id="<?php echo $crow->cat_id ?>" id="ing_cal" name="ing_cal[<?= $cat_id?>]" class="form-control ing_cal" min="0" disabled="disabled" style="width: 70px;float: left;">
                                                                        </td>
                                                                    </tr>
                                                                    <?php } } else{ ?>
                                                                        <tr class="ing-select">
                                                                            <td colspan="2"><p class="ing-name"></p></td>
                                                                            <td>No Subcategory Found!!  </td>
                                                                            <td></td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <?php $i++; } } ?>
                                        </div>
                                   </div>

                                   <!--Categories to create Recipes cats start-->
                                   <div class="main-nav" id="recipies-create-cat" style="border: 1px solid #eee ; height: 500px;margin-top:25px">
                                      <div class="subheader">
                                          <ul>
                                              <li style="font-size: 22px;">Categories to create Recipes</li>
                                          </ul>
                                      </div>

                                        <ul class="nav nav-pills" style=" height: 500px; background-color: #eee; padding: 0px 6px; overflow: auto;">
                                            <?php if($category->num_rows() > 0){
                                                 $j=1; 
                                                foreach( $category->result() as $catname){?>
                                                    <li id="<?=$catname->cat_id?>-pre" class="border-top <?php if($j == 1) echo 'active' ?>"><a data-toggle="pill"  href="#tab_<?=$catname->cat_id?>-own"><?=$catname->cat_name?></a></li>
                                            <?php $j++; }  ?> 
                                        </ul>
                                        <div class="tab-content">
                                            <?php 
                                                $i = 1;
                                                foreach($category->result() as $crow) 
                                                { 
                                                    //Get the Subcategory of the category
                                                    $sql = "select * from sm_subcategories subcat where subcat.parent_cat_id = $crow->cat_id  AND subcat.salad_type=2 AND subcat.subcat_status = 1";
                                                    $Subcategory = $this->Main_model->__callMasterquery($sql);    
                                             ?>
                                                <div id="tab_<?php echo $crow->cat_id ?>-own" class="tab-pane fade <?php if($i == 1) echo 'in active'; ?>">
                                                    <h3><?php echo $crow->cat_name ?></h3>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th style="width: 25%">Sub Category Name</th>
                                                                <th style="width: 10%">Select</th>
                                                                <th style="width: 15%">Qty Up down</th>
                                                                <th style="width: 25%">How Much Unit</th>
                                                                <th style="width: 25%">Price (JD) & Cal</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php  if($Subcategory->num_rows()>0){ 
                                                                         $cat_id=[];
                                                                    foreach($Subcategory->result() as $subcat){ 
                                                                         $cat_id= $subcat->id ;?>
                                                                    <tr class="ing-select" id="ing_row_<?php echo $subcat->id ?>">
                                                                    <input type="hidden" id="catTotalItems" value="0">
                                                                        <td><p><?= $subcat->subcat_name?></p></td>
                                                                        <td>
                                                                            <label class="ing-check-width">
                                                                                <input data-id="<?php echo $crow->cat_id ?>" class="subIds" value="<?= $subcat->id?>" name="id[]" type="checkbox" >
                                                                                <span class="checkmark"></span>
                                                                            </label>
                                                                        </td>
                                                                        <td>
                                                                            <label class=" ing-check-width">
                                                                                <input type="checkbox" data-id="<?php echo $crow->cat_id ?>" class="subQty"  name="qty_up_down[<?= $cat_id?>][]">
                                                                                <span class="checkmark"></span>
                                                                            </label>
                                                                        </td>

                                                                        <td>
                                                                          <input type="number" data-id="<?php echo $crow->cat_id ?>" id="ing_weight" class="form-control totalWeightDefined howMuch2" name="ing_weight[<?= $cat_id?>]" min="0" disabled="disabled" style="width: 70px;float: left;">

                                                                          <input type="text" data-id="<?php echo $crow->cat_id ?>" id="sub_unit_type" class="form-control sub_unit_type" name="sub_unit_type[<?= $cat_id?>]" disabled="disabled" style="width: 70px;float: left;" >
                                                                        </td>
                                                                        <td>
                                                                          <input type="number" data-id="<?php echo $crow->cat_id ?>" id="ing_weight" class="form-control ing_price" name="ing_price[<?= $cat_id?>]" min="0" disabled="disabled" style="width: 70px;float: left;">

                                                                          <input type="number" data-id="<?php echo $crow->cat_id ?>" id="ing_cal"  class="form-control ing_cal" name="ing_cal[<?= $cat_id?>]" disabled="disabled" min="0" style="width: 70px;float: left;">
                                                                        </td>
                                                                    </tr>
                                                                    <?php } } else{ ?>
                                                                        <tr class="ing-select">
                                                                            <td colspan="2"><p class="ing-name"></p></td>
                                                                            <td>No Subcategory Found!!  </td>
                                                                            <td></td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <?php $i++; } } ?>
                                        </div>
                                   </div>

                                   <!--Form Submit buttom Start-->
                                  <div class="clearfix"></div>
                                  <div class="text-left">
                                      <input type="hidden" id="value" name="sub_cat_value" class="sub_cat_id">
                                      <input type="hidden" id="totalWeightDefined">
                                      <label id="sub_value_error" class="error"></label>
                                  </div>
                                  <div class="row" style="margin-top: 20px">
                                      <div class="col-md-12 text-right">
                                          <button type="submit" name="submit" class="btn btn-primary">Submit</button></div>
                                      </div>
                                  </div>
                               </form>
                            </div>
                        </div>
                        <!-- END RECENT PURCHASES -->
                    </div>
                </div>
            </div>
        </div>
                       
        <!-- END MAIN CONTENT -->
        <?php include"include/footer_content.php" ?>
        <!-- END MAIN -->
        <div class="clearfix"></div>

    </div>
    <?php include"include/footer.php"; ?>
<script>
		$(document).ready(function(){
            $("#add-on").click(function(){
                $("div#add-on-cat").show();
                $("#add-on").css({"background-color": "#fcb825", "color":"#fff"} );
                $("#recipies").css({"background-color": "#ddd", "color":"#555"});
                $("div#recipies-create-cat").hide();
            });
            $("#recipies").click(function(){
                $("div#recipies-create-cat").show();
                $("#add-on").css({"background-color": "#ddd", "color":"#555"});
                $("#recipies").css({"background-color": "#fcb825", "color":"#fff"});
                $("div#add-on-cat").hide();
            });

        //      $('div.1-own').hide();
        //    $('a.1-own').click(function(){
        //      $('div.1-own').show();
        //    });
        
			$('form[id="add_ing"]').validate({
				rules: {
					ing_name: {
						required : true,
						minlength: 2,
						maxlength: 50,
						// remote: {
						// 	url: "<?=base_url()?>add-check-ingredient",
						// 	type: "post",
						// 	data:{
						// 		ing_id:function() {
						// 			return $( "#cat_edit_id" ).val();
						// 		},
						// 		ing_type:function() {
						// 			return $( "#cat_edit_type" ).val();
						// 		},
						// 	}
						// }
					},
					unit_type:"required",
					ing_name_arabic:
					{
						required:true,
                    },
					price: {
						required: true,
						number: true,
						min:0,
						max:100000,
						maxlength: 10,
					},
					weight: {
						required: true,
						number: true,
						max:1000,
						maxlength: 6,
					},
					calories: {
						required: true,
						number: true,
						max:100000,
						min:1,
						maxlength: 10,
					},
          Image: "required",
				},
				messages: {
					weight: {
						number: "Weight should be numeric only",
						max:"Weight must not be greater than 1000",
						min:"Weight must not be less than 1"
					},
					price: {
						required: "Please enter Amount",
						number: "Amount should be numeric only",
						max:"Amount must not be greater than 1000",
						min:"Amount must not be less than 1"
					},
					calories: {
						required: "Please enter calories",
						number: "calories should be numeric only",
						max:"calories must not be greater than 1000",
						min:"calories must not be less than 1"
					},
					ing_name:{
						remote:"Ingredients name already exist"
					}
				}
			});

           

			$.validator.addMethod("letterswithspace", function(value, element) {
				return this.optional(element) || /^[a-z][a-z\s]*$/i.test(value);
			}, "Please enter only alphabets");

			$.validator.addMethod("lettersonly", function(value, element) {
				return this.optional(element) || /^[a-z]+$/i.test(value);
			}, "Please enter only alphabets");

			$.validator.addMethod("date", function ( value, element ) {
					var bits = value.match( /([0-9]+)/gi ), str;
					if ( ! bits )
						return this.optional(element) || false;
					str = bits[ 1 ] + '/' + bits[ 0 ] + '/' + bits[ 2 ];
					return this.optional(element) || !/Invalid|NaN/.test(new Date( str ));
				},
				"Please enter a date in the format dd/mm/yyyy"
			);

			$.validator.addMethod("alphanumeric", function(value, element) {
				return this.optional(element) || /^[A-za-z0-9\s]+$/i.test(value);
			}, "Letters, numbers, and underscores only please");

		});
        $(document).ready(function() {

            //Define Sub Unit Type array, call it from Helper
            var subUnitType = new Array();
            subUnitType =  <?php echo json_encode(sub_unit_type()); ?>;
            //alert(subUnitType[0]); alert(subUnitType[1]); alert(subUnitType[2]);

             //this function is used in form validation
             $('.subIds').change(function(){   
                checked = $("input.subIds:checked").length;
                if (checked > 0) {
                    $('#value').val(checked);
                }else if(checked == 0){
                    $('#value').val('');
                }
             });

             $('form[id="add_ing"]').submit(function(){
                var no_checked = $('#value').val();
                var totalWeightDefined = $('#totalWeightDefined').val();

                if(no_checked != totalWeightDefined){
                    $("#sub_value_error").show();
                    $('#sub_value_error').text('Please enter the complete details in subcategories.');
                    return false;
                }
                else if(no_checked == ''){
                     $("#sub_value_error").show();
                    $('#sub_value_error').text('Please select atleast one subcategory');
                    return false;
                }
                else{
                   $('#sub_cat_value_error').text();
                    return true;
                }
             });
             
             
            //====Manage category qty on Selection of the subcategory===
            $('.subIds').change(function(){
                var subcat_id = $(this).attr('data-id');
                var sub_row_id = $(this).parent().parent().parent().attr('id');    
                if($(this).prop("checked") == false){
                    var qty = $("#tab_"+subcat_id+" #"+sub_row_id+ " input.subQty").prop('checked', false);

                    //Decrese the How Much Items one time
                    var totalWeightDefined = $("#totalWeightDefined").val();
                    if(totalWeightDefined != '' && totalWeightDefined > 0){
                      totalWeightDefined--;
                      $("#totalWeightDefined").val(totalWeightDefined);
                    }
                }
            });

            //====Manage category ids on Selection of the subcategory===
            $('.subQty').change(function(){
                var subcat_id = $(this).attr('data-id');
                var sub_row_id = $(this).parent().parent().parent().attr('id'); 
                if($(this).prop("checked") == true){
                    var qty=$("#tab_"+subcat_id+" #"+sub_row_id+ " input.subIds").prop('checked', true); //Qty to check
                }
            });
            
            //====Set Unit type while typing the unit in How Much Unit in Add-ons===
            $('.howMuch').change(function(){
                var ing_weight = $(this).val();
                var subcat_id = $(this).attr('data-id');
                var sub_row_id = $(this).parent().parent().attr('id'); 
                var totalWeightDefined = 0;   
                
                //Ingredient Basic Data
                var weight = $("#weight").val();
                var unit_type = $("#unit_type").val();
                var price = $("#price").val();
                var calories = $("#calories").val();
                if(weight != '' && unit_type != '' && price != '' && calories != '')
                {
                  var ing_sub_unit = '';
                  if(unit_type == 'Kilogram')
                    ing_sub_unit = subUnitType[0];
                  else if(unit_type == 'Litre')
                    ing_sub_unit = subUnitType[1];
                  else if(unit_type == 'Pieces')
                    ing_sub_unit = subUnitType[2];

                  if(ing_weight != ''){
                    $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " input.sub_unit_type").val(ing_sub_unit);

                    //calculate price
                    //convert weight to parent unit type first
                    if(unit_type != 'Pieces')
                      weight = weight*1000;

                    var parent_unit = ing_weight/weight;
                    var howMuchPrice = parent_unit*price;
                    var howMuchCalories = parent_unit*calories;

                    $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " input.ing_price").val(howMuchPrice.toFixed(2));
                    $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " input.ing_cal").val(parseInt(howMuchCalories.toFixed(2)));
                  }
                  else{
                    $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " input.sub_unit_type").val('');
                    $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " input.ing_price").val('');
                    $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " input.ing_cal").val('');
                  }

                  var nonempty = $('.totalWeightDefined').filter(function() {
                    if(this.value != '')
                      totalWeightDefined++;
                  });
                  $("#totalWeightDefined").val(totalWeightDefined);
                }
                else{
                  alert("Please specify Wegith, Unit and Price & Calories");
                  $(this).val('');
                }
            });

            //====Set Unit type while typing the unit in How Much Unit in Categories to create Recipes===
            $('.howMuch2').change(function(){
                var ing_weight = $(this).val();
                var subcat_id = $(this).attr('data-id');
                var sub_row_id = $(this).parent().parent().attr('id'); 
                var totalWeightDefined = 0;   
                
                //Ingredient Basic Data
                var weight = $("#weight").val();
                var unit_type = $("#unit_type").val();
                var price = $("#price").val();
                var calories = $("#calories").val();
                if(weight != '' && unit_type != '' && price != '' && calories != '')
                {
                  var ing_sub_unit = '';
                  if(unit_type == 'Kilogram')
                    ing_sub_unit = subUnitType[0];
                  else if(unit_type == 'Litre')
                    ing_sub_unit = subUnitType[1];
                  else if(unit_type == 'Pieces')
                    ing_sub_unit = subUnitType[2];

                  if(ing_weight != ''){
                    $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " input.sub_unit_type").val(ing_sub_unit);

                    //calculate price
                    //convert weight to parent unit type first
                    if(unit_type != 'Pieces')
                      weight = weight*1000;

                    var parent_unit = ing_weight/weight;
                    var howMuchPrice = parent_unit*price;
                    var howMuchCalories = parent_unit*calories;

                    $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " input.ing_price").val(howMuchPrice.toFixed(2));
                    $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " input.ing_cal").val(parseInt(howMuchCalories.toFixed(2)));
                  }
                  else{
                    $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " input.sub_unit_type").val('');
                    $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " input.ing_price").val('');
                    $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " input.ing_cal").val('');
                  }

                  var nonempty = $('.totalWeightDefined').filter(function() {
                    if(this.value != '')
                      totalWeightDefined++;
                  });
                  $("#totalWeightDefined").val(totalWeightDefined);
                }
                else{
                  alert("Please specify Wegith, Unit and Price & Calories");
                  $(this).val('');
                }
            });

            //====Manage precategory qty on Selection of the subcategory===
            $('.subIds').change(function(){
                var subcat_id = $(this).attr('data-id');
                var sub_row_id = $(this).parent().parent().parent().attr('id');    
                if($(this).prop("checked") == false){
                    var qty = $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " input.subQty").prop('checked', false);
                    $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " .howMuch").val(''); 
                    $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " .sub_unit_type").val(''); 
                    $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " .ing_price").val(''); 
                    $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " .ing_cal").val('');
                    $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " .howMuch").attr('disabled','disabled');
                    $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " .sub_unit_type").attr('disabled','disabled');
                    $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " .ing_price").attr('disabled','disabled');
                    $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " .ing_cal").attr('disabled','disabled');
                }
                if($(this).prop("checked") == true){
                    $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " .howMuch").removeAttr('disabled','disabled');
                    $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " .sub_unit_type").removeAttr('disabled','disabled');
                    $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " .ing_price").removeAttr('disabled','disabled');
                    $("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " .ing_cal").removeAttr('disabled','disabled');
                }
            });
            //====Manage precategory ids on Selection of the subcategory===
            $('.subQty').change(function(){
                 var subcat_id = $(this).attr('data-id');
                var sub_row_id = $(this).parent().parent().parent().attr('id');  
                if($(this).prop("checked") == true){
                    var qty=$("#tab_"+subcat_id+"-pre"+" #"+sub_row_id+ " input.subIds").prop('checked', true); //Qty to check 
                }
            });
            //end of manage category//

            //====Manage customcategory qty on Selection of the subcategory===
            $('.subIds').change(function(){
                var subcat_id = $(this).attr('data-id');
                var sub_row_id = $(this).parent().parent().parent().attr('id');    
                if($(this).prop("checked") == false){
                    var qty = $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " input.subQty").prop('checked', false);  

                    $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " .howMuch2").val(''); 
                    $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " .sub_unit_type").val(''); 
                    $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " .ing_price").val(''); 
                    $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " .ing_cal").val('');
                    $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " .howMuch2").attr('disabled','disabled');
                    $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " .sub_unit_type").attr('disabled','disabled');
                    $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " .ing_price").attr('disabled','disabled');
                    $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " .ing_cal").attr('disabled','disabled');
                }
                if($(this).prop("checked") == true){
                    $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " .howMuch2").removeAttr('disabled','disabled');
                    $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " .sub_unit_type").removeAttr('disabled','disabled');
                    $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " .ing_price").removeAttr('disabled','disabled');
                    $("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " .ing_cal").removeAttr('disabled','disabled');
                }
            });
            //====Manage customcategory ids on Selection of the subcategory===
            $('.subQty').change(function(){
                 var subcat_id = $(this).attr('data-id');
                var sub_row_id = $(this).parent().parent().parent().attr('id');  
                if($(this).prop("checked") == true){
                    var qty=$("#tab_"+subcat_id+"-own"+" #"+sub_row_id+ " input.subIds").prop('checked', true); //Qty to check  
                }
            });
            //end of manage category//  
        });
  
    //This function is used to clean the How many units and price boxes in Sub categories
    function removeQuantity(){
      $(".totalWeightDefined").each(function() {                
          $(this).val('');
      });

      $(".sub_unit_type").each(function() {                
          $(this).val('');
      });

      $(".ing_price").each(function() {                
          $(this).val('');
      });

      $(".ing_cal").each(function() {                
          $(this).val('');
      });

      $("#totalWeightDefined").val('');
    }
	</script>