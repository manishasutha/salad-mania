<?php include "include/header.php"; ?>
<!-- WRAPPER -->
<div id="wrapper">
	<!-- NAVBAR -->
	<?php include "include/navbar.php"; ?>
	</nav>
	<!-- END NAVBAR -->
	<!-- LEFT SIDEBAR -->
	<?php include "include/leftsidebar.php" ?>
	<!-- END LEFT SIDEBAR -->
	<!-- MAIN -->
	<div class="main">
		<div class="subheader">
			<ul>
				<li> <?=$this->lang->line('admin')?$this->lang->line('admin'):'Admin';?> / Ingredient List</li>
			</ul>
			<div class="right" style="float: right">
				<a href="<?= base_url() ?>ingredient/add" class="btn add btn-primary button-fix"><i class="fa fa-plus"></i> <?=$this->lang->line('add_category')?$this->lang->line('add_category'):'Add Ingredient';?></a>
			</div>
		</div>
		<!--  MAIN CONTENT -->
		<div class="main-content">
			<?php
			if($this->session->flashdata('success_msg')){
				$msg = $this->session->flashdata('success_msg');
				echo '<div class="alert alert-success fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Success!</strong>'. $msg.'
			        		</div>';
			}
			if($this->session->flashdata('error_msg')){
				$msg = $this->session->flashdata('error_msg');
				echo '<div class="alert alert-danger fade in">
			        		<a href="#" class="close" data-dismiss="alert">&times;</a>
			        		<strong>Error!</strong>'. $msg.'
			        		</div>';
			}
			?>
			<div class="container-fluid">

				<!-- END OVERVIEW -->
				<div class="row">
					<div class="col-md-12">
						<!-- RECENT PURCHASES -->
						<div class="panel">
							<div class="panel-heading2">
								<h3 class="panel-title2">Ingredient List</h3>

							</div>
							<div class="panel-body no-padding">
								<div class="table-responsive">
									<table class="table table-striped datatable">
										<thead>
										<tr>
											<th>S.No</th>
											<th> Name</th>
											<th>Name Arabic</th>
											<th>Price</th>
											<th>Image</th>
											<th>Status</th>
											<th>Action</th>


										</tr>
										</thead>
										<tbody>
										<?php
										if(!empty($query) && $query->num_rows() > 0){
											$enable = $this->lang->line('enable')?$this->lang->line('enable'):'Enable';
											$disable = $this->lang->line('disable')?$this->lang->line('disable'):'Disable';
											$i=1;
											foreach ($query->result() as $ing) {

												if($ing->ing_status){
													$status_span = '<span class="label label-success">'.$enable.'</span>';
												}else{
													$status_span = '<span class="label label-danger">'.$disable.'</span>';
												}?>

												<tr>
													<th><?php echo $i?></th>
													<td><?php echo $ing->ing_name ?></td>
													<td><?php echo $ing->ing_name_arabic?></td>
													<td><?php echo $ing->ing_price ?>JD</td>
													<td> 
														<?php if( $ing->ing_image!=''){ ?>
															<img style="height:50px;width:50px; " class="post-thumb" src="<?php echo base_url(); ?>uploads/ingredients/<?php echo $ing->ing_image; ?>"></td>
													<?php } else { ?>
																<h4> N/A</h4>
													<?php } ?>
													<td><a><?php echo $status_span ?></a></td>
													<td>
														<a  title="Edit Ingredient!" class="btn btn-xs btn-primary"  id="editIng" href="<?=base_url()?>ingredient/edit/<?=$ing->ing_id?>"><i class="fa fa-pencil"></i>
														</a>
														<?php if($ing->ing_status==1){
															?>
															<a  title="Disable Ingredient!" class="btn btn-xs btn-danger disableItem" href="<?=base_url()?>ingredient/disable/<?=$ing->ing_id;?>">
																<i class='fa fa-eye-slash'></i>
															</a>
														<?php }
														else{
															?>
															<a  title="Enable Ingredient!" style="" class="btn btn-xs btn-info enableItem" href="<?=base_url()?>ingredient/enable/<?=$ing->ing_id;?>">
																<i  class="fa fa-eye"></i>
															</a>
														<?php } ?>

													</td>

												</tr>
												<?php
												$i++;}
										}
										?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="panel-footer">
								<div class="row">
									<div class=" text-left">

									</div>
								</div>
							</div>
						</div>
						<!-- END RECENT PURCHASES -->
					</div>
				</div>
			</div>
		</div>
		<!-- END MAIN CONTENT -->
		<?php include"include/footer_content.php" ?>
		<!-- END MAIN -->
		<div class="clearfix"></div>

	</div>
	<!-- END WRAPPER -->
	<?php include"include/footer.php"; ?>
	