<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title><?=$page_title?> | Salad Mania</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/main.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/demo.css">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="icon" type="image/png" sizes="96x96" href="<?= base_url() ?>assets/img/favicon.png">
	<script src="<?= base_url() ?>assets/vendor/jquery/jquery.min.js"></script>
	<script type="text/javascript">
		var base_url = "<?= base_url() ?>";
	</script>
	<style type="text/css">
		.error {
			  color: red;
			  margin-left: 5px;
			}

			label.error {
			  display: inline;
			}
	</style>
</head>

<body style="background: #F4F5F7; background-image: url(<?= base_url() ?>assets/img/office.jpg); background-size: cover">
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="col-lg-4 col-md-8 col-sm-10 col-xs-12" style="float: none; margin: auto">
                     <div class="login-header">
						<div class="logo text-center">
							
                            <img src="<?= base_url(); ?>uploads/logo/logo.png" height="60"/> Salad Mania
                        </div>
                        <br>
                        <br>
					</div>
					<div class="login-box">
						<?php
				            if($this->session->flashdata('success_msg')){
				            	$msg = $this->session->flashdata('success_msg');
				                echo '<div class="alert alert-success fade in">
				                    <a href="#" class="close" data-dismiss="alert">&times;</a>
				                    <strong>Success! </strong>'. $msg.'
				                    </div>';
				            }
				            if($this->session->flashdata('error_msg')){
				            	$msg = $this->session->flashdata('error_msg');
				                echo '<div class="alert alert-danger fade in">
				                    <a href="#" class="close" data-dismiss="alert">&times;</a>
				                    <strong>Warn! </strong>'. $msg.'
				                    </div>';
				            }
				        ?>
						<div class="content">
							<p class="lead text-center">Forgot Password</p>
							<form class="form-auth-small" method="post" action="<?=base_url().'auth/forgetPasswordAction'?>"  id="forgetPasswordFrm" name="forgetPasswordFrm">
								<div class="form-group">
									<label for="email" class="control-label sr-only">Email</label>
									<input type="email" class="form-control" id="email" name="email" value="<?php echo set_value('email'); ?>" placeholder="Enter email">
									<?php echo form_error('email', '<div class="error">', '</div>'); ?>
								</div>
								
								
								<button type="submit" class="btn btn-primary btn-lg btn-block" id="forgetPassBtn">Submit</button>
								<div class="bottom text-center">
                                <br>
									<span class="helper-text"> <a href="<?= base_url()?>auth/">Login here</a></span>
								</div>
							</form>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
                            
	             <div class="col-sm-12">
	                <ul style="margin: 0; padding: 0; list-style: none">
	                    <li>
	                        <!-- <p class="copyright text-center" style="text-align: center; margin-top: 30px">Reset password link will be sent to your registered email id</p>
	                        <p class="copyright text-center" style="text-align: center; margin-top: 30px; color:#179ab8">Reset link has been sent to your registered email id</p> -->
	                    
	                    </li>
	                </ul>
	            </div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
	<script src="<?= base_url() ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?= base_url() ?>assets/custom/formvalidation.js"></script>
	<script src="<?= base_url() ?>assets/custom/custom.js"></script>
</body>
</html>
