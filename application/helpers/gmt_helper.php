<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('custom_gmt_date'))
{
    function custom_gmt_date($custom_date = '')
    {
		$default_time_zone = date_default_timezone_get();
		$userTimezone = new DateTimeZone($default_time_zone);
		$gmtTimezone = new DateTimeZone('GMT');
		$myDateTime = new DateTime('2018-08-28 05:30:00', $gmtTimezone);
		$offset = $userTimezone->getOffset($myDateTime);
		$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
		$myDateTime->add($myInterval);
		$result_date = $myDateTime->format('Y-m-d H:i:s');
		return $result_date;
    }   
}