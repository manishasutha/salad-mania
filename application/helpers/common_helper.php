<?php
/**
 * THIS FUNCTION USE FOR GETTING SINGLE FIELD DETAILS
 * @param $field
 * @param $val
 * @param $sel_field
 * @param $table
 * @return string
 */

function unit_type($array = NULL){
    $unit=array('Kilogram', 'Litre', 'Pieces');
	return $unit;
}
function sub_unit_type($array = NULL){
    $unit=array('Gram','Millilitre','Piece');
    return $unit;
}

function encryptId($id){
        return substr(md5($id), 0, 6).dechex($id);
    }

    
     function decryptId($id){
        $md5_8 = substr($id, 0, 6);
        $real_id = hexdec(substr($id, 6));
        return ($md5_8==substr(md5($real_id), 0, 6)) ? $real_id : 0;
    }

function getSingleFieldDetailWhere($where, $sel_field, $table) {
    $ci    = &get_instance();
    $ci->db->select($sel_field);
    $ci->db->from($table);
    $ci->db->where($where);
    $query  = $ci->db->get();
    $result = $query->row();
    if ( $result ) {
        $sel_data = $result->$sel_field;
    } else {
        $sel_data = '';
    }
    return $sel_data;
}
function getSingleFieldDetail($field, $val, $sel_field, $table) {
    $ci    = &get_instance();
    $where = array($field => $val);
    $ci->db->select($sel_field);
    $ci->db->from($table);
    $ci->db->where($where);
    $query  = $ci->db->get();
    $result = $query->row();
    if ( $result ) {
        $sel_data = $result->$sel_field;
    } else {
        $sel_data = '';
    }
    return $sel_data;
}

//get count
// function GetCounts(){

//         $count = "select grand_total from sm_orders where 'created_at','=',date('y-m-d')";

//         if(!empty($count)){
//             $counting = 0;
//             foreach ($count as $ce) {
//                 $counting = $counting + $ce->grand_total; 
//             }
//             return $counting;
//         }
//     } 

    //Password Encryption Function
    function encryptPassword($pass){
        return base64_encode(($pass));
    }   

    //Password Dycription Function
    function dycryptPassword($pass){
        return base64_decode($pass);
    }

function getSaladPrice($salad_id){
    $ci    = &get_instance();
    $ci->db->select('i.ing_price');
    $ci->db->from('sm_salad_ingredients as si');
    $ci->db->join('sm_ingredients as i','i.ing_id=si.ing_id');
    $ci->db->where('si.salad_id',$salad_id);
    $query=$ci->db->get();
    $data=$query->result();
    return array_sum(array_column($data,'ing_price'));
}
function getSaladCalories($salad_id){
    $ci    = &get_instance();
    $ci->db->select('i.ing_calories');
    $ci->db->from('sm_salad_ingredients as si');
    $ci->db->join('sm_ingredients as i','i.ing_id=si.ing_id');
    $ci->db->where('si.salad_id',$salad_id);
    $query=$ci->db->get();
    $data=$query->result();
    return array_sum(array_column($data,'ing_calories'));
}
function getSaladIngrediants($salad_id){
    $ci    = &get_instance();
    $ci->db->select('');
    $ci->db->from('sm_salad_ingredients as si');
    $ci->db->join('sm_ingredients as i','i.ing_id=si.ing_id');
    $ci->db->where('si.salad_id',$salad_id);
    $query=$ci->db->get();
    return $query->result();
}

function getAppSettings(){
    $ci    = &get_instance();
    $ci->db->select('');
    $ci->db->from('sm_app_settings');
    $query  = $ci->db->get();
    $result = $query->row();
    
    return $result;
}