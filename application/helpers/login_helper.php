<?php
function is_logged_in() {
    // Get current CodeIgniter instance
    $CI =& get_instance();
    // We need to use $CI->session instead of $this->session
    $login = $CI->session->userdata('logged_in');
    if (!isset($login)) { 
    	redirect(base_url().'admin/login');
    }
}
function is_admin(){
	// Get current CodeIgniter instance
    $CI =& get_instance();
    // We need to use $CI->session instead of $this->session
    $role = $CI->session->userdata('user_role');
    if ($role == 1) { 
    	return $role;
    }else{
    	return false;
    }
}
?>