<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_Controller extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->set_timezone();
        is_logged_in();
        if(!is_admin()){    
            redirect(base_url().('customer'));
            exit;
        }
    }
    
    public function set_timezone() {
        if ($this->session->userdata('user_id')) {
            $this->db->select('timezone');
            $this->db->from($this->db->dbprefix . 'user');
            $this->db->where('user_id', $this->session->userdata('user_id'));
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                date_default_timezone_set($query->row()->timezone);
            } else {
                return false;
            }
        }
    }
    // Uploading file
    public function uploadfile($file_details = array())
    {   
        /*echo "<pre>";
        print_r($file_details);*/
        $config = array();
        $config['upload_path'] = $file_details['upload_path'];
        $config['allowed_types'] = $file_details['allowed_types'];
        $config['max_size']  = $file_details['max_size'];
        if(isset($file_details['max_width']) && isset($file_details['max_height']))
        {
            $config['max_width']  = $file_details['max_width'];
            $config['max_height']  = $file_details['max_height'];
        }
        $file_name = $file_details['filename'];
        
        $this->load->library('Upload',$config);                 
        $upload = $this->upload->do_upload($file_details['filename']);
        
        // If file size is bigger
        if($_FILES[$file_name]['size'] != 0)
        {
            if($upload == NULL)
            {
                $_SESSION['msg']="<div class='alert alert-danger'><i data-dismiss='alert' class='icon-remove close'></i><b>Oops!!</b> File format not supported.</div>";            
                redirect($_SERVER['HTTP_REFERER']);         
            }
        }
        
        $file_data = $this->upload->data();         
        //print_r($file_data['file_name']);exit;        
        return $file_data['file_name'];
    }
}
