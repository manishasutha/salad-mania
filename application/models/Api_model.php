<?php 
class api_model extends CI_Model {
  #This function is used for login from app_users table
  public function login($data){
    $this->db->select('*');
    $this->db->from('app_users');
    $this->db->where('email_id', $data['email']);
    $this->db->where('password', md5($data['password']));
    $query = $this->db->get();
    if($query->num_rows()>0){
      $this->db->set('last_updated_date',date('y-m-d h:i:sa'));
      $this->db->where('email_id', $data['email']);
      $this->db->update('app_users');
      return $query->result_array();
    }else{
      return false;
    }
  }
  #This function is used  add app users.
  public function add_user($data){
    if($this->db->insert('app_users', $data)){
      return true;
    }else{
      return false;
    }
  }
  #this function is used for rating to customer by app user
  public function ratingToCustomer($data){
    if($this->db->insert('rating_comment', $data)){
      return true;
    }else{
      return false;
    }
  }
  #This function is used for update app_user info
  public function updateUser($data){
    $this->db->set('name', $data['name']);
    $this->db->set('zip_code', $data['zip_code']);
    $this->db->set('profile_image', $data['profile_image']);
    $this->db->set('last_updated_date', date('y-m-d h:i:sa'));
    $this->db->where('user_id', $data['user_id']);
    if($this->db->update('app_users')){
      return true;
    }else{
      return false;
    }
  }
  #this function is used for updare app users password.
  public function updateUserPassword($data){
    $this->db->set('password', md5($data['new_password']));
    $this->db->set('last_updated_date', date('y-m-d h:i:sa'));
    $this->db->where('user_id', $data['user_id']);
    if($this->db->update('app_users')){
      return true;
    }else{
      return false;
    }
  }
  #this function is used for search  menu item by menu title and return menu details with customer info.
  public function search($data){
    $this->db->select('customer.serial_number as customer_id,customer.name as title, customer.category_id, customer.latitude,customer.longitude, menu.title,menu.filters, menu.menu_id');
    $this->db->from('menu');
    $this->db->like('title',$data['keywords'], 'both');
    if(isset($data['category_per_filter']) && !empty($data['category_per_filter']) && count($data['category_per_filter'])>0){
      foreach ($data['category_per_filter'] as $key => $menu_category_id) {
        $this->db->or_where('menu.filters like' ,'%"'.$menu_category_id.'"%');
      }
    }
    $this->db->join('customer', 'customer.serial_number = menu.vendor_id');
    $this->db->where('customer.category_id',$data['category']);
    $query1 = $this->db->get_compiled_select();

    if(isset($data['my_favorite']) && !empty($data['my_favorite']) && !empty($data['user_id'])){
      $this->db->select('customer.serial_number as customer_id,customer.name as title, customer.category_id, customer.latitude,customer.longitude, menu.title,menu.filters');
      $this->db->from('menu');
      $this->db->join('customer', 'customer.serial_number = menu.vendor_id');
      $this->db->join('favorite','favorite.fk_user_id ='.$data['user_id']);
      $query2 = $this->db->get_compiled_select();
      $query = $this->db->query($query1." UNION ".$query2); 
    }else{
      $query = $this->db->query($query1); 
    }
    if($query->num_rows() > 0 ){
      $res =  $query->result_array();
      return $res;
    }else{
      return false;
    }
  }
  #this function is used for get customer info with menu details based on different item
  public function listMap($data){
    $this->db->select('customer.serial_number as customer_id,customer.name as title, customer.category_id, customer.latitude,customer.longitude, menu.title,menu.filters,menu.menu_id');
    $this->db->from('menu');
    $this->db->like('title',$data['keywords'], 'both');
    if(isset($data['category_per_filter']) && !empty($data['category_per_filter']) && count($data['category_per_filter'])>0){
      foreach ($data['category_per_filter'] as $key => $menu_category_id) {
        $this->db->or_where('menu.filters like' ,'%"'.$menu_category_id.'"%');
      }
    }
    $this->db->join('customer', 'customer.serial_number = menu.vendor_id');
    $this->db->where('customer.category_id',$data['category']);
    $query1 = $this->db->get_compiled_select();

    if(isset($data['my_favorite']) && !empty($data['my_favorite']) && !empty($data['user_id'])){
      $this->db->select('customer.serial_number as customer_id,customer.name as title, customer.category_id, customer.latitude,customer.longitude, menu.title,menu.filters');
      $this->db->from('menu');
      $this->db->join('customer', 'customer.serial_number = menu.vendor_id');
      $this->db->join('favorite','favorite.fk_user_id ='.$data['user_id']);
      $query2 = $this->db->get_compiled_select();
      $query = $this->db->query($query1." UNION ".$query2); 
    }else{
      $query = $this->db->query($query1); 
    }
    if($query->num_rows() > 0 ){
      $res =  $query->result_array();
      return $res;
    }else{
      return false;
    }
  }
  #this function is used for return customer info by customer id, lat,lon
  public function getCustomerDetailsById($customer_id, $latitude, $longitude){
    $kilo_m = 6371;
    $miles = 3959;
    $this->db->select('c.serial_number as customer_id, c.name as customer_name, c.description, c.phone_number,c.address, c.latitude, c.longitude,( '.$kilo_m.' * acos( cos( radians('.$latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( latitude ) ) ) ) AS distance');
    $this->db->from('customer as c');
    $this->db->where('c.serial_number', $customer_id);
    $this->db->join('users','users.serial_number=c.fk_user_id');
    $query = $this->db->get();
    if($query->num_rows() >0 ){
      $res =  $query->result_array();
      $customer = array();
      foreach ($res as $key => $value) {
        $customer[$key] = $value;
      }
      return $customer;
    }else{
      return false;
    }
  }
  #this function is used for return menu item details by customer id
  public function getCustomerMenusItemsById($customer_id){
    $this->db->select('menu.menu_id, menu.title, menu.category_id, menu.keywords, menu.filters, menu.image_name, menu.image_path,menu.description,customer.category_id as customer_category_id, customer.name as company_name,customer.serial_number as company_id');
    $this->db->from('menu');
    $this->db->where('vendor_id',$customer_id);
    $this->db->join('customer','customer.serial_number = menu.vendor_id');
    $this->db->order_by('menu.category_id','asend');
    $query = $this->db->get();
    if($query->num_rows() >0){
      $result =  $query->result_array();
      return $result;
    }else{
      return false;
    }
  }
  #This function is used for get all customer image list based on customer id
  public function get_customer_images_by_id($id){
    $this->db->select('*');
    $this->db->from('customer_image');
    $this->db->where('fk_customer_id',$id);
    $query = $this->db->get();
    if($query->num_rows() >0){
      $result =  $query->result_array();
      $images = array();
      foreach ($result as $key => $value) {
        $image_path=array('image_path'=> $value['image_path']);
        $images[$key] =$image_path;
      }
      return $images;
    }else{
      return false;
    }

  }
   #This function is used for get all customer rating list based on customer id
  public function get_customer_rating_by_id($id){
    $this->db->select('*');
    $this->db->from('rating_comment');
    $this->db->where('fk_customer_id',$id);
    $query = $this->db->get();
    if($query->num_rows() >0){
      $result =  $query->result_array();
      $rating = array();
      foreach ($result as $key => $value) {
        $rating[] = $value['rating'];
      }
      return $rating;
    }else{
      return false;
    }

  }
   #This function is used for get all customer comments list based on customer id
  public function get_customer_comment_by_id($id){
    $this->db->select('*');
    $this->db->from('rating_comment');
    $this->db->where('fk_customer_id',$id);
    $query = $this->db->get();
    if($query->num_rows() >0){
      $result =  $query->result_array();
      $comment = array();
      foreach ($result as $key => $value) {
         if(isset($value['fk_user_id']) && !empty($value['fk_user_id'])){
          $app_users = $this->get_user_by_id($value['fk_user_id']);
          $comment[$key]['name'] = $app_users[0]['name'];
          $comment[$key]['profile_image'] = $app_users[0]['profile_image']?$app_users[0]['profile_image']:'assets/img/user-medium.png';
          $comment[$key]['comment'] = $value['comment'];
          $comment[$key]['rating'] = $value['rating'];
         } 
      }
      return $comment;
    }else{
      return 0;
    }

  }
  #this function is used for get all favorite customer list
  public function get_all_favorite_customer_by_user_id($user_id){
    $favorite_customer_ids = array();
    $this->db->select('fk_customer_id as favorite_customer_id');
    $this->db->from('favorite');
    $this->db->where('fk_user_id',$user_id);
    $query = $this->db->get();
    if($query->num_rows() >0){
      $result =  $query->result_array();
      foreach ($result as $key => $value) {
        $favorite_customer_ids[$key] = $value['favorite_customer_id'];
      }
      return $favorite_customer_ids;
    }else{
      return false;
    }
  }
  #this function is used for get all favorite customer list 
  public function get_all_favorites_by_user_id_and_customer_id($user_id, $customer_id){
    $favorite_customer_ids = array();
    $this->db->select('fk_customer_id as favorite_customer_id');
    $this->db->from('favorite');
    $this->db->where('fk_user_id',$user_id);
    $this->db->where('fk_customer_id',$customer_id);
    $query = $this->db->get();
    if($query->num_rows() >0){
      $result =  $query->result_array();
      foreach ($result as $key => $value) {
        $favorite_customer_ids[$key] = $value['favorite_customer_id'];
      }
      return $favorite_customer_ids;
    }else{
      return false;
    }
  }
  #This function is used for get pages content based on page name
  Public function get_page_content($type){
    if($type == 'about'){
      $table = 'about';
    }else if($type == 'privacy'){
      $table = 'privacy';
    }else if($type == 'terms'){
      $table = 'terms_conditions';
    }
    $this->db->select('*');
    $this->db->from($table);
    $query = $this->db->get();
    if($query->num_rows() >0){
      $result =  $query->result_array();
      $content = array();
      foreach ($result as $key => $value) {
         $content['title'] = $value['title'];
         $content['content'] = $value['description'];
      }
      return $content;
    }else{
      return false;
    }
  }
  #This function is used for checking user is rated to customer or not
  public function checkUserRating($user_id, $customer_id){
    $this->db->select('*');
    $this->db->from('rating_comment');
    $this->db->where('fk_user_id',$user_id);
    $this->db->where('fk_customer_id',$customer_id);
    $query = $this->db->get();
    if($query->num_rows() >0){
      return true;
    }else{
      return false;
    }

  }
  #this function is used for check email id is register or not
  public function checkEmail($email){
    $this->db->select('email_id');
    $this->db->from('app_users');
    $this->db->where('email_id',$email);
    $query = $this->db->get();
    if($query->num_rows() >0){
      return true;
    }else{
      return false;
    }

  }
  #this function is used for check given user id is exists or not
  public function checkUserId($user_id){
    $this->db->select('user_id');
    $this->db->from('app_users');
    $this->db->where('user_id',$user_id);
    $query = $this->db->get();
    if($query->num_rows() >0){
      return true;
    }else{
      return false;
    }

  }
   #this function is used for check given customer id is exists or not
  public function checkCustomerId($customer_id){
    $this->db->select('serial_number');
    $this->db->from('customer');
    $this->db->where('serial_number',$customer_id);
    $query = $this->db->get();
    if($query->num_rows() >0){
      return true;
    }else{
      return false;
    }

  }
  #this function is used for check given password is correct or not.
  public function password_match_by_id($user_id,$old_password){
    $this->db->select('email_id, password');
    $this->db->from('app_users');
    $this->db->where('user_id', $user_id);
    $query = $this->db->get();
    if($query->num_rows() >0 ){
      $res =  $query->result_array();
      if($res[0]['password'] == md5($old_password)){
        return true;
      }else{
        return false;
      }
      exit;
    }else{
      return false;
    }
  }
  #This function is used for get app_user by user id
  public function get_user_by_id($user_id){
    $this->db->select('*');
    $this->db->from('app_users');
    $this->db->where('user_id', $user_id);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }
  #this function is used for get token from app_user table by email
  public function get_token($email){
    $this->db->select('token');
    $this->db->from('app_users');
    $this->db->where('email_id',$email);
    $query = $this->db->get();
    if($query->num_rows() >0){
          $result = $query->result_array();
          return $result[0]['token'];
    }else{
          return false;
    }
  }
  #this function is used for get all keyword by customer id.
  public function get_keywords_name_by_customer_id($customer_id){
    $this->db->select('keywords');
    $this->db->from('menu');
    $this->db->where('vendor_id',$customer_id);
    $query = $this->db->get();
    if($query->num_rows() >0){
      $result= $query->result_array();
      $keywords_data=array();
      foreach ($result as $key => $value_outer) {
        foreach ($value_outer as $key => $value_inner) {
          $array_data = json_decode($value_inner);
          if(!empty($array_data) && count($array_data) >0){
            foreach($array_data as $key=>$data){
              $keyword_name = $this->get_keywords_name_by_id($data);
              if(!in_array($keyword_name, $keywords_data)){
                $keywords_data[]=$keyword_name;
              }            
            }
          }
        }
      }
      return $keywords_data;
    }else{
      return false;
    }
  }
  #this function is used for get keywords name by keywords id;
  public function get_keywords_name_by_id($keyword_id){
    $this->db->select('keyword_name');
    $this->db->from('keywords');
    $this->db->where('keywords_id',$keyword_id);
    $query = $this->db->get();
    if($query->num_rows() >0){
      $result= $query->result_array();
      return $result[0]['keyword_name'];
    }else{
      return false;
    }
  }
  #this function is used for send notification to users.
  public function notify($data){
    if($this->db->insert('notification', $data)){
        return true;
    }else{
        return false;
    }
  }
  #this function is used for get admin id
  public function get_admin_id(){
    $this->db->select('serial_number,email_address');
    $this->db->from('users');
    $this->db->where('fk_role_id',1);
    $query = $this->db->get();
    if($query->num_rows()){
     $result = $query->result_array();
     return $result[0]['serial_number'];
    }else{
      return 0;
    }
  }
  public function check_user_type1($user_id){
    $this->db->select('*');
    $this->db->from('users');
    $this->db->where('serial_number',$user_id);
    $query = $this->db->get();
    if($query->num_rows() >0){
      return true;
    }else{
      return false;
    }
  }
  public function check_user_type2($customer_id){
    $this->db->select('*');
    $this->db->from('customer');
    $this->db->where('serial_number',$customer_id);
    $query = $this->db->get();
    if($query->num_rows() >0){
      return true;
    }else{
      return false;
    }
  }
  public function check_user_type3($user_id){
    $this->db->select('*');
    $this->db->from('app_users');
    $this->db->where('user_id',$user_id);
    $query = $this->db->get();
    if($query->num_rows() >0){
      return true;
    }else{
      return false;
    }
  }
  #this function is used for get all category list  
  public function get_category_list(){
    $this->db->select('category_id,name');
    $this->db->from('category');
    $query = $this->db->get();
    if($query->num_rows() >0){
      $result = $query->result_array();
      return $result;
    }else{
      return false;
    }
  }
  #This function is used for get customer category name by category id.
  public function get_cat_name_by_id($id){
    $this->db->select('name');
    $this->db->from('category');
    $this->db->where('category_id',$id);
    $query = $this->db->get();
    if($query->num_rows()){
      $result =  $query->result_array();
      return $result[0]['name'];
    }else{
      return false;
    }
  }
  public function get_without_login_list_view($category_id,$latitude, $longitude){
    $kilo_m = 6371;
    $miles = 3959;
    $this->db->select('c.serial_number as id, c.name as company_name,c.short_description, customer_image.image_path,avg(cm.rating) as rating,( '.$kilo_m.' * acos( cos( radians('.$latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( latitude ) ) ) ) AS distance');
    $this->db->from('customer as c');
    $this->db->where('category_id', $category_id);
    //$this->db->having('distance <=','25');
    $this->db->join('customer_image','c.serial_number = customer_image.fk_customer_id');
    $this->db->join('rating_comment as cm','c.serial_number = cm.fk_customer_id','left');
    $this->db->group_by('c.serial_number');
    $this->db->order_by('distance','asend');
    $query = $this->db->get();
    if($query->num_rows() >0){
      $result = $query->result_array();
      return $result;
    }else{
      return false;
    }
  }
  public function get_without_login_map_view($category_id, $latitude='', $longitude=''){
    $kilo_m = 6371;
    $miles = 3959;
    if($latitude==''){
      $latitude = 28.58;
    }
    if($latitude==''){
      $longitude = 77.33;
    }
    $this->db->select('c.name as company_name,c.latitude, c.longitude, c.duty_status, ( '.$kilo_m.' * acos( cos( radians('.$latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( latitude ) ) ) ) AS distance');
    $this->db->from('customer as c');
    $this->db->where('category_id', $category_id);
    //$this->db->having('distance <=','25');
    $query = $this->db->get();
    if($query->num_rows() >0){
      $result = $query->result_array();
      return $result;
    }else{
      return false;
    }
  }
  #This function is used for return avg rating of customer
  public function get_avg_rating_by_customer_id($id){
    $this->db->select_avg('rating','rating');
    $this->db->from('rating_comment');
    $this->db->where('fk_customer_id',$id);
    $query = $this->db->get();
    if($query->num_rows() >0){
      $result = $query->result_array();
      return $result[0]['rating']?number_format($result[0]['rating'],2):0;    
    }else{
      return false;
    }
  }
  #this function is used for check app device token is exist or not
  public function check_device_token($token){
    $this->db->select('*');
    $this->db->from('app_device');
    $this->db->where('token', $token);
    $query = $this->db->get();
    if($query->num_rows() >0){
      return true;
    }else{
      return false;
    }
  }
  #this function is used for insert device token into app_device table
  public function insert_device_token($data){
    if($this->db->insert('app_device', $data)){
        return true;
    }else{
      return false;
    }
  }
  public function check_device_token_by_user_id($id){
    $this->db->select('user_id');
    $this->db->from('app_device');
    $this->db->where('user_id', $id);
    $query = $this->db->get();
    if($query->num_rows() >0){
      return true;
    }else{
      return false;
    }
  }
  #this function is used for updat updated_at in  app_device table
  public function update_device_token($data){
    $this->db->set('updated_at', date('y-m-d h:i:sa'));
    $this->db->set('token', $data['token']);
    $this->db->set('device_name', $data['device_name']);
    $this->db->where('user_id', $data['user_id']);
    if($this->db->update('app_device')){
      return true;
    }else{
      return false;
    }
  }
  #this function is used for get menu by vendor id 
  public function get_menu_group_by_cat_by_vendor_id($vendor_id){
    $this->db->select('*');
    $this->db->from('menu');
    $this->db->where('vendor_id',$vendor_id);
    $this->db->order_by('category_id');
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }
  #this function is used for get filter list by category id
  public function get_filter_list_by_category_id($category_id){
    $this->db->select('serial_number as id, title, filter');
    $this->db->from('filters');
    $this->db->where('category_id',$category_id);
    //$this->db->order_by('category_id');
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }
  #this function is used for get keyword list based on keyword name and category id.
  public function get_keyword_ids_by_search_keyword_name($search_keyword, $category_id){
    $this->db->select('keywords_id');
    $this->db->from('keywords');
    $this->db->like('keyword_name',$search_keyword, 'both');;
    $query = $this->db->get();
    $data = array();
    if($query->num_rows()){
      $res = $query->result_array();
      foreach ($res as $key => $value) {
        $data[]= $value['keywords_id'];
      }
      return $data;
    }else{
      return false;
    }
  }
  #This function is used for get customer details based on filters
  public function filter($data){
    $kilo_m = 6371;
    $miles = 3959;
    $search_keywords_ids ='';
    $favorite_customer_ids = $this->get_all_favorite_customer_by_user_id($data['user_id']);
    /*if(isset($data['search_keyword']) && !empty($data['search_keyword']) && count($data['search_keyword'])>0){
      $search_keywords_ids = $this->get_keyword_ids_by_search_keyword_name($data['search_keyword'], $data['category_id']);
      $this->db->select('c.serial_number as id, c.name as company_name,c.short_description, customer_image.image_path,avg(cm.rating) as rating,c.filter_id,c.category_id,( '.$kilo_m.' * acos( cos( radians('.$data["latitude"].') ) * cos( radians( latitude ) ) * cos( radians( longitude) - radians('.$data["longitude"].') ) + sin( radians('.$data["latitude"].') ) * sin( radians( latitude ) ) ) ) AS distance');
    }else{
      $this->db->select('c.serial_number as id, c.name as company_name,c.short_description, customer_image.image_path,avg(cm.rating) as rating,c.filter_id,c.category_id, ( '.$kilo_m.' * acos( cos( radians('.$data["latitude"].') ) * cos( radians( latitude ) ) * cos( radians( longitude) - radians('.$data["longitude"].') ) + sin( radians('.$data["latitude"].') ) * sin( radians( latitude ) ) ) ) AS distance');
    }*/
    $this->db->select('c.serial_number as id, c.name as company_name,c.short_description, customer_image.image_path,avg(cm.rating) as rating, ( '.$kilo_m.' * acos( cos( radians('.$data["latitude"].') ) * cos( radians( latitude ) ) * cos( radians( longitude) - radians('.$data["longitude"].') ) + sin( radians('.$data["latitude"].') ) * sin( radians( latitude ) ) ) ) AS distance');
    $this->db->from('customer as c');
    $this->db->where('c.category_id', $data['category_id']);
    //$this->db->having('distance <=','25');
    $this->db->join('customer_image','c.serial_number = customer_image.fk_customer_id');
    $this->db->join('rating_comment as cm','c.serial_number = cm.fk_customer_id','left');
    
    if(isset($data['search_filters']) && !empty($data['search_filters']) && count($data['search_filters'])>0){
      $where_status = 1;
      foreach ($data['search_filters'] as $key => $filter_id) {
        if($where_status){
          $this->db->where('c.filter_id like' ,'%"'.$filter_id.'"%');
          $where_status = 0;
        }else{
           $this->db->or_where('c.filter_id like' ,'%"'.$filter_id.'"%');
        }
      }
    }
    if(isset($search_keywords_ids) && !empty($search_keywords_ids) && count($search_keywords_ids)>0){
      $this->db->join('menu','menu.vendor_id = c.serial_number');
      $where_status = 1;
      foreach ($search_keywords_ids as $key => $keyword_id) {
        if($where_status){
          $this->db->where('menu.keywords like' ,'%"'.$keyword_id.'"%');
          $where_status = 0;
        }else{
          $this->db->or_where('menu.keywords like' ,'%"'.$keyword_id.'"%');
        }
      }
    }
    if(isset($data['my_favorites']) && !empty($data['my_favorites']) && $data['my_favorites'] =='yes' && !empty($data['user_id']) && !empty($favorite_customer_ids)){
      $this->db->join('favorite','c.serial_number = favorite.fk_customer_id');
      $this->db->where('favorite.fk_user_id' ,$data['user_id']);
    }
    $this->db->group_by('c.serial_number');
    $this->db->order_by('distance','asend');
    $query = $this->db->get();
    if($query->num_rows() >0){
      $result = $query->result_array();
      return $result;
    }else{
      return false;
    }
  }
  #this function is used for add favorites or update favorites.
  public function add_to_favorites($user_id, $customer_id, $my_favorites){
      $data['my_favorites'] = $my_favorites;
      $data['fk_user_id'] = $user_id;
      $data['fk_customer_id'] = $customer_id;
      $data['creation_date'] = date('y-m-d h:i:sa');
      $data['last_updated_date'] = date('y-m-d h:i:sa');
      if($this->db->insert('favorite', $data)){
        return true;
      }else{
        return false;
      }
  }

  //dataexist at add
   public function check_category_exits($name){
    $this->db->select('*');
    $this->db->from('category');
    $this->db->where('name', $name);
    $query = $this->db->get();
    if($query->num_rows()){
      return true;
    }else{
      return false;
    }
  }

  //data exist
  public function check_category_for_edit($category_id, $name){
    $this->db->select('*');
    $this->db->from('category');
    $this->db->where('name',$name);
    $this->db->where('category_id !=',$category_id);
    $query = $this->db->get();
    if($query->num_rows()){
      return true;
    }else{
      return false;
    }
  }
  #this function is used for remove user from my favorites list.
  public function remove_from_favorites($user_id, $customer_id, $my_favorites){
    $this->db->set('my_favorites', $my_favorites);
    $this->db->set('last_updated_date', date('y-m-d h:i:sa'));
    $this->db->where('fk_customer_id', $customer_id);
    $this->db->where('fk_user_id', $user_id);
    if($this->db->update('favorite')){
      return true;
    }else{
      return false;
    }
  }
  #this function is used for delete all rating by app_user_id 17-09-2018
  public function delete_all_rating_by_app_user_id($id){
    $this->db->where('fk_user_id', $id);
    if($this->db->delete('rating_comment')){
        return true;
    }else{
      return false;
    }
  }
}
  

?>