<?php 
class Notification_model extends CI_Model {

  public function add($data){
      if($this->db->insert('notification', $data)){
        return true;
      }else{
        return false;
      }
  }
  public function get_all_unread_notification(){
    $this->db->select('*');
    $this->db->from('notification');
    $this->db->where('read_status',0);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }
  public function get_all_notification($recipient_id,$recipient_type){
    $this->db->select('*');
    $this->db->from('notification');
    $this->db->where('recipient_id',$recipient_id);
    $this->db->where('recipient_type',$recipient_type);
    $this->db->where('delete_status',0);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }
  public function get_all_trash_notification($recipient_id,$recipient_type){
    $this->db->select('*');
    $this->db->from('notification');
    $this->db->where('recipient_id',$recipient_id);
    $this->db->where('recipient_type',$recipient_type);
    $this->db->where('delete_status',2);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }
  public function get_all_read_notification(){
    $this->db->select('*');
    $this->db->from('notification');
    $this->db->where('read_status',1);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }
  public function get_all_unread_notification_by_recipient_id($recipient_id,$recipient_type){
    $this->db->select('*');
    $this->db->from('notification');
    $this->db->where('read_status',0);
    $this->db->where('recipient_id',$recipient_id);
    $this->db->where('recipient_type',$recipient_type);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }
  public function get_admin_id(){
    $this->db->select('serial_number,email_address');
    $this->db->from('users');
    $this->db->where('fk_role_id',1);
    $query = $this->db->get();
    if($query->num_rows()){
     $result = $query->result_array();
     return $result[0]['serial_number'];
    }else{
      return 0;
    }
  }
  public function updateReadStatus($notification_id){
    $this->db->set('read_status', 1);
    $this->db->where('notification_id', $notification_id);
    if($this->db->update('notification')){
      return true;
    }else{
      return false;
    }
  }
}

?>