<?php 
class Keyword_model extends CI_Model {

  public function add_keyword($data){
      if($this->db->insert('keywords', $data)){
        return true;
      }else{
        return false;
      }
  }

  public function get_all_keyword(){
    $this->db->select('*');
    $this->db->from('keywords');
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }

  public function check_keyword_by_category($category_id, $keyword){
    $this->db->select('*');
    $this->db->from('keywords');
    $this->db->where('category_id',$category_id);
    $this->db->where('keyword_name',$keyword);
    $query = $this->db->get();
    if($query->num_rows()){
      return true;
    }else{
      return false;
    }
  }

  public function check_keyword_for_edit($category_id, $keyword, $keywords_id){
    $this->db->select('*');
    $this->db->from('keywords');
    $this->db->where('category_id',$category_id);
    $this->db->where('keyword_name',$keyword);
    $this->db->where('keywords_id !=',$keywords_id);
    $query = $this->db->get();
    if($query->num_rows()){
      return true;
    }else{
      return false;
    }
  }

  public function get_keyword_by_id($id){
    $this->db->select('*');
    $this->db->from('keywords');
    $this->db->where('keywords_id',$id);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }
  public function get_keywords_name_by_id($keyword_id){
    if($this->session->userdata('site_lang') == 'italian'){
       $this->db->select('keyword_name_italian as keyword_name');
    }else{

       $this->db->select('keyword_name');
    }
    //$this->db->select('keyword_name');
    $this->db->from('keywords');
    $this->db->where('keywords_id',$keyword_id);
    $query = $this->db->get();
    if($query->num_rows() >0){
      $result= $query->result_array();
      return $result[0]['keyword_name'];
    }else{
      return false;
    }
  }
  public function get_keywords_name_by_customer_id($customer_id){
    $this->db->select('keywords');
    $this->db->from('menu');
    $this->db->where('vendor_id',$customer_id);
    $query = $this->db->get();
    if($query->num_rows() >0){
      $result= $query->result_array();
      $keywords_data=array();
      foreach ($result as $key => $value_outer) {
        foreach ($value_outer as $key => $value_inner) {
          $array_data = json_decode($value_inner);
          if(!empty($array_data) && count($array_data) >0){
            foreach($array_data as $key=>$data){
              $keyword_name = $this->get_keywords_name_by_id($data);
              if(!in_array($keyword_name, $keywords_data)){
                $keywords_data[]=$keyword_name;
              }            
            }
          }
        }
      }
      return $keywords_data;
    }else{
      return false;
    }
  }

  public function update_keyword($data){
    $this->db->set('keyword_name', $data['name']);
    $this->db->set('keyword_name_italian', $data['name_italian']);
    $this->db->set('category_id', $data['category_id']);
    $this->db->set('last_updated_by', $data['last_update_by']);
    $this->db->set('updated_date', $data['last_updated_date']);
    $this->db->where('keywords_id', $data['keywords_id']);
    if($this->db->update('keywords')){
      return true;
    }else{
      return false;
    }
  }
  public function get_all_keyword_by_category_id($id){
    $this->db->select('*');
    $this->db->from('keywords');
    $this->db->where('category_id',$id);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }

}

?>