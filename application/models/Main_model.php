<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function __callSelect($tbl, $choice=NULL)
	{
		$this->db->from($tbl);
		if($choice != NULL)
		{
			if(array_key_exists('where',$choice))
			{
				$w = $choice['where'];
				//print_r($choice['where']); die;
				foreach($w as $k => $v)
				{
					$this->db->where($k,$v);  
				}
			}
		}
			
		
		return $query=$this->db->get();  
	}

	function __callSelectWithFields($tbl, $field=NULL, $choice=NULL)
	{
		$this->db->from ($tbl);
		if($field != NULL)
		{
			$this->db->select($field);
		}
		if($choice != NULL)
		{
			if(array_key_exists('where',$choice))
			{
				$w = $choice['where'];
				//print_r($choice['where']); die;
				foreach($w as $k => $v)
				{
					$this->db->where($k,$v);  
				}
			}
		}
			
		
		return $query=$this->db->get();  
	}
	
	// Insert Record Funtion
	function __callInsert($tbl , $record)
	{
		$this->db->insert($tbl, $record);
		return $this->db->insert_id();
	}
	
	function __callUpdate($tbl, $choice, $record)
	{
		$this->db->where($choice);  
		$this->db->update($tbl, $record);
		return $this->db->affected_rows();
	}
	function __callMultiupdate($tbl,$fid,$choice, $record)
	{
		$this->db->where_in($fid,$choice);
		$this->db->update($tbl, $record);
		return $this->db->affected_rows();
	}	
	
	function __callDelete($tbl, $choice)
	{
		$this->db->where($choice);  
		$this->db->delete($tbl);
		return $this->db->affected_rows();
	}
	function __callMultidelete($tbl,$fid,$choice)
	{
		$this->db->where_in($fid,$choice);
		$this->db->delete($tbl);
		return $this->db->affected_rows();
	}
	
	function __callMasterquery($query)
	{
		$result = $this->db->query($query);
		return $result;
	}

	function __callJoin($tbl1,$tbl1_fld,$tbl2,$tbl2_fld,$choice=NULL,$field=NULL,$order)	
	{
		$this->db->select($field);
		$this->db->order_by($order);    
		$this->db->from($tbl1);
		$this->db->join($tbl2, $tbl1.'.'.$tbl1_fld.' = '.$tbl2.'.'.$tbl2_fld);
		
		
		if($choice != NULL)
		{
			if(array_key_exists('where',$choice))
			{
				$w = $choice['where'];
				//print_r($choice['where']); die;
				foreach($w as $k => $v)
				{
					$this->db->where($k,$v);  
				}
			}
		}
	
		return $query = $this->db->get();
	}
	
	function __callView($tbl,$choice=NULL)	
	{
		$this->db->select('*');    
		$this->db->from($tbl);
		//$this->db->join($tbl2, $tbl1.'.'.$tbl1_fld.' = '.$tbl2.'.'.$tbl2_fld);
		if($choice != NULL)
		{
			if(array_key_exists('where',$choice))
			{
				$w = $choice['where'];
				//print_r($choice['where']); die;
				foreach($w as $k => $v)
				{
					$this->db->where($k,$v);  
				}
			}
		}
		return $query = $this->db->get();
	}

	//join with limit
	function __calllimitJoin($tbl1,$tbl1_fld,$tbl2,$tbl2_fld,$choice=NULL,$field=NULL,$order)	
	{
		$this->db->select($field);
		$this->db->order_by($order);    
		$this->db->from($tbl1);
		$this->db->limit(6,0);  
		$this->db->join($tbl2, $tbl1.'.'.$tbl1_fld.' = '.$tbl2.'.'.$tbl2_fld);
		
		
		if($choice != NULL)
		{
			if(array_key_exists('where',$choice))
			{
				$w = $choice['where'];
				//print_r($choice['where']); die;
				foreach($w as $k => $v)
				{
					$this->db->where($k,$v);  
				}
			}
		}
	
		return $query = $this->db->get();
	}
	
//dataexist at add
   public function check_category_exits($table,$name,$catwheres){
    $this->db->select('*');
    $this->db->from($table);
		$this->db->where($name);
		 $this->db->where($catwheres);
    $query = $this->db->get();
    if($query->num_rows()){
      return true;
    }else{
      return false;
    }
  }

  //data exist
  public function check_category_for_edit($table,$wheres,$cat_name){
    $this->db->select('*');
    $this->db->from($table);
    $this->db->where($wheres);
		$this->db->where($cat_name);
    $query = $this->db->get();
    if($query->num_rows()){
      return true;
    }else{
      return false;
    }
  }
	
	public function count_total($table){
    $this->db->select('*');
      $this->db->from($table);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->num_rows();
    }else{
      return 0;
    }
	}
	
	 function get_row($table_name, $select_filds, $where_condition=null, $order = null)
    { 
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($order) {
            $this->db->order_by($order);
        }

        $this->db->from($table_name);
        $this->db->where('deleted_at =', NULL);
        $this->db->where($where_condition);
        $query = $this->db->get();
        $result = $query->row();

        //echo $this->db->last_query();
        return $result;
        //null given if no row found
    }
}