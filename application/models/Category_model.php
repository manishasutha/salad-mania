<?php 
class Category_model extends CI_Model {

  public function add_category($data){
      if($this->db->insert('category', $data)){
        return true;
      }else{
        return false;
      }
  }

  public function get_all_category(){
    $this->db->select('*');
    $this->db->from('category');
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }
  public function cat_num_rows($table){
    $this->db->select('*');
    $this->db->from($table);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->num_rows();
    }else{
      return 0;
    }
  }
  public function get_cat_by_id($id){
    $this->db->select('*');
    $this->db->from('category');
    $this->db->where('category_id',$id);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }
  public function get_cat_name_by_id($id){
    $this->db->select('name');
    $this->db->from('category');
    $this->db->where('category_id',$id);
    $query = $this->db->get();
    if($query->num_rows()){
      $result =  $query->result_array();
      return $result[0]['name'];
    }else{
      return false;
    }
  }
  public function check_category_exits($name){
    $this->db->select('*');
    $this->db->from('category');
    $this->db->where('name', $name);
    $query = $this->db->get();
    if($query->num_rows()){
      return true;
    }else{
      return false;
    }
  }
  public function check_category_for_edit($category_id, $name){
    $this->db->select('*');
    $this->db->from('category');
    $this->db->where('name',$name);
    $this->db->where('category_id !=',$category_id);
    $query = $this->db->get();
    if($query->num_rows()){
      return true;
    }else{
      return false;
    }
  }
  public function update_category($data){
    $this->db->set('name', $data['name']);
    $this->db->set('name_italian', $data['name_italian']);
    $this->db->set('description', $data['description']);
    $this->db->set('last_update_by', $data['last_update_by']);
    $this->db->set('last_updated_date', $data['last_updated_date']);
    $this->db->where('category_id', $data['cat_id']);
    if($this->db->update('category')){
      return true;
    }else{
      return false;
    }
  }
}

?>