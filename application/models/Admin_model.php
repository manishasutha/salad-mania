<?php 
class Admin_model extends CI_Model {

  public function count_total($table){
    $this->db->select('*');
      $this->db->from($table);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->num_rows();
    }else{
      return 0;
    }
  }
  public function get_new_app_user(){
    $this->db->select('*');
    $this->db->from('app_users');
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->num_rows();
    }else{
      return 0;
    }
  }
  public function get_recent_users($table){
    $this->db->order_by('creation_date','desc');
    $this->db->select('*');
    $this->db->from($table);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return 0;
    }
  }
  public function get_email_setting(){
    $this->db->select('*');
    $this->db->from('email_setting');
    $query = $this->db->get();
    if($query->num_rows()){
       return $query->result_array();
    }else{
      return 0;
    }
  }
  public function add_email_setting($data){
    if($this->db->insert('email_setting', $data)){
      return true;
    }else{
      return false;
    }
  }
  public function update_email_setting($data, $id){
    $this->db->set('type', $data['type']);
    $this->db->set('smtp_port', $data['smtp_port']);
    $this->db->set('smtp_host', $data['smtp_host']);
    $this->db->set('smtp_user', $data['smtp_user']);
    $this->db->set('smtp_password', $data['smtp_password']);
    $this->db->where('id', $id);
    if($this->db->update('email_setting')){
      return true;
    }else{
      return false;
    }
  }

}

?>