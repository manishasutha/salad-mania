<?php 
class Customer_model extends CI_Model {


  public function get_all_customer(){
    $this->db->select('users.email_address,users.first_name as name,customer.serial_number,customer. identification_number,customer.phone_number, customer.category_id, customer.status, customer.duty_status, customer.name as company,customer.city');
    $this->db->from('customer');
    $this->db->join('users', 'users.serial_number = customer.fk_user_id');
    $this->db->where('delete_status',0);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
    //$last_id = $this->db->insert_id();
  }
  public function get_all_pending_customer(){
    $this->db->select('users.email_address,users.first_name as name,customer.serial_number,customer. identification_number,customer.phone_number, customer.category_id, customer.status, customer.duty_status, customer.name as company');
    $this->db->from('customer');
    $this->db->join('users', 'users.serial_number = customer.fk_user_id');
    $this->db->where('customer.status','3');
    //$this->db->or_where('customer.status','3');
    $this->db->where('delete_status',0);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
    //$last_id = $this->db->insert_id();
  }
  public function add_user($user_data){
      if($this->db->insert('users', $user_data)){
        $last_id = $this->db->insert_id();
        return $last_id;
      }else{
        return false;
      }
  }

  public function add_customer($customer_data){
      if($this->db->insert('customer', $customer_data)){
        $last_id = $this->db->insert_id();
        return $last_id;
      }else{
        return false;
      }
  }

  public function add_customer_image($customer_image_data){
      if($this->db->insert('customer_image', $customer_image_data)){
        return true;
      }else{
        return false;
      }
  }
  public function get_customer_by_id($id){
    $this->db->select('*');
    $this->db->from('customer');
    $this->db->where('serial_number',$id);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }

  public function get_customer_name_by_id($id){
    $this->db->select('name, delete_status');
    $this->db->from('customer');
    $this->db->where('serial_number',$id);
    $query = $this->db->get();
    if($query->num_rows()){
      $result =  $query->result_array();
      return $result[0]['name'];
    }else{
      return false;
    }
  }
  public function get_product_cat_by_customer_id($id){
    $this->db->select('category_id');
    $this->db->from('customer');
    $this->db->where('serial_number',$id);
    $query = $this->db->get();
    if($query->num_rows()){
      $result =  $query->result_array();
      return $result[0]['category_id'];
    }else{
      return false;
    }
  }
  public function count_total_rating__by_customer_id($id){
    $this->db->select('*');
    $this->db->from('rating_comment');
    $this->db->where('fk_customer_id',$id);
    $this->db->where('rating !=', 0);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->num_rows();
    }else{
      return false;
    }
  }
  public function count_total_menu__by_customer_id($id){
    $this->db->select('*');
    $this->db->from('menu');
    $this->db->where('vendor_id',$id);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->num_rows();
    }else{
      return false;
    }
  }
   public function count_total_favorite_by_customer_id($id){
    $this->db->select('*');
    $this->db->from('favorite');
    $this->db->where('fk_customer_id',$id);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->num_rows();
    }else{
      return false;
    }
  }
  public function get_user_by_id($id){
    $this->db->select('*');
    $this->db->from('users');
    $this->db->where('serial_number',$id);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }
  public function get_customer_image_by_customer_id($id){
    $this->db->select('*');
    $this->db->from('customer_image');
    $this->db->where('fk_customer_id',$id);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }
  public function get_customer_by_fk_user_id($id){
    $this->db->select('*');
    $this->db->from('customer');
    $this->db->where('fk_user_id',$id);
    $this->db->where('delete_status',0);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }
  public function update_user($data, $user_id){
    $this->db->set('first_name', $data['first_name']);
    $this->db->set('last_name', $data['last_name']);
    if(!empty($data['password'])){
       $this->db->set('password', md5($data['password']));
    }
    if(!empty($data['email_address'])){
       $this->db->set('email_address', $data['email_address']);
    }
    $this->db->set('contact_number', $data['contact_number']);
    $this->db->set('profile_image', $data['profile_image']);
    $this->db->set('creation_date', $data['creation_date']);
    $this->db->set('last_updated_date', $data['last_updated_date']);
    $this->db->where('serial_number', $user_id);
    if($this->db->update('users')){
      return true;
    }else{
      return false;
    }
  }
  public function update_customer($data, $customer_id){
    $this->db->set('identification_number', $data['identification_number']);
    $this->db->set('name', $data['name']);
    $this->db->set('name_italian', $data['name_italian']);
    $this->db->set('description', $data['description']);
    $this->db->set('description_italian', $data['description_italian']);
    $this->db->set('short_description', $data['short_description']);
    $this->db->set('short_description_italian', $data['short_description_italian']);
    $this->db->set('sim_number', $data['sim_number']);
    $this->db->set('imei_number', $data['imei_number']);
    $this->db->set('address', $data['address']);
    $this->db->set('latitude', $data['latitude']);
    $this->db->set('longitude', $data['longitude']);
    $this->db->set('phone_number', $data['phone_number']);
    $this->db->set('country', $data['country']);
    $this->db->set('state', $data['state']);
     $this->db->set('city', $data['city']);
    $this->db->set('status', 3);
    if(!empty($data['category_id'])){
      $this->db->set('category_id', $data['category_id']);
    }
    if(!empty($data['filter_id'])){
      $this->db->set('filter_id', $data['filter_id']);
    }
    $this->db->set('last_updated_by', $data['last_updated_by']);
    $this->db->where('serial_number', $customer_id);
    if($this->db->update('customer')){
      return true;
    }else{
      return false;
    }
  }
  public function update_customer_image($data, $customer_image_id){
    $this->db->set('image_name', $data['image_name']);
    $this->db->set('image_path', $data['image_path']);
    $this->db->set('last_update_date', $data['last_update_date']);
    $this->db->set('last_updated_by', $data['last_updated_by']);
    $this->db->where('customer_image_id', $customer_image_id);
    if($this->db->update('customer_image')){
      return true;
    }else{
      return false;
    }
  }
  function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
  }
  
}

?>