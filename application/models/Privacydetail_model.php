<?php 
class Privacydetail_model extends CI_Model {

  public function get_all(){
    $this->db->select('*');
    $this->db->from('privacy_details');
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }
  public function get_privacy_detail_by_id($id){
    $this->db->select('*');
    $this->db->from('privacy_details');
    $this->db->where('id',$id);
    $query = $this->db->get();
    if($query->num_rows()){
      return $query->result_array();
    }else{
      return false;
    }
  }
  
}

?>