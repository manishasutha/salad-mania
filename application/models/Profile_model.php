<?php 
class Profile_model extends CI_Model {

      public function login($email,$password){
      	$this->db->select('*');
      	$this->db->from('sm_admin');
      	$this->db->where('email_address', $email);
		$this->db->where('password', encryptPassword($password));
		// $this->db->where('status', 1);
		$query = $this->db->get();
		if($query->num_rows()){
                  $this->db->set('last_login',date('y-m-d h:i:sa'));
                  $this->db->where('email_address', $email);
                  $this->db->update('sm_admin');
			return $query->result_array();
		}else{
			return false;
		}

      }
      public function checkEmail($email,$id=''){

      	$this->db->select('*');
      	$this->db->from('sm_admin');
      	$this->db->where('email_address',$email);
        if(isset($id) && !empty($id)){
          $this->db->where('serial_number !=',$id);
        }
    		$query = $this->db->get();
    		if($query->num_rows() >0){
    			return true;
    		}else{
    			return false;
    		}
	}
	 public function checkPassword($password,$id=''){
      	$this->db->select('*');
      	$this->db->from('sm_admin');
      	$this->db->where('password',$password);
        if(isset($id) && !empty($id)){
          $this->db->where('serial_number !=',$id);
        }
    		$query = $this->db->get();
    		if($query->num_rows() >0){
    			echo $query->num_rows();
    		}else{
    			echo $query->num_rows();
    		}
      }
      public function get_token($email){
            $this->db->select('token');
            $this->db->from('sm_admin');
            $this->db->where('email_address',$email);
            $query = $this->db->get();
            if($query->num_rows() >0){
                  $result = $query->result_array();
                  return $result[0]['token'];
            }else{
                  return false;
            }

      }
       public function get_user_info_by_customer_id($customer_id){
          $this->db->select('first_name, contact_number');
          $this->db->from('sm_admin');
          $this->db->where('serial_number',$customer_id);
          $query = $this->db->get();
          if($query->num_rows()){
            return $query->result_array();
          }else{
            return 0;
          }
      }
      public function get_user_by_id($user_id){
      	$this->db->select('*');
      	$this->db->from('sm_admin');
      	$this->db->where('serial_number', $user_id);
		$query = $this->db->get();
		if($query->num_rows()){
			return $query->result_array();
		}else{
			return false;
		}
      }
      public function password_match_by_id($user_id,$old_password){
      	$this->db->select('email_address, password');
      	$this->db->from('sm_admin');
      	$this->db->where('serial_number', $user_id);
		$query = $this->db->get();
		if($query->num_rows()){
			$res =  $query->result_array();
			// echo $res[0]['password']."<br>";
			// echo md5($old_password);
			// exit;
			if($res[0]['password'] == md5($old_password)){
				return true;
			}else{
				return false;
			}
			exit;
		}else{
			return false;
		}
      }
      public function change_password_by_id($user_id, $password){
      	$this->db->set('password', md5($password));
      	$this->db->where('serial_number', $user_id);
      	if($this->db->update('sm_admin')){
      		return true;
      	}else{
      		return false;
      	}
      }

      public function update_profile_by_id($data){
      	$this->db->set('first_name', $data['name']);
      	$this->db->set('last_name', $data['surname']);
      	$this->db->set('contact_number', $data['phone']);
            $this->db->set('profile_image', $data['imagepath']);
      	$this->db->where('serial_number', $data['user_id']);
      	if($this->db->update('sm_admin')){
      		return true;
      	}else{
      		return false;
      	}
      }
      public function updated_last_updated_at_in_users($user_id){
            $date = '';
            $this->db->set('last_updated_at', $date);
            $this->db->where('serial_number', $user_id);
            if($this->db->update('sm_admin')){
                  return true;
            }else{
                  return false;
            } 
      }                                    
}

?>