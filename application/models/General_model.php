<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//ticket

class General_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function getGroupByCountRecord($table_name,$group_by, $where_condition = null)
    {
        $this->db->from($table_name);
        if ($where_condition) {
            $this->db->where($where_condition);
        }
        $this->db->group_by($group_by);
        $total_result = $this->db->count_all_results();
        return $total_result;
    }

    /**
     * busmode
     *  this function use for check email already exists in database for register
     * @return query run
     */
    function check_register_email_exists_or_not_web_reg($email)
    {
        $this->db->from('users');
        $this->db->where('email', ($email));
        $email_exists_query = $this->db->get();
        // echo $this->db->last_query();die;
        if ($email_exists_query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    //busmode
    //check email and mobile at time of edit user
    function check_field_exists_or_not($field_name,$value,$user_id)
    {
        $this->db->from('users');
        $this->db->where('id !=',$user_id);
        $this->db->where($field_name, $value);
        $this->db->where('deleted_at =', NULL);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     *  this function use for check email already exists in database
     * @return query run
     */
    function check_register_email_exists_or_not_for_social($email, $login_type = null)
    {

        //$key = 'CtE&*(;N)!@#C78R,.Y12P90T(I45O,.N';
        //header('Content-Type: text/html; charset=utf-8');
        $this->db->from('profile_login');
        $this->db->where('email', salt($email));
//        if ($login_type) {
//            $this->db->where('registration_platform', $login_type);
//        }

        $email_exists_query = $this->db->get();
        //echo $this->db->last_query();
        if ($email_exists_query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function check_register_ip_exists_or_not_for_social($ip, $login_type = null)
    {

        //$key = 'CtE&*(;N)!@#C78R,.Y12P90T(I45O,.N';
        //header('Content-Type: text/html; charset=utf-8');
        $this->db->from('matrimony_profile');
        $this->db->where('ip_address', ($ip));
        if ($login_type) {
            $this->db->where('registration_platform', $login_type);
        }

        $email_exists_query = $this->db->get();
        //echo $this->db->last_query();
        if ($email_exists_query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     *  this function use for check email already exists in database for forgot password
     * @return query run
     */
    function check_register_email_exists_or_not($email, $type = null)
    {
        $type = preg_split('/,/', $type);
        $user_type = str_replace('"', "", $type[1]);

        $this->db->from('matrimony_profile');

        $this->db->where('email', salt($email));
        $this->db->where('registration_type', $user_type);

        $email_exists_query = $this->db->get();
        if ($email_exists_query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }



    /**
     *  this function use for check email already exists in database for register
     * @return query run
     */
    function check_register_email_exists_or_not_profile($email)
    {

        $this->db->from('profile_login');

        $this->db->where('email', salt($email));

        $email_exists_query = $this->db->get();
        if ($email_exists_query->num_rows() > 0) {

            return 1;
        } else {
            return 0;
        }
    }

    /**
     * busmode
     *  this function use for check mobile already exists in database for register
     * @return query run
     */
    function check_register_mobile_exists_or_not_web_reg($mobile)
    {
        $this->db->from('users');
        $this->db->where('phone =', $mobile);
        $email_exists_query = $this->db->get();
        if ($email_exists_query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * this function use for get table full description
     * @param $table_name (get details database table name)
     * @param $select_filds (get details column name list if send black then get full details else get selected columns details)
     * @param $where_condition (where condition variable value mandatory get details)
     * @return mixed (return data row array)
     */
    function get_multiple_row($table_name, $select_filds, $where_condition=null, $order = null)
    {

        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($order) {
            $this->db->order_by($order);
        }

        $this->db->from($table_name);
        if($where_condition){
            $this->db->where($where_condition);
        }

        $query = $this->db->get();
        $result = $query->result();
        //echo $this->db->last_query();
        return $result;
    }

    function get_row($table_name, $select_filds, $where_condition=null, $order = null)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($order) {
            $this->db->order_by($order);
        }

        $this->db->from($table_name);
        $this->db->where($where_condition);
        $query = $this->db->get();
        $result = $query->row();

        //echo $this->db->last_query();
        return $result;
        //null given if no row found
    }

    /**
     * this is simple insert data function
     * @param $table_name (database insert table name)
     * @param $insert_details (insert data array)
     * @return mixed (return true or false)
     */
    function insert_details($table_name, $insert_details)
    {
        $return_value = $this->db->insert($table_name, $insert_details);
        return $return_value;
    }

    /**
     * this function insert data and return last insert id
     * @param $table_name (database insert table name)
     * @param $insert_details (insert data array)
     * @return mixed (return last insert id)
     */
    function get_lastinsertid_after_insertdata($table_name, $insert_details)
    {
        $return_value = $this->db->insert($table_name, $insert_details);
        $last_insert_id = $this->db->insert_id();
        //echo $this->db->last_query();die;
        return $last_insert_id;
    }

    /**
     * this function use for insert batch details
     * @param $table_name
     * @param $insert_details
     * @return mixed
     */
    function insert_batch_details($table_name, $insert_details)
    {
        $return_value = $this->db->insert_batch($table_name, $insert_details);
        return $return_value;
    }

    /**
     * this function use for update data in database
     * @param $table_name (database update table name)
     * @param $update_data (update data array)
     * @param $where_condition (update condition array)
     * @param $orwhere_condition (update secound condition array like or where)
     * @return mixed (return update successfully YES or NOT)
     */
    function update_details($table_name, $update_data, $where_condition = null, $orwhere_condition = null)
    {
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }
        if ($orwhere_condition != '') {
            $this->db->or_where($orwhere_condition);
        }
        $return_data = $this->db->update($table_name, $update_data);
        //echo $this->db->last_query();die;
        return $return_data;
    }

    /**
     * //deleted_at null
     * this function use for get listing data from database
     * @param $table_name (get listing database table name)
     * @param $select_filds (get details column name list if send black then get full details else get selected columns details)
     * @param $where_condition (get data where condition if send black then no condition apply all details fetch else condition base data fetch)
     * @return mixed (return get data array)
     */
    function get_listing_details($table_name, $select_filds, $where_condition = null, $order_by = null, $limit = null, $groupby=null)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }
        if ($order_by) {
            $this->db->order_by($order_by);
        }
        if ($groupby) {
            $this->db-> select_max('id' , 'id');
            $this->db->group_by($groupby);
        }
        if ($limit) {
            $this->db->limit($limit);
        }
        $this->db->where('deleted_at =', null);
        $query = $this->db->get($table_name);
        $result = $query->result();
        //echo $this->db->last_query();die;
        return $result;
        //if no result found then array(0) will receive

    }

    function get_listing_details_array($table_name, $select_filds, $where_condition = null, $order_by = null, $limit = null, $groupby=null)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }
        if ($order_by) {
            $this->db->order_by($order_by);
        }
        if ($groupby) {
            $this->db-> select_max('id' , 'id');
            $this->db->group_by($groupby);
        }
        if ($limit) {
            $this->db->limit($limit);
        }
        $this->db->where('deleted_at =', null);
        $query = $this->db->get($table_name);
        $result = $query->result_array();

        //echo $this->db->last_query();die;
        return $result;
        //if no result found then array(0) will receive

    }

    function get_all_rows_field_like($table_name, $select_filds,$field_name,$value, $where_condition = null, $order_by = null, $limit =
    null,$groupby=null)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }
        if ($order_by) {
            $this->db->order_by($order_by);
        }
        if ($groupby) {
            $this->db-> select_max('id' , 'id');
            $this->db->group_by($groupby);
        }
        if ($limit) {
            $this->db->limit($limit);
        }
        $this->db->like($field_name,$value);
        $this->db->where('deleted_at =', null);
        $query = $this->db->get($table_name);
        $result = $query->result();
        //echo $this->db->last_query();die;
        return $result;
    }

    //for ajax call only
    //function for get data from two tables
    function get_join_listing_details($table_name,$join_with, $join_condition, $select_filds, $where_condition = null, $order_by = null,
                                      $limit = null,$groupby=null)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $keys = array_keys($where_condition);
            $values = array_values($where_condition);
            $this->db->where("$table_name."."$keys[0] =", $values[0]);
            if(count($where_condition)>0){
                $this->db->where("$join_with."."$keys[1] =", $values[1]);
            }

        }
        if ($order_by) {
            $this->db->order_by($order_by);
        }
        if ($groupby) {
            $this->db-> select_max('id' , 'id');
            $this->db->group_by($groupby);
        }
        if ($limit) {
            $this->db->limit($limit);
        }
        $this->db->from($table_name);
        $this->db->join($join_with,$join_condition);
        $query = $this->db->get();
        $result = $query->result();
        //echo $this->db->last_query();die;
        return $result;
    }

    function get_join_listing_details_in_array($table_name,$join_with, $join_condition, $select_filds, $where_condition = null, $order_by = null,
                                               $limit = null,$groupby=null)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $keys = array_keys($where_condition);
            $values = array_values($where_condition);
            $this->db->where("$table_name."."$keys[0] =", $values[0]);
        }
        if ($order_by) {
            $this->db->order_by($order_by);
        }
        if ($groupby) {
            $this->db-> select_max('id' , 'id');
            $this->db->group_by($groupby);
        }
        if ($limit) {
            $this->db->limit($limit);
        }
        $this->db->from($table_name);
        $this->db->join($join_with,$join_condition);
        $query = $this->db->get();
        $result = $query->result_array();
        //echo $this->db->last_query();die;
        return $result;
    }

    //get
    // ----------SINGLE ROW---------------
    // after join two table
    function get_row_with_join_two_tables($table_name,$join_with, $join_condition, $select_filds, $where_condition = null, $order_by = null,
                                          $limit = null,$groupby=null)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $keys = array_keys($where_condition);
            $values = array_values($where_condition);
            $this->db->where("$table_name."."$keys[0] =", $values[0]);
        }
        if ($order_by) {
            $this->db->order_by($order_by);
        }
        if ($groupby) {
            $this->db-> select_max('id' , 'id');
            $this->db->group_by($groupby);
        }
        if ($limit) {
            $this->db->limit($limit);
        }
        $this->db->from($table_name);
        $this->db->join($join_with,$join_condition);
        $this->db->where($table_name.'.deleted_at =', null);
        $this->db->where($join_with.'.deleted_at =', null);
        $query = $this->db->get();
        $result = $query->row();
        //echo $this->db->last_query();die;
        return $result;
    }

    //function for get all rows from two tables
    function get_join_listing($table_name,$join_with, $join_condition, $select_filds, $where_condition = null, $order_by = null,
                              $limit = null,$groupby=null)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }
        if ($order_by) {
            $this->db->order_by($order_by);
        }
        if ($groupby) {
            $this->db-> select_max('id' , 'id');
            $this->db->group_by($groupby);
        }
        if ($limit) {
            $this->db->limit($limit);
        }
        $this->db->from($table_name);
        $this->db->join($join_with,$join_condition);
        $this->db->where($table_name.'.deleted_at =', null);
        $this->db->where($join_with.'.deleted_at =', null);
        $query = $this->db->get();
        $result = $query->result();
        //echo $this->db->last_query();die;
        return $result;
    }

    //function for get data from three tables
    function get_join_three_tables($table_name,$first_join_table, $second_join_table, $first_join_condition, $second_join_condition,
                                   $select_filds,
                                   $where_condition = null, $order_by = null,
                                   $limit = null,$groupby=null)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
//        if ($where_condition != '') {
//          $keys = array_keys($where_condition);
//          $values = array_values($where_condition);
//            $this->db->where("$table_name."."$keys[0] =", $values[0]);
//        }
        if ($order_by) {
            $this->db->order_by($order_by);
        }
        if ($groupby) {
            $this->db-> select_max('id' , 'id');
            $this->db->group_by($groupby);
        }
        if ($limit) {
            $this->db->limit($limit);
        }
        $this->db->from($table_name);
        $this->db->join($first_join_table,$first_join_condition);
        $this->db->join($second_join_table,$second_join_condition);
        $this->db->where($table_name.'.deleted_at =', null);
        $this->db->where($first_join_table.'.deleted_at =', null);
        $this->db->where($second_join_table.'.deleted_at =', null);
        $query = $this->db->get();
        $result = $query->result();
        echo $this->db->last_query();die;
        return $result;
    }



    function get_listing_where_in($table_name, $select_filds, $match_field, $where_condition, $order_by = null)
    {   //var_dump($where_condition);
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $this->db->where_in($match_field, $where_condition);
        }

        if ($order_by) {
            $this->db->order_by($order_by);
        }
        $this->db->where('deleted_at =', null);
        $query = $this->db->get($table_name);
        $result = $query->result();
        // echo $this->db->last_query();exit;
        return $result;
    }



    function get_listing_details_by_limit($table_name, $select_filds, $where_condition, $order_by, $limit, $start)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }

        $this->db->order_by($order_by, 'DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get($table_name);
        $result = $query->result();
        return $result;
    }






    function get_multipel_data($table, $field, $check, $selected)
    {
//        $selected = json_decode($selected);
        $this->db->select($field);
        $this->db->where_in($check, $selected);
        $query = $this->db->get($table);
        $data = array();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return '';
    }

    function get_multipel_data_with_jason_decode($table, $field, $check, $selected)
    {
        $selected = json_decode($selected);
        $this->db->select($field);
        $this->db->where_in($check, $selected);
        $query = $this->db->get($table);
        $data = array();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    /**
     * this function use for remove data from database just pass some value and this is global function
     * @param $table_name remove data table name
     * @param $where_condition remove where condition
     * @return mixed return remove success or not
     */
    function remove_details_from_database($table_name, $where_condition)
    {
        $return_details = $this->db->delete($table_name, $where_condition);
        return $return_details;
    }

    function remove_profile_image_from_database($table_name, $where_condition)
    {

        $this->db->select('profile_image');
        $this->db->from($table_name);
        $this->db->where($where_condition);
        $query = $this->db->get();
        $result = $query->row();
        if ($result) {
            $Path = "upload_dir/member_images/";
            $old_File = $Path . $result->profile_image;
            unlink($old_File);
        }

        $return_details = $this->db->delete($table_name, $where_condition);
        return $return_details;
    }



    /////////////////////////////////////////////////////// end //////////////////////////////////////////////////////////////////

    function get_listing_details_orderBY($table_name, $select_filds, $where_condition, $order_by)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }
        if ($order_by)
            $this->db->order_by($order_by, 'DESC');
        $query = $this->db->get($table_name);
        //echo $this->db->last_query();
        $result = $query->result();
        return $result;
    }

    function deleteDetailFromDatabase($table_name, $where_condition)
    {
        $return_details = $this->db->delete($table_name, $where_condition);
        return $return_details;
    }

    //========Update Data in Database===================//
    function updateDetails($table_name, $update_data, $where_condition, $orwhere_condition)
    {
        //===check where condition===//
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }
        //=====check orwhere condition=//
        if ($orwhere_condition != '') {
            $this->db->or_where($orwhere_condition);
        }
        //=====update query=====//
        $return_data = $this->db->update($table_name, $update_data);
        return $return_data;
    }

    //========Update Data in Database===================//
    function update_details_where_in($table_name, $update_data, $field, $where_condition)
    {
        //===check where condition===//
        if ($where_condition != '') {
            $this->db->where_in($field, $where_condition);
        }
        //=====check orwhere condition=//

        //=====update query=====//
        $return_data = $this->db->update($table_name, $update_data);
        //echo $this->db->last_query();exit;
        return $return_data;
    }

    //========Update Data in Database===================//
    function update_details_where($table_name, $update_data, $where_condition, $where_condition2)
    {
        //===check where condition===//
        if ($where_condition != '') {
            $this->db->where_in($where_condition);
        }
        if ($where_condition2 != '') {
            $this->db->where_in($where_condition2);
        }
        //=====check orwhere condition=//

        //=====update query=====//
        $return_data = $this->db->update($table_name, $update_data);
        return $return_data;
    }


    /**
     * this function use for total count record
     * @param $table_name count from table name
     * @param $where_condition count condition
     * @return mixed return total record
     */
    function get_total_count_record($table_name, $where_condition)
    {
        if ($where_condition != '') {
            $this->db->where($where_condition);
        }
        $this->db->from($table_name);
        $total_result = $this->db->count_all_results();
        //echo $this->db->last_query();die;
        return $total_result;
    }

    /**
     * this function use for update details
     * @table_name (table name)
     * @insert_details (update data details)
     * @where_condition (update condition)
     * @return this function return update status
     */
    function general_updateDetails($table_name, $insert_details, $where_array1)
    {
        $this->db->where($where_array1);
        $return_value = $this->db->update($table_name, $insert_details);
        return $return_value;
    }



    function getCountRecord($table_name, $where_condition = null)
    {

        $this->db->from($table_name);
        if ($where_condition) {
            $this->db->where($where_condition);
        }
        $total_result = $this->db->count_all_results();
        //echo $this->db->last_query();exit;
        return $total_result;

    }


    /**
     *  this function use for check email already exists in database
     * @return query run
     */
    function check_email_exists($table,$where)
    {
        $this->db->from($table);

        $this->db->where($where);

        $email_exists_query = $this->db->get();
        if ($email_exists_query->num_rows() > 0) {

            return 1;
        } else {
            return 0;
        }

    }








    //Get one field of a row of any table
    /**
     * @param $table_name
     * @param $select_filds
     * @param $match_field
     * @param $where_condition
     * @param null $order_by
     * @return mixed
     */
    function get_listing_where($table_name, $select_filds, $match_field, $where_condition, $order_by = null)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $this->db->where($match_field, $where_condition);
        }

        if ($order_by) {
            $this->db->order_by($order_by);
        }
        $query = $this->db->get($table_name);
        $result = $query->row();
        return $result;
    }



    function general_update($table=null,$field=null,$where=null)
    {
        if ($where != '') {
            $this->db->where($where);
        }

        $return_data = $this->db->update($table, $field);
        //echo $this->db->last_query();die;
        return $return_data;
    }



    function get_no_of_total_rows_deleted_at_null($table_name,$where_condition = null){
        $this->db->select();
        $this->db->where('deleted_at =', null);
        if ($where_condition) {
            $this->db->where($where_condition);
        }
        $query = $this->db->get($table_name);
        return $query->num_rows();
    }

    function get_settings(){
        $this->db->from('settings');
        $query = $this->db->get();
        $result = $query->row();
        return $result;
        //null given if no row found
    }
    public function fetch_menu_data($limit, $start){

        $this->db->select("*");
        $this->db->from("sm_categories");
        $this->db->order_by("ing_id", "DESC");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query;
    }

    function get_join_listing_load_details($start,$limit,$table_name,$join_with, $join_condition, $select_filds, $where_condition = null, $order_by = null, $limit_cond = null,$groupby=null)
    {
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($where_condition != '') {
            $keys = array_keys($where_condition);
            $values = array_values($where_condition);
            $this->db->where("$join_with."."$keys[0] =", $values[0]);
            $this->db->where("$table_name."."$keys[1] =", $values[1]);

        }

        $this->db->from($table_name);
        $this->db->join($join_with,$join_condition);
        if ($limit) {
            $this->db->limit($limit,$start);
        }

        if ($groupby) {
            $this->db-> select_max('id' , 'id');
            $this->db->group_by($groupby);
        }
        return  $this->db->get();
        
    }
    function get_multiple_rows_stat_end_limit($table_name,$select_filds,$where_condition,$order=null,$limit=null,$start=null){
        if ($select_filds != '') {
            $this->db->select($select_filds);
        } else {
            $this->db->select('*');
        }
        if ($order) {
            $this->db->order_by($order);
        }

        $this->db->from($table_name);
        if ($limit) {
            $this->db->limit($limit,$start);
        }
        if($where_condition){
            $this->db->where($where_condition);
        }
        return $query = $this->db->get();
//        $result = $query->result();
//        echo $this->db->last_query();
//        die;
//        return $result;
    }


}