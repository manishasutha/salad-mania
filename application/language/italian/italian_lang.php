<?php

/*
 * English language
 */
#Admin dashboard page 
$lang['dashboard'] = 'Cruscotto';
$lang['app_users'] = 'Utenti di app'; 
$lang['customers'] = 'Clienti'; 
$lang['ratings'] = 'Giudizi';
$lang['pending'] = 'In attesa';
$lang['recent_users'] = 'Utenti recenti';
$lang['Admin'] = 'Admin';

$lang['id'] = 'ID';
$lang['name'] = 'Nome';
$lang['email'] = 'e-mail';
$lang['zipe_code'] = 'Cap';
$lang['created_date'] = 'Data di Creazione';
$lang['login_from'] = 'Accedi da';
$lang['status'] = 'Stato';
$lang['privacy_status'] = 'Stato della privacy';
$lang['view_all_users'] = 'Visualizza tutti gli utenti';


#customer list page
$lang['customer_list'] = 'lista dei clienti';
$lang['identity'] = 'Identità';
$lang['category'] = 'Categoria';
$lang['company_name'] = 'Nome della ditta';
$lang['phone_number'] = 'Numero di telefono';
$lang['city'] = 'Città';
$lang['duty_status'] = 'Stato di servizio';
$lang['edit'] = 'modificare';
$lang['delete'] = 'Elimina';
$lang['add_customer'] = 'Aggiungi cliente';
$lang['off_duty'] = 'fuori servizio';
$lang['on_duty'] = 'di turno';
$lang['enable'] = 'abilitare';
$lang['disable'] = 'disattivare';
$lang['waiting_for_approval'] = 'In attesa di approvazione';
$lang['menu'] = 'Menu';

# admin user list page
$lang['view'] = 'vista';
$lang['type'] = 'genere';
$lang['last_login'] = 'Ultimo accesso';
$lang['users'] = 'utenti';

# admin rating page
$lang['rating'] = 'Valutazione';
$lang['rating_list'] = 'Elenco di valutazione';
$lang['customer'] = 'Cliente';
$lang['customer_name'] = 'Nome del cliente';
$lang['total_rating'] = 'Valutazione totale';
$lang['total_comment'] = 'Commento totale';
$lang['last_updated'] = 'Ultimo aggiornamento';

# admin notification list page
$lang['notification'] = 'Notifica';
$lang['date'] = 'Data';
$lang['read_status'] = 'Leggi lo stato';
$lang['action'] = 'Azione';
$lang['notification_list'] = 'Lista di notifica';
$lang['select_action'] = 'seleziona azione';
$lang['move_to_trash'] = 'Sposta nel cestino';
$lang['read'] = 'Leggere';
$lang['unread'] = 'Non letto';

# admin notification list page
$lang['trash_list'] = 'Lista dei rifiuti';
$lang['rate_us'] = 'Votaci';
# ****date 23-08-2018 *************** 
#category page
$lang['setting'] = 'Ambientazione';
$lang['category_list'] = 'Elenco di categorie';
$lang['description'] = 'Descrizione';
$lang['update_date'] = 'Data di aggiornamento';
$lang['add_category'] = 'Aggiungi categoria';
$lang['name_italian'] = 'Nome italiano';
#keywords page
$lang['keywords'] = 'parole';
$lang['add_keywords'] = 'Aggiungi parole chiave';
$lang['category'] = 'Categoria';
$lang['add_keywords'] = 'Aggiungi parola chiave';

#Filter pafe
$lang['filter'] = 'Filtro';
$lang['add_filter'] = 'Aggiungi filtro';
$lang['filter_title'] = 'Filtro titolo';
$lang['filter_description'] = 'Descrizione del filtro';
$lang['filter_title_italian'] = 'Filtro Titolo (italiano)';

#Menu page page
$lang['company'] = 'Azienda';
$lang['menu_title'] = 'Titolo del menu';
$lang['menu_category'] = 'Categoria di menu';
$lang['type'] = 'genere';

# menu category page
$lang['menu_title'] = 'Titolo del menu';
$lang['menu_title_italian'] = 'Titolo del menu italiano';
$lang['add_menu_category'] = 'Aggiungi categoria di menu';
$lang['menu_categories'] = 'Categorie di menu';

#Add category page
$lang['add_category'] = 'Aggiungi categoria';

# ****** 24-08-2018 ****************
#Nav bar content
$lang['Administrator'] = 'Amministratore';
$lang['last_login'] = 'Ultimo accesso';
$lang['language'] = 'linguaggio';
$lang['notifications_not_avilable'] = 'Notifiche non disponibili';
$lang['my_profile'] = 'Il mio profilo';
$lang['change_password'] = 'Cambia la password';
$lang['logout'] = 'Disconnettersi';
$lang['italian'] = 'italiano';
$lang['English'] = 'Inglese';

#Left sidebar Menu item
$lang['Dashboard'] = 'Cruscotto';
$lang['Configuration'] = 'Configurazione';
$lang['Notifications'] = 'notifiche';
$lang['Trash_Notifications'] = 'Notifiche sul Cestino';
$lang['Rate_us'] = 'Votaci';
$lang['Email_Setting'] = 'Impostazioni email';
$lang['Language_Setting'] = 'Impostazione della lingua';

#Left side bar sub menu
$lang['About'] = 'Di';
$lang['Privacy'] = 'vita privata';
$lang['Terms_Conditions'] = 'Termini & Condizioni';
$lang['Filters'] = 'filtri';
$lang['Menus'] = 'menu';
$lang['Details'] = 'Dettagli';

#Footer contebnt
$lang['All_Rights'] = 'Tutti i diritti riservati: BeeOVEG';
#customer panel
$lang['Favourites'] = 'Preferiti';
$lang['Customer'] = 'Cliente';
$lang['Comment'] = 'Commento';
$lang['Post_On'] = 'Postare su';
$lang['recent_rating'] = 'Valutazione recente';
$lang['view_all_rating'] = 'Visualizza tutte le valutazioni';
$lang['title'] = 'Titolo';
$lang['custom_export_button'] = 'Esportare i dati in Excel';
$lang['search'] = 'Ricerca';
$lang['previous'] = 'precedente';
$lang['next'] = 'Il prossimo';
$lang['showing'] = 'Mostrando';

#customer info page.
$lang['Food Truck Details'] = 'Dettagli del camion di cibo';
$lang['Reviews'] = 'Recensioni';
$lang['Overview'] = 'Panoramica';
$lang['Show'] = 'Mostrare';
$lang['Hide'] = 'Nascondere';
$lang['Short Descriptions'] = 'Brevi descrizioni';
$lang['More Info'] = 'Ulteriori informazioni';
$lang['Contact'] = 'Contatto';
$lang['Address'] = 'Indirizzo';
$lang['Duty Status'] = 'Stato di servizio';
$lang['Open'] = 'Aperto';
$lang['Close'] = 'Vicino';
$lang['Call'] = 'Chiamata';
$lang['No Keywords'] = 'Nessuna parola chiave';
$lang['No Filters'] = 'Nessun filtro';
$lang['Menu is not available'] = 'Il menu non è disponibile';
$lang['Rating not available'] = 'Valutazione non disponibile.';
$lang['out of'] = 'fuori da';
$lang['Stars'] = 'Stelle';
$lang['Star'] = 'Stelle';
$lang['Total'] = 'Totale';
$lang['Posted on'] = 'postato su';
$lang['Star'] = 'Stella';

#profile change page
$lang['Profile'] = 'Profilo';
$lang['Surname'] = 'Cognome';
$lang['Change Picture'] = 'Cambia immagine';
$lang['Phone'] = 'Telefono';
$lang['Update'] = '';
$lang['Update'] = 'Aggiornare';

#change password page.
$lang['Change Password'] = 'Cambia la password';
$lang['Old Password'] = 'vecchia password';
$lang['Password'] = "Parola d'ordine";
$lang['Confirm Password'] = 'conferma password';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';

#Add customer page
$lang['Customer Personal info'] = 'Cliente Informazioni personali';
$lang['Identification Number'] = 'Numero identificativo';
$lang['Company Name'] = 'Nome della ditta';
$lang['Customer Details'] = 'Dettagli cliente';
$lang['IMEI Number'] = 'Numero IMEI';
$lang['Sim Number'] = 'Numero Sim';
$lang['Latitude'] = 'Latitudine';
$lang['Longitude'] = 'Longitudine';
$lang['Country'] = 'Nazione';
$lang['State'] = 'Stato';
$lang['City'] = 'Città';
$lang['Upload Image'] = 'Carica immagine';
$lang['You can upload multiple'] = 'Puoi caricare più immagini con proporzioni 3: 1';
$lang['Submit'] = 'Sottoscrivi';
$lang['First Name'] = 'Nome di battesimo';
$lang['Last Name'] = 'Cognome';
$lang['Email'] = 'E-mail';

# customer edit page.
$lang['Customer Edit'] = 'Modifica cliente';
# About us, privacy, terms conditions.
$lang['About Us'] = 'Riguardo a noi';
$lang['Add Privacy'] = 'Aggiungi privacy';
$lang['Add Terms & Conditions'] = 'Aggiungi termini e condizioni';
$lang['Terms & Conditions'] = 'Termini & Condizioni';

# Add keyword page.
$lang['Add Keyword'] = 'Aggiungi parola chiave';
$lang['Edit Keyword'] = 'Modifica parola chiave';
# Add Filter page.
$lang['Add Filter'] = 'Aggiungi filtro';
$lang['Select category'] = 'Seleziona categoria';
$lang['Edit Filter'] = 'Modifica filtro';
#Add menu category page.
$lang['Add Menu Category'] = 'Aggiungi categoria menu';
$lang['Edit Menu Category'] = 'Modifica categoria menu';
$lang['Edit Category'] = 'Modifica categoria';
$lang['Product Category'] = 'categoria di prodotto';

#Add Menu page
$lang['Add Menu'] = 'Aggiungi menu';
$lang['Add Product'] = 'Aggiungi prodotto';
$lang['Menu Category'] = 'Menu Categoria';
$lang['Product Category'] = 'categoria di prodotto';
$lang['Menu Name'] = 'Nome del menu';
$lang['Product Name'] = 'nome del prodotto';

$lang['Menu Description'] = 'Descrizione del menu';
$lang['Product Description'] = 'Descrizione del prodotto';
$lang['Tags'] = 'tag';
$lang['You can upload multiple images of 414x325px'] = 'Puoi caricare più immagini di 414x325px';
$lang['Add More Menu'] = 'Aggiungi altro menu';
$lang['Menu Edit'] = 'Menu Modifica';
$lang['Product Edit'] = 'Modifica del prodotto';
$lang['Product'] = 'Prodotto';

#smtp setting
$lang['Email Setting'] = 'Impostazioni email';
$lang['Port No'] = 'Porto n';
$lang['Host'] = 'Ospite';
$lang['data_table_info'] = 'Visualizzazione della pagina _PAGE_ di _PAGES_';
$lang['Farm Details'] = "Dettagli dell'azienda agricola";
?>





