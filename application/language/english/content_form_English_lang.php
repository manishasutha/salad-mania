<?php
//HOME
$lang['salad_mania'] = 'Salad Mania';
$lang['about_us'] = 'About Us';
$lang['menu'] = 'Menu';
$lang['story'] = 'Our Story';
$lang['contact'] = 'Contact Us';
$lang['login_sign'] = 'Login/SignUp';
$lang['cart'] = 'Cart';
$lang['cal'] = 'Cal: ';
$lang['jd'] = ' JD ';
$lang['qty'] = 'Qty';
$lang['add'] = 'Add';
$lang['extra'] ='Extra';
$lang['make_own_heading'] = 'Do You Wished A Customized Salad';
$lang['make_own_title'] = 'Make Your Own';
$lang['office_add'] = 'Office Address';
$lang['place'] = 'Boulevard Al Abdali';
$lang['city_admin'] = 'Amman-Jordan';
$lang['open_hr'] = 'Open Hours';
$lang['sun_thu'] = 'Sunday-Thursday';
$lang['sun_thu_time'] = '9.00 am to 12 pm';
$lang['fri'] = 'Friday';
$lang['fri_time'] ='9.00 am to 10 pm';
$lang['sat'] = 'Saturday';
$lang['sat_time'] = '10.00 am to 12 pm';
$lang['terms'] = 'Terms';
$lang['policy'] = 'Policy';
$lang['download'] = 'Download The App';
$lang['pay'] = 'Secure Pay';
$lang['top'] = 'Top';
$lang['no_more_salad'] = 'No More Salads';
$lang['done'] = 'Done';
$lang['terms_condition'] = 'Terms & Conditions';
$lang['make_now_text'] = 'pickup items, select quantity and create your own salad';
$lang['enter_recipe_name'] = 'Enter Recipe Name';
//Contact form
$lang['dietician_title'] = 'Contact Our Dietician';
$lang['name'] = 'Name';
$lang['email'] = 'Email';
$lang['subject'] ='Subject';
$lang['type_msg'] = 'Type Your Message';
$lang['send_msg'] = 'Send Message';

//Make your own
$lang['filter_nutrition'] = 'Nutrition Filter';
$lang['no_more_ingredients'] = 'No More Ingredients';
$lang['ingredients'] = 'Ingredients';
$lang['special_request'] = 'Special Request';

//Cart
$lang['bag'] ='Bag';
$lang['pick_up'] = 'Pick Up';
$lang['no_cart_item'] = 'No Cart Item';
$lang['delivery'] ='Delivery';
$lang['checkout'] = 'Checkout';

//Login Form
$lang['hello_user'] = 'Hello User';
$lang['login']='Login';
$lang['logout'] = 'Logout';
$lang['password'] = 'Password';
$lang['forgot_pass'] = 'Forgot Password';
$lang['cancel'] = 'Cancel';
$lang['failed'] = 'Failed';
$lang['created'] = 'Created';
$lang['pending'] = 'Pending Pickup';
$lang['picked'] = 'Picked';
$lang['completed'] = 'Completed';
$lang['canceled'] = 'Canceled';
$lang['not_available'] = 'Not Available';

//Register Form
$lang['register'] = 'Register';
$lang['full_name'] = 'Full Name ';
$lang['building_no'] = 'Building Number';
$lang['street_name'] ='Street Name';
$lang['mobile'] =' Mobile';
$lang['confirm_pass'] = 'Confirm Password';
$lang['agree_terms_condition'] = 'By Submitting, You’r Agree With';
$lang['create_account'] ='Create Account';

//guest login
$lang['guest_login'] = 'Continue As A Guest';

//my account
$lang['my_account'] = 'My Account';
$lang['phone'] = 'Phone';
$lang['address'] = 'Address';
$lang['my_profile'] = 'My Profile';
$lang['my_order'] = 'My Orders';
$lang['change_pass'] ='Change Password ';

//My profile
$lang['my_profile_heading'] = 'Profile';
$lang['update'] = 'Update';
$lang['first_name'] = 'First Name';
$lang['city'] ='City';

//my orders
$lang['choose_order'] = 'Choose Order';
$lang['choose_id'] = 'Choose Order Id';
$lang['shipping_detail'] = 'Shipping Details';
$lang['payment'] = 'Payment';
$lang['status'] = 'Status';
$lang['delivery_charge'] ='Delivery Charges';
$lang['taxes'] = 'Tax';
$lang['total'] = 'Total';
$lang['promo_code'] = 'Promo Code Applied';
$lang['grand_total'] = 'Grand Total';
$lang['order_status'] = 'Order Status';

//Change password
$lang['new_pass'] = 'New Password';
$lang['confirm_new_pass'] = 'Confirm New Password';

// Common text
$lang['submit'] = 'Submit';
$lang['save'] = 'Save';
$lang['awesome'] = 'Awesome';
$lang['payment_confirm'] = 'Your Payment Has Been Confirmed. Please Check Your Email';
$lang['home'] = 'Home';
$lang['error'] = 'Error';
$lang['success'] = 'Success';

//other text
$lang['edit_add'] ='Edit Address';
$lang['edit_detail'] ='Edit Detail';
$lang['main'] ='Main';
$lang['no_item_cart'] = 'No Items In Cart';
$lang['order'] = 'Order';
$lang['confirm'] ='Confirm';
$lang['payment_type'] = 'Payment Type';
$lang['cod'] = 'Cash on Delivery';
$lang['online'] ='Online';
$lang['subtotal'] = 'SubTotal';
$lang['items'] = 'Items';
$lang['date'] ='Date';
$lang['pickup_details'] = 'Pickup Details';
$lang['pickup_add'] = 'Pickup Address';
$lang['edit'] = 'Edit';
$lang['delivery_address'] = 'Delivery Address';
$lang['amman'] = 'Amman';
$lang['reset_password'] = 'Reset Password';
$lang['english'] ='English';
$lang['arabic'] ='عربي';
$lang['make_now'] ='Make Now';
$lang['add'] ='ADD';
$lang['add_bag'] ='Add To Bag';
$lang['add_on'] ='Add-On';
$lang['user_contact'] ='Some user contact with you by contact us form';
$lang['contact_enquery'] ='Contact Us Enquery';
$lang['dietician_contact'] ='Some user contact with you by dietician form';
$lang['dietician_enquery'] ='Dietician Enquery';

//validation msg
//email
$lang['email_not_register'] ='E-mail is not registered!! Please try again!!';
$lang['correct_email_pass'] = 'Please Enter correct email and password';
$lang['email_require'] = 'Please enter email';
$lang['sent_reset_link'] = 'Reset link has been sent to your registered email id';
$lang['error_email_send'] = 'Somthing went wrong on sending email for verify';
$lang['email_reset_pass'] = 'We have send you email to reset your password';
$lang['email_login'] = 'We have send you email to verify!! please verify the email to login';
$lang['valid_email'] ='Please enter a valid email address.';
$lang['email_verify_success'] ='Your email is successfully verified';
$lang['email_exist'] ='Email is already registered';
$lang['email_verify_error'] = 'E-mail is not verified!! Please try again';
 $lang['correct_email_pass'] ='Please Enter correct email and password';
 $lang['not_register_email'] ='E-mail is not registered Please Try again';

//general
$lang['field_require'] = 'The field is required';
$lang['min_character'] = 'Please enter at least 2 characters.';
$lang['regex_alpha'] = 'Please enter only alphabets';
$lang['remove_item_alert'] = 'Do you want to remove this item';
$lang['something_wrong'] = 'Something went wrong';
$lang['data_insert_error'] = 'Somthing went wrong inserting the data in database';
$lang['data_update_error'] = 'Somthing went wrong on updating the data.';
$lang['valid_url'] = 'Please enter a valid URL';
$lang['valid_no'] = 'Please enter a valid number';
$lang['form_submit_success'] = 'Submitted form Successfully';
$lang['login_success'] = 'Sucessfully login';
$lang['guest_register_success'] = 'Successfully register as a guest user';
$lang['token_expired'] ='Sorry, your token have expired';
$lang['record_update'] ='Your records updated successfully.';
$lang['no_record'] ='No record found.';
$lang['went_wrong'] = 'Somthing went wrong'; 

//Phone no
$lang['already_register'] ='Phone number is already registered';
$lang['invalid_no'] = 'Invalid mobile number';

//password
$lang['pass_match'] = 'Password and confirm password do not match';
$lang['pass_regex'] = 'Password must have one lowerCase, one upperCase,one special character and one digit';
$lang['incorrect_pass'] = 'Password is wrong!! Please try again';
$lang['reset_pass_success'] = 'Successfully reset password';
$lang['pass_update_success']='Password updated successfully';
$lang['pass_require'] = 'Please enter password';
$lang['email_not_register'] = 'Enter email is not registered';
$lang['reset_link'] = 'Reset link has been sent to your registered email id';
$lang['delete_success'] = 'Deleted Successfully';

//address
$lang['address_require'] = 'Please enter delivery address';
$lang['update_address_success'] = 'Successfully Updated address';
$lang['update_address_error'] = 'Something went wrong on updating address';

//cart/order
$lang['add_cart_success'] ='Successfully added in the cart';
$lang['order_success_msg'] = 'Your order has been successfully placed';
$lang['order_error_msg'] = 'Something went wrong on placing a order';

//order status
$lang['placed'] = 'Placed';
$lang['preparing'] = 'Preparing';
$lang['ready_to_ship'] = 'Ready to Ship';
$lang['out_for_delivery'] = 'Out for Delivery';
$lang['delivered'] = 'Delivered';
$lang['cancelled'] = 'Cancelled';
$lang['order_detail'] = 'ORDER DETAIL';
$lang['promo_code'] = 'Promo Code Applied';
$lang['delivery_charges'] = 'Delivery Charges';
$lang['grand_total'] = 'Grand Total';
$lang['total'] = 'Total';
$lang['taxes'] = 'Tax';
$lang['your_order'] = 'Your Order';
$lang['checkout'] = 'Checkout';
$lang['no_cart_items'] = 'No Cart Items';


$lang['confirm_address'] = 'Confirm Address';
$lang['change_address'] = 'Change Address';
$lang['add_new_address']='Add New Address';
$lang['add_address']='Add Address';
$lang['delivery_in_jordon']='Delivery is only in JORDAN';
$lang['time']='TIME';
$lang['order_detail']='ORDER DETAILS';
$lang['shipping_detail']='SHIPPING DETAILS';
$lang['order_items']='ORDER ITEMS';

?>
