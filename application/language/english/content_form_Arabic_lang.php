<?php
//HOME
$lang['salad_mania'] = 'Salad Mania';
$lang['about_us'] = 'About Us';
$lang['menu'] = 'القائمة';
$lang['story'] = 'قصتنا';
$lang['contact'] = 'تواصل معنا';
$lang['login_sign'] = 'الدخول/ تسجيل الدخول ';
$lang['cart'] = 'السلة';
$lang['cal'] = 'Cal: ';
$lang['jd'] = ' JD ';
$lang['qty'] = 'Qty';
$lang['add'] = 'اضف';
$lang['add_on'] ='اضافه';
$lang['make_own_heading'] = 'Do You Wished A Customized Salad';
$lang['make_own_title'] = 'سلـطتك عـلى زوقك ';
$lang['office_add'] = 'العنــوان';
$lang['place'] = 'بوليفارد العبـدلي';
$lang['city_admin'] = 'عمان - الاردن ';
$lang['open_hr'] = 'ساعات العمل ';
$lang['sun_thu'] = 'الأحد - الخميـس';
$lang['sun_thu_time'] = '9.00 am to 12 pm';
$lang['fri'] = 'الجمعـة';
$lang['fri_time'] ='9.00 am to 10 pm';
$lang['sat'] = 'السبـت';
$lang['sat_time'] = '10.00 am to 12 pm';
$lang['terms'] = 'الشـروط';
$lang['policy'] = 'سياسة الخصوصية';
$lang['download'] = 'حمـل التـطبيق';
$lang['pay'] = 'قنـوات الدفع الامن';
$lang['top'] = 'Top';
$lang['no_more_salad'] = 'No More Salads';
$lang['terms_condition'] = 'الشروط والاحكام';
$lang['make_now_text'] = 'عناصر الالتقاط ، حدد الكمية وإنشاء السلطة الخاصة بك';

//Contact form
$lang['dietician_title'] = 'للتواصل مع أخصائية التغذية ';
$lang['name'] = 'الاسم';
$lang['email'] = 'الايـميل';
$lang['subject'] ='العنـوان';
$lang['type_msg'] = 'اكتـب رسـالتك';
$lang['send_msg'] = 'ارسل';

//Make your own
$lang['filter_nutrition'] = 'اطـلب حسب نظامك الغذائي';
$lang['no_more_ingredients'] = 'No More Ingredients';

//Cart
$lang['bag'] ='السـلة';
$lang['pick_up'] = 'اسـتلام من المحـل';
$lang['no_cart_item'] = 'لا يوجد عنصر في السـلة';
$lang['delivery'] ='توصيل';
$lang['checkout'] = 'الاستمـرار';
$lang['add_bag'] ='أضف الى الحقيبة';

//Login Form
$lang['hello_user'] = 'مرحبا بك في سـالاد مانيـا';
$lang['login']='الدخـول';
$lang['logout'] = 'الخروج';
$lang['password'] = 'كلمة السـر';
$lang['forgot_pass'] = 'نسيـت كلمة السـر';
$lang['cancel'] = 'الغـاء';
$lang['failed'] = 'فشل';
$lang['created'] = 'خلقت';
$lang['pending'] = 'في انتظار بيك اب';
$lang['picked'] = 'اختار';
$lang['completed'] = 'منجز';
$lang['canceled'] = 'ألغيت';
$lang['not_available'] = 'غير متوفر';
$lang['order_detail']='تفاصيل الطلب';


//Register Form
$lang['register'] = 'تسجيل الدخـول';
$lang['full_name'] = 'اسمـك ';
$lang['building_no'] = 'رقم المبنـى';
$lang['street_name'] ='رقم او اسم الشارع';
$lang['mobile'] =' رقم الموبايل';
$lang['confirm_pass'] = 'تاكيد كلمة السر';
$lang['agree_terms_condition'] = 'عنـد الضغط , يتم قبـول الشروط والاحكـام';
$lang['create_account'] ='تسجيل حساب جديد';

//guest login
$lang['guest_login'] = 'الاستمرار بدون تسجيل ';

//my account
$lang['my_account'] = 'حسـابي';
$lang['phone'] = 'الرقم';
$lang['address'] = 'العنـوان';
$lang['my_profile'] = 'ملفـي الشخصـي';
$lang['my_order'] = 'طلبـاتي';
$lang['change_pass'] ='تغيـير كلـمة السـر ';

//My profile
$lang['my_profile_heading'] = 'ملفي الشخصي';
$lang['update'] = 'تحديـث';
$lang['first_name'] = 'الاسـم';
$lang['city'] ='المديـنة';

//my orders
$lang['choose_order'] = 'اختـار الطلب';
$lang['choose_id'] = 'اختـار نوع الطلـب';
$lang['shipping_detail'] = 'عنـوان التـوصيل';
$lang['payment'] = 'الدفع';
$lang['status'] = 'الحالة';
$lang['delivery_charge'] ='رسـوم الـتوصيل';
$lang['taxes'] = 'الضرائب';
$lang['total'] = 'الاجمـالي';
$lang['promo_code'] = 'عندك كوبون التخفيض؟ ';
$lang['grand_total'] = 'المجمـوع';
$lang['order_status'] = 'حـالة الطلـب';

//Change password
$lang['new_pass'] = 'كلمة سر جديدة';
$lang['confirm_new_pass'] = 'تأكـد كلمـة السـر';

// Common text
$lang['submit'] = 'ارسـل';
$lang['save'] = 'حفظ';
$lang['awesome'] = 'ممتـاز';
$lang['payment_confirm'] = 'تم تاكيد عمليـة الشـراء, الرجاء التحقق من الايميـل';
$lang['home'] = 'الصفحة الرئيسيـة';
$lang['error'] = 'خطـأ';
$lang['success'] = 'نجـاح';

//other text
$lang['edit_add'] ='تعديـل العنـوان';
$lang['no_item_cart'] = '! سلة الشراء فارغة';
$lang['order'] = 'اطلـب';
$lang['confirm'] ='تاكيـد';
$lang['payment_type'] = 'طريقـة الدفـع';
$lang['cod'] = 'ادفع نقدا عند استلام طلبيتك';
$lang['online'] ='الدفع اونلاين';
$lang['subtotal'] = 'المجمـوع';
$lang['items'] = 'العناصر';
$lang['date'] ='التاريـخ';
$lang['pickup_details'] = 'تفاصيـل الاسـتلام';
$lang['pickup_add'] = 'عنوان الاستلام';
$lang['edit'] = 'تعديل';
$lang['delivery_address'] = 'عنـوان التوصيل';
$lang['amman'] = 'عمان';
$lang['reset_password'] = 'إعادة كلمـة السـر';
$lang['english'] ='English';
$lang['arabic'] ='عربي';
$lang['make_now'] ='اضف';
$lang['add'] ='أضف';
$lang['user_contact'] ='بعض اتصالات المستخدم معك عن طريق الاتصال بنا نموذج';
$lang['contact_enquery'] ='اتصل بنا';
$lang['dietician_contact'] ='بعض اتصال المستخدم معك عن طريق اختصاصي التغذية';
$lang['dietician_enquery'] ='اختصاصي التغذية بنا Enquery';


//validation msg
//email
$lang['email_not_register'] ='!!الايمـيل غير مسجل , حاول مره اخرى ';
$lang['correct_email_pass'] = 'الرجاء ادخال ايميل وكلمة سر صحيحتين ';
$lang['email_require'] = 'الرجـاء ادخال الايميـل';
$lang['sent_reset_link'] = 'تم ارسـال رابط إلى ايميلك';
$lang['error_email_send'] = 'حـدث خطا ما خلال ارسال ايميل التعريـف ';
$lang['email_reset_pass'] = 'تـم ارسـال ايميـل لاعادة كلمـة السـر';
$lang['email_login'] = '  تم ارسال الايميـل للتاكد, الرجـاء التحقق من الايميـل للدخول';
$lang['valid_email'] ='الرجـاء ادخـال ايميـل صحيـح';
$lang['email_verify_success'] ='تـم التحـقق من الايميل بنجـاح';
$lang['email_exist'] ='الايميـل مسجـل من قبـل';
$lang['email_verify_error'] = 'لم يتـم التحقق من الايميـل , الرجـاء المحاولة مرة اخرى ';
 $lang['correct_email_pass'] ='Please Enter correct email and password';

//general
$lang['field_require'] = 'هـذا الحقـل مطلـوب';
$lang['min_character'] = 'الـرجاء ادخـال مقطعـين على الاقـل ';
$lang['regex_alpha'] = 'الـرجاء ادخـال احرف فقـط';
$lang['remove_item_alert'] = 'هـل تريد ازالـة هذا العنصر ؟ ';
$lang['something_wrong'] = 'حدث خطـأ ما ';
$lang['data_insert_error'] = 'حدث خطـا اثنـاء ادخال البيـانات';
$lang['data_update_error'] = 'حدث خطـا ما اثنـاء تحديث البيـانات ';
$lang['valid_url'] = 'الرجاء ادخـال رابـط صحيـح';
$lang['valid_no'] = 'الـرجـا ادخـال رقم صحيح';
$lang['form_submit_success'] = 'تـم الارسـال بنجـاح';
$lang['login_success'] = 'تـم الدخـول بنجـاح';
$lang['guest_register_success'] = 'تم الدخول بنجاح بدون تسجيل ';
$lang['token_expired'] =' عفـوا , كوبون الخصـم انتهت صلاحيتـه  ';
$lang['record_update'] ='تم تحديث بياناتك بنجاح.';
$lang['no_record'] ='لا يوجد سجلات';
$lang['email_not_register'] = 'Enter email is not registered';
$lang['reset_link'] = 'Reset link has been sent to your registered email id';
$lang['delete_success'] = 'Deleted Successfully';
$lang['ingredients'] = 'مكونات';
$lang['special_request'] = 'طلب خاص';
$lang['went_wrong'] = 'Somthing went wrong'; 
//Phone no
$lang['already_register'] ='الرقـم مسجـل من قبـل';
$lang['invalid_no'] = 'الرقـم غيـر صحيـح';

//password
$lang['pass_match'] = ' كلمـة السر غير متطابقة ';
$lang['pass_regex'] = 'كلمـة السـر يجب ان تحتـوي حرف كبير , حرف صغير , رقم و رمز ';
$lang['incorrect_pass'] = 'كلمـة السـر غير صحيحـة , الرجـاء المحـاولة مرة اخرى ';
$lang['reset_pass_success'] = 'تـم اعـادة كلمـة السـر بنجـاح';
$lang['pass_update_success']='تـم تحديـث كلمـة السـر بنجـاح';
$lang['pass_require'] = 'الـرجاء ادخـال كلمـة السر ';

//address
$lang['address_require'] = 'الـرجاء ادخال عنـوان التـوصيـل';
$lang['update_address_success'] = 'تـم تحديث العنـوان';
$lang['update_address_error'] = 'حـدث خطـا اثناء تحديث العنوان ';

//cart/order
$lang['add_cart_success'] =' تمـت الاضـافة بنجـاح';
$lang['order_success_msg'] = 'تم ارسـال الطلب بنجـاح';
$lang['order_error_msg'] = 'حدث خطـأ اثناء ارسـال الطـلب ';

//order status
$lang['placed'] = 'وضعت';
$lang['preparing'] = 'خطة';
$lang['ready_to_ship'] = 'على استعداد للسفينة';
$lang['out_for_delivery'] = 'خارج للتوصيل';
$lang['delivered'] = 'تم التوصيل';
$lang['cancelled'] = 'ألغيت';
$lang['order_detail'] = 'تفاصيل الأمر';
$lang['shipping_detail'] = 'تفاصيل الشحن';
$lang['order_items']='طلب بضاعة';
$lang['promo_code'] = 'الرمز الترويجي المطبق';
$lang['delivery_charges'] = 'رسوم التوصيل';
$lang['grand_total'] = 'المجموع الكلي';
$lang['total'] = 'مجموع';
$lang['taxes'] = 'الضرائب';
$lang['done'] = ' تم';
$lang['edit_detail'] ='تحرير التفاصيل';
$lang['main'] ='الأساسية';
$lang['extra'] ='إضافي';
$lang['checkout'] = 'الدفع';
$lang['your_order'] = 'طلبك';

$lang['enter_recipe_name'] = 'أدخل اسم الوصفة';
$lang['no_cart_items'] = 'لا توجد عناصر سلة';

$lang['confirm_address'] = 'تأكيد العنوان';
$lang['change_address'] = 'تغيير العنوان';
$lang['add_new_address']='إضافة عنوان جديد';
$lang['add_address']='اضف عنوان';
$lang['delivery_in_jordon']='تسليم فقط في الأردن';
$lang['time']='زمن';

?>
